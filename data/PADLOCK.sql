USE master
GO

IF EXISTS(select * from sys.databases where name='PADLOCK')
DROP DATABASE PADLOCK

CREATE DATABASE PADLOCK
GO

USE PADLOCK
GO
-- ----------------------------
-- Table structure for ADMINISTRADORES
-- ----------------------------
DROP TABLE [ADMINISTRADORES]
GO
CREATE TABLE [ADMINISTRADORES] (
[ID_ADMINISTRADOR] int NOT NULL IDENTITY(1,1) ,
[ID_USUARIO] int NOT NULL 
)


GO

-- ----------------------------
-- Records of ADMINISTRADORES
-- ----------------------------
BEGIN TRANSACTION
GO
SET IDENTITY_INSERT [ADMINISTRADORES] ON
GO
SET IDENTITY_INSERT [ADMINISTRADORES] OFF
GO
COMMIT TRANSACTION
GO

-- ----------------------------
-- Table structure for ADMINISTRADORES_APLICACIONES
-- ----------------------------
DROP TABLE [ADMINISTRADORES_APLICACIONES]
GO
CREATE TABLE [ADMINISTRADORES_APLICACIONES] (
[ID_ADMINISTRADOR_APLICACION] int NOT NULL IDENTITY(1,1) ,
[ID_ADMINISTRADOR] int NOT NULL ,
[ID_APLICACION] int NOT NULL 
)


GO

-- ----------------------------
-- Records of ADMINISTRADORES_APLICACIONES
-- ----------------------------
BEGIN TRANSACTION
GO
SET IDENTITY_INSERT [ADMINISTRADORES_APLICACIONES] ON
GO
SET IDENTITY_INSERT [ADMINISTRADORES_APLICACIONES] OFF
GO
COMMIT TRANSACTION
GO

-- ----------------------------
-- Table structure for APLICACION_CONFIGURACION
-- ----------------------------
DROP TABLE [APLICACION_CONFIGURACION]
GO
CREATE TABLE [APLICACION_CONFIGURACION] (
[ID_CONFIGURACION] int NOT NULL IDENTITY(1,1) ,
[ID_APLICACION] int NOT NULL ,
[SESSION_MAX_DURATION_MINS] int NULL DEFAULT ((0)) ,
[SESSION_MAX_SAME_IP_CONNECTIONS] int NULL DEFAULT ((0)) ,
[SESSION_REUSE_SESSIONS] int NULL DEFAULT ((0)) ,
[SESSION_MAX_SESSIONS_PER_DAY] int NULL DEFAULT ((0)) ,
[SESSION_MAX_SESSIONS_PER_USER] int NULL DEFAULT ((0)) ,
[SYSTEM_NO_NEW_SESSIONS] int NULL DEFAULT ((0)) ,
[SYSTEM_DOWN] int NULL DEFAULT ((0)) ,
[ESTADO_CONFIGURACION] tinyint NOT NULL DEFAULT ((1)) ,
[ESTADO] tinyint NOT NULL DEFAULT ((1)) 
)


GO

-- ----------------------------
-- Records of APLICACION_CONFIGURACION
-- ----------------------------
BEGIN TRANSACTION
GO
SET IDENTITY_INSERT [APLICACION_CONFIGURACION] ON
GO
INSERT INTO [APLICACION_CONFIGURACION] ([ID_CONFIGURACION], [ID_APLICACION], [SESSION_MAX_DURATION_MINS], [SESSION_MAX_SAME_IP_CONNECTIONS], [SESSION_REUSE_SESSIONS], [SESSION_MAX_SESSIONS_PER_DAY], [SESSION_MAX_SESSIONS_PER_USER], [SYSTEM_NO_NEW_SESSIONS], [SYSTEM_DOWN], [ESTADO_CONFIGURACION], [ESTADO]) VALUES (N'1', N'1', N'0', N'0', N'0', N'0', N'0', N'0', N'0', N'1', N'1')
GO
GO
SET IDENTITY_INSERT [APLICACION_CONFIGURACION] OFF
GO
COMMIT TRANSACTION
GO

-- ----------------------------
-- Table structure for APLICACIONES
-- ----------------------------
DROP TABLE [APLICACIONES]
GO
CREATE TABLE [APLICACIONES] (
[ID_APLICACION] int NOT NULL IDENTITY(1,1) ,
[NOMBRE_APLICACION] varchar(200) NOT NULL ,
[LLAVE_APLICACION] varchar(100) NOT NULL ,
[SECRETO_APLICACION] varchar(100) NOT NULL ,
[URL_IMAGEN_APLICACION] varchar(200) NULL ,
[ESTADO_APLICACION] tinyint NOT NULL DEFAULT ((1)) ,
[ESTADO] tinyint NOT NULL DEFAULT ((1)) 
)


GO

-- ----------------------------
-- Records of APLICACIONES
-- ----------------------------
BEGIN TRANSACTION
GO
SET IDENTITY_INSERT [APLICACIONES] ON
GO
INSERT INTO [APLICACIONES] ([ID_APLICACION], [NOMBRE_APLICACION], [LLAVE_APLICACION], [SECRETO_APLICACION], [URL_IMAGEN_APLICACION], [ESTADO_APLICACION], [ESTADO]) VALUES (N'1', N'Catastro', N'gK7UEjFHpwTgCRkaTcLz', N'AYAPPQnKUYUGxg6YnsRQ6XyrGYY7z9dyBQRTpzKZ', null, N'1', N'1')
GO
GO
SET IDENTITY_INSERT [APLICACIONES] OFF
GO
COMMIT TRANSACTION
GO

-- ----------------------------
-- Table structure for APLICACIONES_ACCIONES
-- ----------------------------
DROP TABLE [APLICACIONES_ACCIONES]
GO
CREATE TABLE [APLICACIONES_ACCIONES] (
[ID_ACCION] int NOT NULL IDENTITY(1,1) ,
[ID_APLICACION] int NOT NULL ,
[LLAVE_ACCION] varchar(50) NOT NULL ,
[NOMBRE_ACCION] varchar(50) NOT NULL ,
[TIPO_ACCION] tinyint NOT NULL DEFAULT ((0)) ,
[DEPENDENCIA_ACCION] int NULL ,
[MODULO_URL_ACCION] varchar(50) NULL ,
[CONTROLADOR_URL_ACCION] varchar(50) NULL ,
[ACCION_URL_ACCION] varchar(50) NULL ,
[ICONO_ACCION] varchar(20) NULL ,
[ESTADO_ACCION] tinyint NOT NULL DEFAULT ((1)) ,
[ESTADO] tinyint NOT NULL DEFAULT ((1)) 
)


GO

-- ----------------------------
-- Records of APLICACIONES_ACCIONES
-- ----------------------------
BEGIN TRANSACTION
GO
SET IDENTITY_INSERT [APLICACIONES_ACCIONES] ON
GO
SET IDENTITY_INSERT [APLICACIONES_ACCIONES] OFF
GO
COMMIT TRANSACTION
GO

-- ----------------------------
-- Table structure for APLICACIONES_ACCIONES_ASIGNACIONES
-- ----------------------------
DROP TABLE [APLICACIONES_ACCIONES_ASIGNACIONES]
GO
CREATE TABLE [APLICACIONES_ACCIONES_ASIGNACIONES] (
[ID_ACCION_ASIGNACION] int NOT NULL IDENTITY(1,1) ,
[ID_ACCION] int NOT NULL ,
[ID_AREA_ROL] int NOT NULL ,
[ESTADO_ACCION_ASIGNACION] tinyint NOT NULL DEFAULT ((1)) ,
[ESTADO] tinyint NOT NULL DEFAULT ((1)) 
)


GO

-- ----------------------------
-- Records of APLICACIONES_ACCIONES_ASIGNACIONES
-- ----------------------------
BEGIN TRANSACTION
GO
SET IDENTITY_INSERT [APLICACIONES_ACCIONES_ASIGNACIONES] ON
GO
SET IDENTITY_INSERT [APLICACIONES_ACCIONES_ASIGNACIONES] OFF
GO
COMMIT TRANSACTION
GO

-- ----------------------------
-- Table structure for APLICACIONES_AREAS
-- ----------------------------
DROP TABLE [APLICACIONES_AREAS]
GO
CREATE TABLE [APLICACIONES_AREAS] (
[ID_AREA] int NOT NULL IDENTITY(1,1) ,
[ID_APLICACION] int NOT NULL ,
[NOMBRE_AREA] varchar(255) NOT NULL ,
[ABREVIATURA_AREA] varchar(100) NULL ,
[ESTADO_AREA] tinyint NOT NULL DEFAULT ((1)) ,
[ESTADO] tinyint NOT NULL DEFAULT ((1)) 
)


GO
DBCC CHECKIDENT(N'[APLICACIONES_AREAS]', RESEED, 2)
GO

-- ----------------------------
-- Records of APLICACIONES_AREAS
-- ----------------------------
BEGIN TRANSACTION
GO
SET IDENTITY_INSERT [APLICACIONES_AREAS] ON
GO
INSERT INTO [APLICACIONES_AREAS] ([ID_AREA], [ID_APLICACION], [NOMBRE_AREA], [ABREVIATURA_AREA], [ESTADO_AREA], [ESTADO]) VALUES (N'2', N'1', N'INFORMÁTICA, MONITOREO Y EVALUACIÓN', N'INFORMÁTICA', N'1', N'1')
GO
GO
SET IDENTITY_INSERT [APLICACIONES_AREAS] OFF
GO
COMMIT TRANSACTION
GO

-- ----------------------------
-- Table structure for APLICACIONES_AREAS_ROLES
-- ----------------------------
DROP TABLE [APLICACIONES_AREAS_ROLES]
GO
CREATE TABLE [APLICACIONES_AREAS_ROLES] (
[ID_AREAL_ROL] int NOT NULL IDENTITY(1,1) ,
[ID_APLICACION] int NOT NULL ,
[ID_AREA] int NOT NULL ,
[ID_ROL] int NULL ,
[ESTADO_AREA_ROL] tinyint NOT NULL DEFAULT ((1)) ,
[ESTADO] tinyint NOT NULL DEFAULT ((1)) 
)


GO
DBCC CHECKIDENT(N'[APLICACIONES_AREAS_ROLES]', RESEED, 2)
GO

-- ----------------------------
-- Records of APLICACIONES_AREAS_ROLES
-- ----------------------------
BEGIN TRANSACTION
GO
SET IDENTITY_INSERT [APLICACIONES_AREAS_ROLES] ON
GO
INSERT INTO [APLICACIONES_AREAS_ROLES] ([ID_AREAL_ROL], [ID_APLICACION], [ID_AREA], [ID_ROL], [ESTADO_AREA_ROL], [ESTADO]) VALUES (N'2', N'1', N'2', N'1', N'1', N'1')
GO
GO
SET IDENTITY_INSERT [APLICACIONES_AREAS_ROLES] OFF
GO
COMMIT TRANSACTION
GO

-- ----------------------------
-- Table structure for APLICACIONES_ROLES
-- ----------------------------
DROP TABLE [APLICACIONES_ROLES]
GO
CREATE TABLE [APLICACIONES_ROLES] (
[ID_ROL] int NOT NULL IDENTITY(1,1) ,
[ID_APLICACION] int NOT NULL ,
[NOMBRE_ROL] varchar(255) NULL ,
[ESTADO_ROL] tinyint NOT NULL DEFAULT ((1)) ,
[ESTADO] tinyint NOT NULL DEFAULT ((1)) 
)


GO

-- ----------------------------
-- Records of APLICACIONES_ROLES
-- ----------------------------
BEGIN TRANSACTION
GO
SET IDENTITY_INSERT [APLICACIONES_ROLES] ON
GO
INSERT INTO [APLICACIONES_ROLES] ([ID_ROL], [ID_APLICACION], [NOMBRE_ROL], [ESTADO_ROL], [ESTADO]) VALUES (N'1', N'1', N'ADMINISTRADOR DEL SISTEMA', N'1', N'1')
GO
GO
SET IDENTITY_INSERT [APLICACIONES_ROLES] OFF
GO
COMMIT TRANSACTION
GO

-- ----------------------------
-- Table structure for APLICACIONES_USUARIOS
-- ----------------------------
DROP TABLE [APLICACIONES_USUARIOS]
GO
CREATE TABLE [APLICACIONES_USUARIOS] (
[ID_APLICACION_USUARIO] int NOT NULL IDENTITY(1,1) ,
[ID_APLICACION] int NOT NULL ,
[ID_USUARIO] int NOT NULL ,
[FECHA_REGISTRO] date NULL ,
[ESTADO_APLICACION_USUARIO] tinyint NOT NULL DEFAULT ((1)) ,
[ESTADO] tinyint NOT NULL DEFAULT ((1)) 
)


GO
DBCC CHECKIDENT(N'[APLICACIONES_USUARIOS]', RESEED, 3)
GO

-- ----------------------------
-- Records of APLICACIONES_USUARIOS
-- ----------------------------
BEGIN TRANSACTION
GO
SET IDENTITY_INSERT [APLICACIONES_USUARIOS] ON
GO
INSERT INTO [APLICACIONES_USUARIOS] ([ID_APLICACION_USUARIO], [ID_APLICACION], [ID_USUARIO], [FECHA_REGISTRO], [ESTADO_APLICACION_USUARIO], [ESTADO]) VALUES (N'2', N'1', N'1', N'2016-09-05', N'1', N'1'), (N'3', N'1', N'2', N'2016-09-06', N'1', N'1')
GO
GO
SET IDENTITY_INSERT [APLICACIONES_USUARIOS] OFF
GO
COMMIT TRANSACTION
GO

-- ----------------------------
-- Table structure for ERRORES
-- ----------------------------
DROP TABLE [ERRORES]
GO
CREATE TABLE [ERRORES] (
[ID_ERROR] int NOT NULL IDENTITY(1,1) ,
[CODIGO_ERROR] int NOT NULL ,
[NOMBRE_ERROR] varchar(100) NULL ,
[DESCRIPCION_ERROR] varbinary(200) NULL ,
[ESTADO_ERROR] tinyint NOT NULL DEFAULT ((1)) ,
[ESTADO] tinyint NOT NULL DEFAULT ((1)) 
)


GO
DBCC CHECKIDENT(N'[ERRORES]', RESEED, 6)
GO

-- ----------------------------
-- Records of ERRORES
-- ----------------------------
BEGIN TRANSACTION
GO
SET IDENTITY_INSERT [ERRORES] ON
GO
INSERT INTO [ERRORES] ([ID_ERROR], [CODIGO_ERROR], [NOMBRE_ERROR], [DESCRIPCION_ERROR], [ESTADO_ERROR], [ESTADO]) VALUES (N'1', N'900', N'APLICACION NO EXISTE', null, N'1', N'1'), (N'2', N'901', N'APLICACION SIN CONFIGURACION', null, N'1', N'1'), (N'3', N'902', N'APLICACION EN MANTENIMIENTO', null, N'1', N'1'), (N'4', N'903', N'APLICACION EN MODO PRIVADO', null, N'1', N'1'), (N'5', N'904', N'USUARIO O CONTRASEÑA INCORRECTOS', null, N'1', N'1'), (N'6', N'905', N'USUARIO SIN ACCESOS', null, N'1', N'1')
GO
GO
SET IDENTITY_INSERT [ERRORES] OFF
GO
COMMIT TRANSACTION
GO

-- ----------------------------
-- Table structure for PERSONAS
-- ----------------------------
DROP TABLE [PERSONAS]
GO
CREATE TABLE [PERSONAS] (
[ID_PERSONA] int NOT NULL IDENTITY(1,1) ,
[NOMBRES_PERSONA] varchar(100) NOT NULL ,
[APELLIDO_PATERNO_PERSONA] varchar(100) NOT NULL ,
[APELLIDO_MATERNO_PERSONA] varchar(100) NOT NULL ,
[ID_TIPO_GENERO] int NULL ,
[FECHA_NACIMIENTO_PERSONA] date NULL ,
[ID_TIPO_DOCUMENTO] int NULL ,
[NUMERO_DOCUMENTO_PERSONA] varchar(50) NULL ,
[ESTADO_PERSONA] tinyint NOT NULL DEFAULT ((1)) ,
[ESTADO] tinyint NOT NULL DEFAULT ((1)) 
)


GO
DBCC CHECKIDENT(N'[PERSONAS]', RESEED, 2)
GO

-- ----------------------------
-- Records of PERSONAS
-- ----------------------------
BEGIN TRANSACTION
GO
SET IDENTITY_INSERT [PERSONAS] ON
GO
INSERT INTO [PERSONAS] ([ID_PERSONA], [NOMBRES_PERSONA], [APELLIDO_PATERNO_PERSONA], [APELLIDO_MATERNO_PERSONA], [ID_TIPO_GENERO], [FECHA_NACIMIENTO_PERSONA], [ID_TIPO_DOCUMENTO], [NUMERO_DOCUMENTO_PERSONA], [ESTADO_PERSONA], [ESTADO]) VALUES (N'1', N'JOSE NOLBERTO', N'VILCHEZ', N'MORENO', N'1', N'1992-02-03', N'1', N'46755178', N'1', N'1'), (N'2', N'USUARIO', N'ADMINISTRADOR', N'DE PRUEBA', N'1', N'1992-02-03', N'1', N'99999999', N'1', N'1')
GO
GO
SET IDENTITY_INSERT [PERSONAS] OFF
GO
COMMIT TRANSACTION
GO

-- ----------------------------
-- Table structure for TIPO_DOCUMENTO_IDENTIDAD
-- ----------------------------
DROP TABLE [TIPO_DOCUMENTO_IDENTIDAD]
GO
CREATE TABLE [TIPO_DOCUMENTO_IDENTIDAD] (
[ID_TIPO_DOCUMENTO] int NOT NULL IDENTITY(1,1) ,
[TIPO_DOCUMENTO] varchar(100) NOT NULL ,
[ABREVIATURA_TIPO_DOCUMENTO] varchar(50) NULL ,
[ESTADO_TIPO_DOCUMENTO] tinyint NOT NULL DEFAULT ((1)) ,
[ESTADO] tinyint NOT NULL DEFAULT ((1)) 
)


GO

-- ----------------------------
-- Records of TIPO_DOCUMENTO_IDENTIDAD
-- ----------------------------
BEGIN TRANSACTION
GO
SET IDENTITY_INSERT [TIPO_DOCUMENTO_IDENTIDAD] ON
GO
INSERT INTO [TIPO_DOCUMENTO_IDENTIDAD] ([ID_TIPO_DOCUMENTO], [TIPO_DOCUMENTO], [ABREVIATURA_TIPO_DOCUMENTO], [ESTADO_TIPO_DOCUMENTO], [ESTADO]) VALUES (N'1', N'DOCUMENTO DE IDENTIDAD', N'DNI', N'1', N'1')
GO
GO
SET IDENTITY_INSERT [TIPO_DOCUMENTO_IDENTIDAD] OFF
GO
COMMIT TRANSACTION
GO

-- ----------------------------
-- Table structure for TIPO_GENERO
-- ----------------------------
DROP TABLE [TIPO_GENERO]
GO
CREATE TABLE [TIPO_GENERO] (
[ID_TIPO_GENERO] int NOT NULL IDENTITY(1,1) ,
[TIPO_GENERO] varchar(100) NOT NULL ,
[DESCRIPCION_TIPO_GENERO] varchar(255) NULL ,
[ESTADO_TIPO_GENERO] tinyint NOT NULL DEFAULT ((1)) ,
[ESTADO] tinyint NOT NULL DEFAULT ((1)) 
)


GO
DBCC CHECKIDENT(N'[TIPO_GENERO]', RESEED, 2)
GO

-- ----------------------------
-- Records of TIPO_GENERO
-- ----------------------------
BEGIN TRANSACTION
GO
SET IDENTITY_INSERT [TIPO_GENERO] ON
GO
INSERT INTO [TIPO_GENERO] ([ID_TIPO_GENERO], [TIPO_GENERO], [DESCRIPCION_TIPO_GENERO], [ESTADO_TIPO_GENERO], [ESTADO]) VALUES (N'1', N'HOMBRE', null, N'1', N'1'), (N'2', N'MUJERE', null, N'1', N'1')
GO
GO
SET IDENTITY_INSERT [TIPO_GENERO] OFF
GO
COMMIT TRANSACTION
GO

-- ----------------------------
-- Table structure for USUARIOS
-- ----------------------------
DROP TABLE [USUARIOS]
GO
CREATE TABLE [USUARIOS] (
[ID_USUARIO] int NOT NULL IDENTITY(1,1) ,
[ID_PERSONA] int NOT NULL ,
[NOMBRE_USUARIO] varchar(20) NOT NULL ,
[CONTRASENIA_USUARIO] varchar(100) NULL ,
[CORREO_USUARIO] varchar(50) NULL ,
[ESTADO_USUARIO] tinyint NOT NULL DEFAULT ((1)) ,
[ESTADO] tinyint NOT NULL DEFAULT ((1)) 
)


GO
DBCC CHECKIDENT(N'[USUARIOS]', RESEED, 2)
GO

-- ----------------------------
-- Records of USUARIOS
-- ----------------------------
BEGIN TRANSACTION
GO
SET IDENTITY_INSERT [USUARIOS] ON
GO
INSERT INTO [USUARIOS] ([ID_USUARIO], [ID_PERSONA], [NOMBRE_USUARIO], [CONTRASENIA_USUARIO], [CORREO_USUARIO], [ESTADO_USUARIO], [ESTADO]) VALUES (N'1', N'1', N'jnolbertovm', N'KXYHEhB/WA8=', N'jnolbertovm@gmail.com', N'1', N'1'), (N'2', N'2', N'admin', N'KXYHEhB/WA8=', N'admin@admin.com', N'1', N'1')
GO
GO
SET IDENTITY_INSERT [USUARIOS] OFF
GO
COMMIT TRANSACTION
GO

-- ----------------------------
-- Table structure for USUARIOS_ACCESOS
-- ----------------------------
DROP TABLE [USUARIOS_ACCESOS]
GO
CREATE TABLE [USUARIOS_ACCESOS] (
[ID_USUARIO_ACCESO] int NOT NULL IDENTITY(1,1) ,
[ID_APLICACION_USUARIO] int NOT NULL ,
[ID_AREA_ROL] int NOT NULL ,
[DESDE_USUARIO_ACCESO] date NOT NULL ,
[HASTA_USUARIO_ACCESO] date NULL ,
[ESTADO_USUARIO_ACCESO] tinyint NOT NULL DEFAULT ((1)) ,
[ESTADO] tinyint NOT NULL DEFAULT ((1)) 
)


GO
DBCC CHECKIDENT(N'[USUARIOS_ACCESOS]', RESEED, 2)
GO

-- ----------------------------
-- Records of USUARIOS_ACCESOS
-- ----------------------------
BEGIN TRANSACTION
GO
SET IDENTITY_INSERT [USUARIOS_ACCESOS] ON
GO
INSERT INTO [USUARIOS_ACCESOS] ([ID_USUARIO_ACCESO], [ID_APLICACION_USUARIO], [ID_AREA_ROL], [DESDE_USUARIO_ACCESO], [HASTA_USUARIO_ACCESO], [ESTADO_USUARIO_ACCESO], [ESTADO]) VALUES (N'1', N'2', N'2', N'2016-09-05', null, N'1', N'1'), (N'2', N'3', N'2', N'2016-09-06', null, N'1', N'1')
GO
GO
SET IDENTITY_INSERT [USUARIOS_ACCESOS] OFF
GO
COMMIT TRANSACTION
GO

-- ----------------------------
-- View structure for VW_AREAS_ROLES_APLICACION
-- ----------------------------
DROP VIEW [VW_AREAS_ROLES_APLICACION]
GO
CREATE VIEW [VW_AREAS_ROLES_APLICACION] AS 
SELECT
	A.ID_AREA
	,A.NOMBRE_AREA
	,A.ABREVIATURA_AREA
	,R.ID_ROL
	,R.NOMBRE_ROL 
	,AR.ID_AREAL_ROL
	,AR.ID_APLICACION
FROM APLICACIONES_AREAS_ROLES AR
INNER JOIN APLICACIONES_AREAS A ON (
	A.ID_AREA = AR.ID_AREA
	AND A.ID_APLICACION = AR.ID_APLICACION
	AND A.ESTADO = 1
	AND A.ESTADO_AREA = 1
)
INNER JOIN APLICACIONES_ROLES R ON (
	R.ID_ROL = AR.ID_ROL
	AND R.ID_APLICACION = AR.ID_APLICACION
	AND R.ESTADO = 1
	AND R.ESTADO_ROL = 1
)
WHERE AR.ESTADO =1
AND AR.ESTADO_AREA_ROL = 1
GO

-- ----------------------------
-- View structure for VW_PERSONAS
-- ----------------------------
DROP VIEW [VW_PERSONAS]
GO
CREATE VIEW [VW_PERSONAS] AS 
SELECT
	P.ID_PERSONA
	,P.NOMBRES_PERSONA
	,P.APELLIDO_PATERNO_PERSONA
	,P.APELLIDO_MATERNO_PERSONA
	,P.FECHA_NACIMIENTO_PERSONA
	,TG.TIPO_GENERO
	,TDI.TIPO_DOCUMENTO
	,TDI.ABREVIATURA_TIPO_DOCUMENTO
	,P.NUMERO_DOCUMENTO_PERSONA
FROM PERSONAS P
INNER JOIN TIPO_GENERO TG ON(
	TG.ID_TIPO_GENERO = P.ID_TIPO_GENERO
	AND TG.ESTADO = 1
	AND TG.ESTADO_TIPO_GENERO = 1
)
INNER JOIN TIPO_DOCUMENTO_IDENTIDAD TDI ON(
	TDI.ID_TIPO_DOCUMENTO = P.ID_TIPO_DOCUMENTO
	AND TDI.ESTADO = 1
	AND TDI.ESTADO_TIPO_DOCUMENTO = 1
)
WHERE P.ESTADO = 1
AND P.ESTADO_PERSONA = 1
GO

-- ----------------------------
-- View structure for VW_USUARIO_ACCESO_APLICACION
-- ----------------------------
DROP VIEW [VW_USUARIO_ACCESO_APLICACION]
GO
CREATE VIEW [VW_USUARIO_ACCESO_APLICACION] AS 
SELECT 
	UA.ID_USUARIO_ACCESO
	,UA.ID_APLICACION_USUARIO
	,UA.DESDE_USUARIO_ACCESO
	,UA.HASTA_USUARIO_ACCESO
	,AR.ID_AREA
	,AR.NOMBRE_AREA
	,AR.ABREVIATURA_AREA
	,AR.ID_ROL
	,AR.NOMBRE_ROL 
	,AR.ID_AREAL_ROL
	,AR.ID_APLICACION
FROM USUARIOS_ACCESOS UA
INNER JOIN VW_AREAS_ROLES_APLICACION AR ON (
	AR.ID_AREAL_ROL = UA.ID_AREA_ROL
)
WHERE UA.ESTADO = 1
AND UA.ESTADO_USUARIO_ACCESO = 1
GO

-- ----------------------------
-- View structure for VW_USUARIOS
-- ----------------------------
DROP VIEW [VW_USUARIOS]
GO
CREATE VIEW [VW_USUARIOS] AS 
SELECT
	U.ID_USUARIO
	,U.NOMBRE_USUARIO
	,U.CONTRASENIA_USUARIO
	,U.ESTADO_USUARIO
	,U.CORREO_USUARIO
	,P.ID_PERSONA
	,P.NOMBRES_PERSONA
	,P.APELLIDO_PATERNO_PERSONA
	,P.APELLIDO_MATERNO_PERSONA
	,P.FECHA_NACIMIENTO_PERSONA
	,P.TIPO_GENERO
	,P.TIPO_DOCUMENTO
	,P.ABREVIATURA_TIPO_DOCUMENTO
	,P.NUMERO_DOCUMENTO_PERSONA
FROM USUARIOS U 
INNER JOIN VW_PERSONAS P ON (
	P.ID_PERSONA = U.ID_PERSONA
)
WHERE U.ESTADO = 1
AND U.ESTADO_USUARIO = 1
GO

-- ----------------------------
-- View structure for VW_USUARIOS_APLICACION
-- ----------------------------
DROP VIEW [VW_USUARIOS_APLICACION]
GO
CREATE VIEW [VW_USUARIOS_APLICACION] AS 
SELECT 
	AU.ID_APLICACION_USUARIO
	,AU.ID_APLICACION
	,AU.FECHA_REGISTRO
	,AU.ESTADO_APLICACION_USUARIO
	,U.ID_USUARIO
	,U.NOMBRE_USUARIO
	,U.CONTRASENIA_USUARIO
	,U.ESTADO_USUARIO
	,U.CORREO_USUARIO
	,U.ID_PERSONA
	,U.NOMBRES_PERSONA
	,U.APELLIDO_PATERNO_PERSONA
	,U.APELLIDO_MATERNO_PERSONA
	,U.FECHA_NACIMIENTO_PERSONA
	,U.TIPO_GENERO
	,U.TIPO_DOCUMENTO
	,U.ABREVIATURA_TIPO_DOCUMENTO
	,U.NUMERO_DOCUMENTO_PERSONA
FROM APLICACIONES_USUARIOS AU 
INNER JOIN VW_USUARIOS U ON (
	U.ID_USUARIO = AU.ID_USUARIO
)
WHERE AU.ESTADO = 1
GO

-- ----------------------------
-- Procedure structure for SP_VALIDAR_ACCESO_APLICACION
-- ----------------------------
DROP PROCEDURE [SP_VALIDAR_ACCESO_APLICACION]
GO

CREATE PROCEDURE [SP_VALIDAR_ACCESO_APLICACION](
	@LLAVE 		VARCHAR(100),
	@SECRETO 	VARCHAR(100)
)
AS
SET NOCOUNT ON;
BEGIN

	-- =============================================
	-- Author:		Nolberto Vilchez
	-- Create date: 01-09-2016
	-- Description:	Validación de la aplicación
	-- =============================================

	DECLARE @ID_APLICACION	INT; -- VARIABLE PARA EL CÓDIGO ÚNICO DE LA APLICACIÓN
	DECLARE @CODIGO_ERROR	INT; -- VARIABLE PARA EL CÓDIGO DE ERROR;

	-- SI LA LLAVE Y EL CÓDIGO SECRETO PERTENECE A UNA APLICACIÓN REGISTRADA
	IF EXISTS(SELECT ID_APLICACION FROM APLICACIONES WHERE LLAVE_APLICACION = @LLAVE AND SECRETO_APLICACION = @SECRETO AND ESTADO = 1 AND ESTADO_APLICACION = 1)
		BEGIN
			DECLARE @SYSTEM_DOWN INT; -- VARIABLE PARA EL CONTROL DE ACCESO AL SISTEMA
			-- CAPTURAMOS EL CÓDIGO DE LA APLICACIÓN 
			SET @ID_APLICACION = (SELECT ID_APLICACION FROM APLICACIONES WHERE LLAVE_APLICACION = @LLAVE AND SECRETO_APLICACION = @SECRETO AND ESTADO = 1 AND ESTADO_APLICACION = 1);
			-- SI LA APLICACIÓN REGISTRA CUENTA CON CONFIGURACIÓN 
			IF EXISTS(SELECT ID_CONFIGURACION FROM APLICACION_CONFIGURACION WHERE ID_APLICACION = @ID_APLICACION AND ESTADO = 1 AND ESTADO_CONFIGURACION = 1)
				BEGIN
					-- CAPTURAMOS EL ESTADO DE ACCESO A LA APLICACION (0 = ACCESIBLE, 1 = INACSESIBLE)
					SET @SYSTEM_DOWN = (SELECT SYSTEM_DOWN FROM APLICACION_CONFIGURACION WHERE ID_APLICACION = @ID_APLICACION AND ESTADO = 1 AND ESTADO_CONFIGURACION = 1);
					-- SI LA APLICACIÓN ES ACCESIBLE
					IF @SYSTEM_DOWN = 0
						BEGIN
							SET @CODIGO_ERROR = 200;
						END
					ELSE
						BEGIN
							SET @CODIGO_ERROR = 902;
						END
					
				END
			ELSE
				BEGIN
					SET @CODIGO_ERROR = 901;
				END
		END
	ELSE
		BEGIN
			SET @CODIGO_ERROR = 900;
		END
	
	IF @CODIGO_ERROR = 200
		BEGIN
			SELECT 200 AS 'CODIGO_ERROR', @ID_APLICACION AS 'ID'
		END
	ELSE
		BEGIN	
			SELECT @ID_APLICACION AS 'ID', CODIGO_ERROR, NOMBRE_ERROR, DESCRIPCION_ERROR FROM ERRORES WHERE CODIGO_ERROR = @CODIGO_ERROR AND ESTADO = 1 AND ESTADO_ERROR = 1
		END
END

GO

-- ----------------------------
-- Procedure structure for SP_VALIDAR_SESSION_APLICACION
-- ----------------------------
DROP PROCEDURE [SP_VALIDAR_SESSION_APLICACION]
GO

CREATE PROCEDURE [SP_VALIDAR_SESSION_APLICACION](
	@ID_APLICACION			INT,
	@NOMBRE_USUARIO			VARCHAR(50),
	@CONTRASENIA_USUARIO	VARCHAR(50)
)
AS
SET NOCOUNT ON;
BEGIN

	-- =============================================
	-- Author:		Nolberto Vilchez
	-- Create date: 01-09-2016
	-- Description:	Validación de la aplicación
	-- =============================================

	DECLARE @SYSTEM_NO_NEW_SESSIONS INT; -- VARIABLE PARA EL CONTROL DE NUEVAS SESIONES A LA APLICAIÓN
	DECLARE @CODIGO_ERROR	INT; -- VARIABLE PARA EL CÓDIGO DE ERROR;

	-- CAPTURAMOS EL ESTADO DE NUEVAS SESIONES PERMITIDAS A LA APLICACIÓN
	SET @SYSTEM_NO_NEW_SESSIONS = (SELECT SYSTEM_NO_NEW_SESSIONS FROM APLICACION_CONFIGURACION WHERE ID_APLICACION = @ID_APLICACION AND ESTADO = 1 AND ESTADO_CONFIGURACION = 1);
	-- SI SE PERMITEN NUEVAS SESIONES A LA APLICACIÓN
	IF @SYSTEM_NO_NEW_SESSIONS = 0
		BEGIN
			DECLARE @ID_APLICACION_USUARIO INT;
			IF EXISTS(SELECT * FROM VW_USUARIOS_APLICACION WHERE ID_APLICACION = @ID_APLICACION AND NOMBRE_USUARIO = @NOMBRE_USUARIO AND CONTRASENIA_USUARIO = @CONTRASENIA_USUARIO)
				BEGIN
					SET @ID_APLICACION_USUARIO = (SELECT ID_APLICACION_USUARIO FROM VW_USUARIOS_APLICACION WHERE ID_APLICACION = @ID_APLICACION AND NOMBRE_USUARIO = @NOMBRE_USUARIO AND CONTRASENIA_USUARIO = @CONTRASENIA_USUARIO);
					IF EXISTS(SELECT ID_APLICACION_USUARIO FROM VW_USUARIO_ACCESO_APLICACION WHERE ID_APLICACION_USUARIO = @ID_APLICACION_USUARIO)
						BEGIN
							SET @CODIGO_ERROR = 200;
						END
					ELSE
						BEGIN
							SET @CODIGO_ERROR = 905;
						END
				END
			ELSE
				BEGIN
					SET @CODIGO_ERROR = 904;
				END	
		END
	ELSE
		BEGIN
			SET @CODIGO_ERROR = 903;
		END

	IF @CODIGO_ERROR = 200
		BEGIN
			SELECT
				200 AS 'CODIGO_ERROR'
				,U.ID_APLICACION_USUARIO
				,U.ID_USUARIO
				,U.ID_PERSONA
				,U.NOMBRES_PERSONA
				,U.APELLIDO_PATERNO_PERSONA
				,U.APELLIDO_MATERNO_PERSONA
				,U.CORREO_USUARIO
				,UA.NOMBRE_AREA
				,UA.NOMBRE_ROL
				,UA.ID_AREAL_ROL
			FROM VW_USUARIOS_APLICACION U
			INNER JOIN VW_USUARIO_ACCESO_APLICACION UA ON(
				UA.ID_APLICACION_USUARIO = U.ID_APLICACION_USUARIO
			) 
			WHERE U.ID_APLICACION = @ID_APLICACION 
			AND U.NOMBRE_USUARIO = @NOMBRE_USUARIO 
			AND U.CONTRASENIA_USUARIO = @CONTRASENIA_USUARIO
		END
	ELSE
		BEGIN	
			SELECT @ID_APLICACION AS 'ID', CODIGO_ERROR, NOMBRE_ERROR, DESCRIPCION_ERROR FROM ERRORES WHERE CODIGO_ERROR = @CODIGO_ERROR AND ESTADO = 1 AND ESTADO_ERROR = 1
		END
						
END

GO

-- ----------------------------
-- Indexes structure for table ADMINISTRADORES
-- ----------------------------

-- ----------------------------
-- Primary Key structure for table ADMINISTRADORES
-- ----------------------------
ALTER TABLE [ADMINISTRADORES] ADD PRIMARY KEY ([ID_ADMINISTRADOR])
GO

-- ----------------------------
-- Indexes structure for table ADMINISTRADORES_APLICACIONES
-- ----------------------------

-- ----------------------------
-- Primary Key structure for table ADMINISTRADORES_APLICACIONES
-- ----------------------------
ALTER TABLE [ADMINISTRADORES_APLICACIONES] ADD PRIMARY KEY ([ID_ADMINISTRADOR_APLICACION])
GO

-- ----------------------------
-- Indexes structure for table APLICACION_CONFIGURACION
-- ----------------------------

-- ----------------------------
-- Primary Key structure for table APLICACION_CONFIGURACION
-- ----------------------------
ALTER TABLE [APLICACION_CONFIGURACION] ADD PRIMARY KEY ([ID_CONFIGURACION])
GO

-- ----------------------------
-- Indexes structure for table APLICACIONES
-- ----------------------------

-- ----------------------------
-- Primary Key structure for table APLICACIONES
-- ----------------------------
ALTER TABLE [APLICACIONES] ADD PRIMARY KEY ([ID_APLICACION])
GO

-- ----------------------------
-- Indexes structure for table APLICACIONES_ACCIONES
-- ----------------------------

-- ----------------------------
-- Primary Key structure for table APLICACIONES_ACCIONES
-- ----------------------------
ALTER TABLE [APLICACIONES_ACCIONES] ADD PRIMARY KEY ([ID_ACCION])
GO

-- ----------------------------
-- Indexes structure for table APLICACIONES_ACCIONES_ASIGNACIONES
-- ----------------------------

-- ----------------------------
-- Primary Key structure for table APLICACIONES_ACCIONES_ASIGNACIONES
-- ----------------------------
ALTER TABLE [APLICACIONES_ACCIONES_ASIGNACIONES] ADD PRIMARY KEY ([ID_ACCION_ASIGNACION])
GO

-- ----------------------------
-- Indexes structure for table APLICACIONES_AREAS
-- ----------------------------

-- ----------------------------
-- Primary Key structure for table APLICACIONES_AREAS
-- ----------------------------
ALTER TABLE [APLICACIONES_AREAS] ADD PRIMARY KEY ([ID_AREA])
GO

-- ----------------------------
-- Indexes structure for table APLICACIONES_AREAS_ROLES
-- ----------------------------

-- ----------------------------
-- Primary Key structure for table APLICACIONES_AREAS_ROLES
-- ----------------------------
ALTER TABLE [APLICACIONES_AREAS_ROLES] ADD PRIMARY KEY ([ID_AREAL_ROL])
GO

-- ----------------------------
-- Indexes structure for table APLICACIONES_ROLES
-- ----------------------------

-- ----------------------------
-- Primary Key structure for table APLICACIONES_ROLES
-- ----------------------------
ALTER TABLE [APLICACIONES_ROLES] ADD PRIMARY KEY ([ID_ROL])
GO

-- ----------------------------
-- Indexes structure for table APLICACIONES_USUARIOS
-- ----------------------------

-- ----------------------------
-- Primary Key structure for table APLICACIONES_USUARIOS
-- ----------------------------
ALTER TABLE [APLICACIONES_USUARIOS] ADD PRIMARY KEY ([ID_APLICACION_USUARIO])
GO

-- ----------------------------
-- Indexes structure for table ERRORES
-- ----------------------------

-- ----------------------------
-- Primary Key structure for table ERRORES
-- ----------------------------
ALTER TABLE [ERRORES] ADD PRIMARY KEY ([ID_ERROR])
GO

-- ----------------------------
-- Indexes structure for table PERSONAS
-- ----------------------------

-- ----------------------------
-- Primary Key structure for table PERSONAS
-- ----------------------------
ALTER TABLE [PERSONAS] ADD PRIMARY KEY ([ID_PERSONA])
GO

-- ----------------------------
-- Indexes structure for table TIPO_DOCUMENTO_IDENTIDAD
-- ----------------------------

-- ----------------------------
-- Primary Key structure for table TIPO_DOCUMENTO_IDENTIDAD
-- ----------------------------
ALTER TABLE [TIPO_DOCUMENTO_IDENTIDAD] ADD PRIMARY KEY ([ID_TIPO_DOCUMENTO])
GO

-- ----------------------------
-- Indexes structure for table TIPO_GENERO
-- ----------------------------

-- ----------------------------
-- Primary Key structure for table TIPO_GENERO
-- ----------------------------
ALTER TABLE [TIPO_GENERO] ADD PRIMARY KEY ([ID_TIPO_GENERO])
GO

-- ----------------------------
-- Indexes structure for table USUARIOS
-- ----------------------------

-- ----------------------------
-- Primary Key structure for table USUARIOS
-- ----------------------------
ALTER TABLE [USUARIOS] ADD PRIMARY KEY ([ID_USUARIO])
GO

-- ----------------------------
-- Indexes structure for table USUARIOS_ACCESOS
-- ----------------------------

-- ----------------------------
-- Primary Key structure for table USUARIOS_ACCESOS
-- ----------------------------
ALTER TABLE [USUARIOS_ACCESOS] ADD PRIMARY KEY ([ID_USUARIO_ACCESO])
GO
