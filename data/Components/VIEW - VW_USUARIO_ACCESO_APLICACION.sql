USE PADLOCK
GO

IF OBJECT_ID('dbo.VW_USUARIO_ACCESO_APLICACION') IS NOT NULL
  DROP VIEW dbo.VW_USUARIO_ACCESO_APLICACION
GO

CREATE VIEW VW_USUARIO_ACCESO_APLICACION AS
SELECT 
	UA.ID_USUARIO_ACCESO
	,UA.ID_APLICACION_USUARIO
	,UA.DESDE_USUARIO_ACCESO
	,UA.HASTA_USUARIO_ACCESO
	,AR.ID_AREA
	,AR.NOMBRE_AREA
	,AR.ABREVIATURA_AREA
	,AR.ID_ROL
	,AR.NOMBRE_ROL 
	,AR.ID_AREAL_ROL
	,AR.ID_APLICACION
FROM USUARIOS_ACCESOS UA
INNER JOIN VW_AREAS_ROLES_APLICACION AR ON (
	AR.ID_AREAL_ROL = UA.ID_AREA_ROL
)
WHERE UA.ESTADO = 1
AND UA.ESTADO_USUARIO_ACCESO = 1