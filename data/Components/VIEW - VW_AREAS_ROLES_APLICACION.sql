USE PADLOCK
GO

IF OBJECT_ID('dbo.VW_AREAS_ROLES_APLICACION') IS NOT NULL
  DROP PROCEDURE dbo.VW_AREAS_ROLES_APLICACION
GO

CREATE VIEW VW_AREAS_ROLES_APLICACION AS
SELECT
	A.ID_AREA
	,A.NOMBRE_AREA
	,A.ABREVIATURA_AREA
	,R.ID_ROL
	,R.NOMBRE_ROL 
	,AR.ID_AREAL_ROL
	,AR.ID_APLICACION
FROM APLICACIONES_AREAS_ROLES AR
INNER JOIN APLICACIONES_AREAS A ON (
	A.ID_AREA = AR.ID_AREA
	AND A.ID_APLICACION = AR.ID_APLICACION
	AND A.ESTADO = 1
	AND A.ESTADO_AREA = 1
)
INNER JOIN APLICACIONES_ROLES R ON (
	R.ID_ROL = AR.ID_ROL
	AND R.ID_APLICACION = AR.ID_APLICACION
	AND R.ESTADO = 1
	AND R.ESTADO_ROL = 1
)
WHERE AR.ESTADO =1
AND AR.ESTADO_AREA_ROL = 1