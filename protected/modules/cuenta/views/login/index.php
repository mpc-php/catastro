<?php
$errors = yii::app()->user->getFlash("errorLogin");

$form = $this->beginWidget('CActiveForm', array(
    'action'      => Yii::app()->createUrl("login/validar"),
    'id'          => 'formLogin',
    'htmlOptions' => array(
        'role'  => 'form',
        'class' => 'login-form'
    )
        ));
?>
<div class="color-app"></div>
<h3 class="form-title font-red">Iniciar Sesión</h3>
<div class="alert alert-danger <?= (!empty($errors)) ? "" : "display-hide" ?>">
    <button class="close" data-close="alert"></button>
    <span> El nombre de Usuario o la Contraseña son incorrectos. </span>
</div>
<div class="form-group">
    <label class="control-label visible-ie8 visible-ie9">Username</label>
    <?= $form->textField($model, 'username', array("class" => "form-control form-control-solid placeholder-no-fix", "placeholder" => "Ingresa tu nombre de usuario", "autofocus" => true, "autocomplete" => 'off')); ?>
    <!--<input class="" type="text" autocomplete="off" placeholder="Username" name="username" /> </div>-->
</div>
<div class="form-group">
    <label class="control-label visible-ie8 visible-ie9">Password</label>
    <?= $form->passwordField($model, 'password', array("class" => "form-control form-control-solid placeholder-no-fix", "placeholder" => "Ingresa tu contraseña")); ?>
    <!--<input class="form-control form-control-solid placeholder-no-fix" type="password" autocomplete="off" placeholder="Password" name="password" /> </div>-->
</div>
<div class="form-actions text-right">
    <button type="submit" class="btn red uppercase">Login</button>
    <!--    <a href="javascript:;" id="forget-password" class="forget-password">Forgot Password?</a>-->
</div>
<?php $this->endWidget(); ?>