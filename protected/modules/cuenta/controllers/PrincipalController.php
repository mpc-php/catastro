

<?php
/**
* Descripcción para Principal
*
* @author Nombre del Programador <correodelprogramador@email.com>
* @package Headaches\Modules\Cuenta\Controllers
*/

class PrincipalController extends Auth
{
	public function actionIndex()
	{
		$this->render('index');
	}
}