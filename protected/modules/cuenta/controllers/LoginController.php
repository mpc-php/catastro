<?php

class LoginController extends Auth {

    public $layout = '//layouts/login';

    public function actionIndex() {
        $model = new ModelLogin;
        $this->render('index', [
            "model" => $model
        ]);
    }

    public function actionValidar() {
        if (Yii::app()->request->getPost("ModelLogin")) {
            $model             = new ModelLogin;
            $post              = Yii::app()->request->getPost("ModelLogin");
            $post["password"]  = TripleDes::Encrypt($post["password"]);
            $model->attributes = $post;

            if ($model->validate() && $model->login()) {
                $this->redirect(Yii::app()->user->returnUrl);
            } else {
                yii::app()->user->setFlash("errorLogin", $model->errors);
                $this->redirect(["/login"]);
            }
        } else {
            throw new CHttpException(404, "Página No Encontrada");
        }
    }

    public function actionLogout() {
        yii::app()->user->logout();
        $this->redirect(["/login"]);
    }

    public function beforeAction($action) {
        if (!yii::app()->user->isGuest && ($this->action->id !== "logout")) {
            $this->redirect(["/"]);
        }
        return parent::beforeAction($action);
    }

}
