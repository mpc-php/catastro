<?php

/**
 * 
 */
class ULogin extends CUserIdentity {

    private $_id;

    public function authenticate() {
        $padlock = Yii::app()->padlock->validarSession($this->username, $this->password);
        $procesos = QUsuario::obtenerDatosDeAniosDelProceso();

        if (!$padlock) {
            $this->errorCode = self::ERROR_USERNAME_INVALID;
        } else {
            $this->errorCode = self::ERROR_NONE;
            $id_persona_catas = $this->getDigitadorId($padlock["ID_PERSONA"]);

            $this->_id = $padlock["ID_USUARIO"];
            $this->setState("id_persona", $padlock["ID_PERSONA"]);
            $this->setState("id_persona_catas", $id_persona_catas);
            $this->setState("nombres", $padlock["NOMBRES_PERSONA"]);
            $this->setState("apellido_paterno", $padlock["APELLIDO_PATERNO_PERSONA"]);
            $this->setState("apellido_materno", $padlock["APELLIDO_MATERNO_PERSONA"]);
            $this->setState("proceso", $procesos);
        }
        return $this->errorCode;
    }

    public function getId() {
        return $this->_id;
    }

    public function getDigitadorId($id_persona) {

        $command = Yii::app()->db->createCommand("SELECT ID_USUARIO FROM USUARIO_RELACION WHERE ID_PERSONA = :ID");
        $command->bindParam(":ID", $id_persona, PDO::PARAM_INT);
        $datos = $command->queryRow();

        if (!isset($datos['ID_USUARIO'])) {
            return 0;
        }
        return $datos['ID_USUARIO'];
    }

}
