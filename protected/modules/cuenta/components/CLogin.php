<?php

class CLogin {

    const SIN_ERRORES                    = 0;
    const ERROR_CREDENCIALES_INCORRECTAS = 1;
    const ERROR_CUENTA_DESACTIVADA       = 2;

}
