<?php

class QUsuario {

    public static function obtenerDatosPorUsuarioYContrasenia($nombre_usuario, $contrasenia_usuario) {
        Criteria::set([
            "nombre_usuario"      => $nombre_usuario,
            "contrasenia_usuario" => $contrasenia_usuario,
            "estado_usuario"      => Constante::ESTADO_USUARIO_ACTIVO,
            "estado"              => Constante::ACTIVO
        ]);

        return ModelUsuarios::model()->find(Criteria::get());
    }

    public static function obtenerDatosDePersonaPorIdUsuario($idUsuario) {
        $sql     = "SELECT * FROM vw_usuarios_datos_persona WHERE id_usuario = :idUsuario";
        $command = Yii::app()->db->createCommand($sql);
        $command->bindParam(":idUsuario", $idUsuario, PDO::PARAM_INT);
        $datos   = $command->queryRow();

        return $datos;
    }

    public static function obtenerDatosDeAccesoPorIdUsuario($idUsuario) {
        $sql     = "SELECT * FROM vw_usuarios_accesos WHERE id_usuario = :idUsuario";
        $command = Yii::app()->db->createCommand($sql);
        $command->bindParam(":idUsuario", $idUsuario, PDO::PARAM_INT);
        $datos   = $command->queryRow();

        return $datos;
    }

    public static function obtenerDatosDeAniosDelProceso() 
    {
        $strShuffle = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
        $command = Yii::app()->db->createCommand('EXEC sp_AnioProceso');
        $table   = $command->queryAll();
        $ntable = ['items' => [], 'selected' => ''];

        foreach ($table as $row => $cell) {
            $key = substr(str_shuffle($strShuffle), 0, 6);
            $ntable['items'][$key] = $cell['id_catastro'];
        }

        $ntable['selected'] = end($ntable['items']);

        return $ntable;
    }

}
