<?php

class UUsuario {

    public static function validarCredenciales($login) {
        return QUsuario::obtenerDatosPorUsuarioYContrasenia($login->username, $login->password);
    }

    public static function configurarDatosBasicos($usuario) {
        return QUsuario::obtenerDatosDePersonaPorIdUsuario($usuario->id_usuario);
    }

    public static function configurarAcceso($usuario) {
        return QUsuario::obtenerDatosDeAccesoPorIdUsuario($usuario->id_usuario);
    }

}
