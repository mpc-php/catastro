/**
 * Configuración personalizada para la carga de librerias del módulo
 * 
 * @type Object
 */
var Builder = {
    module: {
        controllers: {
            login: {
                actions: {
                    index: {
                        js: [{login: ["_PATH_:validate"]}]
                    }
                },
                //Assets para un controllador especifico y todas sus acciones
                js: [],
                css: {
                    libs: [],
                    package: [],
                    custom: []
                }
            }
        },
        //Assets para todo el módulo, todos sus controlladores y todas sus acciones
        js: [],
        css: {
            libs: [],
            package: [],
            custom: []
        }
    }
};
