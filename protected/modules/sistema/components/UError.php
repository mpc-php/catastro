<?php

/**
 * UError es una clase creada para...
 *
 * @author Nolberto Vilchez Moreno <jnolbertovm@gmail.com>
 * @package Siat/
 * @since 29-feb-2016
 */
class UError {

    public static $buscar    = [
        "C:/xampp/htdocs/core/framework/web/",
        "C:/xampp/htdocs/esqueleto/",
        "/protected/",
        "/extensions/",
        "/modules/",
        "/components/",
        "/config/",
    ];
    public static $remplazar = [
        "framework/",
        "project/",
        "/core/",
        "/ext/",
        "/pkg/",
        "/cpns/",
        "/C/",
        "/V/",
    ];

    public static function urlFile($urlFIle) {
        $urlFIle = str_replace("\\", "/", $urlFIle);
        return str_replace(self::$buscar, self::$remplazar, $urlFIle);
    }

}
