<?php

class ErrorController extends Auth {

    public $defaultAction = 'show';

    public function actionShow() {
        if ($error = Yii::app()->errorHandler->error) {
            $this->pageTitle = $error["code"];
            Utils::show($error, true);
            $this->render("index", [
                'error' => $error
            ]);
        } else {
            $this->redirect(["/"]);
        }
    }

}
