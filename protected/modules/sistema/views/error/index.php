<div class="container-xs-height full-height m-t-100">
    <div class="row-xs-height">
        <div class="col-xs-height col-middle p-t-100">
            <div class="error-container text-center">
                <h1 class="error-number"><?= $error["code"] ?></h1>
                <h2 class="semi-bold"><?= $error["message"] ?></h2>
                <p>Es posible que el enlace al cual has ingresado esté roto o que se haya eliminado la página.</p>
                <p>
                    <code class="code">File: <?= UError::urlFile($error['file']) ?> - Line <?= $error['line'] ?></code>
                </p>
            </div>
        </div>
    </div>
</div>