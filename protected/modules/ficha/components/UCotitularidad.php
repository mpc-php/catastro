<?php 
/**
 * Clase que se encarga de manejar las cookies creadas para la ficha de cotitularidad
 * 
 * @author José Alejandro Vilchez Moreno <joalvm@gmail.com>
 * @package Catastro\modules\ficha\components
 */
class UCotitularidad {

	/**
	 * Genera la cookie con el objeto pasado por parametro.
	 *
	 * La funcion reconoce el objecto y lo convierte a un string para la cookie
	 * luego de eso reemplaza a la antigua con los nuevos datos.
	 * @param  string $name  Nombre de la cookie.
	 * @param  array/object  $value Datos que guardará la cookie.
	 * @return void
	 */
	public static function generateCookie($name, $value = [])
	{
		$Encrypt 	= new Encrypt;
		$nvalue;

		if ( is_array($value) OR is_object($value) ) {
			$nvalue = json_encode($value);
		}

		$nvalue = $Encrypt->encode($nvalue, CCotitularidad::KEY_ENCRYPT);

		$cookie = new CHttpCookie($name, $nvalue);

		$cookie->expire = time() + CCotitularidad::COOKIE_EXPIRE;
		$cookie->httpOnly = CCotitularidad::COOKIE_HTTPONLY;

		Yii::app()->request->cookies[$name] = $cookie;
	}

	/**
	 * Obtiene la información de una cookie formateando su valor como objeto para su facil manipulación
	 * @param  string $name Nombre de la cookie.
	 * @return object       Dato formateado de la cookie
	 */
	public static function getCookie($name) 
	{
		$Encrypt 	= new Encrypt;
		$obj 			= new stdClass;

		$value = Yii::app()->request->cookies->contains($name) ? Yii::app()->request->cookies[$name]->value : NULL;

		if ( $value != NULL )
			$obj = json_decode( $Encrypt->decode($value, CCotitularidad::KEY_ENCRYPT) );

		return $obj;
	}

	/**
	 * Crea o actualiza un dato detro de la cookie
	 * @param string $key   key del objeto
	 * @param string $value dato que será asignado
	 */
	public static function setCookie($key = '', $value = '')
	{
		$Encrypt 	= new Encrypt;
		$obj 			= (object)self::getCookieKey($key);

		if ( ! empty((array)$obj) )
		{
			$cookieObj = self::getCookie($obj->nameCookie);
			$cookieObj->{$key} = $value;

			self::generateCookie($obj->nameCookie, $cookieObj);
		}
	}

	/**
	 * Obtiene el valor que se encuentra dentro una cookie
	 * @param  string $name Nombre de la cookie
	 * @param  string $key  Clave que esta dentro de la cookie y que contiene el valor
	 * @return string       valor de la clave que esta dentro de la cookie
	 */
	public static function getCookieValue($name, $key)
	{
		$obj = self::getCookie($name);

		if ( isset($obj->{$key}) ) {
			return $obj->{$key};
		}

		return NULL;
	}

	/**
	 * Obtiene el nombre de la cookie a la que pertenece una clave en especifico.
	 *
	 * La funcion busca entre una lista que guarda la estructura de todas las cookies a la 
	 * que pertenecen la ficha, obteniendo asi cual sería la cookie exacta.
	 * @param  string $key Clave de la cookie
	 * @return string      Nombre de la cookie a la que pertenece la clave
	 */
	public static function getCookieKey($key)
	{
		$keys = [
			'nro_ficha_lote_first' => [ 'nameCookie' => CCotitularidad::HEAD_NAME_CK ], 
			'nro_ficha_lote_second' => [ 'nameCookie' => CCotitularidad::HEAD_NAME_CK ], 
			'cod_hoja_catastral' => [ 'nameCookie' => CCotitularidad::HEAD_NAME_CK ], 
			'action' => [ 'nameCookie' => CCotitularidad::HEAD_NAME_CK ],

			"total_cotitular" => [ 'nameCookie' => CCotitularidad::UP01_NAME_CK ],
			"cond_declara" => [ 'nameCookie' => CCotitularidad::UP01_NAME_CK ],
			"estado_llenado" => [ 'nameCookie' => CCotitularidad::UP01_NAME_CK ],
			"observaciones" => [ 'nameCookie' => CCotitularidad::UP01_NAME_CK ],
			"declarante" => [ 'nameCookie' => CCotitularidad::UP01_NAME_CK ],
			"dni_declarante" => [ 'nameCookie' => CCotitularidad::UP01_NAME_CK ],
			"fecha_declarante" => [ 'nameCookie' => CCotitularidad::UP01_NAME_CK ],
			"tec_catastral" => [ 'nameCookie' => CCotitularidad::UP01_NAME_CK ],
			"cod_tec_catastral" => [ 'nameCookie' => CCotitularidad::UP01_NAME_CK ],
			"fecha_tec_catastral" => [ 'nameCookie' => CCotitularidad::UP01_NAME_CK ],
			"supervisor" => [ 'nameCookie' => CCotitularidad::UP01_NAME_CK ],
			"cod_supervisor" => [ 'nameCookie' => CCotitularidad::UP01_NAME_CK ],
			"fecha_supervisor" => [ 'nameCookie' => CCotitularidad::UP01_NAME_CK ],
			"verif_catastral" => [ 'nameCookie' => CCotitularidad::UP01_NAME_CK ],
			"cod_verif_catastral" => [ 'nameCookie' => CCotitularidad::UP01_NAME_CK ],
			"fecha_verif_catastral" => [ 'nameCookie' => CCotitularidad::UP01_NAME_CK ],
			"cod_usuario" => [ 'nameCookie' => CCotitularidad::UP01_NAME_CK ],
			"tec_calidad" => [ 'nameCookie' => CCotitularidad::UP01_NAME_CK ],
			"cod_tec_calidad" => [ 'nameCookie' => CCotitularidad::UP01_NAME_CK ],
			"fecha_tec_calidad" => [ 'nameCookie' =>	CCotitularidad::UP01_NAME_CK ]
		];

		return isset($keys[$key]) ? $keys[$key] : NULL;
	}

}