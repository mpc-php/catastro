<?php 
/**
 * Clase que se encarga de obtener los valores constantes de la ficha individual
 * 
 * @author José Alejandro Vilchez Moreno <joalvm@gmail.com>
 * @package Catastro\modules\ficha\components
 */
class CIndividual {

	const COOKIE_EXPIRE = 604800;
	const COOKIE_HTTPONLY = TRUE;

	const HEAD_NAME_CK = 'FIHEAD';
	const UP01_NAME_CK = 'FIUP01';
	const UP02_NAME_CK = 'FIUP02';

	const KEY_ENCRYPT = 'r4O10AB2288_23A';
	
}