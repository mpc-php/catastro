<?php
/**
 * Clase que se encarga de interactuar con la base de datos, para el manejo de los datos de la ficha de bien común.
 *
 * @author José Alejandro Vilchez Moreno <joalvm@gmail.com>
 * @package Catastro\modules\ficha\components
 */
class QBiencomun {

	public static function getRecapitulacionEdificios($ficha)
	{
		$sql = "EXEC Sp_ListaRecapitulaEdificio :idficha";

		$command = Yii::app()->db->createCommand($sql);

		$command->bindValue(':idficha', $ficha, PDO::PARAM_STR);

		$table = $command->queryAll();

		return $table;
	}

	public static function actualizaRecapitulacionEdificios($ficha)
	{
		$sql = "EXEC Sp_ActualizaRecapitulaEdificio :idficha";

		$command = Yii::app()->db->createCommand($sql);

		$command->bindValue(':idficha', $ficha, PDO::PARAM_STR);

		$table = $command->queryRow();

		return $table;
	}

	/**
	 * Obtiene de la db, las recapitulaciones asignadas a una ficha
	 * @param  string $ficha ID de ficha
	 * @return array        Lista de los resultados de la busqueda
	 */
	public static function getRecapitulaciones($ficha)
	{
		$sql = "SELECT
							NRO,
							EDIFICA,
							ENTRADA,
							PISO,
							UNIDAD,
							isnull(PORCENTAJE , 0) PORC,
							isnull(AOIC, 0) AOIC,
							isnull( ACC , 0 ) ACC,
							isnull( ATC, 0) ATC,
							right(isnull(ID_FICHA_IND, '') ,7) F_IND
						FROM
							RECAP_BBCC
						WHERE
							ID_FICHA = :ficha
						ORDER BY NRO ASC";

		$command = Yii::app()->db->createCommand($sql);

		$command->bindValue(':ficha', $ficha, PDO::PARAM_STR);

		$table = $command->queryAll();

		return $table;
	}

	/**
	 * Recapitula con los datos asignados
	 * @param  object $params parametros para realizar la acción
	 * @return boolean         resultado de la acción
	 */
	public static function recapitular($params)
	{
		$sql = "EXEC SP_GRABA_RECAP_BBCC :ID_FICHA, :VALOR_ATC, :cod_cat";

		$command = Yii::app()->db->createCommand($sql);

		$command->bindValue(':ID_FICHA', $params['ficha'], PDO::PARAM_STR);
		$command->bindValue(':VALOR_ATC', $params['averificada'], PDO::PARAM_STR);
		$command->bindValue(':cod_cat', $params['catastro'], PDO::PARAM_STR);

		$confirm = $command->execute();

		return $confirm;
	}

	/**
	 * Actualiza los porcentajes de las recapitulaciones
	 * @param  object $params parametros para realizar la acción
	 * @return boolean         resultado de la acción
	 */
	public static function actualizarPorcentaje($params)
	{
		$sql = "EXEC sp_actualiza_procen_BC :nro_FICHA, :porcentaje";

		$command = Yii::app()->db->createCommand($sql);

		$command->bindValue(':nro_FICHA', $params['nro_ficha'], PDO::PARAM_STR);
		$command->bindValue(':porcentaje', $params['porcentaje'], PDO::PARAM_STR);

		$confirm = $command->execute();

		return $confirm;
	}

	/**
	 * Primera paso en la acción de guardar datos de la ficha
	 * @return boolean Confirmación de la acción
	 */
	public static function guardarDatosFicha_step_01()
	{
		$sql = "EXEC UPDATE_BIEN_COMUN_1 :m_id_ficha, :m_id_codcat, :m_cuc, :m_chc, :m_fichas_lote, :m_cod_hab_urb, :m_mza_urb, :m_lot_urb,
						:m_sub_lt, :m_edifica, :m_tip_edifica, :m_clasific, :m_pred_cat_en, :m_cod_uso, :m_estruct, :m_zonif,
						:m_area_titulo, :m_area_verif, :m_fr_me_ca, :m_de_me_ca, :m_iz_me_ca, :m_fo_me_ca, :m_fr_me_ti,
						:m_de_me_ti, :m_iz_me_ti, :m_fo_me_ti, :m_fr_co_ca, :m_de_co_ca, :m_iz_co_ca, :m_fo_co_ca, :m_fr_co_ti,
						:m_de_co_ti, :m_iz_co_ti, :m_fo_co_ti, :m_luz, :m_agua, :m_fono, :m_desague, :m_nro_luz, :m_nro_agua,
						:m_nro_fono, :izq_lim_fre_lot, :der_lim_fre_lot, :fon_lim_fre_lot, :fre_lim_fre_lot, :tot_lim_fre_uc, :m_tip_int, :m_int";

		$head = UBiencomun::getCookie(CBiencomun::HEAD_NAME_CK);
		$up = UBiencomun::getCookie(CBiencomun::UP01_NAME_CK);

		$nro_ficha_lote = (isset($head->nro_ficha_lote_first) ? $head->nro_ficha_lote_first : '') . '/' ;
		$nro_ficha_lote .= (isset($head->nro_ficha_lote_second) ? $head->nro_ficha_lote_second : '');

		$command = Yii::app()->db->createCommand($sql);

		//cabecera
		$command->bindValue(':m_id_ficha', $head->id_ficha, PDO::PARAM_STR);
			$command->bindValue(':m_id_codcat', $head->cod_catastro, PDO::PARAM_STR);
			$command->bindValue(':m_cuc', '', PDO::PARAM_STR);
			$command->bindValue(':m_chc', isset($head->cod_hoja_catastral) ? $head->cod_hoja_catastral : '', PDO::PARAM_STR);
			$command->bindValue(':m_fichas_lote', $nro_ficha_lote, PDO::PARAM_STR);

		//cuerpo
		$command->bindValue(':m_cod_hab_urb', (isset($up->code_haburba) ? $up->code_haburba : ''), PDO::PARAM_STR);
		$command->bindValue(':m_mza_urb', (isset($up->manzana) ? $up->manzana : ''), PDO::PARAM_STR);
		$command->bindValue(':m_lot_urb', (isset($up->lote) ? $up->lote : ''), PDO::PARAM_STR);
		$command->bindValue(':m_sub_lt', (isset($up->sublote) ? $up->sublote : ''), PDO::PARAM_STR);
		$command->bindValue(':m_edifica', (isset($up->name_edificio) ? $up->name_edificio : ''), PDO::PARAM_STR);
		$command->bindValue(':m_tip_edifica', (isset($up->tipo_edificio) ? $up->tipo_edificio : ''), PDO::PARAM_STR);
		$command->bindValue(':m_clasific', (isset($up->clasif_predio) ? $up->clasif_predio : ''), PDO::PARAM_STR);
		$command->bindValue(':m_pred_cat_en', (isset($up->pred_catastral) ? $up->pred_catastral : ''), PDO::PARAM_STR);
		$command->bindValue(':m_cod_uso', (isset($up->uso_predio) ? $up->uso_predio : ''), PDO::PARAM_STR);
		$command->bindValue(':m_estruct', (isset($up->estructuracion) ? $up->estructuracion : ''), PDO::PARAM_STR);
		$command->bindValue(':m_zonif', (isset($up->zonificacion) ? $up->zonificacion : ''), PDO::PARAM_STR);
		$command->bindValue(':m_area_titulo', (isset($up->area_titulo) ? $up->area_titulo : ''), PDO::PARAM_STR);
		$command->bindValue(':m_area_verif', (isset($up->area_verificada) ? $up->area_verificada : ''), PDO::PARAM_STR);
		$command->bindValue(':m_fr_me_ca', (isset($up->medcamp_frente) ? $up->medcamp_frente : ''), PDO::PARAM_STR);
		$command->bindValue(':m_de_me_ca', (isset($up->medcamp_derecha) ? $up->medcamp_derecha : ''), PDO::PARAM_STR);
		$command->bindValue(':m_iz_me_ca', (isset($up->medcamp_izquierda) ? $up->medcamp_izquierda : ''), PDO::PARAM_STR);
		$command->bindValue(':m_fo_me_ca', (isset($up->medcamp_fondo) ? $up->medcamp_fondo : ''), PDO::PARAM_STR);
		$command->bindValue(':m_fr_me_ti', (isset($up->medtit_frente) ? $up->medtit_frente : ''), PDO::PARAM_STR);
		$command->bindValue(':m_de_me_ti', (isset($up->medtit_derecha) ? $up->medtit_derecha : ''), PDO::PARAM_STR);
		$command->bindValue(':m_iz_me_ti', (isset($up->medtit_izquierda) ? $up->medtit_izquierda : ''), PDO::PARAM_STR);
		$command->bindValue(':m_fo_me_ti', (isset($up->medtit_fondo) ? $up->medtit_fondo : ''), PDO::PARAM_STR);
		$command->bindValue(':m_fr_co_ca', (isset($up->colcampo_frente) ? $up->colcampo_frente : ''), PDO::PARAM_STR);
		$command->bindValue(':m_de_co_ca', (isset($up->colcampo_derecha) ? $up->colcampo_derecha : ''), PDO::PARAM_STR);
		$command->bindValue(':m_iz_co_ca', (isset($up->colcampo_izquierda) ? $up->colcampo_izquierda : ''), PDO::PARAM_STR);
		$command->bindValue(':m_fo_co_ca', (isset($up->colcampo_fondo) ? $up->colcampo_fondo : ''), PDO::PARAM_STR);
		$command->bindValue(':m_fr_co_ti', (isset($up->coltit_frente) ? $up->coltit_frente : ''), PDO::PARAM_STR);
		$command->bindValue(':m_de_co_ti', (isset($up->coltit_derecha) ? $up->coltit_derecha : ''), PDO::PARAM_STR);
		$command->bindValue(':m_iz_co_ti', (isset($up->coltit_izquierda) ? $up->coltit_izquierda : ''), PDO::PARAM_STR);
		$command->bindValue(':m_fo_co_ti', (isset($up->coltit_fondo) ? $up->coltit_fondo : ''), PDO::PARAM_STR);
		$command->bindValue(':m_luz', (isset($up->luz) ? $up->luz : ''), PDO::PARAM_STR);
		$command->bindValue(':m_agua', (isset($up->agua) ? $up->agua : ''), PDO::PARAM_STR);
		$command->bindValue(':m_fono', (isset($up->phone) ? $up->phone : ''), PDO::PARAM_STR);
		$command->bindValue(':m_desague', (isset($up->desague) ? $up->desague : ''), PDO::PARAM_STR);
		$command->bindValue(':m_nro_luz', (isset($up->sumluz) ? $up->sumluz : ''), PDO::PARAM_STR);
		$command->bindValue(':m_nro_agua', (isset($up->sumagua) ? $up->sumagua : ''), PDO::PARAM_STR);
		$command->bindValue(':m_nro_fono', (isset($up->sumphone) ? $up->sumphone : ''), PDO::PARAM_STR);
		$command->bindValue(':izq_lim_fre_lot', (isset($up->limpfrente_izquierda) ? $up->limpfrente_izquierda : ''), PDO::PARAM_STR);
		$command->bindValue(':der_lim_fre_lot', (isset($up->limpfrente_derecha) ? $up->limpfrente_derecha : ''), PDO::PARAM_STR);
		$command->bindValue(':fon_lim_fre_lot', (isset($up->limpfrente_fondo) ? $up->limpfrente_fondo : ''), PDO::PARAM_STR);
		$command->bindValue(':fre_lim_fre_lot', (isset($up->limpfrente_frente) ? $up->limpfrente_frente : ''), PDO::PARAM_STR);
		$command->bindValue(':tot_lim_fre_uc', (isset($up->total_limpfrenteuc) ? $up->total_limpfrenteuc : ''), PDO::PARAM_STR);
		$command->bindValue(':m_tip_int', (isset($up->tipo_interior) ? $up->tipo_interior : ''), PDO::PARAM_STR);
		$command->bindValue(':m_int', (isset($up->nro_interior) ? $up->nro_interior : ''), PDO::PARAM_STR);

		$result  = $command->execute();

		$command->reset();

		return $result;
	}

	/**
	 * Segundo paso en la acción de guardar datos de la ficha
	 * @return boolean Confirmación de la acción
	 */
	public static function guardarDatosFicha_step_02()
	{

		$sql = "EXEC UPDATE_BIEN_COMUN_2 :m_id_ficha, :m_id_codcat, :m_id_notaria, :m_kardex,
						:m_fecha_escritura, :m_tipo_part_reg, :m_numero, :m_fojas, :m_asiento, :m_fecha_inscrip,
						:m_declar_fab, :m_asiento_dec_fab, :m_fecha_inscrip_fab, :m_area_inv_lot_col, :m_area_inv_jar_ais,
						:m_area_inv_are_pub, :m_area_inv_are_int, :m_cond_declarante, :m_est_llenado, :m_observaciones,
						:m_firma_dec, :m_id_dec, :m_fecha_dec, :m_firma_tec, :m_id_tec, :m_fecha_tec, :m_firma_sup, :m_id_sup,
						:m_fecha_sup, :m_firma_ver, :m_id_ver, :m_fecha_ver, :m_insert_digitador, :firma_tec_calidad,
						:id_tec_calidad, :fecha_tec_calidad";

		$head = UBiencomun::getCookie(CBiencomun::HEAD_NAME_CK);
		$up02 = UBiencomun::getCookie(CBiencomun::UP02_NAME_CK);

		$command = Yii::app()->db->createCommand($sql);

		$command->bindValue(':m_id_ficha', $head->id_ficha, PDO::PARAM_STR);
		$command->bindValue(':m_id_codcat', $head->cod_catastro, PDO::PARAM_STR);

		$command->bindValue(':m_id_notaria', (isset($up02->notaria) ? $up02->notaria: '' ), PDO::PARAM_STR);
		$command->bindValue(':m_kardex', (isset($up02->kardex) ? $up02->kardex: '' ), PDO::PARAM_STR);
		$command->bindValue(':m_fecha_escritura', (isset($up02->fecha_inscpublica) ? Fecha::format($up02->fecha_inscpublica, 'd/m/Y', 'Y-m-d') : '1900-01-01' ), PDO::PARAM_STR);
		$command->bindValue(':m_tipo_part_reg', (isset($up02->tpartida_reg) ? $up02->tpartida_reg: '' ), PDO::PARAM_STR);
		$command->bindValue(':m_numero', (isset($up02->numeros) ? $up02->numeros: '' ), PDO::PARAM_STR);
		$command->bindValue(':m_fojas', (isset($up02->forja) ? $up02->forja: '' ), PDO::PARAM_STR);
		$command->bindValue(':m_asiento', (isset($up02->asiento) ? $up02->asiento: '' ), PDO::PARAM_STR);
		$command->bindValue(':m_fecha_inscrip', (isset($up02->fecha_insc_enregpredio) ? Fecha::format($up02->fecha_insc_enregpredio, 'd/m/Y', 'Y-m-d') : '1900-01-01' ), PDO::PARAM_STR);
		$command->bindValue(':m_declar_fab', (isset($up02->decla_fabrica) ? $up02->decla_fabrica: '' ), PDO::PARAM_STR);
		$command->bindValue(':m_asiento_dec_fab', (isset($up02->inscfabrica) ? $up02->inscfabrica: '' ), PDO::PARAM_STR);
		$command->bindValue(':m_fecha_inscrip_fab', (isset($up02->fecha_inscfabrica) ? Fecha::format($up02->fecha_inscfabrica, 'd/m/Y', 'Y-m-d') : '1900-01-01' ), PDO::PARAM_STR);
		$command->bindValue(':m_area_inv_lot_col', (isset($up02->enlote_colin) ? $up02->enlote_colin: '' ), PDO::PARAM_STR);
		$command->bindValue(':m_area_inv_jar_ais', (isset($up02->enjardin_aislam) ? $up02->enjardin_aislam: '' ), PDO::PARAM_STR);
		$command->bindValue(':m_area_inv_are_pub', (isset($up02->enarea_public) ? $up02->enarea_public: '' ), PDO::PARAM_STR);
		$command->bindValue(':m_area_inv_are_int', (isset($up02->enarea_intang) ? $up02->enarea_intang: '' ), PDO::PARAM_STR);
		$command->bindValue(':m_cond_declarante', (isset($up02->cond_declara) ? $up02->cond_declara: '' ), PDO::PARAM_STR);
		$command->bindValue(':m_est_llenado', (isset($up02->estado_llenado) ? $up02->estado_llenado: '' ), PDO::PARAM_STR);
		$command->bindValue(':m_observaciones', (isset($up02->observaciones) ? mb_strtoupper($up02->observaciones, 'UTF-8') : '' ), PDO::PARAM_STR);
		$command->bindValue(':m_firma_dec', (isset($up02->declarante) ? $up02->declarante: '0' ), PDO::PARAM_STR);
		$command->bindValue(':m_id_dec', (isset($up02->dni_declarante) ? $up02->dni_declarante: '' ), PDO::PARAM_STR);
		$command->bindValue(':m_fecha_dec', (isset($up02->fecha_declarante) ? Fecha::format($up02->fecha_declarante, 'd/m/Y', 'Y-m-d') : '1900-01-01' ), PDO::PARAM_STR);
		$command->bindValue(':m_firma_tec', (isset($up02->tec_catastral) ? $up02->tec_catastral: '0' ), PDO::PARAM_STR);
		$command->bindValue(':m_id_tec', (isset($up02->cod_tec_catastral) ? $up02->cod_tec_catastral: '' ), PDO::PARAM_STR);
		$command->bindValue(':m_fecha_tec', (isset($up02->fecha_tec_catastral) ? Fecha::format($up02->fecha_tec_catastral, 'd/m/Y', 'Y-m-d') : '1900-01-01' ), PDO::PARAM_STR);
		$command->bindValue(':m_firma_sup', (isset($up02->supervisor) ? $up02->supervisor: '0' ), PDO::PARAM_STR);
		$command->bindValue(':m_id_sup', (isset($up02->cod_supervisor) ? $up02->cod_supervisor: '' ), PDO::PARAM_STR);
		$command->bindValue(':m_fecha_sup', (isset($up02->fecha_supervisor) ? Fecha::format($up02->fecha_supervisor, 'd/m/Y', 'Y-m-d') : '1900-01-01' ), PDO::PARAM_STR);
		$command->bindValue(':m_firma_ver', (isset($up02->verif_catastral) ? $up02->verif_catastral: '0' ), PDO::PARAM_STR);
		$command->bindValue(':m_id_ver', (isset($up02->cod_verif_catastral) ? $up02->cod_verif_catastral: '' ), PDO::PARAM_STR);
		$command->bindValue(':m_fecha_ver', (isset($up02->fecha_verif_catastral) ? Fecha::format($up02->fecha_verif_catastral, 'd/m/Y', 'Y-m-d') : '1900-01-01' ), PDO::PARAM_STR);
		$command->bindValue(':m_insert_digitador', Yii::app()->user->id_persona_catas, PDO::PARAM_STR);
		$command->bindValue(':firma_tec_calidad', (isset($up02->tec_calidad) ? $up02->tec_calidad: '0' ), PDO::PARAM_STR);
		$command->bindValue(':id_tec_calidad', (isset($up02->cod_tec_calidad) ? $up02->cod_tec_calidad: '' ), PDO::PARAM_STR);
		$command->bindValue(':fecha_tec_calidad', (isset($up02->fecha_tec_calidad) ? Fecha::format($up02->fecha_tec_calidad, 'd/m/Y', 'Y-m-d') : '1900-01-01' ), PDO::PARAM_STR);

		$result  = $command->execute();

		$command->reset();

		return $result;
	}

	/**
	 * Ejecuta procedimientos almacenados para las acciones de Creación,
	 * modificación y eliminación de direcciones.
	 * @param  object $params Toda la información con los datos a minipular.
	 * @param  string $action Clave para entender la acción que se realizará.
	 * @return boolean         Confirmación de la acción
	 */
	public static function mantenimientoDirecciones($params, $action)
	{
		$sql = 'EXEC ';

		switch ($action) {
			case 'create':
			case 'update':

				if ( $action == 'create' )
					$sql .= "INSERT_DIRECCIONES :m_id_ficha, :m_cod_via, :m_nro_municipal, :m_tipo_pta, :m_cond_nro, :m_certif_numeracion";
				else if ( $action == 'update' )
					$sql .= "UPDATE_DIRECCIONES :m_id_ficha, :m_nro_registro, :m_cod_via, :m_nro_municipal, :m_tipo_pta, :m_cond_nro, :m_certif_numeracion";
				else
					return FALSE;

				$command = Yii::app()->db->createCommand($sql);

				$command->bindValue(":m_id_ficha", $params['ficha'] , PDO::PARAM_STR);

				if ( $action == 'update' )
					$command->bindValue(":m_nro_registro", $params['rowID'] , PDO::PARAM_STR);

				$command->bindValue(":m_cod_via", $params['via'] , PDO::PARAM_STR);
				$command->bindValue(":m_nro_municipal", $params['nro_municipal'] , PDO::PARAM_STR);
				$command->bindValue(":m_tipo_pta", $params['tipo_puerta'] , PDO::PARAM_STR);
				$command->bindValue(":m_cond_nro", $params['cond_num'] , PDO::PARAM_STR);
				$command->bindValue(":m_certif_numeracion", $params['nro_certificado'] , PDO::PARAM_STR);

				break;
			case 'delete':

				$sql .= "DELETE_DIRECCIONES :m_id_ficha, :m_nro_registro";

				$command = Yii::app()->db->createCommand($sql);

				$command->bindValue(":m_id_ficha", $params['ficha'], PDO::PARAM_STR);
				$command->bindValue(":m_nro_registro", $params['rowID'], PDO::PARAM_STR);

				break;
		}

		$result  = $command->execute();

		return $result;
	}

	/**
	 * Ejecuta procedimientos almacenados para las acciones de Creación,
	 * modificación y eliminación de documentos.
	 * @param  object $params Toda la información con los datos a minipular.
	 * @param  string $action Clave para entender la acción que se realizará.
	 * @return boolean         Confirmación de la acción
	 */
	public static function mantenimientoDocumentos($params, $action)
	{
		$sql = 'EXEC ';

		switch ($action) {
			case 'create':
			case 'update':

				if ( $action == 'create' )
					$sql .= "INSERT_DCTOS_PRESENTADOS ";
				else if ( $action == 'update' )
					$sql .= "UPDATE_DCTOS_PRESENTADOS ";
				else
					return FALSE;

				$sql .= ':m_id_ficha, :m_tip_doc, :m_nro_doc, :m_fecha, :m_area_autorizada, :area_const_P01, :area_const_P02, :area_const_P03, ';
				$sql .= ':area_const_P04, :area_const_P05, :area_const_P06, :area_const_otro, :area_const_sot, :area_const_mez';

				$command = Yii::app()->db->createCommand($sql);

				$command->bindValue(":m_id_ficha", $params['ficha'] , PDO::PARAM_STR);
				$command->bindValue(":m_tip_doc", $params['tipo_doc'] , PDO::PARAM_STR);
				$command->bindValue(":m_nro_doc", $params['nro_documento'] , PDO::PARAM_STR);
				$command->bindValue(":m_fecha", ( ! empty($params['fecha_inicio']) ) ? $params['fecha_inicio'] : '1900-01-01' , PDO::PARAM_STR);
				$command->bindValue(":m_area_autorizada", $params['area_autorizada'] , PDO::PARAM_STR);
				$command->bindValue(":area_const_P01", $params['piso1'] , PDO::PARAM_STR);
				$command->bindValue(":area_const_P02", $params['piso2'] , PDO::PARAM_STR);
				$command->bindValue(":area_const_P03", $params['piso3'] , PDO::PARAM_STR);
				$command->bindValue(":area_const_P04", $params['piso4'] , PDO::PARAM_STR);
				$command->bindValue(":area_const_P05", $params['piso5'] , PDO::PARAM_STR);
				$command->bindValue(":area_const_P06", $params['piso6'] , PDO::PARAM_STR);
				$command->bindValue(":area_const_otro", $params['piso7'] , PDO::PARAM_STR);
				$command->bindValue(":area_const_sot", $params['sotano'] , PDO::PARAM_STR);
				$command->bindValue(":area_const_mez", $params['mezzanine'] , PDO::PARAM_STR);

				break;

			case 'delete':

				$sql .= "DELETE_DCTOS_PRESENTADOS :m_id_ficha, :m_nro_registro";

				$command = Yii::app()->db->createCommand($sql);

				$command->bindValue(":m_id_ficha", $params['ficha'] , PDO::PARAM_STR);
				$command->bindValue(":m_nro_registro", $params['tipo_doc'] , PDO::PARAM_STR);

				break;
		}

		$result  = $command->execute();

		return $result;
	}

	/**
	 * Ejecuta procedimientos almacenados para las acciones de Creación,
	 * modificación y eliminación de construcciones.
	 * @param  object $params Toda la información con los datos a minipular.
	 * @param  string $action Clave para entender la acción que se realizará.
	 * @return boolean         Confirmación de la acción
	 */
	public static function mantenimientoConstrucciones($params, $action)
	{
		$sql = 'EXEC ';

		switch ($action) {
			case 'create':
			case 'update':

				if ( $action == 'create' )
				{
					$sql .= "INSERT_CONSTRUCCION ";
					$sql .= ":m_id_ficha, :m_nro_piso, :m_mes, :m_ano, :m_mep, :m_ecs, :m_ecc, :m_estru_muro_col, :m_estru_techo, :m_acaba_piso, ";
					$sql .= ":m_acaba_puerta_ven, :m_acaba_revest, :m_acaba_bano, :m_inst_elect_sanita, :m_area_declarada, :m_area_verificada, :m_uca";
				}
				else if ( $action == 'update' )
				{
					$sql .= "UPDATE_CONSTRUCCION ";
					$sql .= ":m_id_ficha, :m_nro_registro, :m_nro_piso, :m_mes, :m_ano, :m_mep, :m_ecs, :m_ecc, :m_estru_muro_col, :m_estru_techo, :m_acaba_piso, ";
					$sql .= ":m_acaba_puerta_ven, :m_acaba_revest, :m_acaba_bano, :m_inst_elect_sanita, :m_area_declarada, :m_area_verificada, :m_uca";
				}
				else
				{
					return FALSE;
				}

				$command = Yii::app()->db->createCommand($sql);

				$command->bindValue(":m_id_ficha", $params['ficha'], PDO::PARAM_STR);

				if ( $action == 'update' ) {
					$command->bindValue(":m_nro_registro", (int)$params['id'], PDO::PARAM_INT);
				}

				$command->bindValue(":m_nro_piso", $params['nro_piso'], PDO::PARAM_STR);
				$command->bindValue(":m_mes", $params['mes'], PDO::PARAM_STR);
				$command->bindValue(":m_ano", $params['anio'], PDO::PARAM_STR);
				$command->bindValue(":m_mep", $params['mep'], PDO::PARAM_STR);
				$command->bindValue(":m_ecs", $params['ecs'], PDO::PARAM_STR);
				$command->bindValue(":m_ecc", $params['ecc'], PDO::PARAM_STR);
				$command->bindValue(":m_estru_muro_col", mb_strtoupper($params['muros_col'], 'UTF-8'), PDO::PARAM_STR);
				$command->bindValue(":m_estru_techo", mb_strtoupper($params['techo'], 'UTF-8'), PDO::PARAM_STR);
				$command->bindValue(":m_acaba_piso", mb_strtoupper($params['pisos'], 'UTF-8'), PDO::PARAM_STR);
				$command->bindValue(":m_acaba_puerta_ven", mb_strtoupper($params['puerta_vent'], 'UTF-8'), PDO::PARAM_STR);
				$command->bindValue(":m_acaba_revest", mb_strtoupper($params['reves'], 'UTF-8'), PDO::PARAM_STR);
				$command->bindValue(":m_acaba_bano", mb_strtoupper($params['banios'], 'UTF-8'), PDO::PARAM_STR);
				$command->bindValue(":m_inst_elect_sanita", mb_strtoupper($params['inst_electricas'], 'UTF-8'), PDO::PARAM_STR);
				$command->bindValue(":m_area_declarada", $params['area_declarada'], PDO::PARAM_STR);
				$command->bindValue(":m_area_verificada", $params['area_verificada'], PDO::PARAM_STR);
				$command->bindValue(":m_uca", $params['uca'], PDO::PARAM_STR);

				break;

			case 'delete':

				$sql .= "DELETE_CONSTRUCCION :m_id_ficha, :m_nro_registro";

				$command = Yii::app()->db->createCommand($sql);

				$command->bindValue(":m_id_ficha", $params['ficha'] , PDO::PARAM_STR);
				$command->bindValue(":m_nro_registro", $params['id'] , PDO::PARAM_STR);

				break;
		}

		$result  = $command->execute();

		return $result;
	}

	/**
	 * Ejecuta procedimientos almacenados para las acciones de Creación,
	 * modificación y eliminación de las notarias.
	 * @param  object $params Toda la información con los datos a minipular.
	 * @param  string $action Clave para entender la acción que se realizará.
	 * @return boolean         Confirmación de la acción
	 */
	public static function mantenimientoNotaria($params = [])
	{
		$command = Yii::app()->db->createCommand("EXEC Sp_MantenimientoNotaria :Id_Notaria, :Nom_Notaria, :Id_Ubi_Geo");

		$command->bindValue(":Id_Notaria", $params['id'], PDO::PARAM_STR);
		$command->bindValue(":Nom_Notaria", mb_strtoupper($params['name'], 'UTF-8'), PDO::PARAM_STR);
		$command->bindValue(":Id_Ubi_Geo", $params['ubigeo'], PDO::PARAM_STR);

		$confirm = $command->execute();

		return $confirm;
	}

	/**
	 * Consulta la información basica de una persona en base a filtros
	 * @param  string $rel    Clave que filtra la busqueda entre N° de documento o nombres.
	 * @param  int $tipo   Clave que filtra la busqueda por persona juridica o natural
	 * @param  string $search Texto para la busqueda de persona
	 * @return array         Lista con el resultado de la busqueda
	 */
	public static function showPersonasFicha($rel, $tipo, $search)
	{
		$sql = "EXEC ";

		if ( $rel == 'doc' ) {
			$sql .= "SP_ShowPersonasFichaDocumento :TIPO, :ID_FICHA";
		} else if ( $rel == 'name' ) {
			$sql .= "SP_ShowPersonasFicha :TIPO, :ID_FICHA";
		}

		$command = Yii::app()->db->createCommand($sql);

		$command->bindValue(":TIPO", $tipo , PDO::PARAM_STR);
		$command->bindValue(":ID_FICHA", ($search . '%'), PDO::PARAM_INT);

		$table = $command->queryAll();

		return $table;
	}

	/**
	 * Consulta toda la lista de notarias
	 * @return array Lista con todas las notarias
	 */
	public static function get_Notarias()
	{
		$command = Yii::app()->db->createCommand("EXEC sp_ShowNotaria");

		$table = $command->queryAll();

		return $table;
	}

	/**
	 * Obtiene la lista de habilitaciones urbanas que pertenecen a un sector.
	 * @param  string $sector    ID del sector.
	 * @param  int $pertenece valor de pertenencia.
	 * @return array            Lista con el resultado de la busqueda.
	 */
	public static function get_SectorHabUrbanas($sector, $pertenece)
	{
		//$command = Yii::app()->db->createCommand("EXEC sp_ShowSectorHabUrba :CodSector, :pertenece");
		$command = Yii::app()->db->createCommand("SELECT * FROM dbo.vw_HabilitacionesUrbanas");

		/*$command->bindValue(":CodSector", $sector, PDO::PARAM_STR);
		$command->bindValue(":pertenece", $pertenece, PDO::PARAM_INT);*/

		$table = $command->queryAll();

		$ntable = [];

		foreach ($table as $row => $cell) {
			$ntable[] = [
				"CodHabUrb" => $cell['CODIGO'],
				"HabilitacionUrbana" => $cell['NOMBRE'] . ' - ' . $cell['ZONA']
			];
		}

		return $ntable;
	}

	/**
	 * Obtiene la lista de las zonificaciones que pertenecen a un sector.
	 * @param  string $sector    ID del sector.
	 * @param  int $pertenece valor de pertenencia.
	 * @return array            Lista con el resultado de la busqueda.
	 */
	public static function get_SectorZona($sector, $pertenece)
	{
		$command = Yii::app()->db->createCommand("EXEC sp_ShowSectorZona :CodSector, :pertenece");

		$command->bindValue(":CodSector", '0', PDO::PARAM_STR);
		$command->bindValue(":pertenece", 0, PDO::PARAM_INT);

		$table = $command->queryAll();

		return $table;
	}

	/**
	 * Obtiene la lista de las vias que pertenecen a una habilitación urbana.
	 * @param  string $haburbana Codigo de habilitación urbana.
	 * @return array            Lista con el resultado de la busqueda.
	 */
	public static function get_HabUrbanaVias($haburbana)
	{
		$command = Yii::app()->db->createCommand("EXEC sp_ShowHabUrbaVia :CodHU, :pertenece");

		$command->bindValue(":CodHU", $haburbana, PDO::PARAM_STR);
		$command->bindValue(":pertenece", 1, PDO::PARAM_INT);

		$table = $command->queryAll();

		return $table;
	}

	/**
	 * Obtiene la lista de personas basado en un filtro.
	 * @param  string $nro_documento Numero de documento de la persona,
	 * @param  int $tipo          Tipo de persona juridica o natural.
	 * @return object             Datos completos de la busqueda.
	 */
	public static function get_Personas($nro_documento, $tipo)
	{

		$sql = "SELECT
							p.ID_PERSONA id_persona,
							p.NRO_DOC nro_doc,
							p.TIP_DOC tipo_doc,
							p.TIP_PERSONA tipo_persona,
							p.NOMBRES nombres,
							p.APE_PATERNO ape_paterno,
							p.APE_MATERNO ape_materno,
							(p.APE_PATERNO + ' ' + p.APE_MATERNO + ', ' + p.NOMBRES) nombre_completo,
							p.TIP_PERSONA_JURIDICA tipo_persona_juridica,
							p.NRO_RUC_2 nro_ruc,
							p.ESTADO_CIVIL estado_civil
						FROM
							dbo.PERSONAS p
						WHERE
						p.NRO_DOC = :nro_documento
						AND TIP_PERSONA = :tipo";

		$command = Yii::app()->db->createCommand($sql);

		$command->bindValue(':nro_documento', $nro_documento, PDO::PARAM_STR);
		$command->bindValue(':tipo', $tipo, PDO::PARAM_INT);

		$table  = $command->queryRow();

		return $table;
	}

	/**
	 * Obtiene la lista del personal basado en un filtro.
	 * @param  string $codigo Codigo del personal
	 * @param  string $tipo   Tipo de personal
	 * @param  string $cargo  Cargo que ocupa.
	 * @return object         Datos completos de la busqueda
	 */
	public static function get_Personal($codigo, $tipo, $cargo)
	{
		$sql = "SELECT
							[U].[ID_USUARIO] codigo,
							CASE
								[PER].[TIP_PERSONA]
							WHEN '1' THEN
								CASE
									[PER].[APE_PATERNO]
								WHEN '' THEN
									[PER].[NOMBRES]
								ELSE
									[PER].[APE_PATERNO] + ' ' + [PER].[APE_MATERNO] + ', ' + [PER].[NOMBRES]
								END
							WHEN '2' THEN
								[PER].[NOMBRES]
							END AS nombre_completo
						FROM
							[PERSONAS] AS PER,
							[USUARIOS_BAK] AS U
						WHERE [PER].[ID_PERSONA] = [U].[ID_PERSONA]
						AND [U].[CARGO] = :cargo
						AND [PER].[TIP_PERSONA] = :tipo
						AND [U].[ID_USUARIO] = :codigo";

		$command = Yii::app()->db->createCommand($sql);

		$command->bindValue(':codigo', $codigo, PDO::PARAM_INT);
		$command->bindValue(':tipo', $tipo, PDO::PARAM_INT);
		$command->bindValue(':cargo', $cargo, PDO::PARAM_STR);

		$table  = $command->queryRow();

		return $table;
	}

	/**
	 * Obtiene la información basica de la ficha
	 * @param  string $id_ficha ID de ficha
	 * @return object           resultado de la consulta
	 */
	private static function getTableBasicInfo($id_ficha)
	{
		$sql = "SELECT
							f.[NRO_FICHA] nro_ficha,
							f.[ID_FICHA] id_ficha,
							f.[COD_CAT] cod_catastro,
							f.[NRO_FICHA_LOTE] nro_ficha_lote,
							f.[COD_HOJA_CAT] cod_hoja_catastral,
							f.[DC] dc,
							ISNULL(f.[CUC], '') cuc,
							ISNULL(f.[ESTADO_LLENADO], '') estado_llenado,
							ISNULL(f.[CONDIC_DECLARA], '') cond_declara,
							fc.[AREA_VERIFICADA] area_verificada,
							fc.[AREA_TITULO] area_titulo,
							ISNULL(fc.[CLASIFICACION], '') clasif_predio,
							ISNULL(fc.[PREDIO_CATASTRADO_EN], '') pred_catastral,
							ISNULL(fc.[OBSERVACIONES] , '') observaciones,
							ISNULL(fc.[COD_USO], '') uso_predio,
							fc.[TIPO_INTERIOR] tipo_interior,
							fc.[NRO_INTERIOR] nro_interior,
							lt.[MZA_URB] manzana,
							lt.[LOTE_DIST] lote,
							lt.[SUBLOTE] sublote,
							lt.[ESTRUCTURACION] estructuracion,
							lt.[ZONIFICACION] zonificacion,
							ISNULL(lt.[COD_HAB_URB] , '') code_haburba,
							ISNULL(e.[TIPO_EDIFICACION] , '') tipo_edificio,
							e.[NOMBRE_EDIFICACION] name_edificio
						FROM
							dbo.[FICHAS] f,
							dbo.[FICHAS_BIENES_COMUNES] fc,
							dbo.[LOTES] lt
						LEFT OUTER JOIN dbo.[EDIFICACIONES] e ON lt.[ID_LOTE] = e.[ID_LOTE]
						WHERE
								f.[TIP_FICHA]= '04'
						AND f.[ID_FICHA] = fc.[ID_FICHA]
						AND f.[ID_LOTE] = lt.[ID_LOTE]
						AND f.[ID_FICHA] = :id_ficha";

		$command = Yii::app()->db->createCommand($sql);

		$command->bindValue(":id_ficha", $id_ficha, PDO::PARAM_STR);

		$row = $command->queryRow();
		
		if ( ! $row )
			$row = [];

		return $row;
	}

	/**
	 * Obtiene linderos de la ficha
	 * @param  string $id_ficha ID de ficha
	 * @return object           resultado de la consulta
	 */
	private static function getTableLinderos($id_ficha)
	{
		$sql = "SELECT
							FRE_MED_CAMPO medcamp_frente,
							DER_MED_CAMPO medcamp_derecha,
							IZQ_MED_CAMPO medcamp_izquierda,
							FON_MED_CAMPO medcamp_fondo,
							FRE_MED_TITULO medtit_frente,
							DER_MED_TITULO medtit_derecha,
							IZQ_MED_TITULO medtit_izquierda,
							FON_MED_TITULO medtit_fondo,
							FRE_COL_CAMPO colcampo_frente,
							DER_COL_CAMPO colcampo_derecha,
							IZQ_COL_CAMPO colcampo_izquierda,
							FON_COL_CAMPO colcampo_fondo,
							FRE_COL_TITULO coltit_frente,
							DER_COL_TITULO coltit_derecha,
							IZQ_COL_TITULO coltit_izquierda,
							FON_COL_TITULO coltit_fondo,
							IZQ_LIM_FRE_LOT limpfrente_izquierda,
							DER_LIM_FRE_LOT limpfrente_derecha,
							FON_LIM_FRE_LOT limpfrente_fondo,
							FRE_LIM_FRE_LOT limpfrente_frente,
							TOT_LIM_FRE_UC total_limpfrenteuc
						FROM
							LINDEROS
						WHERE
							ID_FICHA = :id_ficha";

		$command = Yii::app()->db->createCommand($sql);

		$command->bindValue(":id_ficha", $id_ficha, PDO::PARAM_STR);

		$row = $command->queryRow();

		if ( ! $row )
			$row = [];

		return $row;
	}

	/**
	 * Obtiene los servicios basicos de la ficha
	 * @param  string $id_ficha ID de ficha
	 * @return object           resultado de la consulta
	 */
	private static function getTableServiciosBasicos($id_ficha)
	{
		$sql = "SELECT
							TELEFONO phone,
							NRO_TELEFONO sumphone,
							AGUA agua,
							NRO_CONTRATO_AGUA sumagua,
							LUZ luz,
							NRO_SUM_LUZ sumluz,
							DESAGUE desague
						FROM
							SERVICIOS_BASICOS
						WHERE
							ID_FICHA = :id_ficha";

		$command = Yii::app()->db->createCommand($sql);

		$command->bindValue(":id_ficha", $id_ficha, PDO::PARAM_STR);

		$row = $command->queryRow();

		if ( ! $row )
			$row = [];

		return $row;
	}

	/**
	 * Obtiene registro legal asignado a la ficha
	 * @param  string $id_ficha ID de ficha
	 * @return object           resultado de la consulta
	 */
	private static function getTableRegistroLegal($id_ficha)
	{
		$sql = "SELECT
							ID_NOTARIA notaria,
							FECHA_ESCRITURA fecha_inscpublica,
							KARDEX kardex
						FROM
							REGISTRO_LEGAL
						WHERE ID_FICHA = :id_ficha";

		$command = Yii::app()->db->createCommand($sql);

		$command->bindValue(":id_ficha", $id_ficha, PDO::PARAM_STR);

		$row = $command->queryRow();

		if ( ! $row )
			$row = [];

		return $row;
	}

	/**
	 * Obtiene la información de sunarp asignada a la ficha
	 * @param  string $id_ficha ID de ficha
	 * @return object           resultado de la consulta
	 */
	private static function getTableSunarp($id_ficha)
	{
		$sql = "SELECT
							ASIENTO asiento,
							ASIEN_INSCRIP_FAB inscfabrica,
							FECHA_INSCRIP_FAB fecha_inscfabrica,
							DECLAR_FAB decla_fabrica,
							FECHA_INSCRIPCION fecha_insc_enregpredio,
							FOJAS forja,
							NUMERO numeros,
							TIPO_PART_REG tpartida_reg
						FROM
							SUNARP
						WHERE ID_FICHA = :id_ficha";

		$command = Yii::app()->db->createCommand($sql);

		$command->bindValue(":id_ficha", $id_ficha, PDO::PARAM_STR);

		$row = $command->queryRow();

		if ( ! $row )
			$row = [];

		return $row;
	}

	/**
	 * Obtiene la información del area invadida, asignada a la ficha
	 * @param  string $id_ficha ID de ficha
	 * @return object           resultado de la consulta
	 */
	private static function getTableAreaInvadida($id_ficha)
	{
		$sql = "SELECT
							EN_LOTE_COLINDANTE enlote_colin,
							EN_AREA_INTANG enarea_intang,
							EN_JARDIN_AISLAM enjardin_aislam,
							EN_AREA_PUBLICA enarea_public
						FROM
							AREA_TERR_INVADIDA
						WHERE
							ID_FICHA = :id_ficha";

		$command = Yii::app()->db->createCommand($sql);

		$command->bindValue(":id_ficha", $id_ficha, PDO::PARAM_STR);

		$row = $command->queryRow();

		if ( ! $row )
			$row = [];

		return $row;
	}

	/**
	 * Obtiene la información del control de ficha
	 * @param  string $id_ficha ID de ficha
	 * @return object           resultado de la consulta
	 */
	private static function getTableControlFicha($id_ficha)
	{
		$sql = "SELECT
							FIRMA_DECLARANTE declarante,
							ID_DECLARANTE dni_declarante,
							FECHA_DECLARANTE fecha_declarante,
							FIRMA_TEC_CATASTRAL tec_catastral,
							ID_TEC_CATASTRAL cod_tec_catastral,
							FECHA_TEC_CATASTRAL fecha_tec_catastral,
							FIRMA_SUPERVISOR supervisor,
							ID_SUPERVISOR cod_supervisor,
							FECHA_SUPERVISOR fecha_supervisor,
							FIRMA_VERIF_CATASTRAL verif_catastral,
							ID_VERIF_CATASTRAL cod_verif_catastral,
							FECHA_VERIF_CATASTRAL fecha_verif_catastral,
							FIRMA_TEC_CALIDAD tec_calidad,
							ID_TEC_CALIDAD cod_tec_calidad,
							FECHA_TEC_CALIDAD fecha_tec_calidad
						FROM
							CONTROL_DE_FICHA
						WHERE
							ID_FICHA = :id_ficha";

		$command = Yii::app()->db->createCommand($sql);

		$command->bindValue(":id_ficha", $id_ficha, PDO::PARAM_STR);

		$row = $command->queryRow();

		if ( ! $row )
			$row = [];

		return $row;
	}

	/**
	 * Obtiene toda la información de la ficha para el proceso de modificación
	 * @param  string $id_ficha ID de ficha
	 * @return object           objecto formateado con la información obtenida
	 */
	public static function get_DataFicha($id_ficha)
	{

		$dataBasic = self::getTableBasicInfo($id_ficha);

		if ( ! $dataBasic ) {
			return [];
		}

		$datalinderos 		= self::getTableLinderos($id_ficha);
		$serviciosBasicos = self::getTableServiciosBasicos($id_ficha);
		$registroLegal 		= self::getTableRegistroLegal($id_ficha);
		$sunarp 					= self::getTableSunarp($id_ficha);
		$areaInvadida 		= self::getTableAreaInvadida($id_ficha);
		$controlFicha 		= self::getTableControlFicha($id_ficha);

		$data = array_merge(
			$dataBasic,
			$datalinderos,
			$serviciosBasicos,
			$registroLegal,
			$sunarp,
			$areaInvadida,
			$controlFicha
		);

		$nrow = new stdClass;

		$nrow->HEAD = self::get_HeadObject($data);
		$nrow->UP01 = self::get_UPObject('UP01', $data);
		$nrow->UP02 = self::get_UPObject('UP02', $data);

		$nrow->UP01 = self::formater_datetime($nrow->UP01, ['finicio', 'fvencimiento']);
		$nrow->UP02 = self::formater_datetime($nrow->UP02, [
			'fecha_inscpublica', 'fecha_insc_enregpredio', 'fecha_inscfabrica', 'fecha_declarante',
			'fecha_tec_catastral', 'fecha_supervisor', 'fecha_verif_catastral', 'fecha_tec_calidad'
		]);

		return $nrow;
	}

	/**
	 * Crea un objeto que contiene la información perteneciente a la cabecera de la ficha.
	 *
	 * El objeto se crea en base a la consulta de toda la ficha, para que la funcion extraiga toda
	 * la información necesaria para la creación del objeto.
	 * @param  object $row Objeto con toda la información de la consulta de la ficha.
	 * @return object      Objeto con la información de la cabecera de ficha.
	 */
	private static function get_HeadObject($row)
	{
		$head = new stdClass;
		$head->cod_ref_catastral = new stdClass;

		$head->nro_ficha = trim($row['nro_ficha']);
		$head->id_ficha = trim($row['id_ficha']);
		$head->cod_catastro = trim($row['cod_catastro']);
		$head->cod_hoja_catastral = trim($row['cod_hoja_catastral']);

		$lotes_arr = explode('/', $row['nro_ficha_lote']);
		$head->nro_ficha_lote_first = (count($lotes_arr) == 2 OR count($lotes_arr) == 1) ? $lotes_arr[0] : '';
		$head->nro_ficha_lote_second = (count($lotes_arr) == 2) ? $lotes_arr[1] : '';

		$head->cod_ref_catastral->ubigeo = mb_substr($row['cod_catastro'], 0, 6);
    $head->cod_ref_catastral->sector = mb_substr($row['cod_catastro'], 10, 2);
    $head->cod_ref_catastral->manzana = mb_substr($row['cod_catastro'], 12, 3);
    $head->cod_ref_catastral->lote = mb_substr($row['cod_catastro'], 15, 3);
    $head->cod_ref_catastral->edificio = mb_substr($row['cod_catastro'], 18, 2);
    $head->cod_ref_catastral->entrada = mb_substr($row['cod_catastro'], 20, 2);
    $head->cod_ref_catastral->piso = mb_substr($row['cod_catastro'], 22, 2);
    $head->cod_ref_catastral->unidad = mb_substr($row['cod_catastro'], 24, 3);
    $head->cod_ref_catastral->dc = $row['dc'];

    return $head;
	}

	/**
	 * Crea un objeto con dos secciones que contienen la información dividida perteneciente a la ficha.
	 *
	 * El objeto se crea en base a la consulta de toda la ficha, para que la funcion extraiga toda
	 * la información necesaria para la creación del objeto y la división de todos los datos.
	 * @param  object $row Objeto con toda la información de la consulta de la ficha.
	 * @return object      Objeto con la información de los datos de la ficha.
	 */
	private static function get_UPObject($up, $row)
	{
		$data = new stdClass;
		$ups = [
			'UP01' => [
				"code_haburba",
				"manzana",
				"lote",
				"sublote",
				"name_edificio",
				"tipo_edificio",
				"tipo_interior",
				"nro_interior",
				"clasif_predio",
				"pred_catastral",
				"uso_predio",
				"estructuracion",
				"zonificacion",
				"area_titulo",
				"area_verificada",
				"medcamp_frente",
				"medcamp_derecha",
				"medcamp_izquierda",
				"medcamp_fondo",
				"medtit_frente",
				"medtit_derecha",
				"medtit_izquierda",
				"medtit_fondo",
				"colcampo_frente",
				"colcampo_derecha",
				"colcampo_izquierda",
				"colcampo_fondo",
				"coltit_frente",
				"coltit_derecha",
				"coltit_izquierda",
				"coltit_fondo",
				"luz",
				"agua",
				"phone",
				"desague",
				"sumluz",
				"sumagua",
				"sumphone",
				"limpfrente_izquierda",
				"limpfrente_derecha",
				"limpfrente_fondo",
				"limpfrente_frente",
				"total_limpfrenteuc"
			],
			'UP02' => [
				"notaria",
				"kardex",
				"fecha_inscpublica",
				"tpartida_reg",
				"numeros",
				"forja",
				"asiento",
				"fecha_insc_enregpredio",
				"decla_fabrica",
				"inscfabrica",
				"fecha_inscfabrica",
				"enlote_colin",
				"enjardin_aislam",
				"enarea_public",
				"enarea_intang",
				"cond_declara",
				"estado_llenado",
				"observaciones",
				"declarante",
				"dni_declarante",
				"fecha_declarante",
				"tec_catastral",
				"cod_tec_catastral",
				"fecha_tec_catastral",
				"supervisor",
				"cod_supervisor",
				"fecha_supervisor",
				"verif_catastral",
				"cod_verif_catastral",
				"fecha_verif_catastral",
				"cod_usuario",
				"tec_calidad",
				"cod_tec_calidad",
				"fecha_tec_calidad"
			]
		];

		foreach ($ups[$up] as $key)
		{
			if ( isset($row[$key]) )
			{
				if ( ! is_null($row[$key]) AND ! empty(trim($row[$key])) )
					$data->{$key} = trim($row[$key]);
			}
		}

		return $data;
	}

	/**
	 * Permite formatear la fecha en formato DD/MM/YYYY.
	 *
	 * La función compara la fecha si la fecha es de 1900 la deja vacía caso contrario
	 * aplica el formateo dejando solo el dia, mes y año en el formato DD/MM/YYYY.
	 * @param  object $up_obj       sección del objeto que buscará la información.
	 * @param  array $datekeys_arr Array con las claves que contienen fecha
	 * @return object               El mismo objeto con las fechas formateadas
	 */
	private static function formater_datetime($up_obj, $datekeys_arr)
	{
		foreach ($datekeys_arr as $key)
		{
			if ( isset($up_obj->{$key}) ) {
				$dateval = new Datetime($up_obj->{$key});

				if ( $dateval->format('Y') != '1900' ) {
					$up_obj->{$key} = $dateval->format('d/m/Y');
				} else {
					unset($up_obj->{$key});
				}
			}
		}

		return $up_obj;
	}

}

/* End of file QIndividual.php */
/* Location: ./ficha/components/QIndividual.php */
