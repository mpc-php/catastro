<?php 
/**
 * Clase que se encarga de obtener los valores constantes de la ficha de bien común
 * 
 * @author José Alejandro Vilchez Moreno <joalvm@gmail.com>
 * @package Catastro\modules\ficha\components
 */
class CBiencomun {

	const COOKIE_EXPIRE = 604800;
	const COOKIE_HTTPONLY = TRUE;

	const HEAD_NAME_CK = 'FBHEAD';
	const UP01_NAME_CK = 'FBUP01';
	const UP02_NAME_CK = 'FBUP02';

	const KEY_ENCRYPT = 'r4O10AB2288_23A';
	
}