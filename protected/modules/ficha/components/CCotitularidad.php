<?php 
/**
 * Clase que se encarga de obtener los valores constantes de la ficha de cotitularidad
 * 
 * @author José Alejandro Vilchez Moreno <joalvm@gmail.com>
 * @package Catastro\modules\ficha\components
 */
class CCotitularidad {

	const COOKIE_EXPIRE = 604800;
	const COOKIE_HTTPONLY = TRUE;

	const HEAD_NAME_CK = 'FCHEAD';
	const UP01_NAME_CK = 'FCUP01';

	const KEY_ENCRYPT = 'r4O10AB2288_23A';

}