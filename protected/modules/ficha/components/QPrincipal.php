<?php 
/**
 * Clase que se encarga de interactuar con la base de datos, para el manejo de los datos que comparten todas las fichas.
 * 
 * @author José Alejandro Vilchez Moreno <joalvm@gmail.com>
 * @package Catastro\modules\ficha\components
 */
class QPrincipal {
	
	/**
	 * Lista la información basica de todas las fichas.
	 * @param  array  $params Datos que sirven para filtrar las fichas
	 * @param  string $type   Tipo de ficha que será buscada
	 * @return array         Lista de resultados de toda la ficha
	 */
	public static function dataFichas( $params = [], $type )
	{
		$search 	= isset( $params['search'] ) 	? trim($params['search']) 			: '';
		$sort 		= isset( $params['sort'] ) 		? trim($params['sort']) 				: 'nro_ficha';
		$order 		= isset( $params['order'] ) 	? trim($params['order']) 				: 'desc';
		$offset 	= isset( $params['offset'] ) 	? (int)trim($params['offset']) 	: 0;
		$limit 		= isset( $params['limit'] ) 	? (int)trim($params['limit']) 	: 10;
		$orderby = $sort . ' ' . $order;

		$sql = "SELECT 
							f.NRO_FICHA nro_ficha, 
							f.ID_FICHA id_ficha, 
							f.COD_CAT code_catastro, 
							ISNULL(f.ESTADO_LLENADO , '') estado_llenado,
							f.[INSERT_FECHA] fecha_ingreso,
							f.[UPDATE_FECHA] ultima_modificacion
						FROM 
							dbo.FICHAS f 
						WHERE 
							f. tip_ficha = {$type} AND 
							f.ID_CATASTRO = " . Yii::app()->user->proceso['selected'] . " AND (
							f.NRO_FICHA LIKE '%{$search}%' OR 
							f.COD_CAT LIKE '%{$search}%' OR 
							f.ID_FICHA LIKE '%{$search}%'
						) ORDER BY {$orderby} OFFSET {$offset} ROWS FETCH NEXT {$limit} ROWS ONLY";

		$command = Yii::app()->db->createCommand($sql);
		$table   = $command->queryAll();

		foreach ($table as $row => &$cell) 
		{
			$fi = '';
			$fm = '';

			if ( !empty($cell['fecha_ingreso']) ) {
				$fi = DateTime::createFromFormat('Y-m-d H:i:s.u', $cell['fecha_ingreso']);
			}

			if ( ! empty($cell['ultima_modificacion']) ) {
				$fm = DateTime::createFromFormat('Y-m-d H:i:s.u', $cell['ultima_modificacion']);
			}

			$cell['nro'] = ($offset + ($row + 1));
			$cell['nro_ficha'] = trim($cell['nro_ficha']);
			$cell['id_ficha'] = trim($cell['id_ficha']);
			$cell['code_catastro'] = trim($cell['code_catastro']);
			$cell['estado_llenado'] = (int)trim($cell['estado_llenado']);
			//$cell['fecha_ingreso'] = empty($fi) ? '' : $fi->format('d/m/Y h:ia');
			//$cell['ultima_modificacion'] = empty($fm) ? '' : $fm->format('d/m/Y h:ia');
		}

		$data['rows'] = $table;
		$data['total'] = self::get_sizeRowsFicha($params, $type);

		return $data;
	}

	/**
	 * Obtiene el numero de fichas que se han filtrado
	 * @param  array  $params Datos que sirven de filtro
	 * @param  string $type   Tipo de ficha
	 * @return int         Numero de fichas filtradas
	 */
	private static function get_sizeRowsFicha( $params = [], $type )
	{
		$search = isset( $params['search'] ) ? trim($params['search']) : '';

		$sql = "SELECT 
							COUNT(f.NRO_FICHA) cant
						FROM dbo.FICHAS f
						WHERE f. tip_ficha = {$type} AND 
							f.ID_CATASTRO = " . Yii::app()->user->proceso['selected'] . " AND (
							f.NRO_FICHA LIKE '%{$search}%' OR 
							f.COD_CAT LIKE '%{$search}%' OR
							f.ID_FICHA LIKE '%{$search}%'
						)";

		$command = Yii::app()->db->createCommand($sql);
		$row  = $command->queryRow();


		return (int)$row['cant'];
	}

	/**
	 * Genera la ficha partiendo de los datos basicos.
	 * @param  string $id_ficha   ID generado de la ficha
	 * @param  string $cod_cat    Codigo catastral generado
	 * @param  string $dc         Dato unico catastral
	 * @param  string $act_des    Situación de la ficha(Activada, Desactivada)
	 * @param  string $estado_dig Estado de digitación
	 * @param  string $digitador  Codigo del digitador
	 * @return boolean             Resultado de la acción
	 */
	public static function generarFicha($id_ficha, $cod_cat, $dc, $act_des, $estado_dig, $digitador) 
	{
		$sql = "EXEC INSERT_FICHA '{$id_ficha}', '{$cod_cat}', '{$dc}', '{$act_des}', '{$estado_dig}', '{$digitador}'";

		$command = Yii::app()->db->createCommand($sql);
		$result  = $command->execute();

		return $result;
	}

	/**
	 * Elimina una ficha asignada
	 * @param  string $id_ficha ID de ficha
	 * @return boolean           resultado de la acción
	 */
	public static function eliminarFicha($id_ficha) 
	{
		$command = Yii::app()->db->createCommand("EXEC DELETE_FICHAS :m_id_ficha");

		$command->bindValue(":m_id_ficha", $id_ficha, PDO::PARAM_STR);

		$confirm = $command->execute();

		return $confirm;
	}

	/**
	 * Verifica si el numero de ficha ya esta asignado en la db
	 * @param  string $nro_ficha Numero de ficha
	 * @param  string $tipo      Tipo de ficha
	 * @return int            cantidad de coincidencias encontradas
	 */
	public static function validar_ficha($nro_ficha, $tipo) 
	{
		$sql = "SELECT 
							COUNT(nro_ficha) cant 
						FROM fichas 
						WHERE tip_ficha ='{$tipo}' 
						AND  nro_ficha ='{$nro_ficha}' 
						AND id_catastro='".Yii::app()->user->proceso['selected']."'";

		$command = Yii::app()->db->createCommand($sql);
		$row  = $command->queryRow();

		return (int)$row['cant'];
	}

	/**
	 * Verifica si el codigo catastral ya esta registrado en la db
	 * @param  string $cod_cat Codigo catastral
	 * @param  string $tipo    Tipo de ficha
	 * @return int          cantidad de coincidencias encontradas
	 */
	public static function validar_CodigoCatastral($cod_cat, $tipo)
	{
		$sql = "EXEC sp_ValidaCodigoCatastral '{$cod_cat}', '{$tipo}'";

		$command = Yii::app()->db->createCommand($sql);
		$row  = $command->queryRow();

		return (int)$row['registros'];
	}

	/**
	 * Genera el codigo catastral a partir del codigo referencial catastral
	 * @param  object $arr_code Objeto que contiene toda la información del codigo referencial catastral
	 * @return string           Codigo catrastral
	 */
	public static function get_CodigoCatastro($arr_code) 
	{
		return (
			$arr_code['ubigeo'].
			Yii::app()->user->proceso['selected'].
			$arr_code['sector'].
			$arr_code['manzana'].
			$arr_code['lote'].
			$arr_code['edificio'].
			$arr_code['entrada'].
			$arr_code['piso'].
			$arr_code['unidad']
		);
	}

	/**
	 * Genera el ID de ficha para el tipo de ficha asignado
	 * @param  string $cod_ubigeo Ubigeo de la ficha
	 * @param  string $nro_ficha  Numero de ficha
	 * @param  string $tipo       Tipo de ficha
	 * @return string             ID de ficha
	 */
	public static function get_IdFicha($cod_ubigeo, $nro_ficha, $tipo)
	{
		return (
			$cod_ubigeo .
			Yii::app()->user->proceso['selected'] .
			$tipo .
			$nro_ficha
		);
	}

	/**
	 * Consulta en la db la información de los modulos de cada ficha.
	 *
	 * La consulta se realiza en base a la ficha y al tipo de dato que será consultado
	 * permitiendo el listado de los modulos independientes que cada ficha contiene
	 * y que son mostrados en forma de tabla.
	 * @param  string $tipo     Tipo de consulta
	 * @param  string $id_ficha ID de ficha
	 * @return array           Lista de resultados de la consulta
	 */
	public static function get_InfoFichas($tipo, $id_ficha)
	{
		$command = Yii::app()->db->createCommand("EXEC SP_INFORMACION_FICHA_WEB :TIPO, :ID_FICHA");

		$command->bindValue(":TIPO", $tipo , PDO::PARAM_STR);
		$command->bindValue(":ID_FICHA", $id_ficha , PDO::PARAM_INT);

		$table = $command->queryAll();

		if ( ! empty($table) )
		{
			foreach ($table as $row => &$cell) 
			{
				if ( isset($cell['FECHA_DOC']) )
				{
					$date = strtotime($cell['FECHA_DOC']);
					$cell['FECHA_DOC'] = date('d/m/Y', $date);
				}
			}
		}

		return $table;
	}

	/**
	 * Consulta en la db, información que será listado en los elemento de tipo selección
	 * @param  string $tipo tipo de consulta
	 * @param  int $sw   dato desconocido
	 * @return array       Lista de resultados de la consulta
	 */
	public static function get_Tipos($tipo, $sw)
	{
		$command = Yii::app()->db->createCommand("EXEC SP_TIPOS :type, :sw");

		$command->bindValue(":type", $tipo , PDO::PARAM_STR);
		$command->bindValue(":sw", $sw , PDO::PARAM_INT);

		$table = $command->queryAll();

		return $table;
	}

	/**
	 * Consulta el ubigeo en base a su codigo, en caso este sea 0 se listarán los departamentos
	 * @param  string $codigo Codigo de ubigeo
	 * @return array         Lista de resultados de la consulta
	 */
	public static function get_Ubigeo($codigo)
	{
		$command = Yii::app()->db->createCommand("EXEC SP_TIPOS_UBIGEO :COD_UBIGEO");

		$command->bindValue(":COD_UBIGEO", $codigo, PDO::PARAM_STR);

		$table = $command->queryAll();

		return $table;
	}

	/**
	 * Valida la existencia del sector que se está registrando en la ficha
	 * @param  string $sector Codigo del sector.
	 * @param  string $ubigeo Codigo de ubigeo.
	 * @return boolean         Confirmación de su existencia.
	 */
	public static function exists_Sector($sector, $ubigeo) 
	{
		$sql = "SELECT 
							COUNT(s.COD_SECTOR) cant 
						FROM 
							dbo.SECTORES s
						WHERE s.COD_SECTOR = '{$sector}'
						AND s.ID_CATASTRO = '".Yii::app()->user->proceso['selected']."' 
						AND s.ID_UBI_GEO = '{$ubigeo}'";

		$command = Yii::app()->db->createCommand($sql);
		$row  = $command->queryRow();

		return ((int)$row['cant'] > 0) ? TRUE : FALSE;
	}

	/**
	 * Filtra a todas las personas registradas en la tabla personas
	 * @param  array   $params Datos de filtrado
	 * @param  integer $type   Tipo de persona
	 * @return array          Lista de resultados de la consulta
	 */
	public static function search_PersonasData($params = [], $type = 1)
	{
		$search 	= isset( $params['search'] ) 	? trim($params['search']) 			: '';
		$offset 	= isset( $params['offset'] ) 	? (int)trim($params['offset']) 	: 0;
		$limite 		= isset( $params['limit'] ) 	? (int)trim($params['limit']) 	: 10;

		if ( $params['rel'] == 'us' ) 
		{
			$cargo = $params['cargo'];
			$sql = "SELECT 
								U.[ID_USUARIO] CODIGO, 
								PER.[ID_PERSONA],
								CASE 
									PER.[TIP_PERSONA]  
									WHEN '1' THEN 
										CASE 
											PER.[APE_PATERNO] 
											WHEN '' THEN 
												PER.[NOMBRES] 
											ELSE 
												PER.[APE_PATERNO] + ' ' + PER.[APE_MATERNO] + ', ' + PER.[NOMBRES] 
										END 
									WHEN '2' THEN 
										PER.[NOMBRES] 
								END AS NOMBRES
							FROM 
								[dbo].[PERSONAS] PER, 
								[dbo].[USUARIOS_BAK] U 
							WHERE 
								PER.[ID_PERSONA] = U.[ID_PERSONA] 
							AND U.[CARGO] = {$cargo} 
							AND PER.[TIP_PERSONA] = {$type} 
							AND (PER.[APE_PATERNO] + ' ' + PER.[APE_MATERNO] + ' ' + PER.[NOMBRES] + ' ' + U.[ID_USUARIO]) LIKE :search 
							ORDER BY PER.[ID_PERSONA] OFFSET :offset ROWS FETCH NEXT :limite ROWS ONLY";

		} 
		else if ( $params['rel'] == 'pe' ) 
		{
			$sql = "SELECT 
								P.[NRO_DOC] CODIGO,
								P.[ID_PERSONA],
								P.[TIP_DOC],
								CASE P.[TIP_PERSONA] 
									WHEN '1' THEN 
										CASE LTRIM(RTRIM(P.[APE_PATERNO])) 
											WHEN '' THEN 
												P.[NOMBRES] 
											ELSE 
											P.[APE_PATERNO] + ' ' + P.[APE_MATERNO] + ', ' + P.[NOMBRES] 
										END 
									WHEN '2' THEN P.[NOMBRES] 
								END AS NOMBRES, 
								P.[ESTADO_CIVIL]
							FROM 
								[dbo].[PERSONAS] P 
							WHERE 
									P.[TIP_PERSONA] = {$type} 
							AND ( P.[APE_PATERNO] + ' ' + P.[APE_MATERNO] + ' ' + P.[NOMBRES] + ' ' + P.[NRO_DOC] ) LIKE :search 
							ORDER BY P.[APE_PATERNO] OFFSET :offset ROWS FETCH NEXT :limite ROWS ONLY";
		} 
		else 
		{
			return ["rows" => [], "total" => 0];
		};

		$command = Yii::app()->db->createCommand($sql);

		$command->bindValue(':search', '%'.$search.'%', PDO::PARAM_STR);
		$command->bindValue(':offset', $offset, PDO::PARAM_INT);
		$command->bindValue(':limite', $limite, PDO::PARAM_INT);

		$table   = $command->queryAll();

		$data['rows'] = $table;
		$data['total'] = self::search_PersonasLength($params, $type);

		return $data;
	}

	public static function search_via($code)
	{
		$sql = "SELECT 
							v.[CODIGO] CODIGO,
							(v.[DESC_TABLA_DETALLE] + ' ' + v.[NOMBRE]) NOMBRE
						FROM dbo.[vw_Vias] v
						WHERE v.[CODIGO] = :code";

		$command = Yii::app()->db->createCommand($sql);

		$command->bindValue(':code', $code, PDO::PARAM_STR);

		$row  = $command->queryRow();

		if ( ! $row ) {
			$row = [];
		}

		return $row;
	}

	/**
	 *  Cuenta la cantidad de filas que ha generado la ejecución de la función search_PersonasData
	 * @param  array   $params Datos de filtrado
	 * @param  integer $type   Tipo de persona
	 * @return int          Cantidad de filas
	 */
	private static function search_PersonasLength($params = [], $type = 1)
	{
		$search 	= isset( $params['search'] ) 	? trim($params['search']) 			: '';

		if ( $params['rel'] == 'us' )
		{
			$cargo = $params['cargo'];
			$sql = "SELECT 
								COUNT(U.[ID_USUARIO]) cant
							FROM 
								[dbo].[PERSONAS] PER, 
								[dbo].[USUARIOS_BAK] U 
							WHERE 
								PER.[ID_PERSONA] = U.[ID_PERSONA] 
							AND U.[CARGO] = {$cargo}
							AND PER.[TIP_PERSONA] = {$type} 
							AND (PER.[APE_PATERNO] + ' ' + PER.[APE_MATERNO] + ' ' + PER.[NOMBRES] + ' ' + U.[ID_USUARIO]) LIKE :search";
		}
		else if ( $params['rel'] == 'pe' ) 
		{
			$sql = "SELECT 
								COUNT(P.[ID_PERSONA]) cant
							FROM 
								[dbo].[PERSONAS] P 
							WHERE 
									P.[TIP_PERSONA] = {$type}
							AND ( P.[APE_PATERNO] + ' ' + P.[APE_MATERNO] + ' ' + P.[NOMBRES] + ' ' + P.[NRO_DOC] ) LIKE :search";
		}

		$command = Yii::app()->db->createCommand($sql);

		$command->bindValue(':search', '%'.$search.'%', PDO::PARAM_STR);

		$row  = $command->queryRow();

		return (int)$row['cant'];
	}

	/**
	 * Obtiene la lista de campos requeridos para cada ficha, en el proceso de observación
	 * @param  string $type Tipo de ficha
	 * @return array       Lista de resultados de la consulta
	 */
	public static function getFieldRequired($type)
	{
		$sql = "SELECT 
							ID_CAMPO id,
							COD_FICHA cod_ficha,
							CAMPO_NOMBRE field,
							CAMPO_TEXTO title
						FROM dbo.[CAMPOS_REQUERIDOS]
						WHERE COD_FICHA = :type";

		$command = Yii::app()->db->createCommand($sql);

		$command->bindValue(':type', $type, PDO::PARAM_STR);

		$table  = $command->queryAll();

		return $table;
	}

	/**
	 * Guarda en base de datos la observación generada
	 * @param  object $params Datos para ser manipulados
	 * @return boolean         Confirmación de la acción
	 */
	public static function generateObservacion($params)
	{
		$command = Yii::app()->db->createCommand();

		$query = $command->insert('OBSERVACIONES', [
						    'NRO_FICHA'=> $params['nro_ficha'],
						    'COD_REF_CATASTRO'=> $params['cod_ref_catastral'],
						    'COD_DIGITADOR' => Yii::app()->user->id_persona_catas,
						    'FECHA_OBSERVADA' => (new DateTime())->format('d/m/Y'),
						    'OBSERVACION' => $params['observacion'],
						    'TIPO_FICHA' => $params['tipo_ficha']
						]);

		if ( $query ) {

			$id = Yii::app()->db->getLastInsertID();

			if ( isset($params['fields']) ) {
				foreach ( $params['fields'] as $index => $obj ) {
					$command->insert('CAMPOS_OBSERVADOS', [
					    'ID_OBSERVACION'=> $id,
					    'ID_CAMPO' => $obj['id_campo'],
					    'OBSERVACION' => $obj['observacion']
					]);
				}
			}

			$confirm = self::eliminarFicha($params['ficha']);

			return TRUE;

		} else {
			return false;
		}
	}

	public static function getUbicacionPredio($id_ficha)
	{
		$SQL = "SELECT TOP 1
							up.[COD_HAB_URB] AS [codigo_haburbana],
							up.[hab_urb] AS [haburbana],
							up.[ZN_SC_ETAPA] AS [zona],
							up.[MZA_URB] AS [manzana],
							up.[LOTE_DIST] AS [lote],
							up.[SUBLOTE] AS [sublote],
							up.[COD_VIA] AS [codigo_via],
							up.[nombre_via] AS [via],
							up.[NRO_MUNICIPAL] AS [nro_municipal],
							up.[NOMBRE_EDIFICACION] AS [edificacion],
							up.[TIPO_EDIFICACION] AS [tipo_edificacion],
							up.[NRO_INTERIOR] AS [nro_interior]
						FROM 
							dbo.[vw_ubicacionPredio_prin] up
						WHERE  [ID_FICHA] = :id_ficha";

		$command = Yii::app()->db->createCommand($SQL);

		$command->bindValue(':id_ficha', $id_ficha, PDO::PARAM_STR);

		$row  = $command->queryAll();

		foreach ($row as $index => &$col) {
			$col['nro_interior'] = empty($col['nro_interior']) ? '' : $col['nro_interior'];
			$col['nro_municipal'] = empty($col['nro_municipal']) ? '' : $col['nro_municipal'];
			$col['codigo_haburbana'] = empty($col['codigo_haburbana']) ? '' : $col['codigo_haburbana'];
			$col['haburbana'] = empty($col['haburbana']) ? '' : $col['haburbana'];
			$col['zona'] = empty($col['zona']) ? '' : $col['zona'];
			$col['manzana'] = empty($col['manzana']) ? '' : $col['manzana'];
			$col['lote'] = empty($col['lote']) ? '' : $col['lote'];
			$col['sublote'] = empty($col['sublote']) ? '' : $col['sublote'];
			$col['codigo_via'] = empty($col['codigo_via']) ? '' : $col['codigo_via'];
			$col['via'] = empty($col['via']) ? '' : $col['via'];
			$col['edificacion'] = empty($col['edificacion']) ? '' : $col['edificacion'];
			$col['tipo_edificacion'] = empty($col['tipo_edificacion']) ? '' : $col['tipo_edificacion'];
		}

		return empty($row) ? [] : $row[0];
	}

}