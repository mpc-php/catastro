<?php
/**
 * Clase que se encarga de obtener los valores constantes de la ficha de actividad economica
 * 
 * @author José Alejandro Vilchez Moreno <joalvm@gmail.com>
 * @package Catastro\modules\ficha\components
 */
class CActeconomica {

	const COOKIE_EXPIRE = 604800;
	const COOKIE_HTTPONLY = TRUE;

	const HEAD_NAME_CK = 'FAHEAD';
	const UP01_NAME_CK = 'FAUP01';

	const KEY_ENCRYPT = 'r4O10AB2288_23A';

}
