<?php 
/**
 * Clase que se encarga de manejar las cookies creadas para la ficha individual
 * 
 * @author José Alejandro Vilchez Moreno <joalvm@gmail.com>
 * @package Catastro\modules\ficha\components
 */
class UIndividual {

	/**
	 * Genera la cookie con el objeto pasado por parametro.
	 *
	 * La funcion reconoce el objecto y lo convierte a un string para la cookie
	 * luego de eso reemplaza a la antigua con los nuevos datos.
	 * @param  string $name  Nombre de la cookie.
	 * @param  array/object  $value Datos que guardará la cookie.
	 * @return void
	 */
	public static function generateCookie($name, $value = [])
	{
		$Encrypt 	= new Encrypt;
		$nvalue;

		if ( is_array($value) OR is_object($value) ) {
			$nvalue = json_encode($value);
		}

		$nvalue = $Encrypt->encode($nvalue, CIndividual::KEY_ENCRYPT);

		$cookie = new CHttpCookie($name, $nvalue);

		$cookie->expire 	= time() + CIndividual::COOKIE_EXPIRE;
		$cookie->httpOnly = CIndividual::COOKIE_HTTPONLY;

		Yii::app()->request->cookies[$name] = $cookie;
	}

	/**
	 * Obtiene la información de una cookie formateando su valor como objeto para su facil manipulación
	 * @param  string $name Nombre de la cookie.
	 * @return object       Dato formateado de la cookie
	 */
	public static function getCookie($name) 
	{
		$Encrypt 	= new Encrypt;
		$obj 			= new stdClass;

		$value = Yii::app()->request->cookies->contains($name) ? Yii::app()->request->cookies[$name]->value : NULL;

		if ( $value != NULL ) {
			$obj = json_decode( $Encrypt->decode($value, CIndividual::KEY_ENCRYPT) );
		}

		return $obj;
	}

	/**
	 * Crea o actualiza un dato detro de la cookie
	 * @param string $key   key del objeto
	 * @param string $value dato que será asignado
	 */
	public static function setCookie($key = '', $value = '')
	{
		$Encrypt 	= new Encrypt;
		$obj 			= (object)self::getCookieKey($key);

		if ( ! empty((array)$obj) )
		{
			$cookieObj = self::getCookie($obj->nameCookie);
			$cookieObj->{$key} = $value;

			self::generateCookie($obj->nameCookie, $cookieObj);
		}
	}

	/**
	 * Obtiene el valor que se encuentra dentro una cookie
	 * @param  string $name Nombre de la cookie
	 * @param  string $key  Clave que esta dentro de la cookie y que contiene el valor
	 * @return string       valor de la clave que esta dentro de la cookie
	 */
	public static function getCookieValue($name, $key)
	{
		$obj = self::getCookie($name);

		if ( isset($obj->{$key}) ) {
			return $obj->{$key};
		}

		return NULL;
	}

	/**
	 * Obtiene el nombre de la cookie a la que pertenece una clave en especifico.
	 *
	 * La funcion busca entre una lista que guarda la estructura de todas las cookies a la 
	 * que pertenecen la ficha, obteniendo asi cual sería la cookie exacta.
	 * @param  string $key Clave de la cookie
	 * @return string      Nombre de la cookie a la que pertenece la clave
	 */
	public static function getCookieKey($key)
	{
		$keys = [
			'nro_ficha_lote_first' => [ 'dataType' => 1, 'nameCookie' => CIndividual::HEAD_NAME_CK], 
			'nro_ficha_lote_second' => [ 'dataType' => 1, 'nameCookie' => CIndividual::HEAD_NAME_CK], 
			'cod_hoja_catastral' => [ 'dataType' => 1, 'nameCookie' => CIndividual::HEAD_NAME_CK], 
			'cod_predial_rentas' => [ 'dataType' => 1, 'nameCookie' => CIndividual::HEAD_NAME_CK], 
			'ua_predial_rentas' => [ 'dataType' => 1, 'nameCookie' => CIndividual::HEAD_NAME_CK], 

			/* claves para el procedimiento 1 */
			"code_haburba" => [ 'dataType' => 1, 'nameCookie' => CIndividual::UP01_NAME_CK],
				"manzana" => [ 'dataType' => 1, 'nameCookie' => CIndividual::UP01_NAME_CK],
				"lote" => [ 'dataType' => 1, 'nameCookie' => CIndividual::UP01_NAME_CK],
				"sublote" => [ 'dataType' => 1, 'nameCookie' => CIndividual::UP01_NAME_CK],
				"name_edificio" => [ 'dataType' => 1, 'nameCookie' => CIndividual::UP01_NAME_CK],
				"tipo_edificio" => [ 'dataType' => 1, 'nameCookie' => CIndividual::UP01_NAME_CK],
				"tipo_interior" => [ 'dataType' => 1, 'nameCookie' => CIndividual::UP01_NAME_CK],
				"nro_interior" => [ 'dataType' => 1, 'nameCookie' => CIndividual::UP01_NAME_CK],
				"cond_titular" => [ 'dataType' => 1, 'nameCookie' => CIndividual::UP01_NAME_CK],
				"cond_especialpredio" => [ 'dataType' => 1, 'nameCookie' => CIndividual::UP01_NAME_CK],
				"nro_resolucion" => [ 'dataType' => 1, 'nameCookie' => CIndividual::UP01_NAME_CK],
				"porcent" => [ 'dataType' => 1, 'nameCookie' => CIndividual::UP01_NAME_CK],
				"finicio" => [ 'dataType' => 1, 'nameCookie' => CIndividual::UP01_NAME_CK],
				"fvencimiento" => [ 'dataType' => 1, 'nameCookie' => CIndividual::UP01_NAME_CK],
				"clasif_predio" => [ 'dataType' => 1, 'nameCookie' => CIndividual::UP01_NAME_CK],
				"pred_catastral" => [ 'dataType' => 1, 'nameCookie' => CIndividual::UP01_NAME_CK],
				"uso_predio" => [ 'dataType' => 1, 'nameCookie' => CIndividual::UP01_NAME_CK],
				"estructuracion" => [ 'dataType' => 1, 'nameCookie' => CIndividual::UP01_NAME_CK],
				"zonificacion" => [ 'dataType' => 1, 'nameCookie' => CIndividual::UP01_NAME_CK],
				"area_titulo" => [ 'dataType' => 1, 'nameCookie' => CIndividual::UP01_NAME_CK],
				"area_declarada" => [ 'dataType' => 1, 'nameCookie' => CIndividual::UP01_NAME_CK],
				"area_verificada" => [ 'dataType' => 1, 'nameCookie' => CIndividual::UP01_NAME_CK],
				"medcamp_frente" => [ 'dataType' => 1, 'nameCookie' => CIndividual::UP01_NAME_CK],
				"medcamp_derecha" => [ 'dataType' => 1, 'nameCookie' => CIndividual::UP01_NAME_CK],
				"medcamp_izquierda" => [ 'dataType' => 1, 'nameCookie' => CIndividual::UP01_NAME_CK],
				"medcamp_fondo" => [ 'dataType' => 1, 'nameCookie' => CIndividual::UP01_NAME_CK],
				"medtit_frente" => [ 'dataType' => 1, 'nameCookie' => CIndividual::UP01_NAME_CK],
				"medtit_derecha" => [ 'dataType' => 1, 'nameCookie' => CIndividual::UP01_NAME_CK],
				"medtit_izquierda" => [ 'dataType' => 1, 'nameCookie' => CIndividual::UP01_NAME_CK],
				"medtit_fondo" => [ 'dataType' => 1, 'nameCookie' => CIndividual::UP01_NAME_CK],
				"colcampo_frente" => [ 'dataType' => 1, 'nameCookie' => CIndividual::UP01_NAME_CK],
				"colcampo_derecha" => [ 'dataType' => 1, 'nameCookie' => CIndividual::UP01_NAME_CK],
				"colcampo_izquierda" => [ 'dataType' => 1, 'nameCookie' => CIndividual::UP01_NAME_CK],
				"colcampo_fondo" => [ 'dataType' => 1, 'nameCookie' => CIndividual::UP01_NAME_CK],
				"coltit_frente" => [ 'dataType' => 1, 'nameCookie' => CIndividual::UP01_NAME_CK],
				"coltit_derecha" => [ 'dataType' => 1, 'nameCookie' => CIndividual::UP01_NAME_CK],
				"coltit_izquierda" => [ 'dataType' => 1, 'nameCookie' => CIndividual::UP01_NAME_CK],
				"coltit_fondo" => [ 'dataType' => 1, 'nameCookie' => CIndividual::UP01_NAME_CK],
				"luz" => [ 'dataType' => 1, 'nameCookie' => CIndividual::UP01_NAME_CK],
				"agua" => [ 'dataType' => 1, 'nameCookie' => CIndividual::UP01_NAME_CK],
				"phone" => [ 'dataType' => 1, 'nameCookie' => CIndividual::UP01_NAME_CK],
				"desague" => [ 'dataType' => 1, 'nameCookie' => CIndividual::UP01_NAME_CK],
				"sumluz" => [ 'dataType' => 1, 'nameCookie' => CIndividual::UP01_NAME_CK],
				"sumagua" => [ 'dataType' => 1, 'nameCookie' => CIndividual::UP01_NAME_CK],
				"sumphone" => [ 'dataType' => 1, 'nameCookie' => CIndividual::UP01_NAME_CK],
				"fuente_info" => [ 'dataType' => 1, 'nameCookie' => CIndividual::UP01_NAME_CK],
				"area_ocupada" => [ 'dataType' => 1, 'nameCookie' => CIndividual::UP01_NAME_CK],
				"limpfrente_izquierda" => [ 'dataType' => 1, 'nameCookie' => CIndividual::UP01_NAME_CK],
				"limpfrente_derecha" => [ 'dataType' => 1, 'nameCookie' => CIndividual::UP01_NAME_CK],
				"limpfrente_fondo" => [ 'dataType' => 1, 'nameCookie' => CIndividual::UP01_NAME_CK],
				"limpfrente_frente" => [ 'dataType' => 1, 'nameCookie' => CIndividual::UP01_NAME_CK],
				"total_limpfrenteuc" => [ 'dataType' => 1, 'nameCookie' => CIndividual::UP01_NAME_CK],
				"gas" => [ 'dataType' => 1, 'nameCookie' => CIndividual::UP01_NAME_CK],
				"cable" => [ 'dataType' => 1, 'nameCookie' => CIndividual::UP01_NAME_CK],
				"internet" => [ 'dataType' => 1, 'nameCookie' => CIndividual::UP01_NAME_CK],
				"nro_hombres" => [ 'dataType' => 1, 'nameCookie' => CIndividual::UP01_NAME_CK],
				"nro_mujeres" => [ 'dataType' => 1, 'nameCookie' => CIndividual::UP01_NAME_CK],
				"nro_ninios" => [ 'dataType' => 1, 'nameCookie' => CIndividual::UP01_NAME_CK],
				"nro_adulmayor" => [ 'dataType' => 1, 'nameCookie' => CIndividual::UP01_NAME_CK],
				"nro_discapacita" => [ 'dataType' => 1, 'nameCookie' =>	CIndividual::UP01_NAME_CK],

			/* claves para el procedimiento 2 */
			"terreno_legal" => [ 'dataType' => 1, 'nameCookie' => CIndividual::UP02_NAME_CK],
				"terreno_fisico" => [ 'dataType' => 1, 'nameCookie' => CIndividual::UP02_NAME_CK],
				"construc_legal" => [ 'dataType' => 1, 'nameCookie' => CIndividual::UP02_NAME_CK],
				"construc_fisico" => [ 'dataType' => 1, 'nameCookie' => CIndividual::UP02_NAME_CK],
				"notaria" => [ 'dataType' => 1, 'nameCookie' => CIndividual::UP02_NAME_CK],
				"kardex" => [ 'dataType' => 1, 'nameCookie' => CIndividual::UP02_NAME_CK],
				"fecha_inscpublica" => [ 'dataType' => 1, 'nameCookie' => CIndividual::UP02_NAME_CK],
				"tpartida_reg" => [ 'dataType' => 1, 'nameCookie' => CIndividual::UP02_NAME_CK],
				"numeros" => [ 'dataType' => 1, 'nameCookie' => CIndividual::UP02_NAME_CK],
				"forja" => [ 'dataType' => 1, 'nameCookie' => CIndividual::UP02_NAME_CK],
				"asiento" => [ 'dataType' => 1, 'nameCookie' => CIndividual::UP02_NAME_CK],
				"fecha_insc_enregpredio" => [ 'dataType' => 1, 'nameCookie' => CIndividual::UP02_NAME_CK],
				"decla_fabrica" => [ 'dataType' => 1, 'nameCookie' => CIndividual::UP02_NAME_CK],
				"inscfabrica" => [ 'dataType' => 1, 'nameCookie' => CIndividual::UP02_NAME_CK],
				"fecha_inscfabrica" => [ 'dataType' => 1, 'nameCookie' => CIndividual::UP02_NAME_CK],
				"evalpredio" => [ 'dataType' => 1, 'nameCookie' => CIndividual::UP02_NAME_CK],
				"enlote_colin" => [ 'dataType' => 1, 'nameCookie' => CIndividual::UP02_NAME_CK],
				"enjardin_aislam" => [ 'dataType' => 1, 'nameCookie' => CIndividual::UP02_NAME_CK],
				"enarea_public" => [ 'dataType' => 1, 'nameCookie' => CIndividual::UP02_NAME_CK],
				"enarea_intang" => [ 'dataType' => 1, 'nameCookie' => CIndividual::UP02_NAME_CK],
				"cond_declara" => [ 'dataType' => 1, 'nameCookie' => CIndividual::UP02_NAME_CK],
				"estado_llenado" => [ 'dataType' => 1, 'nameCookie' => CIndividual::UP02_NAME_CK],
				"nro_habitantes" => [ 'dataType' => 1, 'nameCookie' => CIndividual::UP02_NAME_CK],
				"nro_familias" => [ 'dataType' => 1, 'nameCookie' => CIndividual::UP02_NAME_CK],
				"mantenimiento" => [ 'dataType' => 1, 'nameCookie' => CIndividual::UP02_NAME_CK],
				"observaciones" => [ 'dataType' => 1, 'nameCookie' => CIndividual::UP02_NAME_CK],
				"declarante" => [ 'dataType' => 1, 'nameCookie' => CIndividual::UP02_NAME_CK],
				"dni_declarante" => [ 'dataType' => 1, 'nameCookie' => CIndividual::UP02_NAME_CK],
				"fecha_declarante" => [ 'dataType' => 1, 'nameCookie' => CIndividual::UP02_NAME_CK],
				"tec_catastral" => [ 'dataType' => 1, 'nameCookie' => CIndividual::UP02_NAME_CK],
				"cod_tec_catastral" => [ 'dataType' => 1, 'nameCookie' => CIndividual::UP02_NAME_CK],
				"fecha_tec_catastral" => [ 'dataType' => 1, 'nameCookie' => CIndividual::UP02_NAME_CK],
				"supervisor" => [ 'dataType' => 1, 'nameCookie' => CIndividual::UP02_NAME_CK],
				"cod_supervisor" => [ 'dataType' => 1, 'nameCookie' => CIndividual::UP02_NAME_CK],
				"fecha_supervisor" => [ 'dataType' => 1, 'nameCookie' => CIndividual::UP02_NAME_CK],
				"verif_catastral" => [ 'dataType' => 1, 'nameCookie' => CIndividual::UP02_NAME_CK],
				"cod_verif_catastral" => [ 'dataType' => 1, 'nameCookie' => CIndividual::UP02_NAME_CK],
				"fecha_verif_catastral" => [ 'dataType' => 1, 'nameCookie' => CIndividual::UP02_NAME_CK],
				"cod_usuario" => [ 'dataType' => 1, 'nameCookie' => CIndividual::UP02_NAME_CK],
				"tec_calidad" => [ 'dataType' => 1, 'nameCookie' => CIndividual::UP02_NAME_CK],
				"cod_tec_calidad" => [ 'dataType' => 1, 'nameCookie' => CIndividual::UP02_NAME_CK],
				"fecha_tec_calidad" => [ 'dataType' => 1, 'nameCookie' =>	CIndividual::UP02_NAME_CK]
		];

		return isset($keys[$key]) ? $keys[$key] : NULL;
	}

}