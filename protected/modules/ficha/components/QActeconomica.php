<?php 
/**
 * Clase que se encarga de interactuar con la base de datos, para el manejo de los datos de la ficha de actividad económica.
 * 
 * @author José Alejandro Vilchez Moreno <joalvm@gmail.com>
 * @package Catastro\modules\ficha\components
 */
class QActeconomica {

	/**
	 * Obtiene de la db toda las actividades guardadas
	 * @return array Lista con el resultado de la busqueda
	 */
	public static function getActividades()
	{
		$sql = "SELECT 
							COD_ACTIVIDAD codigo, 
							DESC_ACTIVIDAD actividad 
						FROM 
							ACTIVIDADES 
						ORDER BY DESC_ACTIVIDAD";

		$command = Yii::app()->db->createCommand($sql);

		$table = $command->queryAll();

		return $table;
	}

	/**
	 * Obtiene de la db todas las actividades que pertenecen a una ficha
	 * @param  string $id ID de ficha
	 * @return array     Lista con el resultado de la busqueda
	 */
	public static function getActividadesFicha($id) 
	{
		$sql = "SELECT 
							lic.COD_ACTIVIDAD codigo, 
							act.DESC_ACTIVIDAD nombre 
						FROM 
							LICENCIAS_FUNCIONAMIENTO lic, 
							ACTIVIDADES act 
						WHERE lic.COD_ACTIVIDAD = act.COD_ACTIVIDAD 
						AND id_ficha = :id_ficha";

		$command = Yii::app()->db->createCommand($sql);

		$command->bindValue(':id_ficha', $id, PDO::PARAM_STR);

		$table = $command->queryAll();

		return $table;
	}

	/**
	 * Ejecuta procedimientos almacenados para las acciones de Creación y eliminación de actividades.
	 * @param  object $params Toda la información con los datos a minipular.
	 * @return boolean         Confirmación de la acción
	 */
	public static function mantenimientoActividad($params)
	{
		$sql = 'EXEC ';

		switch ($params['action']) {
			case 'create':

				$sql .= "INSERT_GIRO_ACT_ECO :m_id_ficha, :m_cod_actividad";

				$command = Yii::app()->db->createCommand($sql);

				$command->bindValue(":m_id_ficha", $params['ficha'], PDO::PARAM_STR);
				$command->bindValue(":m_cod_actividad", $params['id'], PDO::PARAM_STR);
				
				break;
			case 'delete':

				$sql .= "DELETE_GIRO_ACT_ECO :m_id_ficha, :m_cod_actividad";

				$command = Yii::app()->db->createCommand($sql);

				$command->bindValue(":m_id_ficha", $params['ficha'], PDO::PARAM_STR);
				$command->bindValue(":m_cod_actividad", $params['id'], PDO::PARAM_STR);

				break;
		}

		$result  = $command->execute();

		return $result;
	}

	/**
	 * Obtiene de la db la lista de anuncios registrados
	 * @return array Lista con el resultado de la busqueda
	 */
	public static function getAnuncios()
	{
		$sql = "SELECT 
							COD_ANUNCIO codigo, 
							DESC_ANUNCIO anuncio 
						FROM 
							ANUNCIOS 
						ORDER BY COD_ANUNCIO";

		$command = Yii::app()->db->createCommand($sql);

		$table = $command->queryAll();

		return $table;
	}

	/**
	 * Obtiene la lista filtrada de los anuncios
	 * @param  string $code Codigo de anuncio
	 * @return object       Datos del anuncio filtrado
	 */
	public static function filterAnuncio($code) 
	{
		$sql = "SELECT 
							COD_ANUNCIO codigo, 
							DESC_ANUNCIO anuncio 
						FROM 
							ANUNCIOS 
						WHERE COD_ANUNCIO = :code";

		$command = Yii::app()->db->createCommand($sql);

		$command->bindValue(':code', $code, PDO::PARAM_STR);

		$row = $command->queryRow();

		return $row;
	}

	/**
	 * Obtiene de la db todos los anuncios de una ficha
	 * @param  string $id ID de ficha
	 * @return array     Lista con los resultados de la busqueda
	 */
	public static function getAnunciosFicha($id) 
	{
		$sql = "SELECT 
							nro, 
							aut.COD_ANUNCIO, 
							DESC_ANUNCIO, 
							NRO_LADOS, 
							AREA_AUT_ANUNCIO, 
							AREA_VER_ANUNCIO, 
							NRO_EXPEDIENTE, 
							NRO_LICENCIA, 
							FECHA_EXPEDICION, 
							FECHA_VENCIMIENTO 
						FROM 
							ANUNCIOS_ACT aut, 
							ANUNCIOS anun 
						WHERE 
								aut.COD_ANUNCIO = anun.COD_ANUNCIO 
						AND id_ficha = :id_ficha";

		$command = Yii::app()->db->createCommand($sql);

		$command->bindValue(':id_ficha', $id, PDO::PARAM_STR);

		$table = $command->queryAll();

		foreach ($table as $row => &$cell) {
			$cell = self::formater_datetime(((Object)$cell), ['FECHA_EXPEDICION', 'FECHA_VENCIMIENTO']);
		}

		return $table;
	}

	/**
	 * Ejecuta procedimientos almacenados para las acciones de Creación,
	 * modificación y eliminación de anuncios.
	 * @param  object $params Toda la información con los datos a minipular.
	 * @return boolean         Confirmación de la acción
	 */
	public static function mantenimientoAnuncio($params)
	{
		$sql = 'EXEC ';

		switch ($params['action']) {
			case 'create':
			case 'update':
				$sql .= "UPDATE_ANUNCIO_ACT_ECO :nro, :m_id_ficha, :m_cod_anuncio, :m_nro_lados, :m_area_aut_anuncio, 
								:m_area_ver_anuncio, :m_nro_expediente, :m_nro_licencia, :m_fecha_expedicion, :m_fecha_vencimiento";

				$command = Yii::app()->db->createCommand($sql);

				$command->bindValue(':nro', $params['nro'], PDO::PARAM_INT);
				$command->bindValue(':m_id_ficha', $params['ficha'], PDO::PARAM_STR);
				$command->bindValue(':m_cod_anuncio', $params['codanuncio'], PDO::PARAM_STR);
				$command->bindValue(':m_nro_lados', $params['nro_lados'], PDO::PARAM_INT);
				$command->bindValue(':m_area_aut_anuncio', $params['area_autorizada'], PDO::PARAM_STR);
				$command->bindValue(':m_area_ver_anuncio', $params['area_verificada'], PDO::PARAM_STR);
				$command->bindValue(':m_nro_expediente', mb_strtoupper($params['nro_expediente'], 'UTF-8'), PDO::PARAM_STR);
				$command->bindValue(':m_nro_licencia', mb_strtoupper($params['nro_licencia'], 'UTF-8'), PDO::PARAM_STR);
				$command->bindValue(':m_fecha_expedicion', ( ! empty($params['f_expedicion']) ) ? Fecha::format($params['f_expedicion'], 'd/m/Y', 'Y-m-d') : '1900-01-01', PDO::PARAM_STR);
				$command->bindValue(':m_fecha_vencimiento', ( ! empty($params['f_vencimiento']) ) ? Fecha::format($params['f_vencimiento'], 'd/m/Y', 'Y-m-d') : '1900-01-01', PDO::PARAM_STR);
				
				break;
			case 'delete':

				$sql .= "DELETE_ANUNCIO_ACT_ECO :m_id_ficha, :m_cod_anuncio, :nro";

				$command = Yii::app()->db->createCommand($sql);

				$command->bindValue(":m_id_ficha", $params['ficha'], PDO::PARAM_STR);
				$command->bindValue(":m_cod_anuncio", $params['codanuncio'], PDO::PARAM_STR);
				$command->bindValue(":nro", $params['nro'], PDO::PARAM_INT);

				break;
		}

		$result  = $command->execute();

		return $result;
	}

	/**
	 * Primera paso en la acción de guardar datos de la ficha
	 * @return boolean Confirmación de la acción
	 */
	public static function guardarDatosFicha_step_01($curr_ubigeo) 
	{
		$via = "";
		$HU = "";
		$SP = "";

		$head = UActeconomica::getCookie(CActeconomica::HEAD_NAME_CK);
		$up 	= UActeconomica::getCookie(CActeconomica::UP01_NAME_CK);

		$sql = "EXEC ";

		if ( $curr_ubigeo == $up->ubigeo ) {
			$sql .= 'UPDATE_ACT_ECO_LOCAL ';
			$HU = ':m_cod_hab_urb';
			$via = ':m_cod_via';
		} else {
			$sql .= 'UPDATE_ACT_ECO_FORANEO ';
			$HU = ':m_hab_urb';
			$via = ':m_via';
		}

		$sql .= ":m_id_ficha, :m_id_codcat, :m_cuc, :m_chc, :m_fichas_lote, :m_id_persona, :m_nom_comercial, :m_cond_conductor_activ, 
						:m_RUC, :m_ubigeo, :m_telefono, :m_anexo, :m_fax, :m_mail, {$via}, :m_nro_municipal, :m_nom_edifica, :m_nro_int, {$HU}, 
						:m_mza_urbana, :m_lote_urbano, :m_sub_lote, :m_area_aut_predcat, :m_area_aut_viapub, :m_area_aut_bc, :m_area_aut_total, 
						:m_area_ver_predcat, :m_area_ver_viapub, :m_area_ver_bc, :m_area_ver_total, :m_nro_expediente, :m_nro_licencia, 
						:m_fecha_exped, :m_fecha_vencim, :m_inicio_activ, :m_cond_declarante, :m_dctos_presentados, :m_est_llenado, 
						:m_mantenimiento, :m_observaciones, :m_firma_dec, :m_id_dec, :m_fecha_dec, :m_firma_tec, :m_id_tec, :m_fecha_tec, 
						:m_firma_sup, :m_id_sup, :m_fecha_sup, :m_firma_ver, :m_id_ver, :m_fecha_ver, :m_insert_digitador, :firma_tec_calidad, 
						:id_tec_calidad, :fecha_tec_calidad";

		$nro_ficha_lote = (isset($head->nro_ficha_lote_first) ? $head->nro_ficha_lote_first : '') . '/' ;
		$nro_ficha_lote .= (isset($head->nro_ficha_lote_second) ? $head->nro_ficha_lote_second : '');

		$command = Yii::app()->db->createCommand($sql);

		//cabecera
		$command->bindValue(':m_id_ficha', $head->id_ficha, PDO::PARAM_STR);
		$command->bindValue(':m_id_codcat', $head->cod_catastro, PDO::PARAM_STR);
		$command->bindValue(':m_cuc', '', PDO::PARAM_STR);
		$command->bindValue(':m_chc', isset($head->cod_hoja_catastral) ? $head->cod_hoja_catastral : '', PDO::PARAM_STR);
		$command->bindValue(':m_fichas_lote', $nro_ficha_lote, PDO::PARAM_STR);

		//cuerpo
		$command->bindValue(':m_id_persona', ( isset($up->id_persona) ? $up->id_persona : '' ), PDO::PARAM_STR);
			$command->bindValue(':m_nom_comercial', ( isset($up->comercial) ? $up->comercial : '' ), PDO::PARAM_STR);
			$command->bindValue(':m_cond_conductor_activ', ( isset($up->cond_conductor) ? $up->cond_conductor : '' ), PDO::PARAM_STR);
			$command->bindValue(':m_RUC', ( isset($up->ruc) ? $up->ruc : '' ), PDO::PARAM_STR);
			$command->bindValue(':m_ubigeo', ( isset($up->ubigeo) ? $up->ubigeo : '' ), PDO::PARAM_STR);
			$command->bindValue(':m_telefono', ( isset($up->telefono) ? $up->telefono : '' ), PDO::PARAM_STR);
			$command->bindValue(':m_anexo', ( isset($up->anexo) ? $up->anexo : '' ), PDO::PARAM_STR);
			$command->bindValue(':m_fax', ( isset($up->fax) ? $up->fax : '' ), PDO::PARAM_STR);
			$command->bindValue(':m_mail', ( isset($up->email) ? $up->email : '' ), PDO::PARAM_STR);
			$command->bindValue($via, ( isset($up->via) ? $up->via : '' ), PDO::PARAM_STR);
			$command->bindValue(':m_nro_municipal', ( isset($up->nro_municipalidad) ? $up->nro_municipalidad : '' ), PDO::PARAM_STR);
			$command->bindValue(':m_nom_edifica', ( isset($up->nombre_edificio) ? $up->nombre_edificio : '' ), PDO::PARAM_STR);
			$command->bindValue(':m_nro_int', ( isset($up->nro_interior) ? $up->nro_interior : '' ), PDO::PARAM_STR);
			$command->bindValue($HU, ( isset($up->hurbana) ? $up->hurbana : '' ), PDO::PARAM_STR);
			$command->bindValue(':m_mza_urbana', ( isset($up->manzana) ? $up->manzana : '' ), PDO::PARAM_STR);
			$command->bindValue(':m_lote_urbano', ( isset($up->lote) ? $up->lote : '' ), PDO::PARAM_STR);
			$command->bindValue(':m_sub_lote', ( isset($up->sublote) ? $up->sublote : '' ), PDO::PARAM_STR);

		$command->bindValue(':m_area_aut_predcat', ( isset($up->predio_autorizada) ? $up->predio_autorizada : '' ), PDO::PARAM_STR);
			$command->bindValue(':m_area_aut_viapub', ( isset($up->via_autorizada) ? $up->via_autorizada : '' ), PDO::PARAM_STR);
			$command->bindValue(':m_area_aut_bc', ( isset($up->bien_autorizada) ? $up->bien_autorizada : '' ), PDO::PARAM_STR);
			$command->bindValue(':m_area_aut_total', ( isset($up->total_autorizada) ? $up->total_autorizada : '' ), PDO::PARAM_STR);
			$command->bindValue(':m_area_ver_predcat', ( isset($up->predio_verificada) ? $up->predio_verificada : '' ), PDO::PARAM_STR);
			$command->bindValue(':m_area_ver_viapub', ( isset($up->via_verificada) ? $up->via_verificada : '' ), PDO::PARAM_STR);
			$command->bindValue(':m_area_ver_bc', ( isset($up->bien_verificada) ? $up->bien_verificada : '' ), PDO::PARAM_STR);
			$command->bindValue(':m_area_ver_total', ( isset($up->total_verificada) ? $up->total_verificada : '' ), PDO::PARAM_STR);
			$command->bindValue(':m_nro_expediente', ( isset($up->nro_expediente) ? $up->nro_expediente : '' ), PDO::PARAM_STR);
			$command->bindValue(':m_nro_licencia', ( isset($up->nro_licencia) ? $up->nro_licencia : '' ), PDO::PARAM_STR);
			$command->bindValue(':m_fecha_exped', ( isset($up->f_expedicion) ? Fecha::format($up->f_expedicion, 'd/m/Y', 'Y-m-d') : '1900-01-01' ), PDO::PARAM_STR);
			$command->bindValue(':m_fecha_vencim', ( isset($up->f_vencimiento) ? Fecha::format($up->f_vencimiento, 'd/m/Y', 'Y-m-d') : '1900-01-01' ), PDO::PARAM_STR);
			$command->bindValue(':m_inicio_activ', ( isset($up->f_inicio) ? Fecha::format($up->f_inicio, 'd/m/Y', 'Y-m-d') : '1900-01-01' ), PDO::PARAM_STR);

		$command->bindValue(':m_cond_declarante', ( isset($up->cond_declara) ? $up->cond_declara : '' ), PDO::PARAM_STR);
			$command->bindValue(':m_dctos_presentados', ( isset($up->docpresentados) ? $up->docpresentados : '' ), PDO::PARAM_STR);
			$command->bindValue(':m_est_llenado', ( isset($up->estado_llenado) ? $up->estado_llenado : '' ), PDO::PARAM_STR);
			$command->bindValue(':m_mantenimiento', ( isset($up->mantenimiento) ? $up->mantenimiento : '' ), PDO::PARAM_STR);
			$command->bindValue(':m_observaciones', ( isset($up->observaciones) ? mb_strtoupper($up->observaciones, 'UTF-8') : '' ), PDO::PARAM_STR);
			$command->bindValue(':m_firma_dec', ( isset($up->declarante) ? $up->declarante : '0' ), PDO::PARAM_STR);
			$command->bindValue(':m_id_dec', ( isset($up->dni_declarante) ? $up->dni_declarante : '' ), PDO::PARAM_STR);
			$command->bindValue(':m_fecha_dec', ( isset($up->fecha_declarante) ? Fecha::format($up->fecha_declarante, 'd/m/Y', 'Y-m-d') : '1900-01-01' ), PDO::PARAM_STR);
			$command->bindValue(':m_firma_tec', ( isset($up->tec_catastral) ? $up->tec_catastral : '0' ), PDO::PARAM_STR);
			$command->bindValue(':m_id_tec', ( isset($up->cod_tec_catastral) ? $up->cod_tec_catastral : '' ), PDO::PARAM_STR);
			$command->bindValue(':m_fecha_tec', ( isset($up->fecha_tec_catastral) ? Fecha::format($up->fecha_tec_catastral, 'd/m/Y', 'Y-m-d') : '1900-01-01' ), PDO::PARAM_STR);
			$command->bindValue(':m_firma_sup', ( isset($up->supervisor) ? $up->supervisor : '0' ), PDO::PARAM_STR);
			$command->bindValue(':m_id_sup', ( isset($up->cod_supervisor) ? $up->cod_supervisor : '' ), PDO::PARAM_STR);
			$command->bindValue(':m_fecha_sup', ( isset($up->fecha_supervisor) ? Fecha::format($up->fecha_supervisor, 'd/m/Y', 'Y-m-d') : '1900-01-01' ), PDO::PARAM_STR);
			$command->bindValue(':m_firma_ver', ( isset($up->verif_catastral) ? $up->verif_catastral : '0' ), PDO::PARAM_STR);
			$command->bindValue(':m_id_ver', ( isset($up->cod_verif_catastral) ? $up->cod_verif_catastral : '' ), PDO::PARAM_STR);
			$command->bindValue(':m_fecha_ver', ( isset($up->fecha_verif_catastral) ? Fecha::format($up->fecha_verif_catastral, 'd/m/Y', 'Y-m-d') : '1900-01-01' ), PDO::PARAM_STR);
			$command->bindValue(':m_insert_digitador', Yii::app()->user->id_persona_catas, PDO::PARAM_STR);
			$command->bindValue(':firma_tec_calidad', ( isset($up->tec_calidad) ? $up->tec_calidad : '0' ), PDO::PARAM_STR);
			$command->bindValue(':id_tec_calidad', ( isset($up->cod_tec_calidad) ? $up->cod_tec_calidad : '' ), PDO::PARAM_STR);
			$command->bindValue(':fecha_tec_calidad', ( isset($up->fecha_tec_calidad) ? Fecha::format($up->fecha_tec_calidad, 'd/m/Y', 'Y-m-d') : '1900-01-01' ), PDO::PARAM_STR);
		//echo "<pre>"; print_r($command); exit();
		$result  = $command->execute();

		return $result;
	}

	/**
	 * Obtiene toda la información de la ficha para el proceso de modificación
	 * @param  string $id_ficha ID de ficha
	 * @return object           objecto formateado con la información obtenida
	 */
	public static function get_data_ficha($id_ficha)
	{
		$sql = "SELECT 
							f.ID_FICHA id_ficha,
							f.COD_CAT cod_catastro,
							f.DC dc,
							f.NRO_FICHA nro_ficha,
							ISNULL(f.CUC,'') cuc,
							f.COD_HOJA_CAT cod_hoja_catastral,
							f.NRO_FICHA_LOTE nro_ficha_lote,
							ISNULL(f.CONDIC_DECLARA , '') cond_declara,
							ISNULL(f.ESTADO_LLENADO, '') estado_llenado,
							fc.OBSERVACIONES observaciones,
							fc.NOMBRE_COMERCIAL,
							NRO_LICENCIA,
							NRO_EXPEDIENTE,
							fc.AREA_VER_TOTAL,
							AREA_VER_BC,
							AREA_VER_PREDCAT,
							AREA_VER_VIAPUB,
							AREA_AUT_TOTAL,
							AREA_AUT_BC,
							AREA_AUT_VIAPUB,
							AREA_AUT_PREDCAT,
							FECHA_EXPEDICION,
							FECHA_VENCIMIENTO,
							INICIO_ACTIVIDAD,
							ISNULL(DCTOS_PRESENTADOS, '') DCTOS_PRESENTADOS,
							ISNULL(MANTENIMIENTO , '' ) MANTENIMIENTO,
							cf.FIRMA_DECLARANTE,
							cf.ID_DECLARANTE,
							cf.FECHA_DECLARANTE,
							cf.FIRMA_TEC_CATASTRAL,
							cf.ID_TEC_CATASTRAL,
							cf.FECHA_TEC_CATASTRAL,
							cf.FIRMA_SUPERVISOR,
							cf.ID_SUPERVISOR,
							cf.FECHA_SUPERVISOR ,
							cf.FIRMA_VERIF_CATASTRAL ,
							cf.ID_VERIF_CATASTRAL ,
							cf.FECHA_VERIF_CATASTRAL,
							cf.FIRMA_TEC_CALIDAD ,
							cf.ID_TEC_CALIDAD,
							cf.FECHA_TEC_CALIDAD,
							c.ID_PERSONA,
							NOMBRES CONDUCTOR,
							c.NOMBRE_COMERCIAL,
							via nom_via,
							c.CONDICION,
							c.NRO_RUC_2,
							*
						FROM 
							FICHAS f, 
							CONTROL_DE_FICHA cf,
							FICHAS_ECONOMICAS fc,
							ECO_CONDUCTOR c,
							TODOS_DIRECCION d
						WHERE 
							f.tip_ficha = '03'
						AND f.id_ficha = fc.id_ficha 
						AND cf.ID_FICHA = f.ID_FICHA
						AND c.ID_FICHA = f.ID_FICHA
						AND d.ID_FICHA = f.ID_FICHA
						AND f.id_ficha = :id_ficha";

		$command = Yii::app()->db->createCommand($sql);

		$command->bindValue(":id_ficha", $id_ficha, PDO::PARAM_STR);

		$row = $command->queryRow();

		if ( ! $row ) {
			return [];
		}

		$nrow = new stdClass;
		$head = new stdClass;
		$up01 = new stdClass;

		$head->nro_ficha = trim($row['nro_ficha']);
		$head->id_ficha = trim($row['id_ficha']);
		$head->cod_catastro = trim($row['cod_catastro']);
		$head->cod_hoja_catastral = trim($row['cod_hoja_catastral']);

		$lotes_arr = explode('/', $row['nro_ficha_lote']);
		$head->nro_ficha_lote_first = (count($lotes_arr) == 2 OR count($lotes_arr) == 1) ? $lotes_arr[0] : '';
		$head->nro_ficha_lote_second = (count($lotes_arr) == 2) ? $lotes_arr[1] : '';

		$head->cod_ref_catastral = new stdClass;

		$head->cod_ref_catastral->ubigeo = mb_substr($row['cod_catastro'], 0, 6);
    $head->cod_ref_catastral->sector = mb_substr($row['cod_catastro'], 10, 2);
    $head->cod_ref_catastral->manzana = mb_substr($row['cod_catastro'], 12, 3);
    $head->cod_ref_catastral->lote = mb_substr($row['cod_catastro'], 15, 3);
    $head->cod_ref_catastral->edificio = mb_substr($row['cod_catastro'], 18, 2);
    $head->cod_ref_catastral->entrada = mb_substr($row['cod_catastro'], 20, 2);
    $head->cod_ref_catastral->piso = mb_substr($row['cod_catastro'], 22, 2);
    $head->cod_ref_catastral->unidad = mb_substr($row['cod_catastro'], 24, 3);
    $head->cod_ref_catastral->dc = $row['dc'];

    $up01->id_persona = $row['ID_PERSONA'];
		$up01->comercial = $row['NOMBRE_COMERCIAL'];
		$up01->cond_conductor = $row['CONDICION'];
		$up01->ruc = $row['NRO_RUC_2'];
		$up01->ubigeo = $row['ID_UBI_GEO'];
		$up01->telefono = $row['TELEFONO'];
		$up01->anexo = $row['ANEXO'];
		$up01->fax = $row['FAX'];
		$up01->email = $row['CORREO_ELECT'];
		$up01->via = empty($row['cod_via']) ? $row['via'] : $row['cod_via'];
		$up01->nro_municipalidad = $row['NRO_MUNICIPAL'];
		$up01->nombre_edificio = $row['NOMBRE_EDIFICA'];
		$up01->nro_interior = $row['NRO_INTERIOR'];
		$up01->hurbana = empty($row['COD_HAB_URB']) ? $row['NOM_HAB_URB'] : $row['COD_HAB_URB'];
		$up01->manzana = $row['MZA_URBANA'];
		$up01->lote = $row['LOTE_URBANO'];
		$up01->sublote = $row['SUB_LOTE'];
		$up01->predio_autorizada = $row['AREA_AUT_PREDCAT'];
		$up01->via_autorizada = $row['AREA_AUT_VIAPUB'];
		$up01->bien_autorizada = $row['AREA_AUT_BC'];
		$up01->total_autorizada = $row['AREA_AUT_TOTAL'];
		$up01->predio_verificada = $row['AREA_VER_PREDCAT'];
		$up01->via_verificada = $row['AREA_VER_VIAPUB'];
		$up01->bien_verificada = $row['AREA_VER_BC'];
		$up01->total_verificada = $row['AREA_VER_TOTAL'];
		$up01->nro_expediente = $row['NRO_EXPEDIENTE'];
		$up01->nro_licencia = $row['NRO_LICENCIA'];
		$up01->f_expedicion = $row['FECHA_EXPEDICION'];
		$up01->f_vencimiento = $row['FECHA_VENCIMIENTO'];
		$up01->f_inicio = $row['INICIO_ACTIVIDAD'];
		$up01->cond_declara = $row['cond_declara'];
		$up01->docpresentados = $row['DCTOS_PRESENTADOS'];
		$up01->estado_llenado = $row['estado_llenado'];
		$up01->mantenimiento = $row['MANTENIMIENTO'];
		$up01->observaciones = $row['OBSERVACIONES'];
		$up01->declarante = $row['FIRMA_DECLARANTE'];
		$up01->dni_declarante = $row['ID_DECLARANTE'];
		$up01->fecha_declarante = $row['FECHA_DECLARANTE'];
		$up01->tec_catastral = $row['FIRMA_TEC_CATASTRAL'];
		$up01->cod_tec_catastral = $row['ID_TEC_CATASTRAL'];
		$up01->fecha_tec_catastral = $row['FECHA_TEC_CATASTRAL'];
		$up01->supervisor = $row['FIRMA_SUPERVISOR'];
		$up01->cod_supervisor = $row['ID_SUPERVISOR'];
		$up01->fecha_supervisor = $row['FECHA_SUPERVISOR'];
		$up01->verif_catastral = $row['FIRMA_VERIF_CATASTRAL'];
		$up01->cod_verif_catastral = $row['ID_VERIF_CATASTRAL'];
		$up01->fecha_verif_catastral = $row['FECHA_VERIF_CATASTRAL'];
		$up01->tec_calidad = $row['FIRMA_TEC_CALIDAD'];
		$up01->cod_tec_calidad = $row['ID_TEC_CALIDAD'];
		$up01->fecha_tec_calidad = $row['FECHA_TEC_CALIDAD'];

    $nrow->head = $head;

    $up01 = self::formater_datetime($up01, [
    	'fecha_declarante', 
    	'fecha_supervisor', 
    	'fecha_tec_catastral', 
    	'fecha_tec_calidad', 
    	'fecha_verif_catastral',
    	'f_inicio',
    	'f_vencimiento',
    	'f_expedicion'
    ]);

    $nrow->up01 = $up01;

		return $nrow;
	}

	/**
	 * Obtiene de la db la información completa de las personas registradas
	 * @param  string $stype Tipo de persona
	 * @param  string $doc   Numero de documento
	 * @return object        Datos de la persona
	 */
	public static function getInfoPersona($stype, $doc)
	{
		$sql = "SELECT 
							p.ID_PERSONA id_persona, 
							p.NRO_DOC nro_doc, 
							p.TIP_DOC tipo_doc, 
							p.TIP_PERSONA tipo_persona,
							p.NOMBRES nombres,
							p.APE_PATERNO ape_paterno,
							p.APE_MATERNO ape_materno,
							(p.APE_PATERNO + ' ' + p.APE_MATERNO + ', ' + p.NOMBRES) nombre_completo,
							p.TIP_PERSONA_JURIDICA tipo_persona_juridica,
							p.NRO_RUC_2 nro_ruc,
							p.ESTADO_CIVIL estado_civil
						FROM 
							dbo.PERSONAS p 
						WHERE ";

		$sql .= ($stype == 'id') ? 'p.ID_PERSONA = :doc' : 'p.NRO_DOC = :doc';

		$command = Yii::app()->db->createCommand($sql);

		$command->bindValue(':doc', $doc, PDO::PARAM_STR);

		$row  = $command->queryRow();

		$data = [];

		if ( ! $row ) {
			$data['data'] = [];
		} else {
			$data['data'] = [$row];
		}

		return $data;
	}

	/**
	 * Permite formatear la fecha en formato DD/MM/YYYY.
	 *
	 * La función compara la fecha si la fecha es de 1900 la deja vacía caso contrario
	 * aplica el formateo dejando solo el dia, mes y año en el formato DD/MM/YYYY.
	 * @param  object $up_obj       sección del objeto que buscará la información.
	 * @param  array $datekeys_arr Array con las claves que contienen fecha
	 * @return object               El mismo objeto con las fechas formateadas
	 */
	private static function formater_datetime($up_obj, $datekeys_arr)
	{
		foreach ($datekeys_arr as $key) 
		{
			$dateval = new Datetime($up_obj->{$key});

			if ( $dateval->format('Y') != '1900' ) {
				$up_obj->{$key} = $dateval->format('d/m/Y');
			} else {
				unset($up_obj->{$key});
			}

		}

		return $up_obj;
	}
	
}