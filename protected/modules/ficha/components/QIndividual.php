<?php 

/**
 * Clase que se encarga de interactuar con la base de datos, para el manejo de los datos de la ficha individual.
 * 
 * @author José Alejandro Vilchez Moreno <joalvm@gmail.com>
 * @package Catastro\modules\ficha\components
 */
class QIndividual {

	/**
	 * Primera paso en la acción de guardar datos de la ficha
	 * @return boolean Confirmación de la acción
	 */
	public static function guardarDatosFicha_step_01() 
	{
		$sql = "EXEC UPDATE_INDIVIDUAL_1  :m_id_ficha, :m_id_codcat, :m_cuc, :m_chc, :m_fichas_lote, 
					 :m_cod_pred_ren, :m_cod_pred_acum, :m_cod_hab_urb, :m_mza_urb, :m_lot_urb, :m_sub_lt, 
					 :m_edifica, :m_tip_edifica, :m_tip_int, :m_int, :m_cond_titular, :m_cond_esp_pred, 
					 :m_nro_resol, :m_porcent, :m_fec_inicio, :m_fec_venc, :m_clasific, :m_pred_cat_en, 
					 :m_cod_uso, :m_estruct, :m_zonif, :m_area_titulo, :m_area_declar, :m_area_verif, 
					 :m_fr_me_ca, :m_de_me_ca, :m_iz_me_ca, :m_fo_me_ca, :m_fr_me_ti, :m_de_me_ti, :m_iz_me_ti, 
					 :m_fo_me_ti, :m_fr_co_ca, :m_de_co_ca, :m_iz_co_ca, :m_fo_co_ca, :m_fr_co_ti, :m_de_co_ti, 
					 :m_iz_co_ti, :m_fo_co_ti, :m_luz, :m_agua, :m_fono, :m_desague, :m_nro_luz, :m_nro_agua, 
					 :m_nro_fono, :fuente_inform, :area_ocupada, :izq_lim_fre_lot, :der_lim_fre_lot, :fon_lim_fre_lot, 
					 :fre_lim_fre_lot, :tot_lim_fre_uc, :gas, :cable, :internet, :nro_hombres, :nro_mujeres, 
					 :nro_menores, :nro_adulto_mayor, :nro_discapacit";

		$head = UIndividual::getCookie(CIndividual::HEAD_NAME_CK);
		$up = UIndividual::getCookie(CIndividual::UP01_NAME_CK);

		$nro_ficha_lote = (isset($head->nro_ficha_lote_first) ? $head->nro_ficha_lote_first : '') . '/' ;
		$nro_ficha_lote .= (isset($head->nro_ficha_lote_second) ? $head->nro_ficha_lote_second : '');

		$command = Yii::app()->db->createCommand($sql);

		//cabecera
		$command->bindValue(':m_id_ficha', $head->id_ficha, PDO::PARAM_STR);
		$command->bindValue(':m_id_codcat', $head->cod_catastro, PDO::PARAM_STR);
		$command->bindValue(':m_cuc', '', PDO::PARAM_STR);
		$command->bindValue(':m_chc', isset($head->cod_hoja_catastral) ? $head->cod_hoja_catastral : '', PDO::PARAM_STR);
		$command->bindValue(':m_fichas_lote', $nro_ficha_lote, PDO::PARAM_STR);
		$command->bindValue(':m_cod_pred_ren', (isset($head->cod_predial_rentas) ? $head->cod_predial_rentas : ''), PDO::PARAM_STR);
		$command->bindValue(':m_cod_pred_acum', (isset($head->ua_predial_rentas) ? $head->ua_predial_rentas : ''), PDO::PARAM_STR);

		//cuerpo
		$command->bindValue(':m_cod_hab_urb', (isset($up->code_haburba) ? $up->code_haburba : ''), PDO::PARAM_STR);
		$command->bindValue(':m_mza_urb', (isset($up->manzana) ? $up->manzana : ''), PDO::PARAM_STR);
		$command->bindValue(':m_lot_urb', (isset($up->lote) ? $up->lote : ''), PDO::PARAM_STR);
		$command->bindValue(':m_sub_lt', (isset($up->sublote) ? $up->sublote : ''), PDO::PARAM_STR);
		$command->bindValue(':m_edifica', (isset($up->name_edificio) ? $up->name_edificio : ''), PDO::PARAM_STR);
		$command->bindValue(':m_tip_edifica', (isset($up->tipo_edificio) ? $up->tipo_edificio : ''), PDO::PARAM_STR);
		$command->bindValue(':m_tip_int', (isset($up->tipo_interior) ? $up->tipo_interior : ''), PDO::PARAM_STR);
		$command->bindValue(':m_int', (isset($up->nro_interior) ? $up->nro_interior : ''), PDO::PARAM_STR);
		$command->bindValue(':m_cond_titular', (isset($up->cond_titular) ? $up->cond_titular : ''), PDO::PARAM_STR);
		$command->bindValue(':m_cond_esp_pred', (isset($up->cond_especialpredio) ? $up->cond_especialpredio : ''), PDO::PARAM_STR);
		$command->bindValue(':m_nro_resol', (isset($up->nro_resolucion) ? $up->nro_resolucion : ''), PDO::PARAM_STR);
		$command->bindValue(':m_porcent', (isset($up->porcent) ? $up->porcent : ''), PDO::PARAM_STR);
		$command->bindValue(':m_fec_inicio', (isset($up->finicio) ? Fecha::format($up->finicio, 'd/m/Y', 'Y-m-d') : '1900-01-01'), PDO::PARAM_STR);
		$command->bindValue(':m_fec_venc', (isset($up->fvencimiento) ? Fecha::format($up->fvencimiento, 'd/m/Y', 'Y-m-d') : '1900-01-01'), PDO::PARAM_STR);
		$command->bindValue(':m_clasific', (isset($up->clasif_predio) ? $up->clasif_predio : ''), PDO::PARAM_STR);
		$command->bindValue(':m_pred_cat_en', (isset($up->pred_catastral) ? $up->pred_catastral : ''), PDO::PARAM_STR);
		$command->bindValue(':m_cod_uso', (isset($up->uso_predio) ? $up->uso_predio : ''), PDO::PARAM_STR);
		$command->bindValue(':m_estruct', (isset($up->estructuracion) ? $up->estructuracion : ''), PDO::PARAM_STR);
		$command->bindValue(':m_zonif', (isset($up->zonificacion) ? $up->zonificacion : ''), PDO::PARAM_STR);
		$command->bindValue(':m_area_titulo', (isset($up->area_titulo) ? $up->area_titulo : ''), PDO::PARAM_STR);
		$command->bindValue(':m_area_declar', (isset($up->area_declarada) ? $up->area_declarada : ''), PDO::PARAM_STR);
		$command->bindValue(':m_area_verif', (isset($up->area_verificada) ? $up->area_verificada : ''), PDO::PARAM_STR);
		$command->bindValue(':m_fr_me_ca', (isset($up->medcamp_frente) ? $up->medcamp_frente : ''), PDO::PARAM_STR);
		$command->bindValue(':m_de_me_ca', (isset($up->medcamp_derecha) ? $up->medcamp_derecha : ''), PDO::PARAM_STR);
		$command->bindValue(':m_iz_me_ca', (isset($up->medcamp_izquierda) ? $up->medcamp_izquierda : ''), PDO::PARAM_STR);
		$command->bindValue(':m_fo_me_ca', (isset($up->medcamp_fondo) ? $up->medcamp_fondo : ''), PDO::PARAM_STR);
		$command->bindValue(':m_fr_me_ti', (isset($up->medtit_frente) ? $up->medtit_frente : ''), PDO::PARAM_STR);
		$command->bindValue(':m_de_me_ti', (isset($up->medtit_derecha) ? $up->medtit_derecha : ''), PDO::PARAM_STR);
		$command->bindValue(':m_iz_me_ti', (isset($up->medtit_izquierda) ? $up->medtit_izquierda : ''), PDO::PARAM_STR);
		$command->bindValue(':m_fo_me_ti', (isset($up->medtit_fondo) ? $up->medtit_fondo : ''), PDO::PARAM_STR);
		$command->bindValue(':m_fr_co_ca', (isset($up->colcampo_frente) ? $up->colcampo_frente : ''), PDO::PARAM_STR);
		$command->bindValue(':m_de_co_ca', (isset($up->colcampo_derecha) ? $up->colcampo_derecha : ''), PDO::PARAM_STR);
		$command->bindValue(':m_iz_co_ca', (isset($up->colcampo_izquierda) ? $up->colcampo_izquierda : ''), PDO::PARAM_STR);
		$command->bindValue(':m_fo_co_ca', (isset($up->colcampo_fondo) ? $up->colcampo_fondo : ''), PDO::PARAM_STR);
		$command->bindValue(':m_fr_co_ti', (isset($up->coltit_frente) ? $up->coltit_frente : ''), PDO::PARAM_STR);
		$command->bindValue(':m_de_co_ti', (isset($up->coltit_derecha) ? $up->coltit_derecha : ''), PDO::PARAM_STR);
		$command->bindValue(':m_iz_co_ti', (isset($up->coltit_izquierda) ? $up->coltit_izquierda : ''), PDO::PARAM_STR);
		$command->bindValue(':m_fo_co_ti', (isset($up->coltit_fondo) ? $up->coltit_fondo : ''), PDO::PARAM_STR);
		$command->bindValue(':m_luz', (isset($up->luz) ? $up->luz : ''), PDO::PARAM_STR);
		$command->bindValue(':m_agua', (isset($up->agua) ? $up->agua : ''), PDO::PARAM_STR);
		$command->bindValue(':m_fono', (isset($up->phone) ? $up->phone : ''), PDO::PARAM_STR);
		$command->bindValue(':m_desague', (isset($up->desague) ? $up->desague : ''), PDO::PARAM_STR);
		$command->bindValue(':m_nro_luz', (isset($up->sumluz) ? $up->sumluz : ''), PDO::PARAM_STR);
		$command->bindValue(':m_nro_agua', (isset($up->sumagua) ? $up->sumagua : ''), PDO::PARAM_STR);
		$command->bindValue(':m_nro_fono', (isset($up->sumphone) ? $up->sumphone : ''), PDO::PARAM_STR);
		$command->bindValue(':fuente_inform', (isset($up->fuente_info) ? $up->fuente_info : ''), PDO::PARAM_STR);
		$command->bindValue(':area_ocupada', (isset($up->area_ocupada) ? $up->area_ocupada : ''), PDO::PARAM_STR);
		$command->bindValue(':izq_lim_fre_lot', (isset($up->limpfrente_izquierda) ? $up->limpfrente_izquierda : ''), PDO::PARAM_STR);
		$command->bindValue(':der_lim_fre_lot', (isset($up->limpfrente_derecha) ? $up->limpfrente_derecha : ''), PDO::PARAM_STR);
		$command->bindValue(':fon_lim_fre_lot', (isset($up->limpfrente_fondo) ? $up->limpfrente_fondo : ''), PDO::PARAM_STR);
		$command->bindValue(':fre_lim_fre_lot', (isset($up->limpfrente_frente) ? $up->limpfrente_frente : ''), PDO::PARAM_STR);
		$command->bindValue(':tot_lim_fre_uc', (isset($up->total_limpfrenteuc) ? $up->total_limpfrenteuc : ''), PDO::PARAM_STR);
		$command->bindValue(':gas', (isset($up->gas) ? $up->gas : '0'), PDO::PARAM_STR);
		$command->bindValue(':cable', (isset($up->cable) ? $up->cable : '0'), PDO::PARAM_STR);
		$command->bindValue(':internet', (isset($up->internet) ? $up->internet : '0'), PDO::PARAM_STR);
		$command->bindValue(':nro_hombres', (isset($up->nro_hombres) ? (int)$up->nro_hombres : 0), PDO::PARAM_STR);
		$command->bindValue(':nro_mujeres', (isset($up->nro_mujeres) ? (int)$up->nro_mujeres : 0), PDO::PARAM_STR);
		$command->bindValue(':nro_menores', (isset($up->nro_ninios) ? (int)$up->nro_ninios : 0), PDO::PARAM_STR);
		$command->bindValue(':nro_adulto_mayor', (isset($up->nro_adulmayor) ? (int)$up->nro_adulmayor : 0), PDO::PARAM_STR);
		$command->bindValue(':nro_discapacit', (isset($up->nro_discapacita) ? (int)$up->nro_discapacita : 0), PDO::PARAM_STR);
		
		$result  = $command->execute();


		$command->reset();

		return $result;
	}

	/**
	 * Segundo paso en la acción de guardar datos de la ficha
	 * @return boolean Confirmación de la acción
	 */
	public static function guardarDatosFicha_step_02() 
	{

		$sql = "EXEC UPDATE_INDIVIDUAL_2 :m_id_ficha, :m_id_codcat, :m_porc_BC_terr_leg, :m_porc_BC_terr_fis, :m_porc_BC_const_leg, :m_porc_BC_const_fis, 
					:m_id_notaria, :m_kardex, :m_fecha_escritura, :m_tipo_part_reg, :m_numero, :m_fojas, :m_asiento, :m_fecha_inscrip, 
					:m_declar_fab, :m_asiento_dec_fab, :m_fecha_inscrip_fab, :m_evaluac_predcat, :m_area_inv_lot_col, :m_area_inv_jar_ais, 
					:m_area_inv_are_pub, :m_area_inv_are_int, :m_cond_declarante, :m_est_llenado, :m_nro_habitantes, :m_nro_familias, 
					:m_mantenimiento, :m_observaciones, :m_firma_dec, :m_id_dec, :m_fecha_dec, :m_firma_tec, :m_id_tec, :m_fecha_tec, 
					:m_firma_sup, :m_id_sup, :m_fecha_sup, :m_firma_ver, :m_id_ver, :m_fecha_ver, :m_insert_digitador, :firma_tec_calidad, 
					:id_tec_calidad, :fecha_tec_calidad";

		$head = UIndividual::getCookie(CIndividual::HEAD_NAME_CK);
		$up02 = UIndividual::getCookie(CIndividual::UP02_NAME_CK);

		$command = Yii::app()->db->createCommand($sql);

		$command->bindValue(':m_id_ficha', $head->id_ficha, PDO::PARAM_STR);
		$command->bindValue(':m_id_codcat', $head->cod_catastro, PDO::PARAM_STR);

		$command->bindValue(':m_porc_BC_terr_leg', (isset($up02->terreno_legal) ? $up02->terreno_legal: '' ), PDO::PARAM_STR);
		$command->bindValue(':m_porc_BC_terr_fis', (isset($up02->terreno_fisico) ? $up02->terreno_fisico: '' ), PDO::PARAM_STR);
		$command->bindValue(':m_porc_BC_const_leg', (isset($up02->construc_legal) ? $up02->construc_legal: '' ), PDO::PARAM_STR);
		$command->bindValue(':m_porc_BC_const_fis', (isset($up02->construc_fisico) ? $up02->construc_fisico: '' ), PDO::PARAM_STR);
		$command->bindValue(':m_id_notaria', (isset($up02->notaria) ? $up02->notaria: '' ), PDO::PARAM_STR);
		$command->bindValue(':m_kardex', (isset($up02->kardex) ? $up02->kardex: '' ), PDO::PARAM_STR);
		$command->bindValue(':m_fecha_escritura', (isset($up02->fecha_inscpublica) ? Fecha::format($up02->fecha_inscpublica, 'd/m/Y', 'Y-m-d') : '1900-01-01' ), PDO::PARAM_STR);
		$command->bindValue(':m_tipo_part_reg', (isset($up02->tpartida_reg) ? $up02->tpartida_reg: '' ), PDO::PARAM_STR);
		$command->bindValue(':m_numero', (isset($up02->numeros) ? $up02->numeros: '' ), PDO::PARAM_STR);
		$command->bindValue(':m_fojas', (isset($up02->forja) ? $up02->forja: '' ), PDO::PARAM_STR);
		$command->bindValue(':m_asiento', (isset($up02->asiento) ? $up02->asiento: '' ), PDO::PARAM_STR);
		$command->bindValue(':m_fecha_inscrip', (isset($up02->fecha_insc_enregpredio) ? Fecha::format($up02->fecha_insc_enregpredio, 'd/m/Y', 'Y-m-d') : '1900-01-01' ), PDO::PARAM_STR);
		$command->bindValue(':m_declar_fab', (isset($up02->decla_fabrica) ? $up02->decla_fabrica: '' ), PDO::PARAM_STR);
		$command->bindValue(':m_asiento_dec_fab', (isset($up02->inscfabrica) ? $up02->inscfabrica: '' ), PDO::PARAM_STR);
		$command->bindValue(':m_fecha_inscrip_fab', (isset($up02->fecha_inscfabrica) ? Fecha::format($up02->fecha_inscfabrica, 'd/m/Y', 'Y-m-d') : '1900-01-01' ), PDO::PARAM_STR);
		$command->bindValue(':m_evaluac_predcat', (isset($up02->evalpredio) ? $up02->evalpredio: '' ), PDO::PARAM_STR);
		$command->bindValue(':m_area_inv_lot_col', (isset($up02->enlote_colin) ? $up02->enlote_colin: '' ), PDO::PARAM_STR);
		$command->bindValue(':m_area_inv_jar_ais', (isset($up02->enjardin_aislam) ? $up02->enjardin_aislam: '' ), PDO::PARAM_STR);
		$command->bindValue(':m_area_inv_are_pub', (isset($up02->enarea_public) ? $up02->enarea_public: '' ), PDO::PARAM_STR);
		$command->bindValue(':m_area_inv_are_int', (isset($up02->enarea_intang) ? $up02->enarea_intang: '' ), PDO::PARAM_STR);
		$command->bindValue(':m_cond_declarante', (isset($up02->cond_declara) ? $up02->cond_declara: '' ), PDO::PARAM_STR);
		$command->bindValue(':m_est_llenado', (isset($up02->estado_llenado) ? $up02->estado_llenado: '' ), PDO::PARAM_STR);
		$command->bindValue(':m_nro_habitantes', (isset($up02->nro_habitantes) ? (int)$up02->nro_habitantes: '' ), PDO::PARAM_STR);
		$command->bindValue(':m_nro_familias', (isset($up02->nro_familias) ? (int)$up02->nro_familias: '' ), PDO::PARAM_STR);
		$command->bindValue(':m_mantenimiento', (isset($up02->mantenimiento) ? $up02->mantenimiento: '' ), PDO::PARAM_STR);
		$command->bindValue(':m_observaciones', (isset($up02->observaciones) ? mb_strtoupper($up02->observaciones, 'UTF-8') : '' ), PDO::PARAM_STR);
		$command->bindValue(':m_firma_dec', (isset($up02->declarante) ? $up02->declarante: '0' ), PDO::PARAM_STR);
		$command->bindValue(':m_id_dec', (isset($up02->dni_declarante) ? $up02->dni_declarante: '' ), PDO::PARAM_STR);
		$command->bindValue(':m_fecha_dec', (isset($up02->fecha_declarante) ? Fecha::format($up02->fecha_declarante, 'd/m/Y', 'Y-m-d') : '1900-01-01' ), PDO::PARAM_STR);
		$command->bindValue(':m_firma_tec', (isset($up02->tec_catastral) ? $up02->tec_catastral: '0' ), PDO::PARAM_STR);
		$command->bindValue(':m_id_tec', (isset($up02->cod_tec_catastral) ? $up02->cod_tec_catastral: '' ), PDO::PARAM_STR);
		$command->bindValue(':m_fecha_tec', (isset($up02->fecha_tec_catastral) ? Fecha::format($up02->fecha_tec_catastral, 'd/m/Y', 'Y-m-d') : '1900-01-01' ), PDO::PARAM_STR);
		$command->bindValue(':m_firma_sup', (isset($up02->supervisor) ? $up02->supervisor: '0' ), PDO::PARAM_STR);
		$command->bindValue(':m_id_sup', (isset($up02->cod_supervisor) ? $up02->cod_supervisor: '' ), PDO::PARAM_STR);
		$command->bindValue(':m_fecha_sup', (isset($up02->fecha_supervisor) ? Fecha::format($up02->fecha_supervisor, 'd/m/Y', 'Y-m-d') : '1900-01-01' ), PDO::PARAM_STR);
		$command->bindValue(':m_firma_ver', (isset($up02->verif_catastral) ? $up02->verif_catastral: '0' ), PDO::PARAM_STR);
		$command->bindValue(':m_id_ver', (isset($up02->cod_verif_catastral) ? $up02->cod_verif_catastral: '' ), PDO::PARAM_STR);
		$command->bindValue(':m_fecha_ver', (isset($up02->fecha_verif_catastral) ? Fecha::format($up02->fecha_verif_catastral, 'd/m/Y', 'Y-m-d') : '1900-01-01' ), PDO::PARAM_STR);
		$command->bindValue(':m_insert_digitador', Yii::app()->user->id_persona_catas, PDO::PARAM_STR);
		$command->bindValue(':firma_tec_calidad', (isset($up02->tec_calidad) ? $up02->tec_calidad: '0' ), PDO::PARAM_STR);
		$command->bindValue(':id_tec_calidad', (isset($up02->cod_tec_calidad) ? $up02->cod_tec_calidad: '' ), PDO::PARAM_STR);
		$command->bindValue(':fecha_tec_calidad', (isset($up02->fecha_tec_calidad) ? Fecha::format($up02->fecha_tec_calidad, 'd/m/Y', 'Y-m-d') : '1900-01-01' ), PDO::PARAM_STR);

		$result  = $command->execute();

		/*echo "<pre>";
		print_r($result);
		print_r($command);
		exit();*/


		$command->reset();

		return $result;
	}

	/**
	 * Ejecuta procedimientos almacenados para las acciones de Creación,
	 * modificación y eliminación de direcciones.
	 * @param  object $params Toda la información con los datos a minipular.
	 * @param  string $action Clave para entender la acción que se realizará.
	 * @return boolean         Confirmación de la acción
	 */
	public static function mantenimientoDirecciones($params, $action)
	{
		$sql = 'EXEC ';

		switch ($action) {
			case 'create':
			case 'update':

				if ( $action == 'create' ) 
					$sql .= "INSERT_DIRECCIONES :m_id_ficha, :m_cod_via, :m_nro_municipal, :m_tipo_pta, :m_cond_nro, :m_certif_numeracion";
				else if ( $action == 'update' )
					$sql .= "UPDATE_DIRECCIONES :m_id_ficha, :m_nro_registro, :m_cod_via, :m_nro_municipal, :m_tipo_pta, :m_cond_nro, :m_certif_numeracion";
				else
					return FALSE;

				$command = Yii::app()->db->createCommand($sql);

				$command->bindValue(":m_id_ficha", $params['ficha'] , PDO::PARAM_STR);

				if ( $action == 'update' )
					$command->bindValue(":m_nro_registro", $params['rowID'] , PDO::PARAM_STR);

				$command->bindValue(":m_cod_via", $params['via'] , PDO::PARAM_STR);
				$command->bindValue(":m_nro_municipal", $params['nro_municipal'] , PDO::PARAM_STR);
				$command->bindValue(":m_tipo_pta", $params['tipo_puerta'] , PDO::PARAM_STR);
				$command->bindValue(":m_cond_nro", $params['cond_num'] , PDO::PARAM_STR);
				$command->bindValue(":m_certif_numeracion", $params['nro_certificado'] , PDO::PARAM_STR);
				
				break;
			case 'delete':

				$sql .= "DELETE_DIRECCIONES :m_id_ficha, :m_nro_registro";

				$command = Yii::app()->db->createCommand($sql);

				$command->bindValue(":m_id_ficha", $params['ficha'], PDO::PARAM_STR);
				$command->bindValue(":m_nro_registro", $params['rowID'], PDO::PARAM_STR);

				break;
		}

		$result  = $command->execute();

		return $result;
	}

	/**
	 * Ejecuta procedimientos almacenados para las acciones de Creación,
	 * modificación y eliminación de documentos.
	 * @param  object $params Toda la información con los datos a minipular.
	 * @param  string $action Clave para entender la acción que se realizará.
	 * @return boolean         Confirmación de la acción
	 */
	public static function mantenimientoDocumentos($params, $action) 
	{
		$sql = 'EXEC ';

		switch ($action) {
			case 'create':
			case 'update':

				if ( $action == 'create' ) 
					$sql .= "INSERT_DCTOS_PRESENTADOS ";
				else if ( $action == 'update' )
					$sql .= "UPDATE_DCTOS_PRESENTADOS ";
				else
					return FALSE;

				$sql .= ':m_id_ficha, :m_tip_doc, :m_nro_doc, :m_fecha, :m_area_autorizada, :area_const_P01, :area_const_P02, :area_const_P03, ';
				$sql .= ':area_const_P04, :area_const_P05, :area_const_P06, :area_const_otro, :area_const_sot, :area_const_mez';

				$command = Yii::app()->db->createCommand($sql);

				$command->bindValue(":m_id_ficha", $params['ficha'] , PDO::PARAM_STR);
				$command->bindValue(":m_tip_doc", $params['tipo_doc'] , PDO::PARAM_STR);
				$command->bindValue(":m_nro_doc", $params['nro_documento'] , PDO::PARAM_STR);
				$command->bindValue(":m_fecha", ( ! empty($params['fecha_inicio']) ) ? Fecha::format($params['fecha_inicio'], 'd/m/Y', 'Y-m-d') : '1900-01-01', PDO::PARAM_STR);
				$command->bindValue(":m_area_autorizada", $params['area_autorizada'] , PDO::PARAM_STR);
				$command->bindValue(":area_const_P01", $params['piso1'] , PDO::PARAM_STR);
				$command->bindValue(":area_const_P02", $params['piso2'] , PDO::PARAM_STR);
				$command->bindValue(":area_const_P03", $params['piso3'] , PDO::PARAM_STR);
				$command->bindValue(":area_const_P04", $params['piso4'] , PDO::PARAM_STR);
				$command->bindValue(":area_const_P05", $params['piso5'] , PDO::PARAM_STR);
				$command->bindValue(":area_const_P06", $params['piso6'] , PDO::PARAM_STR);
				$command->bindValue(":area_const_otro", $params['piso7'] , PDO::PARAM_STR);
				$command->bindValue(":area_const_sot", $params['sotano'] , PDO::PARAM_STR);
				$command->bindValue(":area_const_mez", $params['mezzanine'] , PDO::PARAM_STR);

				break;

			case 'delete':

				$sql .= "DELETE_DCTOS_PRESENTADOS :m_id_ficha, :m_nro_registro";

				$command = Yii::app()->db->createCommand($sql);

				$command->bindValue(":m_id_ficha", $params['ficha'] , PDO::PARAM_STR);
				$command->bindValue(":m_nro_registro", $params['tipo_doc'] , PDO::PARAM_STR);

				break;
		}

		$result  = $command->execute();

		return $result;
	}

	/**
	 * Ejecuta procedimientos almacenados para las acciones de Creación,
	 * modificación y eliminación de construcciones.
	 * @param  object $params Toda la información con los datos a minipular.
	 * @param  string $action Clave para entender la acción que se realizará.
	 * @return boolean         Confirmación de la acción
	 */
	public static function mantenimientoConstrucciones($params, $action) 
	{
		$sql = 'EXEC ';

		switch ($action) {
			case 'create':
			case 'update':

				if ( $action == 'create' ) 
				{
					$sql .= "INSERT_CONSTRUCCION ";
					$sql .= ":m_id_ficha, :m_nro_piso, :m_mes, :m_ano, :m_mep, :m_ecs, :m_ecc, :m_estru_muro_col, :m_estru_techo, :m_acaba_piso, ";
					$sql .= ":m_acaba_puerta_ven, :m_acaba_revest, :m_acaba_bano, :m_inst_elect_sanita, :m_area_declarada, :m_area_verificada, :m_uca";
				} 
				else if ( $action == 'update' ) 
				{
					$sql .= "UPDATE_CONSTRUCCION ";
					$sql .= ":m_id_ficha, :m_nro_registro, :m_nro_piso, :m_mes, :m_ano, :m_mep, :m_ecs, :m_ecc, :m_estru_muro_col, :m_estru_techo, :m_acaba_piso, ";
					$sql .= ":m_acaba_puerta_ven, :m_acaba_revest, :m_acaba_bano, :m_inst_elect_sanita, :m_area_declarada, :m_area_verificada, :m_uca";
				} 
				else 
				{
					return FALSE;
				}

				$command = Yii::app()->db->createCommand($sql);

				$command->bindValue(":m_id_ficha", $params['ficha'], PDO::PARAM_STR);

				if ( $action == 'update' ) {
					$command->bindValue(":m_nro_registro", (int)$params['id'], PDO::PARAM_INT);
				}

				$command->bindValue(":m_nro_piso", $params['nro_piso'], PDO::PARAM_STR);
				$command->bindValue(":m_mes", $params['mes'], PDO::PARAM_STR);
				$command->bindValue(":m_ano", $params['anio'], PDO::PARAM_STR);
				$command->bindValue(":m_mep", $params['mep'], PDO::PARAM_STR);
				$command->bindValue(":m_ecs", $params['ecs'], PDO::PARAM_STR);
				$command->bindValue(":m_ecc", $params['ecc'], PDO::PARAM_STR);
				$command->bindValue(":m_estru_muro_col", mb_strtoupper($params['muros_col'], 'UTF-8'), PDO::PARAM_STR);
				$command->bindValue(":m_estru_techo", mb_strtoupper($params['techo'], 'UTF-8'), PDO::PARAM_STR);
				$command->bindValue(":m_acaba_piso", mb_strtoupper($params['pisos'], 'UTF-8'), PDO::PARAM_STR);
				$command->bindValue(":m_acaba_puerta_ven", mb_strtoupper($params['puerta_vent'], 'UTF-8'), PDO::PARAM_STR);
				$command->bindValue(":m_acaba_revest", mb_strtoupper($params['reves'], 'UTF-8'), PDO::PARAM_STR);
				$command->bindValue(":m_acaba_bano", mb_strtoupper($params['banios'], 'UTF-8'), PDO::PARAM_STR);
				$command->bindValue(":m_inst_elect_sanita", mb_strtoupper($params['inst_electricas'], 'UTF-8'), PDO::PARAM_STR);
				$command->bindValue(":m_area_declarada", $params['area_declarada'], PDO::PARAM_STR);
				$command->bindValue(":m_area_verificada", $params['area_verificada'], PDO::PARAM_STR);
				$command->bindValue(":m_uca", $params['uca'], PDO::PARAM_STR);

				break;

			case 'delete':

				$sql .= "DELETE_CONSTRUCCION :m_id_ficha, :m_nro_registro";

				$command = Yii::app()->db->createCommand($sql);

				$command->bindValue(":m_id_ficha", $params['ficha'] , PDO::PARAM_STR);
				$command->bindValue(":m_nro_registro", $params['id'] , PDO::PARAM_STR);

				break;
		}

		$result  = $command->execute();

		return $result;
	}

	/**
	 * Ejecuta procedimientos almacenados para las acciones de Creación,
	 * modificación y eliminación del titular.
	 * @param  object $params Toda la información con los datos a minipular.
	 * @param  string $action Clave para entender la acción que se realizará.
	 * @return boolean         Confirmación de la acción
	 */
	public static function mantenimientoTitular($params, $action) 
	{
		if ( $action == 'update' OR $action == 'create' ) 
		{
			$sql = 'EXEC UPDATE_TITULAR :m_id_ficha, :m_id_persona, :m_porc_titular, :m_cod_contribuy, :m_forma_adq, :m_fecha_adq';

			$command = Yii::app()->db->createCommand($sql);
			
			$command->bindValue(":m_id_ficha", $params['ficha'] , PDO::PARAM_STR);
			$command->bindValue(":m_id_persona", $params['id_persona'] , PDO::PARAM_STR);
			$command->bindValue(":m_porc_titular", $params['por_adquisicion'] , PDO::PARAM_STR);
			$command->bindValue(":m_cod_contribuy", $params['cod_contribuyente'] , PDO::PARAM_STR);
			$command->bindValue(":m_forma_adq", $params['forma_adquisicion'] , PDO::PARAM_STR);
			$command->bindValue(":m_fecha_adq", ( ! empty($params['f_adquisicion']) ) ? Fecha::format($params['f_adquisicion'], 'd/m/Y', 'Y-m-d') : '1900-01-01' , PDO::PARAM_STR);

			$command->execute();

			$command->reset();

			$islocal = (bool)$params['islocal'];

			if ( ! $islocal ) 
			{
				$sql = 'EXEC UPDATE_DOMICILIO_FISCAL_FORAN :m_id_ficha, :m_id_persona, :p_estado_civil, :m_id_ubigeo, :m_telefono, ';
				$sql .= ':m_anexo, :m_fax, :m_mail, :m_desc_via, :m_nro_municipal, :m_nombre_edifica, ';
				$sql .= ':m_nro_interior, :m_desc_hab_urb, :m_mza_urbana, :m_lote_urbano, :m_sub_lote';
			} else {
				$sql = 'EXEC UPDATE_DOMICILIO_FISCAL_LOCAL :m_id_ficha, :m_id_persona, :p_estado_civil, :m_id_ubigeo, :m_telefono, ';
				$sql .= ':m_anexo, :m_fax, :m_mail, :m_cod_via, :m_nro_municipal, :m_nombre_edifica, ';
				$sql .= ':m_nro_interior, :m_cod_hab_urb, :m_mza_urbana, :m_lote_urbano, :m_sub_lote';
			}

			$command = Yii::app()->db->createCommand($sql);

			$command->bindValue(":m_id_ficha", $params['ficha'] , PDO::PARAM_STR);
			$command->bindValue(":m_id_persona", $params['id_persona'] , PDO::PARAM_STR);
			$command->bindValue(":p_estado_civil", $params['estado_civil'] , PDO::PARAM_STR);
			$command->bindValue(":m_id_ubigeo", $params['ubigeo'] , PDO::PARAM_STR);
			$command->bindValue(":m_telefono", $params['telefono'] , PDO::PARAM_STR);
			$command->bindValue(":m_anexo", $params['anexo'] , PDO::PARAM_STR);
			$command->bindValue(":m_fax", $params['fax'] , PDO::PARAM_STR);
			$command->bindValue(":m_mail", $params['email'] , PDO::PARAM_STR);

			//via
			if ( ! $islocal ) 
				$command->bindValue(":m_desc_via", $params['via'] , PDO::PARAM_STR);
			else 
				$command->bindValue(":m_cod_via", $params['via'] , PDO::PARAM_STR);

			$command->bindValue(":m_nro_municipal", $params['nro_municipalidad'] , PDO::PARAM_STR);
			$command->bindValue(":m_nombre_edifica", $params['nombre_edificio'] , PDO::PARAM_STR);
			$command->bindValue(":m_nro_interior", $params['nro_interior'] , PDO::PARAM_STR);

			//haburbanas
			if ( ! $islocal ) 
				$command->bindValue(":m_desc_hab_urb", $params['hurbana'] , PDO::PARAM_STR);
			else 
				$command->bindValue(":m_cod_hab_urb", $params['hurbana'] , PDO::PARAM_STR);

			$command->bindValue(":m_mza_urbana", $params['manzana'] , PDO::PARAM_STR);
			$command->bindValue(":m_lote_urbano", $params['lote'] , PDO::PARAM_STR);
			$command->bindValue(":m_sub_lote", $params['sublote'] , PDO::PARAM_STR);

			$command->execute();

			$command->reset();

			if ( $params['condesp_titular'] != '' )
			{
				$sql = 'EXEC UPDATE_EXONERA_TITULAR :m_id_ficha, :m_id_persona, :m_cond_esp_tit, ';
				$sql .=':m_nro_resol_exon, :m_nro_boleta_pension, :m_fecha_inicio_exon, :m_fecha_venc_exon';

				$command = Yii::app()->db->createCommand($sql);

				$command->bindValue(":m_id_ficha", $params['ficha'] , PDO::PARAM_STR);
				$command->bindValue(":m_id_persona", $params['id_persona'] , PDO::PARAM_STR);
				$command->bindValue(":m_cond_esp_tit", $params['condesp_titular'] , PDO::PARAM_STR);
				$command->bindValue(":m_nro_resol_exon", $params['nro_resolucion'] , PDO::PARAM_STR);
				$command->bindValue(":m_nro_boleta_pension", $params['nro_boleta_pension'] , PDO::PARAM_STR);
				$command->bindValue(":m_fecha_inicio_exon", ( ! empty($params['f_inicio']) ) ? Fecha::format($params['f_inicio'], 'd/m/Y', 'Y-m-d') : '1900-01-01' , PDO::PARAM_STR);
				$command->bindValue(":m_fecha_venc_exon", ( ! empty($params['f_vencimiento']) ) ? Fecha::format($params['f_vencimiento'], 'd/m/Y', 'Y-m-d') : '1900-01-01' , PDO::PARAM_STR);
				//Utils::show($command, TRUE);
				$command->execute();
			}

			return TRUE;
		} 
		else if ( $action = 'delete' ) 
		{

			$sql = "EXEC DELETE_TITULAR :m_id_ficha, :m_id_persona";

			$command = Yii::app()->db->createCommand($sql);

			$command->bindValue(":m_id_ficha", $params['ficha'] , PDO::PARAM_STR);
			$command->bindValue(":m_id_persona", $params['id_persona'] , PDO::PARAM_STR);

			$result = $command->execute();

			return $result;
		}

		return false;
	}

	/**
	 * Ejecuta procedimientos almacenados para las acciones de Creación,
	 * modificación y eliminación de las notarias.
	 * @param  object $params Toda la información con los datos a minipular.
	 * @param  string $action Clave para entender la acción que se realizará.
	 * @return boolean         Confirmación de la acción
	 */
	public static function mantenimientoNotaria($params = [])
	{
		$command = Yii::app()->db->createCommand("EXEC Sp_MantenimientoNotaria :Id_Notaria, :Nom_Notaria, :Id_Ubi_Geo");

		$command->bindValue(":Id_Notaria", $params['id'], PDO::PARAM_STR);
		$command->bindValue(":Nom_Notaria", mb_strtoupper($params['name'], 'UTF-8'), PDO::PARAM_STR);
		$command->bindValue(":Id_Ubi_Geo", $params['ubigeo'], PDO::PARAM_STR);

		$confirm = $command->execute();

		return $confirm;
	}

	/**
	 * Ejecuta procedimientos almacenados para las acciones de Creación,
	 * modificación de personas.
	 * @param  object $params Toda la información con los datos a minipular.
	 * @param  string $action Clave para entender la acción que se realizará.
	 * @return boolean         Confirmación de la acción
	 */
	public static function mantenimientoPersonas($params, $action)
	{
		$sql = 'EXEC ';

		switch ($action) {
			case 'create':
			case 'update':

				if ( $params['tipo_documento'] == '01' ) 
					$sql .= "INSERT_PERSONA_SIN_DCTO ";
				else
					$sql .= "usp_Persona ";

				$sql .= ":m_ndoc, :m_tdoc, :m_tper, :m_nomb, :m_mater, :m_pater, :m_tip_per_jur, :m_estado_civil";

				$command = Yii::app()->db->createCommand($sql);

				$command->bindValue(":m_ndoc", $params['nro_documento'], PDO::PARAM_STR);
				$command->bindValue(":m_tdoc", $params['tipo_documento'], PDO::PARAM_STR);
				$command->bindValue(":m_tper", $params['tipo_titular'], PDO::PARAM_STR);
				$command->bindValue(":m_nomb", mb_strtoupper($params['nombres'], 'UTF-8'), PDO::PARAM_STR);
				$command->bindValue(":m_mater", mb_strtoupper($params['ape_materno'], 'UTF-8'), PDO::PARAM_STR);
				$command->bindValue(":m_pater", mb_strtoupper($params['ape_paterno'], 'UTF-8'), PDO::PARAM_STR);
				$command->bindValue(":m_tip_per_jur", $params['persona_juridica'], PDO::PARAM_STR);
				$command->bindValue(":m_estado_civil", $params['estado_civil'], PDO::PARAM_STR);

				break;

			case 'delete':

				$sql .= "";

				$command = Yii::app()->db->createCommand($sql);

				break;
		}

		$result  = $command->execute();

		return $result;
	}

	/**
	 * Ejecuta procedimientos almacenados para las acciones de Creación,
	 * modificación y eliminación de obras.
	 * @param  object $params Toda la información con los datos a minipular.
	 * @param  string $action Clave para entender la acción que se realizará.
	 * @return boolean         Confirmación de la acción
	 */
	public static function mantenimientoObras($params, $action)
	{
		$sql = 'EXEC ';

		switch ($action) {
			case 'create':
			case 'update':

				if ( $action == 'create' ) 
				{
					$sql .= "INSERT_INSTALACION :m_id_ficha, :m_cod_instalacion, :m_mes, :m_ano, :m_mep, :m_ecs, :m_ecc, 
									:m_dimension_largo, :m_dimension_ancho, :m_dimension_alto, :m_producto_total, :m_uca, :unidad";
				} 
				else if ( $action == 'update' ) 
				{
					$sql .= "UPDATE_INSTALACION :m_id_ficha, :m_nro_registro, :m_cod_instalacion, :m_mes, :m_ano, :m_mep, :m_ecs, :m_ecc, 
									:m_dimension_largo, :m_dimension_ancho, :m_dimension_alto, :m_producto_total, :m_uca, :unidad";
				} 
				else 
				{
					return FALSE;
				}

				$command = Yii::app()->db->createCommand($sql);

				$command->bindValue(':m_id_ficha', $params['ficha'], PDO::PARAM_STR);

				if ( $action == 'update' ) {
					$command->bindValue(":m_nro_registro", (int)trim($params['id']), PDO::PARAM_INT);
				}

				$command->bindValue(':m_cod_instalacion', $params['cod_instalacion'], PDO::PARAM_STR);
				$command->bindValue(':m_mes', $params['mes'], PDO::PARAM_STR);
				$command->bindValue(':m_ano', $params['anio'], PDO::PARAM_STR);
				$command->bindValue(':m_mep', $params['mep'], PDO::PARAM_STR);
				$command->bindValue(':m_ecs', $params['ecs'], PDO::PARAM_STR);
				$command->bindValue(':m_ecc', $params['ecc'], PDO::PARAM_STR);
				$command->bindValue(':m_dimension_largo', $params['largo'], PDO::PARAM_STR);
				$command->bindValue(':m_dimension_ancho', $params['ancho'], PDO::PARAM_STR);
				$command->bindValue(':m_dimension_alto', $params['alto'], PDO::PARAM_STR);
				$command->bindValue(':m_producto_total', $params['total'], PDO::PARAM_STR);
				$command->bindValue(':m_uca', $params['uca'], PDO::PARAM_STR);
				$command->bindValue(':unidad', $params['unidad'], PDO::PARAM_INT);

				//Utils::show($command, TRUE);

				break;
			case 'delete':

				$sql .= "DELETE_INSTALACION :m_id_ficha, :m_nro_registro";

				$command = Yii::app()->db->createCommand($sql);

				$command->bindValue(":m_id_ficha", $params['ficha'] , PDO::PARAM_STR);
				$command->bindValue(":m_nro_registro", $params['id'] , PDO::PARAM_STR);

				break;
		}

		$result  = $command->execute();

		return $result;
	}

	/**
	 * Consulta la información completa de un titular asignado a una ficha.
	 * @param  string $ficha      ID ficha.
	 * @param  string $id_persona ID persona.
	 * @return object             Datos del titular.
	 */
	public static function getTitularInfo($ficha, $id_persona)
	{
		$sql = "SELECT 
							tipo , tip_persona , dbo.fn_MultiplicaSaldo('07', df.tip_persona , 0) persona , 
							FORMA_ADQ , FECHA_ADQ , PORCENTAJE_PERTENENCIA , TIP_DOC , NRO_DOC , COD_CONTRIBUY ,  
							NOMBRES , APE_PATERNO , APE_MATERNO , TIP_PERSONA_JURIDICA , ID_UBI_GEO , 
							COD_HAB_URB , NOM_HAB_URB , ZN_SC_ETAPA, cod_via , nom_via , NRO_MUNICIPAL , 
							NRO_INTERIOR , NOMBRE_EDIFICA , MZA_URBANA , LOTE_URBANO, SUB_LOTE, COD_CAT, 
							ID_ficha, ID , ESTADO_CIVIL , TELEFONO , FAX , CORREO_ELECT , CONDICION , 
							NRO_RESOLUCION , NRO_BOLETA_PENSION , FECHA_INICIO , FECHA_VENCIMIENTO  
						FROM DATOS_domicilio_fiscal_principal DF  
						WHERE 
								ID_FICHA  = :ID_FICHA 
						AND id_persona = :id_persona";

		$command = Yii::app()->db->createCommand($sql);

		$command->bindValue(":ID_FICHA", (string)$ficha , PDO::PARAM_STR);
		$command->bindValue(":id_persona", (string)$id_persona , PDO::PARAM_STR);

		$row = $command->queryRow();

		return $row;
	}

	/**
	 * Consulta la información basica de una persona en base a filtros
	 * @param  string $rel    Clave que filtra la busqueda entre N° de documento o nombres.
	 * @param  int $tipo   Clave que filtra la busqueda por persona juridica o natural
	 * @param  string $search Texto para la busqueda de persona
	 * @return array         Lista con el resultado de la busqueda
	 */
	public static function showPersonasFicha($rel, $tipo, $search)
	{
		$sql = "EXEC ";

		if ( $rel == 'doc' ) {
			$sql .= "SP_ShowPersonasFichaDocumento :TIPO, :ID_FICHA";
		} else if ( $rel == 'name' ) {
			$sql .= "SP_ShowPersonasFicha :TIPO, :ID_FICHA";
		}

		$command = Yii::app()->db->createCommand($sql);

		$command->bindValue(":TIPO", $tipo , PDO::PARAM_STR);
		$command->bindValue(":ID_FICHA", ($search . '%'), PDO::PARAM_INT);

		$table = $command->queryAll();

		return $table;
	}

	/**
	 * Consulta toda la lista de notarias
	 * @return array Lista con todas las notarias
	 */
	public static function get_Notarias()
	{
		$command = Yii::app()->db->createCommand("EXEC sp_ShowNotaria");

		$table = $command->queryAll();

		return $table;
	}

	/**
	 * Obtiene la información completa de la instalación
	 * @param  string $ficha ID de ficha.
	 * @param  string $id    ID de instalación.
	 * @return object        Datos completos de la instalación
	 */
	public static function get_instalacion($ficha, $id) 
	{
		$sql = "SELECT 
							ins.id, 
							ins.cod_instalacion, 
							[DESCRIPCION] instalacion,
							[MATER_PREDOM] mate_predominante,
							ins.mes, 
							ins.año anio, 
							ins.mep, 
							ins.ecs, 
							ins.ecc, 
							ins.uca,
							[DIMENCION_LARGO] dim_largo, 
							[DIMENCION_ANCHO] dim_ancho, 
							[DIMENCION_ALTO] dim_alto, 
							[DIMENCION_TOTAL] total,
							[UNIDAD_MEDIDA] umedida,
							[UNIDAD] unidad, 
							[LARGO] largo,
							[ANCHO] ancho,
							[ALTO] alto
							FROM
							dbo.INSTALACIONES AS ins 
							INNER JOIN dbo.OTRAS_INSTALACIONES AS otr ON ins.COD_INSTALACION = otr.COD_INSTALACION 
							INNER JOIN dbo.FICHAS AS f ON ins.ID_FICHA = f.ID_FICHA
							WHERE f.ID_FICHA = :ficha
							AND ins.id = :id";

		$command = Yii::app()->db->createCommand($sql);
		//Utils::show($command, TRUE);
		$command->bindValue(":ficha", $ficha, PDO::PARAM_STR);
		$command->bindValue(":id", $id, PDO::PARAM_STR);

		$row = $command->queryRow();

		if ( $row ) {
			$row['largo'] = (boolean)$row['largo'];
			$row['ancho'] = (boolean)$row['ancho'];
			$row['alto'] = (boolean)$row['alto'];
			$row['cod_instalacion'] = trim($row['cod_instalacion']);
		}

		return $row;
	}

	/**
	 * Obtiene la lista completa o filtrada de otras instalaciones
	 * @param  string $id ID de la otra instalación en caso sea 0 la lista se mostrará completa
	 * @return array      Lista con el resultado de la busqueda
	 */
	public static function get_otrasInstalaciones($id = 0)
	{
		$sql = "SELECT 
							OI.[COD_INSTALACION] cod_instalacion, 
							OI.[DESCRIPCION] instalacion, 
							OI.[MATER_PREDOM] mate_predominante, 
							OI.[UNIDAD_MEDIDA] umedida, 
							OI.[LARGO] largo, 
							OI.[ANCHO] ancho, 
							OI.[ALTO] alto, 
							OI.[CAL_VALOR] calc_valor
						FROM 
							OTRAS_INSTALACIONES OI";

		if ( $id > 0 ) 
			$sql .= " WHERE OI.[COD_INSTALACION] = :id";

		$sql .= " ORDER BY OI.[COD_INSTALACION] ASC";

		$command = Yii::app()->db->createCommand($sql);

		if ( $id > 0 )
			$command->bindValue(":id", $id, PDO::PARAM_STR);

		$table = $command->queryAll();
		//Utils::show($table, TRUE);
		foreach ($table as $row => &$cell) 
		{
			$cell['cod_instalacion'] = trim($cell['cod_instalacion']);
			$cell['alto'] = (boolean)$cell['alto'];
			$cell['largo'] = (boolean)$cell['largo'];
			$cell['ancho'] = (boolean)$cell['ancho'];
			$cell['calc_valor'] = (boolean)$cell['calc_valor'];
		}

		return ($id > 0) ? $table[0] : $table;

	}

	/**
	 * Obtiene la lista de habilitaciones urbanas que pertenecen a un sector.
	 * @param  string $sector    ID del sector.
	 * @param  int $pertenece valor de pertenencia.
	 * @return array            Lista con el resultado de la busqueda.
	 */
	public static function get_SectorHabUrbanas($sector, $pertenece)
	{
		//$command = Yii::app()->db->createCommand("EXEC sp_ShowSectorHabUrba :CodSector, :pertenece");
		$command = Yii::app()->db->createCommand("SELECT * FROM dbo.vw_HabilitacionesUrbanas");

		/*$command->bindValue(":CodSector", $sector, PDO::PARAM_STR);
		$command->bindValue(":pertenece", $pertenece, PDO::PARAM_INT);*/

		$table = $command->queryAll();

		$ntable = [];

		foreach ($table as $row => $cell) {
			$ntable[] = [
				"CodHabUrb" => $cell['CODIGO'],
				"HabilitacionUrbana" => $cell['NOMBRE'] . ' - ' . $cell['ZONA']
			];
		}

		return $ntable;
	}

	/**
	 * Obtiene la lista de las zonificaciones que pertenecen a un sector.
	 * @param  string $sector    ID del sector.
	 * @param  int $pertenece valor de pertenencia.
	 * @return array            Lista con el resultado de la busqueda.
	 */
	public static function get_SectorZona($sector, $pertenece)
	{
		$command = Yii::app()->db->createCommand("EXEC sp_ShowSectorZona :CodSector, :pertenece");

		$command->bindValue(":CodSector", '0', PDO::PARAM_STR);
		$command->bindValue(":pertenece", 0, PDO::PARAM_INT);

		$table = $command->queryAll();

		return $table;
	}

	/**
	 * Obtiene la lista de las vias que pertenecen a una habilitación urbana.
	 * @param  string $haburbana Codigo de habilitación urbana.
	 * @return array            Lista con el resultado de la busqueda.
	 */
	public static function get_HabUrbanaVias($haburbana)
	{
		/*$command = Yii::app()->db->createCommand("EXEC sp_ShowHabUrbaVia :CodHU, :pertenece");

		$command->bindValue(":CodHU", $haburbana, PDO::PARAM_STR);
		$command->bindValue(":pertenece", 1, PDO::PARAM_INT);*/
		$command = Yii::app()->db->createCommand("SELECT v.[CODIGO] Cod_via, (v.[DESC_TABLA_DETALLE] + ' ' + v.[NOMBRE]) via FROM dbo.[vw_Vias] v");

		$table = $command->queryAll();

		return $table;
	}

	/**
	 * Obtiene la lista de personas basado en un filtro.
	 * @param  string $nro_documento Numero de documento de la persona,
	 * @param  int $tipo          Tipo de persona juridica o natural.
	 * @return object             Datos completos de la busqueda.
	 */
	public static function get_Personas($nro_documento, $tipo)
	{

		$sql = "SELECT 
							p.ID_PERSONA id_persona, 
							p.NRO_DOC nro_doc, 
							p.TIP_DOC tipo_doc, 
							p.TIP_PERSONA tipo_persona,
							p.NOMBRES nombres,
							p.APE_PATERNO ape_paterno,
							p.APE_MATERNO ape_materno,
							(p.APE_PATERNO + ' ' + p.APE_MATERNO + ', ' + p.NOMBRES) nombre_completo,
							p.TIP_PERSONA_JURIDICA tipo_persona_juridica,
							p.NRO_RUC_2 nro_ruc,
							p.ESTADO_CIVIL estado_civil
						FROM 
							dbo.PERSONAS p 
						WHERE 
						p.NRO_DOC = :nro_documento 
						AND TIP_PERSONA = :tipo";

		$command = Yii::app()->db->createCommand($sql);

		$command->bindValue(':nro_documento', $nro_documento, PDO::PARAM_STR);
		$command->bindValue(':tipo', $tipo, PDO::PARAM_INT);

		$table  = $command->queryRow();

		return $table;
	}

	/**
	 * Obtiene la lista del personal basado en un filtro.
	 * @param  string $codigo Codigo del personal
	 * @param  string $tipo   Tipo de personal
	 * @param  string $cargo  Cargo que ocupa.
	 * @return object         Datos completos de la busqueda
	 */
	public static function get_Personal($codigo, $tipo, $cargo)
	{
		$sql = "SELECT 
							[U].[ID_USUARIO] codigo, 
							CASE 
								[PER].[TIP_PERSONA]  
							WHEN '1' THEN 
								CASE 
									[PER].[APE_PATERNO] 
								WHEN '' THEN 
									[PER].[NOMBRES]
								ELSE 
									[PER].[APE_PATERNO] + ' ' + [PER].[APE_MATERNO] + ', ' + [PER].[NOMBRES] 
								END 
							WHEN '2' THEN 
								[PER].[NOMBRES] 
							END AS nombre_completo 
						FROM 
							[PERSONAS] AS PER, 
							[USUARIOS_BAK] AS U 
						WHERE [PER].[ID_PERSONA] = [U].[ID_PERSONA] 
						AND [U].[CARGO] = :cargo 
						AND [PER].[TIP_PERSONA] = :tipo 
						AND [U].[ID_USUARIO] = :codigo";

		$command = Yii::app()->db->createCommand($sql);

		$command->bindValue(':codigo', $codigo, PDO::PARAM_INT);
		$command->bindValue(':tipo', $tipo, PDO::PARAM_INT);
		$command->bindValue(':cargo', $cargo, PDO::PARAM_STR);

		$table  = $command->queryRow();

		return $table;
	}

	/**
	 * Obtiene la lista general de todos los titulares
	 * @param  string $strbusqueda Texto de busqueda.
	 * @param  int $tipo        Tipo de persona jurida o natural
	 * @return array              Lista con el resultado de la busqueda
	 */
	public static function get_Titular($strbusqueda, $tipo) 
	{
		$strbusqueda = mb_strtoupper($strbusqueda, 'UTF-8');
		
		$sql = "SELECT 
							p.ID_PERSONA id_persona, 
							p.NRO_DOC nro_documento,
							p.TIP_PERSONA tipo_persona,
							CASE p.TIP_PERSONA WHEN '1' THEN 
									P.APE_PATERNO + ' ' + p.APE_MATERNO + ', ' + p.NOMBRES 
							WHEN '2' THEN 
								p.NOMBRES
							END AS nombres,
							p.TIP_DOC tipo_documento,
							p.ESTADO_CIVIL estado_civil,
							p.TIP_PERSONA_JURIDICA tipo_persona_juridica
						FROM dbo.PERSONAS p
						WHERE ";

		if ( $tipo == 1 ) {
			$sql .= "p.ID_PERSONA = :search";
		} else if ( $tipo == 2 ) {
			$sql .= "(p.NOMBRES + ' ' + p.APE_PATERNO + ' ' + p.APE_MATERNO) = '{$strbusqueda}' OR (p.APE_PATERNO + ' ' + p.APE_MATERNO + ', ' + p.NOMBRES) = '{$strbusqueda}'";
		} else if ( $tipo == 3 ) {
			$sql .= "p.NRO_DOC = :search";
		} else {
			throw new Exception("El parametro de busqueda no es el correcto", 200);
		}

		$command = Yii::app()->db->createCommand($sql);

		if ( $tipo != 2 ) {
			$command->bindValue(':search', $strbusqueda, PDO::PARAM_STR);
		}
		
		$table  = $command->queryAll();

		return $table;
	}

	private static function getTableBasicInfo($id_ficha) 
	{
		$sql = "SELECT 
							f.ID_FICHA id_ficha, 
							f.COD_CAT cod_catastro, 
							f.COD_HOJA_CAT cod_hoja_catastral, 
							f.NRO_FICHA nro_ficha, 
							f.NRO_FICHA_LOTE nro_ficha_lote, 
							f.DC dc, 
							ISNULL(f.CUC,'') cuc, 
							ISNULL(f.ESTADO_LLENADO, '') estado_llenado, 
							ISNULL(f.CONDIC_DECLARA, '') cond_declara, 
							ISNULL(f.MANTENIMIENTO, '') mantenimiento, 
							fi.NRO_INTERIOR nro_interior, 
							fi.EVALUAC_PRED_CAT evalpredio, 
							fi.NRO_FAMILIAS nro_familias, 
							fi.NRO_HABITANTES nro_habitantes, 
							fi.NRO_HOMBRES nro_hombres, 
							fi.NRO_MUJERES nro_mujeres, 
							fi.NRO_MENORES nro_ninios, 
							fi.NRO_ADULTO_MAYOR nro_adulmayor, 
							fi.NRO_DISCAPACIT nro_discapacita, 
							fi.AREA_VERIFICADA area_verificada, 
							fi.AREA_DECLARADA area_declarada, 
							fi.AREA_TITULO area_titulo, 
							fi.AREA_OCUPADA area_ocupada, 
							ISNULL(fi.PORC_BC_TERR_LEG, 0) terreno_legal, 
							ISNULL(fi.PORC_BC_TERR_FIS, 0) terreno_fisico, 
							ISNULL(fi.PORC_BC_CONST_LEG, 0) construc_legal, 
							ISNULL(fi.PORC_BC_CONST_FIS, 0) construc_fisico, 
							ISNULL(fi.OBSERVACIONES, '') observaciones, 
							ISNULL(fi.COD_USO, '') uso_predio, 
							ISNULL(fi.CLASIFICACION, '') clasif_predio, 
							ISNULL(fi.PREDIO_CATASTRADO_EN, '') pred_catastral, 
							ISNULL(fi.UNID_ACUM_COD_PREDIAL, '') ua_predial_rentas, 
							ISNULL(fi.COD_PREDIAL, '') cod_predial_rentas, 
							ISNULL(fi.FUENTE_INFORM, '') fuente_info, 
							ISNULL(fi.CONDICION_TITULAR, '') cond_titular, 
							ISNULL(fi.TIPO_INTERIOR, '') tipo_interior, 
							lt.MZA_URB manzana, 
							lt.LOTE_DIST lote, 
							lt.SUBLOTE sublote, 
							lt.ESTRUCTURACION estructuracion, 
							ISNULL(lt.COD_HAB_URB, '') code_haburba, 
							ISNULL(lt.ZONIFICACION, '') zonificacion, 
							e.NOMBRE_EDIFICACION name_edificio, 
							ISNULL(e.TIPO_EDIFICACION , '') tipo_edificio
						FROM 
							dbo.[FICHAS] f, 
							dbo.[FICHAS_INDIVIDUALES] fi, 
							dbo.[LOTES] lt, 
							dbo.[EDIFICACIONES] e  
						WHERE 
							f.[TIP_FICHA] = '01'  
						AND f.[ID_FICHA] = fi.[ID_FICHA] 
						AND f.[ID_LOTE] = lt.[ID_LOTE] 
						AND fi.[ID_EDIFICACION] = e.[ID_EDIFICACION]
						AND f.[ID_FICHA] = :id_ficha  
						AND f.[ID_CATASTRO] = :year_proceso";

		$command = Yii::app()->db->createCommand($sql);

		$command->bindValue(":id_ficha", $id_ficha, PDO::PARAM_STR);
		$command->bindValue(":year_proceso", Yii::app()->user->proceso['selected'], PDO::PARAM_STR);

		$row = $command->queryRow();

		return (! $row) ? [] : $row;
	}

	private static function getTableExoneracionesPredio($id_ficha)
	{
		$sql = "SELECT 
							ep.[CONDICION] cond_especialpredio, 
							ep.[NRO_RESOLUCION] nro_resolucion, 
							ep.[PORCENTAJE] porcent, 
							ep.[FECHA_VENCIMIENTO] fvencimiento, 
							ep.[FECHA_INICIO] finicio
						FROM 
							dbo.[EXONERACIONES_PREDIO] ep
						WHERE 
							ep.[ID_FICHA] = :id_ficha";

		$command = Yii::app()->db->createCommand($sql);

		$command->bindValue(":id_ficha", $id_ficha, PDO::PARAM_STR);

		$row = $command->queryRow();

		return (! $row) ? [] : $row;
	}

	private static function getTableLinderos($id_ficha)
	{
		$sql = "SELECT 
							li.[FRE_MED_CAMPO] medcamp_frente, 
							li.[FRE_COL_TITULO] coltit_frente, 
							li.[FRE_COL_CAMPO] colcampo_frente, 
							li.[FRE_MED_TITULO] medtit_frente, 
							li.[DER_MED_CAMPO] medcamp_derecha, 
							li.[DER_COL_TITULO] coltit_derecha, 
							li.[DER_COL_CAMPO] colcampo_derecha, 
							li.[DER_MED_TITULO] medtit_derecha, 
							li.[IZQ_MED_CAMPO] medcamp_izquierda, 
							li.[IZQ_COL_TITULO] coltit_izquierda, 
							li.[IZQ_COL_CAMPO] colcampo_izquierda, 
							li.[IZQ_MED_TITULO] medtit_izquierda, 
							li.[FON_MED_CAMPO] medcamp_fondo, 
							li.[FON_COL_TITULO] coltit_fondo, 
							li.[FON_COL_CAMPO] colcampo_fondo, 
							li.[FON_MED_TITULO] medtit_fondo, 
							li.[IZQ_LIM_FRE_LOT] limpfrente_izquierda, 
							li.[DER_LIM_FRE_LOT] limpfrente_derecha, 
							li.[FON_LIM_FRE_LOT] limpfrente_fondo, 
							li.[FRE_LIM_FRE_LOT] limpfrente_frente, 
							li.[TOT_LIM_FRE_UC] total_limpfrenteuc
						FROM 
							dbo.[LINDEROS] li
						WHERE li.[id_ficha] = :id_ficha";

		$command = Yii::app()->db->createCommand($sql);

		$command->bindValue(":id_ficha", $id_ficha, PDO::PARAM_STR);

		$row = $command->queryRow();

		return (! $row) ? [] : $row;
	}

	private static function getTableServiciosBasicos($id_ficha)
	{
		$sql = "SELECT 
							sb.[TELEFONO] AS phone, 
							sb.[NRO_TELEFONO] AS sumphone, 
							sb.[AGUA] AS agua, 
							sb.[NRO_CONTRATO_AGUA] AS sumagua, 
							sb.[LUZ] AS luz, 
							sb.[NRO_SUM_LUZ] AS sumluz, 
							sb.[DESAGUE] AS desague, 
							sb.[GAS] AS gas, 
							sb.[CABLE] AS cable, 
							sb.[INTERNET] AS internet
						FROM 
							dbo.[SERVICIOS_BASICOS] AS sb
						WHERE sb.[ID_FICHA] = :id_ficha";

		$command = Yii::app()->db->createCommand($sql);

		$command->bindValue(":id_ficha", $id_ficha, PDO::PARAM_STR);

		$row = $command->queryRow();

		return (! $row) ? [] : $row;
	}

	private static function getTableRegistroLegal($id_ficha)
	{
		$sql = "SELECT 
							rl.[ID_NOTARIA] AS notaria, 
							rl.[FECHA_ESCRITURA] AS fecha_inscpublica, 
							rl.[KARDEX] AS kardex
						FROM 
							dbo.[REGISTRO_LEGAL] AS rl 
						WHERE rl.[ID_FICHA] = :id_ficha";

		$command = Yii::app()->db->createCommand($sql);

		$command->bindValue(":id_ficha", $id_ficha, PDO::PARAM_STR);

		$row = $command->queryRow();

		return (! $row) ? [] : $row;
	}

	private static function getTableSunarp($id_ficha)
	{
		$sql = "SELECT 
							sn.[ASIENTO] AS asiento, 
							sn.[ASIEN_INSCRIP_FAB] AS inscfabrica, 
							sn.[FECHA_INSCRIP_FAB] AS fecha_inscfabrica, 
							sn.[DECLAR_FAB] AS decla_fabrica, 
							sn.[FECHA_INSCRIPCION] AS fecha_insc_enregpredio, 
							sn.[FOJAS] AS forja, 
							sn.[NUMERO] AS numeros, 
							sn.[TIPO_PART_REG] AS tpartida_reg
						FROM 
							dbo.[SUNARP] AS sn
						WHERE 
							sn.[ID_FICHA] = :id_ficha";

		$command = Yii::app()->db->createCommand($sql);

		$command->bindValue(":id_ficha", $id_ficha, PDO::PARAM_STR);

		$row = $command->queryRow();

		return (! $row) ? [] : $row;
	}

	private static function getTableAreaInvadida($id_ficha)
	{
		$sql = "SELECT 
							ati.[EN_LOTE_COLINDANTE] AS enlote_colin, 
							ati.[EN_AREA_INTANG] AS enarea_intang, 
							ati.[EN_JARDIN_AISLAM] AS enjardin_aislam, 
							ati.[EN_AREA_PUBLICA] AS enarea_public
						FROM 
							dbo.[AREA_TERR_INVADIDA] AS ati
						WHERE ati.[ID_FICHA] = :id_ficha";

		$command = Yii::app()->db->createCommand($sql);

		$command->bindValue(":id_ficha", $id_ficha, PDO::PARAM_STR);

		$row = $command->queryRow();

		return (! $row) ? [] : $row;
	}

	private static function getTableControlFicha($id_ficha)
	{
		$sql = "SELECT 
							cf.[FIRMA_DECLARANTE] AS declarante, 
							cf.[ID_DECLARANTE] AS dni_declarante, 
							cf.[FECHA_DECLARANTE] AS fecha_declarante, 
							cf.[FIRMA_TEC_CATASTRAL] AS tec_catastral, 
							cf.[ID_TEC_CATASTRAL] AS cod_tec_catastral, 
							cf.[FECHA_TEC_CATASTRAL] AS fecha_tec_catastral, 
							cf.[FIRMA_SUPERVISOR] AS supervisor, 
							cf.[ID_SUPERVISOR] AS cod_supervisor, 
							cf.[FECHA_SUPERVISOR] AS fecha_supervisor, 
							cf.[FIRMA_VERIF_CATASTRAL] AS verif_catastral, 
							cf.[ID_VERIF_CATASTRAL] AS cod_verif_catastral, 
							cf.[FECHA_VERIF_CATASTRAL] AS fecha_verif_catastral, 
							cf.[FIRMA_TEC_CALIDAD] AS tec_calidad, 
							cf.[ID_TEC_CALIDAD] AS cod_tec_calidad, 
							cf.[FECHA_TEC_CALIDAD] AS fecha_tec_calidad
						FROM 
							dbo.[CONTROL_DE_FICHA] AS cf 
						WHERE cf.[ID_FICHA] = :id_ficha";

		$command = Yii::app()->db->createCommand($sql);

		$command->bindValue(":id_ficha", $id_ficha, PDO::PARAM_STR);

		$row = $command->queryRow();

		return (! $row) ? [] : $row;
	}

	/**
	 * Obtiene toda la información de la ficha para el proceso de modificación
	 * @param  string $id_ficha ID de ficha
	 * @return object           objecto formateado con la información obtenida
	 */
	public static function get_DataFicha($id_ficha)
	{
		$dataBasic = self::getTableBasicInfo($id_ficha);

		if ( ! $dataBasic ) {
			return [];
		}

		$exoneracionesPredio 	= self::getTableExoneracionesPredio($id_ficha);
		$linderos 						= self::getTableLinderos($id_ficha);
		$serviciosBasicos 		= self::getTableServiciosBasicos($id_ficha);
		$registroLegal 				= self::getTableRegistroLegal($id_ficha);
		$sunarp 							= self::getTableSunarp($id_ficha);
		$areaInvadida 				= self::getTableAreaInvadida($id_ficha);
		$controlFicha 				= self::getTableControlFicha($id_ficha);

		$row = array_merge(
			$dataBasic,
			$exoneracionesPredio,
			$linderos,
			$serviciosBasicos,
			$registroLegal,
			$sunarp,
			$areaInvadida,
			$controlFicha
		);

		$nrow = new stdClass;

		$nrow->HEAD = self::get_HeadObject($row);
		$nrow->UP01 = self::get_UPObject('UP01', $row);
		$nrow->UP02 = self::get_UPObject('UP02', $row);

		$nrow->UP01 = self::formater_datetime($nrow->UP01, ['finicio', 'fvencimiento']);
		$nrow->UP02 = self::formater_datetime($nrow->UP02, [
			'fecha_inscpublica', 'fecha_insc_enregpredio', 'fecha_inscfabrica', 'fecha_declarante', 
			'fecha_tec_catastral', 'fecha_supervisor', 'fecha_verif_catastral', 'fecha_tec_calidad'
		]);

		return $nrow;
	}

	/**
	 * Crea un objeto que contiene la información perteneciente a la cabecera de la ficha.
	 *
	 * El objeto se crea en base a la consulta de toda la ficha, para que la funcion extraiga toda
	 * la información necesaria para la creación del objeto.
	 * @param  object $row Objeto con toda la información de la consulta de la ficha.
	 * @return object      Objeto con la información de la cabecera de ficha.
	 */
	private static function get_HeadObject($row)
	{
		$head = new stdClass;
		$head->cod_ref_catastral = new stdClass;

		$head->nro_ficha = trim($row['nro_ficha']);
		$head->id_ficha = trim($row['id_ficha']);
		$head->cod_catastro = trim($row['cod_catastro']);
		$head->cod_predial_rentas = trim($row['cod_predial_rentas']);
		$head->ua_predial_rentas = trim($row['ua_predial_rentas']);
		$head->cod_hoja_catastral = trim($row['cod_hoja_catastral']);

		$lotes_arr = explode('/', $row['nro_ficha_lote']);
		$head->nro_ficha_lote_first = (count($lotes_arr) == 2 OR count($lotes_arr) == 1) ? $lotes_arr[0] : '';
		$head->nro_ficha_lote_second = (count($lotes_arr) == 2) ? $lotes_arr[1] : '';

		$head->cod_ref_catastral->ubigeo = mb_substr($row['cod_catastro'], 0, 6);
    $head->cod_ref_catastral->sector = mb_substr($row['cod_catastro'], 10, 2);
    $head->cod_ref_catastral->manzana = mb_substr($row['cod_catastro'], 12, 3);
    $head->cod_ref_catastral->lote = mb_substr($row['cod_catastro'], 15, 3);
    $head->cod_ref_catastral->edificio = mb_substr($row['cod_catastro'], 18, 2);
    $head->cod_ref_catastral->entrada = mb_substr($row['cod_catastro'], 20, 2);
    $head->cod_ref_catastral->piso = mb_substr($row['cod_catastro'], 22, 2);
    $head->cod_ref_catastral->unidad = mb_substr($row['cod_catastro'], 24, 3);
    $head->cod_ref_catastral->dc = $row['dc'];

    return $head;
	}

	/**
	 * Crea un objeto con dos secciones que contienen la información dividida perteneciente a la ficha.
	 *
	 * El objeto se crea en base a la consulta de toda la ficha, para que la funcion extraiga toda
	 * la información necesaria para la creación del objeto y la división de todos los datos.
	 * @param  object $row Objeto con toda la información de la consulta de la ficha.
	 * @return object      Objeto con la información de los datos de la ficha.
	 */
	private static function get_UPObject($up, $row)
	{
		$data = new stdClass;
		$ups = [
			'UP01' => [
				"code_haburba",
				"manzana",
				"lote",
				"sublote",
				"name_edificio",
				"tipo_edificio",
				"tipo_interior",
				"nro_interior",
				"cond_titular",
				"cond_especialpredio",
				"nro_resolucion",
				"porcent",
				"finicio",
				"fvencimiento",
				"clasif_predio",
				"pred_catastral",
				"uso_predio",
				"estructuracion",
				"zonificacion",
				"area_titulo",
				"area_declarada",
				"area_verificada",
				"medcamp_frente",
				"medcamp_derecha",
				"medcamp_izquierda",
				"medcamp_fondo",
				"medtit_frente",
				"medtit_derecha",
				"medtit_izquierda",
				"medtit_fondo",
				"colcampo_frente",
				"colcampo_derecha",
				"colcampo_izquierda",
				"colcampo_fondo",
				"coltit_frente",
				"coltit_derecha",
				"coltit_izquierda",
				"coltit_fondo",
				"luz",
				"agua",
				"phone",
				"desague",
				"sumluz",
				"sumagua",
				"sumphone",
				"fuente_info",
				"area_ocupada",
				"limpfrente_izquierda",
				"limpfrente_derecha",
				"limpfrente_fondo",
				"limpfrente_frente",
				"total_limpfrenteuc",
				"gas",
				"cable",
				"internet",
				"nro_hombres",
				"nro_mujeres",
				"nro_ninios",
				"nro_adulmayor",
				"nro_discapacita"
			],
			'UP02' => [
				"terreno_legal",
				"terreno_fisico",
				"construc_legal",
				"construc_fisico",
				"notaria",
				"kardex",
				"fecha_inscpublica",
				"tpartida_reg",
				"numeros",
				"forja",
				"asiento",
				"fecha_insc_enregpredio",
				"decla_fabrica",
				"inscfabrica",
				"fecha_inscfabrica",
				"evalpredio",
				"enlote_colin",
				"enjardin_aislam",
				"enarea_public",
				"enarea_intang",
				"cond_declara",
				"estado_llenado",
				"nro_habitantes",
				"nro_familias",
				"mantenimiento",
				"observaciones",
				"declarante",
				"dni_declarante",
				"fecha_declarante",
				"tec_catastral",
				"cod_tec_catastral",
				"fecha_tec_catastral",
				"supervisor",
				"cod_supervisor",
				"fecha_supervisor",
				"verif_catastral",
				"cod_verif_catastral",
				"fecha_verif_catastral",
				"cod_usuario",
				"tec_calidad",
				"cod_tec_calidad",
				"fecha_tec_calidad"
			]
		];

		foreach ($ups[$up] as $key) 
		{
			if ( isset($row[$key]) ) 
			{
				if ( ! is_null($row[$key]) AND ! empty(trim($row[$key])) )
					$data->{$key} = trim($row[$key]);
			}
		}

		return $data;
	}

	/**
	 * Permite formatear la fecha en formato DD/MM/YYYY.
	 *
	 * La función compara la fecha si la fecha es de 1900 la deja vacía caso contrario
	 * aplica el formateo dejando solo el dia, mes y año en el formato DD/MM/YYYY.
	 * @param  object $up_obj       sección del objeto que buscará la información.
	 * @param  array $datekeys_arr Array con las claves que contienen fecha
	 * @return object               El mismo objeto con las fechas formateadas
	 */
	private static function formater_datetime($up_obj, $datekeys_arr)
	{
		foreach ($datekeys_arr as $key) 
		{
			if ( isset($up_obj->{$key}) ) {
				$dateval = new Datetime($up_obj->{$key});

				if ( $dateval->format('Y') != '1900' ) {
					$up_obj->{$key} = $dateval->format('d/m/Y');
				} else {
					unset($up_obj->{$key});
				}
			}
			

		}

		return $up_obj;
	}

}

/* End of file QIndividual.php */
/* Location: ./ficha/components/QIndividual.php */