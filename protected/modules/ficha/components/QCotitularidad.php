<?php 
/**
 * Clase que se encarga de interactuar con la base de datos, para el manejo de los datos de la ficha de cotitularidad.
 * 
 * @author José Alejandro Vilchez Moreno <joalvm@gmail.com>
 * @package Catastro\modules\ficha\components
 */
class QCotitularidad {

	/**
	 * Primera paso en la acción de guardar datos de la ficha
	 * @return boolean Confirmación de la acción
	 */
	public static function guardarDatosFicha_step_01() 
	{
		$sql = "EXEC UPDATE_COTITULARIDAD :m_id_ficha, :m_id_codcat, :m_nro_cotitulares, :m_cuc, :m_chc, :m_fichas_lote, 
						:m_cond_declarante, :m_est_llenado, :m_observaciones, :m_firma_dec, :m_id_dec, :m_fecha_dec, :m_firma_tec, 
						:m_id_tec, :m_fecha_tec, :m_firma_sup, :m_id_sup, :m_fecha_sup, :m_firma_ver, :m_id_ver, :m_fecha_ver, 
						:firma_tec_calidad, :id_tec_calidad, :fecha_tec_calidad";

		$head = UCotitularidad::getCookie(CCotitularidad::HEAD_NAME_CK);
		$up 	= UCotitularidad::getCookie(CCotitularidad::UP01_NAME_CK);

		$nro_ficha_lote = (isset($head->nro_ficha_lote_first) ? $head->nro_ficha_lote_first : '') . '/' ;
		$nro_ficha_lote .= (isset($head->nro_ficha_lote_second) ? $head->nro_ficha_lote_second : '');

		$command = Yii::app()->db->createCommand($sql);

		//cabecera
		$command->bindValue(':m_id_ficha', $head->id_ficha, PDO::PARAM_STR);
		$command->bindValue(':m_id_codcat', $head->cod_catastro, PDO::PARAM_STR);
		$command->bindValue(':m_nro_cotitulares', (isset($up->total_cotitular) ? (int)$up->total_cotitular: '' ), PDO::PARAM_STR);
		$command->bindValue(':m_cuc', '', PDO::PARAM_STR);
		$command->bindValue(':m_chc', isset($head->cod_hoja_catastral) ? $head->cod_hoja_catastral : '', PDO::PARAM_STR);
		$command->bindValue(':m_fichas_lote', $nro_ficha_lote, PDO::PARAM_STR);

		//cuerpo
		$command->bindValue(':m_cond_declarante', (isset($up->cond_declara) ? $up->cond_declara: '' ), PDO::PARAM_STR);
		$command->bindValue(':m_est_llenado', (isset($up->estado_llenado) ? $up->estado_llenado: '' ), PDO::PARAM_STR);
		$command->bindValue(':m_observaciones', (isset($up->observaciones) ? mb_strtoupper($up->observaciones, 'UTF-8'): '' ), PDO::PARAM_STR);
		$command->bindValue(':m_firma_dec', (isset($up->declarante) ? $up->declarante: '0' ), PDO::PARAM_STR);
		$command->bindValue(':m_id_dec', (isset($up->dni_declarante) ? $up->dni_declarante: '' ), PDO::PARAM_STR);
		$command->bindValue(':m_fecha_dec', (isset($up->fecha_declarante) ? Fecha::format($up->fecha_declarante, 'd/m/Y', 'Y-m-d'): '1900-01-01' ), PDO::PARAM_STR);
		$command->bindValue(':m_firma_tec', (isset($up->tec_catastral) ? $up->tec_catastral: '0' ), PDO::PARAM_STR);
		$command->bindValue(':m_id_tec', (isset($up->cod_tec_catastral) ? $up->cod_tec_catastral: '' ), PDO::PARAM_STR);
		$command->bindValue(':m_fecha_tec', (isset($up->fecha_tec_catastral) ? Fecha::format($up->fecha_tec_catastral, 'd/m/Y', 'Y-m-d'): '1900-01-01' ), PDO::PARAM_STR);
		$command->bindValue(':m_firma_sup', (isset($up->supervisor) ? $up->supervisor: '0' ), PDO::PARAM_STR);
		$command->bindValue(':m_id_sup', (isset($up->cod_supervisor) ? $up->cod_supervisor: '' ), PDO::PARAM_STR);
		$command->bindValue(':m_fecha_sup', (isset($up->fecha_supervisor) ? Fecha::format($up->fecha_supervisor, 'd/m/Y', 'Y-m-d'): '1900-01-01' ), PDO::PARAM_STR);
		$command->bindValue(':m_firma_ver', (isset($up->verif_catastral) ? $up->verif_catastral: '0' ), PDO::PARAM_STR);
		$command->bindValue(':m_id_ver', (isset($up->cod_verif_catastral) ? $up->cod_verif_catastral: '' ), PDO::PARAM_STR);
		$command->bindValue(':m_fecha_ver', (isset($up->fecha_verif_catastral) ? Fecha::format($up->fecha_verif_catastral, 'd/m/Y', 'Y-m-d'): '1900-01-01' ), PDO::PARAM_STR);
		$command->bindValue(':firma_tec_calidad', (isset($up->tec_calidad) ? $up->tec_calidad: '0' ), PDO::PARAM_STR);
		$command->bindValue(':id_tec_calidad', (isset($up->cod_tec_calidad) ? $up->cod_tec_calidad: '' ), PDO::PARAM_STR);
		$command->bindValue(':fecha_tec_calidad', (isset($up->fecha_tec_calidad) ? Fecha::format($up->fecha_tec_calidad, 'd/m/Y', 'Y-m-d'): '1900-01-01' ), PDO::PARAM_STR);
		
		$result  = $command->execute();

		$command->reset();

		return $result;
	}

	/**
	 * Ejecuta procedimientos almacenados para las acciones de Creación,
	 * modificación de personas.
	 * @param  object $params Toda la información con los datos a minipular.
	 * @param  string $action Clave para entender la acción que se realizará.
	 * @return boolean         Confirmación de la acción
	 */
	public static function mantenimientoPersonas($params, $action)
	{
		$sql = 'EXEC ';

		switch ($action) {
			case 'create':
			case 'update':

				if ( $params['tipo_documento'] == '01' ) 
					$sql .= "INSERT_PERSONA_SIN_DCTO ";
				else
					$sql .= "usp_Persona ";

				$sql .= ":m_ndoc, :m_tdoc, :m_tper, :m_nomb, :m_mater, :m_pater, :m_tip_per_jur, :m_estado_civil";

				$command = Yii::app()->db->createCommand($sql);

				$command->bindValue(":m_ndoc", $params['nro_documento'], PDO::PARAM_STR);
				$command->bindValue(":m_tdoc", $params['tipo_documento'], PDO::PARAM_STR);
				$command->bindValue(":m_tper", $params['tipo_titular'], PDO::PARAM_STR);
				$command->bindValue(":m_nomb", mb_strtoupper($params['nombres'], 'UTF-8'), PDO::PARAM_STR);
				$command->bindValue(":m_mater", mb_strtoupper($params['ape_materno'], 'UTF-8'), PDO::PARAM_STR);
				$command->bindValue(":m_pater", mb_strtoupper($params['ape_paterno'], 'UTF-8'), PDO::PARAM_STR);
				$command->bindValue(":m_tip_per_jur", $params['persona_juridica'], PDO::PARAM_STR);
				$command->bindValue(":m_estado_civil", $params['estado_civil'], PDO::PARAM_STR);

				break;

			case 'delete':

				$sql .= "";

				$command = Yii::app()->db->createCommand($sql);

				break;
		}

		$result  = $command->execute();

		return $result;
	}

	/**
	 * Consulta la información completa de un titular asignado a una ficha.
	 * @param  string $ficha      ID ficha.
	 * @param  string $id_persona ID persona.
	 * @return object             Datos del titular.
	 */
	public static function getTitularInfo($ficha, $id_persona)
	{
		$sql = "SELECT 
							tipo , tip_persona , dbo.fn_MultiplicaSaldo('07', df.tip_persona , 0) persona , 
							FORMA_ADQ , FECHA_ADQ , PORCENTAJE_PERTENENCIA , TIP_DOC , NRO_DOC , COD_CONTRIBUY ,  
							NOMBRES , APE_PATERNO , APE_MATERNO , TIP_PERSONA_JURIDICA , ID_UBI_GEO , 
							COD_HAB_URB , NOM_HAB_URB , ZN_SC_ETAPA, cod_via , nom_via , NRO_MUNICIPAL , 
							NRO_INTERIOR , NOMBRE_EDIFICA , MZA_URBANA , LOTE_URBANO, SUB_LOTE, COD_CAT, 
							ID_ficha, ID , ESTADO_CIVIL , TELEFONO , FAX , CORREO_ELECT , CONDICION , 
							NRO_RESOLUCION , NRO_BOLETA_PENSION , FECHA_INICIO , FECHA_VENCIMIENTO  
						FROM DATOS_domicilio_fiscal_principal DF  
						WHERE 
								ID_FICHA  = :ID_FICHA 
						AND id_persona = :id_persona";

		$command = Yii::app()->db->createCommand($sql);

		$command->bindValue(":ID_FICHA", (string)$ficha , PDO::PARAM_STR);
		$command->bindValue(":id_persona", (string)$id_persona , PDO::PARAM_STR);

		$row = $command->queryRow();

		return $row;
	}

	/**
	 * Consulta la información basica de una persona en base a filtros
	 * @param  string $rel    Clave que filtra la busqueda entre N° de documento o nombres.
	 * @param  int $tipo   Clave que filtra la busqueda por persona juridica o natural
	 * @param  string $search Texto para la busqueda de persona
	 * @return array         Lista con el resultado de la busqueda
	 */
	public static function showPersonasFicha($rel, $tipo, $search)
	{
		$sql = "EXEC ";

		if ( $rel == 'doc' ) {
			$sql .= "SP_ShowPersonasFichaDocumento :TIPO, :ID_FICHA";
		} else if ( $rel == 'name' ) {
			$sql .= "SP_ShowPersonasFicha :TIPO, :ID_FICHA";
		}

		$command = Yii::app()->db->createCommand($sql);

		$command->bindValue(":TIPO", $tipo , PDO::PARAM_STR);
		$command->bindValue(":ID_FICHA", ($search . '%'), PDO::PARAM_INT);

		$table = $command->queryAll();

		return $table;
	}

	/**
	 * Obtiene toda la información de la ficha para el proceso de modificación
	 * @param  string $id_ficha ID de ficha
	 * @return object           objecto formateado con la información obtenida
	 */
	public static function get_data_ficha($id_ficha)
	{
		$sql = "SELECT 
							f.ID_FICHA id_ficha,
							f.COD_CAT cod_catastro,
							f.DC dc,
							f.NRO_FICHA nro_ficha,
							ISNULL(f.CUC,'') cod_cud,
							f.COD_HOJA_CAT cod_hoja_catastral,
							f.NRO_FICHA_LOTE nro_ficha_lote,
							isnull(f.CONDIC_DECLARA , '') cond_declara,
							isnull( f.ESTADO_LLENADO, '') estado_llenado,
							isnull(fc.OBSERVACIONES , '' ) observaciones,
							isnull(fc.NRO_COTITULARES,'') total_cotitular,
							c.FIRMA_DECLARANTE declarante, 
							c.ID_DECLARANTE dni_declarante,  
							c.FECHA_DECLARANTE fecha_declarante,
							c.FIRMA_TEC_CATASTRAL tec_catastral, 
							c.ID_TEC_CATASTRAL cod_tec_catastral, 
							c.FECHA_TEC_CATASTRAL fecha_tec_catastral, 
							c.FIRMA_SUPERVISOR supervisor, 
							c.ID_SUPERVISOR cod_supervisor, 
							c.FECHA_SUPERVISOR fecha_supervisor, 
							c.FIRMA_VERIF_CATASTRAL verif_catastral,
							c.ID_VERIF_CATASTRAL cod_verif_catastral,
							c.FECHA_VERIF_CATASTRAL fecha_verif_catastral,
							c.FIRMA_TEC_CALIDAD tec_calidad,
							c.ID_TEC_CALIDAD cod_tec_calidad,
							c.FECHA_TEC_CALIDAD fecha_tec_calidad
						FROM 
							FICHAS f,
							FICHAS_CONDOMINIO fc,
							CONTROL_DE_FICHA c
						WHERE 
							f.TIP_FICHA = '02'  
						AND f.ID_FICHA = fc.ID_FICHA 
						AND f.ID_FICHA = c.ID_FICHA
						AND f.ID_FICHA = :id_ficha";

		$command = Yii::app()->db->createCommand($sql);

		$command->bindValue(":id_ficha", $id_ficha, PDO::PARAM_STR);

		$row = $command->queryRow();

		if ( ! $row )
			return [];

		$nrow = new stdClass;
		$head = new stdClass;
		$up01 = new stdClass;

		$head->nro_ficha = trim($row['nro_ficha']);
		$head->id_ficha = trim($row['id_ficha']);
		$head->cod_catastro = trim($row['cod_catastro']);
		$head->cod_hoja_catastral = trim($row['cod_hoja_catastral']);

		$lotes_arr = explode('/', $row['nro_ficha_lote']);
		$head->nro_ficha_lote_first = (count($lotes_arr) == 2 OR count($lotes_arr) == 1) ? $lotes_arr[0] : '';
		$head->nro_ficha_lote_second = (count($lotes_arr) == 2) ? $lotes_arr[1] : '';

		$head->cod_ref_catastral = new stdClass;

		$head->cod_ref_catastral->ubigeo = mb_substr($row['cod_catastro'], 0, 6);
    $head->cod_ref_catastral->sector = mb_substr($row['cod_catastro'], 10, 2);
    $head->cod_ref_catastral->manzana = mb_substr($row['cod_catastro'], 12, 3);
    $head->cod_ref_catastral->lote = mb_substr($row['cod_catastro'], 15, 3);
    $head->cod_ref_catastral->edificio = mb_substr($row['cod_catastro'], 18, 2);
    $head->cod_ref_catastral->entrada = mb_substr($row['cod_catastro'], 20, 2);
    $head->cod_ref_catastral->piso = mb_substr($row['cod_catastro'], 22, 2);
    $head->cod_ref_catastral->unidad = mb_substr($row['cod_catastro'], 24, 3);
    $head->cod_ref_catastral->dc = $row['dc'];

    $up01->total_cotitular = $row['total_cotitular']; 
    $up01->cond_declara = $row['cond_declara']; 
    $up01->estado_llenado = $row['estado_llenado']; 
    $up01->observaciones = $row['observaciones']; 
    $up01->declarante = $row['declarante']; 
    $up01->dni_declarante = $row['dni_declarante'];
    $up01->fecha_declarante = $row['fecha_declarante'];
    $up01->supervisor = $row['supervisor']; 
    $up01->cod_supervisor = $row['cod_supervisor'];
    $up01->fecha_supervisor = $row['fecha_supervisor'];
    $up01->tec_catastral = $row['tec_catastral']; 
    $up01->cod_tec_catastral = $row['cod_tec_catastral'];
    $up01->fecha_tec_catastral = $row['fecha_tec_catastral'];
    $up01->tec_calidad = $row['tec_calidad']; 
    $up01->cod_tec_calidad = $row['cod_tec_calidad'];
    $up01->fecha_tec_calidad = $row['fecha_tec_calidad'];
    $up01->verif_catastral = $row['verif_catastral']; 
    $up01->cod_verif_catastral = $row['cod_verif_catastral'];
    $up01->fecha_verif_catastral = $row['fecha_verif_catastral'];

    $up01 = self::formater_datetime($up01, [
    	'fecha_declarante', 
    	'fecha_supervisor', 
    	'fecha_tec_catastral', 
    	'fecha_tec_calidad', 
    	'fecha_verif_catastral'
    ]);

    $nrow->head = $head;
    $nrow->up01 = $up01;

		return $nrow;
	}

	/**
	 * Permite formatear la fecha en formato DD/MM/YYYY.
	 *
	 * La función compara la fecha si la fecha es de 1900 la deja vacía caso contrario
	 * aplica el formateo dejando solo el dia, mes y año en el formato DD/MM/YYYY.
	 * @param  object $up_obj       sección del objeto que buscará la información.
	 * @param  array $datekeys_arr Array con las claves que contienen fecha
	 * @return object               El mismo objeto con las fechas formateadas
	 */
	private static function formater_datetime($up_obj, $datekeys_arr)
	{
		foreach ($datekeys_arr as $key) 
		{
			$dateval = new Datetime($up_obj->{$key});

			if ( $dateval->format('Y') != '1900' ) {
				$up_obj->{$key} = $dateval->format('d/m/Y');
			} else {
				unset($up_obj->{$key});
			}

		}

		return $up_obj;
	}

}