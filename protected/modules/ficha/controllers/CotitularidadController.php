<?php 
/**
 * Clase que se encarga de manejar las diferentes vistas y recursos
 * de la ficha de cotitularidad
 * 
 * @author José Alejandro Vilchez Moreno <joalvm@gmail.com>
 * @package Catastro\modules\ficha\controllers
 */
class CotitularidadController extends Auth {

	/**
	 * Muestra la página de inicio donde se listan todas las fichas de cotitularidad.
	 * @return void
	 */
	public function actionIndex()
	{
		$data['action'] = 'index';
		$this->render('index', $data);
	}

	/**
	 * Muestra la página de creación/Modificación, en modo creación, de las fichas de cotitularidad. 
	 *
	 * El metodo verifica si existe en cache una coockie para ficha de cotitlaridad, en caso de haberlo
	 * avisará de su existencia y no permitirá que se cree una ficha nueva.
	 * @return void
	 */
	public function actionCrear()
	{
		$data['action'] = 'create';
		$data['isOnlyView'] = FALSE;

		$head = UCotitularidad::getCookie(CCotitularidad::HEAD_NAME_CK);

		if ( isset($head->action) && $head->action == 'update' ) 
		{

			$text = [
				'ficha' => 'Ficha de cotitularidad',
				'url' => Yii::app()->createUrl('/ficha/cotitularidad/modificar/ficha/' . $head->id_ficha),
				'texto' => 'El sistema a detectado que actualmente, en este navegador, se esta editando una ficha de cotitularidad, 
					no se podran crear nuevas fichas mientras se esté modificando una'
			];

			$this->render('../ficha_cache', $text);
			
		} 
		else 
		{
			unset(Yii::app()->request->cookies[CCotitularidad::UP01_NAME_CK]);
			unset(Yii::app()->request->cookies[CCotitularidad::HEAD_NAME_CK]);

			$this->render('crear', $data);
		}
	}

	/**
	 * Muestra la página de creación/Modificación, en modo modificación, de las fichas de cotitularidad.
	 *
	 * El metodo consulta el ID de ficha y verifica que no haya otra ficha en cache, en caso de haberlo 
	 * y de que esta sea de modo creación, remueve la cache y la reemplaza por la de la ficha encontrada 
	 * y en caso de que sea de modo modificación avisará para que la cache no sea modificada.
	 * @return void
	 */
	public function actionModificar($ficha)
	{
		$head = UCotitularidad::getCookie(CCotitularidad::HEAD_NAME_CK);
		$row = QCotitularidad::get_data_ficha($ficha);
		$data['action'] = 'update';
		$data['isOnlyView'] = FALSE;

		//EN CASO NO ENCUENTRE LA FICHA
		if ( empty($row) ) 
		{
			throw new CHttpException(404,'La ficha no ha sido encontrada');
		} 
		else 
		{
			//SI LA COOKIE EXISTE Y LOS ID'S DE FICHA SON DIFERENTE
			if ( isset($head->id_ficha) AND $head->id_ficha != $ficha ) 
			{
				$text = [
					'ficha' => 'Ficha de cotitularidad',
					'url' => Yii::app()->createUrl('/ficha/cotitularidad/modificar/ficha/' . $head->id_ficha),
					'texto' => 'El sistema a detectado que actualmente, en este navegador, 
						se esta editando una ficha de cotitularidad, no se podran crear o 
						modificar nuevas fichas mientras una ficha se encuentre en proceso de modificación'
				];

				$this->render('../ficha_cache', $text);
			} 
			else 
			{
				//BORRANDO ANTIGUA COOKIES
				unset(Yii::app()->request->cookies[CCotitularidad::UP01_NAME_CK]);
				unset(Yii::app()->request->cookies[CCotitularidad::HEAD_NAME_CK]);

				//AGREGANDO LA ACCION EN LA QUE SE ESTA INVOCANDO LA COOKIE
				$row->head->action = $data['action'];

				//CREANDO LAS NUEVAS COOKIES
				UCotitularidad::generateCookie(CCotitularidad::HEAD_NAME_CK, $row->head);
				UCotitularidad::generateCookie(CCotitularidad::UP01_NAME_CK, $row->up01);

				$this->render('crear', $data);
			}
		}
	}

	public function actionView($ficha)
	{
		$row = QCotitularidad::get_data_ficha($ficha);

		if ( empty((array)$row) ) 
		{
			throw new CHttpException(404,'La ficha no ha sido encontrada');
		} 
		else 
		{
			//AGREGANDO LA ACCION EN LA QUE SE ESTA INVOCANDO LA COOKIE
			$row->action = 'update';
			$row->isOnlyView = TRUE;

			unset(Yii::app()->request->cookies[CCotitularidad::UP01_NAME_CK]);
			unset(Yii::app()->request->cookies[CCotitularidad::HEAD_NAME_CK]);

			$this->render('crear', (array)$row);
		}
	}

	/**
	 * Metodo que permite generar la ficha con sus datos basicos.
	 *
	 * El metodo realiza la verificación de la existencia del numero de ficha y del codigo referencial catastral,
	 * crea el Id de ficha y el Codigo de catastro luego realiza el registro de la ficha y la creación de las cookies
	 * que contendran toda la información que posteriormente será registrada.
	 * @return [Object JSON] Respuesta de tipo JSON que contiene el ID de ficha y el Codigo catastro 
	 */
	public function actionGenerarficha()
	{
		try 
		{
			if (!Yii::app()->request->isAjaxRequest)
				throw new Exception("El metodo no esta permitido", 403);

			//vALIDANDO SI EXISTE EL NUMERO DE FICHA INDIVIDUAL EN EL PROCESO SELECCIONADO
			$exists_ficha = QPrincipal::validar_ficha($_POST['nro_ficha'], '02');
			if ( $exists_ficha > 0 ) 
				throw new Exception("YA EXISTE UNA FICHA COTITULAR CON EL N° " . $_POST['nro_ficha'] . " PARA EL AÑO DE REGISTRO " . Yii::app()->user->proceso['selected'], 200);
			
			//GENERANDO TODO EL CODIGO CATASTRAL
			$cod_catastro = QPrincipal::get_CodigoCatastro($_POST['cod_ref_catastral']);

			//vALIDAR SI EL CODIGO DE REFERENCIA CATASTRAL YA EXISTE PARA LA FICHA INDIVIDUAL
			//$exists_cod_catastro = QPrincipal::validar_CodigoCatastral($cod_catastro, '02');
			//if ( $exists_cod_catastro > 0 )
				//throw new Exception('EL CÓDIGO CATASTRAL "'. $cod_catastro . '" YA HA SIDO GENERADO, VERIFIQUE SUS DATOS.', 200);

			//gENERAR EL ID DE FICHA
			$id_ficha = QPrincipal::get_IdFicha($_POST['cod_ref_catastral']['ubigeo'], $_POST['nro_ficha'], '02');

			$dc = $_POST['cod_ref_catastral']['dc'];

			//INSERTAR EN LA BASE DE DATOS LA DATOS DE LA FICHA Y GENERARLA
			$result = QPrincipal::generarFicha($id_ficha, $cod_catastro, $dc, 'A', 'I', Yii::app()->user->id_persona_catas);

			$message = 'ERROR AL MOMENTO DE GENERAR LA FICHA, VUELVE A INTENTARLO';

			if ( $result ) 
			{
				$message = 'SE GENERÓ LA FICHA ' . $id_ficha . ', CONTINUE CON EL PROCESO DE REGISTRO DE DATOS.';
			}

			$data['id_ficha'] = $id_ficha;
			$data['cod_catastro'] = $cod_catastro;

			$head = [];

			$head['nro_ficha'] = $_POST['nro_ficha'];

			if ( !empty($_POST['nro_ficha_lote_first']) )
				$head['nro_ficha_lote_first'] = $_POST['nro_ficha_lote_first']; 

	    if ( !empty($_POST['nro_ficha_lote_second']) )
	    	$head['nro_ficha_lote_first'] = $_POST['nro_ficha_lote_first']; 

	    if ( !empty($_POST['cod_hoja_catastral']) )
	    	$head['cod_hoja_catastral'] = $_POST['cod_hoja_catastral']; 

	    $head['cod_ref_catastral'] = $_POST['cod_ref_catastral'];

			$head['id_ficha'] = $id_ficha;
			$head['cod_catastro'] = $cod_catastro;

			UCotitularidad::generateCookie(CCotitularidad::HEAD_NAME_CK, $head);
			UCotitularidad::generateCookie(CCotitularidad::UP01_NAME_CK, (new stdClass));

			JSON::response((! ((boolean)$result)), 200, $message, $data);
		} 
		catch (Exception $ex) 
		{
			JSON::response(TRUE, $ex->getCode(), $ex->getMessage(), []);
		}
	}

	/**
	 * Metodo que permite guardar toda la información de la ficha despues que la ficha ha sido generada.
	 *
	 * El metodo ejecuta todos los procedimientos almacenados para insertar la información luego borra de cache
	 * toda la información asignada para poder crear una ficha nueva.
	 * @return void
	 */
	public function actionGuardardatosficha()
	{
		try 
		{
			if (!Yii::app()->request->isAjaxRequest)
				throw new Exception("El metodo no esta permitido", 403);

			$head = UCotitularidad::getCookie(CCotitularidad::HEAD_NAME_CK);

			if ( ! isset($head->id_ficha) ) {
				throw new Exception("El sistema a detectado que la información almacenda a sido alterada, el proceso a sido cancelado. Volviendo a inicio...", 201);
			}

			$step01 = QCotitularidad::guardarDatosFicha_step_01();

			$message = 'ERROR AL MOMENTO DE EJECUTAR LA ACCIÓN, VUELVA A INTENTARLO.';
			$confirm = FALSE;

			if ( ($step01) ) 
			{
				unset(Yii::app()->request->cookies[CCotitularidad::HEAD_NAME_CK]);
				unset(Yii::app()->request->cookies[CCotitularidad::UP01_NAME_CK]);

				$message = 'LA ACCIÓN SE EJECUTO SATISFATORIAMENTE.';
				$confirm = FALSE;
			} 
			else 
			{
				$message = 'ES POSIBLE QUE NO TODA LA INFORMACIÓN SE HAYA REGISTRADO SATISFACTORIAMENTE, VUELVE A INTENTARLO';
				$confirm = TRUE;
			}

			JSON::response($confirm, 200, $message);
			
		} 
		catch (Exception $ex) 
		{
			JSON::response(TRUE, $ex->getCode(), $ex->getMessage(), []);
		}
	}

	/**
	 * Insert/Update de personas
	 * @return void
	 */
	public function actionMantenimientopersonas()
	{
		$allowKeys = [
			'tipo_titular', 'tipo_documento', 'nro_documento', 'nombres', 'ape_paterno', 
			'ape_materno', 'persona_juridica', 'estado_civil', 'action'
		];
		$postKeys = array_keys($_POST);

		try 
		{
			if (!Yii::app()->request->isAjaxRequest)
				throw new Exception("El metodo no esta permitido", 403);

			$diff = array_diff($postKeys, $allowKeys);
      if (!empty($diff))
          throw new Exception("LOS PARAMETROS NO SON LOS CORRECTOS, ACCIÓN CANCELADA", 400);


      $confirm = QCotitularidad::mantenimientoPersonas($_POST, $_POST['action']);

      $message = 'ERROR AL MOMENTO DE EJECUTAR LA ACCIÓN, VUELVA A INTENTARLO.';

			if ( $confirm )
				$message = 'LA ACCIÓN SE EJECUTO SATISFATORIAMENTE.';

			JSON::response((!$confirm), 200, $message);
		} 
		catch (Exception $ex) 
		{
			JSON::response(TRUE, $ex->getCode(), $ex->getMessage());
		}
	}

	/**
	 * Asigna a la cache los campos que se manipulan en la ficha
	 * @return void
	 */
	public function actionAsignardato()
	{
		try {

			if (!Yii::app()->request->isAjaxRequest)
				throw new Exception("El metodo no esta permitido", 403);

			$head = UCotitularidad::getCookie(CCotitularidad::HEAD_NAME_CK);

			if ( ! isset($head->id_ficha) ) {
				throw new Exception("El sistema a detectado que la información almacenda a sido alterada, el proceso a sido cancelado. Volviendo a inicio...", 201);
			}

			$key = array_keys($_POST);

			UCotitularidad::setCookie($key[0], $_POST[$key[0]]);
			
		} catch (Exception $ex) {
			JSON::response(TRUE, $ex->getCode(), $ex->getMessage());
		}
	}

	/**
	 * Acción para reiniciar una ficha y borrar todos los datos en cache
	 * @return void
	 */
	public function actionResetearficha()
	{
		try {

			if (!Yii::app()->request->isAjaxRequest)
				throw new Exception("El metodo no esta permitido", 403);

			unset(Yii::app()->request->cookies[CCotitularidad::UP01_NAME_CK]);
			unset(Yii::app()->request->cookies[CCotitularidad::HEAD_NAME_CK]);

			JSON::response( FALSE, 200, 'OK');
			
		} catch (Exception $ex) {
			JSON::response(TRUE, $ex->getCode(), $ex->getMessage());
		}
	}

	/**
	 * Metodo que se implementa para generar una observación.
	 *
	 * El metodo registra en la db toda la observacion y limpia la ficha
	 * para que se pueda generar una nueva ficha
	 * @return void
	 */
	public function actionGenerarobservacion() 
	{
		try 
		{
			if (!Yii::app()->request->isAjaxRequest)
				throw new Exception("El metodo no esta permitido", 403);

			$confirm = QPrincipal::generateObservacion($_POST);
			$message = 'No se pudo generar la observación';

			if ( $confirm ) {

				$message = 'La observación a sido generada, debe crear una nueva ficha.';

				unset(Yii::app()->request->cookies[CCotitularidad::UP01_NAME_CK]);
				unset(Yii::app()->request->cookies[CCotitularidad::HEAD_NAME_CK]);

			}

			JSON::response(!$confirm, 200, $message);

		} 
		catch (Exception $ex) 
		{
			JSON::response(TRUE, $ex->getCode(), $ex->getMessage(), []);
		}
	}

}
