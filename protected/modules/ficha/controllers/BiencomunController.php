<?php
/**
 * Clase que se encarga de manejar las diferentes vistas y recursos
 * de la ficha de bien común
 *
 * @author José Alejandro Vilchez Moreno <joalvm@gmail.com>
 * @package Catastro\modules\ficha\controllers
 */
class BiencomunController extends Auth {

	/**
	 * Muestra la página de inicio donde se listan todas las fichas de bien común.
	 * @return void
	 */
	public function actionIndex()
	{
		$this->render('index');
	}

	/**
	 * Muestra la página de creación/Modificación, en modo creación, de las fichas de bien común.
	 *
	 * El metodo verifica si existe en cache una coockie para ficha de bien común, en caso de haberlo
	 * avisará de su existencia y no permitirá que se cree una ficha nueva.
	 * @return void
	 */
	public function actionCrear()
	{
		$data['action'] = 'create';
		$data['isOnlyView'] = FALSE;
		$head = UBiencomun::getCookie(CBiencomun::HEAD_NAME_CK);

		if ( isset($head->action) && $head->action == 'update' ) 
		{
			$text = [
				'ficha' => 'Ficha de bien común',
				'url' => Yii::app()->createUrl('/ficha/biencomun/modificar/ficha/' . $head->id_ficha),
				'texto' => 'El sistema a detectado que actualmente, en este navegador,
					se esta editando una ficha de bien común, no se podran crear nuevas
					fichas mientras se esté modificando una'
			];

			$this->render('../ficha_cache', $text);
		} 
		else 
		{
			unset(Yii::app()->request->cookies[CBiencomun::UP01_NAME_CK]);
			unset(Yii::app()->request->cookies[CBiencomun::UP02_NAME_CK]);
			unset(Yii::app()->request->cookies[CBiencomun::HEAD_NAME_CK]);

			$this->render('crear', $data);
		}
	}

	/**
	 * Muestra la página de creación/Modificación, en modo modificación, de las fichas de bien común.
	 *
	 * El metodo consulta el ID de ficha y verifica que no haya otra ficha en cache, en caso de haberlo
	 * y de que esta sea de modo creación, remueve la cache y la reemplaza por la de la ficha encontrada
	 * y en caso de que sea de modo modificación avisará para que la cache no sea modificada.
	 * @return void
	 */
	public function actionModificar($ficha)
	{
		$row = QBiencomun::get_DataFicha($ficha);
		$head = UBiencomun::getCookie(CBiencomun::HEAD_NAME_CK);
		$data['action'] = 'update';
		$data['isOnlyView'] = FALSE;

		//EN CASO NO ENCUENTRE LA FICHA
		if ( empty((array)$row) ) 
		{
			throw new CHttpException(404,'La ficha no ha sido encontrada');
		} 
		else 
		{
			//SI LA COOKIE EXISTE Y LOS ID'S DE FICHA SON DIFERENTE
			if ( isset($head->id_ficha) AND $head->id_ficha != $ficha ) 
			{
				$text = [
					'ficha' => 'Ficha de bien común',
					'url' => Yii::app()->createUrl('/ficha/biencomun/modificar/ficha/' . $head->id_ficha),
					'texto' => 'El sistema a detectado que actualmente, en este navegador,
						se esta editando una ficha de bien común, no se podran crear o modificar
						nuevas fichas mientras una ficha se encuentre en proceso de modificación'
				];

				$this->render('../ficha_cache', $text);
			} 
			else 
			{
				//BORRANDO ANTIGUA COOKIES
				unset(Yii::app()->request->cookies[CBiencomun::HEAD_NAME_CK]);
				unset(Yii::app()->request->cookies[CBiencomun::UP01_NAME_CK]);
				unset(Yii::app()->request->cookies[CBiencomun::UP02_NAME_CK]);

				//AGREGANDO LA ACCION EN LA QUE SE ESTA INVOCANDO LA COOKIE
				$row->HEAD->action = $data['action'];

				//CREANDO LAS NUEVAS COOKIES
				UBiencomun::generateCookie(CBiencomun::HEAD_NAME_CK, $row->HEAD);
				UBiencomun::generateCookie(CBiencomun::UP01_NAME_CK, $row->UP01);
				UBiencomun::generateCookie(CBiencomun::UP02_NAME_CK, $row->UP02);

				$this->render('crear', $data);
			}
		}
	}

	public function actionView($ficha)
	{
		$row = QBiencomun::get_DataFicha($ficha);

		if ( empty((array)$row) ) 
		{
			throw new CHttpException(404,'La ficha no ha sido encontrada');
		} 
		else 
		{
			//AGREGANDO LA ACCION EN LA QUE SE ESTA INVOCANDO LA COOKIE
			$row->action = 'update';
			$row->isOnlyView = TRUE;
			
			unset(Yii::app()->request->cookies[CBiencomun::UP01_NAME_CK]);
			unset(Yii::app()->request->cookies[CBiencomun::UP02_NAME_CK]);
			unset(Yii::app()->request->cookies[CBiencomun::HEAD_NAME_CK]);

			$this->render('crear', (array)$row);
		}
	}


	/**
	 * Metodo que permite guardar toda la información de la ficha despues que la ficha ha sido generada.
	 *
	 * El metodo ejecuta todos los procedimientos almacenados para insertar la información luego borra de cache
	 * toda la información asignada para poder crear una ficha nueva.
	 * @return void
	 */
	public function actionGuardardatosficha()
	{
		try
		{
			if (!Yii::app()->request->isAjaxRequest)
				throw new Exception("El metodo no esta permitido", 403);

			$step01 = QBiencomun::guardarDatosFicha_step_01();
			$step02 = QBiencomun::guardarDatosFicha_step_02();

			$message = 'ERROR AL MOMENTO DE EJECUTAR LA ACCIÓN, VUELVA A INTENTARLO.';
			$confirm = FALSE;

			if ( ($step01) AND ($step02) ) {

				unset(Yii::app()->request->cookies[CBiencomun::UP01_NAME_CK]);
				unset(Yii::app()->request->cookies[CBiencomun::UP02_NAME_CK]);
				unset(Yii::app()->request->cookies[CBiencomun::HEAD_NAME_CK]);

				$message = 'LA ACCIÓN SE EJECUTO SATISFATORIAMENTE.';
				$confirm = FALSE;

			} else {
				$message = 'ES POSIBLE QUE NO TODA LA INFORMACIÓN SE HAYA REGISTRADO SATISFACTORIAMENTE, VUELVE A INTENTARLO';
				$confirm = TRUE;
			}

			JSON::response($confirm, 200, $message);

		}
		catch (Exception $ex)
		{
			JSON::response(TRUE, $ex->getCode(), $ex->getMessage(), []);
		}
	}

	/**
	 * Metodo que permite generar la ficha con sus datos basicos.
	 *
	 * El metodo realiza la verificación de la existencia del numero de ficha y del codigo referencial catastral,
	 * crea el Id de ficha y el Codigo de catastro luego realiza el registro de la ficha y la creación de las cookies
	 * que contendran toda la información que posteriormente será registrada.
	 * @return [Object JSON] Respuesta de tipo JSON que contiene el ID de ficha y el Codigo catastro
	 */
	public function actionGenerarficha()
	{
		try
		{
			if (!Yii::app()->request->isAjaxRequest)
				throw new Exception("El metodo no esta permitido", 403);

			//vALIDANDO SI EXISTE EL NUMERO DE FICHA INDIVIDUAL EN EL PROCESO SELECCIONADO
			$exists_ficha = QPrincipal::validar_ficha($_POST['nro_ficha'], '04');
			if ( $exists_ficha > 0 )
				throw new Exception("YA EXISTE UNA FICHA CATASTRAL BIEN COMÚN CON EL N° " . $_POST['nro_ficha']. " PARA EL AÑO DE REGISTRO " . Yii::app()->user->proceso['selected'], 200);

			//GENERANDO TODO EL CODIGO CATASTRAL
			$cod_catastro = QPrincipal::get_CodigoCatastro($_POST['cod_ref_catastral']);

			//vALIDAR SI EL CODIGO DE REFERENCIA CATASTRAL YA EXISTE PARA LA FICHA INDIVIDUAL
			//$exists_cod_catastro = QPrincipal::validar_CodigoCatastral($cod_catastro, '04');
			//if ( $exists_cod_catastro > 0 )
				//throw new Exception('EL CÓDIGO CATASTRAL "'. $cod_catastro . '" YA HA SIDO GENERADO, VERIFIQUE SUS DATOS.', 200);

			//gENERAR EL ID DE FICHA
			$id_ficha = QPrincipal::get_IdFicha($_POST['cod_ref_catastral']['ubigeo'], $_POST['nro_ficha'], '04');

			$dc = $_POST['cod_ref_catastral']['dc'];

			//INSERTAR EN LA BASE DE DATOS LA DATOS DE LA FICHA Y GENERARLA
			$result = QPrincipal::generarFicha($id_ficha, $cod_catastro, $dc, 'A', 'I', Yii::app()->user->id_persona_catas);

			$message = 'ERROR AL MOMENTO DE GENERAR LA FICHA, VUELVE A INTENTARLO';

			if ( $result )
			{
				$message = 'SE GENERÓ LA FICHA ' . $id_ficha . ', CONTINUE CON EL PROCESO DE REGISTRO DE DATOS.';
			}

			$data['id_ficha'] = $id_ficha;
			$data['cod_catastro'] = $cod_catastro;

			$head = [];

			$head['nro_ficha'] = $_POST['nro_ficha'];

			if ( !empty($_POST['nro_ficha_lote_first']) )
				$head['nro_ficha_lote_first'] = $_POST['nro_ficha_lote_first'];

	    if ( !empty($_POST['nro_ficha_lote_second']) )
	    	$head['nro_ficha_lote_first'] = $_POST['nro_ficha_lote_first'];

	    if ( !empty($_POST['cod_hoja_catastral']) )
	    	$head['cod_hoja_catastral'] = $_POST['cod_hoja_catastral'];

	    if ( !empty($_POST['cod_predial_rentas']) )
	    	$head['cod_predial_rentas'] = $_POST['cod_predial_rentas'];

	    if ( !empty($_POST['ua_predial_rentas']) )
	    	$head['ua_predial_rentas'] = $_POST['ua_predial_rentas'];

	    $head['cod_ref_catastral'] = $_POST['cod_ref_catastral'];

			$head['id_ficha'] = $id_ficha;
			$head['cod_catastro'] = $cod_catastro;

			UBiencomun::generateCookie(CBiencomun::HEAD_NAME_CK, $head);
			UBiencomun::generateCookie(CBiencomun::UP01_NAME_CK, (new stdClass));
			UBiencomun::generateCookie(CBiencomun::UP02_NAME_CK, (new stdClass));

			JSON::response((! ((boolean)$result)), 200, $message, $data);
		}
		catch (Exception $ex)
		{
			JSON::response(TRUE, $ex->getCode(), $ex->getMessage(), []);
		}
	}

	/**
	 * Obtiene todas las recapitulaciones de una ficha
	 * @param  string $rel ID de ficha
	 * @return void
	 */
	public function actionGetrecapitulaciones($rel)
	{
		try
		{
			if (!Yii::app()->request->isAjaxRequest)
				throw new Exception("El metodo no esta permitido", 403);

			$data['data'] = QBiencomun::getRecapitulaciones($rel);

			JSON::response(empty($data['data']), 200, 'OK', $data);
		}
		catch (Exception $ex)
		{
			JSON::response(TRUE, $ex->getCode(), $ex->getMessage(), []);
		}
	}

	/**
	 * Acción que permite buscar y realizar las recapitulaciones
	 * @return void
	 */
	public function actionRecapitular()
	{
		try
		{
			if (!Yii::app()->request->isAjaxRequest)
				throw new Exception("El metodo no esta permitido", 403);

			$result = QBiencomun::recapitular($_POST);

			$error = TRUE;

			if ( $result > 0 ) {
				$error = FALSE;
			}

			JSON::response($error, 200, 'OK', ['affected' => $result]);
		}
		catch (Exception $ex)
		{
			JSON::response(TRUE, $ex->getCode(), $ex->getMessage(), []);
		}
	}

	/**
	 * Acción que permite modificar el porcentaje de las recapitulaciones
	 * @return void
	 */
	public function actionActualizaporcentaje()
	{
		try
		{
			if (!Yii::app()->request->isAjaxRequest)
				throw new Exception("El metodo no esta permitido", 403);

			$result = QBiencomun::actualizarPorcentaje($_POST);

			$error = TRUE;
			$message = "Error al momento de modificar el porcentaje";

			if ( $result ) {

				$error 		= FALSE;
				$message 	= "Porcentaje modificado";

			}

			JSON::response($error, 200, $message);
		}
		catch (Exception $ex)
		{
			JSON::response(TRUE, $ex->getCode(), $ex->getMessage());
		}
	}

	public function actionGetrecapitulacion_edificios($rel)
	{
		try
		{
			if (!Yii::app()->request->isAjaxRequest)
				throw new Exception("El metodo no esta permitido", 403);

			$data['data'] = QBiencomun::getRecapitulacionEdificios($rel);

			JSON::response(empty($data['data']), 200, 'OK', $data);
		}
		catch (Exception $ex)
		{
			JSON::response(TRUE, $ex->getCode(), $ex->getMessage(), []);
		}
	}

	public function actionActualiza_recapitulacion_edificios($rel)
	{
		try
		{
			if (!Yii::app()->request->isAjaxRequest)
				throw new Exception("El metodo no esta permitido", 403);

			$table = QBiencomun::actualizaRecapitulacionEdificios($rel);

			JSON::response(FALSE, 200, 'OK', ['data' => $table]);
		}
		catch (Exception $ex)
		{
			JSON::response(TRUE, $ex->getCode(), $ex->getMessage());
		}
	}

	/**
	 * Insert/Update de las direcciones de la ficha.
	 * @return void
	 */
	public function actionMantenimientodirecciones()
	{
		$allowKeys = ['ficha', 'rowID', 'via', 'nro_municipal', 'tipo_puerta', 'cond_num', 'nro_certificado', 'action'];
		$postKeys = array_keys($_POST);
		try
		{
			if (!Yii::app()->request->isAjaxRequest)
				throw new Exception("El metodo no esta permitido", 403);

			$diff = array_diff($postKeys, $allowKeys);
      if (!empty($diff))
          throw new Exception("LOS PARAMETROS NO SON LOS CORRECTOS, ACCIÓN CANCELADA", 400);

			$confirm = QBiencomun::mantenimientoDirecciones($_POST, $_POST['action']);

			$message = 'ERROR AL MOMENTO DE EJECUTAR LA ACCIÓN, VUELVA A INTENTARLO.';

			if ( $confirm )
				$message = 'LA ACCIÓN SE EJECUTO SATISFATORIAMENTE.';

			JSON::response((!$confirm), 200, $message);

		}
		catch (Exception $ex)
		{
			JSON::response(TRUE, $ex->getCode(), $ex->getMessage(), []);
		}
	}

	/**
	 * Insert/Update de notaria de la ficha.
	 * @return void
	 */
	public function actionMantenimientonotaria()
	{
		$allowKeys = ['id', 'name', 'ubigeo'];
		$postKeys = array_keys($_POST);

		try
		{
			if (!Yii::app()->request->isAjaxRequest)
				throw new Exception("El metodo no esta permitido", 403);

			$diff = array_diff($postKeys, $allowKeys);
      if (!empty($diff))
          throw new Exception("LOS PARAMETROS NO SON LOS CORRECTOS, ACCIÓN CANCELADA", 400);

			$confirm = QBiencomun::mantenimientoNotaria($_POST);

			$message = 'ERROR AL MOMENTO DE EJECUTAR LA ACCIÓN, VUELVA A INTENTARLO.';

			if ( $confirm )
				$message = 'LA ACCIÓN SE EJECUTO SATISFATORIAMENTE.';

			JSON::response((!$confirm), 200, $message);
		}
		catch (Exception $ex)
		{
			JSON::response(TRUE, $ex->getCode(), $ex->getMessage(), []);
		}
	}

	/**
	 * Insert/Update de documentos de la ficha.
	 * @return void
	 */
	public function actionMantenimientodocumentos()
	{
		$allowKeys = [
			'ficha', 'tipo_doc', 'nro_documento', 'fecha_inicio', 'area_autorizada',
			'piso1', 'piso2', 'piso3', 'piso4', 'piso5', 'piso6', 'piso7', 'sotano', 'mezzanine',
			'action'
		];
		$postKeys = array_keys($_POST);

		try
		{
			if (!Yii::app()->request->isAjaxRequest)
				throw new Exception("El metodo no esta permitido", 403);

			$diff = array_diff($postKeys, $allowKeys);
      if (!empty($diff))
          throw new Exception("LOS PARAMETROS NO SON LOS CORRECTOS, ACCIÓN CANCELADA", 400);


      $confirm = QBiencomun::mantenimientoDocumentos($_POST, $_POST['action']);

      $message = 'ERROR AL MOMENTO DE EJECUTAR LA ACCIÓN, VUELVA A INTENTARLO.';

			if ( $confirm )
				$message = 'LA ACCIÓN SE EJECUTO SATISFATORIAMENTE.';

			JSON::response((!$confirm), 200, $message);
		}
		catch (Exception $ex)
		{
			JSON::response(TRUE, $ex->getCode(), $ex->getMessage());
		}
	}

	/**
	 * Insert/Update de construcciones de la ficha.
	 * @return void
	 */
	public function actionMantenimientoconstrucciones()
	{
		$allowKeys = [
			'ficha','id','nro_piso','mes','anio','mep','ecs','ecc','muros_col','techo','pisos',
			'puerta_vent','reves','banios','inst_electricas','area_declarada','area_verificada','uca',
			'action'
		];
		$postKeys = array_keys($_POST);

		try
		{
			if (!Yii::app()->request->isAjaxRequest)
				throw new Exception("El metodo no esta permitido", 403);

			$diff = array_diff($postKeys, $allowKeys);
      if (!empty($diff))
          throw new Exception("LOS PARAMETROS NO SON LOS CORRECTOS, ACCIÓN CANCELADA", 400);


      $confirm = QBiencomun::mantenimientoConstrucciones($_POST, $_POST['action']);

      $message = 'ERROR AL MOMENTO DE EJECUTAR LA ACCIÓN, VUELVA A INTENTARLO.';

			if ( $confirm )
				$message = 'LA ACCIÓN SE EJECUTO SATISFATORIAMENTE.';

			JSON::response((!$confirm), 200, $message);
		}
		catch (Exception $ex)
		{
			JSON::response(TRUE, $ex->getCode(), $ex->getMessage());
		}
	}

	/**
	 * Busca a una persona registrada en base de datos
	 * @param  string $rel    Clave para buscar por nombre o N° de documento.
	 * @param  string $tipo   Clave para filtrar por persona juridica o natural
	 * @param  string $search Texto de busqueda
	 * @return array         Resultado de la busqueda con la información basica de cada persona
	 */
	public function actionShowpersonasficha($rel, $tipo, $search)
	{
		try
		{
			if (!Yii::app()->request->isAjaxRequest)
				throw new Exception("El metodo no esta permitido", 403);

			$data['data'] = QBiencomun::showPersonasFicha($tipo, $rel, $search);

			JSON::response(FALSE, 200, 'OK', $data);

		}
		catch (Exception $ex)
		{
			JSON::response(TRUE, $ex->getCode(), $ex->getMessage(), []);
		}
	}

	/**
	 * Lista todas las notarias.
	 * @return array Resultado con la lista de todas las notarias registradas
	 */
	public function actionGetnotarias()
	{
		try
		{
			if (!Yii::app()->request->isAjaxRequest)
				throw new Exception("El metodo no esta permitido", 403);

			$data['data'] = QBiencomun::get_Notarias();

			JSON::response(empty($data['data']), 200, 'OK', $data);

		}
		catch (Exception $ex)
		{
			JSON::response(TRUE, $ex->getCode(), $ex->getMessage(), []);
		}
	}

	/**
	 * Obtiene las habilitaciones urbanas que pertenecen al sector registrado
	 * @param  string $sector    Codigo del sector
	 * @param  int $pertenece Codición de pertenencia
	 * @return array            Lista con las habilitaciones urbanas encontradas
	 */
	public function actionGetsectorhaburbana($sector, $pertenece)
	{
		try
		{
			if (!Yii::app()->request->isAjaxRequest)
				throw new Exception("El metodo no esta permitido", 403);

			if ( !is_numeric($sector) OR (((int)$sector) <= 0 OR ((int)$sector) > 99 ) )
				throw new Exception("Uno de los parametros no cumple el valor adecuado", 200);

			if ( !is_numeric($pertenece) OR ($pertenece < 0 OR $pertenece > 1) )
				throw new Exception("Uno de los parametros no cumple el valor adecuado", 200);

			$data['data'] = QBiencomun::get_SectorHabUrbanas($sector, $pertenece);

			JSON::response(FALSE, 200, 'OK', $data);

		}
		catch (Exception $ex)
		{
			JSON::response(TRUE, $ex->getCode(), $ex->getMessage(), []);
		}
	}

	/**
	 * Obtiene la lista de zonificaciones que pertenecen al sector registrado
	 * @param  string $sector    Codigo del sector
	 * @param  int $pertenece Codición de pertenencia
	 * @return array            Lista con las zonificaciones encontradas
	 */
	public function actionGetsectorzona($sector, $pertenece)
	{
		try
		{
			if (!Yii::app()->request->isAjaxRequest)
				throw new Exception("El metodo no esta permitido", 403);

			if ( !is_numeric($sector) OR (((int)$sector) <= 0 OR ((int)$sector) > 99 ) )
				throw new Exception("Uno de los parametros no cumple el valor adecuado", 200);

			if ( !is_numeric($pertenece) OR ($pertenece < 0 OR $pertenece > 1) )
				throw new Exception("Uno de los parametros no cumple el valor adecuado", 200);

			$data['data'] = QBiencomun::get_SectorZona($sector, $pertenece);

			JSON::response(FALSE, 200, 'OK', $data);

		}
		catch (Exception $ex)
		{
			JSON::response(TRUE, $ex->getCode(), $ex->getMessage(), []);
		}
	}

	/**
	 * Obtiene la lista vias que pertenecen a una habilitación urbana
	 * @param  string $haburbana Codigo de habilitación urbana
	 * @return array Listado de vias
	 */
	public function actionGethaburbanavia($haburbana)
	{
		try
		{
			if (!Yii::app()->request->isAjaxRequest)
				throw new Exception("El metodo no esta permitido", 403);

			if ( !is_numeric($haburbana) )
				throw new Exception("La habilitación urbana no es correcta", 200);

			$data['data'] = QBiencomun::get_HabUrbanaVias($haburbana);

			JSON::response(FALSE, 200, 'OK', $data);

		}
		catch (Exception $ex)
		{
			JSON::response(TRUE, $ex->getCode(), $ex->getMessage(), []);
		}
	}

	/**
	 * cONSULTA LA INFORMACIÓN BASICA DE UNA PERSONA O UN PERSONAL(SUPERVISOR, TECNICO CATASTRAL,
	 * TECNICO CALIDAD VERIFICADOR CATASTRAL)
	 * @param  [String] $rel  Relación de la consulta. Persona o Personal
	 * @param  String $cond Codigo o N° de documento
	 * @param  Int $t    Tipo de persona
	 * @param  string $u    Tipo de usuario. En caso de ser usuario el dato es 0
	 * @return [Object]       Resultado de la consulta
	 */
	public function actionGetpersona($rel, $cond, $t, $u)
	{
		$allowKeys_rel = ['us', 'pe'];
		try
		{
			if (!Yii::app()->request->isAjaxRequest)
				throw new Exception("El metodo no esta permitido", 403);

			if ( !in_array($rel, $allowKeys_rel) )
				throw new Exception("Uno de los parametros no coinciden.", 400);

			$row = [];

			switch ($rel) {
				case 'us':
					$row['data'] = QBiencomun::get_Personal($cond, $t, $u);
					break;
				case 'pe':
					$row['data'] = QBiencomun::get_Personas($cond, $t);
					break;
			}

			JSON::response( empty($row['data']), 200, 'OK', $row);
		}
		catch(Exception $ex)
		{
			JSON::response(TRUE, $ex->getCode(), $ex->getMessage());
		}
	}

	/**
	 * Asigna a la cache los campos que se manipulan en la ficha
	 * @return void
	 */
	public function actionAsignardato()
	{
		try {

			if (!Yii::app()->request->isAjaxRequest)
				throw new Exception("El metodo no esta permitido", 403);

			$key = array_keys($_POST);

			UBiencomun::setCookie($key[0], $_POST[$key[0]]);

		} catch (Exception $ex) {
			JSON::response(TRUE, $ex->getCode(), $ex->getMessage());
		}
	}

	/**
	 * Acción para reiniciar una ficha y borrar todos los datos en cache
	 * @return void
	 */
	public function actionResetearficha()
	{
		try {

			if (!Yii::app()->request->isAjaxRequest)
				throw new Exception("El metodo no esta permitido", 403);

			unset(Yii::app()->request->cookies[CBiencomun::UP01_NAME_CK]);
			unset(Yii::app()->request->cookies[CBiencomun::UP02_NAME_CK]);
			unset(Yii::app()->request->cookies[CBiencomun::HEAD_NAME_CK]);

			JSON::response( FALSE, 200, 'OK');

		} catch (Exception $ex) {
			JSON::response(TRUE, $ex->getCode(), $ex->getMessage());
		}
	}

	/**
	 * Acción de prueba para la verificación del codigo predial
	 * @return void
	 */
	public function actionVerifycodpredial()
	{
		try {

			if (!Yii::app()->request->isAjaxRequest)
				throw new Exception("El metodo no esta permitido", 403);

			JSON::response( TRUE, 200, 'EL CODIGO PREDIAL: ' . $_POST['cod'] . ' SERÁ CONSULTADO');

		} catch (Exception $ex) {
			JSON::response(TRUE, $ex->getCode(), $ex->getMessage());
		}
	}

	/**
	 * Metodo que se implementa para generar una observación.
	 *
	 * El metodo registra en la db toda la observacion y limpia la ficha
	 * para que se pueda generar una nueva ficha
	 * @return void
	 */
	public function actionGenerarobservacion()
	{
		try
		{
			if (!Yii::app()->request->isAjaxRequest)
				throw new Exception("El metodo no esta permitido", 403);

			$confirm = QPrincipal::generateObservacion($_POST);
			$message = 'No se pudo generar la observación';

			if ( $confirm ) {

				$message = 'La observación a sido generada, debe crear una nueva ficha.';

				unset(Yii::app()->request->cookies[CBiencomun::UP01_NAME_CK]);
				unset(Yii::app()->request->cookies[CBiencomun::UP02_NAME_CK]);
				unset(Yii::app()->request->cookies[CBiencomun::HEAD_NAME_CK]);

			}

			JSON::response(!$confirm, 200, $message);

		}
		catch (Exception $ex)
		{
			JSON::response(TRUE, $ex->getCode(), $ex->getMessage(), []);
		}
	}

}

/* End of file BiencomunController.php */
/* Location: ./ficha/controllers/BiencomunController.php */
