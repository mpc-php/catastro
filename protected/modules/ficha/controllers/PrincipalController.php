<?php
/**
 * Clase que se encarga de manejar los diferentes recursos que comparten todas las fichas
 * @author José Alejandro Vilchez Moreno <joalvm@gmail.com>
 * @package Catastro\modules\ficha\controllers
*/

class PrincipalController extends Auth
{
	public function actionIndex()
	{
		$this->render('index');
	}

	/**
	 * Obtiene todas las fichas de un tipo y permite su filtrado
	 * @param  string $type Tipo de ficha
	 * @return void
	 */
	public function actionData($type) 
	{
		$allowTypes = ['01', '02', '03', '04'];

		try {
			if (!Yii::app()->request->isAjaxRequest)
				throw new Exception("El metodo no esta permitido", 403);

			if ( !in_array($type, $allowTypes) )
				throw new Exception("Uno de los parametros no coinciden.", 400);

			$data = QPrincipal::dataFichas($_GET, $type);

			JSON::response(FALSE, 200, 'OK', $data);

		} catch (Exception $ex) {
			JSON::response(TRUE, $ex->getCode(), $ex->getMessage(), []);
		}
	}

	/**
	 * Obtiene el resultado de la consulta que verifica si el sector existe en un año de proceso determinado
	 * @return void
	 */
	public function actionVerifysector()
	{
		try 
		{
			if (!Yii::app()->request->isAjaxRequest)
				throw new Exception("El metodo no esta permitido", 403);

			if ( !is_numeric($_POST['sector']) )
				throw new Exception("El sector no es el correcto", 200);

			$exists = QPrincipal::exists_Sector($_POST['sector'], $_POST['ubigeo']);
			$message = 'El codigo de sector no existe';

			if ( $exists ) {
				$message = 'OK';
			}

			JSON::response(!$exists, 200, $message);

		} 
		catch (Exception $ex) 
		{
			JSON::response(TRUE, $ex->getCode(), $ex->getMessage(), []);
		}
	}

	/**
	 * Obtiene los resultado de diferentes consultas que se muestran en elementos de lista
	 * @param  string $type Tipo de consulta
	 * @param  int $sw   (Desconocido)
	 * @return void
	 */
	public function actionGetcombo($type, $sw)
	{
		try 
		{
			if (!Yii::app()->request->isAjaxRequest)
				throw new Exception("El metodo no esta permitido", 403);

			if ( !is_numeric($type) OR (((int)$type) <= 0 OR ((int)$type) > 99 ) )
				throw new Exception("Uno de los parametros no cumple el valor adecuado", 200);

			if ( (!is_numeric($sw)) OR ($sw < 0 OR $sw > 20) )
				throw new Exception("Uno de los parametros no cumple el valor adecuado", 200);

			$data['data'] = QPrincipal::get_Tipos($type, $sw);

			JSON::response(FALSE, 200, 'OK', $data);

		} 
		catch (Exception $ex) 
		{
			JSON::response(TRUE, $ex->getCode(), $ex->getMessage(), []);
		}
	}

	/**
	 * Obtiene la lista de los diferente ubigeos
	 * @param  string $c Codigo de ubigeo
	 * @return void
	 */
	public function actionGetubigeo($c)
	{
		try 
		{
			if (!Yii::app()->request->isAjaxRequest)
				throw new Exception("El metodo no esta permitido", 403);

			if ( !is_numeric($c) )
				throw new Exception("Uno de los parametros no cumple el valor adecuado", 200);

			$cod = ($c == '0') ? '' : $c;

			$data['data'] = QPrincipal::get_Ubigeo($cod);

			JSON::response( empty($data['data']), 200, 'OK', $data );

		} 
		catch (Exception $ex) 
		{
			JSON::response(TRUE, $ex->getCode(), $ex->getMessage(), []);
		}
	}

	/**
	 * Obtiene la lista de todas las tablas que su funcionalidad es independiente y que se encuentra dentro de cada ficha
	 * @param  string $type Tipo de consulta
	 * @param  string $rel  ID de ficha relacionada
	 * @return void
	 */
	public function actionGettable($type, $rel)
	{
		try 
		{
			if (!Yii::app()->request->isAjaxRequest)
				throw new Exception("El metodo no esta permitido", 403);

			if ( !is_numeric($type) OR (((int)$type) <= 0 OR ((int)$type) > 99 ) )
				throw new Exception("Uno de los parametros no cumple el valor adecuado", 200);

			$data['data'] = QPrincipal::get_InfoFichas($type, $rel);

			JSON::response(FALSE, 200, 'OK', $data);

		} 
		catch (Exception $ex) 
		{
			JSON::response(TRUE, $ex->getCode(), $ex->getMessage(), []);
		}
	}

	/**
	 * Proceso que elimina la ficha seleccionada
	 * @return void
	 */
	public function actionEliminarficha()
	{
		try 
		{
			if (!Yii::app()->request->isAjaxRequest)
				throw new Exception("El metodo no esta permitido", 403);

			$tipo = $_POST['tipo'];
			$id_ficha = $_POST['id_ficha'];

      $confirm = QPrincipal::eliminarFicha($id_ficha);

      $data['reload'] = FALSE;

      $message = 'ERROR AL MOMENTO DE EJECUTAR LA ACCIÓN, VUELVA A INTENTARLO.';

			if ( $confirm ) 
				$message = 'LA ACCIÓN SE EJECUTO SATISFATORIAMENTE.';

			if ($tipo == '01') 
			{
				$head = UIndividual::getCookie(CIndividual::HEAD_NAME_CK);
				
				if ( ! empty( (array) $head) )
				{
					if ( $head->id_ficha == $id_ficha ) 
					{
						unset(Yii::app()->request->cookies[CIndividual::HEAD_NAME_CK]);
						unset(Yii::app()->request->cookies[CIndividual::UP01_NAME_CK]);
						unset(Yii::app()->request->cookies[CIndividual::UP02_NAME_CK]);

						$data['reload'] = TRUE;
					}
				}
			} 
			else if ($tipo == '02') 
			{
				$head = UCotitularidad::getCookie(CCotitularidad::HEAD_NAME_CK);

				if ( ! empty( (array) $head) )
				{
					if ( $head->id_ficha == $id_ficha ) 
					{
						unset(Yii::app()->request->cookies[CCotitularidad::UP01_NAME_CK]);
						unset(Yii::app()->request->cookies[CCotitularidad::HEAD_NAME_CK]);

						$data['reload'] = TRUE;
					}
				}
			} 
			else if ($tipo == '03') 
			{
				$head = UActeconomica::getCookie(CActeconomica::HEAD_NAME_CK);

				if ( ! empty( (array) $head) )
				{
					if ( $head->id_ficha == $id_ficha ) 
					{
						unset(Yii::app()->request->cookies[CActeconomica::UP01_NAME_CK]);
						unset(Yii::app()->request->cookies[CActeconomica::HEAD_NAME_CK]);

						$data['reload'] = TRUE;
					}
				}
			}
			else if( $tipo == '04' ) 
			{
				$head = UBiencomun::getCookie(CBiencomun::HEAD_NAME_CK);

				if ( ! empty( (array) $head) )
				{
					if ( $head->id_ficha == $id_ficha ) 
					{
						unset(Yii::app()->request->cookies[CBiencomun::HEAD_NAME_CK]);
						unset(Yii::app()->request->cookies[CBiencomun::UP01_NAME_CK]);
						unset(Yii::app()->request->cookies[CBiencomun::UP02_NAME_CK]);

						$data['reload'] = TRUE;
					}
				}
			}

			JSON::response((!$confirm), 200, $message, ['data' => $data]);
		} 
		catch (Exception $ex) 
		{
			JSON::response(TRUE, $ex->getCode(), $ex->getMessage());
		}
	}

	/**
	 * Busqueda general de personas
	 * @param  string $rel   Filtro de la persona N° de documento / nombre
	 * @param  string $cargo Cargo que ocupa la persona
	 * @param  int $type  Tipo de persona juridica o natural
	 * @return void
	 */
	public function actionSearchpersona($rel, $cargo, $type)
	{
		try 
		{
			if (!Yii::app()->request->isAjaxRequest)
				throw new Exception("El metodo no esta permitido", 403);

			$data = QPrincipal::search_PersonasData($_GET, $type);

			JSON::response(FALSE, 200, 'OK', $data);

		} 
		catch (Exception $ex) 
		{
			JSON::response(TRUE, $ex->getCode(), $ex->getMessage());
		}
	}

	public function actionSearchVia($code)
	{
		try 
		{
			if (!Yii::app()->request->isAjaxRequest)
				throw new Exception("El metodo no esta permitido", 403);

			$data = QPrincipal::search_Via($code);

			JSON::response(empty($data), 200, 'OK', ['data' => $data]);
		} 
		catch (Exception $ex) 
		{
			JSON::response(TRUE, $ex->getCode(), $ex->getMessage());
		}
	}

	/**
	 * Busqueda de campos requeridos de cada ficha
	 * @return void
	 */
	public function actionGetfieldrequired()
	{
		$allowTypes = ['01', '02', '03', '04'];
		try 
		{
			if (!Yii::app()->request->isAjaxRequest)
				throw new Exception("El metodo no esta permitido", 403);

			if ( ! isset($_GET['type']) )
				throw new Exception("El recurso no esta permitido", 400);

			if ( !in_array($_GET['type'], $allowTypes) )
				throw new Exception("El recurso buscado no está permitido", 400);

			$data['data'] = QPrincipal::getFieldRequired($_GET['type']);

			JSON::response(FALSE, 200, 'OK', $data);

		} 
		catch (Exception $ex) 
		{
			JSON::response(TRUE, $ex->getCode(), $ex->getMessage());
		}
	}

	public function actionGetubicacionpredio($rel)
	{
		try 
		{
			if ( ! Yii::app()->request->isAjaxRequest )
				throw new Exception("El metodo no esta permitido", 403);

			$data['data'] = QPrincipal::getUbicacionPredio($rel);

			JSON::response(FALSE, 200, 'OK', $data);

		} 
		catch (Exception $ex) 
		{
			JSON::response(TRUE, $ex->getCode(), $ex->getMessage());
		}
	}

}