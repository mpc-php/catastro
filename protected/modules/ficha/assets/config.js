/**
 * Configuración personalizada para la carga de librerias del módulo
 * 
 * @type Object
 */
var Builder = {
    module: {
        controllers: {
            individual: {
                //Assets para un controllador especifico y todas sus acciones
                js: [{individual: ["_PATH_:bT_ES", "_PATH_:PBootbox", "_PATH_:inputmask", "_PATH_:bootstrapGrowlUI"]}],
                css: {
                    libs: [],
                    package: [],
                    custom: ['individual']
                }
            },
            cotitularidad: {
                //Assets para un controllador especifico y todas sus acciones
                js: [{cotitularidad: ["_PATH_:bT_ES", "_PATH_:PBootbox", "_PATH_:inputmask", "_PATH_:bootstrapGrowlUI"]}],
                css: {
                    libs: [],
                    package: [],
                    custom: ['cotitularidad']
                }
            },
            acteconomica: {
                //Assets para un controllador especifico y todas sus acciones
                js: [{acteconomica: ["_PATH_:bT_ES", "_PATH_:PBootbox", "_PATH_:inputmask", "_PATH_:bootstrapGrowlUI"]}],
                css: {
                    libs: [],
                    package: [],
                    custom: ['acteconomica']
                }
            },
            biencomun: {
                //Assets para un controllador especifico y todas sus acciones
                js: [{biencomun: ["_PATH_:bT_ES", "_PATH_:PBootbox", "_PATH_:inputmask", "_PATH_:bootstrapGrowlUI"]}],
                css: {
                    libs: [],
                    package: [],
                    custom: ['biencomun']
                }
            }
        },
        //Assets para todo el módulo, todos sus controlladores y todas sus acciones
        js: [],
        css: {
            libs: ['bootstrap-table/bootstrap-table.min', 'metronic/pages/css/search.min'],
            package: [],
            custom: []
        }
    }
};
