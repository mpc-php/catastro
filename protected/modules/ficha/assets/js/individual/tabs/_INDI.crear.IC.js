/**
 * MODULO PARA INFORMACION COMPLEMENTARIA
 */
INDI.crear.IC = (function (_win, $) {
	
	'use strict';

	var ID_Ficha = null,
			COD_Catastro = null;

	var $cnt_dp = $('div#tab_infcomplementaria'),
			$inputs = $cnt_dp.find('input, select, textarea');

	//CONSTRUCTOR
	var _bind = function (ficha, catastro) {

		ID_Ficha 			= ficha,
		COD_Catastro 	= catastro;

		_load_combos();

		$inputs.filter('input[type=text].only-number').on('keydown.INDI.crear.IC.module', only_number);
		$inputs.filter('input[type=text].only-number').on('change.INDI.crear.IC.module', verify_number);
		$inputs.filter('#txtobservaciones').on('keydown.INDI.crear.IC.module', _open_nex_tab);

		$inputs.filter('.datasend').on('change.INDI.crear.IC.module', _watcher_general);

	},

	_watcher_general = function() {
		var $this = $(this),
			name = $this.attr('name'),
			type = $this.attr('type'),
			tag = $this.prop("tagName").toLowerCase(),
			data = {};

		if ( (tag == 'input' && (type == 'text' || type == 'radio' )) || tag == 'textarea' ) {
			data[name] = $.trim($this.val());
		} else if ( tag == 'select' ) {
			data[name] = $this.children('option:selected').val();
		}

		events.emit('savedata', data);
		
	},

	_open_nex_tab = function(e) {

		if ( e.key == "Tab" && e.keyCode == 9 ) {
			$('#tabs-control').find('a[href="#tab_otros"]').tab('show');
			e.preventDefault();
			return false;
		}

	},

	_load_combos = function() {

		var $selects = $inputs.filter('select.autoload');

		$.each($selects, function(index, el) {

			var $el = $(el),
					type = $el.data('type'),
					sw = $el.data('sw'),
					resource = 'principal/getcombo/type/'+type+'/sw/'+sw;

			$.getJSON(INDI.base_url + resource, function(json, textStatus) {

				if ( ! json.error ) {

					json.data.forEach(function(obj, pos) {

						var params = { value: obj.CODIGO.trim() };

						if ( $el.data('sel') == params.value )
							params.selected = true;

						$el.append( $('<option />', params).text(obj.NOMBRE) );

					});

				} else {

					$el.attr({title:'No se encontraron datos'}).prop({disabled:true});

				}

			});

		});

	};

	return {
		init: _bind
	};

}(window, window.jQuery));