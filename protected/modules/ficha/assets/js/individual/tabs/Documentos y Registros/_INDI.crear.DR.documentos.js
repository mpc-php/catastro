/**
 * MODULO PARA EL MANEJO DE LA TABLA DOCUMENTOS DE LA FICHA INDIVIDUAL
 * @param  {object} _win     objeto global window
 * @param  {object} $        jquery
 * @return {void}
 */
INDI.crear.DR.documentos = ( function( _win, $ ) {

	'use strict';

	var ID_Ficha 			= null,
			COD_Catastro 	= null;
			
	var datasend = {},
			default_ = {
				ficha: null,
				tipo_doc: '',
				nro_documento: '',
				fecha_inicio: '',
				area_autorizada: 0,
				piso1: 0,
				piso2: 0,
				piso3: 0,
				piso4: 0,
				piso5: 0,
				piso6: 0,
				piso7: 0,
				sotano: 0,
				mezzanine: 0,
				action: null
			},

			//Array que almacena todos los tipos de documentos registrados
			//para evitar un documento repetitivo
			tdocs_arr = [];

			//Contenedor principal: tab
	var $container = $({}),
			//Boton que abre el modal en modo crear
			$btn_open_new,

			//Formulario para el manejo de los documentos
			$md_mng,

			//Boton que envia toda la información del formulario
			$btn_done,

			//Elementos inputs, select que se encuentran en el modal
			$inputs,

			//Tabla que se muestra en el tab
			$table,

			//Instancia del modal, que se genera al momento de abrir
			$bb_mng = {};

	//Constructor
	var _bind = function(_id_ficha_, _cod_catastro_, _container_) {

		ID_Ficha = _id_ficha_;
		COD_Catastro = _cod_catastro_;
		$container = _container_;

		//Cacheando datos
		$btn_open_new = $container.find('#btn-add-documentos');
		$md_mng = $container.find('section#md-documentos');
		$inputs = $md_mng.find('input, select');
		$table = $container.find('table#td-documentos');
		$btn_done = $md_mng.find('button#btn-done');

		_build_table();

		$btn_open_new.on('click.INDI.crear.DR.documentos.module', _open_md_new);

	},

	/**
	 * Supervisa los cambios en los inputs y asigna su dato al objeto que se
	 * enviará en la consulta
	 * @return {[type]} [description]
	 */
	_watcher_change = function() {

		var $this = $(this),
			name = $this.attr('name'),
			type = $this.attr('type'),
			tag = $this.prop("tagName").toLowerCase();

		if ( tag == 'input' && type == 'text' ) {

			var value = $.trim($this.val());

			if ( $this.hasClass('maskdate') ) {
				if ( ! Inputmask.isValid(value, { alias: "dd/mm/yyyy"}) ) {
					value = '';
					$this.val(value).css({borderColor:'orange'});
				} else {
					$this.removeAttr('style');
				}
			}

			datasend[name] = value;

		} else if ( tag == 'select' ) {
			datasend[name] = $this.children('option:selected').val();
		}

	},

	/**
	 * Envio de la información mediante metodo post
	 * @return {void} 
	 */
	_submit_data = function() {

		datasend = $.extend({}, default_, datasend);

		if ( ID_Ficha == null ) {
			$.bootstrapGrowl("La ficha no está asignado, el proceso a sido cancelado", INDI.growl('warning'));
			return false;
		} else {
			datasend.ficha = ID_Ficha;
		}

		if ( datasend.tipo_doc.length == 0 ) {
			$.bootstrapGrowl("DEBES ESCOGER UN TIPO DE DOCUMENTO.", INDI.growl('warning'));
			$bb_mng.find('select#cbotipo_doc').focus();
			return false;
		}

		if ( ! $.inArray(datasend.tipo_doc, tdocs_arr) && datasend.action == 'create' ) {
			$.bootstrapGrowl("EL TIPO DE DOCUMENTO YA HA SIDO ASIGNADO, ESCOGA OTRO ITEM", INDI.growl('warning'));
			$bb_mng.find('select#cbotipo_doc').focus();
			return false;
		}

		if ( datasend.nro_documento.length == 0 ) {
			$.bootstrapGrowl("NO HAS ESTABLECIDO UN NUMERO DE DOCUMENTO.", INDI.growl('warning'));
			$bb_mng.find('input[type=text]#txtnro_documento').focus();
			return false;
		}

		if ( datasend.fecha_inicio.length == 0 ) {
			$.bootstrapGrowl("NO HAS INGRESADO UNA FECHA DE INICIO.", INDI.growl('warning'));
			$bb_mng.find('input[type=text]#txtfecha_inicio').focus();
			return false;
		}

		var $elems = $bb_mng.find('select, input, button');

		$elems.prop({ disabled: true });

		$.post(INDI.base_url + 'individual/mantenimientodocumentos', datasend, function(response) {

			if ( ! response.error ) {

				$.bootstrapGrowl(response.message, INDI.growl('success'));
				
				$table.bootstrapTable('refresh');
				
				$elems.prop({ disabled: false });

				$bb_mng.find('#btn-close').click();

			} else {

				$elems.prop({disabled:false});
				$.bootstrapGrowl(response.message, INDI.growl('danger'));
				$bb_mng.find('input[type=text]#cbotipo_doc').focus();

			}
			
		});

	},

	/**
	 * Abre el modal en modo de creación
	 * @return {void}
	 */
	_open_md_new = function() {
		$bb_mng = $md_mng.cBootbox({
			title: "Agregar documentos",
			size: 'large',
			btnClose: '#btn-close',
			beforeOpen: function() {

				_load_tipo_documentos();
				_asign_events();

				$inputs.prop({disabled:false});

				datasend.action = 'create';

				_win.setTimeout(function() {
					$inputs.filter('#cbotipo_doc').focus();
				}, 500);
				
			},
			afterClose: _close_modal
		});
	},

	/**
	 * Abre el modal en modo de edición
	 * @param  {object} e     Instancia del evento
	 * @param  {string} value Valor que contiene el elemento
	 * @param  {object} row   Objeto con todo los datos de la fila seleccionada
	 * @param  {int} index Posición de la fila en la tabla
	 * @return {void}     
	 */
	_open_md_edit = function(e, value, row, index) {

		$bb_mng = $md_mng.cBootbox({
			title: "Editar documento",
			size: 'large',
			btnClose: '#btn-close',
			beforeOpen: function() {

				_load_tipo_documentos();

				_asign_events();

				$inputs.prop({ disabled: false });
				$inputs.filter('select#cbotipo_doc').prop({disabled:true});
				
				$md_mng.find('select#cbotipo_doc').data({sel:row.TIP_DOC}).prop({disabled:true});
				$md_mng.find('input#txtnro_documento').val(row.NRO_DOC);
				$md_mng.find('input#txtfecha_inicio').val(row.FECHA_DOC);
				$md_mng.find('input#txtarea_autorizada').val(parseFloat(row.AREA_AUTORIZADA));
				$md_mng.find('input#txtpiso1').val(parseFloat(row.AREA_CONST_P01));
				$md_mng.find('input#txtpiso2').val(parseFloat(row.AREA_CONST_P02));
				$md_mng.find('input#txtpiso3').val(parseFloat(row.AREA_CONST_P03));
				$md_mng.find('input#txtpiso4').val(parseFloat(row.AREA_CONST_P04));
				$md_mng.find('input#txtpiso5').val(parseFloat(row.AREA_CONST_P05));
				$md_mng.find('input#txtpiso6').val(parseFloat(row.AREA_CONST_P06));
				$md_mng.find('input#txtpiso7').val(parseFloat(row.AREA_CONST_OTRO));
				$md_mng.find('input#txtsotano').val(parseFloat(row.AREA_CONST_SOT));
				$md_mng.find('input#txtmezzanine').val(parseFloat(row.AREA_CONST_MEZ));

				datasend.tipo_doc = row.TIP_DOC;
				datasend.nro_documento = row.NRO_DOC;
				datasend.fecha_inicio = row.FECHA_DOC;
				datasend.area_autorizada = parseFloat(row.AREA_AUTORIZADA);
				datasend.piso1 = parseFloat(row.AREA_CONST_P01);
				datasend.piso2 = parseFloat(row.AREA_CONST_P02);
				datasend.piso3 = parseFloat(row.AREA_CONST_P03);
				datasend.piso4 = parseFloat(row.AREA_CONST_P04);
				datasend.piso5 = parseFloat(row.AREA_CONST_P05);
				datasend.piso6 = parseFloat(row.AREA_CONST_P06);
				datasend.piso7 = parseFloat(row.AREA_CONST_OTRO);
				datasend.sotano = parseFloat(row.AREA_CONST_SOT);
				datasend.mezzanine = parseFloat(row.AREA_CONST_MEZ);
				
				datasend.action = 'update';

				_win.setTimeout(function() {
					$inputs.filter('#cbotipo_doc').focus();
				}, 500);
				
			},
			afterClose: _close_modal
		});

	},

	_asign_events = function () {
		$inputs.filter('input[type=text].only-number').on('keydown.INDI.crear.DR.module', only_number);
		$inputs.filter('input[type=text].only-number').on('change.INDI.crear.DR.module', verify_number);

		$inputs.filter('.md-datasend').on('change.INDI.crear.DR.module', _watcher_change);
		$btn_done.on('click.INDI.crear.DR.documentos.module', _submit_data);

		$md_mng.find('select').on('keypress', select_keyPress);
		
		$inputs.filter('.maskdate').inputmask();

	},

	/**
	 * Evento que elimina una fila de la tabla
	 * @param  {object} e     Instancia del evento
	 * @param  {string} value Valor que contiene el elemento
	 * @param  {object} row   Objeto con todo los datos de la fila seleccionada
	 * @param  {int} index Posición de la fila en la tabla
	 * @return {[type]}       [description]
	 */
	_delete_row = function(e, value, row, index) {

		var $this = $(this),
				postParams = {},
				resource = INDI.base_url + 'individual/mantenimientodocumentos';

		bootbox.confirm({
			title: 'Seguro que quieres eliminar?',
			message: "Documento: <b>" + row.NRO_DOC + " - " + row.DESCRIPCION + "</b>, será eliminado de forma permanente.",
			callback: function(result) {

				if ( result ) {

					if ( ID_Ficha == null ) {
						$.bootstrapGrowl("La ficha no está asignada, el proceso a sido cancelado", INDI.growl('warning'));
						return false;
					} else {
						postParams.ficha = ID_Ficha;
					}

					postParams.tipo_doc = row.TIP_DOC;
					postParams.action = 'delete';

					$this.prop({disabled:true});

					$.post(resource, postParams, function(response) {
						if ( ! response.error ) {

							$.bootstrapGrowl(response.message, INDI.growl('success'));
							$table.bootstrapTable('refresh');
							$this.prop({ disabled: false });

						} else {

							$this.prop({disabled:false});
							$.bootstrapGrowl(response.message, INDI.growl('danger'));

						}
					});

				}
			}
		});

	},

	/**
	 * Evento que se ejecuta inmediatamente despues del cierre del modal
	 * @return {[type]} [description]
	 */
	_close_modal = function() {

		$md_mng.find('input').val('').prop({disabled:true});
		var $cbo = $md_mng.find('select');

		$cbo.children('option:eq(0)').prop({selected:true});
		$cbo.children('option:not(:eq(0))').remove();

		datasend = {};

		$cbo.prop({disabled:true});

		_win.setTimeout(function(){
			$btn_open_new.focus();
		}, 500);

	},

	/**
	 * Antes que se diseñe la tabla, cambiamos algunoss de us valores y almacenamos
	 * todos los TIPOS DE DOCUMENTOS, que han sido asignado, para que no sea escogido
	 * @param  {object} response Contenido de la respues en formato json
	 * @return {void} 
	 */
	_check_tdocs_asignados = function (response) {

		tdocs_arr = [];

		response.data.forEach(function(obj, index) {

			var fecha = ((obj.FECHA_DOC.split(' ')[0]).split('-')).reverse().join('/');

			obj.FECHA_DOC = fecha;
			obj.AREA_AUTORIZADA = parseFloat(obj.AREA_AUTORIZADA);
			obj.AREA_CONST_P01 = parseFloat(obj.AREA_CONST_P01);
			obj.AREA_CONST_P02 = parseFloat(obj.AREA_CONST_P02);
			obj.AREA_CONST_P03 = parseFloat(obj.AREA_CONST_P03);
			obj.AREA_CONST_P04 = parseFloat(obj.AREA_CONST_P04);
			obj.AREA_CONST_P05 = parseFloat(obj.AREA_CONST_P05);
			obj.AREA_CONST_P06 = parseFloat(obj.AREA_CONST_P06);
			obj.AREA_CONST_OTRO = parseFloat(obj.AREA_CONST_OTRO);
			obj.AREA_CONST_SOT = parseFloat(obj.AREA_CONST_SOT);
			obj.AREA_CONST_MEZ = parseFloat(obj.AREA_CONST_MEZ);

			tdocs_arr.push(obj.TIP_DOC);

		});

		return response;
	},

	/**
	 * Carga en el combo todos los tipos de documento
	 * @return {void}
	 */
	_load_tipo_documentos = function() {

		var $cbo 			= $inputs.filter('select#cbotipo_doc'),
				resource 	= 'principal/getcombo/type/21/sw/3';

		$cbo.children('option:not(:eq(0))').remove();

		$.getJSON(INDI.base_url + resource, function(json, textStatus) {

			if ( ! json.error ) {

				json.data.forEach(function(obj, pos) {

					var params = { value: obj.CODIGO.trim() };

					if ( $cbo.data('sel') == params.value ) {
						params.selected = true;
					}

					$cbo.append( $('<option />', params).text(obj.NOMBRE) );

				});

				$btn_done.prop({disabled:false});

			} else {

				$cbo.attr({title:'No se encontraron datos'}).prop({disabled:true});

			}

		});

	},

	/**
	 * Contiene los botones de opciones para cada fila
	 * @param  {string} value Valor que contiene el elemento
	 * @param  {object} row   Objeto con todo los datos de la fila seleccionada
	 * @param  {int} index Posición de la fila en la tabla
	 * @return {string} HTML de los botones dibujados
	 */
	_action_buttons = function(value, row, index) {
    return [
			'<button id="btn-edit-'+row.TIP_DOC+'" class="btn-actions edit btn btn-sm" data-id="'+row.TIP_DOC+'" data-accion="edit" tabindex="-1">',
				'<i class="fa fa-pencil" aria-hidden="true"></i>',
			'</button>',
			'<button id="btn-delete-'+row.TIP_DOC+'" class="btn-actions delete btn btn-sm" data-id="'+row.TIP_DOC+'" data-accion="delete" tabindex="-1">',
				'<i class="fa fa-trash-o" aria-hidden="true"></i>',
			'</button>'
		].join('');
  },

  /**
   * Carga la tabla con todos los datos
   * @return {void}
   */
	_build_table = function() {

		var bootTable = {
			escape: false,
		  locale: 'es-SP',
		  idField: "TIP_DOC",
		  responseHandler: _check_tdocs_asignados,
		  url: INDI.base_url + 'principal/gettable/type/03/rel/' + ID_Ficha,
		  onPostHeader: function(e){
				$table.find('tr th').removeAttr('tabindex');
		  }
		};

  	//TABLE HEADER
		bootTable.columns = [
			{ 
				field: 'action', 
				title: '<i class="fa fa-cog" aria-hidden="true"></i>', 
				align: 'center', 
				class: 'config-width',
				formatter : _action_buttons, 
				events : {
	        'click .edit': _open_md_edit,
	        'click .delete': _delete_row
    		}
    	},
			{ field: 'DESCRIPCION', title: 'T. DOCUMENTO', align: 'left', class: 'ds-width' },
			{ field: 'NRO_DOC', title: 'N° DOCUMENTO', align: 'left' },
			{ field: 'FECHA_DOC', title: 'FECHA', align: 'left' },
			{ field: 'AREA_AUTORIZADA', title: 'A. AUTORIZADA', align: 'right' },
			{ field: 'AREA_CONST_P01', title: '1° PISO', align: 'right' },
			{ field: 'AREA_CONST_P02', title: '2° PISO', align: 'right' },
			{ field: 'AREA_CONST_P03', title: '3° PISO', align: 'right' },
			{ field: 'AREA_CONST_P04', title: '4° PISO', align: 'right' },
			{ field: 'AREA_CONST_P05', title: '5° PISO', align: 'right' },
			{ field: 'AREA_CONST_P06', title: '6° PISO', align: 'right' },
			{ field: 'AREA_CONST_OTRO', title: 'OTRO', align: 'right' },
			{ field: 'AREA_CONST_SOT', title: 'SOTANO', align: 'right' },
			{ field: 'AREA_CONST_MEZ', title: 'MEZZANINES', align: 'right' }
		];

		$table.bootstrapTable(bootTable);

	};


	return {
		init: _bind
	}

}(window, window.jQuery));