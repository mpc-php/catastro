// @koala-append "Ubicacion del Predio/_INDI.crear.UP.ubicaciones.js"

/**
 * MODULO PARA UBICACION DE PREDIO
 */
INDI.crear.UP = (function (_win, $) {
	
	'use strict';

	var ID_Ficha 			= null,
			COD_Catastro 	= null,
			SECTOR 				= null,
			dataHU 				= [],
			selectedHU 		= '';

	var $cnt_up 			= $('div#tab_ubicpredio'),
			$inputs 			= $cnt_up.find('input[type=text], select'),
			$txtHaburb 		= $inputs.filter('input#txtcode_haburba');

	//CONSTRUCTOR
	var _bind = function (_ficha_, _catastro_, _sector_) {

		ID_Ficha 			= _ficha_;
		COD_Catastro 	= _catastro_;
		SECTOR 				= _sector_;

		//INICIANDO SUBMODULOS
		INDI.crear.UP.ubicaciones.init(
			_ficha_, 
			_catastro_, 
			_sector_, 
			$cnt_up.find('button#btn-md-ubicacion'), 
			$cnt_up.find('table#td-UP')
		);

		_load_combos();
		_load_haburbanas();

		if ( $.trim($cnt_up.find('input#txtHaburb').val()) != '' ) {
			events.emit('set_hurbana', $.trim($cnt_up.find('input#txtHaburb').val()));
		} else {
			events.emit('set_hurbana', null);
		}

		$inputs.filter('input[type=text].only-number').on('keydown.INDI.crear.UP.module', only_number);
		$inputs.filter('input[type=text].only-number').on('change.INDI.crear.UP.module', verify_number);
		$cnt_up.find('#txtsublote').on('keydown.INDI.crear.UP.module', _open_nex_tab);

		$txtHaburb.on('change.INDI.crear.UP.module', _search_haburbana);
		$inputs.filter('.datasend').on('change.INDI.crear.UP.module', _watcher_general);

	},

	_watcher_general = function() {

		var $this = $(this),
			name = $this.attr('name'),
			type = $this.attr('type'),
			tag = $this.prop("tagName").toLowerCase(),
			data = {};

		if ( tag == 'input' && type == 'text' ) {
			data[name] = $.trim($this.val());
		} else if ( tag == 'select' ) {
			data[name] = $this.children('option:selected').val();
		}

		events.emit('savedata', data);
		
	},

	_open_nex_tab = function(e) {

		if ( e.key == "Tab" && e.keyCode == 9 ) {
			$('#tabs-control').find('a[href="#tab_titularidad"]').tab('show');
			e.preventDefault();
			return false;
		}

	},

	_load_combos = function() {

		var $selects = $inputs.filter('select.autoload');

		$.each($selects, function(index, el) {

			var $el 			= $(el),
					type 			= $el.data('type'),
					sw 				= $el.data('sw'),
					resource 	= 'principal/getcombo/type/'+type+'/sw/'+sw;

			$.getJSON(INDI.base_url + resource, function(json, textStatus) {

				if ( ! json.error ) {

					json.data.forEach(function(obj, pos) {

						var params = { value: obj.CODIGO.trim() };

						if ( $el.data('sel') == params.value ) {
							params.selected = true;
						}

						$el.append( $('<option />', params).text(obj.NOMBRE) );

					});

					$el.change();

				} else {

					$el.attr({title:'No se encontraron datos'}).prop({disabled:true});

				}

			});

		});

	},

	_load_haburbanas = function() {

		var resource = INDI.base_url + 'individual/getsectorhaburbana/sector/' + SECTOR + '/pertenece/1';

		$.getJSON(resource, function(json) {
			
			if ( ! json.error ) {

				if ( json.data.length > 0 ) {

					dataHU = json.data;
					$txtHaburb.prop({disabled:false}).change();

				} else {

					$txtHaburb.prop({disabled:true});

				}
			} else {

				$txtHaburb.prop({disabled:true});

			}

		});

	},

	_search_haburbana = function() {

		var $this = $(this),
				value = $.trim($this.val()),
				foundObj = {},
				found = false;

		if (value.length == 0 && selectedHU == '') {
			//PASANDO CODIGO AL SUBMODULO
			events.emit('set_hurbana', null);
			$inputs.filter('input#txtdesc_haburba').val('');
			return false;
		} else if ( selectedHU != '' && INDI.crear.UP.ubicaciones.coutRows() > 0 ) {
			//$.bootstrapGrowl("Para poder cambiar de habilitación urbana debes eliminar todas las ubicaciones que pertenecen a ella", INDI.growl('danger'));
			//$this.val(selectedHU);
			//return false;
		}

		dataHU.forEach(function(obj, index) {
			if ( obj.CodHabUrb == value ) {
				foundObj = obj;
				found = true;
				return false;
			}
		});

		if ( found ) {

			$inputs.filter('input#txtdesc_haburba').val(foundObj.HabilitacionUrbana);

			$this.removeAttr('style', 'title');

			//PASANDO CODIGO AL SUBMODULO
			events.emit('set_hurbana', foundObj.CodHabUrb);
			selectedHU = foundObj.CodHabUrb;

		} else {

			$this.css({borderColor:'red'}).attr({title:'No existe la Habilitación urbana.'});
			$inputs.filter('input#txtdesc_haburba').val('');

			//PASANDO CODIGO AL SUBMODULO
			events.emit('set_hurbana', null);

			$this.val('');

			$.bootstrapGrowl("El codigo de habilitación urbana no existe.", INDI.growl('warning'));

		}

	};

	return {
		init: _bind
	};

}(window, window.jQuery));