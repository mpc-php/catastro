// @koala-append "Otros/_INDI.crear.OT.buscar.js"

/**
 * MODULO PARA OTROS
 */
INDI.crear.OT = (function (_win, $) {
	
	'use strict';

	var allowedOpen = false;

	var ID_Ficha = null,
			COD_Catastro = null;

	var $cnt_ot = $('div#tab_otros'),
			$inputs = $cnt_ot.find('input'),
			$md_buscar = $('section#md-buscar'),
			$table 		= $md_buscar.find('table#dt-buscar'),
			$bb_buscar = {};

	//CONSTRUCTOR
	var _bind = function (ficha, catastro) {

		ID_Ficha 			= ficha,
		COD_Catastro 	= catastro;

		$inputs.filter('.maskdate').inputmask();

		$inputs.filter('input[type=text].getdata').on('keydown.INDI.crear.OT.module', _capture_keyfunction);
		$inputs.filter('input[type=text].getdata').on('keyup.INDI.crear.OT.module', _open_modal);
		$inputs.filter('input[type=text].getdata').on('change.INDI.crear.OT.module', _request_info_personal);
		$inputs.filter('.datasend').on('change.INDI.crear.OT.module', _watcher_general);
		$inputs.filter('input[type=checkbox]').on('change.INDI.crear.OT.module', _limpiar_campos);

		INDI.crear.OT.buscar.init();

		events.on('getRowSelected', _select_row_modal);
		
		_check_values();

	},

	_select_row_modal = function(info) {
		var nameInput = info.target_elem.attr('name'),
				$descr_input = $inputs.filter(info.target_elem.data('inputref')),
				data = {};

		info.target_elem.val(info.CODIGO);
		$descr_input.val(info.NOMBRES);

		data[nameInput] = info.CODIGO;

		events.emit('savedata', data);

		_win.setTimeout(function() {
			info.target_elem.focus();
		}, 300);
		
	},

	_request_info_personal = function (e) {

		var $this 	= $(this),
				name 		= $this.attr('name'),
				val 		= $.trim($this.val()),
				params 	= $this.data();

		var parent = $this.parent('div.input-group');

		if ( val == '' ) {
			
			var data = {};
			data[name] = val;
			events.emit('savedata', data);

			$inputs.filter(params.inputref).val('');

			return false;
		}

		//armando los parametros de la consulta
		var strParams = 'rel/' + params.rel + '/';

		strParams += 'cond/' + val + '/';
		strParams += 't/' + params.t + '/';
		strParams += 'u/' + params.u;

		$this.prop({disabled:true});

		$.getJSON(INDI.base_url + 'individual/getpersona/' + strParams, function(json) {
			
			var data = {};

			if ( json.error ) {

				$.bootstrapGrowl('Datos referentes no encontrados.', INDI.growl('danger'));

				$this.css({borderColor:'red'}).attr({title:'Datos referentes no encontrados.'});

				$inputs.filter(params.inputref).val('');
				data[name] = '';

			} else {

				$this.removeAttr('style', 'title');

				if ( params.rel == 'pe' ) {
					$inputs.filter(params.inputref).val(json.data.nombre_completo);
					data[name] = json.data.nro_doc;
				} else {
					$inputs.filter(params.inputref).val(json.data.nombre_completo);
					data[name] = json.data.codigo;
				}

			}

			events.emit('savedata', data);

			$this.prop({disabled:false});

		});

		e.stopPropagation();

	},

	_capture_keyfunction = function(e) {

		allowedOpen = false;

		if ( e.which == 112 ) {

			allowedOpen = true;

			e.stopImmediatePropagation();
			e.preventDefault();
		}

	},

	_open_modal = function() {

		if ( allowedOpen ) {
			INDI.crear.OT.buscar.show($(this));
		}
	},

	_watcher_general = function() {

		var $this = $(this),
			name 		= $this.attr('name'),
			type 		= $this.attr('type'),
			tag 		= $this.prop("tagName").toLowerCase(),
			data 		= {};

		if ( tag == 'input' && type == 'text' ) {

			var value = this.value.trim();

			if ( $this.hasClass('maskdate') ) {
				if ( ! Inputmask.isValid(value, { alias: "dd/mm/yyyy"}) ) {
					value = '';
					$this.val(value);
				} else {
					$this.removeAttr('style');
				}
			}

			data[name] = value;

		} if ( tag == 'input' && type == 'checkbox' ) {
			data[name] = $this.is(':checked') ? '1' : '0';
		} else if ( tag == 'select' ) {
			data[name] = $this.children('option:selected').val();
		}

		events.emit('savedata', data);

	},

	_limpiar_campos = function() {

		if ( ! this.checked ) {
			var $this = $(this),
					parent = $this.parents('fieldset');

			parent.find('input[type=text]').val('');

			_win.setTimeout(function(){
				parent.find('input[type=text]:not(:disabled)').change();
			}, 500);

		}

	},

	_check_values = function() {
		$.each($inputs.filter('input[type=text].getdata'), function(index, element) {
			if ( element.value.trim() != '' ) {
				$(element).change();
			}
		});
	};

	return {
		init: _bind
	};

}(window, window.jQuery));



