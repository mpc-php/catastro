INDI.crear.OC.instalaciones.agregar = ( function(_win, $) {

	'use strict';

	var $btn_add = $('<button/>'),
			$md_mng = $('section#md-add-instalacion'),
			$inputs = $md_mng.find('input'),
			$bb_mng = {};

	var _bind = function(_btnactivator_) {

		$btn_add = _btnactivator_;

		$btn_add.on('click.INDI.crear.OC.instalaciones.agregar.module', _open_modal);

	},

	_submit_data = function(e) {

		var $this = $(this),
				data = $this.serialize(),
				resource = (Request.Host + Request.BaseUrl) + '/mantenedor/obrascomplementarias/manage';

		if ( $.trim($inputs.filter('#txtcode').val()) == '' ) {
			$.bootstrapGrowl('Este campo es requerido', INDI.growl('warning'));
			$inputs.filter('#txtcode').focus();
			e.preventDefault();
			return false;
		}

		if ( $.trim($inputs.filter('#txtname').val()) == '' ) {
			$.bootstrapGrowl('Este campo es requerido', INDI.growl('warning'));
			$inputs.filter('#txtname').focus();
			e.preventDefault();
			return false;
		}

		$this.find('button[type=submit]').prop({disabled: true});

		$.post(resource, data, function (response) {

			if (!response.error) {

				$bb_mng.find('#btn-close').click();

				$.bootstrapGrowl(response.message, INDI.growl('success'));

				INDI.crear.OC.instalaciones.table.refresh(
					$inputs.filter('#txtname').val()
				);

				events.emit('refreshComboInstalaciones', true);

			} else {

				$this.find('button[type=submit]').prop({disabled: false});
				$.bootstrapGrowl(response.message, INDI.growl('success'));

			}

		}).always(function () {
			$this.find('button[type=submit]').prop({disabled: false});
		});

		e.preventDefault();

	},

	_open_modal = function() {

		$bb_mng = $md_mng.cBootbox({
			title: "Nueva instalación",
			btnClose: '#btn-close',
			afterClose: _afterClose_modal,
			beforeOpen: function() {
				$md_mng.find('select').on('keypress', select_keyPress);
				$inputs.prop({disabled:false});

				$md_mng.find('button[type=submit]').prop({disabled:false});

				$md_mng.find('form').on('submit.INDI.crear.OC.instalaciones.agregar.module', _submit_data);

				_win.setTimeout(function(){
					$inputs.filter('#txtcode').focus();
				}, 300);
			}
		});

	},

	_open_modal_edit = function(e, value, row, index) {

		$md_mng.find('select').on('keypress', select_keyPress);

		//aBRIMOS EL MODAL PARA EL MANEJO DEL FORMULARIO
		$bb_mng = $md_mng.cBootbox({
			title: "Editar instalación",
			btnClose: "#btn-close",
			beforeOpen: function () {

				$inputs.filter('#txtcode').val(row.cod_instalacion);
				$inputs.filter('#txtname').val(row.instalacion);
				$inputs.filter('#txtunit').val(row.umedida);
				$inputs.filter('#txtmaterial').val(row.mate_predominante);

				$inputs.filter('#chkcalculo').prop({checked:row.calc_valor});
				$inputs.filter('#chklargo').prop({checked:row.largo});
				$inputs.filter('#chkancho').prop({checked:row.ancho});
				$inputs.filter('#chkalto').prop({checked:row.alto});

				$inputs.prop({disabled:false});

				$md_mng.find('button[type=submit]').prop({disabled:false});

				$md_mng.find('form').on('submit.INDI.crear.OC.instalaciones.agregar.module', _submit_data);

				_win.setTimeout(function(){
					$inputs.filter('#txtcode').focus();
				}, 300);

			},
			afterClose: _afterClose_modal
		});

	},

	_afterClose_modal = function() {

		$inputs.filter('[type=text]').val('');
		$inputs.filter('[type=checkbox]').prop({checked:false})

		$inputs.prop({disabled:true});

		$md_mng.find('button[type=submit]').prop({disabled:true});

	};

	return {
		init: _bind,
		edit: _open_modal_edit
	};

}(window, window.jQuery));