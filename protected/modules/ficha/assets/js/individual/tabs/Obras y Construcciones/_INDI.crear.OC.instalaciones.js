INDI.crear.OC.instalaciones = (function(_win, $){

	'use strict';

	var ID_Ficha,
			Sector;

	var $btn_add = $('<button />'),
			$md_mng = $('section#md-instalaciones'),
			$bb_mng = {},
			$table = $md_mng.find('table#dt-instalaciones');

	var _bind = function(_ficha_, _btnactivator_) {

		ID_Ficha = _ficha_;
		$btn_add = _btnactivator_;

		$btn_add.on('click.INDI.crear.OC.instalaciones.module', _open_modal);

	},

	_open_modal = function() {
		$bb_mng = $md_mng.cBootbox({
			title: "Instalaciones",
			btnClose: '#btn-cancel',
			afterClose: _afterClose_modal,
			beforeOpen: function() {
				_build_table();
				INDI.crear.OC.instalaciones.agregar.init($md_mng.find('#btn-add'));
			}
		});
	},

	_afterClose_modal = function() {
		$table.bootstrapTable('destroy');
	},

	_action_buttons = function (value, row, index) {
    return [
      '<button id="btn-edit-' + row.cod_instalacion + '" class="btn-actions edit btn btn-sm" data-id="' + row.cod_instalacion + '" title="Modificar datos de fila">',
      '<i class="fa fa-pencil"></i>',
      '</button>'
    ].join('');
  },

  _checkbox_cell_format = function(value, row, index) {

  	return '<input type="checkbox" '+ ((value) ? 'checked' : '') + ' onclick="return false;" onkeydown="e = e || window.event; if(e.keyCode !== 9) return false;" />';

  },

  _refresh_table = function(search) {
  	$table.bootstrapTable('refresh', {searchText:search});
  },

  /**
   * cONSTRULLE TABLA PRINCIPAL(CABECERA, CUERPO), ATRAVEZ DE UNA CONSULTA JSON
   * @return {VOID}
   */
  _build_table = function () {

    var bootTable = {
			escape: false,
		  locale: 'es-SP',
		  idField: "cod_instalacion",
		  pagination:true,
		  search: true,
		  onPostHeader: function(e){
				$table.find('tr th').removeAttr('tabindex');

				_win.setTimeout(function(){
					($bb_mng.find('.search')).children('input').focus();
				}, 200);

		  }
		};

    bootTable.url = INDI.base_url + 'individual/getotrasinstalaciones';

    //TABLE HEADER
    bootTable.columns = [
    	{
          field: 'action',
          title: '<i class="fa fa-cog" aria-hidden="true"></i>',
          align: 'center',
          formatter: _action_buttons,
          events: {
              'click .edit': INDI.crear.OC.instalaciones.agregar.edit
          }
      },
      {field: 'cod_instalacion', title: 'Código', align: 'center', sortable: true},
      {field: 'instalacion', title: 'Descripcion', align: 'left', sortable: true},
      {field: 'mate_predominante', title: 'Material Predominante', align: 'left', sortable: true},
      {field: 'umedida', title: 'U. Medida', align: 'left', sortable: true},
      {field: 'largo', title: 'Largo', align: 'left', value: 'lrg', formatter: _checkbox_cell_format},
      {field: 'ancho', title: 'Ancho', align: 'left', value: 'anc', formatter: _checkbox_cell_format},
      {field: 'alto', title: 'Alto', align: 'left', value: 'alt', formatter: _checkbox_cell_format},
      {field: 'calc_valor', title: 'Calculo', align: 'left', value: 'cal', formatter: _checkbox_cell_format}
    ];

    $table.bootstrapTable(bootTable);

  };

	return {
		init: _bind,
		table: {
			refresh: _refresh_table
		}
	}

}(window, window.jQuery));