// @koala-append "TItularidad/_INDI.crear.TI.titular.js"
// @koala-append "TItularidad/_INDI.crear.TI.titular.agregar.js"
// @koala-append "TItularidad/_INDI.crear.TI.titular.buscar.js"
// @koala-append "TItularidad/_INDI.crear.TI.titular.buscar.vias.js"
// @koala-append "TItularidad/_INDI.crear.TI.titular.buscar.haburbanas.js"

/**
 * MODULO PARA LA PESTAÑA TITULARIDAD
 */
INDI.crear.TI = (function (_win, $) {

	'use strict';

	var ID_Ficha 			= null,
			COD_Catastro 	= null;

	var $cnt_ti 			= $('div#tab_titularidad'),
			$inputs 			= $cnt_ti.find('input, select'),
			$inputs_mask 	= $inputs.filter('.maskdate');

	//CONSTRUCTOR
	var _bind = function (_ficha_, _catastro_, _sector_, _ubigeo_) {

		ID_Ficha 			= _ficha_,
		COD_Catastro 	= _catastro_;

		$cnt_ti.find('button#btn-add-titular').on('click.INDI.crear.TI.module', _before_open_titular);
		$cnt_ti.find('#btn-add-titular').on('keydown.INDI.crear.TI.module', _open_nex_tab);

		//INSTANCIANDO SUBMODULO
		INDI.crear.TI.titular.init(
			_ficha_,
			_catastro_,
			_sector_,
			_ubigeo_,
			$cnt_ti.find('button#btn-add-titular'),
			$cnt_ti.find('table#td-titular')
		);

		_load_combos();

		$inputs_mask.inputmask();

		$inputs.filter('input[type=text].only-number').on('keydown.INDI.crear.TI.module', only_number);
		$inputs.filter('input[type=text].only-number').on('change.INDI.crear.TI.module', verify_number);

		$inputs.filter('#txtsearch_cbocond_especialpredio').on('change.INDI.crear.TI.module', _search_in_cbo);

		$inputs.filter('.datasend').on('change.INDI.crear.TI.module', _watcher_general);

	},

	_before_open_titular = function(e) {

		var $condti = $inputs.filter('select#cbocond_titular');

		if ($condti.val() == '') {

			$.bootstrapGrowl("DEBES ESCOGER UNA CONDICIÓN DEL TITULAR", INDI.growl('warning'));
			e.stopImmediatePropagation();

		} else if ( $condti.val() == '05' || $condti.val() == '06' ) {

			$.bootstrapGrowl("LA CONDICIÓN DEL TITULAR DEBE SER DIFERENTE A <b>COTITULARIDAD</b> Ó <b>LITIGIO</b>, PARA INGRESAR UN NUEVO TITULAR", INDI.growl('warning'));
			e.stopImmediatePropagation();

		} else if ( $condti.val() != '04' && $cnt_ti.find('#td-titular tbody tr:not(.no-records-found)').length >= 1  ) {

			$.bootstrapGrowl("LA CONDICIÓN DEL TITULAR DEBE SER <b>SOCIEDAD CONYUGAL</b>, PARA PODER INGRESAR OTRO TITULAR", INDI.growl('warning'));
			e.stopImmediatePropagation();

		}

	},

	_watcher_general = function() {

		var $this = $(this),
				name 	= $this.attr('name'),
				type 	= $this.attr('type'),
				tag 	= $this.prop("tagName").toLowerCase(),
				data 	= {};

		if ( tag == 'input' && type == 'text' ) {

			var value = this.value.trim();

			if ( $this.hasClass('maskdate') ) {
				if ( ! Inputmask.isValid(value, { alias: "dd/mm/yyyy"}) ) {
					value = '';
					$this.val(value);
				} else {
					$this.removeAttr('style');
				}
			}

			data[name] = value;

		} else if ( tag == 'select' ) {
			data[name] = $this.children('option:selected').val();
		}

		events.emit('savedata', data);

	},

	_open_nex_tab = function(e) {

		if ( e.key == "Tab" && e.keyCode == 9 ) {
			$('#tabs-control').find('a[href="#tab_descpredio"]').tab('show');
			e.preventDefault();
			return false;
		}

	},

	_load_combos = function() {

		var $selects = $inputs.filter('select.autoload');

		$.each($selects, function(index, el) {

			var $el 			= $(el),
					type 			= $el.data('type'),
					sw 				= $el.data('sw'),
					resource 	= 'principal/getcombo/type/'+type+'/sw/'+sw;

			$.getJSON(INDI.base_url + resource, function(json, textStatus) {

				if ( ! json.error ) {

					json.data.forEach(function(obj, pos) {

						var params = { value:obj.CODIGO.trim() };

						if ( $el.data('sel') == params.value ) {
							params.selected = true;
						}

						$el.append( $('<option />', params).text(obj.NOMBRE) );

					});

				} else {

					$el.attr({title:'No se encontraron datos'}).prop({disabled:true});

				}

			});

		});

	},

	_search_in_cbo = function() {

		var $this = $(this),
				$cbo 	= $inputs.filter('#cbocond_especialpredio'),
				$item = $cbo.children('option[value="' + $.trim($this.val()) + '"]');

		if ( $item.length == 0 ) {

			$this.val('').css({borderColor:'red'});
			$cbo.children('option:eq(0)').prop({selected:true});

		} else {

			$this.removeAttr('style');
			$cbo.children('option:eq(0)').removeAttr('selected');
			$item.prop({selected:true});

		}

		$cbo.change();

	};

	return {
		init: _bind
	};

}(window, window.jQuery));
