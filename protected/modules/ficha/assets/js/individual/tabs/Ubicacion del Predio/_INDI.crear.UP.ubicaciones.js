//SUBMODULO PARA EL MANEJO DE UBICACIONES
INDI.crear.UP.ubicaciones = (function (_win, $) {

	'use strict';

	var ID_Ficha = null,
			COD_Catastro = null,
			COD_HUrbana = null,
			Sector = null;

	var datasend = {},
			default_ = {
				ficha: ID_Ficha,
				rowID: 0,
				via: 0,
				nro_municipal: '',
				tipo_puerta: 0,
				cond_num: '',
				nro_certificado: '',
				action: ''
			};

	var $btn_add 	= $('<button />'),
			$table 		= $('<table />'),
			$md_mng 	= $('section#md-ubicaciones'),
			$bb_mng 	= {},
			$inputs 	= $md_mng.find('input, select');

	//Constructor
	var _bind = function( _ficha_, _catastro_, _sector_, _btnadd_, _table_ ) {

		ID_Ficha 			= _ficha_;
		COD_Catastro 	= _catastro_;
		Sector 				= _sector_;

		$btn_add 			= _btnadd_;
		$table 				= _table_;

		_build_table();

		events.on('set_hurbana', _set_cod_hurbana);

		$btn_add.on('click.INDI.crear.UP.ubicaciones.module', _open_md_new);

	},

	_set_cod_hurbana = function( _codigo_ ) {

		COD_HUrbana = _codigo_;

	},

	_open_md_new = function() {

		/*if ( COD_HUrbana == null ) {
			$.bootstrapGrowl("digita un código, correcto, de habilitación urbanas.", INDI.growl('warning'));
			return false;
		}*/

		$bb_mng = $md_mng.cBootbox({
			title: "Ubicación del predio",
			btnClose: '#btn-close',
			beforeOpen: function() {

				_load_cbovias();
				_load_combos();

				$md_mng.find('input, select').prop({disabled:false});
				$md_mng.find('button#btn-done').prop({disabled:false});

				_asignar_eventos();

				datasend.action = 'create';

				_win.setTimeout(function(){
					$md_mng.find('input#txtcodvia').focus();
				}, 500);

			},
			afterClose: _afterClose_modal
		});

	},

	_open_md_edit = function (e, value, row, index) {

		/*if ( COD_HUrbana == null ){
			$.bootstrapGrowl("digita un código, correcto, de habilitación urbanas.", INDI.growl('warning'));
			return false;
		}*/

		$bb_mng = $md_mng.cBootbox({
			title: "Ubicación del predio",
			btnClose: '#btn-close',
			beforeOpen: function() {

				_asignar_eventos();

				$md_mng.find('input, select').prop({disabled:false});
				$md_mng.find('button#btn-done').prop({disabled:false});

				$md_mng.find('input#txtcodvia').val(row.cod_via).change();
				//$md_mng.find('select#cbovias').data({id:row.cod_via});
				$md_mng.find('select#cbotipo_puerta').data({sel:row.TIPO_PUERTA});
				$md_mng.find('input#txtnro_municip').val(row.nro_municipal).change();
				$md_mng.find('select#cbocondnum').data({sel:row.COND_NUMERACION});
				$md_mng.find('input#txtnro_certnumer').val(row.NRO_CERTIF_NUMERAC).change();

				datasend.rowID = row.ID;
				datasend.via = row.cod_via;
				datasend.nro_municipal = row.nro_municipal;
				datasend.tipo_puerta = row.TIPO_PUERTA;
				datasend.cond_num = row.COND_NUMERACION;
				datasend.nro_certificado = row.NRO_CERTIF_NUMERAC;

				_load_cbovias();
				_load_combos();


				datasend.action = 'update';

				_win.setTimeout(function(){
					$md_mng.find('input#txtcodvia').focus();
				}, 500);

			},
			afterClose: _afterClose_modal
		});

	},

	_delete_row = function(e, value, row, index) {

		var $this = $(this),
				postParams = {},
				resource = INDI.base_url + 'individual/mantenimientodirecciones';

		bootbox.confirm({
			title: 'Seguro que quieres eliminar?',
			message: "La fila seleccionada será eliminada de forma permanente.",
			callback: function(result) {

				if ( result ) {

					if ( ID_Ficha == null ) {
						$.bootstrapGrowl("La ficha no está asignada, el proceso a sido cancelado", INDI.growl('warning'));
						return false;
					} else {
						postParams.ficha 	= ID_Ficha;
					}

					postParams.rowID 		= row.ID;
					postParams.action 	= 'delete';

					$this.prop({disabled:true});

					$.post(resource, postParams, function(response) {

						if ( ! response.error ) {

							$.bootstrapGrowl(response.message, INDI.growl('success'));

							$table.bootstrapTable('refresh');

							$this.prop({ disabled: false });

						} else {

							$this.prop({disabled:false});

							$.bootstrapGrowl(response.message, INDI.growl('danger'));

						}

					});

				}
			}
		});

	},

	_asignar_eventos = function() {

		$md_mng.find('select').on('keypress', select_keyPress);

		$inputs.filter('input[type=text].only-number').on('keydown.INDI.crear.UP.ubicaciones.module', only_number);
		$inputs.filter('input[type=text].only-number').on('change.INDI.crear.UP.ubicaciones.module', verify_number);

		$md_mng.find('button#btn-done').on('click.INDI.crear.UP.ubicaciones.module', _submit_data);

		$md_mng.find('input#txtcodvia')
			.on('keydown.INDI.crear.UP.ubicaciones.module', only_number)
			.on('change.INDI.crear.UP.ubicaciones.module', _selected_to_cbovia);

		//$md_mng.find('select#cbovias').on('change.INDI.crear.UP.ubicaciones.module', _copy_codvia_to_input);

		$inputs.filter('.md-datasend').on('change.INDI.crear.UP.ubicaciones.module', _watcher_change);

	},

	_afterClose_modal = function() {

		$md_mng.find('select#cbovias').off('change.INDI.crear.UP.ubicaciones.module');

		var $selects = $md_mng.find('select');

		$selects.children('option.default').prop({selected:true});
		$selects.children('option:not(.default)').remove();

		$selects.removeData('sel');
		$selects.removeData('id');
		$selects.prop({disabled:true});

		$md_mng.find('input').val('').prop({
			disabled: true
		});

		datasend = {};

		$btn_add.focus();

	},

	_load_cbovias = function () {

		var resource = INDI.base_url + 'individual/gethaburbanavia/haburbana/'+COD_HUrbana;

		$.getJSON(resource, function(json) {
			
			var $cbovias = $md_mng.find('select#cbovias');
			
			if ( !json.error ) {

				//LLENANDO LAS OPCIONES DE LA LISTA DE VIAS
				json.data.forEach(function(obj, index) {

					var opts = { value:obj.Cod_via };

					if ( typeof(datasend.rowID) != 'undefined' && $cbovias.data('id') == obj.Cod_via ) {
						opts.selected = true;
						datasend.via = obj.Cod_via;
					}

					$cbovias.append($('<option />', opts).text(obj.via));
					
				});

				if ( typeof(datasend.rowID) != 'undefined' ) {
					if ( $cbovias.children('option:selected').eq() == 0 ){
						$md_mng.find('input#txtcodvia').val('');
					}
				}

			}

		});

	},

	_load_combos = function() {

		var $selects = $inputs.filter('select.autoload');

		$.each($selects, function(index, el) {

			var $el 			= $(el),
					type 			= $el.data('type'),
					sw 				= $el.data('sw'),
					resource 	= 'principal/getcombo/type/'+type+'/sw/'+sw;

			$.getJSON(INDI.base_url + resource, function(json, textStatus) {

				if ( ! json.error ) {

					json.data.forEach(function(obj, pos) {

						var params = { value: obj.CODIGO };

						if ( $el.data('sel') == obj.NOMBRE ) {
							params.selected = true;
						}

						$el.append( $('<option />', params).text(obj.NOMBRE) );

					});

					$el.change();

				} else {

					$el.attr({title:'No se encontraron datos'}).prop({disabled:true});

				}

			});

		});

		setFocus($md_mng.find('input#txtcodvia'));

	},

	_copy_codvia_to_input = function() {

		var $this = $(this),
				$input = $md_mng.find('input#txtcodvia');

		if ( $this.children('option:selected').val() != '' )
			$input.val($this.val()).removeAttr('style');

	},

	_selected_to_cbovia = function() {

		var $this = $(this),
				code = this.value.trim();

		if (code.length == 0) return false;

		$this.prop({disabled:true});

		$.getJSON(INDI.base_url + 'principal/searchvia/code/' + code, function(json) {
			
			if ( ! json.error ) {

				$inputs.filter('#txtdescvias').val(json.data.NOMBRE);
				$this.removeAttr('style');

			} else {

				$.bootstrapGrowl("La via no existe.", INDI.growl('warning'));
				$this.css({ borderColor: 'red' }).val('');
				$inputs.find('#txtdescvias').val('');

			}

			$this.prop({disabled:false});

		});

	},

	_watcher_change = function() {

		var $this = $(this),
			name 		= $this.attr('name'),
			type 		= $this.attr('type'),
			tag 		= $this.prop("tagName").toLowerCase();

		if ( tag == 'input' && type == 'text' ) {

			datasend[name] = $.trim($this.val());

		} else if ( tag == 'select' ) {

			datasend[name] = $this.children('option:selected').val();

		}

	},

	_submit_data = function() {

		datasend.tipo_puerta = $inputs.filter('select#cbotipo_puerta').val();

		datasend = $.extend({}, default_, datasend);

		if ( ID_Ficha == null ) {
			$.bootstrapGrowl("La ficha no está asignado, el proceso a sido cancelado", INDI.growl('warning'));
			return false;
		} else {
			datasend.ficha = ID_Ficha;
		}

		if ( datasend.via == 0 ) {
			$.bootstrapGrowl("Debe seleccionar una via", INDI.growl('warning'));
			$inputs.filter('input#txtcodvia').focus();
			return false;
		}

		if ( datasend.tipo_puerta == 0 ) {
			$.bootstrapGrowl("Debe seleccionar un tipo de puerta", INDI.growl('warning'));
			$inputs.filter('select#cbotipo_puerta').focus();
			return false;
		}

		var $elems = $bb_mng.find('select, input, button');

		$elems.prop({ disabled: true });

		$.post(INDI.base_url + 'individual/mantenimientodirecciones', datasend, function(response) {

			if ( ! response.error ) {

				$.bootstrapGrowl(response.message, INDI.growl('success'));

				//ACTIVAR LOS ELEMENTOS
				$elems.filter('input').val('');
				($elems.filter('select')).children('option.default').prop({selected:true});

				//HABILITAR TODOS LOS CONTROLES PARA AGREGAR NUEVOS DATOS
				$elems.prop({disabled:false});

				if ( datasend.rowID > 0 ) {
					$bb_mng.find('#btn-close').click();
				} else {
					//PONER EL FOCO EN EL PRIMER ELEMENTO
					$elems.filter('input#txtcodvia').focus();
				}

				//LIMPIAR LA DATA PARA UNA NUEVA INSERCIÓN
				var tmp_action = datasend.action;
				datasend = {};
				datasend.action = tmp_action;

				//REFRESCAR LA TABLA
				$table.bootstrapTable('refresh');

			} else {
				$.bootstrapGrowl(response.message, INDI.growl('danger'));
				$elems.prop({disabled:false});
				$elems.filter('input#txtcodvia').focus();
			}

		});

	},

	_get_rows_counts = function() {
		return $table.find('tbody tr:not(.no-records-found)').length;
	},

	_action_buttons = function(value, row, index) {
		
    return [
			'<button id="btn-edit-'+row.ID+'" class="btn-actions edit btn btn-sm" data-id="'+row.ID+'" tabindex="-1">',
				'<i class="fa fa-pencil" aria-hidden="true"></i>',
			'</button>',
			'<button id="btn-delete-'+row.ID+'" class="btn-actions delete btn btn-sm" data-id="'+row.ID+'" tabindex="-1">',
				'<i class="fa fa-trash-o" aria-hidden="true"></i>',
			'</button>'
		].join('');

  },

	_build_table = function() {

  	var bootTable = {
		  locale: 'es-SP',
		  idField: "ID",
		  sortable: false,
		  url: INDI.base_url + 'principal/gettable/type/04/rel/' + ID_Ficha,
		  onPostHeader: function(e){
				$table.find('tr th').removeAttr('tabindex');
		  }
		};

  	//TABLE HEADER
		bootTable.columns = [
			{ 
				field: 'action', 
				title: '<i class="fa fa-cog" aria-hidden="true"></i>', 
				align: 'center', 
				formatter : _action_buttons,
				events : {
					'click .edit': _open_md_edit,
					'click .delete': _delete_row
	    	}
	    },
			{ field: 'ID', title: 'ID', align: 'center', sortable:true },
			{ field: 'id_ficha', title: 'Ficha', align: 'left', sortable:true, visible:false },
			{ field: 'cod_via', title: 'Cod. Vía', align: 'left', sortable:true },
			{ field: 'TIPO_VIA', title: 'Tipo vía', align: 'left', sortable:true },
			{ field: 'NOM_VIA', title: 'Nombre vía', align: 'left', sortable:true },
			{ field: 'TIPO_PUERTA', title: 'Tipo de puerta', align: 'left', sortable:true },
			{ field: 'nro_municipal', title: 'N° Municipal', align: 'right', sortable:true },
			{ field: 'COND_NUMERACION', title: 'Cond. Numer.', align: 'left', sortable:true },
			{ field: 'NRO_CERTIF_NUMERAC', title: 'N° Certificado', align: 'right', sortable:true }
		];

		$table.bootstrapTable(bootTable);

  };

	return {
		init: _bind,
		coutRows: _get_rows_counts
	}

}(window, window.jQuery));