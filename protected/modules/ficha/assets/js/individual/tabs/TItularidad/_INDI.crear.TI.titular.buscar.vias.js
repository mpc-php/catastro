/**
 * SUBMODULO PARA AGREGAR PERSONAS
 */
INDI.crear.TI.titular.buscar.vias = ( function (_win, $) {

	'use strict';

	var SECTOR,
			allowedOpen = false,
			vias_arr;

	var $md_mng = $('section#md-buscar'),
			$table 	= $md_mng.find('table#dt-buscar'),
			$input = $('input[type=text]#txtcod_via'),
			$bb_mng = {};

	var _bind = function() {

		$input.on('keydown.INDI.crear.TI.buscar.vias.module', _capture_keyfunction);
		$input.on('keyup.INDI.crear.TI.buscar.vias.module', _open_modal);

		events.on('setVias', _set_vias);

	},

	_set_vias = function( data ) {
		vias_arr = data;
	},

	_capture_keyfunction = function(e) {

		allowedOpen = false;

		if ( e.which == 112 ) {

			allowedOpen = true;

			e.stopImmediatePropagation();
			e.preventDefault();
		}

	},

	_open_modal = function(e) {

		var that =  this;

		if ( allowedOpen ) {
			$bb_mng = $md_mng.cBootbox({
				title: "Vias de Hab. Urbana",
				btnClose: '#btn-cancel',
				afterClose: _afterClose_modal,
				beforeOpen: function() {

					_build_table(vias_arr, that.value.trim());

					$md_mng.find('button#btn-done, button#btn-new').prop({disabled:false});

					$md_mng.find('button#btn-done').on('click.INDI.crear.TI.buscar.vias.module', _click_selection);

				}

			});

			allowedOpen = false;
			e.preventDefault();
		}

	},

	_dblClick_selection = function(row, $element, field) {

		events.emit('getRowSelectedVia', row);

		$bb_mng.find('button#btn-cancel').click();

	},

	_click_selection = function() {

		var selection = $table.bootstrapTable('getSelections');

		if ( selection.length > 0 ) {
			events.emit('getRowSelectedVia', selection[0]);
		}

		$bb_mng.find('button#btn-cancel').click();

	},

	_actions_postHeader = function() {

		setFocus(($bb_mng.find('.search')).children('input'), false);

		$table.find('thead tr th').removeAttr('tabindex');

	},

	_afterClose_modal = function() {
		$table.bootstrapTable('destroy');
	},

	_build_table = function(data, searchText) {

		var bootTable = {
		  locale: 'es-SP',
		  search: true,
		  selectItemName: 'vias',
		  clickToSelect: true,
		  idField: "Cod_via",
		  height: 410,
		  pagination:true,
		  data: data,
		  searchText: searchText,
		  onDblClickRow: _dblClick_selection,
		  onPostHeader: _actions_postHeader
		};
  	//TABLE HEADER
		bootTable.columns = [
			{ field: 'state', radio: true, align: 'center' },
			{ field: 'Cod_via', title: 'Cod. Vía', align: 'center' },
			{ field: 'via', title: 'Vía', align: 'left' }
		];

		$table.bootstrapTable(bootTable);

	};

	return {
		init: _bind
	};

}(window, window.jQuery));