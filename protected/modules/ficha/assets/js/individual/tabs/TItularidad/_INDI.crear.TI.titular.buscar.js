/**
 * SUBMODULO PARA BUSQUEDA DE PERSONAS
 */
INDI.crear.TI.titular.buscar = (function(_win, $) {

	'use strict';

	var tipo_titular = 1;

	var $btn_activator = $('<button />');

	var $md_mng 	= $('section#md-buscar-titular'),
			$btn_new 	= $md_mng.find('button#btn-new'),
			$table 		= $md_mng.find('table#dt-buscar-titular'),
			$bb_mng 	= {};

	var _bind = function(_btnactivator_) {

		$btn_activator = _btnactivator_;

		$btn_activator.on('click.INDI.crear.TI.titular.buscarmodule', _open_md_new);

		$md_mng.find('select#cbobuscarpor').append([
			'<option value="" selected disabled>Seleccione...</option>',
			'<option value="1">N° Documento</option>',
			'<option value="2">Nombres</option>'
		]);

		events.on('set_tipo_titular', _set_tipo_titular);

	},

	_set_tipo_titular = function(tipo) {

		tipo_titular = tipo;

		var $cbo = $md_mng.find('select#cbobuscarpor');

		$cbo.children('option').remove();

		if ( tipo_titular == 1 ) {

			$cbo.append([
				'<option value="0" class="default" selected disabled>Seleccione...</option>',
				'<option value="1">N° Documento</option>',
				'<option value="2">Nombres</option>'
			]);

		} else if ( tipo_titular == 2 ) {
			$cbo.append([
				'<option value="" selected disabled>Seleccione...</option>',
				'<option value="1">N° RUC</option>',
				'<option value="2">Razón Social</option>'
			]);
		}

	},

	_open_md_new = function() {

		$bb_mng = $md_mng.cBootbox({
			title: "Buscar persona",
			btnClose: '#btn-cancel',
			afterClose: _afterClose_modal,
			beforeOpen: function() {

				_build_table();

				$md_mng.find('button#btn-done, button#btn-new').prop({disabled:false});
				$md_mng.find('button#btn-buscar').on('click.INDI.crear.TI.titular.buscar.module', _iniciar_busqueda);
				$md_mng.find('input#txtfiltro').on('keyup.INDI.crear.TI.titular.buscar.module', _trigger_iniciar_busqueda);
				$md_mng.find('button#btn-done').on('click.INDI.crear.TI.titular.buscar.module', _aceptar_fila_seleccionada);

				INDI.crear.TI.titular.agregar.init(
					$md_mng.find('button#btn-new'),
					$table
				);

				var select = $btn_activator.parents('div.input-group').find('select#cbotipo_titular');

				tipo_titular = select.children('option:selected').val();

				_win.setTimeout(function(){
					$md_mng.find('#cbobuscarpor').focus();
				}, 500);

			}
		});

	},

	_aceptar_fila_seleccionada = function() {

		var selection = $table.bootstrapTable('getSelections');

		if ( selection.length > 0 ) {
			events.emit('search_titular', selection[0].ID_PERSONA);
		}

		$bb_mng.find('button#btn-cancel').click();

	},

	_trigger_iniciar_busqueda = function(e) {

		var code = e.which;

    if ( code == 13 ) {
    	_iniciar_busqueda()
    }

	},

	_iniciar_busqueda = function() {

		var $input = $md_mng.find('#txtfiltro'),
				$select = $md_mng.find('select#cbobuscarpor');

		if ( ($input.val()).trim() == '' ) {
			$input.focus();
			return false;
		}

		if ( $select.find('option:selected').val() == '' ) {
			$select.focus();
			return false;
		}

		var resource = INDI.base_url + 'individual/showpersonasficha/';

		resource += 'tipo/' + ((tipo_titular == 1) ? '111/' : '102/');
		resource += 'rel/' + (( $select.find('option:selected').val() == '1' ) ? 'doc/' : 'name/');
		resource += 'search/' + ($input.val()).trim();

		$.getJSON(resource, function(json) {

			$table.bootstrapTable('load', json.data);

		});

	},

	_afterClose_modal = function() {
		$table.bootstrapTable('destroy');
		($md_mng.find('select#cbobuscarpor').children('option:eq(0)')).prop({selected:true});
		$md_mng.find('select#cbobuscarpor').change();
		$md_mng.find('input').val('');
	},

	_choise_option = function( objRow, row ) {

		events.emit('search_titular', objRow.ID_PERSONA);

		$bb_mng.find('button#btn-cancel').click();

	},

	_build_table = function() {

		var bootTable = {
			escape: true,
		  locale: 'es-SP',
		  search: true,
		  searchAlign:'left',
		  selectItemName: 'personas',
		  height: 300,
		  clickToSelect: true,
		  pagination:true,
		  data: [],
		  idField: "ID_PERSONA",
		  onDblClickRow: _choise_option,
		  onPostHeader: function(e){
				$table.find('tr th').removeAttr('tabindex');
		  }
		};

  	//TABLE HEADER
		bootTable.columns = [
			{ field: 'state', radio: true, align: 'center' },
			{ field: 'ID_PERSONA', title: 'ID', align: 'center', sortable:true },
			{ field: 'nro_doc', title: 'N° Doc.', align: 'left', sortable:true },
			{ field: 'NOMBRES', title: 'Nombre de persona', align: 'left', sortable:true }
		];

		$table.bootstrapTable(bootTable);

	};

	return {
		init: _bind
	}

}(window, window.jQuery));