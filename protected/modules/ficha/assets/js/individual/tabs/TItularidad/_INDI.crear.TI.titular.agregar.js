/**
 * SUBMODULO PARA AGREGAR PERSONAS
 */
INDI.crear.TI.titular.agregar = (function(_win, $) {

	'use strict';

	var sizeRUC = 11,
			sizeDNI = 8;

	var datasend = {},
			default_ = {
				tipo_titular: 1,
				tipo_documento: '',
				nro_documento: '',
				nombres: '',
				ape_paterno: '',
				ape_materno: '',
				persona_juridica: '',
				estado_civil: '',
				action: ''
			};

	var $btn_activator = $('<button />'),
			$table 					= $('<table />'),
			$md_mng 				= $('section#md-mng-titular'),
			$inputs 				= $md_mng.find('input, select'),
			$btn_done 			= $md_mng.find('button#btn-done'),
			$bb_mng 				= {};

	var _bind = function(_btnactivator_, _table_) {

		$btn_activator = _btnactivator_;
		$table = _table_;

		$btn_activator.on('click.INDI.crear.TI.titular.buscarmodule', _open_md_new);

	},

	_watcher_change = function() {

		var $this = $(this),
			name = $this.attr('name'),
			type = $this.attr('type'),
			tag = $this.prop("tagName").toLowerCase();

		if ( tag == 'input' && type == 'text' ) {

			datasend[name] = this.value.trim();

		}  else if ( tag == 'select' ) {

			datasend[name] = $this.children('option:selected').val();

		}

	},

	_submit_data = function() {

		datasend = $.extend({}, default_, datasend);

		if ( datasend.tipo_titular != 1 && datasend.tipo_titular != 2 ) {
			$.bootstrapGrowl("DEBES ESCOGER UN TIPO DE PERSONA VALIDO", INDI.growl('danger'));
			$inputs.filter('select#cbotipo_titular').focus();
			return false;
		}

		if ( datasend.tipo_documento == '' ) {
			$.bootstrapGrowl("DEBES ESCOGER UN TIPO DE DOCUMENTO VALIDO", INDI.growl('danger'));
			$inputs.filter('select#cbotipo_documento').focus();
			return false;
		}

		if ( datasend.tipo_titular == 1 ) {

			if ( datasend.ape_paterno == '' && (datasend.nombres.substring(0, 3)).toUpperCase() == 'SUC' ) {
				$.bootstrapGrowl("ESTE CAMPO ES REQUERIDO", INDI.growl('danger'));
				$inputs.filter('input#txtape_paterno').focus();
				return false;
			}

			if ( datasend.ape_materno == '' && (datasend.nombres.substring(0, 3)).toUpperCase() == 'SUC' ) {
				$.bootstrapGrowl("ESTE CAMPO ES REQUERIDO", INDI.growl('danger'));
				$inputs.filter('input#txtape_materno').focus();
				return false;
			}

			if ( datasend.nombres == '' ) {
				$.bootstrapGrowl("INGRESE UN NUMERO DE DOCUMENTO", INDI.growl('danger'));
				$inputs.filter('input#txtnro_documento').focus();
				return false;
			}

			if ( datasend.estado_civil == '' ) {
				$.bootstrapGrowl("ESCOJA UN ESTADO CIVIL", INDI.growl('danger'));
				$inputs.filter('input#cboestado_civil').focus();
				return false;
			}

		} else {

			if ( datasend.persona_juridica == '' ) {
				$.bootstrapGrowl("ESCOJA EL TIPO DE PERSONA JURIDICA", INDI.growl('danger'));
				$inputs.filter('input#cbopersona_juridica').focus();
				return false;
			}

		}

		if ( datasend.nombres == '' ) {
			$.bootstrapGrowl("INGRESE UN NUMERO DE DOCUMENTO", INDI.growl('warning'));
			$inputs.filter('input#txtnro_documento').focus();
			return false;
		}

		$.post(INDI.base_url + 'individual/mantenimientopersonas', datasend, function(response) {

			if ( ! response.error ) {

				$.bootstrapGrowl(response.message, INDI.growl('success'));

				$table.bootstrapTable('refresh', {searchText:datasend.nro_documento});

				$bb_mng.find('button#btn-close').click();

			}
		});

	},

	_open_md_new = function() {

		datasend.action = 'create';

		$bb_mng = $md_mng.cBootbox({
			title: "Agregar persona",
			size: 'small',
			btnClose: '#btn-close',
			afterClose: _afterClose_modal,
			beforeOpen: function() {

				$md_mng.find('select').on('keypress', select_keyPress);

				_load_combos();

				$inputs.filter('select#cbotipo_titular')
					.on('change.INDI.crear.TI.titular.agregar.module', _active_controls_tipo_persona);

				$inputs.filter('select#cbotipo_documento')
					.on('change.INDI.crear.TI.titular.agregar.module', _cambio_tipo_documento);

				$inputs.filter('.md-datasend')
					.on('change.INDI.crear.TI.titular.agregar.module', _watcher_change);

				$btn_done.on('click.INDI.crear.TI.titular.agregar.module', _submit_data);

				$btn_done.prop({disabled:false});

				_win.setTimeout(function(){
					$inputs.filter('select#cbotipo_titular').focus();
				}, 500);

			}
		});

	},

	_afterClose_modal = function() {

		$inputs.filter('input').val('');

		($inputs.filter('select')).find('option.default').prop({selected:true});

		datasend = {};

	},

	_active_controls_tipo_persona = function() {

		if ( parseInt(this.value.trim()) == 1 ) {

			$md_mng.find('label#lblnro_documento').text('N° Documento');
			$md_mng.find('label#lblnombre').text('Nombres');

			($inputs.filter([
				'select#cbotipo_documento',
				'select#cbopersona_juridica'
			].join(','))).find('option.default').prop({selected:true}).change();

			$inputs.filter('select#cbopersona_juridica').prop({disabled:true});

			$inputs.filter([
				'select#cboestado_civil',
				'input#txtape_paterno',
				'input#txtape_materno',
				'select#cbotipo_documento'
			].join(',')).prop({disabled:false});

		} else if ( parseInt(this.value.trim()) == 2 ) {

			$md_mng.find('label#lblnro_documento').text('N° RUC');
			$md_mng.find('label#lblnombre').text('Razón social');

			$inputs.filter('select#cbopersona_juridica').prop({disabled: false});

			$inputs.filter([
				'select#cboestado_civil',
				'input#txtape_paterno',
				'input#txtape_materno'
			].join(',')).prop({disabled: true});

			($inputs.filter('select#cbotipo_documento')).find('option[value="00"]').prop({selected:true}).change();
			($inputs.filter('select#cboestado_civil')).find('option.default').prop({selected:true}).change();

		}

		$inputs.filter('input[type=text]').val('').change();

	},

	_cambio_tipo_documento = function(e) {

		var value = this.options[this.selectedIndex].value.trim(),
				text = this.options[this.selectedIndex].text.trim()

		if ( text == 'NO PRESENTO DOCUMENTO' ) {

			$inputs.filter('input#txtnro_documento').prop({disabled:true}).val('');
			$inputs.filter('input#txtnombres').focus();

		} else if ( text == 'DOCUMENTO NACIONAL DE IDENTIDAD' ) {
			$inputs.filter('input#txtnro_documento').prop({disabled:false, maxLength: sizeDNI});
		} else if ( text == 'REGISTRO UNICO DE CONTRIBUYENTE' ) {
			$inputs.filter('input#txtnro_documento').prop({disabled:false, maxLength: sizeRUC});
		} else {
			$inputs.filter('input#txtnro_documento').prop({maxLength:50}).removeProp('disabled');
		}

	},

	_load_combos = function() {

		var $selects = $inputs.filter('select.autoload');

		$.each($selects, function(index, el) {

			var $el 			= $(el),
					type 			= $el.data('type'),
					sw 				= $el.data('sw'),
					resource 	= 'principal/getcombo/type/'+type+'/sw/'+sw;

			$el.children('option:not(.default)').remove();

			$.getJSON(INDI.base_url + resource, function(json, textStatus) {

				if ( ! json.error ) {

					json.data.forEach(function(obj, pos) {

						var params = { value: obj.CODIGO.trim() };

						if ( $.trim($el.data('sel')) == params.value ) {
							params.selected = true;
						}

						$el.append( $('<option />', params).text(obj.NOMBRE) );

					});

					if ( $el.data('exechange') )
						$el.change();

				} else {

					$el.attr({title:'No se encontraron datos'}).prop({disabled:true});

				}

			});

		});

	};

	return {
		init: _bind
	};

}(window, window.jQuery));