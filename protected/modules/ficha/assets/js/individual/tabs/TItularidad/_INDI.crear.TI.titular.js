/**
 * SUBMODULO PARA REGISTRAR TITULARIDAD
 */
INDI.crear.TI.titular = (function(_win, $) {

	'use strict';

	var sizeRUC = 11,
			sizeDNI = 8,
			action = '';

	var HUrbanas_arr 	= [],
			Vias_arr 			= [];

	var ID_Ficha 			= null,
			COD_Catastro 	= null,
			Ubigeo 				= null,
			Sector 				= null;

	var datasend = {},
			default_ = {
				ficha: null,
				id_persona: 0,
				por_adquisicion: '',
				cod_contribuyente: '',
				forma_adquisicion: '',
				f_adquisicion: '01/01/1990',
				estado_civil: '',
				ubigeo: '',
				telefono: '',
				anexo: '',
				fax: '',
				email: '',
				islocal: 0,
				via: '',
				nro_municipalidad: '',
				nombre_edificio: '',
				nro_interior: '',
				hurbana: '',
				manzana: '',
				lote: '',
				sublote: '',
				condesp_titular: '',
				nro_resolucion: '',
				nro_boleta_pension: '',
				f_inicio: '',
				f_vencimiento: ''
			};

	var $btn_add 			= $('<button />'),
			$table 				= $('<table />'),
			$md_mng 			= $('section#md-titular'),
			$bb_mng 			= {},
			$inputs 			= $md_mng.find('input, select');

	var _bind = function(_ficha_, _catastro_, _sector_, _ubigeo_, _btnadd_, _table_) {

		ID_Ficha 			= _ficha_;
		COD_Catastro 	= _catastro_;
		Sector 				= _sector_;
		Ubigeo 				= _ubigeo_;
		$btn_add 			= _btnadd_;
		$table 				= _table_;

		_build_table();

		$btn_add.on('click.INDI.crear.TI.titular.module', _open_md_new);

		events.on('search_titular', _busca_persona_from_modal);
		events.on('getRowSelectedHaburbana', _select_row_haburbana);
		events.on('getRowSelectedVia', _select_row_via);

	},

	_open_md_new = function() {

		$bb_mng = $md_mng.cBootbox({
			title: "Agregar titular",
			size: 'large',
			btnClose: '#btn-close',
			afterClose: _afterClose_modal,
			beforeOpen: function() {

				action = 'create';

				_load_combos();
				_load_haburbanas();
				_load_vias('');

				_load_ubigeos($inputs.filter('select#cbodepartamento'), '0', true);

				_asignar_eventos();

				$md_mng.find('button#btn-done').prop({disabled:false});

				INDI.crear.TI.titular.buscar.init( $md_mng.find('button#btn-buscar-titular') );

				INDI.crear.TI.titular.buscar.haburbanas.init();
				INDI.crear.TI.titular.buscar.vias.init();

				_win.setTimeout(function(){
					$inputs.filter('select#cbotipo_titular').focus();
				}, 500);

			}
		});

	},

	_open_md_edit = function( e, value, row, index ) {

		$bb_mng = $md_mng.cBootbox({
			title: "Editar titular",
			size: 'large',
			btnClose: '#btn-close',
			afterClose: _afterClose_modal,
			beforeOpen: function() {

				action = 'update';

				var resource = INDI.base_url + '/individual/gettitularinfo/';

				resource += 'ficha/' + ID_Ficha + '/persona/' + row.codigo;

				//Consultando si es que pertenece ubicación de predio
				$.getJSON(resource, function(json) {

					if ( ! json.error ) {

						_asignar_eventos();

						//CARGAMOS LOS DATOS A LOS ELEMENTOS
						$inputs.filter('select#cbotipo_titular').data({sel:parseInt(json.data.tip_persona), exechange:true});
						$inputs.filter('select#cboforma_adquisicion').data({sel:json.data.FORMA_ADQ});
						$inputs.filter('select#cboestado_civil').data({sel:json.data.ESTADO_CIVIL});
						$inputs.filter('select#cbotipo_documento').data({sel:json.data.TIP_DOC, exechange:true});
						$inputs.filter('select#cbopersona_juridica').data({sel:json.data.TIP_PERSONA_JURIDICA.trim()});
						$inputs.filter('select#cbocondesp_titular').data({sel:json.data.CONDICION});

						var $cbodep =$inputs.filter('select#cbodepartamento'),
								$cbopro =$inputs.filter('select#cboprovincia'),
								$cbodis =$inputs.filter('select#cbodistrito');

						var dep = json.data.ID_UBI_GEO.substr(0, 2),
								pro = json.data.ID_UBI_GEO.substr(0, 4),
								dis = json.data.ID_UBI_GEO.substr(0, 6);

						$cbodep.data({sel:dep});
						$cbopro.data({sel:pro});
						$cbodis.data({sel:dis});

						$inputs.filter('input#txtcod_contribuyente').val(json.data.COD_CONTRIBUY.trim());
						$inputs.filter('input#txtf_adquisicion').val(json.data.FECHA_ADQ.trim());
						$inputs.filter('input#txtpor_adquisicion').val(json.data.PORCENTAJE_PERTENENCIA);
						$inputs.filter('input#txtnro_documento').val(json.data.NRO_DOC);
						$inputs.filter('input#txtnombre').val( json.data.NOMBRES + ' ' + json.data.APE_PATERNO + ' ' + json.data.APE_MATERNO );
						$inputs.filter('input#txtnro_resolucion').val(json.data.NRO_RESOLUCION);
						$inputs.filter('input#txtnro_boleta_pension').val(json.data.NRO_BOLETA_PENSION);
						$inputs.filter('input#txtf_inicio').val(json.data.FECHA_INICIO);
						$inputs.filter('input#txtf_vencimiento').val(json.data.FECHA_VENCIMIENTO);
						$inputs.filter('input#txttelefono').val(json.data.TELEFONO);
						$inputs.filter('input#txtanexo').val((json.data.ANEXO || ''));
						$inputs.filter('input#txtfax').val(json.data.FAX);
						$inputs.filter('input#txtemail').val(json.data.CORREO_ELECT);

						$inputs.filter('input#txtcod_hurbana').val(json.data.COD_HAB_URB);
						$inputs.filter('input#txtnombre_hurbana').val(json.data.NOM_HAB_URB);
						$inputs.filter('input#txtzon_sec_etapa').val(json.data.ZN_SC_ETAPA);
						$inputs.filter('input#txtmanzana').val(json.data.MZA_URBANA);
						$inputs.filter('input#txtlote').val(json.data.LOTE_URBANO);
						$inputs.filter('input#txtsublote').val(json.data.SUB_LOTE);

						$inputs.filter('input#txtcod_via').val(json.data.cod_via);
						$inputs.filter('input#txtnombre_via').val(json.data.nom_via);
						$inputs.filter('input#txtnro_municipalidad').val(json.data.NRO_MUNICIPAL);
						$inputs.filter('input#txtnombre_edificio').val(json.data.NOMBRE_EDIFICA);
						$inputs.filter('input#txtnro_interior').val(json.data.NRO_INTERIOR);

						//CARGAMOS LOS DATOS QUE SE ENVIARAN MEDIANTE POST
						datasend.id_persona 				=  row.codigo,
						datasend.por_adquisicion 		=  json.data.PORCENTAJE_PERTENENCIA,
						datasend.cod_contribuyente 	=  json.data.COD_CONTRIBUY,
						datasend.forma_adquisicion 	=  json.data.FORMA_ADQ,
						datasend.f_adquisicion 			=  json.data.FECHA_ADQ,
						datasend.estado_civil 			=  json.data.ESTADO_CIVIL,
						datasend.ubigeo 						=  json.data.ID_UBI_GEO,
						datasend.telefono 					=  json.data.TELEFONO,
						datasend.anexo 							=  (json.data.ANEXO || ''),
						datasend.fax 								=  json.data.FAX,
						datasend.email 							=  json.data.CORREO_ELECT,
						datasend.islocal 						=  (json.data.ID_UBI_GEO == Ubigeo) ? 1 : 0,
						datasend.via 								=  (json.data.ID_UBI_GEO == Ubigeo) ? json.data.cod_via : json.data.nom_via,
						datasend.nro_municipalidad 	=  json.data.NRO_MUNICIPAL,
						datasend.nombre_edificio 		=  json.data.NOMBRE_EDIFICA,
						datasend.nro_interior 			=  json.data.NRO_INTERIOR,
						datasend.hurbana 						=  (json.data.ID_UBI_GEO == Ubigeo) ? json.data.COD_HAB_URB : json.data.NOM_HAB_URB,
						datasend.manzana 						=  json.data.MZA_URBANA,
						datasend.lote 							=  json.data.LOTE_URBANO,
						datasend.sublote 						=  json.data.SUB_LOTE,
						datasend.condesp_titular 		=  json.data.CONDICION,
						datasend.nro_resolucion 		=  json.data.NRO_RESOLUCION,
						datasend.nro_boleta_pension =  json.data.NRO_BOLETA_PENSION,
						datasend.f_inicio 					=  json.data.FECHA_INICIO,
						datasend.f_vencimiento 			=  json.data.FECHA_VENCIMIENTO

						//LLENAMOS LOS COMBOS
						_load_combos();

						_load_haburbanas();
						_load_vias('');

						//VERIFICAMOS SI EL UBIGEO ES EL DE CALLAO PARA ACTIVAR
						//EL CHECKBOX QUE INDICA QUE LA INFORMACIÓN ESTA DENTRO
						//DE LA DATA DE CATASTO
						/*if ( json.data.ID_UBI_GEO == Ubigeo ) {

							$inputs.filter('input#chklocal').prop({checked:true}).change();
							
						} else {

							$inputs.filter('select#cboprovincia').prop({disabled:false});
							$inputs.filter('select#cbodistrito').prop({disabled:false});

							_load_ubigeos($cbodep, '0', false);
							_load_ubigeos($cbopro, dep, false);
							_load_ubigeos($cbodis, pro, false);

						}*/

						_load_ubigeos($cbodep, '0', false);
						_load_ubigeos($cbopro, dep, false);
						_load_ubigeos($cbodis, pro, false);

						$inputs.filter('select#cboprovincia').prop({disabled:false});
						$inputs.filter('select#cbodistrito').prop({disabled:false});

						$md_mng.find('button#btn-done').prop({disabled:false});

						_win.setTimeout(function(){
							$inputs.filter('select#cbotipo_titular').focus();
						}, 500);

					} else {
						$.bootstrapGrowl("ERROR, NO SE ENCONTRARON LOS DATOS DE LA PERSONA", INDI.growl('danger'));
						$bb_mng.find('button#btn-close').click();
					}

				});

				INDI.crear.TI.titular.buscar.init($md_mng.find('button#btn-buscar-titular'));
				INDI.crear.TI.titular.buscar.haburbanas.init();
				INDI.crear.TI.titular.buscar.vias.init();

			}
		});

	},

	_delete_row = function(e, value, row, index) {

		var $this = $(this),
				postParams = {},
				resource = INDI.base_url + 'individual/mantenimientotitular';

		bootbox.confirm({
			title: 'Seguro que quieres eliminar?',
			message: "El Titular: <b>"+row.TITULAR+"</b>, será eliminado de forma permanente de esta ficha.",
			callback: function(result) {

				if ( result ) {

					if ( ID_Ficha == null ) {
						$.bootstrapGrowl("La ficha no está asignada, el proceso a sido cancelado", INDI.growl('warning'));
						return false;
					} else {
						postParams.ficha = ID_Ficha;
					}

					postParams.id_persona = row.codigo;
					postParams.action = 'delete';

					$this.prop({disabled:true});

					$.post(resource, postParams, function(response) {

						if ( ! response.error ) {

							$.bootstrapGrowl(response.message, INDI.growl('success'));
							$table.bootstrapTable('refresh');
							$this.prop({ disabled: false });

						} else {

							$this.prop({disabled:false});
							$table.bootstrapTable('refresh');
							$.bootstrapGrowl(response.message, INDI.growl('danger'));

						}

					});

				}
			}
		});

	},

	_asignar_eventos = function() {

		$md_mng.find('select').on('keypress', select_keyPress);

		$inputs.filter('select#cbotipo_titular').on('change.INDI.crear.TI.titular.module', _active_controls_tipo_persona);

		$inputs.filter('input[type=checkbox]#chklocal').on('change.INDI.crear.TI.titular.module', _seleccionar_ubigeolocal);

		$inputs.filter('input[type=text].only-number').on('keydown.INDI.crear.DR.module', only_number);
		$inputs.filter('input[type=text].only-number').on('change.INDI.crear.DR.module', verify_number);

		$inputs.filter('select#cbodepartamento').on('change.INDI.crear.TI.titular.module', _choise_departamento);
		$inputs.filter('select#cboprovincia').on('change.INDI.crear.TI.titular.module', _choise_provincia);
		$inputs.filter('select#cbodistrito').on('change.INDI.crear.TI.titular.module', _choise_distrito);
		$inputs.filter('input#txtcod_hurbana').on('focusin.INDI.crear.TI.titular.module', _reload_haburbanas);
		$inputs.filter('input#txtcod_hurbana').on('change.INDI.crear.TI.titular.module', _search_haburbana);
		$inputs.filter('input#txtcod_via').on('change.INDI.crear.TI.titular.module', _search_vias);
		$inputs.filter('input#txtnro_documento').on('change.INDI.crear.TI.titular.module', _buscar_x_documento);
		$inputs.filter('input#txtnombre').on('change.INDI.crear.TI.titular.module', _buscar_x_nombres);
		$inputs.filter('select#cbotipo_documento').on('change.INDI.crear.TI.titular.module', _cambio_tipo_documento);
		$inputs.filter('.md-datasend').on('change.INDI.crear.TI.titular.module', _watcher_change);
		$inputs.filter('input#txtcod_contribuyente').on('change.INDI.crear.TI.titular.module', _buscar_contribuyente);

		$md_mng.find('button#btn-done').on('click.INDI.crear.TI.titular.module', _submit_data);

		$inputs.filter('.masker').inputmask();

	},

	_afterClose_modal = function() {

		$inputs.filter('input[type=text]').val('');
		$inputs.filter('input[type=checkbox]').prop({checked:false}).change();

		var $selects = $inputs.filter('select');

		$selects.removeData('sel');
		$selects.find('option:not(.default)').remove();
		$selects.find('option.default').prop({selected:true});

		($selects.filter('select#cbotipo_titular')).find('option:eq(1)');
		$selects.filter('select#cbotipo_titular').change();

		Vias_arr = [];
		HUrbanas_arr = [];
		datasend = [];

	},

	_watcher_change = function() {

		var $this = $(this),
			name = $this.attr('name'),
			type = $this.attr('type'),
			tag = $this.prop("tagName").toLowerCase();

		if ( tag == 'input' && type == 'text' ) {

			var value = this.value.trim();

			if ( $this.hasClass('masker') ) {
				if ( ! Inputmask.isValid(value, { alias: "dd/mm/yyyy"}) ) {
					value = '';
					$this.val(value);
				} else {
					$this.removeAttr('style');
				}
			}

			datasend[name] = value;

		} else if ( tag == 'input' && type == 'checkbox' ) {

			datasend[name] = $this.is(':checked') ? 1 : 0;

		} else if ( tag == 'select' ) {

			datasend[name] = $this.children('option:selected').val();

		}

	},

	_submit_data = function() {

		datasend = $.extend({}, default_, datasend);

		if ( ID_Ficha == null ) {
			$.bootstrapGrowl("La ficha no está asignado, el proceso a sido cancelado", INDI.growl('warning'));
			return false;
		} else {
			datasend.ficha = ID_Ficha;
		}

		var tipo_persona = ($inputs.filter('select#cbotipo_titular')).children('option:selected').val();
		if ( tipo_persona != 1 && tipo_persona !=  2 ) {
			$.bootstrapGrowl("DEBES ESCOGER UN TIPO DE PERSONA VALIDO", INDI.growl('warning'));
			$inputs.filter('select#cbotipo_titular').focus();
			return false;
		}

		if ( datasend.ubigeo.length == 0 ) {
			$.bootstrapGrowl("ES NECESARIO CONOCER EL LA UBICACIÓN DE LA PERSONA", INDI.growl('warning'));
			$inputs.filter('select#cbodepartamento').focus();
			return false;
		}

		if ( datasend.id_persona == 0 ) {
			$.bootstrapGrowl("NO HAY DATOS DE LA PERSONA", INDI.growl('warning'));
			$inputs.filter('select#cbotipo_documento').focus();
			return false;
		}

		if ( $inputs.filter('#cbocondesp_titular').prop('selectedIndex') > 0 ) {

			if ( $.trim($inputs.filter('#txtnro_resolucion').val()) == '' ) {
				$.bootstrapGrowl("Dato requerido", INDI.growl('warning'));
				$inputs.filter('#txtnro_resolucion').focus();
				return false;
			}

			if ( $.trim($inputs.filter('#txtnro_boleta_pension').val()) == '' ) {
				$.bootstrapGrowl("Dato requerido", INDI.growl('warning'));
				$inputs.filter('#txtnro_boleta_pension').focus();
				return false;
			}

			var finicio = $.trim($inputs.filter('#txtf_inicio').val());
			if ( finicio == '' && (! Inputmask.isValid(finicio, { alias: "dd/mm/yyyy"})) ) {
				$.bootstrapGrowl("Dato requerido y la fecha tiene que ser correcta", INDI.growl('warning'));
				$inputs.filter('#txtf_inicio').focus();
				return false;
			}

			var fvencimiento = $.trim($inputs.filter('#txtf_vencimiento').val());
			if ( fvencimiento == '' && (! Inputmask.isValid(fvencimiento, { alias: "dd/mm/yyyy"})) ) {
				$.bootstrapGrowl("Dato requerido y la fecha tiene que ser correcta", INDI.growl('warning'));
				$inputs.filter('#txtf_vencimiento').focus();
				return false;
			}
		}

		datasend.action = action;

		$md_mng.find('button#btn-donde').prop({disabled:true});

		$.post(INDI.base_url + '/individual/mantenimientotitular', datasend, function(response) {

			if ( ! response.error ) {

				$.bootstrapGrowl(response.message, INDI.growl('success'));

				$table.bootstrapTable('refresh');

				$bb_mng.find('#btn-close').click();

			} else {

				$.bootstrapGrowl(response.message, INDI.growl('danger'));
				$md_mng.find('button#btn-donde').prop({disabled:false});
				$inputs.filter('select#cbotipo_titular').focus();

			}

		});

	},

	_cambio_tipo_documento = function(e) {

		var value = this.options[this.selectedIndex].value.trim(),
				text = this.options[this.selectedIndex].text.trim()

		if ( value == '01' ) {

			$inputs.filter('input#txtnro_documento').prop({disabled:true});

			if ( action == 'create' ) {
				$inputs.filter('input#txtnro_documento').val('');
			}

		} else if ( text == 'DOCUMENTO NACIONAL DE IDENTIDAD' ) {
			$inputs.filter('input#txtnro_documento').prop({disabled:false, maxLength: sizeDNI});
		} else if ( text == 'REGISTRO UNICO DE CONTRIBUYENTE' ) {
			$inputs.filter('input#txtnro_documento').prop({disabled:false, maxLength: sizeRUC});
		} else {
			$inputs.filter('input#txtnro_documento').prop({maxLength:50}).removeProp('disabled');
		}

	},

	_buscar_x_documento = function() {

		var $this = $(this),
				$cbotiti = $inputs.filter('select#cbotipo_titular'),
				$cbotidoc = $inputs.filter('select#cbotipo_documento');

		if ( $cbotiti.val() != '' ) {

			if ( $cbotidoc.val() == '00' && (this.value.trim()).length != sizeRUC ) {

				$this.css({borderColor:'red'}).val('');
				$.bootstrapGrowl("El numero de documento debe tener "+sizeRUC+" digitos exactamente.", INDI.growl('warning'));
				return false;

			} else if ( $cbotidoc.val() == '02' && (this.value.trim()).length != sizeDNI ) {

				$this.css({borderColor:'red'}).val('');
				$.bootstrapGrowl("El numero de documento debe tener "+sizeDNI+" digitos exactamente.", INDI.growl('warning'));
				return false;

			}

			$this.removeAttr('style');

			_busca_persona( 3, this.value.trim(), $this, $inputs.filter('#txt') );

		}

	},

	_buscar_x_nombres = function() {

		var cbo = document.getElementById('#cbotipo_documento');

		if ( this.value.trim() && cbo.options[cbo.selectedIndex].text == 'NO PRESENTO DOCUMENTO' ) {
			_busca_persona(2, this.value.trim(), $(this), $inputs.filter('#txt'));
		}

	},

	_busca_persona_from_modal = function(valor) {

		if ( valor != '' ) {

			_busca_persona(
				1,
				valor,
				$inputs.filter('#txtnro_documento'),
				$inputs.filter('#txtcod_contribuyente')
			);

		} else {
			_win.setTimeout(function(){
				$md_mng.find('#btn-buscar-titular');
			}, 300);
		}

	},

	_busca_persona = function( tipo, valorabuscar, $element, $elementFocus ) {

		var resource = 'individual/gettitular',
				params = {
					type: tipo,
					str: valorabuscar
				};

		if ( ! valorabuscar ) return false;

		$element.prop({disabled:true}).css({cursor:'progress'});

		$.post(INDI.base_url + resource, params, function(json) {

			if ( ! json.error ) {

				if ( json.data.length > 0 ) {

					var foundObj = json.data[0];

					if ( json.data.length > 1 )  {

						foundObj = {};

						json.data.forEach( function(obj, index) {
							if ( (obj.nro_documento.trim()).length > 0 ) foundObj = {};
						});

					}

					datasend.id_persona = foundObj.id_persona;
					$inputs.filter('#txtnombre').val(foundObj.nombres);

					if ( tipo == 1 )
						$inputs.filter('#txtnro_documento').val(foundObj.nro_documento);

					var $cbotipotitu = $inputs.filter('#cbotipo_titular');
					if ( foundObj.tipo_persona.trim() != $cbotipotitu.val() ) {
						$cbotipotitu.children('option[value="'+foundObj.tipo_persona.trim()+'"]').prop({selected:true});
					}

					var $cbotipodoc = $inputs.filter('#cbotipo_documento');
					if ( foundObj.tipo_documento.trim() != $cbotipodoc.val() ) {
						$cbotipodoc.children('option[value="'+foundObj.tipo_documento.trim()+'"]').prop({selected:true}).change();
					}

					var $cboestado = $inputs.filter('#cboestado_civil');
					if ( foundObj.estado_civil.trim() == '' ) {
						$cboestado.children('option.default').prop({selected:true}).change();
					} else {
						$cboestado.children('option[value="'+foundObj.estado_civil.trim()+'"]').prop({selected:true}).change();
					}

					var $cbotpj = $inputs.filter('#cbopersona_juridica');
					if ( foundObj.tipo_persona_juridica.trim() == '' ) {
						$cbotpj.children('option.default').prop({selected:true}).change();
					} else {
						$cbotpj.children('option[value="'+foundObj.tipo_persona_juridica.trim()+'"]').prop({selected:true}).change();
					}

				} else {
					datasend.id_persona = '';
					$element.val('').css({borderColor:'red'});
					$.bootstrapGrowl("No se encontró datos de la persona", INDI.growl('warning'));
					$inputs.filter('#txtnombre').val('').change();
					$inputs.filter('#cboestado_civil').children('option.default').prop({selected:true}).change();
					$inputs.filter('#cbopersona_juridica').children('option.default').prop({selected:true}).change();
				}

			} else {
				datasend.id_persona = '';
				$element.val('').css({borderColor:'red'});
				$.bootstrapGrowl("No se encontró datos de la persona", INDI.growl('warning'));
				$inputs.filter('#txtnombre').val('').change();
				$inputs.filter('#cboestado_civil').children('option.default').prop({selected:true}).change();
				$inputs.filter('#cbopersona_juridica').children('option.default').prop({selected:true}).change();
			}

			$element.prop({disabled:false}).removeAttr('style');

			_win.setTimeout(function(){
				$elementFocus.focus();
			}, 300);

		});

	},

	_buscar_contribuyente = function() {
		if ( this.value.trim() == '' ) {
			return false;
		}

		$.getJSON(INDI.base_url + 'individual/buscarcontribuyente', function(json) {
			if ( ! json.error ) {
				$.bootstrapGrowl(json.message, INDI.growl('info'));
			}
		});

	},

	_seleccionar_ubigeolocal = function() {

		var $this = $(this);

		if ( this.checked ) {

			$inputs.filter('select.ubigeo').prop({disabled:true});

			var $cbodep = $inputs.filter('select#cbodepartamento'),
					$cbopro = $inputs.filter('select#cboprovincia'),
					$cbodis = $inputs.filter('select#cbodistrito');

			var dep = Ubigeo.substr(0, 2),
					pro = Ubigeo.substr(0, 4),
					dis = Ubigeo.substr(0, 6);

			$cbodep.data({sel:dep});
			$cbopro.data({sel:pro});
			$cbodis.data({sel:dis});

			datasend.ubigeo = Ubigeo;

			_load_ubigeos($cbodep, '0', false);
			_load_ubigeos($cbopro, dep, false);
			_load_ubigeos($cbodis, pro, false);

			var resource = INDI.base_url + 'principal/getubicacionpredio/rel/' + ID_Ficha;

			$this.prop({disabled:true});

			$.getJSON(resource, function(json) {

				var result = json.data;
				
				if ( ! $.isEmptyObject(result) ) {
				
					var $panelUP 			= $('#tab_ubicpredio'),
							codhaburbana 	= (result.codigo_haburbana.length > 0) ? result.codigo_haburbana : $panelUP.find('#txtcode_haburba').val(),
							haburbana 		= (result.haburbana.length > 0) ? result.haburbana : $panelUP.find('#txtdesc_haburba').val(),
							zona 					= result.zona,
							manzana 			= (result.manzana.length > 0) ? result.manzana : $panelUP.find('#txtmanzana').val(),
							lote 					= (result.lote.length > 0) ? result.lote : $panelUP.find('#txtlote').val(),
							sublote 			= (result.sublote.length > 0) ? result.sublote : $panelUP.find('#txtsublote').val(),
							codvia 				= result.codigo_via,
							via 					= result.via,
							nro_municipal = result.nro_municipal,
							edificacion 	= (result.edificacion.length > 0) ? result.edificacion : $panelUP.find('#txtname_edificio').val(),
							nro_interior 	= (result.nro_interior.length > 0) ? result.nro_interior : $panelUP.find('#txtnumber_interior').val();
					
					$inputs.filter('#txtcod_hurbana').val(codhaburbana);
					$inputs.filter('#txtnombre_hurbana').val(haburbana)
					$inputs.filter('#txtzon_sec_etapa').val(zona);
					$inputs.filter('#txtcod_via').val(codvia);
					$inputs.filter('#txtnombre_via').val(via);

					if (manzana.length > 0 ) {
						$inputs.filter('#txtmanzana').val(manzana);
						datasend.manzana = manzana;
					}

					if (lote.length > 0) {
						$inputs.filter('#txtlote').val(lote);
						datasend.lote = lote;
					}

					if (sublote.length > 0) {
						$inputs.filter('#txtsublote').val(sublote);
						datasend.sublote = sublote;
					}

					if (nro_municipal.length > 0) {
						$inputs.filter('#txtnro_municipalidad').val(nro_municipal);
						datasend.nro_municipalidad = nro_municipal;
					}

					if (edificacion.length > 0) {
						$inputs.filter('#txtnombre_edificio').val(edificacion);
						datasend.nombre_edificio = edificacion;
					}

					if (nro_interior.length > 0) {
						$inputs.filter('#txtnro_interior').val(nro_interior);
						datasend.nro_interior = nro_interior;
					}

					datasend.via = codvia;
					datasend.hurbana = codhaburbana;
					
				}

				$inputs.filter('#txtcod_hurbana,#txtcod_via').prop({disabled:false});
				$inputs.filter('#txtnombre_hurbana, #txtzon_sec_etapa, #txtnombre_via').prop({disabled:true});

				$this.prop({disabled:false});

			});

			$inputs.filter('#txttelefono').focus();

		} else {

			$inputs.filter('select.ubigeo').removeData('sel');
			$inputs.filter('select.ubigeo:eq(0)').prop({disabled:false});
			($inputs.filter('select.ubigeo')).children('option.default').prop({selected:true});

			($inputs.filter('select#cboprovincia')).find('option:not(.default)').remove();
			($inputs.filter('select#cbodistrito')).find('option:not(.default)').remove();

			$inputs.filter('select#cbodistrito').change();

			$inputs.filter('#txtcod_hurbana').val('');
			$inputs.filter('#txtnombre_hurbana').val('');
			$inputs.filter('#txtzon_sec_etapa').val('');
			$inputs.filter('#txtmanzana').val('');
			$inputs.filter('#txtlote').val('');
			$inputs.filter('#txtsublote').val('');
			$inputs.filter('#txtcod_via').val('');
			$inputs.filter('#txtnombre_via').val('');
			$inputs.filter('#txtnro_municipalidad').val('');
			$inputs.filter('#txtnombre_edificio').val('');
			$inputs.filter('#txtnro_interior').val('');

			datasend.ubigeo = '';
			datasend.via = '';
			datasend.nro_municipalidad = '';
			datasend.nombre_edificio = '';
			datasend.nro_interior = '';
			datasend.hurbana = '';
			datasend.manzana = '';
			datasend.lote = '';
			datasend.sublote = '';

			$inputs.filter('#txtcod_hurbana,#txtcod_via').prop({disabled:true});
			$inputs.filter('#txtnombre_hurbana, #txtnombre_via').prop({disabled:false});

			$inputs.filter('select#cbodepartamento').focus();

		}

	},

	_choise_departamento = function() {

		var $this 	= $(this),
				cbopro 	= $inputs.filter('select#cboprovincia'),
				cbodis 	= $inputs.filter('select#cbodistrito'),
				curCode = $.trim($this.children('option:selected').val());

		if ( curCode != '0' ) {

			cbopro.prop({disabled: true});

			_load_ubigeos(cbopro, curCode, true);

			cbodis.children('option:not(:eq(0))').remove();
			cbodis.prop({disabled:true});

		} else {
			cbopro.prop({disabled: true});
			cbodis.children('option:not(:eq(0))').remove();
			cbodis.prop({disabled:true});
		}

	},

	_choise_provincia = function() {

		var $this = $(this),
				cbodis = $inputs.filter('select#cbodistrito'),
				curCode = $.trim($this.children('option:selected').val());

		if ( curCode != '0' ) {

			cbodis.prop({ disabled:true });

			_load_ubigeos(cbodis, curCode, true);

		} else {
			cbodis.find('option:not(:eq(0))').remove();
		}

	},

	_choise_distrito = function() {

		var $chklocal = $inputs.filter('input[type=checkbox]#chklocal');
		
		datasend.via = '';
		datasend.hurbana = '';

		if ( this.value == Ubigeo ) {

			$inputs.filter('#txtcod_hurbana,#txtcod_via').prop({disabled:false});
			$inputs.filter('#txtnombre_hurbana, #txtzon_sec_etapa, #txtnombre_via').prop({disabled:true});

			if ( ! $chklocal.is(':checked') ) {
				$inputs.filter('#txtcod_hurbana,#txtcod_via'),val('');
				$inputs.filter('#txtnombre_hurbana, #txtzon_sec_etapa, #txtnombre_via').val('');
			}

			//if ( ! $chklocal.is(':checked') ) $chklocal.prop({checked:true}).change();

		} else {

			$inputs.filter('#txtcod_hurbana,#txtcod_via').prop({disabled:true});
			$inputs.filter('#txtnombre_hurbana, #txtnombre_via').prop({disabled:false});

			//if ( $chklocal.is(':checked') ) $chklocal.prop({checked:false});

		}

	},

	_search_vias = function() {

		var $this 		= $(this),
				value 		= this.value.trim(),
				foundObj 	= {},
				found 		= false;

		if ( value.length == 0 ) {
			$inputs.filter('input#txtnombre_via').val('');
			return false;
		}

		Vias_arr.forEach(function(obj, index) {
			if ( obj.Cod_via == value ) {
				foundObj = obj;
				found = true;
				return false;
			}
		});

		if ( found ) {

			$inputs.filter('input#txtnombre_via').val(foundObj.via);
			$this.removeAttr('style', 'title');

		} else {

			$this.css({
				borderColor:'red'
			}).attr({
				title: "No existe vía para la habilitación urbana."
			});

			$inputs.filter('input#txtnombre_via').val('');

			$this.val('');

			$.bootstrapGrowl("No existe vía para la habilitación urbana.", INDI.growl('warning'));

		}

	},

	_search_haburbana = function() {

		var $this 		= $(this),
				value 		= this.value.trim(),
				foundObj 	= {},
				found 		= false;

		if ( value.length == 0 ) {
			$inputs.filter('input#txtnombre_hurbana,input#txtzon_sec_etapa').val('');
			//Vias_arr = [];
			return false;
		}

		HUrbanas_arr.forEach(function(obj, index) {
			if ( obj.code == value ) {
				foundObj = obj;
				found = true;
				return false;
			}
		});

		if ( found ) {

			//_load_vias(foundObj.code);

			$inputs.filter('input#txtnombre_hurbana').val(foundObj.name);
			$inputs.filter('input#txtzon_sec_etapa').val(foundObj.zoni);

			$this.removeAttr('style', 'title');

		} else {

			$this.css({borderColor:'red'}).attr({title:'No existe la Habilitación urbana para este sector'});
			$inputs.filter('input#txtnombre_hurbana,input#txtzon_sec_etapa').val('');

			$this.val('');

			//Vias_arr = [];

			$.bootstrapGrowl("No existe habilitación urbana.", INDI.growl('warning'));

		}

	},

	_load_ubigeos = function($select, _cod, _enabled, _exec_change) {

		var code = _cod || '0';

		if ( $select.hasClass('sending') ) {
			_win.clearTimeout($select.data('timeID'));
		}

		$select.children('option:eq(0)').prop({ selected: true });
		$select.children('option:not(:eq(0))').remove();

		(function (cd, $cbo) {

			var timeID = _win.setTimeout(function() {

				$.getJSON(INDI.base_url + 'principal/getubigeo/c/' + code, function(json) {

					if ( ! json.error ) {

						json.data.forEach(function(obj, index) {

							var params = { value: obj.CODIGO.trim() };

							if ( $cbo.data('sel') == obj.CODIGO.trim() ) {
								params.selected = true;
							}

							$cbo.append( $('<option />', params).text(obj.NOMBRE) );

						});

						if( json.data.length > 0 && _enabled ) $cbo.prop({disabled:false});

						if ( _exec_change ) $cbo.change();

					} else {
						$cbo.prop({selected:true});
					}

				}).complete(function() {
					$cbo.removeData('timeID');
					$cbo.removeClass('sending');
				});

			}, 500);

			$cbo.addClass('sending').data({timeID: timeID});

		}(code, $select));

	},

	_load_vias = function( cod_hurbana ) {

		var resource = INDI.base_url + 'individual/gethaburbanavia/haburbana/' + cod_hurbana;

		//$inputs.filter('input#txtcod_via').prop({disabled:true});

		$.getJSON(resource, function(json) {

			if ( ! json.error ) {

				if ( json.data.length > 0 ) {

					Vias_arr = json.data;
					events.emit('setVias', json.data);

					//$inputs.filter('input#txtcod_via').prop({disabled:false});

					if ( $inputs.filter('input#txtcod_via').val() != '' ) {
						$inputs.filter('input#txtcod_via').change();
					}

				} else {

					Vias_arr = [];
					events.emit('setVias', []);
					//$inputs.filter('input#txtcod_via').prop({disabled:true});

				}

			} else {
				Vias_arr = [];
				events.emit('setVias', []);
				//$inputs.filter('input#txtcod_via').prop({disabled:true});
			}

		});

	},

	_load_haburbanas = function() {

		var resource = (Request.Host + Request.BaseUrl) + '/mantenedor/haburbanas/show';

		//$inputs.filter('#txtcod_hurbana').prop({disabled:true});

		$.getJSON(resource, function(json) {

			if ( ! json.error ) {

				if ( json.data.length > 0 ) {

					HUrbanas_arr = json.data;
					//$inputs.filter('#txtcod_hurbana').prop({disabled:false});

					events.emit('setHaburbanas', json.data);

					if ($inputs.filter('#txtcod_hurbana').val() != '') {
						$inputs.filter('#txtcod_hurbana').change();
					}

				} else {

					events.emit('setHaburbanas', []);
					//$inputs.filter('#txtcod_hurbana').prop({disabled:true});

				}

			} else {

				events.emit('setHaburbanas', []);
				//$inputs.filter('#txtcod_hurbana').prop({disabled:true});

			}

		});

	},

	_reload_haburbanas = function (e) {

		if ( HUrbanas_arr.length > 0 ) {
			e.preventDefault();
			return false;
		}

		var resource = (Request.Host + Request.BaseUrl) + '/mantenedor/haburbanas/show';

		//$inputs.filter('#txtcod_hurbana').prop({disabled:true});

		$.getJSON(resource, function(json) {

			if ( ! json.error ) {

				if ( json.data.length > 0 ) {

					HUrbanas_arr = json.data;
					//$inputs.filter('#txtcod_hurbana').prop({disabled:false}).focus();

					events.emit('setHaburbanas', json.data);

				} else {

					events.emit('setHaburbanas', []);
					//$inputs.filter('#txtcod_hurbana').prop({disabled:true});

				}

			} else {

				events.emit('setHaburbanas', []);
				//$inputs.filter('#txtcod_hurbana').prop({disabled:true});

			}

		});

	},

	_load_combos = function() {

		var $selects = $inputs.filter('select.autoload');

		$.each($selects, function(index, el) {

			var $el 			= $(el),
					type 			= $el.data('type'),
					sw 				= $el.data('sw'),
					resource 	= 'principal/getcombo/type/'+type+'/sw/'+sw;

			$el.children('option:not(.default)').remove();

			$.getJSON(INDI.base_url + resource, function(json, textStatus) {

				if ( ! json.error ) {

					json.data.forEach(function(obj, pos) {

						var params = { value: obj.CODIGO.trim() };

						if ( $.trim($el.data('sel')) == params.value ) {
							params.selected = true;
						}

						$el.append( $('<option />', params).text(obj.NOMBRE) );

					});

					if ( $el.data('exechange') )
						$el.change();

				} else {

					$el.attr({title:'No se encontraron datos'}).prop({disabled:true});

				}

			});

		});

	},

	_active_controls_tipo_persona = function() {

		if ( parseInt(this.value.trim()) == 1 ) {

			$md_mng.find('label#lblnro_documento').text('N° Documento');
			$md_mng.find('label#lblnombre').text('Nombres');

			$inputs.filter('select#cbopersona_juridica').prop({disabled:true});
			$inputs.filter('select#cboestado_civil, select#cbotipo_documento').prop({disabled:false});

			if ( action == 'create' ) {
				($inputs.filter('select#cbopersona_juridica, select#cbotipo_documento'))
					.children('option.default').prop({selected: true});
			}

			$inputs.filter('select#cbotipo_documento').data({sel:'02'}).change();

		} else if( parseInt(this.value.trim()) == 2 ) {

			$md_mng.find('label#lblnro_documento').text('N° RUC');
			$md_mng.find('label#lblnombre').text('Razón social');

			$inputs.filter('select#cbopersona_juridica').prop({disabled: false});
			$inputs.filter('select#cboestado_civil, select#cbotipo_documento').prop({disabled: true});

			if ( action == 'create' ) {
				($inputs.filter('select#cboestado_civil'))
					.children('option.default').prop({selected: true});

				($inputs.filter('select#cbotipo_documento'))
					.children('option:contains("REGISTRO UNICO DE CONTRIBUYENTE")').prop({selected: true});
			}

			$inputs.filter('select#cbotipo_documento').data({sel:'00'}).change();

		}

		if ( action == 'create' ) {
			$inputs.filter('#txtnro_documento,#txtnombre').val('');
		}

		action = action || 'create';

		events.emit('set_tipo_titular', parseInt(this.value.trim()));

	},

	_action_buttons = function(value, row, index) {

    return [
			'<button id="btn-edit-'+row.codigo+'" class="btn-actions edit btn btn-sm" data-id="'+row.codigo+'" tabindex="-1">',
				'<i class="fa fa-pencil" aria-hidden="true"></i>',
			'</button>',
			'<button id="btn-delete-'+row.codigo+'" class="btn-actions delete btn btn-sm" data-id="'+row.codigo+'" tabindex="-1">',
				'<i class="fa fa-trash-o" aria-hidden="true"></i>',
			'</button>'
		].join('');

  },

	_build_table = function() {

  	var bootTable = {
		  locale: 'es-SP',
		  idField: "id",
		  url: INDI.base_url + 'principal/gettable/type/12/rel/' + ID_Ficha,
		  onPostHeader: function(e){
				$table.find('tr th').removeAttr('tabindex');
		  }
		};

  	//TABLE HEADER
		bootTable.columns = [
			{
				field: 'action',
				title: '<i class="fa fa-cog" aria-hidden="true"></i>',
				align: 'center',
				formatter : _action_buttons,
				events : {
					'click .edit': _open_md_edit,
					'click .delete': _delete_row
	    	}
	    },
			{ field: 'id', title: 'ID', align: 'center', sortable:true, visible:false },
			{ field: 'codigo', title: 'Codigo', align: 'center', sortable:true},
			{ field: 'Tipo_Doc', title: 'Tipo de Doc.', align: 'center', sortable:true },
			{ field: 'NRO_DOC', title: 'N° Documento', align: 'center', sortable:true },
			{ field: 'TITULAR', title: 'Titular', align: 'left', sortable:true },
			{ field: 'ID_UBI_GEO', title: 'Ubigeo', align: 'center', sortable:true },
			{ field: 'TELEFONO', title: 'Telefono', align: 'center', sortable:true },
			{ field: 'mail', title: 'Email', align: 'left', sortable:true },
			{ field: 'porcentaje', title: '% Pertenencia', align: 'center', sortable:true }
		];

		$table.bootstrapTable(bootTable);

  },

  _select_row_haburbana = function(row) {

		var $txtcode = $inputs.filter('input#txtcod_hurbana'),
				$txtname = $inputs.filter('input#txtnombre_hurbana'),
				name = $txtcode.attr('name'),
				send = {};

		$txtcode.val(row.code).removeAttr('style');
		$txtname.val(row.name + ' - ' + row.zoni);

		datasend.hurbana = row.code;

		setFocus($inputs.filter('#txtmanzana'));

	},

	_select_row_via = function(row) {

		var $txtcode = $inputs.filter('input#txtcod_via'),
				$txtname = $inputs.filter('input#txtnombre_via'),
				name = $txtcode.attr('name'),
				send = {};

		$txtcode.val(row.Cod_via).removeAttr('style');
		$txtname.val(row.via);

		datasend.via = row.Cod_via;

		setFocus($inputs.filter('#txtnro_municipalidad'));

	};

	return {
		init:_bind
	};

}(window, window.jQuery));