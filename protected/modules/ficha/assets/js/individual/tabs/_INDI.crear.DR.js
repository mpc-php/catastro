// @koala-append "Documentos y Registros/_INDI.crear.DR.documentos.js"

/**
 * MODULO PARA DOCUMENTOS Y REGISTROS
 */
INDI.crear.DR = (function (_win, $) {
	
	'use strict';

	var ID_Ficha = null,
			COD_Catastro = null;

	var data_notaria = {},
			default_notaria = {
				id: '0',
				name: '',
				ubigeo: '000000'
			};

	var $cnt_dr = $('div#tab_docregistro'),
			$inputs = $cnt_dr.find('input, select'),

			$md_notarias 			= $cnt_dr.find('section#md-notaria'),
			$btn_new 					= $md_notarias.find('button#btn-new'),
			$btn_done 				= $md_notarias.find('button#btn-done'),
			$dt_notarias 			= $md_notarias.find('table#dt-notarias'),
			$bb_notarias 			= {},

			$md_mng_notarias 	= $cnt_dr.find('section#md-mng-notarias'),
			$btn_mng_done 		= $md_mng_notarias.find('button#btn-done'),
			$bb_mng_notarias 	= {};

	//CONSTRUCTOR
	var _bind = function (ficha, catastro) {

		ID_Ficha 			= ficha,
		COD_Catastro 	= catastro;

		_load_combos();

		$inputs.filter('.maskdate').inputmask();
		$inputs.filter('.datasend').on('change.INDI.crear.DR.module', _watcher_general);
		$cnt_dr.find('#btn-open-notarias').on('click.INDI.crear.DR.module', _open_md_index_notaria);
		$inputs.filter('#txtfecha_inscfabrica').on('keydown.INDI.crear.DR.module', _open_nex_tab);

		INDI.crear.DR.documentos.init(ID_Ficha, COD_Catastro, $cnt_dr);

		events.on('refreshComboNotarias', _load_notarias);

		_load_notarias();

	},

	_watcher_general = function() {

		var $this = $(this),
			name 		= $this.attr('name'),
			type 		= $this.attr('type'),
			tag 		= $this.prop("tagName").toLowerCase(),
			data 		= {};

		if ( tag == 'input' && type == 'text' ) {
			
			var value = this.value.trim();

			if ( $this.hasClass('maskdate') ) {
				if ( ! Inputmask.isValid(value, { alias: "dd/mm/yyyy"}) ) {
					value = '';
					$this.val(value);
				} else {
					$this.removeAttr('style');
				}
			}

			data[name] = value;

		} else if ( tag == 'select' ) {
			data[name] = $this.children('option:selected').val();
		}

		events.emit('savedata', data);
		
	},

	_open_nex_tab = function(e) {

		if ( e.key == "Tab" && e.keyCode == 9 ) {
			$('#tabs-control').find('a[href="#tab_infcomplementaria"]').tab('show');
			e.preventDefault();
			return false;
		}

	},

	_choise_option = function() {

		var selection = $dt_notarias.bootstrapTable('getSelections'),
				$cbo = $cnt_dr.find('#cbonotarias');

		$cbo.find('option[value="'+selection[0].ID_NOTARIA+'"]').prop({selected:true});

		$cbo.change();

		$bb_notarias.find('#btn-cancel').click();

		setFocus($inputs.filter('#txtkardex'));

	},

	_load_combos = function() {

		var $selects = $inputs.filter('select.autoload');

		$.each($selects, function(index, el) {

			var $el = $(el),
					type = $el.data('type'),
					sw = $el.data('sw'),
					resource = 'principal/getcombo/type/' + type + '/sw/' + sw;

			$.getJSON(INDI.base_url + resource, function(json, textStatus) {

				if ( ! json.error ) {

					json.data.forEach(function(obj, pos) {
						var params = {value:obj.CODIGO.trim()};

						if ( $el.data('sel') == params.value ) {
							params.selected = true;
						}

						$el.append( $('<option />', params).text(obj.NOMBRE) );
					});

				} else {

					$el.attr({title:'No se encontraron datos'}).prop({disabled:true});

				}

			});

		});
	
	},

	_load_ubigeos = function($cbo, $cod) {

		$cod = $cod || '0';

		$cbo.children('option:eq(0)').prop({ selected: true });

		$cbo.children('option:not(:eq(0))').remove();

		$.getJSON(INDI.base_url + 'principal/getubigeo/c/' + $cod, function(json) {

			if ( ! json.error ) {

				json.data.forEach(function(obj, index) {

					var params = { value: obj.CODIGO.trim() };

					if ( $cbo.data('sel') == obj.CODIGO.trim() ) {
						params.selected = true;
					}

					$cbo.append( $('<option />', params).text(obj.NOMBRE) );

				});

				if( json.data.length > 0 )
					$cbo.prop({disabled:false});

			} else {
				$cbo.prop({selected:true});
			}
			
		});
	
	},

	_open_md_index_notaria = function() {

		$bb_notarias = $md_notarias.cBootbox({
			title: "Notarías",
			btnClose: '#btn-cancel',
			beforeOpen: function() {

				_load_table_notarias();

				$btn_done.prop({disabled:false});
				$btn_new.prop({disabled:false});

				$md_notarias.find('select').on('keypress', select_keyPress);

				$btn_done.on('click.INDI.crear.DR.module', _choise_option);
				$btn_new.on('click.INDI.crear.DR.module', _open_md_new_notaria);

				$md_notarias.find('.search input').focus();

			},
			afterClose: function() {
				$dt_notarias.bootstrapTable('destroy');
			}
		});

	},

	_open_md_new_notaria = function() {

		$bb_mng_notarias = $md_mng_notarias.cBootbox({
			title: "Nueva notaría",
			btnClose: '#btn-close',
			size:'small',
			beforeOpen: function() {

				var cbodep = $md_mng_notarias.find('select#cbodepartamento'),
						cbopro = $md_mng_notarias.find('select#cboprovincia'),
						cbodis = $md_mng_notarias.find('select#cbodistrito');

				_load_ubigeos(cbodep, '0');

				_assign_events_to_modal_manage();

				$btn_mng_done.prop({disabled:false});

			},
			afterClose: _close_modal_manage_notaria
		});

	},

	_open_md_edit_notaria = function(e, value, row, index) {
		$bb_mng_notarias = $md_mng_notarias.cBootbox({
			title: "Editar notaría",
			btnClose: '#btn-close',
			size:'small',
			beforeOpen: function() {

				var $cbodep = $md_mng_notarias.find('select#cbodepartamento'),
						$cbopro = $md_mng_notarias.find('select#cboprovincia'),
						$cbodis = $md_mng_notarias.find('select#cbodistrito');

				data_notaria.id = row.ID_NOTARIA;
				data_notaria.name = row.NOM_NOTARIA;
				data_notaria.ubigeo = row.ID_UBI_GEO;

				$md_mng_notarias.find('input[type=text]#txtname').val(row.NOM_NOTARIA);

				var dep = row.ID_UBI_GEO.substr(0, 2),
						pro = row.ID_UBI_GEO.substr(0, 4),
						dis = row.ID_UBI_GEO.substr(0, 6);

				$cbodep.data({sel:dep});
				$cbopro.data({sel:pro});
				$cbodis.data({sel:dis});

				_load_ubigeos($cbodep, '0');
				_load_ubigeos($cbopro, dep);
				_load_ubigeos($cbodis, pro);

				_assign_events_to_modal_manage();

				$btn_mng_done.prop({disabled:false});

			},
			afterClose: _close_modal_manage_notaria
		});
	},

	_assign_events_to_modal_manage = function() {

		$md_mng_notarias.find('select').on('keypress', select_keyPress);

		$md_mng_notarias.find('select#cbodepartamento').on('change.INDI.crear.DR.module', _choise_departamento);
		$md_mng_notarias.find('select#cboprovincia').on('change.INDI.crear.DR.module', _choise_provincia);

		$md_mng_notarias.find('.datasend_notaria').on('change.INDI.crear.DR.module', _watcher_inputs_md_manage_notarias);
		$btn_mng_done.on('click.INDI.crear.DR.module', _submit_manage_notaria);

	},

	_close_modal_manage_notaria = function() {

		$md_mng_notarias.find('input[type=text]#txtname').val('');

		$bb_mng_notarias.find('select#cbodepartamento').children('option:not(:eq(0))').remove();
		$bb_mng_notarias.find('select#cboprovincia').children('option:not(:eq(0))').remove();
		$bb_mng_notarias.find('select#cbodistrito').children('option:not(:eq(0))').remove();

		$bb_mng_notarias.find('select#cbodepartamento').children('option:eq(0)').prop({selected:true});
		$bb_mng_notarias.find('select#cboprovincia').children('option:eq(0)').prop({selected:true});
		$bb_mng_notarias.find('select#cbodistrito').children('option:eq(0)').prop({selected:true});

		$btn_mng_done.prop({ disabled: true });

		data_notaria = {};

	},

	_choise_departamento = function() {

		var $this = $(this),
				cbopro = $md_mng_notarias.find('select#cboprovincia');

		cbopro.prop({disabled:true});

		_load_ubigeos(cbopro, $this.children('option:selected').val());

		var dist = $md_mng_notarias.find('select#cbodistrito');

		dist.children('option:not(:eq(0))').remove();
		dist.prop({disabled:true});

	},

	_choise_provincia = function() {

		var $this = $(this),
				cbodis = $md_mng_notarias.find('select#cbodistrito');

		cbodis.prop({disabled:true});

		_load_ubigeos(cbodis, $this.children('option:selected').val());

	},

	_submit_manage_notaria = function() {

		data_notaria = $.extend({}, default_notaria, data_notaria);
		
		if ( data_notaria.name.length == 0 ) {
			$.bootstrapGrowl("Debes asignar un nombre.", INDI.growl('warning'));
			$md_mng_notarias.find('input[type=text]#txtname').focus();
			return false;
		}

		if ( data_notaria.ubigeo.length == 0 || data_notaria.ubigeo == '000000' ) {
			$.bootstrapGrowl("El ubigeo no está completo.", INDI.growl('warning'));
			$md_mng_notarias.find('select#cbodepartamento').focus();
			return false;
		}

		$.post(INDI.base_url + 'individual/mantenimientonotaria', data_notaria, function(response) {

			if ( ! response.error ) {

				$.bootstrapGrowl(response.message, INDI.growl('success'));

				$dt_notarias.bootstrapTable('refresh');

				_load_notarias();

				$bb_mng_notarias.find('#btn-close').click();

			} else {

				$btn_mng_done.prop({disabled:false});
				$.bootstrapGrowl(response.message, INDI.growl('danger'));
				$md_mng_notarias.find('input[type=text]#txtname').focus();

			}

		});

	},

	_load_notarias = function() {

		var $cbo = $inputs.filter('#cbonotarias');

		$cbo.find('option.default').prop({selected:true});
		$cbo.find('option:not(.default)').remove();

		$.getJSON(INDI.base_url + 'individual/getnotarias', function(json) {
			
			if ( ! json.error ) {

				var data = json.data;

				if ( data.length > 0 ) {

					data.forEach( function(obj, index) {

						var params = {value:obj.ID_NOTARIA.trim()};

						if ( $cbo.data('sel') == params.value ) {
							params.selected = true;
						}

						$cbo.append($('<option />', params).text(obj.NOM_NOTARIA));

					});

				}
			}

		});
	},

	_watcher_inputs_md_manage_notarias = function() {
		var $this = $(this),
			name = $this.attr('name'),
			type = $this.attr('type'),
			tag = $this.prop("tagName").toLowerCase();

		if ( tag == 'input' && type == 'text' ) {
			data_notaria[name] = $.trim($this.val());
		} else if ( tag == 'select' ) {
			data_notaria[name] = $this.children('option:selected').val();
		}
	},

	_btns_dtnotarias = function(value, row, index) {
    return [
			'<button id="btn-edit-'+row.ID_NOTARIA+'" class="btn-actions edit btn btn-sm" data-id="'+row.ID_NOTARIA+'" data-accion="edit">',
				'<i class="fa fa-pencil"></i>',
			'</button>'
		].join('');
  },

	_load_table_notarias = function() {

		var bootTable = {
			escape: true,
		  locale: 'es-SP',
		  search: true,
		  searchAlign:'left',
		  selectItemName: 'inotaria',
		  height: 400,
		  clickToSelect: true,
		  idField: "ID_NOTARIA",
		  url: INDI.base_url + 'individual/getnotarias',
		  onDblClickRow: _choise_option,
		  onPostHeader: function(){
		  	$dt_notarias.find('tr th').removeAttr('tabindex');
		  	_win.setTimeout(function(){
		  		($bb_notarias.find('.search')).find('input').focus();
		  	}, 500);
		  }
		};

  	//TABLE HEADER
		bootTable.columns = [
			{ 
				field: 'action', 
				title: '<i class="fa fa-asterisk" aria-hidden="true"></i>', 
				align: 'center', 
				formatter : _btns_dtnotarias, 
				events : { 'click .edit': _open_md_edit_notaria }
	    },
			{ field: 'state', radio: true, align: 'center' },
			{ field: 'ID_NOTARIA', title: 'ID', align: 'center', sortable:true },
			{ field: 'NOM_NOTARIA', title: 'NOTARIA', align: 'left', sortable:true }
		];

		$dt_notarias.bootstrapTable(bootTable);

	};

	return {
		init: _bind
	};

}(window, window.jQuery));

