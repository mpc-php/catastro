INDI.crear.OT.buscar = (function ( _win, $ ) {

	'use strict';

	var $curr_element;

	var $md_mng = $('section#md-buscar'),
			$table = $md_mng.find('table#dt-buscar'),
			$bb_mng = {};

	var _bind = function() {

	},

	_open_modal = function(curr_element) {

		$curr_element = curr_element;

		var data = $curr_element.data();

		$bb_mng = $md_mng.cBootbox({
			title: "Buscar persona",
			btnClose: '#btn-cancel',
			afterClose: _afterClose_modal,
			beforeOpen: function() {

				_build_table(data.rel, data.u, $.trim($curr_element.val()) );

				$md_mng.find('button#btn-done, button#btn-new').prop({disabled:false});

				$md_mng.find('button#btn-done').on('click.INDI.crear.OT.buscar.module', _click_selection);

			}

		});

	},

	_dblClick_selection = function(row, $element, field) {

		row.target_elem = $curr_element

		events.emit('getRowSelected', row);

		$bb_mng.find('button#btn-cancel').click();

	},

	_click_selection = function() {

		var selection = $table.bootstrapTable('getSelections'),
				row = selection[0];

		row.target_elem = $curr_element;

		events.emit('getRowSelected', row);

		$bb_mng.find('button#btn-cancel').click();
		
	},

	_actions_postHeader = function() {

		_win.setTimeout(function(){
			($bb_mng.find('.search')).children('input').focus().select();
		}, 500);
			
		$table.find('thead tr th').removeAttr('tabindex');
		
	},

	_afterClose_modal = function() {
		$table.bootstrapTable('destroy');
	},

	_build_table = function(rel, cargo, searchText) {

		var bootTable = {
		  locale: 'es-SP',
		  search: true,
		  sortable:false,
		  selectItemName: 'personas',
		  height: 400,
		  clickToSelect: true,
		  sidePagination: 'server',
		  pagination: true,
		  idField: "ID_PERSONA",
		  searchText: searchText,
		  onDblClickRow: _dblClick_selection,
		  onPostHeader: _actions_postHeader
		};

		bootTable.url = INDI.base_url + 'principal/searchpersona/rel/'+rel+'/cargo/'+cargo+'/type/1';

  	//TABLE HEADER
		bootTable.columns = [
			{ field: 'state', radio: true, align: 'center' },
			{ field: 'CODIGO', title: 'CODIGO', align: 'center' },
			{ field: 'ID_PERSONA', title: 'ID PERSONA', align: 'left' },
			{ field: 'NOMBRES', title: 'NOMBRE DE PERSONA', align: 'left' }
		];

		$table.bootstrapTable(bootTable);

	};

	return {
		init: _bind,
		show: _open_modal
	};

}(window, window.jQuery));