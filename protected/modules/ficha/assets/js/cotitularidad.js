// @koala-prepend "_helpers/_settings.js"
// @koala-prepend "_helpers/_base.js"

// @koala-prepend "_modules/_observaciones.js"

// @koala-prepend "cotitularidad/_COTI.index.js"
// @koala-prepend "cotitularidad/_COTI.crear.js"

$(function() {

	var $page = $('div#page');

	switch($page.data('module')) {
		case 'index':
			COTI.index.init();
			break;
		case 'create':
		case 'update':
			COTI.crear.init();
			break;
	}
	
});