( function( _win, $, exports ) {

	'use strict';

	var tipoFicha,
			modulesName = {
				'01': 'individual',
				'02': 'cotitularidad',
				'03': 'acteconomica',
				'04': 'biencomun'
			};

	var ID_Ficha,
			nro_ficha,
			cod_catastro;

	var datasend = {};

	var $btn_add 	= $('button#btn-observation'),
			$md_mng 	= $('section#md-observation'),
			$table 		= $md_mng.find('table#td-campos'),
			$td_desc 	= $md_mng.find('#td-description'),
			$bb_mng 	= {};

	exports.init = function( _ficha_, _tipo_ ) {

		ID_Ficha 	= _ficha_;
		tipoFicha = _tipo_;
		nro_ficha = $('input[type=text]#txtnro_ficha').val();

		$td_desc.find('#lblficha').html(nro_ficha);
		$td_desc.find('#lblidficha').html(ID_Ficha);

		_asign_cod_ref_catastral();

		$btn_add.on('click.observacion.module', _open_modal);

	};

	var _submit_data = function() {

		var $this = $(this),
				resource = settings.moduleUrl + modulesName[tipoFicha] + '/generarobservacion';

		if ( ID_Ficha == null ) {
			$.bootstrapGrowl("La ficha no está asignado, el proceso a sido cancelado", settings.growl('warning'));
			return false;
		} else {
			datasend.ficha = ID_Ficha;
			datasend.nro_ficha = nro_ficha;
			datasend.cod_ref_catastral = cod_catastro;
			datasend.tipo_ficha = tipoFicha;
		}

		if ( $.trim($md_mng.find('#txtobservaciones').val()) == '' ) {

			$.bootstrapGrowl("Este campo es requerido", settings.growl('warning'));

			$md_mng.find('#txtobservaciones').focus();

			return false;

		} else {
			datasend.observacion = $.trim($md_mng.find('#txtobservaciones').val());
		}

		_get_fields_required();

		$this.prop({disabled:true});

		$.post(resource, datasend, function(response) {

			if ( ! response.error ) {

				$.bootstrapGrowl(response.message, settings.growl('warning'));

				_win.setTimeout(function() {

					_win.location.reload(true);

				}, 3000);

			} else {
				$.bootstrapGrowl(response.message, settings.growl('warning'));
				$this.prop({disabled:false});
			}
			
		});
		

	},

	_get_fields_required = function() {

		var $checkboxs = $table.find('input[type=checkbox]:checked');

		datasend.fields = [];

		$.each($checkboxs, function(index, element) {

			var $input = $table.find( '#txt' + element.name );

			datasend.fields.push({
				id_campo: element.value,
				observacion: $.trim($input.val())
			});
			
		});

	},

	_asign_cod_ref_catastral = function() {

		var $tdcat = $('table#td_codrefcatastro'),
				tdinputs = $tdcat.find('input[type=text]'),
				str = '';

		$.each(tdinputs, function(index, element) {
			str += this.value.trim();
		});

		cod_catastro = str;
		$td_desc.find('#lblcodref_catastral').html(str);

	},

	_open_modal = function() {

		$bb_mng = $md_mng.cBootbox({
			title: "Observaciones en ficha generada",
			btnClose: '#btn-close',
			afterClose: _afterClose_modal,
			beforeOpen: function() {

				var resource = settings.moduleUrl +  'principal/getfieldrequired',
						params = { type: tipoFicha };

				$.getJSON(resource, params, function(json) {

					if ( ! json.error ) {

						var $tbody = $('<tbody />');

						$table.find('tbody').remove();

						json.data.forEach( function(obj, index) {
							$tbody.append(_get_row(obj));
						});

						$md_mng.find('#btn-done').prop({disabled:false});

						$tbody.find('input[type=checkbox]')
							.on('change.observacion.module', _active_description);

						$table.append($tbody);

						$md_mng.find('#btn-done').on('click.observacion.module', _submit_data);

					}

				});

				_win.setTimeout(function(){
					$bb_mng.find('#txtobservaciones').focus();
				}, 500);

			}

		});

	},

	_afterClose_modal = function() {

		$md_mng.find('#btn-done').prop({disabled:true});

		$table.find('tbody tr').remove();

		$table.find('tbody').append(
			[
				'<tr>',
					'<td colspan="2" style="text-align:center;">',
						'Cargando campos requeridos de la ficha...',
					'</td>',
				'</tr>'
			].join('')
		);

	},

	_active_description = function() {

		var $txt = $table.find( ('#txt' + this.name) );

		$txt.prop({disabled:!this.checked}).val('').focus();

	},

	_get_row = function(DATA) {

		return $([
			'<tr>',
				'<td>',
					'<div class="checkbox">',
						'<label>',
							'<input type="checkbox" id="chk_'+DATA.field+'" name="' + DATA.field + '" value="'+DATA.id+'" >',
							DATA.title,
						'</label>',
					'</div>',
				'</td>',
				'<td>',
					'<input type="text" id="txt' + DATA.field + '" data-id="' + DATA.id + '" class="form-control" disabled>',
				'</td>',
			'</tr>',
		].join(''));

	};

}( window, window.jQuery, this.Observaciones = {} ) );