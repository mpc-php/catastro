// @koala-prepend "_helpers/_base.js"
// @koala-prepend "_helpers/_settings.js"

// @koala-prepend "_modules/_observaciones.js"

// @koala-prepend "biencomun/_BICO.index.js"
// @koala-prepend "biencomun/_BICO.crear.js"

$(function() {

	var $page = $('div#page');

	switch($page.data('module')) {
		case 'index':
			BICO.index.init();
			break;
		case 'create':
		case 'update':
			BICO.crear.init();
			break;
	}
	
});