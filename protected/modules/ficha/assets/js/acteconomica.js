// @koala-prepend "_helpers/_settings.js"
// @koala-prepend "_helpers/_base.js"

// @koala-prepend "_modules/_observaciones.js"

// @koala-prepend "acteconomica/_ACTE.index.js"
// @koala-prepend "acteconomica/_ACTE.crear.js"

$(function() { 

	var $page = $('div#page');

	switch($page.data('module')) {
		case 'index':
			ACTE.index.init();
			break;
		case 'create':
		case 'update':
			ACTE.crear.init();
			break;
	}
	
});