// @koala-append "Obras y Construcciones/_BICO.crear.OC.obras.js"
// @koala-append "Obras y Construcciones/_BICO.crear.OC.construcciones.js"
// @koala-append "Obras y Construcciones/_BICO.crear.OC.instalaciones.js"
// @koala-append "Obras y Construcciones/_BICO.crear.OC.instalaciones.agregar.js"

/**
 * MODULO PARA OBRAS/CONSTRUCCIÓN
 */
BICO.crear.OC = (function (_win, $) {
	
	'use strict';

	var ID_Ficha 			= null,
			COD_Catastro 	= null;

	var $cnt_oc 			= $('div#tab_obrasconstr'),
			$inputs 			= $cnt_oc.find('input, select');

	//CONSTRUCTOR
	var _bind = function (ficha, catastro) {

		ID_Ficha 			= ficha,
		COD_Catastro 	= catastro;

		$inputs.filter('input[type=text].only-number').on('keydown.BICO.crear.OC.module', only_number);
		$inputs.filter('input[type=text].only-number').on('change.BICO.crear.OC.module', verify_number);

		$inputs.filter('.datasend').on('change.BICO.crear.OC.module', _watcher_general);
		$cnt_oc.find('#add-obras').on('keydown.BICO.crear.OC.module', _open_nex_tab);

		BICO.crear.OC.construcciones.init(ID_Ficha, COD_Catastro, $cnt_oc);
		BICO.crear.OC.obras.init(ID_Ficha, COD_Catastro, $cnt_oc);

	},

	_watcher_general = function() {
		
		var $this = $(this),
				name = $this.attr('name'),
				data = {};

		data[name] = $.trim($this.val())
		
		events.emit('savedata', data);

	},

	_open_nex_tab = function(e) {

		if ( e.key == "Tab" && e.keyCode == 9 ) {
			$('#tabs-control').find('a[href="#tab_docregistro"]').tab('show');
			e.preventDefault();
			return false;
		}

	};

	return {
		init: _bind
	};

}(window, window.jQuery));