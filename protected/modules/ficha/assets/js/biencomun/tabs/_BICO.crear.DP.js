/**
 * MODULO PARA DESCRIPCION DEL PREDIO
 */
BICO.crear.DP = (function (_win, $) {
	
	'use strict';

	var ID_Ficha 			= null,
			COD_Catastro 	= null,
			SECTOR 				= null;

	var $cnt_dp 			= $('div#tab_descpredio'),
			$inputs 			= $cnt_dp.find('input, select');

	//CONSTRUCTOR
	var _bind = function (ficha, catastro, _sector) {

		ID_Ficha 			= ficha,
		COD_Catastro 	= catastro;
		SECTOR 				= _sector;

		_load_combos();
		_load_zonificacion();

		//$inputs.filter('input[type=text].in-table').on('keydown.BICO.crear.DP.module', _change_control);
		$inputs.filter('input[type=text].in-table').on('keyup.BICO.crear.DP.module', _change_control);
		$inputs.filter('input[type=text].only-number').on('keydown.BICO.crear.DP.module', only_number);
		$inputs.filter('input[type=text].only-number').on('change.BICO.crear.DP.module', verify_number);
		$inputs.filter('input.calctotaluc').on('keyup.BICO.crear.DP.module', _calcule_total_UC);
		$inputs.filter('.datasend').on('change.BICO.crear.DP.module', _watcher_general);
		$cnt_dp.find('#chkbdesague').on('keydown.BICO.crear.DP.module', _open_nex_tab);
		$inputs.filter('input#txtcond_uso').on('change.BICO.crear.DP.module', _search_in_cbo);
		$inputs.filter('#cbouso_predio').on('change.BICO.crear.DP.module', _copy_value_to_input_search);
		$inputs.filter('input[type=checkbox].activated').on('change.BICO.crear.DP.module', _activated_control);
		$inputs.filter('input[type=checkbox].activated').change();

	},

	_watcher_general = function() {

		var $this 	= $(this),
				name 		= $this.attr('name'),
				type 		= $this.attr('type'),
				tag 		= $this.prop("tagName").toLowerCase(),
				data 		= {};

		if ( tag == 'input' && type == 'text' ) {

			data[name] = $.trim($this.val());

		} if ( tag == 'input' && type == 'checkbox' ) {

			data[name] = $this.is(':checked') ? '1' : '0';

		} else if ( tag == 'select' ) {

			data[name] = $this.children('option:selected').val();

		}

		events.emit('savedata', data);
		
	},

	_open_nex_tab = function(e) {

		if ( e.key == "Tab" && e.keyCode == 9 ) {
			$('#tabs-control').find('a[href="#tab_obrasconstr"]').tab('show');
			e.preventDefault();
			return false;
		}

	},

	_change_control = function(e) {

		var $this = $(this),
				dataKeys = $this.data();

		if ( e.shiftKey ) {
			switch (e.which) {
				case 37:
					if ( dataKeys.left != 'undefined' ) $inputs.filter('#'+dataKeys.left).focus();
					break;
				case 38:
					if ( dataKeys.up != 'undefined' ) $inputs.filter('#'+dataKeys.up).focus();
					break;
				case 40:
					if ( dataKeys.down != 'undefined' ) $inputs.filter('#'+dataKeys.down).focus();
					break;
				case 39:
					if ( dataKeys.right != 'undefined' ) $inputs.filter('#'+dataKeys.right).focus();
					break;
			}

			if ( $.inArray(e.which, [37, 38, 40, 39]) ) {
				e.stopImmediatePropagation();
			}
		}

	},


	_load_combos = function() {

		var $selects = $inputs.filter('select.autoload');

		$.each($selects, function(index, el) {

			var $el 			= $(el),
					type 			= $el.data('type'),
					sw 				= $el.data('sw'),
					resource 	= 'principal/getcombo/type/'+type+'/sw/'+sw;

			$el.prop({disabled:true});

			$.getJSON(BICO.base_url + resource, function(json) {

				if ( ! json.error ) {

					json.data.forEach(function(obj, pos) {

						var params = {value:obj.CODIGO.trim()};

						if ( $el.data('sel') == params.value ) {
							params.selected = true;
						}

						$el.append( $('<option />', params).text(obj.NOMBRE) );

					});

					$el.prop({disabled:false});

				} else {
					$el.attr({title:'No se encontraron datos'}).prop({disabled:true});
				}

			});

		});

	},

	_load_zonificacion = function() {

		var $cbo 			= $inputs.filter('select#cbozonificacion'),
				resource 	= BICO.base_url + 'biencomun/getsectorzona/sector/'+SECTOR+'/pertenece/1';

		$cbo.prop({disabled:true});

		$.getJSON(resource, function(json) {
			
			if ( ! json.error ) {

				json.data.forEach(function(obj, pos) {

					var params = {value:obj.Zonificacion, 'data-vinc': obj.IdSectorZona};

						if ( $cbo.data('sel') == params.value ) {
							params.selected = true;
						}

					$cbo.append( $('<option />', params).text(obj.Zonificacion + ' | ' + obj.Descrip_zona ) );

				});

				$cbo.prop({disabled:false});

			} else {
				$cbo.prop({disabled:true});
			}

		});

	},

	_search_in_cbo = function() {

		var $this = $(this),
				$cbo 	= $inputs.filter('#cbouso_predio'),
				$item = $cbo.children('option[value="' + this.value.trim() + '"]');

		if ( $item.length == 0 ) {

			$this.val('').css({borderColor:'red'});
			$cbo.children('option:eq(0)').prop({selected:true});

		} else {

			$this.removeAttr('style');
			$cbo.children('option:eq(0)').removeAttr('selected');
			$item.prop({selected:true});

		}

		$cbo.change();

	},

	_copy_value_to_input_search = function () {
		var $this = $(this);
		$inputs.filter('input#txtcond_uso').val($this.children('option:selected').val());
	},

	_activated_control = function () {

		var $this = $(this),
				$item = $inputs.filter('input[type=text]'+$this.data('activate'));

		if ( $this.is(':checked') ) {
			$item.prop({disabled:false}).focus();
		} else {
			$item.prop({disabled:true}).val('').change();
		}

	},

	_calcule_total_UC = function() {

		var $this 		= $(this),
				$bases 		= $inputs.filter('input.calctotaluc'),
				$elem 		= $inputs.filter('input#txttotal_limpfrenteuc'),
				currValue = 0;

		$.each($bases, function(index, elem) {
			var val 	= (elem.value.trim() == '') ? 0 : elem.value.trim();
			currValue += parseFloat(val);
		});

		$elem.val(currValue);

		_win.setTimeout(function(){ $elem.change(); }, 300);
		
	};

	return {
		init: _bind
	};

}(window, window.jQuery));