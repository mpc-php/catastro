// @koala-append "Recapitulacion de bienes/_BICO.crear.DR.recapitular.js"
// @koala-append "Recapitulacion de bienes/_BICO.crear.DR.recapitularEdificios.js"
// @koala-append "Recapitulacion de bienes/_BICO.crear.DR.notarias.js"
// @koala-append "Recapitulacion de bienes/_BICO.crear.DR.notarias.manage.js"

/**
 * MODULO PARA DOCUMENTOS Y REGISTROS
 */
BICO.crear.DR = (function (_win, $) {

	'use strict';

	var ID_Ficha = null,
			COD_Catastro = null;

	var $cnt_dr = $('div#tab_docregistro'),
			$inputs = $cnt_dr.find('input, select');

	//CONSTRUCTOR
	var _bind = function (_ficha_, _catastro_) {

		ID_Ficha 			= _ficha_,
		COD_Catastro 	= _catastro_;

		_load_combos();

		$inputs.filter('.maskdate').inputmask();
		$inputs.filter('.datasend').on('change.BICO.crear.DR.module', _watcher_general);
		$cnt_dr.find('#txtfecha_inscfabrica').on('keydown.BICO.crear.DR.module', _open_nex_tab);

		_load_notarias();

		// VERIFICAR SI LA FICHA SE PUEDE RECAPITULAR LAS EDIFICACIONES
		// LA CONDICION ES QUE, SE PUEDE RECAPITULAR EDIFICIOS SOLO SI,
		// EL VALOR DEL CAMPO EDIFICIO EN EL CODIGO CATASTRAL ES 99
		var edifica = document.getElementById('txtedificio');

		if ( edifica.value != '99' ) {

			$cnt_dr.find('#rowRecapitulacionEdificios').remove();

			BICO.crear.DR.recapitular.init(
				ID_Ficha,
				COD_Catastro,
				$cnt_dr.find('button#btn-add-recapitulacion'),
				$cnt_dr.find('table#td-recapitulacion')
			);

		} else {

			$cnt_dr.find('#rowRecapitulacion').remove();

			BICO.crear.DR.recapitularEdificios.init(
				ID_Ficha,
				COD_Catastro,
				$cnt_dr.find('button#btn-add-recapedificios'),
				$cnt_dr.find('table#td-recapedificios')
			);

		}


		BICO.crear.DR.notarias.init(
			$cnt_dr.find('#btn-open-notarias'),
			$inputs.filter('#cbonotarias')
		);

		events.on('refreshComboNotarias', _load_notarias);

		events.on('choiseOption', _choise_option);

	},

	_watcher_general = function() {
		var $this = $(this),
			name = $this.attr('name'),
			type = $this.attr('type'),
			tag = $this.prop("tagName").toLowerCase(),
			data = {};

		if ( tag == 'input' && type == 'text' ) {

			var value = this.value.trim();

			if ( $this.hasClass('maskdate') ) {
				if ( ! Inputmask.isValid(value, { alias: "dd/mm/yyyy"}) ) {
					value = '';
					$this.val(value);
				} else {
					$this.removeAttr('style');
				}
			}

			data[name] = value;

		} else if ( tag == 'select' ) {
			data[name] = $this.children('option:selected').val();
		}

		events.emit('savedata', data);

	},

	_open_nex_tab = function(e) {

		if ( e.key == "Tab" && e.keyCode == 9 ) {
			$('#tabs-control').find('a[href="#tab_infcomplementaria"]').tab('show');
			e.preventDefault();
			return false;
		}

	},

	_choise_option = function(selection) {

		var $cbo = $inputs.filter('#cbonotarias');

		$cbo.find('option[value="'+selection[0].ID_NOTARIA+'"]').prop({selected:true});

		$cbo.change();

		setFocus($inputs.filter('#txtkardex'));

	},

	_load_notarias = function(conrt) {

		var $cbo = $inputs.filter('#cbonotarias');

		$cbo.find('option.default').prop({selected:true});
		$cbo.find('option:not(.default)').remove();

		$.getJSON(BICO.base_url + 'individual/getnotarias', function(json) {

			if ( ! json.error ) {

				var data = json.data;

				if ( data.length > 0 ) {

					data.forEach( function(obj, index) {

						var params = {value:obj.ID_NOTARIA.trim()};

						if ( $cbo.data('sel') == params.value ) {
							params.selected = true;
						}

						$cbo.append($('<option />', params).text(obj.NOM_NOTARIA));

					});

				}
			}

		});

	},

	_load_combos = function() {

		var $selects = $inputs.filter('select.autoload');

		$.each($selects, function(index, el) {

			var $el = $(el),
					type = $el.data('type'),
					sw = $el.data('sw'),
					resource = 'principal/getcombo/type/'+type+'/sw/'+sw;

			$.getJSON(BICO.base_url + resource, function(json, textStatus) {

				if ( ! json.error ) {

					json.data.forEach(function(obj, pos) {

						var params = {value:obj.CODIGO.trim()};

						if ( $el.data('sel') == params.value ) {
							params.selected = true;
						}

						$el.append( $('<option />', params).text(obj.NOMBRE) );
					});

				} else {

					$el.attr({title:'No se encontraron datos'}).prop({disabled:true});

				}

			});

		});

	};

	return {
		init: _bind
	};

}(window, window.jQuery));