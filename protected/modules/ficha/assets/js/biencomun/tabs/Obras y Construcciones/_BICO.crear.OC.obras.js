/**
 * SUBMODULO PARA OBRAS COMPLEMENTARIAS / OTRAS CONSTRUCCIONES
 */
BICO.crear.OC.obras = (function(_win, $) {

	'use strict';

	var ID_Ficha 			= null,
			COD_Catastro 	= null;

	var largo_,
			ancho_,
			alto_;

	var datasend = {},
			default_ = {
				ficha: '',
				id: 0,
				cod_instalacion: '',
				mes: '',
				anio: '',
				mep: '',
				ecs: '',
				ecc: '',
				largo: 0,
				ancho: 0,
				alto: 0,
				total: 0,
				uca: '',
				unidad: ''
			};

	var $container = $({}),
			$md_mng = $('section#md-obras'),
			$bb_mng = {},
			$table,
			$inputs,
			$btn_new;

	//Constructor
	var _bind = function(_ficha_, _catastro_, _container_) {

		ID_Ficha 			= _ficha_;
		COD_Catastro 	= _catastro_;
		$container 		= _container_;

		$table = $container.find('table#td-obras');
		$btn_new = $container.find('button#add-obras');
		$inputs = $md_mng.find('input, select');

		_build_table();

		$btn_new.on('click.BICO.crear.OC.obras.modules', _open_md_new);

		events.on('refreshComboInstalaciones', _refresh_combo_instalacion);

	},

	_watcher_change = function() {

		var $this = $(this),
			name = $this.attr('name'),
			type = $this.attr('type'),
			tag = $this.prop("tagName").toLowerCase();

		if ( tag == 'input' && type == 'text' ) {

			datasend[name] = $.trim($this.val());

		} else if ( tag == 'select' ) {

			datasend[name] = $this.children('option:selected').val();

		}

	},

	_refresh_combo_instalacion = function(confirm) {

		$bb_mng.find('#btn-close').click();

		/*var $el = $inputs.filter('select#cboinstalacion'),
				type = $el.data('type'),
				sw = $el.data('sw'),
				resource = 'principal/getcombo/type/'+type+'/sw/'+sw;

		$el.find('option.default').prop({selected:true});

		$el.find('option:not(.default)').remove();

		$.getJSON(BICO.base_url + resource, function( json ) {

			if ( ! json.error ) {

				json.data.forEach(function(obj, pos) {

					var params = { value: obj.CODIGO.trim() };

					if ( $el.data('sel') == params.value ) {
						params.selected = true;
					}

					$el.append( $('<option />', params).text(obj.NOMBRE) );

				});

			} else {
				$el.attr({title:'No se encontraron datos'}).prop({disabled:true});
			}

		});*/

	},

	_submit_data = function() {

		var $this = $(this);

		datasend = $.extend({}, default_, datasend);

		if ( ID_Ficha == null ) {
			$.bootstrapGrowl("La ficha no está asignado, el proceso a sido cancelado", BICO.growl('warning'));
			return false;
		} else {
			datasend.ficha = ID_Ficha;
		}

		if ( ! _is_valid_data() ) return false;

		$this.prop({disabled:true});

		$.post(BICO.base_url + 'individual/mantenimientoobras', datasend, function(response) {

			if ( ! response.error ) {

				$.bootstrapGrowl(response.message, BICO.growl('success'));
				
				$table.bootstrapTable('refresh');

				if ( datasend.action == 'create' ) {

					$inputs.filter('input').val('');

					($inputs.filter('select')).find('option.default').prop({selected:true});
					
					largo_ 	= false;
					ancho_ 	= false;
					alto_ 	= false;
					
					datasend = {};

					datasend.action = 'create';

					$inputs.filter('#txtcod_instalacion').focus();

				} else {

					$bb_mng.find('#btn-close').click();
				
				}

			} else {

				$.bootstrapGrowl(response.message, BICO.growl('danger'));

				$bb_mng.find('input#txtnro_piso').focus();

			}

			$this.prop({disabled:false});

		});
		
	},

	_open_md_new = function() {

		$bb_mng = $md_mng.cBootbox({
			title: "Agregar Obras complementarias",
			size:'large',
			btnClose: '#btn-close',
			beforeOpen: function() {

				_asigna_eventos();

				_load_combos();

				BICO.crear.OC.instalaciones.init(ID_Ficha, $md_mng.find('button#btn-add'));

				$md_mng.find('button#btn-done').prop({disabled:false});

				datasend.action = "create";

				_win.setTimeout(function(){
					$inputs.filter('#txtcod_instalacion').focus();
				}, 500);
				
			}
		});

	},

	_open_md_edit = function(e, value, row) {

		$bb_mng = $md_mng.cBootbox({
			title: "Agregar Obras complementarias",
			size:'large',
			btnClose: '#btn-close',
			afterClose: _afterClose_modal,
			beforeOpen: function() {

				var resource 	= BICO.base_url + 'individual/getinstalacion',
						params 		= {
							ficha: ID_Ficha,
							id: row.ID
						};

				$.getJSON(resource, params, function(json) {

					if ( ! json.error ) {

						if ( typeof json.data.cod_instalacion != 'undefined' ) {

							var data = json.data;

							$inputs.filter('#txtcod_instalacion').val(data.cod_instalacion);
							$inputs.filter('#txtmatpredo').val(data.mate_predominante);
							$inputs.filter('#txtmes').val(data.mes);
							$inputs.filter('#txtanio').val(data.anio);
							$inputs.filter('#txtunidad').val(data.unidad);
							$inputs.filter('#txtlargo').val(data.dim_largo);
							$inputs.filter('#txtancho').val(data.dim_ancho);
							$inputs.filter('#txtalto').val(data.dim_alto);
							$inputs.filter('#txttotal').val(data.total);
							$inputs.filter('#txtumedida').val(data.umedida);

							$md_mng.find('select#cboinstalacion').data({ sel: data.cod_instalacion });
							$md_mng.find('select#cbouca').data({ sel: data.uca.trim() });
							$md_mng.find('select#cbomep').data({ sel: data.mep.trim() });
							$md_mng.find('select#cboecs').data({ sel: data.ecs.trim() });
							$md_mng.find('select#cboecc').data({ sel: data.ecc.trim() });

							datasend = {
								id: data.id,
								cod_instalacion: data.cod_instalacion,
								mes: data.mes,
								anio: data.anio,
								largo: data.dim_largo,
								ancho: data.dim_ancho,
								alto: data.dim_alto,
								total: data.total,
								mep: data.mep,
								ecs: data.ecs,
								ecc: data.ecc,
								uca: data.uca,
								unidad: data.unidad
							};

							largo_ = data.largo;
							ancho_ = data.ancho;
							alto_ = data.alto;

							if ( data.umedida == 'UNIDAD' ) {
								$inputs.filter('#txtunidad').prop({disabled:false});
							} else {
								$inputs.filter('#txtunidad').prop({disabled:true});
							}


							_asigna_eventos();

							_load_combos();

							$md_mng.find('button#btn-done').prop({disabled:false});

							datasend.action = "update";

						} else {
							$.bootstrapGrowl('El recurso no ha sido encontrado', BICO.growl('danger'));
							$bb_mng.find('button#btn-close').click();
						}

					} else {
						$.bootstrapGrowl("El recurso no ha sido encontrado.", BICO.growl('danger'));
						$bb_mng.find('button#btn-close').click();
					}

				});

				BICO.crear.OC.instalaciones.init(ID_Ficha, $md_mng.find('button#btn-add'));

				_win.setTimeout(function(){
					$inputs.filter('#txtcod_instalacion').focus();
				}, 500);
				
			}

		});

	},

	_delete_row = function(e, value, row, index) {

		var $this = $(this),
				postParams = {},
				resource = BICO.base_url + 'individual/mantenimientoobras';

		bootbox.confirm({
			title: 'Seguro que quieres eliminar?',
			message: "La fila seleccionada será eliminada de forma permanente.",
			callback: function(result) {

				if ( result ) {

					if ( ID_Ficha == null ) {
						$.bootstrapGrowl("La ficha no está asignada, el proceso a sido cancelado", BICO.growl('warning'));
						return false;
					} else {
						postParams.ficha 	= ID_Ficha;
					}

					postParams.id 		= row.ID;
					postParams.action 	= 'delete';

					$this.prop({disabled:true});

					$.post(resource, postParams, function(response) {

						if ( ! response.error ) {

							$.bootstrapGrowl(response.message, BICO.growl('success'));

							$table.bootstrapTable('refresh');

							$this.prop({ disabled: false });

						} else {

							$this.prop({disabled:false});

							$.bootstrapGrowl(response.message, BICO.growl('danger'));

						}

					});

				}
			}
		});

	},

	_asigna_eventos = function() {

		$md_mng.find('select').on('keypress', select_keyPress);

		$inputs.filter('.only-number').on('keydown.BICO.crear.OC.obras.module', only_number);
		$inputs.filter('.only-number').on('change.BICO.crear.OC.obras.module', verify_number);
		$inputs.filter('#txtcod_instalacion').on('change.BICO.crear.OC.obras.module', _search_otras_instalaciones);
		$inputs.filter('#cboinstalacion').on('change.BICO.crear.OC.obras.module', _change_cod_instalacion);
		$inputs.filter('input.letter-category').on('keydown.BICO.crear.OC.obras.module', _only_letters_category);
		$inputs.filter('input.checkdate').on('change.BICO.crear.OC.obras.module', _verificar_tipo_de_fecha);

		$inputs.filter('#txtlargo, #txtancho, #txtalto').on('change.BICO.crear.OC.obras.module', _check_total);

		$inputs.filter('.md-datasend').on('change.BICO.crear.OC.obras.module', _watcher_change);

		$md_mng.find('button#btn-done').on('click.BICO.crear.OC.obras.module', _submit_data);

	},

	_afterClose_modal = function() {

		$inputs.filter('input').val('');

		$inputs.filter('select').removeData('sel');

		($inputs.filter('select')).find('option:not(.default)').remove();
		($inputs.filter('select')).find('option.default').prop({selected:true});

		largo_ = false;
		ancho_ = false;
		alto_ = false;

		datasend = {};

	},

	_search_otras_instalaciones = function() {

		var that = this,
				$cbo = $inputs.filter('#cboinstalacion'),
				opts = $cbo.find('option'),
				found = false;

		if ( this.value.trim() == '' ) return;

		$.each(opts, function(index, element) {
			if ( element.value == that.value ) {
				$cbo.find('option[value="'+that.value+'"]').prop({selected:true});
				$cbo.change();
				found = true;
				return false;
			}
		});

		if ( ! found ) {
			$cbo.find('option.default').prop({selected:true});
			$cbo.change();
		}

	},

	_change_cod_instalacion = function() {

		var $this = $(this);

		if ( this.value.trim() == '' ) {
			$inputs.filter('#txtcod_instalacion').val('');
			$inputs.filter('#txtmatpredo').val('');
			$inputs.filter('#txtumedida').val('');
			$inputs.filter('#txtlargo').prop({disabled:true}).val('').change();
			$inputs.filter('#txtancho').prop({disabled:true}).val('').change();
			$inputs.filter('#txtalto').prop({disabled:true}).val('').change();
			$inputs.filter('#txttotal').prop({disabled:true}).val('').change();
			return false;
		}

		var resource = BICO.base_url + 'individual/getotrasinstalaciones',
				params = { code: this.value.trim() };

		$this.prop({disabled:true});

		$.getJSON(resource, params, function(json) {

			if ( ! json.error ) {

				if ( typeof(json.data.cod_instalacion) != 'undefined' ) {

					var data = json.data;

					$inputs.filter('#txtcod_instalacion').val(data.cod_instalacion);
					$inputs.filter('#txtmatpredo').val(data.mate_predominante);

					$inputs.filter('#txtlargo').prop({disabled:(!data.largo)}).val('').change();
					$inputs.filter('#txtancho').prop({disabled:(!data.ancho)}).val('').change();
					$inputs.filter('#txtalto').prop({disabled:(!data.alto)}).val('').change();
					$inputs.filter('#txttotal').prop({disabled:(!data.calc_valor)}).val('').change();

					$inputs.filter('#txtumedida').val(data.umedida);

					largo_ = data.largo;
					ancho_ = data.ancho;
					alto_ = data.alto;

					if ( data.umedida == 'UNIDAD' ) {
						$inputs.filter('#txtunidad').prop({disabled:false}).val('');
					} else {
						$inputs.filter('#txtunidad').prop({disabled:true}).val('');
					}

					$this.prop({disabled:false}).focus();

				} else {

					$this.css({borderColor:'red'});
					$.bootstrapGrowl('No se encontró la instalación', BICO.growl('danger'));

				}

			} else {
				$this.css({borderColor:'red'});
				$.bootstrapGrowl('No se encontró la instalación', BICO.growl('danger'));
			}
			
		});
	
	},

	_check_total = function() {

		this.value = this.value == '' ? 0 : parseFloat(this.value);

		var $txtancho = $inputs.filter('#txtancho'),
				$txtalto 	= $inputs.filter('#txtalto'),
				$txtlargo = $inputs.filter('#txtlargo'),
				$txttotal = $inputs.filter('#txttotal');

		var ancho = parseFloat( ($txtancho.val() == '') ? 0 : $txtancho.val() );
		var alto = parseFloat( ($txtalto.val() == '') ? 0 : $txtalto.val() );
		var largo = parseFloat( ($txtlargo.val() == '') ? 0 : $txtlargo.val() );

		if ( ((largo * ancho) * alto) > 0 ) $txttotal.val( ((largo * ancho) * alto) );
		if ( ( largo == 0.0 ) & (ancho == 0.0) ) $txttotal.val( alto );
    if ( ( largo == 0.0 ) & ( alto == 0.0 ) ) $txttotal.val( ancho );
    if ( ( ancho == 0.0 ) & ( alto == 0.0 ) ) $txttotal.val( largo );
    if ( (largo == 0.0) & ( ( ancho * alto ) > 0.0) ) $txttotal.val( ancho * alto );
    if ( (alto == 0.0) & (( largo * ancho ) > 0.0) ) $txttotal.val( largo * ancho );
    if ( ( ancho == 0.0 ) & ( ( largo * alto ) > 0.0 ) ) $txttotal.val( largo * alto );

    datasend.total = $txttotal.val();

	},

	_verificar_tipo_de_fecha = function() {

		var fecha = parseInt(this.value.trim());

		if ( $(this).data('type') == 'mes' ) {
			if ( fecha > 12 || fecha < 1 ) {
				this.value = '01';
			}
			this.value = this.value.format('00');
		} else {
			if ( fecha < 1900 || fecha > 2099 ) {
				this.value = 1900;
			}
		}

	},

	_only_letters_category = function(e) {

		if ($.inArray(e.keyCode, [8, 9, 27, 13, 127, 37, 39, 116]) !== -1 ||
        (e.keyCode == 37 && e.ctrlKey === true) ||
        (e.keyCode == 39 && e.ctrlKey === true)) {
             return;
    }
    
    if ((e.shiftKey || (e.keyCode < 65 || e.keyCode > 73)) && (e.keyCode < 65 || e.keyCode > 73)) {
        e.preventDefault();
    }

	},

	_load_combos = function() {

		$.each($md_mng.find('select.autoload'), function(index, el) {

			var $el = $(el),
					type = $el.data('type'),
					sw = $el.data('sw'),
					resource = 'principal/getcombo/type/'+type+'/sw/'+sw;

			$el.children('option:not(:eq(0))').remove();

			$.getJSON(BICO.base_url + resource, function(json, textStatus) {

				if ( ! json.error ) {

					json.data.forEach(function(obj, pos) {

						var params = { value: obj.CODIGO.trim() };

						if ( $el.data('sel') == params.value ) {
							params.selected = true;
						}

						$el.append( $('<option />', params).text(obj.NOMBRE) );

					});

					if ( $el.data('onchange') ) $el.change();

				} else {
					$el.attr({title:'No se encontraron datos'}).prop({disabled:true});
				}

			});

		});
	
	},

	_is_valid_data = function() {

		if ( datasend.cod_instalacion == '' ) {
			$.bootstrapGrowl("Seleccione una instalacion", BICO.growl('warning'));
			$inputs.filter('#txtcod_instalacion').focus();
			return false;
		}

		if ( datasend.mes == '' ) {
			$.bootstrapGrowl("Este dato es requerido", BICO.growl('warning'));
			$inputs.filter('#txtmes').focus();
			return false;
		}

		if ( datasend.anio == '' ) {
			$.bootstrapGrowl("Este dato es requerido", BICO.growl('warning'));
			$inputs.filter('#txtanio').focus();
			return false;
		}

		if ( datasend.mep == '' ) {
			$.bootstrapGrowl("Este dato es requerido", BICO.growl('warning'));
			$inputs.filter('#cbomep').focus();
			return false;
		}

		if ( datasend.ecs == '' ) {
			$.bootstrapGrowl("Este dato es requerido", BICO.growl('warning'));
			$inputs.filter('#cboecs').focus();
			return false;
		}

		if ( datasend.ecc == '' ) {
			$.bootstrapGrowl("Este dato es requerido", BICO.growl('warning'));
			$inputs.filter('#cboecc').focus();
			return false;
		}

		if ( (datasend.largo == '' || datasend.largo == 0) && largo_ ) {
			$.bootstrapGrowl("Este dato es requerido", BICO.growl('warning'));
			$inputs.filter('#txtlargo').focus();
			return false;
		}

		if ( (datasend.ancho == '' || datasend.ancho == 0) && ancho_ ) {
			$.bootstrapGrowl("Este dato es requerido", BICO.growl('warning'));
			$inputs.filter('#txtancho').focus();
			return false;
		}

		if ( (datasend.alto == '' || datasend.alto == 0) && alto_ ) {
			$.bootstrapGrowl("Este dato es requerido", BICO.growl('warning'));
			$inputs.filter('#txtalto').focus();
			return false;
		}

		return true;

	},

	/**
	 * Contiene los botones de opciones para cada fila
	 * @param  {string} value Valor que contiene el elemento
	 * @param  {object} row   Objeto con todo los datos de la fila seleccionada
	 * @param  {int} index Posición de la fila en la tabla
	 * @return {string} HTML de los botones dibujados
	 */
	_action_buttons = function(value, row, index) {
    return [
			'<button id="btn-edit-'+row.TIP_DOC+'" class="btn-actions edit btn btn-sm" data-id="'+row.TIP_DOC+'" data-accion="edit" tabindex="-1">',
				'<i class="fa fa-pencil" aria-hidden="true"></i>',
			'</button>',
			'<button id="btn-delete-'+row.TIP_DOC+'" class="btn-actions delete btn btn-sm" data-id="'+row.TIP_DOC+'" data-accion="delete" tabindex="-1">',
				'<i class="fa fa-trash-o" aria-hidden="true"></i>',
			'</button>'
		].join('');
  },

  /**
   * Carga la tabla con todos los datos
   * @return {void}
   */
	_build_table = function() {

		var bootTable = {
			escape: false,
		  locale: 'es-SP',
		  idField: "ID",
		  url: BICO.base_url + 'principal/gettable/type/01/rel/' + ID_Ficha,
		  onPostHeader: function(e){
				$table.find('tr th').removeAttr('tabindex');

		  }
		};

  	//TABLE HEADER
		bootTable.columns = [
			{ 
				field: 'action', 
				title: '<i class="fa fa-cog" aria-hidden="true"></i>', 
				align: 'center', 
				class: 'config-width',
				formatter : _action_buttons, 
				events : {
	        'click .edit': _open_md_edit,
	        'click .delete': _delete_row
    		}
    	},
			{ field: 'ID', title: 'N°', align: 'center' },
			{ field: 'COD_INSTALACION', title: 'Cod. Instalación', align: 'left' },
			{ field: 'DESCRIPCION', title: 'Descripción', align: 'left' },
			{ field: 'MES', title: 'Mes', align: 'center' },
			{ field: 'ANIO', title: 'Año', align: 'center' },
			{ field: 'MEP', title: 'MEP', align: 'center' },
			{ field: 'ECS', title: 'ECS', align: 'center' },
			{ field: 'ECC', title: 'ECC', align: 'center' },
			{ field: 'largo', title: 'Largo', align: 'right' },
			{ field: 'Ancho', title: 'Ancho', align: 'right' },
			{ field: 'alto', title: 'Alto', align: 'right' },
			{ field: 'total', title: 'Total', align: 'right' },
			{ field: 'UNIDAD_MEDIDA', title: 'U. Medida', align: 'center' }
			
		];

		$table.bootstrapTable(bootTable);

	};

	return {
		init: _bind
	}

}(window, window.jQuery));