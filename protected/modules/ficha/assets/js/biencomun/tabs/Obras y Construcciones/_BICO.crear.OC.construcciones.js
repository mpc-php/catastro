/**
 * SUBMODULO PARA CONSTRUCCIONES
 */
BICO.crear.OC.construcciones = (function (_win, $) {

	'use strict';

	var ID_Ficha 			= null,
			COD_Catastro 	= null,
			action 				= '';

	var datasend = {},
			default_ = {
				ficha: null,
				id: 0,
				nro_piso: '',
				mes: '',
				anio: '',
				mep: '',
				ecs: '',
				ecc: '',
				muros_col: '',
				techo: '',
				pisos: '',
				puerta_vent: '',
				reves: '',
				banios: '',
				inst_electricas: '',
				area_declarada: '',
				area_verificada: '',
				uca: '',
				action: ''
			};

	var $container = $({}),
			$md_mng = $('section#md-construcciones'),
			$bb_mng,
			$table,
			$inputs,
			$btn_new;

	//Constructor
	var _bind = function(_ficha_, _catastro_, _container_) {

		ID_Ficha 			= _ficha_;
		COD_Catastro 	= _catastro_;
		$container 		= _container_;

		$table = $container.find('table#td-construcciones');
		$btn_new = $container.find('button#add-construccion');
		$inputs = $md_mng.find('input, select');

		_build_table();

		$btn_new.on('click.BICO.crear.OC.construcciones.modules', _open_md_new);

	},

	_watcher_change = function() {

		var $this = $(this),
			name = $this.attr('name'),
			type = $this.attr('type'),
			tag = $this.prop("tagName").toLowerCase();

		if ( tag == 'input' && type == 'text' ) {

			datasend[name] = $.trim($this.val());

		} else if ( tag == 'select' ) {

			datasend[name] = $this.children('option:selected').val();

		}

	},

	_submit_data = function() {

		datasend = $.extend({}, default_, datasend);

		if ( ! _campos_requeridos_completos() ) {
			return false;
		}

		if ( ID_Ficha == null ) {
			$.bootstrapGrowl("La ficha no está asignado, el proceso a sido cancelado", BICO.growl('warning'));
			return false;
		} else {
			datasend.ficha = ID_Ficha;
		}

		var $elems = $md_mng.find('input, select, button');

		$elems.prop({disabled:true});

		$.post(BICO.base_url + 'biencomun/mantenimientoconstrucciones', datasend, function(response) {

			if ( ! response.error ) {

				$.bootstrapGrowl(response.message, BICO.growl('success'));
				
				$table.bootstrapTable('refresh');

				$inputs.filter('input').val('');

				($inputs.filter('select')).find('option.default').prop({selected:true});

				datasend = {};
				datasend.action = action;
				
				$elems.prop({ disabled: false });

				$inputs.filter('#txtnro_piso').focus();

			} else {

				$elems.prop({disabled:false});
				$.bootstrapGrowl(response.message, BICO.growl('danger'));

				$bb_mng.find('input#txtnro_piso').focus();

			}

		});
		

	},

	_open_md_new = function() {

		$bb_mng = $md_mng.cBootbox({
			title: "Agregar construcción",
			size: 'large',
			btnClose: '#btn-close',
			afterClose: _after_close_modal,
			beforeOpen: function() {

				_load_combos();

				$inputs.prop({disabled:false});
				$md_mng.find('button#btn-done').prop({disabled:false});

				_asigna_eventos();

				_win.setTimeout(function(){
					$inputs.filter('#txtnro_piso').focus();
				}, 500);

				action = 'create';
				datasend.action = action;
				
			}
		});

	},

	_open_md_edit = function(e, value, row, index) {
		$bb_mng = $md_mng.cBootbox({
			title: "Editar construcción",
			size: 'large',
			btnClose: '#btn-close',
			afterClose: _after_close_modal,
			beforeOpen: function() {

				_asigna_eventos();

				$inputs.prop({disabled:false});

				$md_mng.find('button#btn-done').prop({disabled:false});
			
				$md_mng.find('input#txtnro_piso').val(row.NIVEL).change();
				$md_mng.find('input#txtmes').val(row.MES).change();
				$md_mng.find('input#txtanio').val(row.ANIO).change();
				$md_mng.find('input#txtmuros_col').val(row.ESTRU_MUR_COL).change();
				$md_mng.find('input#txttecho').val(row.ESTRU_TECHO).change();
				$md_mng.find('input#txtpisos').val(row.ACABA_PISO).change();
				$md_mng.find('input#txtpuerta_vent').val(row.ACABA_PUE_VEN).change();
				$md_mng.find('input#txtreves').val(row.ACABA_REVEST).change();
				$md_mng.find('input#txtbanios').val(row.ACABA_BANIO).change();
				$md_mng.find('input#txtinst_electricas').val(row.INSTA_ELEC_SANIT).change();
				$md_mng.find('input#txtarea_declarada').val(row.AREA_DECLARADA).change();
				$md_mng.find('input#txtarea_verificada').val(row.AREA_VERIFICADA).change();

				($md_mng.find('select#cbouca')).data({sel:row.UCA});
				($md_mng.find('select#cbomep')).data({sel:row.MEP});
				($md_mng.find('select#cboecs')).data({sel:row.ECS});
				($md_mng.find('select#cboecc')).data({sel:row.ECC});

				datasend.id = row.ID;

				_load_combos();

				action = 'update';
				datasend.action = action;
				
			}
		});
	},

	/**
	 * Evento que elimina una fila de la tabla
	 * @param  {object} e     Instancia del evento
	 * @param  {string} value Valor que contiene el elemento
	 * @param  {object} row   Objeto con todo los datos de la fila seleccionada
	 * @param  {int} index Posición de la fila en la tabla
	 * @return {[type]}       [description]
	 */
	_delete_row = function(e, value, row, index) {

		var $this = $(this),
				postParams = {},
				resource = BICO.base_url + 'biencomun/mantenimientoconstrucciones';

		bootbox.confirm({
			title: 'Seguro que quieres eliminar?',
			message: "La fila seleccionada será eliminado de forma permanente.",
			callback: function(result) {

				if ( result ) {

					if ( ID_Ficha == null ) {
						$.bootstrapGrowl("La ficha no está asignada, el proceso a sido cancelado", BICO.growl('warning'));
						return false;
					} else {
						postParams.ficha = ID_Ficha;
					}

					postParams.id = row.ID;
					postParams.action = 'delete';

					$this.prop({disabled:true});

					$.post(resource, postParams, function(response) {
						if ( ! response.error ) {

							$.bootstrapGrowl(response.message, BICO.growl('success'));
							$table.bootstrapTable('refresh');
							$this.prop({ disabled: false });

						} else {

							$this.prop({disabled:false});
							$.bootstrapGrowl(response.message, BICO.growl('danger'));

						}
					});

				}
			}
		});

	},

	_asigna_eventos = function() {

		$md_mng.find('select').on('keypress', select_keyPress);

		$md_mng.find('button#btn-done').on('click.BICO.crear.OC.construcciones.module', _submit_data);

		$inputs.filter('input[type=text].only-number').on('keydown.BICO.crear.OC.module', only_number);
		$inputs.filter('input[type=text].only-number').on('change.BICO.crear.OC.module', verify_number);

		$inputs.filter('input.letter-category').on('keydown.BICO.crear.OC.construcciones.module', _only_letters_category);
		$inputs.filter('input.checkdate').on('change.BICO.crear.OC.construcciones.module', _verificar_tipo_de_fecha);

		$inputs.filter('.md-datasend').on('change.BICO.crear.OC.module', _watcher_change);
	},

	_after_close_modal = function() {

		$md_mng.find('input').val('').prop({disabled:true});

		var $cbo = $md_mng.find('select');

		$cbo.removeData('sel');
		$cbo.children('option.default').prop({selected:true});
		$cbo.children('option:not(.default)').remove();

		datasend = {};
		action = '';

		$cbo.prop({disabled:true});

	},

	_verificar_tipo_de_fecha = function() {

		var fecha = parseInt(this.value.trim());

		if ( $(this).data('type') == 'mes' ) {
			if ( fecha > 12 || fecha < 1 ) {
				this.value = '01';
			}
			this.value = this.value.format('00');
		} else {
			if ( fecha < 1900 || fecha > 2099 ) {
				this.value = 1900;
			}
		}

	},

	_only_letters_category = function(e) {
		if ($.inArray(e.keyCode, [8, 9, 27, 13, 127, 37, 39, 116]) !== -1 ||
        (e.keyCode == 37 && e.ctrlKey === true) ||
        (e.keyCode == 39 && e.ctrlKey === true)) {
             return;
    }
    
    if ((e.shiftKey || (e.keyCode < 65 || e.keyCode > 73)) && (e.keyCode < 65 || e.keyCode > 73)) {
        e.preventDefault();
    }
	},

	_load_combos = function() {

		$.each($md_mng.find('select.autoload'), function(index, el) {

			var $el = $(el),
					type = $el.data('type'),
					sw = $el.data('sw'),
					resource = 'principal/getcombo/type/'+type+'/sw/'+sw;

			$.getJSON(BICO.base_url + resource, function(json, textStatus) {

				if ( ! json.error ) {

					json.data.forEach(function(obj, pos) {

						var params = { value: obj.CODIGO.trim() };

						if ( $el.data('sel') == params.value || obj.NOMBRE.indexOf($el.data('sel')) !== -1 ) {
							params.selected = true;
						}

						$el.append( $('<option />', params).text(obj.NOMBRE) );

					});

					$el.change();

				} else {
					$el.attr({title:'No se encontraron datos'}).prop({disabled:true});
				}

			});

		});
	},

	_campos_requeridos_completos = function() {

		var errors = [];

		$.each(datasend, function(key, val) {

			var $elem = $inputs.filter('[name="'+ key +'"]');

			// VALIDAR NO REQUERIDOS
			if ( key != 'action' && 
						key != 'id' && 
						key != 'ficha' && 
						key != 'uca' &&
						key != 'area_declarada' &&
						key != 'area_verificada' ) {

				var tag = $elem.prop("tagName").toLowerCase();

				if ( (val.trim()).length == 0 ) {
					errors.push({
						elem: $elem,
						message: 'Este campo es requerido'
					});
				} else {
					$elem.removeAttr('style');
				}

			}

		});

		if ( errors.length > 0 ) {
			$.bootstrapGrowl(errors[0].message, BICO.growl('warning'));
			errors[0].elem.css({borderColor:'red'}).focus();
			return false;
		}


		return true;

	},

	/**
	 * Contiene los botones de opciones para cada fila
	 * @param  {string} value Valor que contiene el elemento
	 * @param  {object} row   Objeto con todo los datos de la fila seleccionada
	 * @param  {int} index Posición de la fila en la tabla
	 * @return {string} HTML de los botones dibujados
	 */
	_action_buttons = function(value, row, index) {
    return [
			'<button id="btn-edit-'+row.TIP_DOC+'" class="btn-actions edit btn btn-sm" data-id="'+row.TIP_DOC+'" data-accion="edit" tabindex="-1">',
				'<i class="fa fa-pencil" aria-hidden="true"></i>',
			'</button>',
			'<button id="btn-delete-'+row.TIP_DOC+'" class="btn-actions delete btn btn-sm" data-id="'+row.TIP_DOC+'" data-accion="delete" tabindex="-1">',
				'<i class="fa fa-trash-o" aria-hidden="true"></i>',
			'</button>'
		].join('');
  },

  /**
   * Carga la tabla con todos los datos
   * @return {void}
   */
	_build_table = function() {

		var bootTable = {
			escape: false,
		  locale: 'es-SP',
		  idField: "ID",
		  url: BICO.base_url + 'principal/gettable/type/02/rel/' + ID_Ficha,
		  onPostHeader: function(e){
				$table.find('tr th').removeAttr('tabindex');
		  }
		};

  	//TABLE HEADER
		bootTable.columns = [
			{ 
				field: 'action', 
				title: '<i class="fa fa-cog" aria-hidden="true"></i>', 
				align: 'center', 
				class: 'config-width',
				formatter : _action_buttons, 
				events : {
	        'click .edit': _open_md_edit,
	        'click .delete': _delete_row
    		}
    	},
			{ field: 'ID', title: 'N°', align: 'center' },
			{ field: 'NIVEL', title: 'NIVEL', align: 'center' },
			{ field: 'MES', title: 'MES', align: 'center' },
			{ field: 'ANIO', title: 'AÑO', align: 'center' },
			{ field: 'MEP', title: 'MEP', align: 'center', class:'mep' },
			{ field: 'ECS', title: 'ECS', align: 'center' },
			{ field: 'ECC', title: 'ECS', align: 'center' },
			{ field: 'ESTRU_MUR_COL', title: 'MyC', align: 'center' },
			{ field: 'ESTRU_TECHO', title: 'TECHO', align: 'center' },
			{ field: 'ACABA_PISO', title: 'PISO', align: 'center' },
			{ field: 'ACABA_PUE_VEN', title: 'PyV', align: 'center' },
			{ field: 'ACABA_REVEST', title: 'REV.', align: 'center' },
			{ field: 'ACABA_BANIO', title: 'BAÑOS', align: 'center' },
			{ field: 'INSTA_ELEC_SANIT', title: 'IE', align: 'center' },
			{ field: 'AREA_DECLARADA', title: 'A. DEC', align: 'right' },
			{ field: 'AREA_VERIFICADA', title: 'A. VER', align: 'right' },
			{ field: 'UCA', title: 'UCA', align: 'left' }
		];

		$table.bootstrapTable(bootTable);

	};

	return {
		init: _bind
	}

}(window, window.jQuery));