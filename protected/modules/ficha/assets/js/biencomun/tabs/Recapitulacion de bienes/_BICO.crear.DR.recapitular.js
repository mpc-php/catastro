BICO.crear.DR.recapitular = ( function( _win, $ ) {

	'use strict';

	var ID_Ficha,
			cod_catastro;

	var $table 						= $('<table />'),
			$btn_recapitular 	= $('<button />'),
			$md_mng 					= $('section#md-actporcentaje'),
			$btn_done 				= $md_mng.find('#btn-done'),
			$bb_mng 					= {};

	var _bind = function(_ficha_, _catastro_, _btnactivator_, _table_) {

		ID_Ficha = _ficha_;
		cod_catastro = _catastro_;
		$btn_recapitular = _btnactivator_;
		$table  = _table_;

		$btn_recapitular.on('click.BICO.crear.DR.recapitular.module', _recapitular);

		_build_table();

	},

	_recapitular = function() {

		var txtareaverificada = document.getElementById('txtarea_verificada');

		if ( txtareaverificada.value.trim() == '' ) {

			$.bootstrapGrowl("Debe registrar el area verificada", BICO.growl('danger'));
			$('#tabs-control').find('[href="#tab_descpredio"]').tab('show');
			txtareaverificada.focus();
			return false;

		}

		var $this = $(this),
				resource = BICO.base_url + 'biencomun/recapitular',
				params = {
					ficha: ID_Ficha,
					averificada: txtareaverificada.value.trim(),
					catastro: cod_catastro
				};

		$this.prop({disabled:true});

		$.post(resource, params, function( response ) {

			/*if ( ! response.error ) {
				$this.prop({disabled:false});
				$table.bootstrapTable('refresh');
			} else {
				$.bootstrapGrowl("Error al momento de recapitular", BICO.growl('danger'));
				$this.prop({disabled:false});
			}*/

			$table.bootstrapTable('refresh');
			$this.prop({disabled:false});

		});

	},

	_format_number_decimal = function (x) {

		var parts = x.toString().split(".");

		parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");

		return parts.join(".");

	},

	_calcula_totales = function(data) {

		if ( data.length > 0 ) {

			var $cell_porc = $table.find('#celltotalporcen'),
					$cell_aoic = $table.find('#celltotalaoic'),
					$cell_acc 	= $table.find('#celltotalacc'),
					$cell_atc 	= $table.find('#celltotalatc');

			var totalporc = 0,
					totalaoic = 0,
					totalacc = 0,
					totalatc = 0;

			data.forEach( function(obj, index) {
				totalporc += (parseFloat(obj.PORC));
				totalaoic += (parseFloat(obj.AOIC));
				totalacc += (parseFloat(obj.ACC));
				totalatc += (parseFloat(obj.ATC));
			});

			$cell_porc.html( (_format_number_decimal(totalporc.toFixed(2)) + "%") );
			$cell_aoic.html( _format_number_decimal(totalaoic.toFixed(2)) );
			$cell_acc.html( _format_number_decimal(totalacc.toFixed(2)) );
			$cell_atc.html( _format_number_decimal(totalatc.toFixed(2)) );

		}

	},

	_active_btn = function() {

		if ( this.value.trim() != '' ) {

			if ( $.isNumeric(this.value.trim()) ) {

				$btn_done.prop({disabled:false});

			} else {

				$btn_done.prop({disabled:true});
				this.value = '';

			}

		} else {
			$btn_done.prop({disabled:true});
		}


	},

	_submit_porcentaje = function() {

		var porcentaje = $.trim($md_mng.find('#txtporcentaje').val()),
				resource = BICO.base_url + 'biencomun/actualizaporcentaje',
				params = {
					nro_ficha: $md_mng.find('#lblnro_ficha').text(),
					porcentaje: porcentaje
				};

		if ( porcentaje == '' ) {
			$.bootstrapGrowl("Esta dato es requerido.", BICO.growl('warning'));
			$md_mng.find('#txtporcentaje').focus();
			return false;
		}

		$btn_done.prop({disabled:true});

		$.post(resource, params, function( response ) {

			$.bootstrapGrowl(response.message, BICO.growl('success'));

			if ( ! response.error ) {

				$table.bootstrapTable('refresh');

				$md_mng.find('#btn-close').click();

			} else {

				$btn_done.prop({disabled:false});
				$md_mng.find('#txtporcentaje').focus();

			}

		});

	},

	_editar_porcentaje = function(e, value, row, index) {

		$bb_mng = $md_mng.cBootbox({
			title: "Modificar Porcentaje",
			size: "small",
			btnClose: '#btn-close',
			beforeOpen: function() {

				$md_mng.find('#lblnro_ficha').text(row.F_IND);
				$md_mng.find('#lbledificacion').text(row.EDIFICA);
				$md_mng.find('#lblentrada').text(row.ENTRADA);
				$md_mng.find('#lblpiso').text(row.PISO);
				$md_mng.find('#lblunidad').text(row.UNIDAD);
				$md_mng.find('#txtporcentaje').val(row.PORC);

				_win.setTimeout(function() {
					$md_mng.find('#txtporcentaje').focus();
				}, 500);

				$btn_done.on('click.BICO.crear.DR.recapitular.module', _submit_porcentaje);
				$md_mng.find('#txtporcentaje').on('keyup.BICO.crear.DR.recapitular.module', _active_btn);

			}
		});

	},

	_action_buttons = function(value, row, index) {

    return [
			'<button id="btn-edit-'+row.ID+'" class="btn-actions edit btn btn-sm" data-id="'+row.ID+'" tabindex="-1" title="Modificar Porcentaje">',
				'<i class="fa fa-pencil" aria-hidden="true"></i>',
			'</button>'
		].join('');

  },

	_build_table = function() {

		var bootTable = {
		  locale: 'es-SP',
		  idField: "NRO",
		  sortable: false,
		  url: BICO.base_url + 'biencomun/getrecapitulaciones/rel/' + ID_Ficha,
		  onPostHeader: function(e){
				$table.find('tr th').removeAttr('tabindex');
		  },
		  onPreBody: _calcula_totales
		};

  	//TABLE HEADER
		bootTable.columns = [
			{
				field: 'action',
				title: '<i class="fa fa-cog" aria-hidden="true"></i>',
				align: 'center',
				formatter : _action_buttons,
				events : {
					'click .edit': _editar_porcentaje
	    	}
	    },
			{ field: 'NRO', title: 'N°', align: 'center' },
			{ field: 'EDIFICA', title: 'Edificación', align: 'center' },
			{ field: 'ENTRADA', title: 'Entrada', align: 'center' },
			{ field: 'PISO', title: 'Piso', align: 'center' },
			{ field: 'UNIDAD', title: 'Unidad', align: 'center' },
			{ field: 'PORC', title: 'Porcentaje', align: 'center' },
			{ field: 'ATC', title: 'ATC', align: 'center' },
			{ field: 'AOIC', title: 'AOIC', align: 'center' },
			{ field: 'ACC', title: 'ACC', align: 'center' },
			{ field: 'F_IND', title: 'F. Individual', align: 'center' }
		];

		$table.bootstrapTable(bootTable);

	};

	return {
		init: _bind
	}

}(window, window.jQuery));