BICO.crear.DR.recapitularEdificios = ( function( _win, $ ) {

	'use strict';

	var ID_Ficha,
			cod_catastro;

	var $table 						= $('<table />'),
			$btn_actualizar 	= $('<button />');

	var _bind = function(_ficha_, _catastro_, _btnactivator_, _table_) {

		ID_Ficha = _ficha_;
		cod_catastro = _catastro_;
		$btn_actualizar = _btnactivator_;
		$table  = _table_;

		$btn_actualizar.on('click.BICO.crear.DR.recapitularEdificios.module', _actualizar);

		_build_table();

	},

	_actualizar = function() {

		var $this = $(this),
				resource = BICO.base_url + 'biencomun/actualiza_recapitulacion_edificios/rel/' + ID_Ficha;

		$this.prop({disabled:true});

		$.post(resource, {}, function( response ) {

			var mess = 'danger';

			if ( response.data.Error == 0 ) {
				mess = 'success';
			}

			$.bootstrapGrowl(response.data.mensaje, BICO.growl(mess));

			$this.prop({disabled:false});

			if ( response.data.Error == 0 ) {
				$table.bootstrapTable('refresh');
			}

		});

	},

	_format_number_decimal = function (x) {

		var parts = x.toString().split(".");

		parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");

		return parts.join(".");

	},

	_calcula_totales = function(data) {

		if ( data.length > 0 ) {

			var $cell_porc = $table.find('#celltotalporcen'),
					$cell_aoic = $table.find('#celltotalaoic'),
					$cell_acc 	= $table.find('#celltotalacc'),
					$cell_atc 	= $table.find('#celltotalatc');

			var totalporc = 0,
					totalaoic = 0,
					totalacc = 0,
					totalatc = 0;

			data.forEach( function(obj, index) {
				totalporc += (parseFloat(obj.porcentaje));
				totalaoic += (parseFloat(obj.aoic));
				totalacc += (parseFloat(obj.acc));
				totalatc += (parseFloat(obj.atc));
			});

			$cell_porc.html( (_format_number_decimal(totalporc.toFixed(2)) + "%") );
			$cell_aoic.html( _format_number_decimal(totalaoic.toFixed(2)) );
			$cell_acc.html( _format_number_decimal(totalacc.toFixed(2)) );
			$cell_atc.html( _format_number_decimal(totalatc.toFixed(2)) );

		}

	},

	_active_btn = function() {

		if ( this.value.trim() != '' ) {

			if ( $.isNumeric(this.value.trim()) ) {

				$btn_done.prop({disabled:false});

			} else {

				$btn_done.prop({disabled:true});
				this.value = '';

			}

		} else {
			$btn_done.prop({disabled:true});
		}


	},

	_build_table = function() {

		var bootTable = {
		  locale: 'es-SP',
		  idField: "NRO",
		  sortable: false,
		  url: BICO.base_url + 'biencomun/getrecapitulacion_edificios/rel/' + ID_Ficha,
		  onPostHeader: function(e){
				$table.find('tr th').removeAttr('tabindex');
		  },
		  onPreBody: _calcula_totales
		};

  	//TABLE HEADER
		bootTable.columns = [
			{ field: 'Edificio', title: 'Edificación', align: 'center' },
			{ field: 'porcentaje', title: 'Porcentaje', align: 'right' },
			{ field: 'atc', title: 'ATC M<sup>2</sup>', align: 'right' },
			{ field: 'aoic', title: 'AOIC M<sup>2</sup>', align: 'right' },
			{ field: 'acc', title: 'ACC M<sup>2</sup>', align: 'right' }
		];

		$table.bootstrapTable(bootTable);

	};

	return {
		init: _bind
	}

}(window, window.jQuery));