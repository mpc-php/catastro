/**
 * SUBMODULO PARA EL MANEJO DE LA CREACION Y/O ACTUALIZACIÓN
 */
BICO.crear.DR.notarias.manage = (function (_win, $) {

	'use strict';

	var datasend = {},
			default_ = {
				id: '0',
				name: '',
				ubigeo: '000000'
			};

	var $md_mng 	= $('section#md-mng-notarias'),
			$btn_done = $md_mng.find('button#btn-done'),
			$bb_mng 	= {};

	var _bind = function(_btnactivator_) {
		_btnactivator_.on('click.BICO.crear.DR.notarias.manage.module', _open_modal);
	},

	_watcher_change = function() {

		var $this = $(this),
			name = $this.attr('name'),
			type = $this.attr('type'),
			tag = $this.prop("tagName").toLowerCase();

		if ( tag == 'input' && type == 'text' ) {
			datasend[name] = $.trim($this.val());
		} else if ( tag == 'select' ) {
			datasend[name] = $this.children('option:selected').val();
		}

	},

	_submit_data = function() {

		var resource = BICO.base_url + 'biencomun/mantenimientonotaria';

		datasend = $.extend({}, default_, datasend);

		if ( datasend.name.length == 0 ) {
			$.bootstrapGrowl("Debes asignar un nombre.", BICO.growl('warning'));
			$md_mng.find('input[type=text]#txtname').focus();
			return false;
		}

		if ( datasend.ubigeo.length == 0 || datasend.ubigeo == '000000' ) {
			$.bootstrapGrowl("El ubigeo no está completo.", BICO.growl('warning'));
			$md_mng.find('select#cbodepartamento').focus();
			return false;
		}

		$.post(resource, datasend, function(response) {

			if ( ! response.error ) {

				$.bootstrapGrowl(response.message, BICO.growl('success'));

				BICO.crear.DR.notarias.table.refresh(datasend.name);

				$bb_mng.find('#btn-close').click();

			} else {

				$.bootstrapGrowl(response.message, BICO.growl('danger'));

				$btn_done.prop({disabled:false});

				$md_mng.find('input[type=text]#txtname').focus();

			}

		});

	},

	_open_modal = function() {

		$bb_mng = $md_mng.cBootbox({
			title: "Nueva notaría",
			btnClose: '#btn-close',
			size:'small',
			beforeOpen: function() {

				var cbodep = $md_mng.find('select#cbodepartamento'),
						cbopro = $md_mng.find('select#cboprovincia'),
						cbodis = $md_mng.find('select#cbodistrito');

				_load_ubigeos(cbodep, '0');

				_asignar_eventos();

				$btn_done.prop({disabled:false});

			},
			afterClose: _afterClose_modal
		});

	},

	_open_modal_edit = function(e, value, row, index) {

		$bb_mng = $md_mng.cBootbox({
			title: "Editar notaría",
			btnClose: '#btn-close',
			size:'small',
			beforeOpen: function() {

				var $cbodep = $md_mng.find('select#cbodepartamento'),
						$cbopro = $md_mng.find('select#cboprovincia'),
						$cbodis = $md_mng.find('select#cbodistrito');

				datasend.id = row.ID_NOTARIA;
				datasend.name = row.NOM_NOTARIA;
				datasend.ubigeo = row.ID_UBI_GEO;

				$md_mng.find('input[type=text]#txtname').val(row.NOM_NOTARIA);

				var dep = row.ID_UBI_GEO.substr(0, 2),
						pro = row.ID_UBI_GEO.substr(0, 4),
						dis = row.ID_UBI_GEO.substr(0, 6);

				$cbodep.data({sel:dep});
				$cbopro.data({sel:pro});
				$cbodis.data({sel:dis});

				_load_ubigeos($cbodep, '0');
				_load_ubigeos($cbopro, dep);
				_load_ubigeos($cbodis, pro);

				_asignar_eventos();

				$btn_done.prop({disabled:false});

			},
			afterClose: _afterClose_modal
		});

	},

	_load_ubigeos = function($cbo, $cod) {

		$cod = $cod || '0';

		$cbo.children('option:eq(0)').prop({ selected: true });

		$cbo.children('option:not(:eq(0))').remove();

		$.getJSON(BICO.base_url + 'principal/getubigeo/c/' + $cod, function(json) {

			if ( ! json.error ) {

				json.data.forEach(function(obj, index) {

					var params = { value: obj.CODIGO.trim() };

					if ( $cbo.data('sel') == obj.CODIGO.trim() ) {
						params.selected = true;
					}

					$cbo.append( $('<option />', params).text(obj.NOMBRE) );

				});

				if( json.data.length > 0 )
					$cbo.prop({disabled:false});

			} else {
				$cbo.prop({selected:true});
			}

		});

	},

	_choise_departamento = function() {

		var $this = $(this),
				cbopro = $md_mng.find('select#cboprovincia');

		cbopro.prop({disabled:true});

		_load_ubigeos(cbopro, $this.children('option:selected').val());

		var dist = $md_mng.find('select#cbodistrito');

		dist.children('option:not(:eq(0))').remove();
		dist.prop({disabled:true});

	},

	_choise_provincia = function() {

		var $this = $(this),
				cbodis = $md_mng.find('select#cbodistrito');

		cbodis.prop({disabled:true});

		_load_ubigeos(cbodis, $this.children('option:selected').val());

	},

	_asignar_eventos = function() {

		$md_mng.find('select').on('keypress', select_keyPress);

		$md_mng.find('select#cbodepartamento').on('change.BICO.crear.DR.module', _choise_departamento);
		$md_mng.find('select#cboprovincia').on('change.BICO.crear.DR.module', _choise_provincia);

		$md_mng.find('.datasend_notaria').on('change.BICO.crear.DR.module', _watcher_change);
		$btn_done.on('click.BICO.crear.DR.module', _submit_data);

	},

	_afterClose_modal = function() {

		$md_mng.find('input[type=text]#txtname').val('');

		$bb_mng.find('select#cbodepartamento').children('option:not(.default)').remove();
		$bb_mng.find('select#cboprovincia').children('option:not(.default)').remove();
		$bb_mng.find('select#cbodistrito').children('option:not(.default)').remove();

		$bb_mng.find('select#cbodepartamento').children('option.default').prop({selected:true});
		$bb_mng.find('select#cboprovincia').children('option.default').prop({selected:true});
		$bb_mng.find('select#cbodistrito').children('option.default').prop({selected:true});

		$btn_done.prop({ disabled: true });

		datasend = {};

	};

	return {
		init: _bind,
		update: _open_modal_edit
	};

}(window, window.jQuery));