/**
 * SUBMODULO PARA EL MANEJO DE NOTARIAS
 */
BICO.crear.DR.notarias = (function (_win, $) {

	'use strict';

	var $cbolist 		= $('<select />'),
			$md_mng 		= $('section#md-notaria'),
			$btn_new 		= $md_mng.find('button#btn-new'),
			$btn_done 	= $md_mng.find('button#btn-done'),
			$table 			= $md_mng.find('table#dt-notarias'),
			$bb_mng 		= {};

	var _bind = function( _clickActivator_, _select_container ) {

		$cbolist = _select_container;
		_clickActivator_.on('click.BICO.crear.DR.module', _open_modal);

	},

	_open_modal = function() {

		$bb_mng = $md_mng.cBootbox({
			title: "Notarías",
			btnClose: '#btn-cancel',
			beforeOpen: function() {

				_build_table();

				$btn_done.prop({disabled:false});
				$btn_new.prop({disabled:false});

				BICO.crear.DR.notarias.manage.init($btn_new);

				$btn_done.on('click.BICO.crear.DR.module', _choise_option);

			},
			afterClose: function() { $table.bootstrapTable('destroy'); }
		});

	},

	_choise_option = function() {

		var selection = $table.bootstrapTable('getSelections');

		events.emit('choiseOption', selection);

		$bb_mng.find('#btn-cancel').click();

	},

	_refresh_table = function(search) {

		$table.bootstrapTable('refresh', {searchText:search});

		events.emit('refreshComboNotarias', false);

	},

	_action_buttons = function(value, row, index) {

    return [
			'<button id="btn-edit-'+row.ID_NOTARIA+'" class="btn-actions edit btn btn-sm" data-id="'+row.ID_NOTARIA+'" data-accion="edit">',
				'<i class="fa fa-pencil"></i>',
			'</button>'
		].join('');

  },

	_build_table = function() {

		var bootTable = {
			escape: true,
		  locale: 'es-SP',
		  search: true,
		  searchAlign:'left',
		  selectItemName: 'inotaria',
		  height: 400,
		  clickToSelect: true,
		  idField: "ID_NOTARIA",
		  url: BICO.base_url + 'biencomun/getnotarias',
		  onDblClickRow: _choise_option,
		  onPostHeader: function(){

		  	$table.find('tr th').removeAttr('tabindex');

		  	_win.setTimeout(function() {
		  		($bb_mng.find('.search')).find('input').focus();
		  	}, 500);

		  }
		};

  	//TABLE HEADER
		bootTable.columns = [
			{
				field: 'action',
				title: '<i class="fa fa-cog" aria-hidden="true"></i>',
				align: 'center',
				formatter : _action_buttons,
				events : { 'click .edit': BICO.crear.DR.notarias.manage.update }
	    },
			{ field: 'state', radio: true, align: 'center' },
			{ field: 'ID_NOTARIA', title: 'ID', align: 'center', sortable:true },
			{ field: 'NOM_NOTARIA', title: 'NOTARIA', align: 'left', sortable:true }
		];

		$table.bootstrapTable(bootTable);

	}

	return {
		init: _bind,
		table: {
			refresh: _refresh_table
		}
	};

}(window, window.jQuery));