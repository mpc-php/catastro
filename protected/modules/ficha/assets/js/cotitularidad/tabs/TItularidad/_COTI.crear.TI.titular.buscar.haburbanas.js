/**
 * SUBMODULO PARA AGREGAR PERSONAS
 */
COTI.crear.TI.titular.buscar.haburbanas = ( function (_win, $) {

	'use strict';

	var SECTOR,
			allowedOpen = false,
			hurbanas_arr;

	var $md_mng = $('section#md-buscar'),
			$table 	= $md_mng.find('table#dt-buscar'),
			$input = $('input[type=text]#txtcod_hurbana'),
			$bb_mng = {};

	var _bind = function() {

		$input.on('keydown.COTI.crear.TI.buscar.haburbanas.module', _capture_keyfunction);
		$input.on('keyup.COTI.crear.TI.buscar.haburbanas.module', _open_modal);

		events.on('setHaburbanas', _set_haburbanas);

	},

	_set_haburbanas = function( data ) {

		hurbanas_arr = data;

	},

	_capture_keyfunction = function(e) {

		allowedOpen = false;

		if ( e.which == 112 ) {

			allowedOpen = true;

			e.stopImmediatePropagation();
			e.preventDefault();
		}

	},

	_open_modal = function(e) {

		var that =  this;

		if ( allowedOpen ) {

			$bb_mng = $md_mng.cBootbox({
				title: "Habilitaciones urbanas",
				btnClose: '#btn-cancel',
				afterClose: _afterClose_modal,
				beforeOpen: function() {

					_build_table(hurbanas_arr, that.value.trim());

					$md_mng.find('button#btn-done, button#btn-new').prop({disabled:false});

					$md_mng.find('button#btn-done').on('click.COTI.crear.TI.buscar.module', _click_selection);

				}
			});

			allowedOpen = false;

			e.preventDefault();

		}

	},

	_dblClick_selection = function(row, $element, field) {

		events.emit('getRowSelectedHaburbana', row);

		$bb_mng.find('button#btn-cancel').click();

	},

	_click_selection = function() {

		var selection = $table.bootstrapTable('getSelections');

		if ( selection.length > 0 ) {
			events.emit('getRowSelectedHaburbana', selection[0]);
		}

		$bb_mng.find('button#btn-cancel').click();

	},

	_actions_postHeader = function() {

		setFocus(($bb_mng.find('.search')).children('input'), false);

		$table.find('thead tr th').removeAttr('tabindex');

	},

	_afterClose_modal = function() {

		$table.bootstrapTable('destroy');

	},

	_build_table = function(data, searchText) {

		var bootTable = {
		  locale: 'es-SP',
		  search: true,
		  selectItemName: 'hurbanas',
		  clickToSelect: true,
		  idField: "code",
		  height: 410,
		  pagination:true,
		  data: data,
		  searchText: searchText,
		  onDblClickRow: _dblClick_selection,
		  onPostHeader: _actions_postHeader
		};
  	//TABLE HEADER
		bootTable.columns = [
			{ field: 'state', radio: true, align: 'center' },
			{ field: 'code', title: 'Codigo', align: 'center' },
			{ field: 'name', title: 'Nombre', align: 'left' },
			{ field: 'zoni', title: 'Zona|Sector|Etapa', align: 'left' }
		];

		$table.bootstrapTable(bootTable);

	};

	return {
		init: _bind
	};

}(window, window.jQuery));