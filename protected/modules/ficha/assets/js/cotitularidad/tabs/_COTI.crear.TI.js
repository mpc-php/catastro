// @koala-append "Titularidad/_COTI.crear.TI.titular.js"
// @koala-append "Titularidad/_COTI.crear.TI.titular.agregar.js"
// @koala-append "Titularidad/_COTI.crear.TI.titular.buscar.js"
// @koala-append "Titularidad/_COTI.crear.TI.titular.buscar.vias.js"
// @koala-append "Titularidad/_COTI.crear.TI.titular.buscar.haburbanas.js"

/**
 * MODULO PARA LA PESTAÑA COTITULARIDAD
 */
COTI.crear.TI = (function (_win, $) {

	'use strict';

	var ID_Ficha 			= null,
			COD_Catastro 	= null;

	var $cnt_ti 			= $('div#tab_titularidad');

	//CONSTRUCTOR
	var _bind = function (_ficha_, _catastro_, _sector_, _ubigeo_) {

		ID_Ficha 			= _ficha_,
		COD_Catastro 	= _catastro_;

		$cnt_ti.find('#btn-add-titular').on('keydown.COTI.crear.TI.module', _open_nex_tab);

		//INSTANCIANDO SUBMODULO
		COTI.crear.TI.titular.init(
			_ficha_,
			_catastro_,
			_sector_,
			_ubigeo_,
			$cnt_ti.find('button#btn-add-titular'),
			$cnt_ti.find('table#td-titular')
		);

	},

	_open_nex_tab = function(e) {

		if ( e.key == "Tab" && e.keyCode == 9 ) {
			$('#tabs-control').find('a[href="#tab_infcomplementaria"]').tab('show');
			e.preventDefault();
			return false;
		}

	};

	return {
		init: _bind
	};

}(window, window.jQuery));