// @koala-prepend "_helpers/_base.js"
// @koala-prepend "_helpers/_settings.js"

// @koala-prepend "_modules/_observaciones.js"

// @koala-prepend "individual/_INDI.index.js"
// @koala-prepend "individual/_INDI.crear.js"

$(function() {

	var $page = $('div#page');

	switch($page.data('module')) {
		case 'index':
			INDI.index.init();
			break;
		case 'create':
		case 'update':
			INDI.crear.init();
			break;
	}
	
});