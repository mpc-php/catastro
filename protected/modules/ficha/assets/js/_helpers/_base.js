//@koala-prepend "_shortcut.js";
//@koala-prepend "_events.js";

//formatea las cadenas y agrega ceros para completar
//la cadena con el tamaño especifico del formato
String.prototype.format = function(FORMAT) {
	return FORMAT.slice(this.length, FORMAT.length) + this;
};

String.prototype.isEmpty = function() {
  return (this.trim()).length == 0 ? true : false;
};

function only_number(e) {

  if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190, 127, 37, 39, 116]) !== -1 ||
    (e.keyCode == 65 && e.ctrlKey === true) ||
    (e.keyCode == 67 && e.ctrlKey === true) ||
    (e.keyCode == 88 && e.ctrlKey === true) ||
    (e.keyCode == 86 && e.ctrlKey === true) ||
    (e.keyCode == 37 && e.ctrlKey === true) ||
    (e.keyCode == 39 && e.ctrlKey === true) ||
    (e.keyCode >= 35 && e.keyCode <= 39) || e.keyCode == 116) {
      return;
  }
  if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
    e.preventDefault();
  }

};

function verify_number(e) {

  var $this = $(this),
      num = this.value.trim();

  if ( ! $.isNumeric(num) ) {
    this.value = null;
    e.stopPropagation();
  } else {
    //si es decimal, redondea a 2 digitos decimales
    if ( Number(num) === parseFloat(num) && parseFloat(num) % 1 !== 0 ) {
      this.value = parseFloat(Math.round(num * 100) / 100).toFixed(2);
    }
  }

};


function setFocus(element, selected, time) {

  if ( typeof time === 'undefined' ) time = 300;
  if ( typeof selected === 'undefined' ) selected = false;

  window.setTimeout(function() {

    if ( selected ) element.focus().select();
    else element.focus();

  }, 300);

};

/*
* Evento que se ejecuta en todos los selects, permitiendo tener una interacción silimar al del .net
* en cuanto a la busqueda de datos al presionar teclas
*/
var select_keyPress = function(e) {

  window.clearTimeout(this.dataset.timeout);

  if ( typeof (this.dataset.search) == 'undefined') {
    this.dataset.search = '';
  }

  if (e.which !== 0) {
    this.dataset.search += String.fromCharCode(e.which);
  }

  for (var i = 0; i < this.options.length; i++) {
    var regex = new RegExp("^" + this.dataset.search, "i");
    var match = this.options[i].text.match(regex);
    var contains = (this.options[i].text.toLowerCase()).indexOf(this.dataset.search.toLowerCase()) != -1;

    if (match || contains) {

      var that = this,
					$that = $(that);

			if ( $(that).hasClass('no-keypress-change') ) {
				$that.children('option:eq(' + i + ')').prop({selected:true});
			} else {
				$that.children('option:eq(' + i + ')').prop({selected:true}).change();
			}

      this.dataset.timeout = window.setTimeout(function(){
        that.dataset.search = '';
        that.dataset.timeout = 0;
      }, 300);

      return;

    } else {

      var that = this;

			$(that).children('option:eq(0)').prop({selected:true}).change();

      this.dataset.timeout = window.setTimeout(function(){
        that.dataset.search = '';
        that.dataset.timeout = 0;
      }, 300);
    }
  }
};

$('select').on('keypress', select_keyPress);


/*
* INICIO DE EVENTOS QUE PERMITEN EJECUTAR LA BARRA DE PROGRESO EN BASE A LA CANTIDAD DE PETICIONES QUE
* HACE AL SERVIDOR, ESTAS FUNCIONES NO SE EJECUTAN CUANDO SE ACTUALIZA LA COOKIE
*/

function progressBar() {
  return $([
    '<div id="progress-data" class="progress">',
      '<div class="progress-bar" role="progressbar" style="width: 5%;">',
        'Cargando datos...',
      '</div>',
    '</div>'
  ].join(''));
}

function calculaPorcentaje() {
  return ((CountRequests * 100) / TotalRequests).toFixed(2);
}

var TotalRequests = 0,
    CountRequests = 0,
    showProgressBar = true;
    $progress = progressBar();

$.ajaxSetup({
  beforeSend: function(param1) {
    if ( showProgressBar ) TotalRequests += 1;
  },
  complete:function(param1) {

    if ( showProgressBar ) {

      CountRequests += 1;

      var porcent = '0%';

      if ( CountRequests > 5 ) {
        porcent = calculaPorcentaje() + '%';
      } else {
        porcent = '5%';
      }

      ($progress.children('div')).css({
        width: porcent
      }).text('Cargando datos... (' + porcent + ').');

    }

  }
});

$( document ).ajaxStart(function(param1) {

  if ( showProgressBar ) {

    if ( $('#progress-data').length == 0 ) {

      $progress = progressBar();

      $('body').append($progress);

    }

  }

});

$( document ).ajaxStop(function(param1) {

  if ( showProgressBar ) {

    ($progress.children('div')).css({
      width: '100%'
    }).text('Datos cargados satisfactoriamente. (100%)');

    window.setTimeout(function(){

      $progress.slideUp(300, function() {

        $progress.remove();

        TotalRequests = 0;
        CountRequests = 0;

      });

      $('.btn-first-disabled').prop({disabled:false});

    }, 1000);

  }

});
