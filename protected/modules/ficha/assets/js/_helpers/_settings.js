var settings = {

	moduleUrl : (Request.BaseUrl + '/' + Request.UrlHash.m + '/'),
	baseUrl : (Request.Host + Request.BaseUrl),

	growl : function(type, Options) {
		
		Options = Options || {};

		return $.extend({}, {
			ele: 'body',
		  type: type || 'success',
		  offset: {from: 'top', amount: 10},
		  align: 'center',
		  width: 400,
		  delay: 5000,
		  allow_dismiss: true,
		  stackup_spacing: 5
		}, Options);

	}

};