var ACTE = {};

ACTE.base_url = Request.BaseUrl + '/' + Request.UrlHash.m + '/';

ACTE.bootTable = {
	escape: false,
	locale: 'es-SP',
	search: true,
	pagination: true,
	pageSize: 10,
	idField: "nro_ficha"
};

ACTE.growl = function (type, element) {
	return {
		ele: element || 'body',
		type: type || null,
		offset: {from: 'top', amount: 10},
		align: 'center',
		width: 400,
		delay: 5000,
		allow_dismiss: true,
		stackup_spacing: 5
	};
};

ACTE.index = (function (_win, $) {

	'use strict';

	var $table = $('table#tdcacteconomica'),
			$cnt_table = $(".search-table"),
			$input_search_gen = $('input#buscador_generico'),
			$btn_search_gen = $('button#buscador_generico_btn');

	//CONSTRUCTOR
	var _bind = function () {

		$input_search_gen.on('change.ACTE.index.module', _change_format_search_input);

		_build_table();

	},

	_delete_ficha = function (e, value, row, index) {

		var $this = $(this);

		bootbox.confirm({
			title: '<i class="fa fa-exclamation-triangle" aria-hidden="true" style="color:orange"></i>&nbsp;Seguro que quieres eliminar?',
			message: "La ficha de actividad económica con el N°: <b>" + row.nro_ficha + "</b>, será eliminado de forma permanente.",
			callback: function (result) {

				if (result) {
					$this.prop({disabled: true});

					$.post(ACTE.base_url + 'principal/eliminarficha', {
							id_ficha: row.id_ficha,
							tipo: '03'
						}, function (response) {

						$.bootstrapGrowl('FICHA ELIMINADA', ACTE.growl('success'));

						$this.prop({disabled: false});

						if ( response.data.reload || response.data.reload == '' ) {

							_win.location.reload(true);

						} else {
							$table.bootstrapTable('refresh');
						}

					});
				}

			}
		});

	},

	_change_format_search_input = function () {

		if ( ! this.value.trim().length ) return false;

		this.value = this.value.trim().format('0000000');

	},

	/**
	 * bOTONES DE ACCION PARA CADA FILA
	 * @param  {string} value 'valor que tendrá el boton'
	 * @param  {object} row   Objeto que contiene todos los valores de la fila
	 * @param  {int} index Posición de la fila
	 * @return {string}       Cadena de botones para cada fila
	 */
	_action_buttons = function (value, row, index) {
		return [
			'<a href="' + (ACTE.base_url + 'acteconomica/view/ficha/' + row.id_ficha ) + '" class="btn-actions btn btn-sm" title="Visualizar datos de la ficha">',
				'<i class="fa fa-eye"></i>',
			'</a>',
			'<a href="' + (ACTE.base_url + 'acteconomica/modificar/ficha/' + row.id_ficha) + '" class="btn-actions edit btn btn-sm btn-info">',
			'<i class="fa fa-pencil"></i>',
			'</a>',
			'<button id="btn-delete-' + row.id_ficha + '" class="btn-actions delete btn btn-sm btn-danger" data-id="' + row.id_ficha + '">',
			'<i class="fa fa-times"></i>',
			'</button>'
		].join('');
	
	},

	/**
	 * cONSTRULLE TABLA PRINCIPAL(CABECERA, CUERPO), ATRAVEZ DE UNA CONSULTA JSON
	 * @return {VOID}
	 */
	_build_table = function () {

		var tableParams = $.extend(true, {}, ACTE.bootTable);

		tableParams.url = ACTE.base_url + 'principal/data/type/03';
		tableParams.sidePagination = "server";
		/*tableParams.responseHandler = function (res) {
		 console.log(res);
		 };*/

		//TABLE HEADER
		tableParams.columns = [
			{field: 'nro', title: 'N°'},
			{field: 'nro_ficha', title: 'N° Ficha', sortable: true},
			{field: 'id_ficha', title: 'ID Ficha', sortable: true},
			{field: 'code_catastro', title: 'Código Catastro', sortable: true},
			{field: 'estado_llenado', title: 'Estado'},
			{ field: 'fecha_ingreso', title: 'F. Ingreso' },
			{ field: 'ultima_modificacion', title: 'F. Ult. Modificación' },
			{
				field: 'action',
				title: ' Acciones',
				align: 'center',
				formatter: _action_buttons,
				events: {'click .delete': _delete_ficha}
			}
		];

		$table.bootstrapTable(tableParams);

		//CUSTOM SEARCH
		$input_search_gen.on("keyup, change", function () {
			$cnt_table.find('.search input').val(this.value).trigger("keyup");
		});

		$btn_search_gen.on("click", function () {
			$cnt_table.find('.search input').val($input_search_gen.val()).trigger("keyup");
		});
	
	};

	return {
		init: _bind
	};

}(window, window.jQuery));