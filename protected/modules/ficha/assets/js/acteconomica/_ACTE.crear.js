// @koala-append "tabs/_ACTE.crear.IDC.js"
// @koala-append "tabs/_ACTE.crear.AMF.js"
// @koala-append "tabs/_ACTE.crear.IC.js"

ACTE.crear = (function (_win, $) {

    'use strict';

    var data_general = {},
            setTimeIDs = [],
            action;

    var ID_Ficha = null,
            COD_Catastro = null,
            ID_persona = 0,
            datasend = {
                cod_ref_catastral: {}
            },
    default_ = {
        nro_ficha: '',
        nro_ficha_lote_first: '',
        nro_ficha_lote_second: '',
        cod_hoja_catastral: '',
        cod_ref_catastral: {
            ubigeo: '070101',
            sector: '',
            manzana: '',
            lote: '',
            edificio: '',
            entrada: '',
            piso: '',
            unidad: '',
            dc: ''
        }
    };

    var $cnt_ficha_header = $('div#cnt-ficha-header'),
            $btn_save_ficha = $('button[type=button]#btn-save-ficha'),
            $inputs = $cnt_ficha_header.find('input[type=text].ficha-header:not(:disabled)'),
            $inputs_required = $inputs.filter('[required=required]'),
            $btn_general = $('button#btn-general'),
            $btn_reset = $('button#btn-reset-ficha'),
            $tabs_control = $('ul#tabs-control'),
            $optionYear = $('ul#processYear');

    //CONSTRUCTOR
    var _bind = function () {

        var $ofa = $cnt_ficha_header.find('.exists_ficha');

        $optionYear.find('a.itemYear').off('click.system.module');
        $optionYear.find('a.itemYear').on('click.ACTE.crear.module', _not_change_year_proceso);

        //Cuando existe una cookie pero estas en modo crear
        if ($ofa.length > 0 && $ofa.filter('#action').val() == 'create') {

            action = $ofa.filter('#action').val();
            ID_Ficha = $ofa.filter('#ficha_id').val();
            COD_Catastro = $ofa.filter('#catastro_cod').val();
            ID_persona = $ofa.filter('#persona').val();

            bootbox.confirm({
                title: 'Se detectó una antigua ficha',
                message: ([
                    "El sistema a detectado que existe una ficha que no ha sido completada:<br /><br />",
                    "<b>OK</b> para cargar los datos de la anterior ficha.<br />",
                    "<b>CANCELAR</b> para generar una nueva ficha."
                ].join('')),
                callback: function (result) {

                    if (!result) {

                        _reset_ficha();

                    } else {

                        _continue_bind(false);

                        _InitModules(
                                $ofa.filter('#ficha_id').val(),
                                $ofa.filter('#catastro_cod').val(),
                                $ofa.filter('#sector_cod').val(),
                                $ofa.filter('#ubigeo_cod').val()
                                );

                        //HABILITAR EVENTOS PARA QUE SE SIGA GUARDANDO EN LA COOKIE ALGUNOS DATOS DE LA CABECERA
                        $inputs.filter('.aftergenerate').on('change.ACTE.crear.module', _savedata_to_cookie);

                        //HABILITA LAS PESTAÑAS
                        $('div#cnt-data').removeClass('disabled');

                        //BAJAR LA VISTA Y POSICIONAR EL FOCO EN 
                        //EL PRIMER ELEMENTO DE LA PRIMERA PESTAÑA
                        var $elem = $('#' + ($tabs_control.find('li.active a')).data('firstelement'));


                        $('html, body').animate({scrollTop: $elem.offset().top}, 400);

                        _win.setTimeout(function () {
                            $elem.focus();
                        }, 420);

                    }
                }
            });

            //Cuando existe una cookie y está en modo actualizar (Por defecto el modo actualizar chanca toda la información)
            //de cualquier cookie que se encuentre editando
        } else if ($ofa.length > 0 && $ofa.filter('#action').val() == 'update') {

            ID_Ficha = $ofa.filter('#ficha_id').val();
            COD_Catastro = $ofa.filter('#catastro_cod').val();
            action = $ofa.filter('#action').val();
            ID_persona = $ofa.filter('#persona').val();

            _continue_bind(false);

            //HABILITAR EVENTOS PARA QUE SE SIGA GUARDANDO EN LA COOKIE ALGUNOS DATOS DE LA CABECERA
            $inputs.filter('.aftergenerate').on('change.ACTE.crear.module', _savedata_to_cookie);

            _InitModules(
                    ID_Ficha,
                    COD_Catastro,
                    $ofa.filter('#sector_cod').val(),
                    $ofa.filter('#ubigeo_cod').val()
                    );

            //HABILITA LAS PESTAÑAS
            $('div#cnt-data').removeClass('disabled');

            //BAJAR LA VISTA Y POSICIONAR EL FOCO EN 
            //EL PRIMER ELEMENTO DE LA PRIMERA PESTAÑA
            var $elem = $('#' + ($tabs_control.find('li.active a')).data('firstelement'));

            $('html, body').animate({scrollTop: $elem.offset().top}, 400);

            _win.setTimeout(function () {
                $elem.focus();
            }, 420);

            //FICHA LIMPIA
        } else {
            action = 'create';
            _continue_bind(true);
        }

    },
            _continue_bind = function (native) {

                shortcut.add('Ctrl+Right', _move_to_next_tab);
                shortcut.add('Ctrl+Left', _move_to_preview_tab);

                $inputs.filter('.only-number').on('keydown.ACTE.crear.module', only_number);
                $inputs.filter('.only-number').on('keydown.ACTE.crear.module', verify_number);
                $inputs.filter('.formater:not(#txtnro_ficha)').on('focusout.ACTE.crear.module', _agregar_valor_default);
                $inputs.filter('.formater').on('change.ACTE.crear.module', _change_formater);
                $inputs.filter('.datasend').on('change.ACTE.crear.module', _watcher_changes);
                $tabs_control.find('a[data-toggle="tab"]').on('shown.bs.tab', _shown_tabs);

                $btn_general.on('click.ACTE.crear.module', _save_all_data);
                $btn_reset.on('click.ACTE.crear.module', _before_reset_ficha);

                events.on('savedata', _watcher_change_data);

                if (native) {
                    $btn_save_ficha.on('click.ACTE.crear.module', _submit_generate_ficha);
                    $inputs_required.filter('input#txtsector').on('change.ACTE.crear.module', _verify_sector);
                    $inputs_required.filter('input#txtnro_ficha').on('focusout.ACTE.crear.module', _submit_generate_ficha);
                }

            },
            _not_change_year_proceso = function () {

                bootbox.alert({
                    title: '<i class="fa fa-ban" aria-hidden="true" style="color:red"></i>&nbsp;Cambio año proceso',
                    message: "Mientras se esta creando o actualizando una ficha, el cambio de año de proceso esta deshabilitado",
                    callback: function () {}
                });

                e.stopImmediatePropagation();
                return false;

            },
            _before_reset_ficha = function () {

                bootbox.confirm({
                    title: '<i class="fa fa-exclamation-triangle" style="color:orange" aria-hidden="true"></i>&nbsp;Atención!',
                    message: ([
                        "AL REINICIAR LA FICHA, TODOS LOS DATOS TEMPORALMENTE ALMACENADOS SERÁN ELIMINADOS.<br />",
                        "PRESIONA <b>OK</b>, PARA CONTINUAR.",
                    ].join('')),
                    callback: function (result) {
                        if (result)
                            _reset_ficha();
                    }
                });

            },
            _reset_ficha = function () {

                var $this = $(this);

                $this.prop({disabled: true});

                $.post(ACTE.base_url + 'acteconomica/resetearficha', function (response) {

                    if (!response.error) {

                        if (action == 'create') {
                            $.bootstrapGrowl("La página se está actualizando.", ACTE.growl('warning'));
                            location.reload(true);
                        } else {
                            $.bootstrapGrowl("Redireccionando a la vista principal, espere un momento...", ACTE.growl('warning'));
                            location.href = ACTE.base_url + 'acteconomica';
                        }

                    }

                });

            },
            _watcher_changes = function () {

                var $this = $(this),
                        name = $this.attr('name'),
                        parent = $this.data('parent');

                if (typeof (parent) === 'undefined') {
                    datasend[name] = $.trim($this.val());
                } else {
                    datasend[parent][name] = $.trim($this.val());
                    _calculaDC();
                }

            },
            _watcher_change_data = function (DATA) {

                var key = (Object.keys(DATA))[0];

                if (typeof (setTimeIDs[key]) !== 'undefined') {
                    _win.clearTimeout(setTimeIDs[key]);
                    delete setTimeIDs[key];
                }

                setTimeIDs[key] = _win.setTimeout(function () {

                    //ENVIALO A LA COOKIE
                    (function (D, K) {

                        data_general[K] = D[K];

                        //Variable se encuentra en el archivo _helpers/_base.js
                        //Evita que la barra de progreso se muestra cada ves
                        //que se cambia la información de la cookie
                        showProgressBar = false;

                        $.post(ACTE.base_url + 'acteconomica/asignardato', D)
                                .always(function () {

                                    //Cabiamos la visibilidad del indicador del progress
                                    showProgressBar = true;

                                    delete setTimeIDs[K];

                                });

                    })(DATA, key);

                }, 1000);

                if (key == 'id_persona') {
                    ID_persona = DATA[key];
                }

            },
            /**
             * Evento que se ejecuta despues de haber generado la ficha y que permite
             * guardar datos de la cabecera a la cookie
             * @return {void}
             */
            _savedata_to_cookie = function () {

                var $this = $(this),
                        name = $this.attr('name'),
                        data = {};

                data[name] = $.trim($this.val());

                _watcher_change_data(data);

            },
            _save_all_data = function () {
                $("#btn-general").attr('disabled', 'disabled');
                if (ID_Ficha == null) {
                    $.bootstrapGrowl("La ficha no esta asignada, proceso cancelado", ACTE.growl('danger'));
                    document.getElementById('txtnro_ficha').focus();
                    return false;
                }

                if (!_is_valid_data()) {
                    return false;
                }


                window.setTimeout(function () {
                    bootbox.confirm({
                        title: 'Confirmas guardado de datos?',
                        message: "Antes de continuar, debe estar seguro que la información proporcionada sea la correcta.<br/><br/>&nbsp;&nbsp;- Presione <b>OK</b> para continuar.",
                        callback: function (result) {

                            if (result) {
                                $btn_save_ficha.prop({disabled: true}).attr({disabled: true});
                                $btn_reset.prop({disabled: true}).attr({disabled: true});

                                $.post(ACTE.base_url + 'acteconomica/guardardatosficha', data_general, function (response) {

                                    if (!response.error) {

                                        $.bootstrapGrowl(response.message, ACTE.growl('success'));

                                        if (action == 'create') {

                                            _win.setTimeout(function () {

                                                $.bootstrapGrowl('Recargando la página...', ACTE.growl('info'));

                                                _win.setTimeout(function () {
                                                    _win.location.reload(true)
                                                }, 2000);

                                            }, 2000);

                                        } else {

                                            $.bootstrapGrowl('Recargando la página...', ACTE.growl('info'));
                                            _win.setTimeout(function () {
                                                _win.location.href = ACTE.base_url + 'acteconomica'
                                            }, 2000);

                                        }


                                    } else {
                                        $.bootstrapGrowl(response.message, ACTE.growl('danger'));
                                        $btn_save_ficha.prop({disabled: false}).attr({disabled: false});
                                        $btn_reset.prop({disabled: false}).attr({disabled: false});
                                    }

                                });
                            } else {
                                $("#btn-general").removeAttr('disabled');
                            }
                        }
                    });
                }, 500); // 0.5 seconds expressed in milliseconds

            },
            _is_valid_data = function () {

                if (ID_persona == 0) {
                    $.bootstrapGrowl("Debe buscar un conductor", ACTE.growl('danger'));
                    $tabs_control.find('[href="#tab_identconductor"]').tab('show');
                    document.getElementById('txtnro_documento').focus();
                    return false;
                }
                ;

                //COMPRABAR EL ESTADO DE LLENADO
                var cbotdoc = document.getElementById('cbotipo_documento');
                if (cbotdoc.selectedIndex == 0) {
                    $.bootstrapGrowl("Es necesario seleccionar un tipo de documento", ACTE.growl('danger'));
                    $tabs_control.find('[href="#tab_identconductor"]').tab('show');
                    cbotdoc.focus();
                    return false;
                }

                //COMPRABAR EL ESTADO DE LLENADO
                var cboubigeo = document.getElementById('cbodistrito');
                if (cboubigeo.selectedIndex == 0) {
                    $.bootstrapGrowl("Es necesario seleccionar el lugar de procedencia", ACTE.growl('danger'));
                    $tabs_control.find('[href="#tab_identconductor"]').tab('show');
                    cboubigeo.focus();
                    return false;
                }

                //COMPRABAR EL ESTADO DE LLENADO
                var cboestado = document.getElementById('cboestado_llenado');
                if (cboestado.selectedIndex == 0) {
                    $.bootstrapGrowl("Es necesario seleccionar el estado de llenado", ACTE.growl('danger'));
                    $tabs_control.find('[href="#tab_infcomplementaria"]').tab('show');
                    cboestado.focus();
                    return false;
                }

                //SI EL CHECKBOX DEL DECLARANTE ESTA ACTIVO SE REQUIERE 
                //QUE LA FIRMA Y LA FECHA ESTEN COMPLETADOS
                var chkdeclarante = document.getElementById('chkdeclarante');
                if (chkdeclarante.checked) {

                    var txtdocidentidad = document.getElementById('txtdni_declarante'),
                            txtfechadeclara = document.getElementById('txtfecha_declarante');

                    if (txtdocidentidad.value.isEmpty()) {
                        $.bootstrapGrowl("Ingrese el numero de documento", ACTE.growl('danger'));
                        $tabs_control.find('[href="#tab_infcomplementaria"]').tab('show');
                        txtdocidentidad.focus();
                        return false;
                    }

                    if (txtfechadeclara.value.isEmpty()) {
                        $.bootstrapGrowl("Ingrese la fecha en la que se firmó.", ACTE.growl('danger'));
                        $tabs_control.find('[href="#tab_infcomplementaria"]').tab('show');
                        txtfechadeclara.focus();
                        return false;
                    }

                }

                //LA FIRMA DEL SUPERVISOR ES REQUERIDA SE DEBE ACTIVAR OBLIGATORIAMENTE Y
                //VERIFICAR QUE SU FIRMA Y FECHA ESTEN AGREGADAS
                var chksupervisor = document.getElementById('chksupervisor');
                if (!chksupervisor.checked) {

                    /*$.bootstrapGrowl("Es necesario la firma del supervisor", ACTE.growl('danger'));
                     $tabs_control.find('[href="#tab_infcomplementaria"]').tab('show');
                     chksupervisor.focus();
                     return false;*/

                } else {

                    var txtcodsupervisor = document.getElementById('txtcod_supervisor'),
                            txtfechasupervisor = document.getElementById('txtfecha_supervisor');

                    if (txtcodsupervisor.value.isEmpty()) {
                        $.bootstrapGrowl("Ingrese el Codigo del supervisor", ACTE.growl('danger'));
                        $tabs_control.find('[href="#tab_infcomplementaria"]').tab('show');
                        txtcodsupervisor.focus();
                        return false;
                    }

                    if (txtfechasupervisor.value.isEmpty()) {
                        $.bootstrapGrowl("Ingrese la fecha en la que se firmó.", ACTE.growl('danger'));
                        $tabs_control.find('[href="#tab_infcomplementaria"]').tab('show');
                        txtfechasupervisor.focus();
                        return false;
                    }

                }

                //LA FIRMA DEL TECNICO CATASTRAL ES REQUERIDA SE DEBE ACTIVAR OBLIGATORIAMENTE Y
                //VERIFICAR QUE SU FIRMA Y FECHA ESTEN AGREGADAS
                var chktec_catastral = document.getElementById('chktec_catastral');
                if (!chktec_catastral.checked) {

                    /*$.bootstrapGrowl("Es necesario la firma del técnico catastral", ACTE.growl('danger'));
                     $tabs_control.find('[href="#tab_infcomplementaria"]').tab('show');
                     chktec_catastral.focus();
                     return false;*/

                } else {

                    var txtcodtec_catastral = document.getElementById('txtcod_tec_catastral'),
                            txtfechatec_catastral = document.getElementById('txtfecha_tec_catastral');

                    if (txtcodtec_catastral.value.isEmpty()) {
                        $.bootstrapGrowl("Ingrese el Codigo del Tecnico catastral", ACTE.growl('danger'));
                        $tabs_control.find('[href="#tab_infcomplementaria"]').tab('show');
                        txtcodtec_catastral.focus();
                        return false;
                    }

                    if (txtfechatec_catastral.value.isEmpty()) {

                        $.bootstrapGrowl("Ingrese la fecha en la que se firmo.", ACTE.growl('danger'));
                        $tabs_control.find('[href="#tab_infcomplementaria"]').tab('show');
                        txtfechatec_catastral.focus();
                        return false;

                    }

                }

                //LA FIRMA DEL TECNICO DE CALIDAD ES REQUERIDA SE DEBE ACTIVAR OBLIGATORIAMENTE Y
                //VERIFICAR QUE SU FIRMA Y FECHA ESTEN AGREGADAS
                var chktec_calidad = document.getElementById('chktec_calidad');
                if (!chktec_calidad.checked) {

                    /*$.bootstrapGrowl("Es necesario la firma del técnico de calidad", ACTE.growl('danger'));
                     $tabs_control.find('[href="#tab_infcomplementaria"]').tab('show');
                     chktec_calidad.focus();
                     return false;*/

                } else {

                    var txtcodtec_calidad = document.getElementById('txtcod_tec_calidad'),
                            txtfechatec_calidad = document.getElementById('txtfecha_tec_calidad');

                    if (txtcodtec_calidad.value.isEmpty()) {
                        $.bootstrapGrowl("Ingrese el Codigo del Tecnico Calidad", ACTE.growl('danger'));
                        $tabs_control.find('[href="#tab_infcomplementaria"]').tab('show');
                        txtcodtec_calidad.focus();
                        return false;
                    }

                    if (txtfechatec_calidad.value.isEmpty()) {

                        $.bootstrapGrowl("Ingrese la fecha en la que se firmo.", ACTE.growl('danger'));
                        $tabs_control.find('[href="#tab_infcomplementaria"]').tab('show');
                        txtfechatec_calidad.focus();
                        return false;

                    }

                }

                //SI EL CHECKBOX DEL VERIFICADOR CATASTRAL ESTA ACTIVO SE REQUIERE 
                //QUE LA FIRMA Y LA FECHA ESTEN COMPLETADOS
                var chkverificador = document.getElementById('chkverif_catastral');
                if (chkverificador.checked) {

                    var txtcodverificador = document.getElementById('txtcod_verif_catastral'),
                            txtfechaverificador = document.getElementById('txtfecha_verif_catastral');

                    if (txtcodverificador.value.isEmpty()) {
                        $.bootstrapGrowl("Ingrese el codigo del vericador castastral", ACTE.growl('danger'));
                        $tabs_control.find('[href="#tab_infcomplementaria"]').tab('show');
                        txtcodverificador.focus();
                        return false;
                    }

                    if (txtfechaverificador.value.isEmpty()) {
                        $.bootstrapGrowl("Ingrese la fecha en la que se firmó.", ACTE.growl('danger'));
                        $tabs_control.find('[href="#tab_infcomplementaria"]').tab('show');
                        txtfechaverificador.focus();
                        return false;
                    }

                }

                return true;

            },
            _submit_generate_ficha = function () {

                if ($.trim($inputs_required.filter('#txtnro_ficha').val()) == '')
                    return false;

                datasend = $.extend(true, default_, datasend);

                if (_valid_requerid()) {
                    $.bootstrapGrowl("Complete el campo marcado", ACTE.growl('danger', $cnt_ficha_header));
                    return false;
                }

                bootbox.confirm({
                    title: '<i class="fa fa-info-circle" style="color:#2196F3" aria-hidden="true"></i>&nbsp;Atención!',
                    message: "Se procederá a ingresar la ficha castastral de cotitularidad con el N° <b>" + datasend.nro_ficha + "</b>",
                    callback: function (result) {

                        if (result) {

                            $btn_save_ficha.prop({disabled: true});
                            $inputs_required.prop({disabled: true});

                            $.post(ACTE.base_url + 'acteconomica/generarficha', datasend, function (response) {

                                if (!response.error) {

                                    //REGISTRANDO EL ID DE FICHA Y EL CODIGO CATASTRO QUE ENVIA EL SERVIDOR
                                    ID_Ficha = response.id_ficha;
                                    COD_Catastro = response.cod_catastro;

                                    $.bootstrapGrowl(response.message, ACTE.growl(((response.error) ? 'danger' : 'success')));
                                    //INICIANDO TODOS LOS MODULOS QUE CONTROLAN
                                    //LA ACCION DE CADA PESTAÑA
                                    _InitModules();

                                    //HABILITAR EVENTOS PARA QUE SE SIGA GUARDANDO EN LA COOKIE ALGUNOS DATOS DE LA CABECERA
                                    $inputs.filter('.aftergenerate').on('change.ACTE.crear.module', _savedata_to_cookie);

                                    //REMOVER EVENTOS POR MAYOR SEGURIDAD
                                    $btn_save_ficha.off('click.ACTE.crear.module');
                                    $inputs_required.filter('input#txtsector').off('change.ACTE.crear.module');
                                    $inputs_required.filter('input#txtnro_ficha').off('focusout.ACTE.crear.module');

                                    //HABILITA LAS PESTAÑAS
                                    $('div#cnt-data').removeClass('disabled');

                                    $inputs.filter('#txtnfl1').val('').change();
                                    setFocus($inputs.filter('#txtnfl1'));

                                } else {

                                    $.bootstrapGrowl(response.message, ACTE.growl('danger'));

                                    $btn_save_ficha.prop({disabled: true});
                                    $inputs_required.prop({disabled: false});

                                    setFocus($inputs_required.filter('#txtsector', true));

                                }

                            }).fail(function (jqXHR, textStatus, errorThrown) {

                                $.bootstrapGrowl(errorThrown, ACTE.growl('danger', $cnt_ficha_header));

                                $btn_save_ficha.prop({disabled: true});
                                $inputs_required.prop({disabled: false});

                                setFocus($inputs_required.filter('#txtsector', true));

                            });
                        }

                    }
                });

            },
            _agregar_valor_default = function () {
                if (this.value.trim() == '') {
                    this.value = 1;
                    $(this).change();
                }
            },
            /**
             * Busca el formato para el input asignado en su data
             * @return {void}
             */
            _change_formater = function () {

                if (this.value.trim() == '')
                    return false;

                var $this = $(this);

                this.value = (this.value.trim()).format($this.data('formater'));

            },
            _calculaDC = function () {

                var $items = $inputs.filter('.cdc'),
                        acum = 0;

                $.each($items, function (index, el) {
                    var $el = $(el);
                    if ($el.attr('name') == 'ubigeo') {
                        acum += (parseInt($el.val().substr(0, 2)) + parseInt($el.val().substr(2, 2)) + parseInt($el.val().substr(4, 2)));
                    } else {
                        acum += ($.trim($el.val()).length == 0 ? 0 : parseInt($.trim($el.val())));
                    }
                });

                $cnt_ficha_header.find('input[type=text]#txtdc').val((acum % 9));
                datasend.cod_ref_catastral.dc = (acum % 9);

            },
            _valid_requerid = function () {

                var error = false;

                $.each($inputs_required, function (index, el) {

                    var $el = $(el);

                    if (($el.val()).length == 0 || parseInt($el.val()) == 0) {

                        $el.css({borderColor: 'red'});
                        error = true;
                        $el.focus();
                        return false;

                    } else {
                        $el.removeAttr('style');
                    }

                });

                return error;

            },
            _verify_sector = function () {

                var $this = $(this),
                        val = $.trim($this.val());

                if (val == '') {
                    $this.val('1').change();
                    return false;
                }

                var data = {
                    sector: val,
                    ubigeo: default_.cod_ref_catastral.ubigeo
                };

                $this.prop({disabled: true});
                $btn_save_ficha.prop({disabled: true});

                $.post(ACTE.base_url + 'principal/verifysector', data, function (reponse) {

                    if (reponse.error) {
                        $.bootstrapGrowl(reponse.message, ACTE.growl('danger', $cnt_ficha_header));
                        $this.css({borderColor: 'red'}).val('').attr({title: reponse.message});
                        datasend.cod_ref_catastral[$this.attr('name')] = '';
                    } else {
                        $this.removeAttr('style', 'title');
                    }

                    $this.prop({disabled: false});
                    $btn_save_ficha.prop({disabled: false});

                });

            },
            _InitModules = function (id_ficha, cod_catastro, cod_sector, _ubigeo_) {

                var ficha = id_ficha || ID_Ficha,
                        catastro = cod_catastro || COD_Catastro,
                        sector = cod_sector || datasend.cod_ref_catastral.sector,
                        ubigeo = _ubigeo_ || datasend.cod_ref_catastral.ubigeo;

                ACTE.crear.IDC.init(ficha, catastro, sector, ubigeo);
                ACTE.crear.AMF.init(ficha, catastro);
                ACTE.crear.IC.init(ficha, catastro);

                //INICIAMOS EL MODULO DE OBSERVACIONES SOLO PARA EL CASO DE CREACIÓN
                if (action == 'create') {
                    Observaciones.init(ficha, '03');
                }

            },
            _move_to_preview_tab = function () {

                var $li_activated = $tabs_control.find('li.active'),
                        $link = $li_activated.children('a');

                $tabs_control.find('a[href="' + $link.data('preview') + '"]').tab('show');

            },
            _move_to_next_tab = function () {

                var $li_activated = $tabs_control.find('li.active'),
                        $link = $li_activated.children('a');

                $tabs_control.find('a[href="' + $link.data('next') + '"]').tab('show');

            },
            _shown_tabs = function (e) {
                var $this = $(this);

                document.getElementById($this.data('firstelement')).focus();
            };

    return {
        init: _bind
    };

}(window, window.jQuery)); 