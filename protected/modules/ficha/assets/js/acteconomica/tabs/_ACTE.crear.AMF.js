// @koala-append "Autorizacion Municipal de F/_ACTE.crear.AMF.actividad.js"
// @koala-append "Autorizacion Municipal de F/_ACTE.crear.AMF.anuncio.js"

/**
 * MODULO PARA INFORMACION COMPLEMENTARIA
 */
ACTE.crear.AMF = (function (_win, $) {

	'use strict';

	var ID_Ficha = null,
			COD_Catastro = null;

	var $cnt_dp = $('div#tab_amfuncionamiento'),
			$inputs = $cnt_dp.find('input');

	//CONSTRUCTOR
	var _bind = function (ficha, catastro) {

		ID_Ficha = ficha,
		COD_Catastro = catastro;

		$inputs.filter('.maskdate').inputmask();

		$inputs.filter('input[type=text].only-number').on('keydown.ACTE.crear.AMF.module', only_number);
		$inputs.filter('input[type=text].only-number').on('change.ACTE.crear.AMF.module', verify_number);

		$inputs.filter('.calcule').on('change.ACTE.crear.AMF.module', _calcule_areas);

		$inputs.filter('.datasend').on('change.ACTE.crear.AMF.module', _watcher_general);
		$cnt_dp.find('#btn-add-anuncio').on('keydown.ACTE.crear.AMF.module', _open_nex_tab);

		ACTE.crear.AMF.actividad.init(
			ficha, 
			$cnt_dp.find('table#td-actividad'), 
			$cnt_dp.find('button#btn-add-actividad')
		);

		ACTE.crear.AMF.anuncio.init(
			ficha, 
			$cnt_dp.find('table#td-anuncio'), 
			$cnt_dp.find('button#btn-add-anuncio')
		);

	},

	_watcher_general = function () {

		var $this = $(this),
				name = $this.attr('name'),
				type = $this.attr('type'),
				tag = $this.prop("tagName").toLowerCase(),
				data = {};

		if ((tag == 'input' && type == 'text') || tag == 'textarea') {

			var value = this.value.trim();

			if ($this.hasClass('maskdate')) {
				if (!Inputmask.isValid(value, {alias: "dd/mm/yyyy"})) {
					value = '01/01/1990';
					$this.val(value);
				} else {
					$this.removeAttr('style');
				}
			}

			data[name] = value;

		}

		events.emit('savedata', data);

	},

	_open_nex_tab = function(e) {

		if ( e.key == "Tab" && e.keyCode == 9 ) {
			$('#tabs-control').find('a[href="#tab_infcomplementaria"]').tab('show');
			e.preventDefault();
			return false;
		}

	},

	_calcule_areas = function() {
		var $this = $(this),
				data = $this.data(),
				total = 0;

		$.each($inputs.filter('[data-group="'+data.group+'"]'), function(index, element) {
			total += (!element.value.trim()) ? 0 : parseFloat(element.value.trim()); 
		});

		if ( data.group == 'autorizada' ) {
			$inputs.filter('#txttotal_autorizada').val(total);
			_win.setTimeout(function(){$inputs.filter('#txttotal_autorizada').change()}, 300);
		} else {
			$inputs.filter('#txttotal_verificada').val(total).change();
			_win.setTimeout(function(){$inputs.filter('#txttotal_verificada').change()}, 300);
		}
	};

	return {
		init: _bind
	};

}(window, window.jQuery));