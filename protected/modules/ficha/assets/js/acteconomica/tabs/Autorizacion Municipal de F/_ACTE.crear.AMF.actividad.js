/**
 * SUBMODULO PARA EL MANEJO DE ACTIVIDADES
 */
ACTE.crear.AMF.actividad = (function(_win, $){

	'use strict';

	var ID_Ficha;

	var $table 		= $('<table />'),
			$btn_add 	= $('<button />'),
			$md_mng 	= $('section#md-buscar'),
			$table_s 	= $md_mng.find('table#dt-buscar'),
			$bb_mng 	= {};

	var _bind = function(_ficha_, _table_, _btnactivator_) {

		ID_Ficha 	= _ficha_;
		$table 		= _table_;
		$btn_add 	= _btnactivator_;

		_build_table();

		$btn_add.on('click.ACTE.crear.AMF.actividad.module', _open_modal);

	},

	_open_modal = function() {
		$bb_mng = $md_mng.cBootbox({
			title: "Actividades",
			btnClose: '#btn-cancel',
			afterClose: _afterClose_modal,
			beforeOpen: function() {

				_build_table_s();

				$md_mng.find('button#btn-done').prop({disabled:false});

				$md_mng.find('button#btn-done').on('click.ACTE.crear.IDC.buscar.module', _click_selection);

			}
		});
	},

	_dblClick_selection = function(row) {

		var resource = ACTE.base_url + 'acteconomica/mantenimientoactividad',
				postParams = {
					id: row.codigo,
					ficha: ID_Ficha,
					action: 'create'
				};

		$.post(resource, postParams, function(response) {

			if ( ! response.error ) {

				$.bootstrapGrowl(response.message, ACTE.growl('success'));
				
				$table.bootstrapTable('refresh');
				
				$md_mng.find('button#btn-done').prop({ disabled: false });
				
				$bb_mng.find('button#btn-cancel').click();

			} else {

				$md_mng.find('button#btn-done').prop({disabled:false});

				$.bootstrapGrowl(response.message, ACTE.growl('danger'));

				$md_mng.find('button#btn-done').focus();

			}

		});

	},

	_click_selection = function() {

		var selection = $table_s.bootstrapTable('getSelections');

		if ( selection.length >  0 ) {
			_dblClick_selection(selection[0]);
		} else {
			$bb_mng.find('button#btn-cancel').click();
		}

	},

	_afterClose_modal = function() {
		$table_s.bootstrapTable('destroy');
	},

	/**
	 * Evento que elimina una fila de la tabla
	 * @param  {object} e     Instancia del evento
	 * @param  {string} value Valor que contiene el elemento
	 * @param  {object} row   Objeto con todo los datos de la fila seleccionada
	 * @param  {int} index Posición de la fila en la tabla
	 * @return {[type]}       [description]
	 */
	_delete_row = function(e, value, row, index) {

		var $this = $(this),
				postParams = {},
				resource = ACTE.base_url + 'acteconomica/mantenimientoactividad';

		bootbox.confirm({
			title: 'Seguro que quieres eliminar?',
			message: "La actividad: <strong>"+row.nombre+"</strong>, será eliminado de forma permanente.",
			callback: function(result) {

				if ( result ) {

					if ( ID_Ficha == null ) {
						$.bootstrapGrowl("La ficha no está asignada, el proceso a sido cancelado", ACTE.growl('warning'));
						return false;
					} else {
						postParams.ficha = ID_Ficha;
					}

					postParams.id = row.codigo;
					postParams.action = 'delete';

					$this.prop({disabled:true});

					$.post(resource, postParams, function(response) {
						if ( ! response.error ) {

							$.bootstrapGrowl(response.message, ACTE.growl('success'));
							$table.bootstrapTable('refresh');
							$this.prop({ disabled: false });

						} else {

							$this.prop({disabled:false});
							$.bootstrapGrowl(response.message, ACTE.growl('danger'));

						}
					});

				}
			}
		});

	},

	/**
	 * Contiene los botones de opciones para cada fila
	 * @param  {string} value Valor que contiene el elemento
	 * @param  {object} row   Objeto con todo los datos de la fila seleccionada
	 * @param  {int} index Posición de la fila en la tabla
	 * @return {string} HTML de los botones dibujados
	 */
	_action_buttons = function(value, row, index) {
    return [
			'<button id="btn-delete-'+row.codigo+'" class="btn-actions delete btn btn-sm" data-id="'+row.codigo+'" data-accion="delete" tabindex="-1">',
				'<i class="fa fa-trash-o" aria-hidden="true"></i>',
			'</button>'
		].join('');
  },

  /**
   * Carga la tabla con todos los datos
   * @return {void}
   */
	_build_table = function() {

		var bootTable = {
			escape: false,
		  locale: 'es-SP',
		  idField: "codigo",
		  url: ACTE.base_url + 'acteconomica/getactividadesficha/id/' + ID_Ficha,
		  onPostHeader: function(e){
				$table.find('tr th').removeAttr('tabindex');
		  }
		};

  	//TABLE HEADER
		bootTable.columns = [
			{ 
				field: 'action', 
				title: '<i class="fa fa-cog" aria-hidden="true"></i>', 
				align: 'center', 
				class: 'config-width',
				formatter : _action_buttons, 
				events : {
	        'click .delete': _delete_row
    		}
    	},
			{ field: 'codigo', title: 'Codigo', align: 'center', class: 'ds-width' },
			{ field: 'nombre', title: 'Desc. Actividad', align: 'left' }
		];

		$table.bootstrapTable(bootTable);

	},

	_build_table_s = function() {

		var bootTable = {
		  locale: 'es-SP',
		  search: true,
		  selectItemName: 'actividades',
		  clickToSelect: true,
		  idField: "codigo",
		  height: 410,
		  pagination:true,
		  url: ACTE.base_url + 'acteconomica/getactividades',
		  onDblClickRow: _dblClick_selection,
		  onPostHeader: function(){
		  	($bb_mng.find('.search')).children('input').focus().select();
		  	$table_s.find('tr th').removeAttr('tabindex');
		  }
		};
  	//TABLE HEADER
		bootTable.columns = [
			{ field: 'state', radio: true, align: 'center' },
			{ field: 'codigo', title: 'Codigo', align: 'center' },
			{ field: 'actividad', title: 'Nombre de actividad', align: 'left' },
		];

		$table_s.bootstrapTable(bootTable);

	};

	return {
		init: _bind
	};

}(window, window.jQuery));