/**
 * SUBMODULO PARA EL MANEJO DE ACTIVIDADES
 */
ACTE.crear.AMF.anuncio = (function(_win, $){

	'use strict';

	var ID_Ficha;

	var datasend = {},
			default_ = {
				nro: 0,
				ficha: '',
				codanuncio: '',
				nro_lados: '',
				area_autorizada: '',
				area_verificada: '',
				nro_expediente: '',
				nro_licencia: '',
				f_expedicion: '01/01/1900',
				f_vencimiento: '01/01/1900',
				action: ''
			};

	var $table 		= $('<table />'),
			$btn_add 	= $('<button />'),
			$md_mng 	= $('section#md-anuncio'),
			$inputs   = $md_mng.find('input'),
			$bb_mng 	= {};

	var _bind = function(_ficha_, _table_, _btnactivator_) {

		ID_Ficha 	= _ficha_;
		$table 		= _table_;
		$btn_add 	= _btnactivator_;

		_build_table();

		$btn_add.on('click.ACTE.crear.AMF.anuncio.module', _open_modal);

	},

	_watcher_changes = function() {

		var $this = $(this),
				value = this.value.trim(),
				name 	= $this.attr('name'),
				data 	= {};

		if ( $this.hasClass('maskdate') ) {
			if ( ! Inputmask.isValid(value, { alias: "dd/mm/yyyy" }) ) {
				value = '01/01/1990';
				$this.val(value);
			} else {
				$this.removeAttr('style');
			}
		}

		data[name] = value;
		datasend[name] = value;

		events.emit('savedata', data);

	},

	_submit_data = function() {

		var $this = $(this);

		datasend = $.extend({}, default_, datasend);

		if ( ID_Ficha == null ) {
			$.bootstrapGrowl("La ficha no está asignado, el proceso a sido cancelado", ACTE.growl('warning'));
			_win.location.reload(true);
			return false;
		} else {
			datasend.ficha = ID_Ficha;
		}

		if ( datasend.codanuncio == '' ) {
			$.bootstrapGrowl("Este dato es requerido", ACTE.growl('danger'));
			$inputs.filter('#txtcodanuncio').focus();
			return false;
		}

		if ( datasend.nro_lados == '' ) {
			$.bootstrapGrowl("Este dato es requerido", ACTE.growl('danger'));
			$inputs.filter('#txtnro_lados').focus();
			return false;
		}

		if ( datasend.area_verificada == '' ) {
			$.bootstrapGrowl("Este dato es requerido", ACTE.growl('danger'));
			$inputs.filter('#txtareaverif').focus();
			return false;
		}

		$this.prop({disabled:true});

		$.post(ACTE.base_url + 'acteconomica/mantenimientoanuncios', datasend, function(response) {
			
			if ( ! response.error ) {

				$.bootstrapGrowl(response.message, ACTE.growl('success'));

				if ( datasend.action == 'create' ) {
					
					$md_mng.find('input[type=text]').val('');
					
					datasend = {};
					datasend.action = 'create';
					
					$this.prop({disabled:false});

					$md_mng.find('#txtcodanuncio').focus();
				
				} else {
					$md_mng.find('button#btn-close').click();
				}

				$table.bootstrapTable('refresh');

			} else {

				$.bootstrapGrowl(response.message, ACTE.growl('danger'));

				$this.prop({disabled:false});

			}

		});
		
	},

	search_anuncio = function() {

		var $this = $(this),
				code = this.value.trim(),
				resource = ACTE.base_url + 'acteconomica/filteranuncio/code/' + code;

		if ( code == '' ) {
			$inputs.filter('#txtdescripcion').val('');
			datasend.codanuncio = '';
			return false;
		}

		$this.prop({disabled:true});

		$.getJSON(resource, function( json ) {

			if ( ! json.error ) {
				
				var data = json.data;
				
				if ( typeof data.codigo != 'undefined' ) {

					$inputs.filter('#txtdescripcion').val(data.anuncio);
					datasend.codanuncio = data.codigo;
					$this.removeAttr('style');

				} else {

					$.bootstrapGrowl('EL ANUNCIO NO HA SIDO ENCONTRADO', ACTE.growl('danger'));
					$this.css({borderColor:'red'});

				}

			} else {

				$.bootstrapGrowl('EL ANUNCIO NO HA SIDO ENCONTRADO', ACTE.growl('danger'));
				$this.css({borderColor:'red'});

			}

			$this.prop({disabled:false});
			
		});

	},

	_open_modal = function() {

		$bb_mng = $md_mng.cBootbox({
			title: "Registrar Anuncio",
			btnClose: '#btn-close',
			afterClose: _afterClose_modal,
			beforeOpen: function() {

				_asignar_eventos();

				datasend.action = 'create';

				_win.setTimeout(function() {
					$inputs.filter('#txtcodanuncio').focus();
				}, 500);

			}
		});

	},

	_update_row = function(e, value, row, index) {
		$bb_mng = $md_mng.cBootbox({
			title: "Editar Anuncio",
			btnClose: '#btn-close',
			afterClose: _afterClose_modal,
			beforeOpen: function() {

				_asignar_eventos();

				datasend.nro = row.nro;

				$inputs.filter('#txtcodanuncio').val(row.COD_ANUNCIO).change();
				$inputs.filter('#txtnro_lados').val(row.NRO_LADOS).change();
				$inputs.filter('#txtareaauto').val(row.AREA_AUT_ANUNCIO).change();
				$inputs.filter('#txtareaverif').val(row.AREA_VER_ANUNCIO).change();
				$inputs.filter('#txtnro_expediente').val(row.NRO_EXPEDIENTE).change();
				$inputs.filter('#txtnro_licencia').val(row.NRO_LICENCIA).change();
				$inputs.filter('#txtf_expedicion').val(row.FECHA_EXPEDICION).change();
				$inputs.filter('#txtf_vencimiento').val(row.FECHA_VENCIMIENTO).change();

				datasend.action = 'update';

				_win.setTimeout(function() {
					$inputs.filter('#txtcodanuncio').focus().select();
				}, 500);

			}
		});
	
	},

	_asignar_eventos = function() {

		$md_mng.find('button#btn-done, #btn-close').prop({disabled:false});

		$md_mng.find('select').on('keypress', select_keyPress);
		$md_mng.find('button#btn-done').on('click.ACTE.crear.AMF.anuncio.module', _submit_data);
		$inputs.filter('input[type=text].only-number').on('keydown.ACTE.crear.AMF.anuncio.module', only_number);
		$inputs.filter('input[type=text].only-number').on('change.ACTE.crear.AMF.anuncio.module', verify_number);

		$inputs.filter('input[type=text]#txtcodanuncio').on('change.ACTE.crear.AMF.anuncio.module', search_anuncio);

		$inputs.filter('.md-datasend').on('change.ACTE.crear.AMF.anuncio.module', _watcher_changes);

		$inputs.filter('.maskdate').inputmask();

	},

	_afterClose_modal = function() {
		$md_mng.find('input[type=text]').val('');
		datasend = {};
	},

	_delete_row = function(e, value, row, index) {

		var $this = $(this),
				postParams = {},
				resource = ACTE.base_url + 'acteconomica/mantenimientoanuncios';

		bootbox.confirm({
			title: 'Seguro que quieres eliminar?',
			message: "El anuncio: <strong>"+row.DESC_ANUNCIO+"</strong>, será eliminado de forma permanente.",
			callback: function(result) {

				if ( result ) {

					if ( ID_Ficha == null ) {
						$.bootstrapGrowl("La ficha no está asignada, el proceso a sido cancelado", ACTE.growl('warning'));
						return false;
					} else {
						postParams.ficha = ID_Ficha;
					}

					postParams.nro = row.nro;
					postParams.codanuncio = row.COD_ANUNCIO;
					postParams.action = 'delete';

					$this.prop({disabled:true});

					$.post(resource, postParams, function(response) {

						if ( ! response.error ) {

							$.bootstrapGrowl(response.message, ACTE.growl('success'));
							$table.bootstrapTable('refresh');
							$this.prop({ disabled: false });

						} else {

							$this.prop({disabled:false});
							$.bootstrapGrowl(response.message, ACTE.growl('danger'));

						}

					});

				}
			}
		});

	},

	_action_buttons = function(value, row, index) {
    return [
    	'<button id="btn-update-'+row.COD_ANUNCIO+'" class="btn-actions update btn btn-sm" data-id="'+row.COD_ANUNCIO+'" data-accion="edit" tabindex="-1">',
				'<i class="fa fa-pencil" aria-hidden="true"></i>',
			'</button>',
			'<button id="btn-delete-'+row.COD_ANUNCIO+'" class="btn-actions delete btn btn-sm" data-id="'+row.COD_ANUNCIO+'" data-accion="delete" tabindex="-1">',
				'<i class="fa fa-trash-o" aria-hidden="true"></i>',
			'</button>'
		].join('');
  },

  /**
   * Carga la tabla con todos los datos
   * @return {void}
   */
	_build_table = function() {

		var bootTable = {
			escape: false,
		  locale: 'es-SP',
		  idField: "COD_ANUNCIO",
		  url: ACTE.base_url + 'acteconomica/getanunciosficha/id/' + ID_Ficha,
		  onPostHeader: function(e){
				$table.find('tr th').removeAttr('tabindex');
		  }
		};

  	//TABLE HEADER
		bootTable.columns = [
			{ 
				field: 'action', 
				title: '<i class="fa fa-cog" aria-hidden="true"></i>', 
				align: 'center', 
				class: 'config-width',
				formatter : _action_buttons, 
				events : {
	        'click .delete': _delete_row,
	        'click .update': _update_row
    		}
    	},
    	{ field: 'nro', title: 'N°', align: 'center', class: 'ds-width' },
			{ field: 'COD_ANUNCIO', title: 'Codigo', align: 'center', class: 'ds-width' },
			{ field: 'DESC_ANUNCIO', title: 'Desc. Anuncio', align: 'left' },
			{ field: 'NRO_LADOS', title: 'N° Lados', align: 'left' },
			{ field: 'AREA_AUT_ANUNCIO', title: 'A. Autorizada', align: 'left' },
			{ field: 'AREA_VER_ANUNCIO', title: 'A. Verificada', align: 'left' },
			{ field: 'NRO_EXPEDIENTE', title: 'N° Expediente', align: 'left' },
			{ field: 'NRO_LICENCIA', title: 'N° Licencia', align: 'left' },
			{ field: 'FECHA_EXPEDICION', title: 'F. Expedición', align: 'left' },
			{ field: 'FECHA_VENCIMIENTO', title: 'F. Vencimiento', align: 'left' }
		];

		$table.bootstrapTable(bootTable);

	};

	return {
		init : _bind
	};

}(window, window.jQuery));