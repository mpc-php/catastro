// @koala-append "Informacion del Conductor/_ACTE.crear.IDC.agregar.js"
// @koala-append "Informacion del Conductor/_ACTE.crear.IDC.buscar.js"
// @koala-append "Informacion del Conductor/_ACTE.crear.IDC.buscar.vias.js"
// @koala-append "Informacion del Conductor/_ACTE.crear.IDC.buscar.haburbanas.js"

/**
 * MODULO PARA INFORMACION COMPLEMENTARIA
 */
ACTE.crear.IDC = (function (_win, $) {

	'use strict';

	var HUrbanas_arr 	= [],
			Vias_arr 			= [];

	var ID_Ficha 			= null,
			COD_Catastro 	= null,
			Ubigeo 				= null,
			Sector 				= null;

	var $cnt_dp 	= $('div#tab_identconductor'),
			$inputs 	= $cnt_dp.find('input, select, textarea');

	//CONSTRUCTOR
	var _bind = function (_ficha_, _catastro_, _sector_, _ubigeo_) {

		ID_Ficha 			= _ficha_,
		COD_Catastro 	= _catastro_;
		Sector 				= _sector_;
		Ubigeo 				= _ubigeo_;

		$inputs.filter('input[type=text].only-number').on('keydown.ACTE.crear.IDC.module', only_number);
		$inputs.filter('input[type=text].only-number').on('change.ACTE.crear.IDC.module', verify_number);

		$inputs.filter('.datasend').on('change.ACTE.crear.IDC.module', _watcher_general);
		$inputs.filter('#cbotipo_titular').on('change.ACTE.crear.IDC.module', _change_tipo_titular);
		$inputs.filter('#cbotipo_documento').on('change.ACTE.crear.IDC.module', _change_tipo_documento);

		$inputs.filter('select#cbodepartamento').on('change.ACTE.crear.IDC.module', _choise_departamento);
		$inputs.filter('select#cboprovincia').on('change.ACTE.crear.IDC.module', _choise_provincia);
		$inputs.filter('select#cbodistrito').on('change.ACTE.crear.IDC.module', _choise_distrito);
		$inputs.filter('input#txtcod_hurbana').on('change.ACTE.crear.IDC.module', _search_haburbana);
		$inputs.filter('input#txtcod_via').on('change.ACTE.crear.IDC.module', _search_vias);
		$inputs.filter('input#txtnro_documento').on('change.ACTE.crear.IDC.module', _search_persona);
		$inputs.filter('input#txtnro_interior').on('keydown.ACTE.crear.IDC.module', _open_nex_tab);

		events.on('search_titular', _busca_persona_from_modal);
		events.on('getRowSelectedHaburbana', _select_row_haburbana);
		events.on('getRowSelectedVia', _select_row_via);

		//CARGAR SUBMODULOS
		ACTE.crear.IDC.buscar.init($cnt_dp.find('button#btn-buscar-titular'));
		ACTE.crear.IDC.buscar.haburbanas.init();
		ACTE.crear.IDC.buscar.vias.init();

		var tipo_titular = $inputs.filter('select#cbotipo_titular').children('option:selected').val();
		events.emit('set_tipo_titular', parseInt(tipo_titular));

		_check_initial_data();

		$inputs.filter('.maskdate').inputmask();

		_load_combos();
		_load_haburbanas();

	},

	_check_initial_data = function() {

		$inputs.filter('input#txtnro_documento').change();

		if ( $inputs.filter('select#cbodepartamento').data('ubigeo') == '' ) {

			_load_ubigeos($inputs.filter('select#cbodepartamento'), '0', true);

		} else {

			var codubigeo = $inputs.filter('select#cbodepartamento').data('ubigeo').toString();

			var $cbodep = $inputs.filter('select#cbodepartamento'),
					$cbopro = $inputs.filter('select#cboprovincia'),
					$cbodis = $inputs.filter('select#cbodistrito');

			var dep = codubigeo.substr(0, 2),
					pro = codubigeo.substr(0, 4),
					dis = codubigeo.substr(0, 6);

			$cbodep.data({sel:dep});
			$cbopro.data({sel:pro});
			$cbodis.data({sel:dis});

			_load_ubigeos($cbodep, '0', true);
			_load_ubigeos($cbopro, dep, true);
			_load_ubigeos($cbodis, pro, true, true);

		}

	},

	_open_nex_tab = function(e) {

		if ( e.key == "Tab" && e.keyCode == 9 ) {
			$('#tabs-control').find('a[href="#tab_amfuncionamiento"]').tab('show');
			e.preventDefault();
			return false;
		}

	},

	_watcher_general = function () {

		var $this 	= $(this),
				name 		= $this.attr('name'),
				type 		= $this.attr('type'),
				tag 		= $this.prop("tagName").toLowerCase(),
				data 		= {};

		if ((tag == 'input' && type == 'text') || tag == 'textarea') {

			var value = this.value.trim();

			if ($this.hasClass('maskdate')) {

				if (!Inputmask.isValid(value, {alias: "dd/mm/yyyy"})) {
					value = '01/01/1990';
					$this.val(value);
				} else {
					$this.removeAttr('style');
				}

			}

			data[name] = value;

		} else if (tag == 'input' && type == 'checkbox') {
			data[name] = $this.is(':checked') ? 1 : 0;
		} else if (tag == 'select') {
			data[name] = $this.children('option:selected').val();
		}

		events.emit('savedata', data);

	},

	_change_tipo_documento = function() {

		$inputs.filter('#txtnombre').val('');
		$inputs.filter('#txtnro_documento').val('');
		$inputs.filter('#txtnro_documento').prop({disabled:false});

		if ( this.value == '00' ) {
			$inputs.filter('#txtnro_documento').prop({maxLength:11});
		} else if ( this.value == '02' ) {
			$inputs.filter('#txtnro_documento').prop({maxLength:8});
		} else if (this.value == '01') {
			$inputs.filter('#txtnro_documento').prop({disabled:true}).val('');
		} else {
			$inputs.filter('#txtnro_documento').prop({maxLength:30});
		}

	},

	_change_tipo_titular = function() {

		var $select = $inputs.filter('#cbotipo_documento');

		$cnt_dp.find('button#btn-buscar-titular').data({t:this.value});

		if ( this.value == 2 ) {
			$select.find('option[value="00"]').prop({selected:true});
			$select.prop({disabled:true});
		} else {
			$select.find('option.default').prop({selected:true});
			$select.prop({disabled:false});
		}

		$inputs.filter('input#txtnro_documento').val('');
		$inputs.filter('input#txtnombre').val('');

		events.emit('savedata', { id_persona: 0 });
		events.emit('set_tipo_titular', parseInt(this.value.trim()));

	},

	_choise_departamento = function() {

		var $this = $(this),
				cbopro = $inputs.filter('select#cboprovincia');

		cbopro.prop({disabled:true});

		_load_ubigeos(cbopro, $this.children('option:selected').val(), true);

		var dist = $inputs.filter('select#cbodistrito');

		dist.children('option:not(:eq(0))').remove();
		dist.prop({disabled:true});

	},

	_choise_provincia = function() {

		var $this = $(this),
				cbodis = $inputs.filter('select#cbodistrito');

		cbodis.prop({disabled:true});

		_load_ubigeos(cbodis, $this.children('option:selected').val(), true);

	},

	_choise_distrito = function() {

		if ( this.value == Ubigeo ) {

			$inputs.filter('#txtcod_hurbana,#txtcod_via').prop({disabled:false});
			$inputs.filter('#txtnombre_hurbana,#txtnombre_via').prop({disabled:true});

		} else {

			$inputs.filter('#txtcod_hurbana,#txtcod_via').prop({disabled:true}).val('');
			$inputs.filter('#txtnombre_hurbana,#txtnombre_via').prop({disabled:false});

			//HUrbanas_arr = [];
			//Vias_arr = [];
		}

	},

	_search_persona = function() {

		var $this = $(this),
				data = $this.data(),
				resource = ACTE.base_url + 'acteconomica/getinfopersona/stype/';

		if ( data.persona != 0 ) {
			resource += 'id/doc/' + data.persona;
		} else if ( $.trim($this.val()) != '' ) {
			resource += 'nro/doc/' + $.trim($this.val());
		} else {
			$inputs.filter('#cbotipo_titular').find('option:eq(1)').prop({selected:true});
			$inputs.filter('#cbotipo_titular').change();
			return false;
		}

		$this.prop({disabled:true});

		$.getJSON(resource, function(json) {

			if ( json.data.length > 0 ) {

				var pers = json.data[0];

				$this.val(pers.nro_doc);

				$inputs.filter('#txtnombre').val(pers.nombre_completo);

				//BUSCA EL COMBO E IDENTIFICA SI YA TIENE OPCIONES O SI TODAVIA
				//NO LAS HA CARGADO EN BASE A ESO SELECCIONA O ASIGNA AUTOSELECCION
				var $cbottitular = $inputs.filter('#cbotipo_titular');

				if ( $cbottitular[0].options.length == 0 ) {
					$cbottitular.data({sel:pers.tipo_persona});
				} else {
					$cbottitular.find('option[value="'+pers.tipo_persona+'"]').prop({selected:true});
				}

				var $cbotdoc = $inputs.filter('#cbotipo_documento');

				if ( $cbotdoc[0].options.length <= 1 ) {
					$cbotdoc.data({sel:pers.tipo_doc});
				} else {
					$cbotdoc.find('option[value="'+pers.tipo_doc+'"]').prop({selected:true});
				}

				$cnt_dp.find('button#btn-buscar-titular').data({t:pers.tipo_persona});

				if ( pers.tipo_persona == 2 ) {
					$inputs.filter('#cbotipo_documento').prop({disabled:true});
				} else {
					$inputs.filter('#cbotipo_documento').prop({disabled:false});
				}

				if ( data.persona == 0 ) {
					events.emit('savedata', {id_persona:pers.id_persona});
				}

				$this.data({persona:0});

			} else {
				$.bootstrapGrowl("La persona que se busca no ha sido encontrada", ACTE.growl('warning'));
			}

			$this.prop({disabled:false});

		});

	},

	_busca_persona_from_modal = function(valor) {

		if ( valor != '' ) {
			_busca_persona(
				1,
				valor,
				$inputs.filter('#txtnro_documento'),
				$inputs.filter('#txt_comercial')
			);
		} else {
			_win.setTimeout(function(){
				$md_mng.find('#btn-buscar-titular');
			}, 300);
		}

	},

	_busca_persona = function( tipo, valorabuscar, $element, $elementFocus ) {

		var resource = 'individual/gettitular',
				params = {
					type: tipo,
					str: valorabuscar
				};

		if ( ! valorabuscar ) return false;

		$element.prop({disabled:true}).css({cursor:'progress'});

		$.post(ACTE.base_url + resource, params, function(json) {

			if ( ! json.error ) {

				if ( json.data.length > 0 ) {

					var info = json.data[0],
							$cbotipodoc = $inputs.filter('#cbotipo_documento');

					$cbotipodoc.find('option[value="'+info.tipo_documento+'"]').prop({selected:true});

					$cbotipodoc.change();

					$inputs.filter('#txtnombre').val(info.nombres);
					$inputs.filter('#txtnro_documento').val(info.nro_documento);

					events.emit('savedata', { id_persona:info.id_persona });

					setFocus($inputs.filter('#txt_comercial'));

				}

			} else {

				events.emit('savedata', { id_persona: '' });
				$element.val('').css({borderColor:'red'});
				$.bootstrapGrowl("No se encontró datos de la persona", ACTE.growl('warning'));
				$inputs.filter('#txtnombre').val('').change();
				$inputs.filter('#cbopersona_juridica').children('option.default').prop({selected:true}).change();

			}

			$element.prop({disabled:false}).removeAttr('style');

			_win.setTimeout(function(){
				$elementFocus.focus();
			}, 300);

		});

	},

	_search_vias = function() {

		var $this 		= $(this),
				value 		= this.value.trim(),
				foundObj 	= {},
				found 		= false;

		if ( value.length == 0 ) {
			$inputs.filter('input#txtnombre_via').val('');
			return false;
		}

		Vias_arr.forEach(function(obj, index) {
			if ( obj.Cod_via == value ) {
				foundObj = obj;
				found = true;
				return false;
			}
		});

		if ( found ) {

			$inputs.filter('input#txtnombre_via').val(foundObj.via);
			$this.removeAttr('style', 'title');

		} else {

			$this.css({
				borderColor:'red'
			}).attr({
				title: "No existe vía para la habilitación urbana."
			});

			$inputs.filter('input#txtnombre_via').val('');

			$this.val('');

			$.bootstrapGrowl("No existe vía para la habilitación urbana.", ACTE.growl('warning'));

		}

	},

	_search_haburbana = function() {

		var $this 		= $(this),
				value 		= this.value.trim(),
				foundObj 	= {},
				found 		= false;

		if ( value.length == 0 ) {

			$inputs.filter('#txtnombre_hurbana, #txtcod_via, #txtnombre_via').val('');

			_win.setTimeout(function(){
				$inputs.filter('#txtcod_via').change();
			}, 300);

			Vias_arr = [];

			return false;
		}

		HUrbanas_arr.forEach(function(obj, index) {
			if ( obj.code == value ) {
				foundObj = obj;
				found = true;
				return false;
			}
		});

		if ( found ) {

			_load_vias(foundObj.code);

			$inputs.filter('input#txtnombre_hurbana').val(foundObj.name + ' - ' + foundObj.zoni);

			$this.removeAttr('style', 'title');

		} else {

			$this.css({borderColor:'red'}).attr({
				title:'No existe la Habilitación urbana para este sector'
			});

			$inputs.filter('#txtnombre_hurbana, #txtcod_via, #txtnombre_via').val('');

			$inputs.filter('#txtcod_via').change();

			$this.val('');

			var send = {};

			send[$this.attr('name')] = '';

			events.emit('savedata', send);

			Vias_arr = [];

			$.bootstrapGrowl("No existe habilitación urbana.", ACTE.growl('warning'));

		}

	},

	_load_ubigeos = function($select, _cod, _enabled, _exec_change) {

		var code = _cod || '0';

		if ( $select.hasClass('sending') ) {
			_win.clearTimeout($select.data('timeID'));
		}

		$select.children('option:eq(0)').prop({ selected: true });
		$select.children('option:not(:eq(0))').remove();

		(function (cd, $cbo ) {

			var timeID = _win.setTimeout(function() {

				$.getJSON(ACTE.base_url + 'principal/getubigeo/c/' + cd, function(json) {

					if ( ! json.error ) {

						json.data.forEach(function(obj, index) {

							var params = { value: obj.CODIGO.trim() };

							if ( $cbo.data('sel') == obj.CODIGO.trim() ) {
								params.selected = true;
							}

							$cbo.append( $('<option />', params).text(obj.NOMBRE) );

						});

						if( json.data.length > 0 && _enabled ) $cbo.prop({disabled:false});

						if ( _exec_change ) $cbo.change();

					} else {
						$cbo.prop({selected:true});
					}

				}).complete(function() {
					$cbo.removeData('timeID');
					$cbo.removeClass('sending');
				});

			}, 500);

			$cbo.addClass('sending').data({timeID: timeID});

		}(code, $select));

	},

	_load_combos = function () {

		var $selects = $inputs.filter('select.autoload');

		$.each($selects, function (index, el) {

			var $el = $(el),
					type = $el.data('type'),
					sw = $el.data('sw'),
					resource = 'principal/getcombo/type/' + type + '/sw/' + sw;

			$.getJSON(ACTE.base_url + resource, function (json, textStatus) {

				if (!json.error) {

					json.data.forEach(function (obj, pos) {

						var params = {value: obj.CODIGO.trim()};

						if ($el.data('sel') == params.value)
							params.selected = true;

						$el.append($('<option />', params).text(obj.NOMBRE));

					});

				} else {

					$el.attr({title: 'No se encontraron datos'}).prop({disabled: true});

				}

			});

		});

	},

	_load_vias = function( cod_hurbana ) {

		var resource = ACTE.base_url + 'individual/gethaburbanavia/haburbana/' + cod_hurbana;

		$inputs.filter('input#txtcod_via').prop({disabled:true});

		$.getJSON(resource, function(json) {

			if ( ! json.error ) {

				if ( json.data.length > 0 ) {

					Vias_arr = json.data;
					events.emit('setVias', json.data);

					$inputs.filter('input#txtcod_via').prop({disabled:false});

					if ( $inputs.filter('input#txtcod_via').val() != '' ) {
						$inputs.filter('input#txtcod_via').change();
					}

				} else {

					Vias_arr = [];
					events.emit('setVias', []);
					$inputs.filter('input#txtcod_via').prop({disabled:true});

				}

			} else {
				Vias_arr = [];
				events.emit('setVias', []);
				$inputs.filter('input#txtcod_via').prop({disabled:true});
			}

		});

	},

	_load_haburbanas = function() {

		var resource = (Request.Host + Request.BaseUrl) + '/mantenedor/haburbanas/show';

		$inputs.filter('#txtcod_hurbana').prop({disabled:true});

		$.getJSON(resource, function(json) {

			if ( ! json.error ) {

				if ( json.data.length > 0 ) {

					HUrbanas_arr = json.data;
					$inputs.filter('#txtcod_hurbana').prop({disabled:false});

					events.emit('setHaburbanas', json.data);

					if ($inputs.filter('#txtcod_hurbana').val() != '') {
						$inputs.filter('#txtcod_hurbana').change();
					}

				} else {

					events.emit('setHaburbanas', []);
					$inputs.filter('#txtcod_hurbana').prop({disabled:true});

				}

			} else {

				events.emit('setHaburbanas', []);
				$inputs.filter('#txtcod_hurbana').prop({disabled:true});

			}

		});

	},

	_select_row_titular = function(info) {

		var $cbotipodoc = $inputs.filter('#cbotipo_documento');

		$cbotipodoc.find('option[value="'+info.TIP_DOC+'"]').prop({selected:true});

		$cbotipodoc.change();

		$inputs.filter('#txtnombre').val(info.NOMBRES);
		$inputs.filter('#txtnro_documento').val(info.CODIGO);

		events.emit('savedata', { id_persona:info.ID_PERSONA });

		setFocus($inputs.filter('#txt_comercial'));

	},

	_select_row_haburbana = function(row) {

		var $txtcode = $inputs.filter('input#txtcod_hurbana'),
				$txtname = $inputs.filter('input#txtnombre_hurbana'),
				name = $txtcode.attr('name'),
				send = {};

		$txtcode.val(row.code).removeAttr('style');
		$txtname.val(row.name + ' - ' +row.zoni);

		send[name] = row.code;

		events.emit('savedata', send);

		_load_vias(row.code);

		setFocus($inputs.filter('#txtmanzana'));

	},

	_select_row_via = function(row) {

		var $txtcode = $inputs.filter('input#txtcod_via'),
				$txtname = $inputs.filter('input#txtnombre_via'),
				name = $txtcode.attr('name'),
				send = {};

		$txtcode.val(row.Cod_via).removeAttr('style');
		$txtname.val(row.via);

		send[name] = row.Cod_via;

		events.emit('savedata', send);

		setFocus($inputs.filter('#txtnro_municipalidad'));

	};

	return {
		init: _bind
	};

}(window, window.jQuery));