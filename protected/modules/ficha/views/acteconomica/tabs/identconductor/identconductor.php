<?php

	$up = new stdClass;

	if ( ! $isOnlyView ) 
	{
		$up = UActeconomica::getCookie(CActeconomica::UP01_NAME_CK);
	}
	else 
	{
		$up = $UP01;
	}

	$persona = isset($up->id_persona) ? $up->id_persona : 0;
	$comercial = isset($up->comercial) ? $up->comercial : NULL;
	$cond_conductor = isset($up->cond_conductor) ? $up->cond_conductor : NULL;
	$ruc = isset($up->ruc) ? $up->ruc : NULL;
	$ubigeo = isset($up->ubigeo) ? $up->ubigeo : 0;
	$telefono = isset($up->telefono) ? $up->telefono : NULL;
	$anexo = isset($up->anexo) ? $up->anexo : NULL;
	$fax = isset($up->fax) ? $up->fax : NULL;
	$email = isset($up->email) ? $up->email : NULL;
	$hurbana = isset($up->hurbana) ? $up->hurbana : NULL;
	$manzana = isset($up->manzana) ? $up->manzana : NULL;
	$lote = isset($up->lote) ? $up->lote : NULL;
	$sublote = isset($up->sublote) ? $up->sublote : NULL;
	$via = isset($up->via) ? $up->via : NULL;
	$nro_municipalidad = isset($up->nro_municipalidad) ? $up->nro_municipalidad : NULL;
	$nombre_edificio = isset($up->nombre_edificio) ? $up->nombre_edificio : NULL;
	$nro_interior = isset($up->nro_interior) ? $up->nro_interior : NULL;

?>

<div role="tabpanel" class="tab-pane active" id="tab_identconductor">

	<div class="container">

		<div class="row">
			<div class="col-xs-12">
				<fieldset>
					<legend>Identificación del conductor</legend>
					<div class="row">

						<div class="col-sm-4 form-group">
							<label for="cbotipo_titular" class="control-label">Tipo de Titular</label>
							<div class="input-group">
								<select id="cbotipo_titular" data-sel="1" data-exchange="true" data-type="07" data-sw="0" class="form-control autoload"></select>
								<span class="input-group-btn">
									<button type="button" data-rel="pe" data-t="1" data-u="00" data-search="" id="btn-buscar-titular" class="btn btn-default">
										<i class="fa fa-search" aria-hidden="true"></i>
									</button>
								</span>
							</div>
						</div>

						<div class="col-sm-5">
							<label for="txt_comercial" class="control-label">Comercial</label>
							<input type="text" id="txt_comercial" name="comercial" value="<?= $comercial; ?>" class="form-control datasend"/>
						</div>

						<div class="col-sm-3">
							<label for="cbocond_conductor" class="control-label">Cond. Conductor</label>
							<select id="cbocond_conductor" name="cond_conductor" data-sel="<?= $cond_conductor; ?>" data-type="31" data-sw="3" class="form-control autoload datasend">
								<option value="" class="default" selected>Seleccione...</option>
							</select>
						</div>

					</div>

					<div class="row">
						<div class="col-xs-12">
							<fieldset>
								<legend>Conductor</legend>
								<div class="row">

									<div class="col-sm-4 form-group">
										<label for="cbotipo_documento" class="control-label">Tipo documento identidad</label>
										<select id="cbotipo_documento" data-type="09" data-sw="0" class="form-control autoload">
											<option value="" class="default" selected>Seleccione...</option>
										</select>
									</div>

									<div class="col-sm-2 form-group">
										<label id="lblnro_documento" for="txtnro_documento" class="control-label">N° de Documento</label>
										<input type="text" id="txtnro_documento" data-persona="<?= $persona; ?>" class="form-control only-number">
									</div>

									<div class="col-sm-4 form-group">
										<label id="lblnombre" for="txtnombre" class="control-label">Nombres ó Razón Social</label>
										<input type="text" id="txtnombre" class="form-control">
									</div>

									<div class="col-sm-2 form-group">
										<label id="lblnombre" for="txtruc" class="control-label">RUC</label>
										<input type="text" id="txtruc" name="ruc" value="<?= $ruc; ?>" class="form-control only-number datasend">
									</div>

								</div>
							</fieldset>
						</div>
					</div>

				</fieldset>
			</div>
		</div>

		<div class="row">
			<div class="col-xs-12">
				<fieldset>
					<legend>Domicilio fiscal del titular catastral</legend>

					<div class="row">
						<div class="col-sm-4 form-group">
							<label for="cbodepartamento" class="control-label">Departamento</label>
							<select id="cbodepartamento" data-ubigeo="<?= $ubigeo; ?>" class="form-control ubigeo">
								<option value="" selected class="default" disabled>Seleccione...</option>
							</select>
						</div>
						<div class="col-sm-4 form-group">
							<label for="cboprovincia" class="control-label">Provincia</label>
							<select id="cboprovincia" class="form-control ubigeo" disabled>
								<option value="" selected class="default" disabled>Seleccione...</option>
							</select>
						</div>
						<div class="col-sm-4 form-group">
							<label for="cbodistrito" class="control-label">Distrito</label>
							<select id="cbodistrito" name="ubigeo" class="form-control ubigeo datasend" disabled>
								<option value="" selected class="default" disabled>Seleccione...</option>
							</select>
						</div>
					</div>

					<div class="row">
						<div class="col-sm-3 form-group">
							<label for="txttelefono" class="control-label">Telefono</label>
							<input type="text" id="txttelefono" name="telefono" value="<?= $telefono; ?>" class="form-control only-number datasend">
						</div>
						<div class="col-sm-3 form-group">
							<label for="txtanexo" class="control-label">Anexo</label>
							<input type="text" id="txtanexo" name="anexo" value="<?= $anexo; ?>" class="form-control only-number datasend">
						</div>
						<div class="col-sm-3 form-group">
							<label for="txtfax" class="control-label">Fax</label>
							<input type="text" id="txtfax" name="fax" value="<?= $fax ?>" class="form-control only-number datasend">
						</div>
						<div class="col-sm-3 form-group">
							<label for="txtemail" class="control-label">Correo Electronico</label>
							<input type="text" id="txtemail" name="email" value="<?= $email; ?>" class="form-control datasend">
						</div>
					</div>

					<div class="row">
						<div class="col-xs-12">
							<fieldset>
								<div class="row">
									<div class="col-sm-2 form-group">
										<label for="txtcod_hurbana" class="control-label">Cod. HU</label>
										<input type="text" id="txtcod_hurbana" name="hurbana" value="<?= is_numeric($hurbana) ? $hurbana : ''; ?>" maxlength="5" class="form-control only-number datasend" disabled >
									</div>
									<div class="col-sm-4 form-group">
										<label for="txtnombre_hurbana" class="control-label">Nombre Hab. urbana</label>
										<input type="text" id="txtnombre_hurbana" name="hurbana"  value="<?= $hurbana; ?>" class="form-control datasend" >
									</div>
									<div class="col-sm-2 form-group">
										<label for="txtmanzana" class="control-label">Manzana</label>
										<input type="text" id="txtmanzana" name="manzana" value="<?= $manzana; ?>" class="form-control datasend" >
									</div>
									<div class="col-sm-2 form-group">
										<label for="txtlote" class="control-label">Lote</label>
										<input type="text" id="txtlote" name="lote" value="<?= $lote; ?>" class="form-control datasend" >
									</div>
									<div class="col-sm-2 form-group">
										<label for="txtsublote" class="control-label">Sub-lote</label>
										<input type="text" id="txtsublote" name="sublote" value="<?= $sublote; ?>" class="form-control datasend" >
									</div>
								</div>
							</fieldset>
						</div>
					</div>

					<div class="row">
						<div class="col-xs-12">
							<fieldset style="margin-bottom: 0;">
								<div class="row">
									<div class="col-sm-2 form-group">
										<label for="txtcod_via" class="control-label">Cod. Vía</label>
										<input type="text" id="txtcod_via" name="via" value="<?= is_numeric($via) ? $via : ''; ?>" class="form-control only-number datasend" disabled >
									</div>
									<div class="col-sm-4 form-group">
										<label for="txtnombre_via" class="control-label">Nombre de Vía</label>
										<input type="text" id="txtnombre_via" name="via" value="<?= $via; ?>" class="form-control datasend" >
									</div>
									<div class="col-sm-2 form-group">
										<label for="txtnro_municipalidad" class="control-label">N° Municipalidad</label>
										<input type="text" id="txtnro_municipalidad" name="nro_municipalidad" value="<?= $nro_municipalidad; ?>" class="form-control only-number datasend" >
									</div>
									<div class="col-sm-3 form-group">
										<label for="txtnombre_edificio" class="control-label">Nombre edificación</label>
										<input type="text" id="txtnombre_edificio" name="nombre_edificio" value="<?= $nombre_edificio; ?>" class="form-control datasend" >
									</div>
									<div class="col-sm-1 form-group">
										<label for="txtnro_interior" class="control-label"><small>N° interior</small></label>
										<input type="text" id="txtnro_interior" name="nro_interior" value="<?= $nro_interior; ?>" class="form-control datasend" >
									</div>
								</div>
							</fieldset>
						</div>
					</div>

				</fieldset>
			</div>
		</div>


	</div>

</div>

<?php $this->renderPartial('tabs/identconductor/modal/md_buscar_titular'); ?>
<?php $this->renderPartial('tabs/identconductor/modal/md_agrega_titular'); ?>
