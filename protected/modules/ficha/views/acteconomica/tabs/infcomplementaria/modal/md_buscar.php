<section id="md-buscar" style="display: none;">

  <div class="row">
    <div class="col-xs-12">

      <style type="text/css">

        table#dt-buscar tbody tr:hover {
          background-color: #f0f0f0;
        }

        table#dt-buscar td {
          padding: 8px !important;
          vertical-align: middle;
          font-size: 0.85em;
          user-select:none;
          -webkit-user-select:none;
          cursor: pointer;
          white-space: -moz-pre-wrap !important;  /* Mozilla, since 1999 */
          white-space: -pre-wrap;      /* Opera 4-6 */
          white-space: -o-pre-wrap;    /* Opera 7 */
          white-space: pre-wrap;       /* css-3 */
        }

        .pagination-info { display: inline-block !important; }
        .page-list { display: none !important; }
        .btn-actions {
          position: relative;
          z-index: 5; 
          margin: auto !important; 
        }

        .table-responsive {
          background-color: white !important;
          border: none !important;
          margin-bottom: 15px;
        }

        #cnt-dt-buscar .search {
          display: block !important;
          margin: 0;
          width: 100%;
          border-bottom: none;
        }

        #cnt-dt-buscar .search input {
          border: 1px solid #ddd;
          height: 45px;
          width: 100%;
          display: block;
        }

      </style>

      <div id="cnt-dt-buscar" class="">
        <table id="dt-buscar" class="table table-custom"></table>
      </div>
    </div>
  </div>

  <div class="footer-body align-right">

    <button type="submit" id="btn-done" disabled class="btn purple">
      <i class="fa fa-floppy-o" aria-hidden="true"></i>&nbsp;Aceptar
    </button>

    <button type="button" id="btn-cancel" class="btn default" >
      <i class="fa fa-times" aria-hidden="true"></i>&nbsp;Cancelar
    </button>

  </div>

</section>