<?php

	$up = new stdClass;

	if ( ! $isOnlyView ) 
	{
		$up = UActeconomica::getCookie(CActeconomica::UP01_NAME_CK);
	}
	else 
	{
		$up = $UP01;
	}

	$predio_verificada = isset($up->predio_verificada) ? $up->predio_verificada : NULL;
	$predio_autorizada = isset($up->predio_autorizada) ? $up->predio_autorizada : NULL;
	$total_verificada = isset($up->total_verificada) ? $up->total_verificada : NULL;
	$total_autorizada = isset($up->total_autorizada) ? $up->total_autorizada : NULL;
	$via_autorizada = isset($up->via_autorizada) ? $up->via_autorizada : NULL;
	$via_verificada = isset($up->via_verificada) ? $up->via_verificada : NULL;
	$bien_autorizada = isset($up->bien_autorizada) ? $up->bien_autorizada : NULL;
	$bien_verificada = isset($up->bien_verificada) ? $up->bien_verificada : NULL;
	$nro_expediente = isset($up->nro_expediente) ? $up->nro_expediente : NULL;
	$nro_licencia = isset($up->nro_licencia) ? $up->nro_licencia : NULL;
	$f_expedicion = isset($up->f_expedicion) ? $up->f_expedicion : NULL;
	$f_vencimiento = isset($up->f_vencimiento) ? $up->f_vencimiento : NULL;
	$f_inicio = isset($up->f_inicio) ? $up->f_inicio : NULL;

?>

<style type="text/css">
		#tab_amfuncionamiento .container .row > [class*="col-"] {
				padding-left: 7.5px;
				padding-right: 7.5px;
		}

		#tab_amfuncionamiento .container .row > [class*="col-"] > .row {
				margin-left: -7.5px;
				margin-right: -7.5px;
		}
		.in-table {
			border: none !important;
		}

		#td-area-actividad td {
			padding: 0 !important;
			border: 1px solid #ddd !important;
		}

		#td-area-actividad tbody th, #td-area-actividad tfoot th {
			text-align: right;
			padding: 0 4px 0 0;
			vertical-align: middle;
		}

		#td-area-actividad thead tr th {
			text-align: center;
			font-size: 0.75em;
		}
</style>

<div role="tabpanel" class="tab-pane" id="tab_amfuncionamiento">

	<div class="container">

		<div class="row">

				<div class="col-xs-12 col-md-4">
					<fieldset>
						<div class="row">
							<div class="col-xs-12">
								<div class="table-responsive">
									<table id="td-actividad" class="table table-custom"></table>
								</div>
							</div>
							<div class="col-xs-12" style="text-align:right;">
								<button type="button" id="btn-add-actividad" class="btn grey-salsa">
									<i class="fa fa-plus" aria-hidden="true"></i>&nbsp;
									Agregar
								</button>
							</div>
						</div>
					</fieldset>
				</div>

				<div class="col-xs-12 col-md-4">
						<fieldset>
							<legend>Área de la Actividad Económica</legend>
							<div class="row">
								<div class="col-xs-12">
									<table id="td-area-actividad" class="table table-custom">
										<thead>
											<tr>
												<th style="width: 130px" data-align="right" data-field="codigo">Ubicación</th>
												<th data-align="right" data-field="area_auto">A. Autorizada</th>
												<th data-align="right" data-field="area_verif">A. Verificada</th>
											</tr>
										</thead>
										<tbody>
											<tr>
												<th><small>Predio Catastral</small></th>
												<td><input type="text" id="txtpredio_autorizada" name="predio_autorizada" value="<?= $predio_autorizada; ?>" data-group="autorizada" class="form-control in-table only-number calcule datasend"/></td>
												<td><input type="text" id="txtpredio_verificada" name="predio_verificada" value="<?= $predio_verificada; ?>" data-group="verificada" class="form-control in-table only-number calcule datasend"/></td>
											</tr>
											<tr>
												<th><small>Vía Pública</small></th>
												<td><input type="text" id="txtvia_autorizada" name="via_autorizada" value="<?= $via_autorizada; ?>" data-group="autorizada" class="form-control in-table only-number calcule datasend"/></td>
												<td><input type="text" id="txtvia_verificada" name="via_verificada" value="<?= $via_verificada; ?>" data-group="verificada" class="form-control in-table only-number calcule datasend"/></td>
											</tr>
											<tr>
												<th><small>Bien Común</small></th>
												<td><input type="text" id="txtbien_autorizada" name="bien_autorizada" value="<?= $bien_autorizada; ?>" data-group="autorizada" class="form-control in-table only-number calcule datasend"/></td>
												<td><input type="text" id="txtbien_verificada" name="bien_verificada" value="<?= $bien_verificada; ?>" data-group="verificada" class="form-control in-table only-number calcule datasend"/></td>
											</tr>
										</tbody>
										<tfoot>
											<tr>
												<th><small>Total</small></th>
												<td><input type="text" id="txttotal_autorizada" name="total_autorizada" value="<?= $total_autorizada; ?>" class="form-control in-table datasend" readonly="true" tabindex="-1"/></td>
												<td><input type="text" id="txttotal_verificada" name="total_verificada" value="<?= $total_verificada; ?>" class="form-control in-table datasend" readonly="true" tabindex="-1"/></td>
											</tr>
										</tfoot>
									</table>
								</div>
							</div>
						</fieldset>
				</div>

				<div class="col-xs-12 col-md-4">
					<div class="row">
						<div class="col-sm-6 form-group">
							<label for="txtnro_expediente" class="control-label">Num. Expediente</label>
							<input type="text" id="txtnro_expediente" name="nro_expediente" value="<?= $nro_expediente; ?>" class="form-control datasend">
						</div>
						<div class="col-sm-6 form-group">
							<label for="txtnro_licencia" class="control-label">Num. Licencia</label>
							<input type="text" id="txtnro_licencia" name="nro_licencia" value="<?= $nro_licencia; ?>" class="form-control datasend">
						</div>
					</div>
					<fieldset>
						<legend>Vigencia de Autorización</legend>
						<br>
						<div class="row">
							<div class="col-sm-12 col-md-4 form-group">
								<label for="txtf_expedicion" class="control-label"><small>F. Expedicion</small></label>
								<input type="text" id="txtf_expedicion" name="f_expedicion" value="<?= $f_expedicion; ?>" class="form-control datasend maskdate"  data-inputmask="'alias': 'date'" placeholder="dd/mm/yyyy">
							</div>
							<div class="col-sm-12 col-md-4 form-group">
								<label for="txtf_vencimiento" class="control-label"><small>F. Vencimiento</small></label>
								<input type="text" id="txtf_vencimiento" name="f_vencimiento" value="<?= $f_vencimiento; ?>" class="form-control datasend maskdate"  data-inputmask="'alias': 'date'" placeholder="dd/mm/yyyy">
							</div>
							<div class="col-sm-12 col-md-4 form-group">
								<label for="txtfinicio" class="control-label"><small>Inicio de Activ.</small></label>
								<input type="text" id="txtfinicio" name="f_inicio" value="<?= $f_inicio; ?>" class="form-control datasend maskdate"  data-inputmask="'alias': 'date'" placeholder="dd/mm/yyyy">
							</div>
						</div>
					</fieldset>
				</div>
		</div>

		<div class="row">
			<div class="col-xs-12">
				<fieldset>

					<div class="row">
						<div class="col-xs-12">
							<div class="table-responsive">
								<table id="td-anuncio" class="table table-hover table-custom"></table>
							</div>
						</div>
						<div class="col-xs-12" style="text-align:right;">
							<button type="button" id="btn-add-anuncio" class="btn grey-salsa">
								<i class="fa fa-plus" aria-hidden="true"></i>&nbsp;
								Agregar
							</button>
						</div>
					</div>

				</fieldset>
			</div>
		</div>

	</div>

</div>

<?php $this->renderPartial('tabs/amfuncionamiento/modal/md_anuncio'); ?>
