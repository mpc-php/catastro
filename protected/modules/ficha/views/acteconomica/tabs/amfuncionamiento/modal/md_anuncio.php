<style type="text/css">
	.toupper {text-transform: uppercase;}

	#md-anuncio div[class*="col-"] {
		padding-left: 7.5px;
		padding-right: 7.5px;
	}

</style>
<section id="md-anuncio" style="display:none;">

	<div class="row">
		<div class="col-xs-12 col-md-3 form-group">
			<label for="txtcodanuncio" class="control-label">Cod. Tipo Anuncio</label>
			<input type="text" id="txtcodanuncio" name="codanuncio" maxlength="6" autofocus class="form-control only-number md-datasend"/>
		</div>

		<div class="col-xs-12 col-md-7 form-group">
			<label for="txtdescripcion" class="control-label">Descripcion Tipo Anuncio</label>
			<input type="text" id="txtdescripcion" class="form-control md-datasend" disabled />
		</div>

		<div class="col-xs-12 col-md-2 form-group">
			<label for="txtnro_lados" class="control-label">N° Lados</label>
			<input type="text" id="txtnro_lados" name="nro_lados" class="form-control only-number md-datasend">
		</div>
	</div>

	<div class="row">
		<div class="col-xs-12 col-md-3 form-group">
			<label for="txtareaauto" class="control-label">Área Autorizada</label>
			<input type="text" id="txtareaauto" name="area_autorizada" class="form-control only-number md-datasend toupper">
		</div>

		<div class="col-xs-12 col-md-3 form-group">
			<label for="txtareaverif" class="control-label">Área Verificada</label>
			<input type="text" id="txtareaverif" name="area_verificada" class="form-control only-number md-datasend toupper">
		</div>

		<div class="col-xs-12 col-md-3 form-group">
			<label for="txtnro_expediente" class="control-label">Nro. Expediente</label>
			<input type="text" id="txtnro_expediente" name="nro_expediente" class="form-control md-datasend toupper">
		</div>
		<div class="col-xs-12 col-md-3 form-group">
			<label for="txtnro_licencia" class="control-label">Nro. Licencia</label>
			<input type="text" id="txtnro_licencia" name="nro_licencia" class="form-control md-datasend toupper">
		</div>
	</div>
	<div class="row">

		<div class="col-xs-12 col-md-3 form-group">
			<label for="txtf_expedicion" class="control-label">F. Expedición</label>
			<input type="text" id="txtf_expedicion" name="f_expedicion" class="form-control md-datasend toupper maskdate"  data-inputmask="'alias': 'date'" placeholder="dd/mm/yyyy">
		</div>
		
		<div class="col-xs-12 col-md-3 form-group">
			<label for="txtf_vencimiento" class="control-label">F. Vencimiento</label>
			<input type="text" id="txtf_vencimiento" name="f_vencimiento" class="form-control md-datasend toupper maskdate"  data-inputmask="'alias': 'date'" placeholder="dd/mm/yyyy">
		</div>

	</div>

	<div class="footer-body align-right">
		<button type="submit" id="btn-done" disabled class="btn purple">
			<i class="fa fa-floppy-o" aria-hidden="true"></i>&nbsp;Aceptar
		</button>
		<button type="button" id="btn-close" class="btn default" >
			<i class="fa fa-times" aria-hidden="true"></i>&nbsp;Cerrar
		</button>
	</div>

</section>