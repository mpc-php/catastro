<section id="md-instalaciones" style="display: none;">

  <div class="row">
    <div class="col-xs-12">

      <style type="text/css">

        table#dt-instalaciones tbody tr:hover {
          background-color: #f0f0f0;
        }

        table#dt-instalaciones td {
          padding: 0 8px !important;
          vertical-align: middle;
          font-size: 0.85em;
          user-select:none;
          -webkit-user-select:none;
          cursor: pointer;
          white-space: -moz-pre-wrap !important;  /* Mozilla, since 1999 */
          white-space: -pre-wrap;      /* Opera 4-6 */
          white-space: -o-pre-wrap;    /* Opera 7 */
          white-space: pre-wrap;       /* css-3 */
        }

        table#dt-instalaciones input[type=radio]:focus ~ span {
          outline: 1px solid #36c6d3!important;
          border-color: #36c6d3!important;
        }

        .pagination-info { display: inline-block !important; }
        .page-list { display: none !important; }
        .btn-actions {
          position: relative;
          z-index: 5; 
          margin: auto !important; 
        }

        .table-responsive {
          background-color: white !important;
          border: none !important;
        }

        #cnt-dt-instalaciones .search {
          display: block !important;
          margin: 0;
          width: 100%;
          border-bottom: none;
        }

        #cnt-dt-instalaciones .search input {
          border: 1px solid #ddd;
          height: 45px;
          width: 100%;
          display: block;
        }

      </style>

      <div id="cnt-dt-instalaciones" class="">
        <table id="dt-instalaciones" class="table table-custom"></table>
      </div>
    </div>
  </div>

  <div class="footer-body align-right">

    <button type="button" id="btn-add" class="btn green" style="float:left" >
      <i class="fa fa-plus" aria-hidden="true"></i>&nbsp;Agregar
    </button>

    <button type="button" id="btn-cancel" class="btn default" >
      <i class="fa fa-times" aria-hidden="true"></i>&nbsp;cerrar
    </button>

  </div>

</section>