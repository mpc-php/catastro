<div role="tabpanel" class="tab-pane" id="tab_obrasconstr">

	<div class="container">

		<div class="row">

			<div class="col-xs-12">

			<fieldset>

				<legend>Construcciones</legend>

				<div class="col-xs-12">
					<div class="table-responsive">
						<table id="td-construcciones" class="table table-hover table-custom"></table>
					</div>

				</div>

				<div class="col-xs-12" style="text-align:right;">
					<button type="button" id="add-construccion" class="btn grey-salsa">
						<i class="fa fa-plus" aria-hidden="true"></i>&nbsp;
						Agregar
					</button>
				</div>

			</fieldset>

			</div>

		</div>

		<div class="row">

			<div class="col-xs-12">

				<fieldset>
				<legend>Obras complementarias / Otras instalaciones</legend>

				<div class="col-xs-12">
					<div class="table-responsive">
						<table id="td-obras" class="table table-hover table-custom"></table>
					</div>
				</div>

				<div class="col-xs-12" style="text-align:right;">
					<button type="button" id="add-obras" class="btn grey-salsa">
						<i class="fa fa-plus" aria-hidden="true"></i>&nbsp;
						Agregar
					</button>
				</div>


			</fieldset>

			</div>

		</div>

	</div>

</div>

<?php $this->renderPartial('tabs/obrasconstr/modal/md_construcciones'); ?>
<?php $this->renderPartial('tabs/obrasconstr/modal/md_obras'); ?>
<?php $this->renderPartial('tabs/obrasconstr/modal/md_instalacion'); ?>
<?php $this->renderPartial('tabs/obrasconstr/modal/md_buscar_instalacion'); ?>
