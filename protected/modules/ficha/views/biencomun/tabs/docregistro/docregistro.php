<?php

	$up = new stdClass;

	if ( ! $isOnlyView ) 
	{
		$up = UBiencomun::getCookie(CBiencomun::UP02_NAME_CK);
	}
	else 
	{
		$up = $UP02;
	}

	$notaria 								= ((isset($up->notaria)) ? $up->notaria : NULL);
  $kardex 								= ((isset($up->kardex)) ? $up->kardex : NULL);
  $fecha_inscpublica 			= ((isset($up->fecha_inscpublica)) ? $up->fecha_inscpublica : NULL);
  $tpartida_reg 					= ((isset($up->tpartida_reg)) ? $up->tpartida_reg : NULL);
  $numeros 								= ((isset($up->numeros)) ? $up->numeros : NULL);
  $forja 									= ((isset($up->forja)) ? $up->forja : NULL);
  $asiento 								= ((isset($up->asiento)) ? $up->asiento : NULL);
  $fecha_insc_enregpredio = ((isset($up->fecha_insc_enregpredio)) ? $up->fecha_insc_enregpredio : NULL);
  $decla_fabrica 					= ((isset($up->decla_fabrica)) ? $up->decla_fabrica : NULL);
  $inscfabrica 						= ((isset($up->inscfabrica)) ? $up->inscfabrica : NULL);
  $fecha_inscfabrica 			= ((isset($up->fecha_inscfabrica)) ? $up->fecha_inscfabrica : NULL);
  $enlote_colin 					= isset($up->enlote_colin) ? $up->enlote_colin : null;
  $enjardin_aislam 				= isset($up->enjardin_aislam) ? $up->enjardin_aislam : null;
  $enarea_public 					= isset($up->enarea_public) ? $up->enarea_public : null;
  $enarea_intang 					= isset($up->enarea_intang) ? $up->enarea_intang : null;

?>


<div role="tabpanel" class="tab-pane" id="tab_docregistro">

	<div class="container">

		<div id="rowRecapitulacionEdificios" class="row">

			<div class="col-xs-12">
				<fieldset>
					<legend>Recapitulación edificios</legend>
					<style type="text/css">
						#td-recapedificios .ds-width {min-width: 350px !important }
						#td-recapedificios .config-width {min-width: 80px !important }
						#td-recapedificios tfoot td.totales {
							border:1px solid #444 !important;
							background-color: #f9f9f9;
							text-align: right;
						}
					</style>
					<div class="row">
						<div class="col-xs-12">
							<div class="table-responsive">
								<table id="td-recapedificios" class="table table-hover table-custom">
									<tfoot>
										<tr>
											<td>&nbsp;</td>
											<td id="celltotalporcen" class="totales">&nbsp;</td>
											<td id="celltotalatc" class="totales">&nbsp;</td>
											<td id="celltotalaoic" class="totales">&nbsp;</td>
											<td id="celltotalacc" class="totales">&nbsp;</td>
										</tr>
									</tfoot>
								</table>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12" style="text-align:right;">
							<button type="button" id="btn-add-recapedificios" class="btn grey-salsa">
								<i class="fa fa-refresh" aria-hidden="true"></i>&nbsp;
								Actualizar
							</button>
						</div>
					</div>
				</fieldset>
			</div>

		</div>

		<div id="rowRecapitulacion" class="row">

			<div class="col-xs-12">
				<fieldset>
					<legend>Recapitulación de bienes comunes</legend>
					<style type="text/css">
						#td-recapitulacion .ds-width {min-width: 350px !important }
						#td-recapitulacion .config-width {min-width: 80px !important }
						#td-recapitulacion tfoot td.totales {
							border:1px solid #444 !important;
							background-color: #f9f9f9;
							text-align: center;
						}
					</style>
					<div class="row">
						<div class="col-xs-12">
							<div class="table-responsive">
								<table id="td-recapitulacion" class="table table-hover table-custom">
									<tfoot>
										<tr>
											<td>&nbsp;</td>
											<td>&nbsp;</td>
											<td>&nbsp;</td>
											<td>&nbsp;</td>
											<td>&nbsp;</td>
											<td>&nbsp;</td>
											<td id="celltotalporcen" class="totales">&nbsp;</td>
											<td id="celltotalatc" class="totales">&nbsp;</td>
											<td id="celltotalaoic" class="totales">&nbsp;</td>
											<td id="celltotalacc" class="totales">&nbsp;</td>
											<td>&nbsp;</td>
										</tr>
									</tfoot>
								</table>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12" style="text-align:right;">
							<button type="button" id="btn-add-recapitulacion" class="btn grey-salsa">
								<i class="fa fa-object-group" aria-hidden="true"></i>&nbsp;
								Recapitular
							</button>
						</div>
					</div>
				</fieldset>
			</div>

		</div>

		<fieldset>
			<legend>Area del terreno invadida (M<sup>2</sup>)</legend>
			<div class="row">
				<div class="col-sm-6 col-md-3">
					<div class="form-group">
					  <label for="txtenlote_colin">En el lote colindante</label>
					  <input type="text" id="txtenlote_colin" name="enlote_colin" value="<?= $enlote_colin; ?>" class="form-control datasend only-number" style="text-align: right;">
					</div>
				</div>
				<div class="col-sm-6 col-md-3">
					<div class="form-group">
					  <label for="txtenjardin_aislam">En jardín de aislam.</label>
					  <input type="text" id="txtenjardin_aislam" name="enjardin_aislam" value="<?= $enjardin_aislam; ?>" class="form-control datasend only-number" style="text-align: right;">
					</div>
				</div>
				<div class="col-sm-6 col-md-3">
					<div class="form-group">
					  <label for="txtenarea_public">En área pública</label>
					  <input type="text" id="txtenarea_public" name="enarea_public" value="<?= $enarea_public; ?>" class="form-control datasend only-number" style="text-align: right;">
					</div>
				</div>
				<div class="col-sm-6 col-md-3">
					<div class="form-group">
					  <label for="txtenarea_intang">En área intangible</label>
					  <input type="text" id="txtenarea_intang" name="enarea_intang" value="<?= $enarea_intang; ?>" class="form-control datasend only-number" style="text-align: right;">
					</div>
				</div>
			</div>
		</fieldset>

		<fieldset>

			<legend>Registro Notarial de escritura pública</legend>

			<div class="row">
				<div class="col-sm-5">
					<div class="form-group">
							<label for="txtdisplay_notarias" class="control-label">Nombre de la notaria</label>
						<div class="input-group">
							<span class="input-group-btn">
								<button type="button" id="btn-open-notarias" class="btn btn-default">
									<span class="glyphicon glyphicon-search"></span>
								</button>
							</span>
							<select name="notaria" id="cbonotarias" data-sel="<?= $notaria; ?>" class="form-control datasend">
								<option value="" class="default" selected >Seleccione...</option>
							</select>
						</div>
					</div>
				</div>
				<div class="col-sm-4">
					<div class="form-group">
						<label for="txtkardex" class="control-label">Kardex</label>
						<input type="text" id="txtkardex" name="kardex" value="<?= $kardex; ?>" class="form-control datasend">
					</div>
				</div>
				<div class="col-sm-3">
					<div class="form-group">
						<label for="txtfecha_inscpublica" class="control-label">F. Insc. Pública.</label>
						<input type="text" id="txtfecha_inscpublica" name="fecha_inscpublica" value="<?= $fecha_inscpublica; ?>" class="form-control datasend maskdate" data-inputmask="'alias': 'date'" placeholder="dd/mm/yyyy">
					</div>
				</div>
			</div>

		</fieldset>

		<fieldset>
			<legend>Inscripción del predio catastral en el registro de predios</legend>
			<div class="row">
				<div class="col-sm-4">
					<div class="form-group">
						<label for="txttpartida_reg" class="control-label">Tipo de partida registral</label>
						<select name="tpartida_reg" id="txttpartida_reg" data-sel="<?= $tpartida_reg; ?>" class="form-control datasend autoload" data-type="22" data-sw="3">
							<option value="" class="default" selected >Seleccione una opción...</option>
						</select>
					</div>
				</div>
				<div class="col-sm-2">
					<div class="form-group">
						<label for="txtnumeros" class="control-label">Numeros</label>
						<input type="text" id="txtnumeros" name="numeros" value="<?= $numeros; ?>" class="form-control datasend">
					</div>
				</div>
				<div class="col-sm-2">
					<div class="form-group">
						<label for="txtforja" class="control-label">Fojas</label>
						<input type="text" id="txtforja" name="forja" value="<?= $forja; ?>" class="form-control datasend">
					</div>
				</div>
				<div class="col-sm-2">
					<div class="form-group">
						<label for="txtasiento" class="control-label">Asientos</label>
						<input type="text" id="txtasiento" name="asiento" value="<?= $asiento; ?>" class="form-control datasend">
					</div>
				</div>
				<div class="col-sm-2">
					<div class="form-group">
						<label for="txtfecha_insc_enregpredio" class="control-label">Fecha</label>
						<input type="text" id="txtfecha_insc_enregpredio" name="fecha_insc_enregpredio" value="<?= $fecha_insc_enregpredio; ?>" class="form-control datasend maskdate" data-inputmask="'alias': 'date'" placeholder="dd/mm/yyyy">
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-sm-5">
					<div class="form-group">
						<label for="txtdecla_fabrica" class="control-label">Declaratoria de fabrica</label>
						<select name="decla_fabrica" id="txtdecla_fabrica" data-sel="<?= $decla_fabrica; ?>" class="form-control datasend autoload" data-type="23" data-sw="3">
							<option value="" class="default" selected >Seleccione una opción...</option>
						</select>
					</div>
				</div>
				<div class="col-sm-4">
					<div class="form-group">
						<label for="" class="control-label">Asiento de insc. de fabrica</label>
						<input type="text" id="txtasiento_inscfabrica" name="inscfabrica" value="<?= $inscfabrica; ?>" class="form-control datasend">
					</div>
				</div>
				<div class="col-sm-3">
					<div class="form-group">
						<label for="" class="control-label">Fecha de insc.</label>
						<input type="text" id="txtfecha_inscfabrica" name="fecha_inscfabrica" value="<?= $fecha_inscfabrica; ?>" class="form-control datasend maskdate" data-inputmask="'alias': 'date'" placeholder="dd/mm/yyyy">
					</div>
				</div>
			</div>

		</fieldset>

	</div>

</div>

<?php $this->renderPartial('tabs/docregistro/modal/md_notaria'); ?>

<?php $this->renderPartial('tabs/docregistro/modal/md_buscar_notaria'); ?>

<?php $this->renderPartial('tabs/docregistro/modal/md_actporcentaje'); ?>
