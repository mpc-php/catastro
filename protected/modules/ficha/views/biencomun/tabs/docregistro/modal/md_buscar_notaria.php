<section id="md-notaria" style="display: none;">

  <div class="row">
    <div class="col-xs-12">

      <style type="text/css">

        table#dt-notarias tbody tr:hover {
          background-color: #f0f0f0;
        }

        table#dt-notarias td {
          padding: 0 8px !important;
          vertical-align: middle;
          font-size: 0.85em;
          user-select:none;
          -webkit-user-select:none;
          cursor: pointer;
        }

        .pagination-info { display: none !important; }
        .btn-actions {
          position: relative;
          z-index: 5; 
          margin: auto !important; 
        }

        #cnt-dt-notarias .search {
          display: block !important;
          margin: 0;
          width: 100%;
        }

        #cnt-dt-notarias .search input {
          border: 1px solid #ddd;
          height: 45px;
          width: 100%;
          display: block;
        }

      </style>

      <div id="cnt-dt-notarias">
        <table id="dt-notarias" class="table table-hover table-custom"></table>
      </div>
    </div>
  </div>

  <div class="footer-body align-right" style="padding-top: 0;">

    <button type="submit" id="btn-done" disabled class="btn purple">
      <i class="fa fa-floppy-o" aria-hidden="true"></i>&nbsp;Aceptar
    </button>

    <button type="submit" id="btn-new" disabled class="btn green" style="float: left;">
      <i class="fa fa-plus" aria-hidden="true"></i>&nbsp;Nuevo
    </button>

    <button type="button" id="btn-cancel" class="btn default">
      <i class="fa fa-times" aria-hidden="true"></i>&nbsp;Cancelar
    </button>

  </div>

</section>