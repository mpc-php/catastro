<style type="text/css">
  #td-porcentajes th {
    text-align: right;
    padding-right: 10px;
    width: 110px;
    vertical-align: middle;
  }

  #td-porcentajes td, #td-porcentajes th {
    padding: 2px 4px;
  }
</style>
<section id="md-actporcentaje" style="display:none;">

  <div class="row">
    <div class="col-xs-12">
      <table id="td-porcentajes" class="table">
        <tbody>
          <tr>
            <th>N° Ficha</th>
            <td><span id="lblnro_ficha"></span></td>
          </tr>
          <tr>
            <th>Edificación</th>
            <td><span id="lbledificacion"></span></td>
          </tr>
          <tr>
            <th>Entrada</th>
            <td><span id="lblentrada"></span></td>
          </tr>
          <tr>
            <th>Piso</th>
            <td><span id="lblpiso"></span></td>
          </tr>
          <tr>
            <th>Unidad</th>
            <td><span id="lblunidad"></span></td>
          </tr>
          <tr>
            <th>% A. Terreno</th>
            <td><input type="text" id="txtporcentaje" class="form-control"></td>
          </tr>
        </tbody>
      </table>
    </div>
  </div>

  <div class="footer-body align-right">

    <button type="submit" id="btn-done" disabled class="btn purple">
      <i class="fa fa-floppy-o" aria-hidden="true"></i>&nbsp;Aceptar
    </button>

    <button type="button" id="btn-close" class="btn default" >
      <i class="fa fa-times" aria-hidden="true"></i>&nbsp;Cerrar
    </button>

  </div>

</section>