<section id="md-mng-notarias" style="display:none;">

  <div class="row">
    <div class="col-xs-12 form-group">
      <label for="txtname" class="control-label">Nombre</label>
      <input type="text" id="txtname" name="name" maxlength="100" class="form-control datasend_notaria" required>
    </div>

    <div class="col-xs-12 form-group">
      <label for="cbodepartamento" class="control-label">Departamento</label>
      <select id="cbodepartamento" name="departamento" class="form-control" required>
        <option value="00" selected disabled>Seleccione...</option>
      </select>
    </div>

    <div class="col-xs-12 form-group">
      <label for="cboprovincia" class="control-label">Provincia</label>
      <select id="cboprovincia" name="provincia" class="form-control" required disabled>
        <option value="0000" selected disabled>Seleccione...</option>
      </select>
    </div>

    <div class="col-xs-12 form-group">
      <label for="cbodistrito" class="control-label">Distrito</label>
      <select id="cbodistrito" name="ubigeo" class="form-control datasend_notaria" required disabled>
        <option value="000000" selected disabled>Seleccione...</option>
      </select>
    </div>
  </div>

  <div class="footer-body align-right">
    <button type="submit" id="btn-done" disabled class="btn purple">
      <i class="fa fa-floppy-o" aria-hidden="true"></i>&nbsp;Aceptar
    </button>
    <button type="button" id="btn-close" class="btn default" >
      <i class="fa fa-times" aria-hidden="true"></i>&nbsp;Cerrar
    </button>
  </div>

</section>