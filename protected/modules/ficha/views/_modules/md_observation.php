<style type="text/css">

	#md-observation .checkbox label {
		padding-left: 20px;
	}

	#md-observation table td {
		padding: 0 !important;
		vertical-align: middle;
	}

	#td-description th {
		text-align: right;
		vertical-align: middle;
		color: #444;
		width: 150px;
	}

</style>

<section id="md-observation" style="display:none;">

	<table id="td-description" class="table" style="margin-bottom: 20px;">
		<tbody>
			<tr>
				<th>N° Ficha:</th>
				<td><span id="lblficha"></span></td>
			</tr>
			<tr>
				<th>ID Ficha:</th>
				<td><span id="lblidficha"></span></td>
			</tr>
			<tr>
				<th>C.R. Catastral:</th>
				<td><span id="lblcodref_catastral"></span></td>
			</tr>
		</tbody>
	</table>

	<div class="row">
		<div class="col-xs-12">
			<div class="form-group">
				<label for="txtobservaciones">Observación general</label>
				<textarea id="txtobservaciones" name="observaciones" class="form-control datasend" style="min-height: 80px;" placeholder="Describe una observación para toda la ficha"></textarea>
			</div>
		</div>
	</div>
	
	<fieldset>
		<legend>Campos requeridos</legend>
		<div class="row">
			<div class="col-xs-12">
				<table id="td-campos" class="table">
					<thead>
						<tr>
							<th>Campo observado</th>
							<th>Descripción de lo observado</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td colspan="2" style="text-align:center;">
								Cargando campos requeridos de la ficha...
							</td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
	</fieldset>

	<div class="footer-body align-right">

		<button type="button" id="btn-done" disabled class="btn purple">
			<i class="fa fa-floppy-o" aria-hidden="true"></i>&nbsp;Aceptar
		</button>

		<button type="button" id="btn-close" class="btn default" style="float:left" >
			<i class="fa fa-times" aria-hidden="true"></i>&nbsp;Cerrar
		</button>

	</div>
	
</section>