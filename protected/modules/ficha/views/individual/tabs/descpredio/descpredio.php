<?php

	$up = new stdClass;

	if ( ! $isOnlyView )
	{
		$up = UIndividual::getCookie('FIUP01');
	}
	else
	{
		$up = $UP01;
	}

	$clasif_predio = isset($up->clasif_predio) ? $up->clasif_predio : NULL;
  $pred_catastral = isset($up->pred_catastral) ? $up->pred_catastral : NULL;
  $uso_predio = isset($up->uso_predio) ? $up->uso_predio : NULL;
  $estructuracion = isset($up->estructuracion) ? $up->estructuracion : NULL;
  $zonificacion = isset($up->zonificacion) ? $up->zonificacion : NULL;
  $area_titulo = isset($up->area_titulo) ? $up->area_titulo : NULL;
  $area_declarada = isset($up->area_declarada) ? $up->area_declarada : NULL;
  $area_verificada = isset($up->area_verificada) ? $up->area_verificada : NULL;
  $area_ocupada = isset($up->area_ocupada) ? $up->area_ocupada : NULL;
  $medcamp_frente = isset($up->medcamp_frente) ? $up->medcamp_frente : NULL;
  $medcamp_derecha = isset($up->medcamp_derecha) ? $up->medcamp_derecha : NULL;
  $medcamp_izquierda = isset($up->medcamp_izquierda) ? $up->medcamp_izquierda : NULL;
  $medcamp_fondo = isset($up->medcamp_fondo) ? $up->medcamp_fondo : NULL;
  $medtit_frente = isset($up->medtit_frente) ? $up->medtit_frente : NULL;
  $medtit_derecha = isset($up->medtit_derecha) ? $up->medtit_derecha : NULL;
  $medtit_izquierda = isset($up->medtit_izquierda) ? $up->medtit_izquierda : NULL;
  $medtit_fondo = isset($up->medtit_fondo) ? $up->medtit_fondo : NULL;
  $colcampo_frente = isset($up->colcampo_frente) ? $up->colcampo_frente : NULL;
  $colcampo_derecha = isset($up->colcampo_derecha) ? $up->colcampo_derecha : NULL;
  $colcampo_izquierda = isset($up->colcampo_izquierda) ? $up->colcampo_izquierda : NULL;
  $colcampo_fondo = isset($up->colcampo_fondo) ? $up->colcampo_fondo : NULL;
  $coltit_frente = isset($up->coltit_frente) ? $up->coltit_frente : NULL;
  $coltit_derecha = isset($up->coltit_derecha) ? $up->coltit_derecha : NULL;
  $coltit_izquierda = isset($up->coltit_izquierda) ? $up->coltit_izquierda : NULL;
  $coltit_fondo = isset($up->coltit_fondo) ? $up->coltit_fondo : NULL;
  $limpfrente_izquierda = isset($up->limpfrente_izquierda) ? $up->limpfrente_izquierda : NULL;
  $limpfrente_derecha = isset($up->limpfrente_derecha) ? $up->limpfrente_derecha : NULL;
  $limpfrente_fondo = isset($up->limpfrente_fondo) ? $up->limpfrente_fondo : NULL;
  $limpfrente_frente = isset($up->limpfrente_frente) ? $up->limpfrente_frente : NULL;
  $total_limpfrenteuc = isset($up->total_limpfrenteuc) ? $up->total_limpfrenteuc : 0;
  $luz = isset($up->luz) ? $up->luz : NULL;
  $sumluz = isset($up->sumluz) ? $up->sumluz : NULL;
  $agua = isset($up->agua) ? $up->agua : NULL;
  $sumagua = isset($up->sumagua) ? $up->sumagua : NULL;
  $phone = isset($up->phone) ? $up->phone : NULL;
  $sumphone = isset($up->sumphone) ? $up->sumphone : NULL;
  $desague = isset($up->desague) ? $up->desague : NULL;
  $gas = isset($up->gas) ? $up->gas : NULL;
  $cable = isset($up->cable) ? $up->cable : NULL;
  $internet = isset($up->internet) ? $up->internet : NULL;

?>

<div role="tabpanel" class="tab-pane" id="tab_descpredio">

	<div class="container" >

		<div class="row">

			<div class="col-sm-3">
				<div class="form-group">
					<label for="cboclasif_predio" class="control-label">Clasificación del predio</label>
					<select name="clasif_predio" id="cboclasif_predio" data-sel="<?= $clasif_predio; ?>" class="form-control datasend autoload" data-type="15" data-sw="3">
						<option value="" class="default" selected>Escoja una opción...</option>
					</select>
				</div>
			</div>
			<div class="col-sm-3">
				<div class="form-group">
					<label for="cbopred_catastral" class="control-label">Predio catastral en:</label>
					<select name="pred_catastral" id="cbopred_catastral" data-sel="<?= $pred_catastral; ?>" class="form-control datasend autoload" data-type="16" data-sw="3">
						<option value="" class="default" selected>Escoja una opción...</option>
					</select>
				</div>
			</div>

			<div class="col-sm-2">
				<div class="form-group">
					<label for="txtcond_uso" class="control-label">Cod. Uso</label>
					<input type="text" name="cond_uso" id="txtcond_uso" value="<?= $uso_predio; ?>" class="form-control only-number">
				</div>
			</div>

			<div class="col-sm-4">
				<div class="form-group">
					<label for="cbouso_predio" class="control-label">Uso predio catastral<small>(Descripción)</small></label>
					<select name="uso_predio" id="cbouso_predio" data-sel="<?= $uso_predio; ?>" class="form-control datasend autoload" data-type="99" data-sw="0">
						<option value="" class="default" selected>Escoja una opción...</option>
					</select>
				</div>
			</div>

		</div>

		<div class="row">

			<div class="col-sm-1">
				<div class="form-group">
					<label for="txtestructuracion" class="control-label" title="Estructuración">Estruc.</label>
					<input type="text" name="estructuracion" id="txtestructuracion" title="Estructuración" value="<?= $estructuracion; ?>" class="form-control datasend">
				</div>
			</div>

			<div class="col-sm-3">
				<div class="form-group">
					<label for="cbozonificacion" class="control-label">Zonificación</label>
					<select name="zonificacion" id="cbozonificacion" data-sel="<?= $zonificacion; ?>" class="form-control datasend">
						<option value="" class="default" selected>Escoja una opción...</option>
					</select>
				</div>
			</div>

			<div class="col-sm-2">
				<div class="form-group">
					<label for="" class="control-label"><small>A. T. Titulo<sup>M2</sup></small></label>
					<input type="text" id="txtarea_titulo" name="area_titulo" value="<?= $area_titulo; ?>" class="form-control only-number datasend">
				</div>
			</div>
			<div class="col-sm-2">
				<div class="form-group">
					<label for="" class="control-label"><small>A. T. Declarada<sup>M2</sup></small></label>
					<input type="text" id="txtarea_declarada" name="area_declarada" value="<?= $area_declarada; ?>" class="form-control only-number datasend">
				</div>
			</div>
			<div class="col-sm-2">
				<div class="form-group">
					<label for="" class="control-label"><small>A. T. Verifada<sup>M2</sup></small></label>
					<input type="text" id="txtarea_verificada" name="area_verificada" value="<?= $area_verificada; ?>" class="form-control only-number datasend">
				</div>
			</div>
			<div class="col-sm-2">
				<div class="form-group">
					<label for="" class="control-label"><small>A. Ocupada<sup>M2</sup></small></label>
					<input type="text" id="txtarea_ocupada" name="area_ocupada" value="<?= $area_ocupada; ?>" class="form-control only-number datasend">
				</div>
			</div>

		</div>

		<div class="row">
			<div class="col-xs-12">
				<style type="text/css">
					.in-table {
						border: none;
					}

					.table-DP td {
						padding: 0 !important;
						border: 1px solid #ddd;
					}

					.table-DP tbody th {
						text-align: right;
					}

					.table-DP thead tr th {
						text-align: center;
						font-size: 0.75em;
					}
				</style>
				<fieldset>
					<legend>Linderos y Colindancias</legend>
					<div class="row">
						<div class="col-xs-12">
							<table class="table table-DP">
								<thead>
									<tr>
										<th>Linderos del Lote (ML)</th>
										<th>Medida en Campo</th>
										<th>Medida según Titulo</th>
										<th>Colindancias en campo</th>
										<th>Colindancias según Tit.</th>
										<th colspan="2">Limpieza frente de lote (ML)</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<th>Frente</th>
										<td><input type="text" id="txtmedcamp_frente" data-down="txtmedcamp_derecha" data-right="txtmedtit_frente" name="medcamp_frente" value="<?= $medcamp_frente; ?>" style="text-align: right;" class="form-control in-table datasend"></td>
										<td><input type="text" id="txtmedtit_frente" data-down="txtmedtit_derecha" data-left="txtmedcamp_frente" data-right="txtcolcampo_frente" name="medtit_frente" value="<?= $medtit_frente; ?>" style="text-align: right;" class="form-control in-table datasend"></td>
										<td><input type="text" id="txtcolcampo_frente" data-down="txtcolcampo_derecha" data-left="txtmedtit_frente" data-right="txtcoltit_frente" name="colcampo_frente" value="<?= $colcampo_frente; ?>" class="form-control in-table datasend"></td>
										<td><input type="text" id="txtcoltit_frente" data-down="txtcoltit_derecha" data-left="txtcolcampo_frente" data-right="txtlimpfrentelt_frente" name="coltit_frente" value="<?= $coltit_frente; ?>" class="form-control in-table datasend"></td>
										<td><input type="text" id="txtlimpfrentelt_frente" data-down="txtlimpfrentelt_derecha" data-left="txtcoltit_frente" name="limpfrente_frente" value="<?= $limpfrente_frente; ?>" style="text-align: right;" class="form-control in-table datasend only-number calctotaluc"></td>
										<td rowspan="3" style="width:120px; vertical-align:middle; text-align:center;">Limpieza frente del lote de la UC (ML)</td>
									</tr>
									<tr>
										<th>Derecha</th>
										<td><input type="text" id="txtmedcamp_derecha" name="medcamp_derecha" data-up="txtmedcamp_frente" data-down="txtmedcamp_izquierda" data-right="txtmedtit_derecha" value="<?= $medcamp_derecha; ?>" style="text-align: right;" class="form-control in-table datasend"></td>
										<td><input type="text" id="txtmedtit_derecha" name="medtit_derecha" data-left="txtmedcamp_derecha" data-up="txtmedtit_frente" data-down="txtmedtit_izquierda" data-right="txtcolcampo_derecha" value="<?= $medtit_derecha; ?>" style="text-align: right;" class="form-control in-table datasend"></td>
										<td><input type="text" id="txtcolcampo_derecha" name="colcampo_derecha" data-left="txtmedtit_derecha" data-up="txtcolcampo_frente" data-down="txtcolcampo_izquierda" data-right="txtcoltit_derecha" value="<?= $colcampo_derecha; ?>" class="form-control in-table datasend"></td>
										<td><input type="text" id="txtcoltit_derecha" name="coltit_derecha" data-left="txtcolcampo_derecha" data-up="txtcoltit_frente" data-down="txtcoltit_izquierda" data-right="txtlimpfrentelt_derecha" value="<?= $coltit_derecha; ?>" class="form-control in-table datasend"></td>
										<td><input type="text" id="txtlimpfrentelt_derecha" name="limpfrente_derecha" data-left="txtcoltit_derecha" data-up="txtlimpfrentelt_frente" data-down="txtlimpfrentelt_izquierda" value="<?= $limpfrente_derecha; ?>" style="text-align: right;" class="form-control in-table datasend only-number calctotaluc"></td>
									</tr>
									<tr>
										<th>Izquierda</th>
										<td><input type="text" id="txtmedcamp_izquierda" name="medcamp_izquierda" data-up="txtmedcamp_derecha" data-down="txtmedcamp_fondo" data-right="txtmedtit_izquierda" value="<?= $medcamp_izquierda; ?>" style="text-align: right;" class="form-control in-table datasend"></td>
										<td><input type="text" id="txtmedtit_izquierda" name="medtit_izquierda" data-left="txtmedcamp_izquierda" data-up="txtmedtit_derecha" data-down="txtmedtit_fondo" data-right="txtcolcampo_izquierda" value="<?= $medtit_izquierda; ?>" style="text-align: right;" class="form-control in-table datasend"></td>
										<td><input type="text" id="txtcolcampo_izquierda" name="colcampo_izquierda" data-left="txtmedtit_izquierda" data-up="txtcolcampo_derecha" data-down="txtcolcampo_fondo" data-right="txtcoltit_izquierda" value="<?= $colcampo_izquierda; ?>" class="form-control in-table datasend"></td>
										<td><input type="text" id="txtcoltit_izquierda" name="coltit_izquierda" data-left="txtcolcampo_izquierda" data-up="txtcoltit_derecha" data-down="txtcoltit_fondo" data-right="txtlimpfrentelt_izquierda" value="<?= $coltit_izquierda; ?>" class="form-control in-table datasend"></td>
										<td><input type="text" id="txtlimpfrentelt_izquierda" name="limpfrente_izquierda" data-left="txtcoltit_izquierda" data-up="txtlimpfrentelt_derecha" data-down="txtlimpfrentelt_fondo" value="<?= $limpfrente_izquierda; ?>" style="text-align: right;" class="form-control in-table datasend only-number calctotaluc"></td>
									</tr>
									<tr>
										<th>Fondo</th>
										<td><input type="text" id="txtmedcamp_fondo" name="medcamp_fondo" data-up="txtmedcamp_izquierda" data-right="txtmedtit_fondo" value="<?= $medcamp_fondo; ?>" style="text-align: right;" class="form-control in-table datasend"></td>
										<td><input type="text" id="txtmedtit_fondo" name="medtit_fondo" data-left="txtmedcamp_fondo" data-up="txtmedtit_izquierda" data-right="txtcolcampo_fondo" value="<?= $medtit_fondo; ?>" style="text-align: right;" class="form-control in-table datasend"></td>
										<td><input type="text" id="txtcolcampo_fondo" name="colcampo_fondo" data-left="txtmedtit_fondo" data-up="txtcolcampo_izquierda" data-right="txtcoltit_fondo" value="<?= $colcampo_fondo; ?>" class="form-control in-table datasend"></td>
										<td><input type="text" id="txtcoltit_fondo" name="coltit_fondo" data-left="txtcolcampo_fondo" data-up="txtcoltit_izquierda" data-right="txtlimpfrentelt_fondo" value="<?= $coltit_fondo; ?>" class="form-control in-table datasend"></td>
										<td><input type="text" id="txtlimpfrentelt_fondo" name="limpfrente_fondo" data-left="txtcoltit_fondo" data-up="txtlimpfrentelt_izquierda" value="<?= $limpfrente_fondo; ?>" style="text-align: right;" class="form-control in-table datasend only-number calctotaluc"></td>
										<td><input type="text" id="txttotal_limpfrenteuc" name="total_limpfrenteuc" value="<?= $total_limpfrenteuc; ?>" style="text-align: right;" value="0" class="form-control in-table datasend" readonly></td>
									</tr>
								</tbody>
							</table>
							<small><b style="color:red">*</b> Shift + (Tecla de direcciones) = Moverse entre celdas</small>
						</div>
					</div>
				</fieldset>
			</div>
		</div>

		<div class="row">
			<div class="col-xs-12">
				<fieldset>
					<legend>Servicios basicos</legend>
					<div class="col-sm-4 col-md-3 col-lg-2">
						<div class="form-group">
							<label for="" class="control-label">N° Sum. Luz</label>
							<div class="input-group">
								<span class="input-group-addon">
									<input type="checkbox" name="luz" id="chkbluz" <?= ($luz == 1) ? 'checked' : '' ?> data-activate="#txtsumluz" class="datasend activated">
								</span>
								<input type="text" id="txtsumluz" name="sumluz" value="<?= $sumluz; ?>" class="form-control datasend" <?= ($sumluz == '') ? 'disabled' : '' ?>>
							</div>
						</div>
					</div>
					<div class="col-sm-4 col-md-3 col-lg-2">
						<div class="form-group">
							<label for="" class="control-label">N° Sum. Agua</label>
							<div class="input-group">
								<span class="input-group-addon">
									<input type="checkbox" name="agua" id="chkbagua" <?= ($agua == 1) ? 'checked' : '' ?> data-activate="#txtsumagua" class="datasend activated">
								</span>
								<input type="text" id="txtsumagua" name="sumagua" value="<?= $sumagua; ?>" class="form-control datasend"  <?= ($sumagua == '') ? 'disabled' : '' ?>>
							</div>
						</div>
					</div>
					<div class="col-sm-4 col-md-3 col-lg-2">
						<div class="form-group">
							<label for="" class="control-label">N° Sum. Teléfono</label>
							<div class="input-group">
								<span class="input-group-addon">
									<input type="checkbox" id="chkbphone" name="phone" <?= ($phone == 1) ? 'checked' : '' ?> data-activate="#txtsumphone" class="datasend activated">
								</span>
								<input type="text" id="txtsumphone" name="sumphone" value="<?= $sumphone; ?>" class="form-control datasend"  <?= ($sumphone == '') ? 'disabled' : '' ?>>
							</div>
						</div>
					</div>
					<div class="col-sm-12 col-md-3 col-lg-6">
						<div class="form-group">
							<label class="control-label">&nbsp;</label>
							<table style="width:100%">
								<tbody>
									<tr>
										<td style="width:25%; text-align:center;">
											<div class="checkbox">
												<label><input type="checkbox" id="chkbdesague" name="desague" <?= ($desague == 1) ? 'checked' : '' ?> class="datasend">Desague</label>
											</div>
										</td>
										<td style="width:25%; text-align:center;">
											<div class="checkbox">
												<label><input type="checkbox" id="chkbgas" name="gas" <?= ($gas == 1) ? 'checked' : '' ?> class="datasend">Gas</label>
											</div>
										</td>
										<td style="width:25%; text-align:center;">
											<div class="checkbox">
												<label><input type="checkbox" id="chkbcable" name="cable" <?= ($cable == 1) ? 'checked' : '' ?> class="datasend">Cable</label>
											</div>
										</td>
										<td style="width:25%; text-align:center;">
											<div class="checkbox">
												<label><input type="checkbox" id="chkbinternet" name="internet" <?= ($internet == 1) ? 'checked' : '' ?> class="datasend">Internet</label>
											</div>
										</td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</fieldset>
			</div>
		</div>

	</div>


</div>
