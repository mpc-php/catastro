<?php

	$up = new stdClass;
	$sec = new stdClass;

	if ( ! $isOnlyView ) 
	{
		$up = UIndividual::getCookie('FIUP02');
		$sec = UIndividual::getCookie('FIUP01');
	}
	else 
	{
		$up = $UP02;
		$sec = $UP01;
	} 

	$evalpredio = isset($up->evalpredio) ? $up->evalpredio : null;
  $enlote_colin = isset($up->enlote_colin) ? $up->enlote_colin : null;
  $enjardin_aislam = isset($up->enjardin_aislam) ? $up->enjardin_aislam : null;
  $enarea_public = isset($up->enarea_public) ? $up->enarea_public : null;
  $enarea_intang = isset($up->enarea_intang) ? $up->enarea_intang : null;
  $cond_declara = isset($up->cond_declara) ? $up->cond_declara : null;
  $estado_llenado = isset($up->estado_llenado) ? $up->estado_llenado : null;
  $nro_habitantes = isset($up->nro_habitantes) ? $up->nro_habitantes : null;
  $nro_familias = isset($up->nro_familias) ? $up->nro_familias : null;
  $mantenimiento = isset($up->mantenimiento) ? $up->mantenimiento : null;
  $observaciones = isset($up->observaciones) ? $up->observaciones : null;
  $nro_hombres = isset($sec->nro_hombres) ? $sec->nro_hombres : null;
  $nro_mujeres = isset($sec->nro_mujeres) ? $sec->nro_mujeres : null;
  $nro_ninios = isset($sec->nro_ninios) ? $sec->nro_ninios : null;
  $nro_adulmayor = isset($sec->nro_adulmayor) ? $sec->nro_adulmayor : null;
  $nro_discapacita = isset($sec->nro_discapacita) ? $sec->nro_discapacita : null;

?>

<div role="tabpanel" class="tab-pane" id="tab_infcomplementaria">

	<div class="container">

		<div class="row">

			<div class="col-xs-12">

				<fieldset>
					<legend>Evaluación del predio catastral</legend>

						<style type="text/css">
							.radio label { padding-left: 20px; }
						</style>

						<div class="row">
							<div class="col-xs-12">
								<fieldset>
									<legend>Evaluación del predio</legend>

									<div class="row">
										<div class="col-sm-6 col-md-3">

											<div class="radio">
											  <label>
											    <input type="radio" id="rbopomiso" name="evalpredio" value="01" <?= ($evalpredio == '01') ? 'checked' : '' ?> class="datasend">
											    Predio catastral omiso
											  </label>
											</div>

										</div>
										<div class="col-sm-6 col-md-3">

											<div class="radio">
											  <label>
											    <input type="radio" id="psobrevaluado" name="evalpredio" value="03" <?= ($evalpredio == '03') ? 'checked' : '' ?> class="datasend">
											    Predio catastral sobrevaluado
											  </label>
											</div>

										</div>
										<div class="col-sm-6 col-md-3">

											<div class="radio">
											  <label>
											    <input type="radio" id="psubvaluado" name="evalpredio" value="02" <?= ($evalpredio == '02') ? 'checked' : '' ?> class="datasend">
											    Predio catastral subvaluado
											  </label>
											</div>

										</div>
										<div class="col-sm-6 col-md-3">

											<div class="radio">
											  <label>
											    <input type="radio" id="pconforme" name="evalpredio" value="04" <?= ($evalpredio == '04') ? 'checked' : '' ?> class="datasend">
											    Predio catastral conforme
											  </label>
											</div>

										</div>
									</div>
								</fieldset>
							</div>
						</div>

						<div class="row">
							<div class="col-xs-12">
								<fieldset>
									<legend>Area del terreno invadida (M<sup>2</sup>)</legend>
									<div class="row">
										<div class="col-sm-6 col-md-3">
											<div class="form-group">
											  <label for="txtenlote_colin">En el lote colindante</label>
											  <input type="text" id="txtenlote_colin" name="enlote_colin" value="<?= $enlote_colin; ?>" class="form-control datasend only-number" dir="rtl" style="text-align: right;">
											</div>
										</div>
										<div class="col-sm-6 col-md-3">
											<div class="form-group">
											  <label for="txtenjardin_aislam">En jardín de aislam.</label>
											  <input type="text" id="txtenjardin_aislam" name="enjardin_aislam" value="<?= $enjardin_aislam; ?>" class="form-control datasend only-number" dir="rtl" style="text-align: right;">
											</div>
										</div>
										<div class="col-sm-6 col-md-3">
											<div class="form-group">
											  <label for="txtenarea_public">En área pública</label>
											  <input type="text" id="txtenarea_public" name="enarea_public" value="<?= $enarea_public; ?>" class="form-control datasend only-number" dir="rtl" style="text-align: right;">
											</div>
										</div>
										<div class="col-sm-6 col-md-3">
											<div class="form-group">
											  <label for="txtenarea_intang">En área intangible</label>
											  <input type="text" id="txtenarea_intang" name="enarea_intang" value="<?= $enarea_intang; ?>" class="form-control datasend only-number" dir="rtl" style="text-align: right;">
											</div>
										</div>
									</div>
								</fieldset>
							</div>
						</div>

				</fieldset>

			</div>

		</div>

		<div class="row">
			<div class="col-xs-12">
				<fieldset>
					<legend>Información complementaria</legend>
					<div class="row">
						<div class="col-sm-4">
							<div class="form-group">
								<label for="cbocond_declara" class="control-label">Cond. Declarante</label>
								<select name="cond_declara" id="cbocond_declara" data-sel="<?= $cond_declara; ?>" class="form-control datasend autoload" data-type="25" data-sw="3">
									<option value="" class="default" selected>Seleccione una opción...</option>
								</select>
							</div>
						</div>
						<div class="col-sm-4">
							<div class="form-group">
								<label for="cboestado_llenado" class="control-label">Estado de llenado</label>
								<select name="estado_llenado" id="cboestado_llenado" data-sel="<?= $estado_llenado; ?>" class="form-control datasend autoload" data-type="26" data-sw="3">
									<option value="" class="default" selected>Seleccione una opción...</option>
								</select>
							</div>
						</div>
						<div class="col-sm-2">
							<div class="form-group">
								<label for="txtnro_habitantes" class="control-label">N° habitantes</label>
								<input type="text" id="txtnro_habitantes" name="nro_habitantes" value="<?= $nro_habitantes; ?>" class="form-control datasend only-number">
							</div>
						</div>
						<div class="col-sm-2">
							<div class="form-group">
								<label for="txtnro_familias" class="control-label">N° familias</label>
								<input type="text" id="txtnro_familias" name="nro_familias" value="<?= $nro_familias; ?>" class="form-control datasend only-number">
							</div>
						</div>
					</div>

					<div class="row">
						<div class="col-sm-4 col-md-3">
							<div class="form-group">
								<label for="cbomantenimiento" class="control-label">Mantenimiento</label>
								<select name="mantenimiento" id="cbomantenimiento" data-sel="<?= $mantenimiento; ?>" class="form-control datasend autoload" data-type="27" data-sw="3">
									<option value="" class="default" selected>Seleccione una opción...</option>
								</select>
							</div>
						</div>
						<div class="col-sm-4 col-md-2">
							<div class="form-group">
								<label for="txtnro_hombres" class="control-label">N° Hombres</label>
								<input type="text" id="txtnro_hombres" name="nro_hombres" value="<?= $nro_hombres; ?>" class="form-control datasend only-number">
							</div>
						</div>
						<div class="col-sm-4 col-md-2">
							<div class="form-group">
								<label for="txtnro_mujeres" class="control-label">N° Mujeres</label>
								<input type="text" id="txtnro_mujeres" name="nro_mujeres" value="<?= $nro_mujeres; ?>" class="form-control datasend only-number">
							</div>
						</div>
						<div class="col-sm-4 col-md-1">
							<div class="form-group">
								<label for="txtnro_ninios" class="control-label">N° Niños</label>
								<input type="text" id="txtnro_ninios" name="nro_ninios" value="<?= $nro_ninios; ?>" class="form-control datasend only-number">
							</div>
						</div>
						<div class="col-sm-4 col-md-2">
							<div class="form-group">
								<label for="txtnro_adulmayor" class="control-label">N° Adulto mayor</label>
								<input type="text" id="txtnro_adulmayor" name="nro_adulmayor" value="<?= $nro_adulmayor; ?>" class="form-control datasend only-number">
							</div>
						</div>
						<div class="col-sm-4 col-md-2">
							<div class="form-group">
								<label for="txtnro_discapacita" class="control-label">N° De discapacitados</label>
								<input type="text" id="txtnro_discapacita" name="nro_discapacita" value="<?= $nro_discapacita; ?>" class="form-control datasend only-number">
							</div>
						</div>
					</div>
				</fieldset>
			</div>
		</div>

		<div class="row">
			<div class="col-xs-12">
				<div class="form-group">
					<label for="txtobservaciones" class="control-label">Observaciones</label>
					<textarea id="txtobservaciones" name="observaciones" class="form-control datasend"><?= $observaciones; ?></textarea>
				</div>
			</div>
		</div>

	</div>

</div>
