<section id="md-add-instalacion" style="display:none;">

	<form id="frm-add-instalacion" action="" method="post" autocomplete="off" spellcheck="off">

		<div class="row">

			<div class="col-xs-6 col-md-6 form-group">
				<label for="txtcode" class="control-label">Codigo</label>
				<input type="text" id="txtcode" name="code" maxlength="3" class="form-control" required disabled>
			</div>

			<div class="col-xs-6 col-md-6 form-group">
				<label for="txtname" class="control-label">Descripcion</label>
				<input type="text" id="txtname" name="description" maxlength="100" class="form-control" required disabled>
			</div>

		</div>

		<div class="row">

			<div class="col-xs-6 col-md-6 form-group">
				<label for="txtunit" class="control-label">U. de Medida</label>
				<input type="text" id="txtunit" name="unit" maxlength="12" class="form-control" required disabled>
			</div>

			<div class="col-xs-6 col-md-6 form-group">
				<label for="txtmaterial" class="control-label">Material de Medida</label>
				<input type="text" id="txtmaterial" name="material" maxlength="50" class="form-control" required disabled>
			</div>

		</div>

		<style type="text/css">
			.mychecks .checkbox > label {
				padding-left: 20px !important;
			}			
		</style>

		<div class="row mychecks">

			<div class="col-xs-3 col-md-3 form-group">
				<div class="checkbox">
					<label for="chkcalculo">
						<input type="checkbox" id="chkcalculo" name="calculo" disabled>
						Calculo
					</label>
				</div>
			</div>

			<div class="col-xs-3 col-md-3 form-group">
				<div class="checkbox">
					<label for="chklargo">
						<input type="checkbox" id="chklargo" name="largo" disabled>
						Largo
					</label>
				</div>
			</div>

			<div class="col-xs-3 col-md-3 form-group">
				<div class="checkbox">
					<label for="chkancho">
						<input type="checkbox" id="chkancho" name="ancho" disabled>
						Ancho
					</label>
				</div>
			</div>

			<div class="col-xs-3 col-md-3 form-group">
				<div class="checkbox">
					<label for="chkalto">
						<input type="checkbox" id="chkalto" name="alto" disabled>
						Alto
					</label>
				</div>
			</div>

		</div>

		<div class="footer-body align-right">

			<button type="submit" id="btn-done" disabled class="btn purple">
				<i class="fa fa-floppy-o" aria-hidden="true"></i>&nbsp;Aceptar
			</button>

			<button type="button" id="btn-close" class="btn default" >
				<i class="fa fa-times" aria-hidden="true"></i>&nbsp;Cerrar
			</button>

		</div>

	</form>

</section>