<style type="text/css">

	.letter-category, .checkdate {
		text-align: center;
		text-transform: uppercase;
	}

	.letter-2 {
		padding-left: 6px;
		padding-right: 6px;
	}

	#md-construcciones fieldset {
		padding-bottom: 0 !important;
	}

	#md-construcciones fieldset div[class*="col-"] {
		padding-left: 7.5px;
		padding-right: 7.5px;
	}

</style>
<section id="md-construcciones" style="display:none;">

	<fieldset style="padding-bottom: 0; margin-bottom: 0;">

		<legend>Construcciones</legend>

		<div class="row">

			<div class="col-sm-1 form-group">
				<label for="txtnro_piso" class="control-label"><small>N° Piso</small></label>
				<input type="text" id="txtnro_piso" name="nro_piso" maxlength="2" class="form-control md-datasend letter-2" required disabled>
			</div>

			<div class="col-sm-3 form-group">
				<label for="txtnro_documento" class="control-label">F. Construcción</label>
				<table>
					<tbody>
						<tr>
							<td><input type="text" id="txtmes" name="mes" maxlength="2" class="form-control md-datasend only-number checkdate formater" data-format="00" data-type="mes" placeholder="Mes" required disabled></td>
							<td><input type="text" id="txtanio" name="anio" maxlength="4" class="form-control md-datasend only-number checkdate" data-type="año" placeholder="Año" required disabled></td>
						</tr>
					</tbody>
				</table>
			</div>

			<div class="col-sm-3 form-group">
				<label for="cbomep" class="control-label">MEP</label>
				<select id="cbomep" name="mep" data-type="17" data-sw="3" class="form-control md-datasend autoload" required>
					<option value="" class="default" selected>Seleccione...</option>
				</select>
			</div>

			<div class="col-sm-2 form-group">
				<label for="cboecs" class="control-label">ECS</label>
				<select id="cboecs" name="ecs" data-type="18" data-sw="3" class="form-control md-datasend autoload" required>
					<option value="" class="default" selected>Seleccione...</option>
				</select>
			</div>

			<div class="col-sm-3 form-group">
				<label for="cboecc" class="control-label">ECC</label>
				<select id="cboecc" name="ecc" data-type="19" data-sw="3" class="form-control md-datasend autoload" required>
					<option value="" class="default" selected>Seleccione...</option>
				</select>
			</div>

			<div class="row">
				<div class="col-xs-12">
					<fieldset>
						<legend>Estructura</legend>
						<div class="row">
							<div class="col-sm-3 form-group">
								<label for="txtmuros_col" class="control-label">Muros y columnas</label>
								<input type="text" id="txtmuros_col" name="muros_col" maxlength="1" class="form-control md-datasend letter-category" required disabled>
							</div>
							<div class="col-sm-3 form-group">
								<label for="txttecho" class="control-label">Techos</label>
								<input type="text" id="txttecho" name="techo" maxlength="1" class="form-control md-datasend letter-category" required disabled>
							</div>
						</div>
					</fieldset>

					<fieldset>
						<legend>Acabados</legend>
						<div class="row">
							<div class="col-sm-2 form-group">
								<label for="txtpisos" class="control-label">Pisos</label>
								<input type="text" id="txtpisos" name="pisos" maxlength="1" class="form-control md-datasend letter-category" required disabled>
							</div>
							<div class="col-sm-3 form-group">
								<label for="txtpuerta_vent" class="control-label">Puertas y ventanas</label>
								<input type="text" id="txtpuerta_vent" name="puerta_vent" maxlength="1" class="form-control md-datasend letter-category" required disabled>
							</div>
							<div class="col-sm-2 form-group">
								<label for="txtreves" class="control-label">Reves.</label>
								<input type="text" id="txtreves" name="reves" maxlength="1" class="form-control md-datasend letter-category" required disabled>
							</div>
							<div class="col-sm-2 form-group">
								<label for="txtbanios" class="control-label">Baños</label>
								<input type="text" id="txtbanios" name="banios" maxlength="1" class="form-control md-datasend letter-category" required disabled>
							</div>
							<div class="col-sm-3 form-group">
								<label for="txtinst_electricas" class="control-label">Inst. Electricas sanitarias</label>
								<input type="text" id="txtinst_electricas" name="inst_electricas" maxlength="1" class="form-control md-datasend letter-category" required disabled>
							</div>
						</div>
					</fieldset>

					<fieldset>
						<legend>Area construida (M<sup>2</sup>)</legend>
						<div class="row">
							<div class="col-sm-3 form-group">
								<label for="txtarea_declarada" class="control-label">Declaras</label>
								<input type="text" id="txtarea_declarada" name="area_declarada" maxlength="10" class="form-control md-datasend only-number" required disabled>
							</div>
							<div class="col-sm-3 form-group">
								<label for="txtarea_verificada" class="control-label">Verificada</label>
								<input type="text" id="txtarea_verificada" name="area_verificada" maxlength="10" class="form-control md-datasend only-number" required disabled>
							</div>
							<div class="col-sm-6 form-group">
								<label for="cbouca" class="control-label">UCA</label>
								<select id="cbouca" name="uca" data-type="20" data-sw="3" class="form-control md-datasend autoload">
									<option value="" class="default" selected>Seleccione...</option>
								</select>
							</div>
						</div>
					</fieldset>
				</div>
			</div>



		</div>

	</fieldset>

	<div class="footer-body align-right">

		<button type="button" id="btn-done" disabled class="btn purple">
			<i class="fa fa-floppy-o" aria-hidden="true"></i>&nbsp;Aceptar
		</button>

		<button type="button" id="btn-close" class="btn default" >
			<i class="fa fa-times" aria-hidden="true"></i>&nbsp;Cerrar
		</button>

	</div>

</section>
