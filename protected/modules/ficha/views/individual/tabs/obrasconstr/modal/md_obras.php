<section id="md-obras" style="display:none;">

	<fieldset>
		<legend>Obras complementarias/Otras instalaciones</legend>

		<div class="row">
			<div class="col-sm-2 form-group">
				<label for="txtcod_instalacion" class="control-label">Codigo</label>
				<input type="text" id="txtcod_instalacion" class="form-control only-number letter-2">
			</div>
			<div class="col-sm-5 form-group">
				<label for="cboinstalacion" class="control-label">Descripción</label>
				<select id="cboinstalacion" name="cod_instalacion" data-type="88" data-sw="0" class="form-control autoload md-datasend">
					<option value="" class="default" selected>Seleccione...</option>
				</select>
			</div>
			<div class="col-sm-5 form-group">
				<label for="txtmatpredo" class="control-label">Mat. Predominante</label>
				<input type="text" id="txtmatpredo" name="mate_predominante" readonly tabindex="-1" class="form-control">
			</div>
		</div>

		<div class="row">

			<div class="col-sm-3 form-group">
				<label for="txtnro_documento" class="control-label">F. Construcción</label>
				<table>
					<tbody>
						<tr>
							<td><input type="text" id="txtmes" name="mes" maxlength="2" class="form-control md-datasend only-number checkdate formater" data-format="00" data-type="mes" placeholder="Mes" required></td>
							<td><input type="text" id="txtanio" name="anio" maxlength="4" class="form-control md-datasend only-number checkdate" data-type="año" placeholder="Año" required></td>
						</tr>
					</tbody>
				</table>
			</div>

			<div class="col-sm-3 form-group">
				<label for="cbomep" class="control-label">MEP</label>
				<select id="cbomep" name="mep" data-type="17" data-sw="3" class="form-control md-datasend autoload" required>
					<option value="" class="default" selected>Seleccione...</option>
				</select>
			</div>

			<div class="col-sm-3 form-group">
				<label for="cboecs" class="control-label">ECS</label>
				<select id="cboecs" name="ecs" data-type="18" data-sw="3" class="form-control md-datasend autoload" required>
					<option value="" class="default" selected>Seleccione...</option>
				</select>
			</div>

			<div class="col-sm-3 form-group">
				<label for="cboecc" class="control-label">ECC</label>
				<select id="cboecc" name="ecc" data-type="19" data-sw="3" class="form-control md-datasend autoload" required>
					<option value="" class="default" selected>Seleccione...</option>
				</select>
			</div>

		</div>

		<div class="row">

			<div class="col-sm-2 form-group">
				<label for="txtunidad" class="control-label">Unidad</label>
				<input type="text" id="txtunidad" name="unidad" class="form-control md-datasend only-number letter-2" required>
			</div>

			<div class="col-sm-6 form-group">
				<fieldset style="padding: 5px 0; margin: 0;">

					<legend><small>Dimensiones verificadas</small></legend>

					<div class="row">

						<div class="col-sm-4 form-group" style="margin-bottom: 0;">
							<label for="txtlargo" class="control-label">Largo</label>
							<input type="text" id="txtlargo" name="largo" class="form-control md-datasend only-number" required>
						</div>

						<div class="col-sm-4 form-group" style="margin-bottom: 0;">
							<label for="txtancho" class="control-label">Ancho</label>
							<input type="text" id="txtancho" name="ancho" class="form-control md-datasend only-number" required>
						</div>

						<div class="col-sm-4 form-group" style="margin-bottom: 0;">
							<label for="txtalto" class="control-label">Altura</label>
							<input type="text" id="txtalto" name="alto" class="form-control md-datasend only-number" required>
						</div>

					</div>

				</fieldset>
			</div>

			<div class="col-sm-2 form-group">
				<label for="txttotal" class="control-label">Prod. Total</label>
				<input type="text" id="txttotal" name="total" class="form-control" tabindex="-1" readonly>
			</div>

			<div class="col-sm-2 form-group">
				<label for="txtumedida" class="control-label">U. Medida</label>
				<input type="text" id="txtumedida" name="umedida" class="form-control" tabindex="-1" readonly>
			</div>

		</div>

		<div class="row">
			<div class="col-sm-6 form-group">
				<label for="cbouca" class="control-label">UCA</label>
				<select id="cbouca" name="uca" data-type="20" data-sw="3" class="form-control md-datasend autoload">
					<option value="" class="default" selected>Seleccione...</option>
				</select>
			</div>
		</div>

	</fieldset>

	<div class="footer-body align-right">

		<button type="button" id="btn-done" disabled class="btn purple">
			<i class="fa fa-floppy-o" aria-hidden="true"></i>&nbsp;Aceptar
		</button>

		<button type="button" id="btn-add" class="btn green" style="float:left" >
      <i class="fa fa-plus" aria-hidden="true"></i>&nbsp;Instalaciones
    </button>

		<button type="button" id="btn-close" class="btn default" >
			<i class="fa fa-times" aria-hidden="true"></i>&nbsp;Cerrar
		</button>

	</div>

</section>
