<?php

	$up = new stdClass;
	
	if ( ! $isOnlyView ) 
	{
		$up = UIndividual::getCookie('FIUP02');
	}
	else 
	{
		$up = $UP02;
	}

	$terreno_legal = isset($up->terreno_legal) ? $up->terreno_legal : null;
  $terreno_fisico = isset($up->terreno_fisico) ? $up->terreno_fisico : null;
  $construc_legal = isset($up->construc_legal) ? $up->construc_legal : null;
  $construc_fisico = isset($up->construc_fisico) ? $up->construc_fisico : null;

?>

<div role="tabpanel" class="tab-pane" id="tab_obrasconstr">

	<div class="container">

		<div class="row">

			<div class="col-xs-12">

			<fieldset>

				<legend>Construcciones</legend>

				<div class="col-xs-12">
					<div class="table-responsive">
						<table id="td-construcciones" class="table table-hover table-custom"></table>
					</div>

				</div>

				<div class="col-xs-12" style="text-align:right;">
					<button type="button" id="add-construccion" class="btn grey-salsa">
						<i class="fa fa-plus" aria-hidden="true"></i>&nbsp;
						Agregar
					</button>
				</div>

				<div class="col-sm-3">
					<div class="form-group">
						<label for="" class="control-label"><small>% B.C Terreno - Legal</small></label>
						<input type="text" id="txterreno_legal" name="terreno_legal" value="<?= $terreno_legal; ?>" class="form-control datasend only-number">
					</div>
				</div>
				<div class="col-sm-3">
					<div class="form-group">
						<label for="" class="control-label"><small>% B.C Terreno - Físico</small></label>
						<input type="text" id="txtterreno_fisico" name="terreno_fisico" value="<?= $terreno_fisico; ?>" class="form-control datasend only-number">
					</div>
				</div>
				<div class="col-sm-3">
					<div class="form-group">
						<label for="" class="control-label"><small>% B.C Construc - Legal</small></label>
						<input type="text" id="txtconstruc_legal" name="construc_legal" value="<?= $construc_legal; ?>" class="form-control datasend only-number">
					</div>
				</div>
				<div class="col-sm-3">
					<div class="form-group">
						<label for="" class="control-label"><small>% B.C Construc - Físico</small></label>
						<input type="text" id="construc_fisico" name="construc_fisico" value="<?= $construc_fisico; ?>" class="form-control datasend only-number">
					</div>
				</div>


			</fieldset>

			</div>

		</div>

		<div class="row">

			<div class="col-xs-12">

				<fieldset>
				<legend>Obras complementarias / Otras instalaciones</legend>

				<div class="col-xs-12">
					<div class="table-responsive">
						<table id="td-obras" class="table table-hover table-custom"></table>
					</div>
				</div>

				<div class="col-xs-12" style="text-align:right;">
					<button type="button" id="add-obras" class="btn grey-salsa">
						<i class="fa fa-plus" aria-hidden="true"></i>&nbsp;
						Agregar
					</button>
				</div>


			</fieldset>

			</div>

		</div>

	</div>

</div>

<?php $this->renderPartial('tabs/obrasconstr/modal/md_construcciones'); ?>
<?php $this->renderPartial('tabs/obrasconstr/modal/md_obras'); ?>
<?php $this->renderPartial('tabs/obrasconstr/modal/md_instalacion'); ?>
<?php $this->renderPartial('tabs/obrasconstr/modal/md_buscar_instalacion'); ?>
