<section id="md-documentos" style="display:none;">

	<fieldset>

		<legend>Documentos</legend>

		<div class="row">

			<div class="col-sm-12 col-md-5 form-group">
				<label for="cbotipo_doc" class="control-label">Tipo de documento<sup class="red">*</sup></label>
				<select id="cbotipo_doc" name="tipo_doc" class="form-control md-datasend" required disabled>
					<option value="" class="default" selected disabled>Seleccione...</option>
				</select>
			</div>

			<div class="col-sm-4 col-md-3 form-group">
				<label for="txtnro_documento" class="control-label">N° Documento</label>
				<input type="text" id="txtnro_documento" name="nro_documento" class="form-control md-datasend" disabled>
			</div>

			<div class="col-sm-4 col-md-2 form-group">
				<label for="txtfecha_inicio" class="control-label">F. Inicio</label>
				<input type="text" id="txtfecha_inicio" name="fecha_inicio" maxlength="10" class="form-control md-datasend maskdate" data-inputmask="'alias':'date'" placeholder="dd/mm/yyyy" disabled>
			</div>

			<div class="col-sm-4 col-md-2 form-group">
				<label for="txtarea_autorizada" class="control-label">Area Autorizada</label>
				<input type="text" id="txtarea_autorizada" name="area_autorizada" maxlength="10" class="form-control md-datasend only-number" disabled>
			</div>

		</div>

	</fieldset>

	<fieldset>

		<legend><small>Areas construidas por piso y/p niveles M<sup>2</sup></small></legend>

		<div class="row">
			<div class="col-sm-3 form-group">
				<label for="txtpiso1" class="control-label">1° Piso</label>
				<input type="text" id="txtpiso1" name="piso1" maxlength="10" class="form-control md-datasend only-number" disabled>
			</div>
			<div class="col-sm-3 form-group">
				<label for="txtpiso2" class="control-label">2° Piso</label>
				<input type="text" id="txtpiso2" name="piso2" maxlength="10" class="form-control md-datasend only-number" disabled>
			</div>
			<div class="col-sm-3 form-group">
				<label for="txtpiso3" class="control-label">3° Piso</label>
				<input type="text" id="txtpiso3" name="piso3" maxlength="10" class="form-control md-datasend only-number" disabled>
			</div>
			<div class="col-sm-3 form-group">
				<label for="txtpiso4" class="control-label">4° Piso</label>
				<input type="text" id="txtpiso4" name="piso4" maxlength="10" class="form-control md-datasend only-number" disabled>
			</div>
		</div>

		<div class="row">
			<div class="col-sm-3 form-group">
				<label for="txtpiso5" class="control-label">5° Piso</label>
				<input type="text" id="txtpiso5" name="piso5" maxlength="10" class="form-control md-datasend only-number" disabled>
			</div>
			<div class="col-sm-3 form-group">
				<label for="txtpiso6" class="control-label">6° Piso</label>
				<input type="text" id="txtpiso6" name="piso6" maxlength="10" class="form-control md-datasend only-number" disabled>
			</div>
			<div class="col-sm-3 form-group">
				<label for="txtpiso7" class="control-label">7° Piso</label>
				<input type="text" id="txtpiso7" name="piso7" maxlength="10" class="form-control md-datasend only-number" disabled>
			</div>
			<div class="col-sm-3 form-group">
				<label for="txtsotano" class="control-label">Sotanos</label>
				<input type="text" id="txtsotano" name="sotano" maxlength="10" class="form-control md-datasend only-number" disabled>
			</div>
			<div class="col-sm-3 form-group">
				<label for="txtmezzanine" class="control-label">Mezzanines</label>
				<input type="text" id="txtmezzanine" name="mezzanine" maxlength="10" class="form-control md-datasend only-number" disabled>
			</div>
		</div>

	</fieldset>

	<div class="footer-body align-right">

		<button type="button" id="btn-done" disabled class="btn purple">
			<i class="fa fa-floppy-o" aria-hidden="true"></i>&nbsp;Aceptar
		</button>

		<button type="button" id="btn-close" class="btn default" >
			<i class="fa fa-times" aria-hidden="true"></i>&nbsp;Cerrar
		</button>

	</div>

</section>
