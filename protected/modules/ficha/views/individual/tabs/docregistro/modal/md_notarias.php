<section id="md-notaria" style="display: none;">

  <div class="row">
    <div class="col-xs-12">

      <style type="text/css">

        table#dt-notarias tbody tr:hover {
          background-color: #f0f0f0;
        }

        table#dt-notarias td {
          padding: 0 8px !important;
          vertical-align: middle;
          font-size: 0.85em;
          user-select:none;
          -webkit-user-select:none;
          cursor: pointer;
        }

        .pagination-info { display: none !important; }
        .btn-actions {
          position: relative;
          z-index: 5; 
          margin: auto !important; 
        }

        .table-responsive {
          background-color: white !important;
          border: none !important;
        }

        #cnt-dt-notarias .search {
          display: block !important;
          margin: 0;
          width: 100%;
        }

        #cnt-dt-notarias .search input {
          border: 1px solid #ddd;
          height: 45px;
          width: 100%;
          display: block;
        }

      </style>

      <div id="cnt-dt-notarias" class="table-">
        <table id="dt-notarias" class="table table-striped table-custom">
          <thead class="bg-green"></thead>
        </table>
      </div>
    </div>
  </div>

  <div class="footer-body align-right" style="padding-top: 0;">

    <button type="button" id="btn-cancel" class="btn default" style="float: left;" >
      <i class="fa fa-times" aria-hidden="true"></i>&nbsp;Cancelar
    </button>

    <button type="submit" id="btn-new" disabled class="btn green">
      <i class="fa fa-plus" aria-hidden="true"></i>&nbsp;Nuevo
    </button>

    <button type="submit" id="btn-done" disabled class="btn purple">
      <i class="fa fa-floppy-o" aria-hidden="true"></i>&nbsp;Aceptar
    </button>

  </div>

</section>

<section id="md-mng-notarias" style="display:none;">

  <div class="row">
    <div class="col-xs-12 form-group">
      <label for="txtname" class="control-label">Nombre</label>
      <input type="text" id="txtname" name="name" maxlength="100" class="form-control datasend_notaria" required>
    </div>

    <div class="col-xs-12 form-group">
      <label for="cbodepartamento" class="control-label">Departamento</label>
      <select id="cbodepartamento" name="departamento" class="form-control" required>
        <option value="00" selected disabled>Seleccione...</option>
      </select>
    </div>

    <div class="col-xs-12 form-group">
      <label for="cboprovincia" class="control-label">Provincia</label>
      <select id="cboprovincia" name="provincia" class="form-control" required disabled>
        <option value="0000" selected disabled>Seleccione...</option>
      </select>
    </div>

    <div class="col-xs-12 form-group">
      <label for="cbodistrito" class="control-label">Distrito</label>
      <select id="cbodistrito" name="ubigeo" class="form-control datasend_notaria" required disabled>
        <option value="000000" selected disabled>Seleccione...</option>
      </select>
    </div>
  </div>

  <div class="footer-body align-right">
    
    <button type="submit" id="btn-done" disabled class="btn purple">
      <i class="fa fa-floppy-o" aria-hidden="true"></i>&nbsp;Aceptar
    </button>
    
    <button type="button" id="btn-close" class="btn default" >
      <i class="fa fa-times" aria-hidden="true"></i>&nbsp;Cerrar
    </button>
  
  </div>

</section>