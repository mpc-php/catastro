<?php
	
	$up = new stdClass;

	if ( ! $isOnlyView ) 
	{
		$up = UIndividual::getCookie('FIUP02');
	}
	else 
	{
		$up = $UP02;
	}

	$notaria 								= ((isset($up->notaria)) ? $up->notaria : NULL);
  $kardex 								= ((isset($up->kardex)) ? $up->kardex : NULL);
  $fecha_inscpublica 			= ((isset($up->fecha_inscpublica)) ? $up->fecha_inscpublica : NULL);
  $tpartida_reg 					= ((isset($up->tpartida_reg)) ? $up->tpartida_reg : NULL);
  $numeros 								= ((isset($up->numeros)) ? $up->numeros : NULL);
  $forja 									= ((isset($up->forja)) ? $up->forja : NULL);
  $asiento 								= ((isset($up->asiento)) ? $up->asiento : NULL);
  $fecha_insc_enregpredio = ((isset($up->fecha_insc_enregpredio)) ? $up->fecha_insc_enregpredio : NULL);
  $decla_fabrica 					= ((isset($up->decla_fabrica)) ? $up->decla_fabrica : NULL);
  $inscfabrica 						= ((isset($up->inscfabrica)) ? $up->inscfabrica : NULL);
  $fecha_inscfabrica 			= ((isset($up->fecha_inscfabrica)) ? $up->fecha_inscfabrica : NULL);

?>


<div role="tabpanel" class="tab-pane" id="tab_docregistro">

	<div class="container">

		<div class="row">

			<div class="col-xs-12">
				<fieldset>
					<legend>Documentos</legend>
					<style type="text/css">
						#td-documentos .ds-width {min-width: 350px !important }
						#td-documentos .config-width {min-width: 80px !important }
					</style>
					<div class="row">
						<div class="col-xs-12">
							<div class="table-responsive">
								<table id="td-documentos" class="table table-hover table-custom"></table>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12" style="text-align:right;">
							<button type="button" id="btn-add-documentos" class="btn grey-salsa btn-sm">
								<i class="fa fa-plus" aria-hidden="true"></i>&nbsp;
								Agregar
							</button>
						</div>
					</div>
				</fieldset>
			</div>

		</div>

		<fieldset>

			<legend>Registro Notarial de escritura pública</legend>

			<div class="row">
				<div class="col-sm-5">
					<div class="form-group">
							<label for="txtdisplay_notarias" class="control-label">Nombre de la notaria</label>
						<div class="input-group">
							<span class="input-group-btn">
								<button type="button" id="btn-open-notarias" class="btn btn-default">
									<span class="glyphicon glyphicon-search"></span>
								</button>
							</span>
							<select name="notaria" id="cbonotarias" data-sel="<?= $notaria; ?>" class="form-control datasend">
								<option value="" class="default" selected>Seleccione...</option>
							</select>
						</div>
					</div>
				</div>
				<div class="col-sm-4">
					<div class="form-group">
						<label for="txtkardex" class="control-label">Kardex</label>
						<input type="text" id="txtkardex" name="kardex" value="<?= $kardex; ?>" class="form-control datasend">
					</div>
				</div>
				<div class="col-sm-3">
					<div class="form-group">
						<label for="txtfecha_inscpublica" class="control-label">F. Insc. Pública.</label>
						<input type="text" id="txtfecha_inscpublica" name="fecha_inscpublica" value="<?= $fecha_inscpublica; ?>" class="form-control datasend maskdate" data-inputmask="'alias': 'date'" placeholder="dd/mm/yyyy">
					</div>
				</div>
			</div>

		</fieldset>

		<fieldset>
			<legend>Inscripción del predio catastral en el registro de predios</legend>
			<div class="row">
				<div class="col-sm-4">
					<div class="form-group">
						<label for="txttpartida_reg" class="control-label">Tipo de partida registral</label>
						<select name="tpartida_reg" id="txttpartida_reg" data-sel="<?= $tpartida_reg; ?>" class="form-control datasend autoload" data-type="22" data-sw="3">
							<option value="" class="default" selected>Seleccione una opción...</option>
						</select>
					</div>
				</div>
				<div class="col-sm-2">
					<div class="form-group">
						<label for="txtnumeros" class="control-label">Numeros</label>
						<input type="text" id="txtnumeros" name="numeros" value="<?= $numeros; ?>" class="form-control datasend">
					</div>
				</div>
				<div class="col-sm-2">
					<div class="form-group">
						<label for="txtforja" class="control-label">Fojas</label>
						<input type="text" id="txtforja" name="forja" value="<?= $forja; ?>" class="form-control datasend">
					</div>
				</div>
				<div class="col-sm-2">
					<div class="form-group">
						<label for="txtasiento" class="control-label">Asientos</label>
						<input type="text" id="txtasiento" name="asiento" value="<?= $asiento; ?>" class="form-control datasend">
					</div>
				</div>
				<div class="col-sm-2">
					<div class="form-group">
						<label for="txtfecha_insc_enregpredio" class="control-label">Fecha</label>
						<input type="text" id="txtfecha_insc_enregpredio" name="fecha_insc_enregpredio" value="<?= $fecha_insc_enregpredio; ?>" class="form-control datasend maskdate" data-inputmask="'alias': 'date'" placeholder="dd/mm/yyyy">
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-sm-5">
					<div class="form-group">
						<label for="txtdecla_fabrica" class="control-label">Declaratoria de fabrica</label>
						<select name="decla_fabrica" id="txtdecla_fabrica" data-sel="<?= $decla_fabrica; ?>" class="form-control datasend autoload" data-type="23" data-sw="3">
							<option value="" class="default" selected>Seleccione una opción...</option>
						</select>
					</div>
				</div>
				<div class="col-sm-4">
					<div class="form-group">
						<label for="" class="control-label">Asiento de insc. de fabrica</label>
						<input type="text" id="txtasiento_inscfabrica" name="inscfabrica" value="<?= $inscfabrica; ?>" class="form-control datasend">
					</div>
				</div>
				<div class="col-sm-3">
					<div class="form-group">
						<label for="" class="control-label">Fecha de insc.</label>
						<input type="text" id="txtfecha_inscfabrica" name="fecha_inscfabrica" value="<?= $fecha_inscfabrica; ?>" class="form-control datasend maskdate" data-inputmask="'alias': 'date'" placeholder="dd/mm/yyyy">
					</div>
				</div>
			</div>
		</fieldset>

	</div>

	<?php $this->renderPartial('tabs/docregistro/modal/md_notarias'); ?>

	<?php $this->renderPartial('tabs/docregistro/modal/md_documentos'); ?>

</div>
