<section id="md-buscar-titular" style="display: none;">
  
  <div class="row">
    <div class="col-md-3">
      <div class="form-group">
        <label for="cbobuscarpor" class="control-label">Buscar por:</label>
        <select id="cbobuscarpor" class="form-control"></select>
      </div>
    </div>
    <div class="col-md-9">
      <div class="form-group">
        <label for="txtfiltro" class="control-label">Filtro</label>
        <div class="input-group">
          <input type="text" id="txtfiltro" class="form-control" />
          <span class="input-group-btn">
            <button type="button" id="btn-buscar" class="btn btn-default">
              <i class="fa fa-search" aria-hidden="true"></i>
            </button>
          </span>
        </div>
      </div>
    </div>
  </div>

  <div class="row">
    <div class="col-xs-12">

      <style type="text/css">

        table#dt-buscar-titular tbody tr:hover {
          background-color: #f0f0f0;
        }

        table#dt-buscar-titular td {
          padding: 8px !important;
          vertical-align: middle;
          font-size: 0.85em;
          user-select:none;
          -webkit-user-select:none;
          cursor: pointer;
          white-space: -moz-pre-wrap !important;  /* Mozilla, since 1999 */
          white-space: -pre-wrap;      /* Opera 4-6 */
          white-space: -o-pre-wrap;    /* Opera 7 */
          white-space: pre-wrap;       /* css-3 */
        }

        .pagination-info { display: none !important; }
        .btn-actions {
          position: relative;
          z-index: 5; 
          margin: auto !important; 
        }

        .table-responsive {
          background-color: white !important;
          border: none !important;
        }

        #cnt-dt-buscar-titular .search {
          display: block !important;
          margin: 0;
          width: 100%;
        }

        #cnt-dt-buscar-titular .search input {
          border: 1px solid #ddd;
          height: 45px;
          width: 100%;
          display: block;
        }

      </style>

      <div id="cnt-dt-buscar-titular" class="">
        <table id="dt-buscar-titular" class="table table-striped table-custom">
        </table>
      </div>
    </div>
  </div>

  <div class="row">
    <div class="col-xs-12">
      <p style="margin-bottom: 0;">Debes efectuar un filtro para poder visualizar la busqueda</p>
    </div>
  </div>

  <div class="footer-body align-right">

    <button type="submit" id="btn-done" disabled class="btn purple">
      <i class="fa fa-floppy-o" aria-hidden="true"></i>&nbsp;Aceptar
    </button>

    <button type="submit" id="btn-new" disabled class="btn default green">
      <i class="fa fa-plus" aria-hidden="true"></i>&nbsp;Nuevo
    </button>

    <button type="button" id="btn-cancel" class="btn default" tabindex="-1" style="margin-right: 8px;" >
      <i class="fa fa-times" aria-hidden="true"></i>&nbsp;Cancelar
    </button>

  </div>

</section>