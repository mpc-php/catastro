
<section id="md-titular" style="display:none;">
<style type="text/css">

	#md-titular div[class*="col-"] {
		padding-left: 7.5px;
		padding-right: 7.5px;
	}

	#md-titular .row {
		margin-bottom: 0 !important;
	}

	#md-titular fieldset fieldset {
		margin-left: 2.5px !important;
		margin-right: 2.5px !important;
	}

</style>

	<div class="row">
		<div class="col-xs-12">
			<fieldset>
				<legend>Identificación del titular registral</legend>

				<div class="row">
					<div class="col-sm-6 form-group">
						<label for="cbotipo_titular" class="control-label">Tipo de Titular</label>
						<div class="input-group">
							<select id="cbotipo_titular" name="tipo_titular" data-sel="1" data-exchange="true" data-type="07" data-sw="0" class="form-control autoload"></select>
							<span class="input-group-btn">
								<button type="button" id="btn-buscar-titular" class="btn btn-default">
									<i class="fa fa-search" aria-hidden="true"></i>
								</button>
							</span>
						</div>
					</div>
				</div>

				<div class="row">
					<div class="col-sm-2 form-group">
						<label for="txtcod_contribuyente" class="control-label">Cod. Contribuyente</label>
						<input type="text" id="txtcod_contribuyente" name="cod_contribuyente" class="form-control only-number md-datasend" >
					</div>
					<div class="col-sm-4 form-group">
						<label for="cboforma_adquisicion" class="control-label">Forma Adquisición</label>
						<select id="cboforma_adquisicion" name="forma_adquisicion" data-type="13" data-sw="0" class="form-control md-datasend autoload">
							<option value="" selected class="default" disabled>Seleccione...</option>
						</select>
					</div>
					<div class="col-sm-3 form-group">
						<label for="txtf_adquisicion" class="control-label">F. adquisición</label>
						<input type="text" id="txtf_adquisicion" name="f_adquisicion" maxlength="10" class="form-control md-datasend masker" data-inputmask="'alias':'date'" placeholder="dd/mm/yyyy">
					</div>
					<div class="col-sm-3 form-group">
						<label for="txtpor_adquisicion" class="control-label">% Pertenencia</label>
						<input type="text" id="txtpor_adquisicion" name="por_adquisicion" maxlength="6" class="form-control only-number md-datasend">
					</div>
				</div>

				<div class="row">
					<div class="col-xs-12">
						<fieldset>
							<div class="row">
								<div class="col-sm-2 form-group">
									<label for="cboestado_civil" class="control-label">Estado Civil</label>
									<select id="cboestado_civil" name="estado_civil" data-type="08" data-sw="0" class="form-control md-datasend autoload" required>
										<option value="" selected class="default">Seleccione...</option>
									</select>
								</div>
								<div class="col-sm-3 form-group">
									<label for="cbotipo_documento" class="control-label">Tipo documento identidad</label>
									<select id="cbotipo_documento" name="tipo_documento" data-type="09" data-sw="0" class="form-control autoload" required>
										<option value="" value="" selected class="default" disabled>Seleccione...</option>
									</select>
								</div>
								<div class="col-sm-2 form-group">
									<label id="lblnro_documento" for="txtnro_documento" class="control-label">N° de Documento</label>
									<input type="text" id="txtnro_documento" name="nro_documento" class="form-control only-number">
								</div>
								<div class="col-sm-5 form-group">
									<label id="lblnombre" for="txtnombre" class="control-label">Nombres</label>
									<input type="text" id="txtnombre" name="nombre" class="form-control only-number md-datasend">
								</div>
							</div>
							<div class="row">
								<div class="col-sm-6 form-group">
									<label for="cbopersona_juridica" class="control-label">Persona juridica</label>
									<select id="cbopersona_juridica" name="persona_juridica" data-type="10" data-sw="0" class="form-control autoload" disabled>
										<option value="" selected class="default">Seleccione...</option>
									</select>
								</div>
								<div class="col-sm-6 form-group">
									<br>
									<div class="checkbox" style="margin-bottom: 0;">
										<label style="padding-left: 20px;">
											<input type="checkbox" id="chkcontriestitular" name="contriestitular"> Contribuyente titular catastral
										</label>
									</div>
								</div>
							</div>
						</fieldset>
					</div>
				</div>
				<div class="row">
					<div class="col-xs-12">
						<fieldset>
							<legend>Exoneración</legend>
							<div class="row">
								<div class="col-sm-3 form-group">
									<label for="cbocondesp_titular" class="control-label">Cond. Esp. del titular</label>
									<select id="cbocondesp_titular" name="condesp_titular" data-type="11" data-sw="0" class="form-control md-datasend autoload" required>
										<option value="" selected class="default" disabled>Seleccione...</option>
									</select>
								</div>
								<div class="col-sm-2 form-group">
									<label for="txtnro_resolucion" class="control-label">N° resolución</label>
									<input type="text" id="txtnro_resolucion" name="nro_resolucion" class="form-control md-datasend">
								</div>
								<div class="col-sm-3 form-group">
									<label for="txtnro_boleta_pension" class="control-label">N° boleta pensionista</label>
									<input type="text" id="txtnro_boleta_pension" name="nro_boleta_pension" class="form-control only-number md-datasend">
								</div>
								<div class="col-sm-2 form-group">
									<label for="txtf_inicio" class="control-label">F. Inicio</label>
									<input type="text" id="txtf_inicio" name="f_inicio" class="form-control md-datasend masker" data-inputmask="'alias':'date'" placeholder="dd/mm/yyyy">
								</div>
								<div class="col-sm-2 form-group">
									<label for="txtf_vencimiento" class="control-label">F. Vencimiento</label>
									<input type="text" id="txtf_vencimiento" name="f_vencimiento" class="form-control md-datasend masker" data-inputmask="'alias':'date'" placeholder="dd/mm/yyyy">
								</div>
							</div>

							<div class="row">
								<div class="col-xs-12">
									<small>
										<span style="color: red;">(*)</span>&nbsp;
										 Recuerde escoger una condición especial para que los demas datos,
										 de este grupo, puedan ser guardados.
									</small>
								</div>
							</div>
						</fieldset>
					</div>
				</div>
			</fieldset>
		</div>
	</div>

	<div class="row">
		<div class="col-xs-12">
			<fieldset style="margin-bottom: 0;">
				<legend>Domicilio fiscal del titular catastral</legend>

				<div class="row">
					<div class="col-sm-12 form-group">
						<div class="checkbox" style="margin-bottom: 0;">
							<label style="padding-left: 20px;">
								<input type="checkbox" id="chklocal" name="islocal" class="md-datasend">Datos de ubicación de predio
							</label>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-4 form-group">
						<label for="cbodepartamento" class="control-label">Departamento</label>
						<select id="cbodepartamento" class="form-control ubigeo" required>
							<option value="" selected class="default" disabled>Seleccione...</option>
						</select>
					</div>
					<div class="col-sm-4 form-group">
						<label for="cboprovincia" class="control-label">Provincia</label>
						<select id="cboprovincia" class="form-control ubigeo" required disabled>
							<option value="" selected class="default" disabled>Seleccione...</option>
						</select>
					</div>
					<div class="col-sm-4 form-group">
						<label for="cbodistrito" class="control-label">Distrito</label>
						<select id="cbodistrito" name="ubigeo" class="form-control ubigeo md-datasend" required disabled>
							<option value="" selected class="default" disabled>Seleccione...</option>
						</select>
					</div>
				</div>

				<div class="row">
					<div class="col-sm-3 form-group">
						<label for="txttelefono" class="control-label">Telefono</label>
						<input type="text" id="txttelefono" name="telefono" class="form-control md-datasend">
					</div>
					<div class="col-sm-3 form-group">
						<label for="txtanexo" class="control-label">Anexo</label>
						<input type="text" id="txtanexo" name="anexo" class="form-control md-datasend">
					</div>
					<div class="col-sm-3 form-group">
						<label for="txtfax" class="control-label">Fax</label>
						<input type="text" id="txtfax" name="fax" class="form-control md-datasend">
					</div>
					<div class="col-sm-3 form-group">
						<label for="txtemail" class="control-label">Correo Electronico</label>
						<input type="text" id="txtemail" name="email" class="form-control md-datasend">
					</div>
				</div>

				<div class="row">
					<div class="col-xs-12">
						<fieldset>
							<div class="row">
								<div class="col-sm-3 form-group">
									<label for="txtcod_hurbana" class="control-label">Cod. HU</label>
									<input type="text" id="txtcod_hurbana" name="hurbana" maxlength="5" class="form-control only-number md-datasend" disabled >
								</div>
								<div class="col-sm-9 form-group">
									<label for="txtnombre_hurbana" class="control-label">Nombre Hab. urbana</label>
									<input type="text" id="txtnombre_hurbana" name="hurbana" class="form-control md-datasend" >
								</div>
							</div>
							<div class="row">
								<div class="col-sm-6 form-group">
									<label for="txtzon_sec_etapa" class="control-label">Zona/Sector/Etapa</label>
									<input type="text" id="txtzon_sec_etapa" class="form-control" disabled >
								</div>
								<div class="col-sm-2 form-group">
									<label for="txtmanzana" class="control-label">Manzana</label>
									<input type="text" id="txtmanzana" name="manzana" class="form-control md-datasend" >
								</div>
								<div class="col-sm-2 form-group">
									<label for="txtlote" class="control-label">Lote</label>
									<input type="text" id="txtlote" name="lote" class="form-control md-datasend" >
								</div>
								<div class="col-sm-2 form-group">
									<label for="txtsublote" class="control-label">Sub-lote</label>
									<input type="text" id="txtsublote" name="sublote" class="form-control md-datasend" >
								</div>
							</div>
						</fieldset>
					</div>
				</div>

				<div class="row">
					<div class="col-xs-12">
						<fieldset style="margin-bottom: 0;">
							<div class="row">
								<div class="col-sm-3 form-group">
									<label for="txtcod_via" class="control-label">Cod. Vía</label>
									<input type="text" id="txtcod_via" name="via" class="form-control only-number md-datasend" disabled >
								</div>
								<div class="col-sm-9 form-group">
									<label for="txtnombre_via" class="control-label">Nombre de Vía</label>
									<input type="text" id="txtnombre_via" name="via" class="form-control md-datasend" >
								</div>
							</div>
							<div class="row">
								<div class="col-sm-3 form-group">
									<label for="txtnro_municipalidad" class="control-label">N° Municipalidad</label>
									<input type="text" id="txtnro_municipalidad" name="nro_municipalidad" class="form-control only-number md-datasend" >
								</div>
								<div class="col-sm-6 form-group">
									<label for="txtnombre_edificio" class="control-label">Nombre de la edificación</label>
									<input type="text" id="txtnombre_edificio" name="nombre_edificio" class="form-control md-datasend" >
								</div>
								<div class="col-sm-3 form-group">
									<label for="txtnro_interior" class="control-label">N° de interior</label>
									<input type="text" id="txtnro_interior" name="nro_interior" class="form-control md-datasend" >
								</div>
							</div>
						</fieldset>
					</div>
				</div>

			</fieldset>
		</div>
	</div>

	<div class="footer-body align-right">
    
    <button type="button" id="btn-done" disabled class="btn purple">
      <i class="fa fa-floppy-o" aria-hidden="true"></i>&nbsp;Aceptar
    </button>

    <button type="button" id="btn-close" class="btn default" >
      <i class="fa fa-times" aria-hidden="true"></i>&nbsp;Cerrar
    </button>
    
  </div>

</section>
