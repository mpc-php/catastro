<?php

	$up = new stdClass;

	if ( ! $isOnlyView ) 
	{
		$up = UIndividual::getCookie('FIUP01');
	}
	else 
	{
		$up = $UP01;
	}

	$cond_titular = isset($up->cond_titular) ? $up->cond_titular : null;
	$fuente_info = isset($up->fuente_info) ? $up->fuente_info : null;
  $cond_especialpredio = isset($up->cond_especialpredio) ? $up->cond_especialpredio : null;
  $nro_resolucion = isset($up->nro_resolucion) ? $up->nro_resolucion : null;
  $porcent = isset($up->porcent) ? $up->porcent : null;
  $finicio = isset($up->finicio) ? $up->finicio : null;
  $fvencimiento = isset($up->fvencimiento) ? $up->fvencimiento : null;

?>

<div role="tabpanel" class="tab-pane" id="tab_titularidad">

	<div class="container">

		<div class="row">

			<div class="col-xs-12">

				<fieldset>

					<legend>Caracteristicas de la Titularidad</legend>

					<div class="col-sm-6">
						<div class="form-group">
							<label for="cbocond_titular" class="control-label">Condición del Titular</label>
							<select name="cond_titular" id="cbocond_titular" data-sel="<?= $cond_titular; ?>" class="form-control datasend autoload" data-type="12" data-sw="3">
								<option value="" class="default" selected>Escoja una opción...</option>
							</select>
						</div>
					</div>
					<div class="col-sm-6">
						<div class="form-group">
							<label for="cbofuente_info" class="control-label">Fuente de Información</label>
							<select name="fuente_info" id="cbofuente_info" data-sel="<?= $fuente_info; ?>" class="form-control datasend autoload" data-type="42" data-sw="3">
								<option value="" class="default" selected>Escoja una opción...</option>
							</select>
						</div>
					</div>

					<div class="col-xs-12">
						<style type="text/css">
							.fieldset-1 > div[class*="col-"] {
								padding-left: 7.5px;
								padding-right: 7.5px;
							}
						</style>
						<fieldset class="fieldset-1">
							<legend>Exoneración del Predio</legend>

							<div class="col-sm-6 col-md-4">
								<div class="form-group">
									<label for="" class="control-label">Cond. Especial del predio</label>
									<table style="width:100%">
										<tbody>
											<tr>
												<td width="45"><input type="text" id="txtsearch_cbocond_especialpredio" value="<?= $cond_especialpredio; ?>" data-formater="00" class="form-control" maxlength="2"></td>
												<td>
													<select name="cond_especialpredio" id="cbocond_especialpredio" data-sel="<?= $cond_especialpredio; ?>" class="form-control datasend autoload" data-type="14" data-sw="3">
														<option value="" class="default" selected>Escoja una opción...</option>
													</select>
												</td>
											</tr>
										</tbody>
									</table>
								</div>
							</div>
							<div class="col-sm-6 col-md-3">
								<div class="form-group">
									<label for="txtnro_resolucion" class="control-label">N° de Resolución</label>
									<input type="text" name="nro_resolucion" id="txtnro_resolucion" value="<?= $nro_resolucion; ?>" class="form-control datasend">
								</div>
							</div>
							<div class="col-sm-4 col-md-1">
								<div class="form-group">
									<label for="txtporcent" class="control-label">Porcentaje</label>
									<input type="text" name="porcent" id="txtporcent" value="<?= $porcent; ?>" maxlength="6" class="form-control datasend only-number">
								</div>
							</div>
							<div class="col-sm-4 col-md-2">
								<div class="form-group">
									<label for="txtfinicio" class="control-label">F. Inicio</label>
									<input type="text" name="finicio" id="txtfinicio" value="<?= $finicio; ?>" class="form-control datasend maskdate" data-inputmask="'alias': 'date'" placeholder="dd/mm/yyyy">
								</div>
							</div>
							<div class="col-sm-4 col-md-2">
								<div class="form-group">
									<label for="txtfvencimiento" class="control-label">F. Vencimiento</label>
									<input type="text" name="fvencimiento" id="txtfvencimiento" value="<?= $fvencimiento; ?>" class="form-control datasend maskdate" data-inputmask="'alias': 'date'" placeholder="dd/mm/yyyy">
								</div>
							</div>
						</fieldset>
					</div>

				</fieldset>

			</div>

		</div>

		<div class="row">
			<div class="col-xs-12">

				<div class="table-responsive">
					<table id="td-titular" class="table table-custom"></table>
				</div>

			</div>

			<div class="col-xs-12" style="text-align:right;">
				<button type="button" id="btn-add-titular" class="btn grey-salsa btn-sm">
					<i class="fa fa-plus" aria-hidden="true"></i>&nbsp;
					Agregar
				</button>
			</div>
		</div>

	</div>

</div>

<?php $this->renderPartial('tabs/titularidad/modal/md_titular'); ?>
<?php $this->renderPartial('tabs/titularidad/modal/md_buscar_titular'); ?>
<?php $this->renderPartial('tabs/titularidad/modal/md_agrega_titular'); ?>
