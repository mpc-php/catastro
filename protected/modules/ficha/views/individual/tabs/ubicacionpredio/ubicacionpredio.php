<?php

	$up = new stdClass;

	if ( ! $isOnlyView ) 
	{
		$up = UIndividual::getCookie('FIUP01');
	}
	else 
	{
		$up = $UP01;
	}

	$code_haburba = isset($up->code_haburba) ? $up->code_haburba : null;
  $manzana = isset($up->manzana) ? $up->manzana : null;
  $lote = isset($up->lote) ? $up->lote : null;
  $sublote = isset($up->sublote) ? $up->sublote : null;
  $name_edificio = isset($up->name_edificio) ? $up->name_edificio : null;
  $tipo_edificio = isset($up->tipo_edificio) ? $up->tipo_edificio : null;
  $tipo_interior = isset($up->tipo_interior) ? $up->tipo_interior : null;
  $nro_interior = isset($up->nro_interior) ? $up->nro_interior : null;

?>

<div role="tabpanel" class="tab-pane active" id="tab_ubicpredio">

	<div class="container">

		<div class="row">
			<div class="col-xs-12">
				<fieldset>
					<legend>Ubicaciones</legend>
					<div class="row">
						<div class="col-xs-12">
							<div class="table-responsive">
								<table id="td-UP" class="table table-custom"></table>
							</div>
						</div>
						<div class="col-xs-12" style="text-align:right;">
							<button id="btn-md-ubicacion" type="button" class="btn grey-salsa">
								<i class="fa fa-plus" aria-hidden="true"></i>&nbsp;
								Agregar
							</button>
						</div>
					</div>
				</fieldset>
			</div>
		</div>

		<div class="row">

			<div class="col-sm-4">
				<div class="form-group">
					<label for="txtname_edificio" class="control-label">Nombre de edificación</label>
					<input type="text" id="txtname_edificio" name="name_edificio" value="<?= $name_edificio ?>" class="form-control datasend" maxlength="50">
				</div>
			</div>

			<div class="col-sm-3">
				<div class="form-group">
					<label for="cbotipo_edificio" class="control-label">Tipo edificación</label>
					<select id="cbotipo_edificio" name="tipo_edificio" data-sel="<?= $tipo_edificio; ?>" data-type="05" data-sw="3" class="autoload form-control datasend">
						<option value="" class="default" selected>Escoja una opción...</option>
					</select>
				</div>
			</div>

			<div class="col-xs-8 col-sm-3">
				<div class="form-group">
					<label for="cbotipo_interior" class="control-label">Tipo interior</label>
					<select name="tipo_interior" id="cbotipo_interior" data-sel="<?= $tipo_interior ?>" data-type="06" data-sw="3" class="autoload form-control datasend">
						<option value="" class="default" selected>Escoja una opción...</option>
					</select>
				</div>
			</div>

			<div class="col-xs-4 col-sm-2">
				<div class="form-group">
					<label for="txtnumber_interior" class="control-label">N° Interior</label>
					<input type="text" id="txtnumber_interior" name="nro_interior" value="<?= $nro_interior ?>" class="form-control datasend">
				</div>
			</div>

		</div>

		<div class="row">

			<div class="col-sm-2">
				<div class="form-group">
					<label for="" class="control-label">Cod HU</label>
					<input type="text" id="txtcode_haburba" name="code_haburba" value="<?= $code_haburba ?>" disabled class="form-control datasend only-number datasend" maxlength="5">
				</div>
			</div>

			<div class="col-sm-4">
				<div class="form-group">
					<label for="" class="control-label">Habilitación urbana</label>
					<input type="text" id="txtdesc_haburba" class="form-control" disabled>
				</div>
			</div>

			<div class="col-xs-2">
				<div class="form-group">
					<label for="" class="control-label">Mz.</label>
					<input type="text" id="txtmanzana" name="manzana" value="<?= $manzana ?>" class="form-control datasend">
				</div>
			</div>

			<div class="col-xs-2">
				<div class="form-group">
					<label for="" class="control-label">Lote</label>
					<input type="text" name="lote" id="txtlote" value="<?= $lote ?>" class="form-control datasend">
				</div>
			</div>

			<div class="col-xs-2">
				<div class="form-group">
					<label for="" class="control-label">Sub-lote</label>
					<input type="text" name="sublote" id="txtsublote" value="<?= $sublote ?>" class="form-control datasend">
				</div>
			</div>

		</div>

	</div>

</div>

<?php $this->renderPartial('tabs/ubicacionpredio/modal/md_ubicaciones') ?>
