<section id="md-ubicaciones" style="display:none;">
	<form id="frm-ubicaciones" action="" method="post" autocomplete="off" spellcheck="off">
		<div class="row">
			<div class="col-xs-4 col-sm-4 form-group">
				<label for="txtcodvia" class="control-label">Cod. Vía</label>
				<input type="text" id="txtcodvia" name="via" maxlength="6" data-format="000000" class="form-control md-datasend" disabled>
			</div>
			<div class="col-xs-8 col-sm-8 form-group">
				<label for="txtdescvias" class="control-label">Nombre de vía <sup class="red">*</sup></label>
				<input type="text" id="txtdescvias" class="form-control" disabled readonly>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-4 form-group">
				<label for="cbotipo_puerta" class="control-label">Tipo puerta  <sup class="red">*</sup></label>
				 <select id="cbotipo_puerta" name="tipo_puerta" data-type="03" data-sw="0" class="form-control md-datasend autoload" required disabled>
					<option selected value="0" class="default">Seleccione...</option>
				</select>
			</div>
			<div class="col-sm-8 form-group">
				<label for="txtnro_municip" class="control-label">N° Municip.</label>
				<input type="text" id="txtnro_municip" name="nro_municipal" class="form-control md-datasend" disabled>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-6 form-group">
				<label for="cbocondnum" class="control-label">Cond. Num.</label>
				<select id="cbocondnum" name="cond_num" data-type="04" data-sw="0" class="form-control md-datasend autoload" disabled>
					<option value="" class="default" selected>&nbsp;</option>
				</select>
			</div>
			<div class="col-sm-6 form-group">
				<label for="txtnro_certnumer" class="control-label">N° Cert. Numeración</label>
				<input type="text" id="txtnro_certnumer" name="nro_certificado" class="form-control only-number md-datasend" disabled>
			</div>
		</div>
		<div class="footer-body align-right">
			
			<button type="button" id="btn-done" disabled class="btn purple">
				<i class="fa fa-floppy-o" aria-hidden="true"></i>&nbsp;Aceptar
			</button>
			<button type="button" id="btn-close" class="btn default" >
				<i class="fa fa-times" aria-hidden="true"></i>&nbsp;Cerrar
			</button>
			
		</div>
	</form>
</section>
