<!--
	El ID es importante para que los modulos de javascript no se activen
	en una pagina que no le corresponde
-->

<div id="page" data-module="<?= $action; ?>" class="page-head indicrear">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 header-title" style="text-align: center;">
				<h3 style="text-transform: uppercase;">Nueva ficha individual</h3>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12">
				<fieldset class="groupbox">
					<legend>Datos de la ficha</legend>
					<?php 

						$data['action'] = $action;
						$data['isOnlyView'] = $isOnlyView;

						if ( $isOnlyView ) {
							$data['HEADER'] = $HEAD;
							$data['UP01'] = $UP01;
							$data['UP02'] = $UP02;
						}

						$this->renderPartial('partial/header', $data); 

					?>
				</fieldset>
			</div>
		</div>
	</div>
</div>

<div id="cnt-data" class="tabbable-line indicrear disabled">

	<!--
	|   pESTAÑAS
	-->
	<ul id="tabs-control" class="nav nav-tabs nav-justified tab-custom" role="tablist">
		<li role="presentation" class="active">
			<a href="#tab_ubicpredio" data-next="#tab_titularidad" data-preview="#tab_otros" data-firstelement="btn-md-ubicacion" aria-controls="ubicacion" role="tab" data-toggle="tab" tabindex="-1" title="Ubicación del predio">
				<span class="hidden-xs hidden-sm hidden-md"><b><u>U</u></b>bic. del predio</span>
				<span class="glyphicon glyphicon-map-marker hidden-lg"></span>
			</a>
		</li>
		<li role="presentation">
			<a href="#tab_titularidad" data-next="#tab_descpredio" data-preview="#tab_ubicpredio" data-firstelement="cbocond_titular" aria-controls="titularidad" role="tab" data-toggle="tab" tabindex="-1" title="Titularidad">
				<span class="hidden-xs hidden-sm hidden-md">Titularidad</span>
				<span class="glyphicon glyphicon-user hidden-lg"></span>
			</a>
		</li>
		<li role="presentation">
			<a href="#tab_descpredio" data-next="#tab_obrasconstr" data-preview="#tab_titularidad" data-firstelement="cboclasif_predio" aria-controls="predio" role="tab" data-toggle="tab" tabindex="-1" title="Descripción del predio">
				<span class="hidden-xs hidden-sm hidden-md">Desc. del predio</span>
				<span class="glyphicon glyphicon-list-alt hidden-lg"></span>
			</a>
		</li>
		<li role="presentation">
			<a href="#tab_obrasconstr" data-next="#tab_docregistro" data-preview="#tab_descpredio" data-firstelement="add-construccion" aria-controls="obras" role="tab" data-toggle="tab" tabindex="-1" title="Obras/construcción">
				<span class="hidden-xs hidden-sm hidden-md">Obras/Construcciones</span>
				<span class="glyphicon glyphicon-home hidden-lg"></span>
			</a>
		</li>
		<li role="presentation">
			<a href="#tab_docregistro" data-next="#tab_infcomplementaria" data-preview="#tab_obrasconstr" data-firstelement="btn-add-documentos" aria-controls="documentos" role="tab" tabindex="-1" data-toggle="tab" title="Documentos y registros">
				<span class="hidden-xs hidden-sm hidden-md">Documentos y registros</span>
				<span class="glyphicon glyphicon-duplicate hidden-lg"></span>
			</a>
		</li>
		<li role="presentation">
			<a href="#tab_infcomplementaria" data-next="#tab_otros" data-preview="#tab_docregistro" data-firstelement="rbopomiso" aria-controls="informacion" role="tab" data-toggle="tab" tabindex="-1" title="Información complementaria">
				<span class="hidden-xs hidden-sm hidden-md">Inf. complementaria</span>
				<span class="glyphicon glyphicon-info-sign hidden-lg"></span>
			</a>
		</li>
		<li role="presentation">
			<a href="#tab_otros" data-next="#tab_ubicpredio" data-preview="#tab_infcomplementaria" data-firstelement="chkdeclarante" aria-controls="otros" role="tab" data-toggle="tab" tabindex="-1" title="Otros">
				<span class="hidden-xs hidden-sm hidden-md">Otros</span>
				<span class="glyphicon glyphicon-link hidden-lg"></span>
			</a>
		</li>
	</ul>
	
	<!--
	|   cONTENEDOR DE LOS PANELES DE LAS PESTAÑAS
	-->
	<div class="tab-content">
	<?php 
		/**
		 * uBICACIÓN DEL PREDIO
		 */
		$this->renderPartial('tabs/ubicacionpredio/ubicacionpredio', $data);

		/**
		 * tITULARIDAD
		 */
		$this->renderPartial('tabs/titularidad/titularidad', $data);

		/**
		 * dESCRIPCIÓN DEL PREDIO
		 */
		$this->renderPartial('tabs/descpredio/descpredio', $data);

		/**
		 * oBRAS/CONSTRUCCIÓN
		 */
		$this->renderPartial('tabs/obrasconstr/obrasconstr', $data);

		/**
		 * dOCUMENTOS Y REGISTROS
		 */
		$this->renderPartial('tabs/docregistro/docregistro', $data);

		/**
		 * iNFORMACIÓN COMPLEMENTARIA
		 */
		$this->renderPartial('tabs/infcomplementaria/infcomplementaria', $data);

		/**
		 * oTROS
		 */
		$this->renderPartial('tabs/otros/otros', $data);
	?>
	</div>

<?php if ( ! $isOnlyView ): ?>
	
	<div class="container">
		<div class="row">
			<div class="col-xs-12" style="text-align:right; padding: 20px; background-color: white; margin-top: -20px; box-shadow: 0 1px 4px 0 rgba(0, 0, 0, 0.14);">
				
				<?php 
					if ( $action == 'create' ) 
					{
						$text_btn = "Limpiar ficha";
					} 
					else 
					{
						$text_btn = "Salir de ficha";
					}
				?>

				<button type="button" id="btn-reset-ficha" tabindex="-1" style="float:left" class="btn yellow btn-outline btn-first-disabled" disabled>
					<?= $text_btn; ?>
				</button>
				
				<?php if ($action == 'create'): ?>
					<button type="button" id="btn-observation" style="float: left; margin-left: 10px;" class="btn blue-hoki btn-outline sbold uppercase btn-first-disabled" disabled>
						Generar observación
					</button>
				<?php endif; ?>

				<button type="button" id="btn-general" tabindex="-1" class="btn green btn-first-disabled" disabled>Guardar datos de ficha</button>
			</div>
		</div>
	</div>

<?php endif; ?>

</div>

<?php if ($action == 'create'): ?>

	<?php $this->renderPartial('../_modules/md_observation'); ?>

<?php endif; ?>