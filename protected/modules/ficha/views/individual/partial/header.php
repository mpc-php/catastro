<?php 

	$head = new stdClass;

	if ( ! $isOnlyView )
	{
		$head = UIndividual::getCookie(CIndividual::HEAD_NAME_CK);
	}
	else
	{
		$head = $HEADER;
	}
		

	$nro_ficha 							= ((isset($head->nro_ficha)) ? $head->nro_ficha : '');
	$nro_ficha_lote_first 	= ((isset($head->nro_ficha_lote_first)) ? $head->nro_ficha_lote_first : '');
	$nro_ficha_lote_second 	= ((isset($head->nro_ficha_lote_second)) ? $head->nro_ficha_lote_second : '');
	$cod_hoja_catastral 		= ((isset($head->cod_hoja_catastral)) ? $head->cod_hoja_catastral : '');
	$cod_predial_rentas 		= ((isset($head->cod_predial_rentas)) ? $head->cod_predial_rentas : '');
	$ua_predial_rentas 			= ((isset($head->ua_predial_rentas)) ? $head->ua_predial_rentas : '');
	$ubigeo 								= ((isset($head->cod_ref_catastral->ubigeo)) ? $head->cod_ref_catastral->ubigeo : '070101');
	$sector 								= ((isset($head->cod_ref_catastral->sector)) ? $head->cod_ref_catastral->sector : '');
	$manzana 								= ((isset($head->cod_ref_catastral->manzana)) ? $head->cod_ref_catastral->manzana : '');
	$lote 									= ((isset($head->cod_ref_catastral->lote)) ? $head->cod_ref_catastral->lote : '');
	$edificio 							= ((isset($head->cod_ref_catastral->edificio)) ? $head->cod_ref_catastral->edificio : '');
	$entrada 								= ((isset($head->cod_ref_catastral->entrada)) ? $head->cod_ref_catastral->entrada : '');
	$piso 									= ((isset($head->cod_ref_catastral->piso)) ? $head->cod_ref_catastral->piso : '');
	$unidad 								= ((isset($head->cod_ref_catastral->unidad)) ? $head->cod_ref_catastral->unidad : '');
	$dc 										= ((isset($head->cod_ref_catastral->dc)) ? $head->cod_ref_catastral->dc : '0');
	$id_ficha 							= ((isset($head->id_ficha)) ? $head->id_ficha : '');
	$cod_catastro 					= ((isset($head->cod_catastro)) ? $head->cod_catastro : '');

?>

<div id="cnt-ficha-header" class="container-fluid">

	<?php if ( ! empty($nro_ficha) ): ?>
		<input type="hidden" id="ficha_nro" class="exists_ficha" value="<?= $nro_ficha; ?>">
		<input type="hidden" id="ficha_id" class="exists_ficha" value="<?= $id_ficha; ?>">
		<input type="hidden" id="catastro_cod" class="exists_ficha" value="<?= $cod_catastro; ?>">
		<input type="hidden" id="sector_cod" class="exists_ficha" value="<?= $sector; ?>">
		<input type="hidden" id="ubigeo_cod" class="exists_ficha" value="<?= $ubigeo; ?>">
		<input type="hidden" id="action" class="exists_ficha" value="<?= $action; ?>">
	<?php endif; ?>

	<div class="row">

		<div class="col-sm-12 col-md-8">
			<div class="form-group">
				<label for="" class="control-label">Codigo de referencia catastral</label>
				<div class="table-responsive not-boottable">
					<table id="td_codrefcatastro" class="table-center" style="text-align:center;">
						<tbody>
							<tr>
								<td>
									<input type="text" id="txtubigeo" name="ubigeo" value="<?= $ubigeo ?>" <?= (!empty($nro_ficha) ? 'disabled': '') ?> required data-parent="cod_ref_catastral" class="datasend ficha-header form-control letter-6 only-number formater cdc" data-formater="" readonly>
									<label for="txtubigeo">Ubigeo</label>
								</td>
								<td>
									<input type="text" id="txtsector" name="sector" value="<?= $sector ?>" <?= (!empty($nro_ficha) ? 'disabled': '') ?> required data-df="1" data-parent="cod_ref_catastral" class="datasend ficha-header form-control letter-2 only-number formater cdc" data-formater="00" maxlength="2" placeholder="00" autofocus>
									<label for="txtsector">Sector</label>
								</td>
								<td>
									<input type="text" id="txtmanzana" name="manzana" value="<?= $manzana ?>" <?= (!empty($nro_ficha) ? 'disabled': '') ?> required data-df="1" data-parent="cod_ref_catastral" class="datasend ficha-header form-control letter-3 only-number formater cdc" data-formater="000" maxlength="3" placeholder="000">
									<label for="txtmanzana">Manzana</label>
								</td>
								<td>
									<input type="text" id="txtlote" name="lote" value="<?= $lote ?>" <?= (!empty($nro_ficha) ? 'disabled': '') ?> required data-df="1" data-parent="cod_ref_catastral" class="datasend ficha-header form-control letter-3 only-number formater cdc" data-formater="000" maxlength="3" placeholder="000">
									<label for="txtlote">Lote</label>
								</td>
								<td>
									<input type="text" id="txtedificio" name="edificio" value="<?= $edificio ?>" <?= (!empty($nro_ficha) ? 'disabled': '') ?> required data-df="1" data-parent="cod_ref_catastral" class="datasend ficha-header form-control letter-2 only-number formater cdc" data-formater="00" maxlength="2" placeholder="00">
									<label for="txtedificio">Edificio</label>
								</td>
								<td>
									<input type="text" id="txtentrada" name="entrada" value="<?= $entrada ?>" <?= (!empty($nro_ficha) ? 'disabled': '') ?> required data-df="1" data-parent="cod_ref_catastral" class="datasend ficha-header form-control letter-2 only-number formater cdc" data-formater="00" maxlength="2" placeholder="00">
									<label for="txtentrada">Entrada</label>
								</td>
								<td>
									<input type="text" id="txtpiso" name="piso" value="<?= $piso ?>" <?= (!empty($nro_ficha) ? 'disabled': '') ?> required data-df="1" data-parent="cod_ref_catastral" class="datasend ficha-header form-control letter-2 only-number formater cdc" data-formater="00" maxlength="2" placeholder="00">
									<label for="piso">Piso</label>
								</td>
								<td>
									<input type="text" id="txtunidad" name="unidad" value="<?= $unidad ?>" <?= (!empty($nro_ficha) ? 'disabled': '') ?> required data-df="1" data-parent="cod_ref_catastral" class="datasend ficha-header form-control letter-3 only-number formater cdc" data-formater="000" maxlength="3" placeholder="000">
									<label for="txtunidad">Unidad</label>
								</td>
								<td>
									<input type="text" id="txtdc" name="dc" value="<?= $dc ?>" data-parent="cod_ref_catastral" class="datasend ficha-header form-control letter-1 only-number" disabled maxlength="1">
									<label for="txtdc">DC</label>
								</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>
		<div class="col-sm-6 col-md-2">
			<div class="form-group">
				<label for="" class="control-label">N° de Ficha</label>
				<input type="text" id="txtnro_ficha" name="nro_ficha" maxlength="10" value="<?= $nro_ficha ?>" <?= (!empty($nro_ficha) ? 'disabled': '') ?> required class="datasend ficha-header form-control only-number formater" data-formater="0000000" >
			</div>
		</div>
		<div class="col-sm-6 col-md-2">
			<div class="form-group">
				<label for="" class="control-label">N° de Ficha por lote</label>
				<table>
					<tbody>
						<tr>
							<td><input type="text" id="txtnfl1" name="nro_ficha_lote_first" value="<?= $nro_ficha_lote_first ?>" data-df="0" class="datasend ficha-header form-control only-number formater aftergenerate" data-formater="0000" maxlength="4" placeholder="0000"></td>
							<td>&nbsp;/&nbsp;</td>
							<td><input type="text" id="txtnfl2"  name="nro_ficha_lote_second" value="<?= $nro_ficha_lote_second ?>" data-df="0" class="datasend ficha-header form-control only-number formater aftergenerate" data-formater="0000" maxlength="4" placeholder="0000"></td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>

	</div>
	<div class="row">

		<div class="col-sm-3 col-md-2">
			<div class="form-group">
				<label for="" class="control-label">Codigo predial rentas</label>
				<input type="text" id="txtcod_predial_rentas" name="cod_predial_rentas" value="<?= $cod_predial_rentas ?>" class="datasend ficha-header form-control aftergenerate" placeholder="">
			</div>
		</div>

		<div class="col-sm-3 col-md-2">
			<div class="form-group">
				<label for="" class="control-label">U.A predial rentas</label>
				<input type="text" name="ua_predial_rentas" value="<?= $ua_predial_rentas ?>" class="datasend ficha-header form-control aftergenerate" placeholder="">
			</div>
		</div>

		<div class="col-sm-6 col-md-4">
			<div class="form-group">
				<label for="" class="control-label">Codigo hoja catastral</label>
				<input type="text" name="cod_hoja_catastral" value="<?= $cod_hoja_catastral ?>" class="datasend ficha-header form-control aftergenerate" placeholder="">
			</div>
		</div>
		
		<?php if ( ! $isOnlyView ): ?>

			<div class="col-sm-12 col-md-4 align-right">
				<br>
				<button type="button" id="btn-save-ficha" <?= (!empty($nro_ficha) ? 'disabled': '') ?> class="btn green btn-fieldset">
					Grabar ficha
				</button>
			</div>

		<?php endif; ?>

	</div>
</div>