<style type="text/css">
	.page-wrapper .page-wrapper-middle {
		background-color: #e5e5e5;
	}
	.jumbotron {
		margin-top: 20px;
		border-right: 2px;
		background-color: white;
		box-shadow: 0 0 2px rgba(0,0,0,.12),0 2px 4px rgba(0,0,0,.24);
	}

	.jumbotron h1 {
		font-size: 3.5rem;
		font-weight: 500;
		color: #444;
	}
</style>
<div class="container">
	<div class="row">
		<div class="col-sm-12">
			<div class="jumbotron">
				<h1><?= $ficha ?></h1>
				<p><?= $texto ?>.</p>
				<a href="<?= $url ?>" class="btn green-soft uppercase bold">Cargar ficha almacenada <i class="fa fa-files-o" aria-hidden="true"></i></a>
				<br><br><br>
				<span><strong>Nota:</strong></span><br>
				<span>En caso de no encontrarse la ficha, intente cambiando el año de proceso.</span>
			</div>
		</div>
	</div>
</div>