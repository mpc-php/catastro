<?php

	$up = new stdClass;
	
	if ( ! $isOnlyView ) 
	{
		$up = UCotitularidad::getCookie(CCotitularidad::UP01_NAME_CK);
	}
	else 
	{
		$up = $UP01;
	}

	$cond_titular = isset($up->total_cotitular) ? $up->total_cotitular : 0;

?>

<div role="tabpanel" class="tab-pane active" id="tab_titularidad">

	<div class="container">

		<div class="row">
			<div class="col-xs-12">
				<div class="table-responsive">
					<table id="td-titular" class="table table-custom">
						<thead>
							<tr>
								<th data-align="center" data-field="action"><i class="fa fa-cog" aria-hidden="true"></i></th>
								<th data-align="right" data-field="codigo">Codigo</th>
								<th data-align="right" data-field="Tipo_Doc">Tipo de Doc.</th>
								<th data-align="right" data-field="NRO_DOC">N° Documento</th>
								<th data-align="left" data-field="TITULAR">Titular</th>
								<th data-align="right" data-field="ID_UBI_GEO">Ubigeo</th>
								<th data-align="right" data-field="TELEFONO">Telefono</th>
								<th data-align="left" data-field="mail">Email</th>
								<th data-align="right" data-field="porcentaje">% Pertenencia</th>
							</tr>
						</thead>
					</table>
				</div>
			</div>
		</div>
		<div class="row">
      <div class="col-sm-6">&nbsp;</div>
			<div class="col-sm-4 form-group" style="text-align: right;">
				<table style="display: block; width: 100%;">
					<tbody>
						<tr>
							<td style="padding-right: 10px;">
								<label for="txttotal_cotitular" class="control-label">Total de cotitulares</label></td>
							<td style="width: 20%">
								<input type="text" id="txttotal_cotitular" name="total_cotitular" tabindex="-1" class="form-control" value="<?= $cond_titular; ?>" style="text-align: right;" readonly /></td>
						</tr>
					</tbody>
				</table>
      </div>
			<div class="col-xs-2" style="text-align:right;">
				<button type="button" id="btn-add-titular" class="btn grey-salsa">
					<i class="fa fa-plus" aria-hidden="true"></i>&nbsp;
					Agregar
				</button>
			</div>
		</div>

	</div>

</div>

<?php $this->renderPartial('tabs/titularidad/modal/md_titular'); ?>
<?php $this->renderPartial('tabs/titularidad/modal/md_buscar_titular'); ?>
<?php $this->renderPartial('tabs/titularidad/modal/md_agrega_titular'); ?>
