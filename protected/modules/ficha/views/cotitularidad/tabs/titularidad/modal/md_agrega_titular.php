<section id="md-mng-titular" style="display:none;">
  
  <style type="text/css">
    .toupper {text-transform: uppercase;}
  </style>

  <div class="row">
    <div class="col-xs-12 form-group">
      <label for="cbotipo_titular" class="control-label">Tipo de Titular</label>
      <select id="cbotipo_titular" name="tipo_titular" data-sel="1" data-exechange="true" data-type="07" data-sw="0" class="form-control autoload md-datasend">
        <option value="" selected class="default" disabled>Seleccione...</option>
      </select>
    </div>

    <div class="col-xs-12 form-group">
      <label for="cbotipo_documento" class="control-label">Tipo documento identidad</label>
      <select id="cbotipo_documento" name="tipo_documento" data-type="09" data-sw="0" class="form-control autoload md-datasend">
        <option value="" selected class="default" disabled>Seleccione...</option>
      </select>
    </div>

    <div class="col-xs-12 form-group">
      <label id="lblnro_documento" for="txtnro_documento" class="control-label">N° de Documento</label>
      <input type="text" id="txtnro_documento" name="nro_documento" class="form-control only-number md-datasend">
    </div>

    <div class="col-xs-12 form-group">
      <label id="lblnombre" for="txtnombres" class="control-label">Nombres</label>
      <input type="text" id="txtnombres" name="nombres" class="form-control md-datasend toupper">
    </div>

    <div class="col-xs-12 form-group">
      <label for="txtapepaterno" class="control-label">Apellido Paterno</label>
      <input type="text" id="txtape_paterno" name="ape_paterno" class="form-control md-datasend toupper">
    </div>

    <div class="col-xs-12 form-group">
      <label for="txtapematerno" class="control-label">Apellido Materno</label>
      <input type="text" id="txtape_materno" name="ape_materno" class="form-control md-datasend toupper">
    </div>

    <div class="col-xs-12 form-group">
      <label for="cbopersona_juridica" class="control-label">Persona juridica</label>
      <select id="cbopersona_juridica" name="persona_juridica" data-type="10" data-sw="0" class="form-control autoload md-datasend">
        <option value="" selected class="default" disabled>Seleccione...</option>
      </select>
    </div>

    <div class="col-xs-12 form-group">
      <label for="cboestado_civil" class="control-label">Estado Civil</label>
      <select id="cboestado_civil" name="estado_civil" data-type="08" data-sw="0" class="form-control md-datasend autoload">
        <option value="" selected class="default" disabled>Seleccione...</option>
      </select>
    </div>
  </div>

  <div class="footer-body align-right">
    <button type="submit" id="btn-done" disabled class="btn purple">
      <i class="fa fa-floppy-o" aria-hidden="true"></i>&nbsp;Aceptar
    </button>
    <button type="button" id="btn-close" class="btn default" >
      <i class="fa fa-times" aria-hidden="true"></i>&nbsp;Cerrar
    </button>
  </div>

</section>