<?php

	$up = new stdClass;

	if ( ! $isOnlyView ) 
	{
		$up = UCotitularidad::getCookie(CCotitularidad::UP01_NAME_CK);
	}
	else 
	{
		$up = $UP01;
	}

	$cond_declara 					= isset($up->cond_declara) ? trim($up->cond_declara) : null;
  $estado_llenado 				= isset($up->estado_llenado) ? trim($up->estado_llenado) : null;
	$observaciones 					= isset($up->observaciones) ? $up->observaciones : null;
	$declarante 						= isset($up->declarante) ? $up->declarante : 0;
  $dni_declarante 				= isset($up->dni_declarante) ? $up->dni_declarante : NULL;
  $fecha_declarante 			= isset($up->fecha_declarante) ? $up->fecha_declarante : NULL;
  $tec_catastral 					= isset($up->tec_catastral) ? $up->tec_catastral : NULL;
  $cod_tec_catastral 			= isset($up->cod_tec_catastral) ? $up->cod_tec_catastral : NULL;
  $fecha_tec_catastral 		= isset($up->fecha_tec_catastral) ? $up->fecha_tec_catastral : NULL;
  $supervisor 						= isset($up->supervisor) ? $up->supervisor : 0;
  $cod_supervisor 				= isset($up->cod_supervisor) ? $up->cod_supervisor : NULL;
  $fecha_supervisor 			= isset($up->fecha_supervisor) ? $up->fecha_supervisor : NULL;
  $verif_catastral 				= isset($up->verif_catastral) ? $up->verif_catastral : 0;
  $cod_verif_catastral 		= isset($up->cod_verif_catastral) ? $up->cod_verif_catastral : NULL;
  $fecha_verif_catastral 	= isset($up->fecha_verif_catastral) ? $up->fecha_verif_catastral : NULL;
  $tec_calidad 						= isset($up->tec_calidad) ? $up->tec_calidad : 0;
  $cod_tec_calidad 				= isset($up->cod_tec_calidad) ? $up->cod_tec_calidad : NULL;
  $fecha_tec_calidad 			= isset($up->fecha_tec_calidad) ? $up->fecha_tec_calidad : NULL;

?>

<div role="tabpanel" class="tab-pane" id="tab_infcomplementaria">

	<div class="container">

		<div class="row">
			<div class="col-xs-12">
				<fieldset>
					<legend>Información complementaria</legend>
					<div class="row">
						<div class="col-sm-4">
							<div class="form-group">
								<label for="cbocond_declara" class="control-label">Cond. Declarante</label>
								<select name="cond_declara" id="cbocond_declara" data-sel="<?= $cond_declara; ?>" class="form-control datasend autoload" data-type="25" data-sw="3">
									<option value="" class="default" selected >Seleccione una opción...</option>
								</select>
							</div>
						</div>
						<div class="col-sm-4">
							<div class="form-group">
								<label for="cboestado_llenado" class="control-label">Estado de llenado</label>
								<select name="estado_llenado" id="cboestado_llenado" data-sel="<?= $estado_llenado; ?>" class="form-control datasend autoload" data-type="26" data-sw="3">
									<option value="" class="default" selected >Seleccione una opción...</option>
								</select>
							</div>
						</div>
					</div>

					<div class="row">
						<div class="col-xs-12">
							<div class="form-group">
								<label for="txtobservaciones" class="control-label">Observaciones</label>
								<textarea id="txtobservaciones" name="observaciones" rows="5" class="form-control datasend"><?= $observaciones; ?></textarea>
							</div>
						</div>
					</div>

				</fieldset>
			</div>
		</div>

		<div class="row">
			<div class="col-sm-12">

				<fieldset>
					<legend>Firma del declarante</legend>
					<div class="row">
						<div class="col-sm-3">
							<div class="form-group">
								<label for="chkdeclarante" class="control-label">DNI</label>
								<div class="input-group">
									<span class="input-group-addon">
										<input type="checkbox" name="declarante" id="chkdeclarante" <?= ($declarante == 1) ? 'checked' : '' ?> class="datasend">
									</span>
									<input type="text" id="txtdni_declarante" name="dni_declarante" value="<?= $dni_declarante; ?>" minlength="8" maxlength="8" data-rel="pe" data-t="1" data-u="00" data-inputref="#txtdesc_declarante" class="form-control datasend getdata">
								</div>
							</div>
						</div>
						<div class="col-sm-3">
							<div class="form-group">
								<label for="txtfecha_declarante" class="control-label">Fecha</label>
								<input type="text" id="txtfecha_declarante" name="fecha_declarante" value="<?= $fecha_declarante; ?>" class="form-control datasend maskdate"  data-inputmask="'alias': 'date'" placeholder="dd/mm/yyyy">
							</div>
						</div>
						<div class="col-sm-6">
							<div class="form-group">
								<label for="txtdesc_declarante" class="control-label" disabled>&nbsp;</label>
								<input type="text" id="txtdesc_declarante" class="form-control" disabled>
							</div>
						</div>
					</div>
				</fieldset>

			</div>
		</div>

		<div class="row">
			<div class="col-sm-12">

				<fieldset>
					<legend>Firma del supervisor</legend>
					<div class="row">
						<div class="col-sm-3">
							<div class="form-group">
								<label for="chksupervisor" class="control-label">Código</label>
								<div class="input-group">
									<span class="input-group-addon">
										<input type="checkbox" name="supervisor" id="chksupervisor" <?= ($supervisor == 1) ? 'checked' : '' ?> class="datasend">
									</span>
									<input type="text" id="txtcod_supervisor" name="cod_supervisor" value="<?= $cod_supervisor; ?>" data-rel="us" data-t="1" data-u="02" data-inputref="#txtdesc_supervisor" class="form-control datasend getdata">
								</div>
							</div>
						</div>
						<div class="col-sm-3">
							<div class="form-group">
								<label for="txtfecha_supervisor" class="control-label">Fecha</label>
								<input type="text" id="txtfecha_supervisor" name="fecha_supervisor" value="<?= $fecha_supervisor; ?>" class="form-control datasend maskdate" data-inputmask="'alias':'date'" placeholder="dd/mm/yyyy">
							</div>
						</div>
						<div class="col-sm-6">
							<div class="form-group">
								<label for="txtdesc_supervisor" class="control-label">&nbsp;</label>
								<input type="text" id="txtdesc_supervisor" class="form-control" disabled>
							</div>
						</div>
					</div>
				</fieldset>

			</div>
		</div>

		<div class="row">
			<div class="col-sm-12">

				<fieldset>
					<legend>Firma del técnico catastral</legend>
					<div class="row">
						<div class="col-sm-3">
							<div class="form-group">
								<label for="chktec_catastral" class="control-label">Código</label>
								<div class="input-group">
									<span class="input-group-addon">
										<input type="checkbox" name="tec_catastral" id="chktec_catastral" <?= ($tec_catastral == 1) ? 'checked' : '' ?> class="datasend">
									</span>
									<input type="text" id="txtcod_tec_catastral" name="cod_tec_catastral" value="<?= $cod_tec_catastral; ?>" data-rel="us" data-t="1" data-u="03" data-inputref="#txtdesc_tec_catastral" class="form-control datasend getdata">
								</div>
							</div>
						</div>
						<div class="col-sm-3">
							<div class="form-group">
								<label for="txtfecha_tec_catastral" class="control-label">Fecha</label>
								<input type="text" id="txtfecha_tec_catastral" name="fecha_tec_catastral" value="<?= $fecha_tec_catastral; ?>" class="form-control datasend maskdate" data-inputmask="'alias':'date'" placeholder="dd/mm/yyyy">
							</div>
						</div>
						<div class="col-sm-6">
							<div class="form-group">
								<label for="txtdesc_tec_catastral" class="control-label">&nbsp;</label>
								<input type="text" id="txtdesc_tec_catastral" class="form-control" disabled>
							</div>
						</div>
					</div>
				</fieldset>

			</div>
		</div>

		<div class="row">
			<div class="col-sm-12">

				<fieldset>
					<legend>Firma del técnico calidad</legend>
					<div class="row">
						<div class="col-sm-3">
							<div class="form-group">
								<label for="chktec_calidad" class="control-label">Código</label>
								<div class="input-group">
									<span class="input-group-addon">
										<input type="checkbox" name="tec_calidad" id="chktec_calidad" <?= ($tec_calidad == 1) ? 'checked' : '' ?> data-activate="#txtcod_tec_calidad,#txtfecha_tec_calidad" class="datasend activated">
									</span>
									<input type="text" id="txtcod_tec_calidad" name="cod_tec_calidad" value="<?= $cod_tec_calidad; ?>" data-rel="us" data-t="1" data-u="07" data-inputref="#txtdesc_tec_calidad" class="form-control datasend getdata">
								</div>
							</div>
						</div>
						<div class="col-sm-3">
							<div class="form-group">
								<label for="" class="control-label">Fecha</label>
								<input type="text" id="txtfecha_tec_calidad" name="fecha_tec_calidad" value="<?= $fecha_tec_calidad; ?>" class="form-control datasend maskdate" placeholder="dd/mm/yyyy" data-inputmask="'alias':'date'">
							</div>
						</div>
						<div class="col-sm-6">
							<div class="form-group">
								<label for="txtdesc_tec_calidad" class="control-label">&nbsp;</label>
								<input type="text" id="txtdesc_tec_calidad" class="form-control" disabled>
							</div>
						</div>
					</div>
				</fieldset>

			</div>
		</div>

		<div class="row">
			<div class="col-sm-12">

				<fieldset>
					<legend>V° B° del verificador catastral</legend>
					<div class="row">
						<div class="col-sm-3">
							<div class="form-group">
								<label for="chkverif_catastral" class="control-label">Código</label>
								<div class="input-group">
									<span class="input-group-addon">
										<input type="checkbox" name="verif_catastral" id="chkverif_catastral" <?= ($verif_catastral == 1) ? 'checked' : '' ?> data-activate="#txtcod_verif_catastral,#txtfecha_verif_catastral" class="datasend activated">
									</span>
									<input type="text" id="txtcod_verif_catastral" name="cod_verif_catastral"  value="<?= $cod_verif_catastral; ?>" data-rel="us" data-t="1" data-u="04" data-inputref="#txtdesc_verif_catastral" class="form-control datasend getdata">
								</div>
							</div>
						</div>
						<div class="col-sm-3">
							<div class="form-group">
								<label for="txtfecha_verif_catastral" class="control-label">Fecha</label>
								<input type="text" id="txtfecha_verif_catastral" name="fecha_verif_catastral" value="<?= $fecha_verif_catastral; ?>" class="form-control datasend maskdate" placeholder="dd/mm/yyyy" data-inputmask="'alias':'date'">
							</div>
						</div>
						<div class="col-sm-6">
							<div class="form-group">
								<label for="txtdesc_verif_catastral" class="control-label">&nbsp;</label>
								<input type="text" id="txtdesc_verif_catastral" class="form-control" disabled>
							</div>
						</div>
					</div>
				</fieldset>

			</div>
		</div>

	</div>

</div>

<?php $this->renderPartial('tabs/infcomplementaria/modal/md_buscar'); ?>
