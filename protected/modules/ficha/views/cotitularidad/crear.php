<!--
	El ID es importante para que los modulos de javascript no se activen
	en una pagina que no le corresponde
-->

<div id="page" data-module="<?= $action ?>" class="page-head indicrear">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 header-title" style="text-align: center;">
				<h3 style="text-transform: uppercase;">Nueva ficha cotitularidad</h3>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12">
				<fieldset class="groupbox">
					<legend>Datos de la ficha</legend>
					<?php 

						$data['action'] = $action;
						$data['isOnlyView'] = $isOnlyView;

						if ( $isOnlyView ) 
						{
							$data['HEADER'] = $head;
							$data['UP01'] = $up01;
						}

						$this->renderPartial('partial/header', $data); 
						
					?>
				</fieldset>
			</div>
		</div>
	</div>
</div>

<div id="cnt-data" class="tabbable-line indicrear disabled">

	<!--
	|   pESTAÑAS
	-->
	<ul id="tabs-control" class="nav nav-tabs nav-center tab-custom" role="tablist">
		<li role="presentation" class="active">
			<a href="#tab_titularidad" data-next="#tab_infcomplementaria" data-preview="#tab_infcomplementaria" data-firstelement="btn-add-titular" aria-controls="titularidad" role="tab" data-toggle="tab" tabindex="-1" title="Cotitularidad">
				<span class="hidden-xs hidden-sm hidden-md">Ident. Cotitular catastral</span>
				<span class="glyphicon glyphicon-user hidden-lg"></span>
			</a>
		</li>
		<li role="presentation">
			<a href="#tab_infcomplementaria" data-next="#tab_titularidad" data-preview="#tab_titularidad" data-firstelement="cbocond_declara" aria-controls="infcomplentaria" role="tab" data-toggle="tab" tabindex="-1" title="Información Complementaria">
				<span class="hidden-xs hidden-sm hidden-md">Inf. Complementaria</span>
				<span class="glyphicon glyphicon-link hidden-lg"></span>
			</a>
		</li>
	</ul>
	
	<!--
	|   cONTENEDOR DE LOS PANELES DE LAS PESTAÑAS
	-->
	<div class="tab-content">
	<?php 

		$this->renderPartial('tabs/titularidad/titularidad', $data);

		/**
		 * oTROS
		 */
		$this->renderPartial('tabs/infcomplementaria/infcomplementaria', $data);
	?>
	</div>
	
	<?php if (! $isOnlyView): ?>
		<div class="container">
			<div class="row">
				<div class="col-xs-12" style="text-align:right; padding: 20px; background-color: white; margin-top: -20px; box-shadow: 0 1px 4px 0 rgba(0, 0, 0, 0.14);">
					<?php 
						if ( $action == 'create' ) {
							$text_btn = "Limpiar ficha";
						} else {
							$text_btn = "Salir de ficha";
						}
					?>
					<button type="button" tabindex="-1" id="btn-reset-ficha" style="float:left" class="btn yellow btn-outline btn-first-disabled" disabled><?= $text_btn ?></button>
					
					<?php if ($action == 'create'): ?>
						<button type="button" id="btn-observation" style="float: left; margin-left: 10px;" class="btn blue-hoki btn-outline sbold uppercase btn-first-disabled" disabled>Generar observación</button>
					<?php endif; ?>

					<button type="button" tabindex="-1" id="btn-general" class="btn green btn-first-disabled" disabled>Guardar datos de ficha</button>
				</div>
			</div>
		</div>
	<?php endif; ?>
	

</div>

<?php if ($action == 'create'): ?>

	<?php $this->renderPartial('../_modules/md_observation'); ?>

<?php endif; ?>