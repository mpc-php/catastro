<?php

/**
 * Descripcción para Principal
 *
 * @author Nombre del Programador <correodelprogramador@email.com>
 * @package Web\Modules\Inicio\Controllers
 */
class ProcesoController extends Auth {

    public function actionCambio() 
    {
			try 
			{

				if (!Yii::app()->request->isAjaxRequest)
					throw new Exception("El metodo no esta permitido", 403);

				if (!isset($_POST['year']))
					throw new Exception("Faltan parametros para continuar con la acción", 400);

				$newKey = $_POST['year'];
				$items = Yii::app()->user->proceso['items'];
				$error = TRUE;

				if ( array_key_exists($newKey, $items) )
				{
					Yii::app()->user->setState("proceso", [
						'items' => $items,
						'selected' => $items[$newKey]
					]);

					$error = FALSE;
				}

				JSON::response($error, 200, '', []);
			} 
			catch (Exception $ex) 
			{
				JSON::response(TRUE, $ex->getCode(), $ex->getMessage(), []);
			}
    }

}