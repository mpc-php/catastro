

<?php

/**
 * Descripcción para Principal
 *
 * @author Nombre del Programador <correodelprogramador@email.com>
 * @package Web\Modules\Inicio\Controllers
 */
class PrincipalController extends Auth {

    public function actionIndex() {
      $this->render('index');
    }

}
