var popup = function (size) {
    bootbox.dialog({
        size: size,
        animate: false,
        message: "I am a custom dialog<a class='btn btn-primary bootboxe'>Click Popup</a>",
        title: "Custom title",
        buttons: {
            success: {
                label: "Success!",
                className: "btn-success",
                callback: function () {
                }
            },
            danger: {
                label: "Danger!",
                className: "btn-danger",
                callback: function () {
                }
            },
            main: {
                label: "Click ME!",
                className: "btn-primary",
                callback: function () {
                }
            }
        }
    });
    $(".bootboxe").click(function () {
        popup("small");
    });

}
popup("large");




