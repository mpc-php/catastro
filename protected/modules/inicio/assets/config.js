/**
 * Configuración personalizada para la carga de librerias del módulo
 * 
 * @type Object
 */
var Builder = {
    module: {
        controllers: {
            principal: {
                actions: {
                    bootbox: {
//Assets para un controlador especifico y una acción especifica
                        js: [],
                        css: {
                            libs: [],
                            package: [],
                            custom: []
                        }
                    },
                    holder: {
                        //Assets para un controlador especifico y una acción especifica
                        js: [],
                        css: {
                            libs: [],
                            package: [],
                            custom: []
                        }
                    },
                    holderimagen: {
                        //Assets para un controlador especifico y una acción especifica
                        js: [],
                        css: {
                            libs: [],
                            package: [],
                            custom: []
                        }
                    }
                },
                //Assets para un controllador especifico y todas sus acciones
                js: [],
                css: {
                    libs: [],
                    package: [],
                    custom: []
                }
            }
        },
        //Assets para todo el módulo, todos sus controlladores y todas sus acciones
        js: ["popup"],
        css: {
            libs: ["metronic/pages/css/faq.min"],
            package: [],
            custom: []
        }
    }
};
