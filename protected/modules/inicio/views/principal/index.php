<!-- BEGIN CONTENT BODY -->
<!-- BEGIN PAGE HEAD-->
<div class="page-head">
    <div class="container">
        <!-- BEGIN PAGE TITLE -->
        <div class="page-title">
            <h1>FAQ
                <small>Preguntas Frecuentes</small>
            </h1>
        </div>
        <!-- END PAGE TITLE -->
    </div>
</div>
<!-- END PAGE HEAD-->
<!-- BEGIN PAGE CONTENT BODY -->
<div class="page-content">
    <div class="container">
        <!-- BEGIN PAGE CONTENT INNER -->
        <div class="page-content-inner">
            <div class="faq-page faq-content-1">
                <div class="search-bar ">
                    <div class="input-group">
                        <input type="text" class="form-control" placeholder="Search for..."> </div>
                </div>
                <div class="faq-content-container">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="faq-section ">
                                <h2 class="faq-title uppercase font-blue">Atajos de Teclado</h2>
                                <div class="panel-group accordion faq-content" id="accordion3">
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <h4 class="panel-title">
                                                <i class="fa fa-circle"></i>
                                                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion3" href="#collapse_3_1"> ¿En qué ventanas puedo usar atajos de teclado?</a>
                                            </h4>
                                        </div>
                                        <div id="collapse_3_1" class="panel-collapse collapse">
                                            <div class="panel-body">
                                                <p> En las ventanas del módulo de Digitación (creación y modificación de fichas). </p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <h4 class="panel-title">
                                                <i class="fa fa-circle"></i>
                                                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion3" href="#collapse_3_2"> ¿Qué clase de atajos se pueden usar?</a>
                                            </h4>
                                        </div>
                                        <div id="collapse_3_2" class="panel-collapse collapse">
                                            <div class="panel-body">
                                                <p> Cambiar de pestañas.</p>
                                                <p> Cambiar entre campos.</p>
                                                <p> Abrir las ventanas de búsqueda.</p>
                                                <p> Acceso directo al listado de fichas principales (En desarrollo).</p>
                                                <p> Acceso directo a la creación de fichas principales (En desarrollo).</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <h4 class="panel-title">
                                                <i class="fa fa-circle"></i>
                                                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion3" href="#collapse_3_3"> ¿Cuál es el atajo para cambiar entre pestañas?</a>
                                            </h4>
                                        </div>
                                        <div id="collapse_3_3" class="panel-collapse collapse">
                                            <div class="panel-body">
                                                <p> Ir hacia la pestaña de la derecha, CTRL + ->. </p>
                                                <p> Ir hacia la pestaña de la izquierda, CTRL + <-. </p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <h4 class="panel-title">
                                                <i class="fa fa-circle"></i>
                                                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion3" href="#collapse_3_4"> ¿Cuál es el atajo para cambiar entre campos?</a>
                                            </h4>
                                        </div>
                                        <div id="collapse_3_4" class="panel-collapse collapse">
                                            <div class="panel-body">
                                                <p> En un formulario, se presiona la tecla TAB. </p>
                                                <p> En una tabla, la combinación SHIFT + Flechas direccionales. </p>
                                                <p> Al encontrarse en el último campo de un formulario, si se presiona la tecla TAB, se cambia al primer campo de la siguiente pestaña. </p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <h4 class="panel-title">
                                                <i class="fa fa-circle"></i>
                                                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion3" href="#collapse_3_5"> ¿Cuál es el atajo para abrir las ventanas de búsqueda?</a>
                                            </h4>
                                        </div>
                                        <div id="collapse_3_5" class="panel-collapse collapse">
                                            <div class="panel-body">
                                                <p> Para las búsqueda de hab. urbanas, vías y personas, se puede utilizar la tecla F1 para abrir la ventana de búsqueda. </p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <h4 class="panel-title">
                                                <i class="fa fa-circle"></i>
                                                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion3" href="#collapse_3_6"> ¿Cuál es el atajo para el acceso directo del listado de fichas?</a>
                                            </h4>
                                        </div>
                                        <div id="collapse_3_6" class="panel-collapse collapse">
                                            <div class="panel-body">
                                                <p> Para el listado de fichas individuales, se presionará la combinación ALT + I. </p>
                                                <p> Para el listado de fichas cotitularidad, se presionará la combinación ALT + C. </p>
                                                <p> Para el listado de fichas activ. econom., se presionará la combinación ALT + A. </p>
                                                <p> Para el listado de fichas bienes comunes, se presionará la combinación ALT + B. </p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <h4 class="panel-title">
                                                <i class="fa fa-circle"></i>
                                                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion3" href="#collapse_3_7"> ¿Cuál es el atajo para el acceso directo de la creación de fichas?</a>
                                            </h4>
                                        </div>
                                        <div id="collapse_3_7" class="panel-collapse collapse">
                                            <div class="panel-body">
                                                <p> Para la creación de fichas individuales, se presionará la combinación ALT + SHIFT + I. </p>
                                                <p> Para la creación de fichas cotitularidad, se presionará la combinación ALT + SHIFT + C. </p>
                                                <p> Para la creación de fichas activ. econom., se presionará la combinación ALT + SHIFT + A. </p>
                                                <p> Para la creación de fichas bienes comunes, se presionará la combinación ALT + SHIFT + B. </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="faq-section ">
                                <h2 class="faq-title uppercase font-blue">Módulo de Observación</h2>
                                <div class="panel-group accordion faq-content" id="accordion2">
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <h4 class="panel-title">
                                                <i class="fa fa-circle"></i>
                                                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapse_2_1"> ¿Qué es el módulo de Observación?</a>
                                            </h4>
                                        </div>
                                        <div id="collapse_2_1" class="panel-collapse collapse">
                                            <div class="panel-body">
                                                <p> El módulo de observación es el medio por el cuál el digitador indica excepciones que no le permitan guardar una ficha. </p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <h4 class="panel-title">
                                                <i class="fa fa-circle"></i>
                                                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapse_2_2"> ¿En qué momento se puede usar el módulo de Observación?</a>
                                            </h4>
                                        </div>
                                        <div id="collapse_2_2" class="panel-collapse collapse">
                                            <div class="panel-body">
                                                <p> Se utiliza exclusivamente en la ventana de creación de fichas, y se habilita después de haber generado la ficha. </p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <h4 class="panel-title">
                                                <i class="fa fa-circle"></i>
                                                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapse_2_5"> ¿Cómo se utiliza el módulo de Observaciones?</a>
                                            </h4>
                                        </div>
                                        <div id="collapse_2_5" class="panel-collapse collapse">
                                            <div class="panel-body">
                                                <p> Al presionar el botón "Generar Observación", se abrirá una ventana la cuál permitirá registrar una observación general y/o indicar que campo mínimo requerido no pudo ser registrado o generó error. </p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <h4 class="panel-title">
                                                <i class="fa fa-circle"></i>
                                                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapse_2_3"> ¿Qué clase de excepciones pueden haber que requiera el uso del módulo de Observaciones?</a>
                                            </h4>
                                        </div>
                                        <div id="collapse_2_3" class="panel-collapse collapse">
                                            <div class="panel-body">
                                                <p> No cumplir con los campos mínimos requeridos del registro de la ficha. </p>
                                                <p> Cuando se encuentren dificultades en la ficha física (mala digitación, mala impresión, papel dañado, etc). </p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <h4 class="panel-title">
                                                <i class="fa fa-circle"></i>
                                                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapse_2_4"> ¿De qué nos sirve registrar las excepciones en el módulo de Observación?</a>
                                            </h4>
                                        </div>
                                        <div id="collapse_2_4" class="panel-collapse collapse">
                                            <div class="panel-body">
                                                <p> Tener un control de las incidencias ocurridas que no permita registrar una ficha. </p>
                                                <p> Generar un reporte de las incidencias más comunes de una ficha física. </p>
                                                <p> Visualizar las gestiones de las excepciones registradas por el digitador. (En desarrollo)</p>
                                            </div>
                                        </div>
                                    </div>                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- END PAGE CONTENT INNER -->
    </div>
</div>
<!-- END PAGE CONTENT BODY -->
<!-- END CONTENT BODY -->