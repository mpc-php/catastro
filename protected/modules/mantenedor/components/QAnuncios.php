<?php

/**
 * Clase que se encarga de las funciones que se conectarán a la Base de Datos para el submodulo de Anuncios
 *
 * @author Francisco Isasi Chiesa <franco16fra@gmail.com>
 * @package Catastro\Modules\Mantenedor\Components
 */
class QAnuncios {

    /**
     * Funcion que obtiene los anuncios
     * 
     * @return Array
     */
    public static function get_Anuncios() {
        $sql     = "SELECT COD_ANUNCIO code, DESC_ANUNCIO name FROM ANUNCIOS";
        $command = Yii::app()->db->createCommand($sql);
        $table   = $command->queryAll();
        return $table;
    }

    /**
     * Funcion que registra o actualiza un anuncio mediante la data enviada
     * 
     * @param Array $data
     * @return boolean
     */
    public static function manage_Anuncios($data = []) {
        $command = Yii::app()->db->createCommand("EXECUTE usp_Anuncio :code, :name");

        $command->bindValue(":code", $data->code, PDO::PARAM_STR);
        $command->bindValue(":name", mb_strtoupper($data->name, 'UTF-8'), PDO::PARAM_STR);

        $confirm = $command->execute();

        return $confirm;
    }

    /**
     * Funcion que elimina un anuncio por su codigo
     * 
     * @param String $code Codigo del anuncio a ser eliminada
     * @return boolean
     */
    public static function delete_Anuncios($code) {
        $command = Yii::app()->db->createCommand("EXECUTE usp_Anuncio_Del01 :code");

        $command->bindValue(":code", $code, PDO::PARAM_STR);

        $confirm = $command->execute();

        return $confirm;
    }

}