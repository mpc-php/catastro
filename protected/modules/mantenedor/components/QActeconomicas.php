<?php

/**
 * Clase que se encarga de las funciones que se conectarán a la Base de Datos para el submodulo de Actividades Economicas
 *
 * @author Francisco Isasi Chiesa <franco16fra@gmail.com>
 * @package Catastro\Modules\Mantenedor\Components
 */
class QActeconomicas {

    /**
     * Funcion que obtiene las actividades economicas
     * 
     * @param String $search
     * @return Array
     */
    public static function get_ActividadesEconomicas($search = '') {
        $sql = "SELECT 
							COD_ACTIVIDAD code, 
							DESC_ACTIVIDAD name
						FROM ACTIVIDADES  
						WHERE 
							COD_ACTIVIDAD LIKE '%'+:search1+'%' 
						AND 
							DESC_ACTIVIDAD LIKE '%'+:search2+'%'";

        $command = Yii::app()->db->createCommand($sql);

        $command->bindValue(":search1", $search, PDO::PARAM_STR);
        $command->bindValue(":search2", $search, PDO::PARAM_STR);

        $table = $command->queryAll();

        return $table;
    }

    /**
     * Funcion que registra o actualiza una actividad economica mediante la data enviada
     * 
     * @param Array $data
     * @return boolean
     */
    public static function manage_ActividadesEconomicas($data) {
        $command = Yii::app()->db->createCommand("EXEC usp_Actividad :code, :name");

        $command->bindValue(":code", $data->code, PDO::PARAM_STR);
        $command->bindValue(":name", mb_strtoupper($data->name, 'UTF-8'), PDO::PARAM_STR);

        $confirm = $command->execute();

        return $confirm;
    }

    /**
     * Funcion que elimina una actividad economica por su codigo
     * 
     * @param String $code Codigo de la actividad economica a ser eliminada
     * @return boolean
     */
    public static function delete_ActividadesEconomicas($code) {
        $command = Yii::app()->db->createCommand("EXEC usp_Actividad_Del01 :code");

        $command->bindValue(":code", $code, PDO::PARAM_STR);

        $confirm = $command->execute();

        return $confirm;
    }

}
