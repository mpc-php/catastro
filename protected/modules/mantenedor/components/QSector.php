<?php

/**
 * Clase que se encarga de las funciones que se conectarán a la Base de Datos para el submodulo de Sector
 *
 * @author Francisco Isasi Chiesa <franco16fra@gmail.com>
 * @package Catastro\Modules\Mantenedor\Components
 */
class QSector {

    /**
     * Funcion que obtiene los sectores
     * 
     * @return Array
     */
    public static function get_Sectores() {
//        $sql     = "SELECT CodSector code, DesSector name, 'ACTIVO' status FROM MaeSector WHERE EstSector = 1";
        $sql = "SELECT sector.COD_SECTOR code,mae.DesSector name,sector.ID_CATASTRO anio, 'ACTIVO' status FROM MaeSector mae 
                RIGHT JOIN SECTORES sector ON mae.CodSector = sector.COD_SECTOR AND mae.Anio = sector.ID_CATASTRO
                where mae.EstSector = 1 ORDER BY sector.COD_SECTOR ";
        $command = Yii::app()->db->createCommand($sql);
        $model = $command->queryAll();
        return $model;
    }

    /**
     * Funcion que registra o actualiza un sector mediante la data enviada
     * 
     * @param Array $data
     * @return boolean
     */
    public static function manage_Sector($data = []) {
        $command = Yii::app()->db->createCommand("EXEC sp_MantenimientoMaeSector :code, :name, :anio, :status");

        $command->bindValue(":code", $data->code, PDO::PARAM_STR);
        $command->bindValue(":name", mb_strtoupper($data->name, 'UTF-8'), PDO::PARAM_STR);
        $command->bindValue(":anio", $data->anio, PDO::PARAM_STR);
        $command->bindValue(":status", Constante::ACTIVO, PDO::PARAM_INT);

        $confirm = $command->execute();

        return $confirm;
    }

    /**
     * Funcion que elimina un sector por su codigo
     * 
     * @param String $code Codigo del sector a ser eliminado
     * @return boolean
     */
    public static function delete_Sector($code,$anio) {
        $command = Yii::app()->db->createCommand("update MaeSector set EstSector=0 where CodSector= :code and anio =:anio");

        $command->bindValue(":code", $code, PDO::PARAM_STR);
        $command->bindValue(":anio", $anio, PDO::PARAM_STR);

        $confirm = $command->execute();

        return $confirm;
    }

    /**
     * Funcion que obtiene las zonificaciones
     * 
     * @param String $id ID del Sector para obtener su zonificacion.
     * @param boolean $pertenece parametro para indicar si pertenece o no a la zona
     * @return Array
     */
    public static function obtenerZonificacion($id, $pertenece) {
        if ($pertenece) {
            $condicion = 1;
        } else {
            $condicion = 0;
        }
        $sql = "EXECUTE sp_ShowSectorZona '{$id}',{$condicion}";
        $command = Yii::app()->db->createCommand($sql);
        $model = $command->queryAll();
        return $model;
    }

    /**
     * Funcion que obtiene las habilitaciones urbanas
     * 
     * @param String $id ID del Sector para obtener su hab. urbanas.
     * @param boolean $pertenece parametro para indicar si pertenece o no a la zona
     * @return Array
     */
    public static function obtenerHabUrbana($id, $pertenece) {
        if ($pertenece) {
            $condicion = 1;
        } else {
            $condicion = 0;
        }
        $sql = "EXECUTE sp_ShowSectorHabUrba '{$id}',{$condicion}";
        $command = Yii::app()->db->createCommand($sql);
        $model = $command->queryAll();
        return $model;
    }

    /**
     * Funcion para asignar y desasignar habilitaciones urbanas de un sector
     * 
     * @param String $strUsosAEco Cadena de habilitaciones urbanas asignadas
     * @param String $codeUso codigo de la zona
     * @param String $strAEco cadena de habilitaciones urbanas no asignadas
     * @return boolean
     */
    public static function assign_Sector_HabUrbana($strUsosAEco, $codeUso, $strAEco) {

        $command = Yii::app()->db->createCommand("EXEC SP_MantenimientoSectorHabUrba :CadIdUsoActividad, :CodUso, :CadActividad");

        $command->bindValue(":CadIdUsoActividad", $strUsosAEco, PDO::PARAM_STR);
        $command->bindValue(":CodUso", $codeUso, PDO::PARAM_STR);
        $command->bindValue(":CadActividad", $strAEco, PDO::PARAM_STR);

        $table = $command->execute();


        return $table;
    }

    /**
     * Funcion para asignar y desasignar zonificaciones de un sector
     * 
     * @param String $strUsosAEco Cadena de zonificaciones asignadas
     * @param String $codeUso codigo de la zona
     * @param String $strAEco cadena de zonificaciones no asignadas
     * @return boolean
     */
    public static function assign_Sector_Zonificacion($strUsosAEco, $codeUso, $strAEco) {

        $command = Yii::app()->db->createCommand("EXEC SP_MantenimientoSectorZona :CadIdUsoActividad, :CodUso, :CadActividad");

        $command->bindValue(":CadIdUsoActividad", $strUsosAEco, PDO::PARAM_STR);
        $command->bindValue(":CodUso", $codeUso, PDO::PARAM_STR);
        $command->bindValue(":CadActividad", $strAEco, PDO::PARAM_STR);

        $table = $command->execute();


        return $table;
    }

}
