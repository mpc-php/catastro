<?php

/**
 * Clase que se encarga de las funciones que se conectarán a la Base de Datos para el submodulo de Vias
 *
 * @author Francisco Isasi Chiesa <franco16fra@gmail.com>
 * @package Catastro\Modules\Mantenedor\Components
 */
class QVia {

     /**
     * Funcion que obtiene las vias
     * 
     * @return Array
     */
    public static function get_Vias() {
        $sql     = "SELECT vw.tipoVia type, vw.codVia code, vw.nomVia name FROM vw_mantenedorVias vw";
        $command = Yii::app()->db->createCommand($sql);
        $model   = $command->queryAll();
        return $model;
    }
    
    /**
     * Funcion que registra o actualiza una via mediante la data enviada
     * 
     * @param Array $data
     * @return boolean
     */
    public static function manage_Via($data = []) {
        $command = Yii::app()->db->createCommand("EXEC TABLA_VIA :type, :code, :name");

        $command->bindValue(":type", $data->type, PDO::PARAM_STR);
        $command->bindValue(":code", $data->code, PDO::PARAM_STR);
        $command->bindValue(":name", mb_strtoupper($data->name, 'UTF-8'), PDO::PARAM_STR);

        $confirm = $command->execute();

        return $confirm;
    }

    /**
     * Funcion que elimina una via por su codigo
     * 
     * @param String $code Codigo de la via a ser eliminada
     * @return boolean
     */
    public static function delete_Via($code) {
        $command = Yii::app()->db->createCommand("EXEC DELETE_VIA :code");

        $command->bindValue(":code", $code, PDO::PARAM_STR);

        $confirm = $command->execute();

        return $confirm;
    }

    /**
     * Funcion que obtiene los tipos de vias
     * 
     * @return Array
     */
    public static function obtenerTipoVias() {
        $sql     = "EXECUTE SP_TIPOS '02',0";
        $command = Yii::app()->db->createCommand($sql);
        $model   = $command->queryAll();
        return $model;
    }

}
