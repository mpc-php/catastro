<?php

/**
 * Clase que se encarga de las funciones que se conectarán a la Base de Datos para el submodulo de Tecnicos
 *
 * @author Franklin Ruiz ASto Leon <franklin.asto.leon@gmail.com>
 * @package Catastro\Modules\Mantenedor\Components
 */
class QTecnico {

    /**
     * Funcion que obtiene a los Tecnicos
     * 
     * @return Array
     */
    public static function get_Tecnico() {
        $sql = "SELECT * FROM vw_tecnico";
        $command = Yii::app()->db->createCommand($sql);
        $model = $command->queryAll();
        return $model;
    }

    /**
     * Funcion que registra o actualiza un tecnico mediante la data enviada
     * 
     * @param Array $data
     * @return boolean
     */
    public static function manage_Tecnico($data = []) {
        $command = Yii::app()->db->createCommand("EXEC TABLE_TECNICO :id, :idpeople, :type, :code, :name, :last_name1, :last_name2, :ides");

        $command->bindValue(":id", $data->id, PDO::PARAM_STR);
        $command->bindValue(":idpeople", $data->idpeople, PDO::PARAM_STR);
        $command->bindValue(":type", $data->type, PDO::PARAM_STR);
        $command->bindValue(":code", $data->code, PDO::PARAM_STR);
        $command->bindValue(":name", mb_strtoupper($data->name, 'UTF-8'), PDO::PARAM_STR);
        $command->bindValue(":last_name1", mb_strtoupper($data->last_name1, 'UTF-8'), PDO::PARAM_STR);
        $command->bindValue(":last_name2", mb_strtoupper($data->last_name2, 'UTF-8'), PDO::PARAM_STR);
        $command->bindValue(":ides", $data->ides, PDO::PARAM_INT);

        $confirm = $command->execute();

        return $confirm;
    }

    /**
     * Funcion que elimina un Tecnico por su codigo
     * 
     * @param String $id Codigo del tecnico a ser eliminada de manera logica
     * @return boolean
     */
    public static function delete_Tecnico($id) {
        $command = Yii::app()->db->createCommand("EXEC DELETE_TECNICO :id");
        $command->bindValue(":id", $id, PDO::PARAM_STR);
        $confirm = $command->execute();
        return $confirm;
    }

    /**
     * Funcion que obtiene los tipos de cargo
     * 
     * @return Array
     */

    public static function get_Cargo() {
        $sql = "SELECT * FROM  TABLAS_DETALLE WHERE CODIGO IN('02','03','04','07') AND ID_TABLA = 33";
        $command = Yii::app()->db->createCommand($sql);
        $model = $command->queryAll();
        return $model;
    }

    public static function buscar_Tecnico($ndoc) {
        $command = Yii::app()->db->createCommand("SELECT 
                                                  U.ID_PERSONA 
                                                  FROM USUARIOS_BAK U
                                                  INNER JOIN PERSONAS P ON U.ID_PERSONA = P.ID_PERSONA 
                                                  WHERE P.NRO_DOC = :dni AND U.ESTADO = 'A'");
        $command->bindValue(":dni", $ndoc, PDO::PARAM_STR);
        $model = $command->queryAll();
        return $model;
    }
    
     public static function recuperar_Tecnico($ndoc) {
        $command = Yii::app()->db->createCommand("SELECT 
                                                  U.ID_PERSONA 
                                                  FROM USUARIOS_BAK U
                                                  INNER JOIN PERSONAS P ON U.ID_PERSONA = P.ID_PERSONA 
                                                  WHERE P.NRO_DOC = :dni AND U.ESTADO = 'I'");
        $command->bindValue(":dni", $ndoc, PDO::PARAM_STR);
        $model = $command->queryAll();
        return $model;
    }
    
    public static function recover_Tecnico($ndoc) {
        $command = Yii::app()->db->createCommand("EXEC RECOVER_TECNICO :dni");
        $command->bindValue(":dni", $ndoc, PDO::PARAM_STR);
        $confirm = $command->execute();
        return $confirm;
    }
    
     public static function buscar_persona($ndoc) {
        $command = Yii::app()->db->createCommand("SELECT ID_PERSONA,NOMBRES,APE_PATERNO,APE_MATERNO FROM PERSONAS WHERE NRO_DOC = :dni");
        $command->bindValue(":dni", $ndoc, PDO::PARAM_STR);
        $model = $command->queryAll();
        return $model;
    }

}
