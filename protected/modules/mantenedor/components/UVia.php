<?php

/**
 * Clase que se encargará de funciones a ser reutilizadas en el modulo Mantenedor para las Vias
 *
 * @author Francisco Isasi Chiesa <franco16fra@gmail.com>
 * @package Catastro\Modules\Mantenedor\Components
 */
class UVia {

    /**
     * Funcion que devuelve el listado de tipo de vias en formato para ser usadas en un SELECT
     * 
     * @return array 
     */
    public static function obtenerTipoVias() {
        $models = QVia::obtenerTipoVias();

        if ($models) {
            return CHtml::listData($models, 'CODIGO', 'NOMBRE');
        } else {
            return array();
        }
    }

}
