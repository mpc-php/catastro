<?php

/**
 * Clase que se encarga de las funciones que se conectarán a la Base de Datos para el submodulo de Habilitaciones Urbanas
 *
 * @author Francisco Isasi Chiesa <franco16fra@gmail.com>
 * @package Catastro\Modules\Mantenedor\Components
 */
class QHaburbanas {

    /**
     * Funcion que obtiene las habilitaciones urbanas
     * 
     * @return Array
     */
    public static function get_HabilitacionesUrbanas() {
        $sql     = "SELECT 
							isnull(dbo.fn_MultiplicaSaldo('01', TIP_HAB_URB, 0), '') AS type , 
							COD_HAB_URB code, 
							NOM_HAB_URB name, 
							ZN_SC_ETAPA zoni 
						FROM HAB_URBA 
						ORDER BY 1";
        $command = Yii::app()->db->createCommand($sql);
        $table   = $command->queryAll();
        return $table;
    }

    /**
     * Funcion que obtiene el tipo de habilitaciones urbanas
     * 
     * @return Array
     */
    public static function get_TipoHabilitacionesUrbanas() {
        $command = Yii::app()->db->createCommand("EXEC SP_TIPOS :type, :sw");

        $command->bindValue(":type", '01', PDO::PARAM_STR);
        $command->bindValue(":sw", 1, PDO::PARAM_INT);

        $table = $command->queryAll();

        return $table;
    }

    /**
     * Funcion que registra o actualiza una habilitacion urbana mediante la data enviada
     * 
     * @param Array $data
     * @return boolean
     */
    public static function manage_HabilitacionesUrbanas($data = []) {
        $command = Yii::app()->db->createCommand("EXEC TABLA_HABURB :type, :code, :name, :zoni");

        $command->bindValue(":type", $data->type, PDO::PARAM_INT);
        $command->bindValue(":code", $data->code, PDO::PARAM_STR);
        $command->bindValue(":name", mb_strtoupper($data->name, 'UTF-8'), PDO::PARAM_STR);
        $command->bindValue(":zoni", mb_strtoupper($data->zoni, 'UTF-8'), PDO::PARAM_STR);

        $confirm = $command->execute();

        return $confirm;
    }

    /**
     * Funcion que elimina una habilitacion urbana por su codigo
     * 
     * @param String $code Codigo de la habilitacion urbana a ser eliminada
     * @return boolean
     */
    public static function delete_HabilitacionesUrbanas($code) {
        $command = Yii::app()->db->createCommand("EXEC DELETE_HABURB :code");

        $command->bindValue(":code", $code, PDO::PARAM_STR);

        $confirm = $command->execute();

        return $confirm;
    }

    /**
     * Funcion para obtener las vias que pertenecen a una habilitaciones urbana
     * 
     * @param String $code Codigo de la habilitacion urbana
     * @param int $pertenece Indica si pertenece o no a una habilitacion urbana
     * @return Array
     */
    public static function get_HabilitacionesUrbanas_Vias($code, $pertenece) {
        $command = Yii::app()->db->createCommand("EXEC sp_ShowHabUrbaVia :code, :pertenece");

        $command->bindValue(":code", $code, PDO::PARAM_STR);
        $command->bindValue(":pertenece", $pertenece, PDO::PARAM_INT);

        $table = $command->queryAll();

        return $table;
    }

    /**
     * Funcion que asigna y desasigna las vias de una habilitacion urbana.
     * 
     * @param String $cadIdHUVias Cadena de ID de las vias asignadas
     * @param String $codHUs Codigo de la habilitacion urbana
     * @param String $cadCodVias Cadena de ID de las vias no asignadas
     * @return boolean
     */
    public static function assign_HabilitacionesUrbanas_Vias($cadIdHUVias, $codHUs, $cadCodVias) {
        $command = Yii::app()->db->createCommand("EXEC SP_MantenimientoHabUrbaVia :cadIdHUVia, :codHU, :cadCodVia");

        $command->bindValue(":cadIdHUVia", $cadIdHUVias, PDO::PARAM_STR);
        $command->bindValue(":codHU", $codHUs, PDO::PARAM_STR);
        $command->bindValue(":cadCodVia", $cadCodVias, PDO::PARAM_STR);

        $table = $command->execute();


        return $table;
    }

}
