<?php

/**
 * Clase que se encarga de las funciones que se conectarán a la Base de Datos para el submodulo de Obras Complementarias
 *
 * @author Francisco Isasi Chiesa <franco16fra@gmail.com>
 * @package Catastro\Modules\Mantenedor\Components
 */
class QObrasComplementarias {

    /**
     * Funcion que obtiene las obras complementarias
     * 
     * @return Array
     */
    public static function get_ObrasComplementarias() {
        $sql     = "select COD_INSTALACION as code ,UNIDAD_MEDIDA as unit ,MATER_PREDOM as material, DESCRIPCION as description, CAL_VALOR as calculo, LARGO as largo, ANCHO as ancho, ALTO as alto from OTRAS_INSTALACIONES order by COD_INSTALACION";
        $command = Yii::app()->db->createCommand($sql);
        $model   = $command->queryAll();
        return $model;
    }

    /**
     * Funcion que registra o actualiza una obra complementaria mediante la data enviada
     * 
     * @param Array $data
     * @return boolean
     */
    public static function manage_ObrasComplementarias($data = []) {

        $command = Yii::app()->db->createCommand("EXEC TABLA_INSTALACIONA :code, :unidad, :material, :description, :calculo, :largo, :ancho, :alto");

        $command->bindValue(":code", $data->code, PDO::PARAM_STR);
        $command->bindValue(":unidad", mb_strtoupper($data->unit, 'UTF-8'), PDO::PARAM_STR);
        $command->bindValue(":material", mb_strtoupper($data->material, 'UTF-8'), PDO::PARAM_STR);
        $command->bindValue(":description", mb_strtoupper($data->description, 'UTF-8'), PDO::PARAM_STR);
        $command->bindValue(":calculo", (isset($data->calculo) ? 1 : 0), PDO::PARAM_INT);
        $command->bindValue(":largo", (isset($data->largo) ? 1 : 0), PDO::PARAM_INT);
        $command->bindValue(":ancho", (isset($data->ancho) ? 1 : 0), PDO::PARAM_INT);
        $command->bindValue(":alto", (isset($data->alto) ? 1 : 0), PDO::PARAM_INT);

        $confirm = $command->execute();

        return $confirm;
    }

    /**
     * Funcion que elimina una obra complementaria por su codigo
     * 
     * @param String $code Codigo de la obra complementaria a ser eliminada
     * @return boolean
     */
    public static function delete_ObrasComplementarias($code) {
        $command = Yii::app()->db->createCommand("DELETE FROM OTRAS_INSTALACIONES where COD_INSTALACION= :code");

        $command->bindValue(":code", $code, PDO::PARAM_STR);

        $confirm = $command->execute();

        return $confirm;
    }

}
