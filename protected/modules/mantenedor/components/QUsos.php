<?php

/**
 * Clase que se encarga de las funciones que se conectarán a la Base de Datos
 *
 * @author Francisco Isasi Chiesa <franco16fra@gmail.com>
 * @package Catastro\Modules\Mantenedor\Components
 */
class QUsos {

    /**
     * Funcion que obtiene los usos
     * 
     * @return Array
     */
    public static function get_Usos() {
        $sql = "SELECT 
							ISNULL(dbo.fn_MultiplicaSaldo('38', TIPO_ficha, 0), '') type, 
							cod_uso code, 
							desc_uso name
						FROM USO";

        $command = Yii::app()->db->createCommand($sql);

        $table = $command->queryAll();

        return $table;
    }

    /**
     * Funcion que obtiene los tipos de ficha
     * 
     * @return Array
     */
    public static function get_TipoFichas() {
        $command = Yii::app()->db->createCommand("EXEC SP_TIPOS :type, :sw");

        $command->bindValue(":type", '38', PDO::PARAM_STR);
        $command->bindValue(":sw", 0, PDO::PARAM_INT);

        $table = $command->queryAll();

        return $table;
    }

    /**
     * Funcion que registra o actualiza una habilitacion urbana mediante la data enviada
     * 
     * @param Array $data
     * @return boolean
     */
    public static function manage_Usos($data = []) {
        $command = Yii::app()->db->createCommand("EXEC TABLA_USO :type, :code, :name");

        $command->bindValue(":type", $data->type, PDO::PARAM_INT);
        $command->bindValue(":code", $data->code, PDO::PARAM_STR);
        $command->bindValue(":name", mb_strtoupper($data->name, 'UTF-8'), PDO::PARAM_STR);

        $confirm = $command->execute();

        return $confirm;
    }

    /**
     * Funcion que elimina un uso por su codigo
     * 
     * @param String $code Codigo del uso a ser eliminado
     * @return boolean
     */
    public static function delete_Usos($code) {
        $command = Yii::app()->db->createCommand("EXEC DELETE_USO :code");

        $command->bindValue(":code", $code, PDO::PARAM_STR);

        $confirm = $command->execute();

        return $confirm;
    }

    /**
     * Funcion para obtener las actividades economicas que pertenecen a un uso
     * 
     * @param String $code Codigo del uso
     * @param int $pertenece Indica si pertenece o no a un uso
     * @return Array
     */
    public static function get_Usos_ActEconomicas($code, $pertenece) {
        $command = Yii::app()->db->createCommand("EXEC sp_ShowUsoActividad :code, :pertenece");

        $command->bindValue(":code", $code, PDO::PARAM_STR);
        $command->bindValue(":pertenece", $pertenece, PDO::PARAM_INT);

        $table = $command->queryAll();

        if (!$pertenece) {
            foreach ($table as $row => &$cell) {
                $cell['Cod_Actividad'] = (int) trim($cell['Cod_Actividad']);
            }
        } else {
            foreach ($table as $row => &$cell) {
                $cell['CodActividad']   = (int) trim($cell['CodActividad']);
                $cell['IdUsoActividad'] = (int) trim($cell['IdUsoActividad']);
            }
        }

        return $table;
    }

    /**
     * Funcion que asigna y desasigna las actividades economicas de un uso.
     * 
     * @param String $strUsosAEco Cadena de ID de las actividades economicas asignadas
     * @param String $codeUso Codigo del uso
     * @param String $strAEco Cadena de ID de las actividades economicas no asignadas
     * @return boolean
     */
    public static function assign_Usos_ActEconomicas($strUsosAEco, $codeUso, $strAEco) {
        $command = Yii::app()->db->createCommand("EXEC SP_MantenimientoUsoActividad :CadIdUsoActividad, :CodUso, :CadActividad");

        $command->bindValue(":CadIdUsoActividad", $strUsosAEco, PDO::PARAM_STR);
        $command->bindValue(":CodUso", $codeUso, PDO::PARAM_STR);
        $command->bindValue(":CadActividad", $strAEco, PDO::PARAM_STR);

        $table = $command->execute();


        return $table;
    }

}
