<section id="md-manage" style="display:none;">
    <form id="frm-manage" action="" method="post" autocomplete="off" spellcheck="off">
        <div class="row">
            <input type="text" id="txtid" name="id" hidden="hidden">
            <input type="text" id="txtpeople" name="idpeople" hidden="hidden">
            <input type="text" id="txtes" name="ides" hidden="hidden">
            <div class="col-xs-9 col-md-8 form-group">
                <label for="cbotype" class="control-label">Cargo</label>
                <select id="cbotype" name="type" class="form-control" required disabled>
                    <option selected disabled>Seleccione...</option>
                    <?php foreach (QTecnico::get_Cargo() as $row => $cell): ?>
                        <option data-selected="<?= $cell['DESC_TABLA_DETALLE']; ?>" value="<?= $cell['CODIGO'] ?>"><?= $cell['DESC_TABLA_DETALLE']; ?></option>
                    <?php endforeach; ?>
                </select>
            </div>
            <div class="col-xs-3 col-md-4 form-group">
                <label for="txtcode" class="control-label">N° Documento</label>
                <input type="text" id="txtcode" name="code" maxlength="8" class="form-control" required disabled>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12 form-group">
                <label for="txtname" class="control-label">Nombres</label>
                <input type="text" id="txtname" name="name" maxlength="100" class="form-control" required disabled>
            </div>
            <div class="col-xs-12 form-group">
                <label for="last_name1" class="control-label">A. Paterno</label>
                <input type="text" id="last_name1" name="last_name1" maxlength="100" class="form-control" required disabled>
            </div>
            <div class="col-xs-12 form-group">
                <label for="last_name2" class="control-label">A. Materno</label>
                <input type="text" id="last_name2" name="last_name2" maxlength="100" class="form-control" required disabled>
            </div>
        </div>
        <div class="footer-body align-right">
            <button type="button" id="btn-close" class="btn default" >
                <i class="fa fa-times" aria-hidden="true"></i>&nbsp;Cerrar
            </button>
            <button type="submit" id="btn-done" disabled class="btn purple">
                <i class="fa fa-floppy-o" aria-hidden="true"></i>&nbsp;Aceptar
            </button>
        </div>
    </form>
</section>