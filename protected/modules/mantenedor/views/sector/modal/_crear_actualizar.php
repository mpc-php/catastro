<section id="md-manage" style="display:none;">
    <form id="frm-manage" action="" method="post" autocomplete="off" spellcheck="off">
        <div class="row">
            <div class="col-xs-3 col-md-4 form-group">
                <label for="txtcode" class="control-label">Codigo</label>
                <input type="text" id="txtcode" name="code" maxlength="6" class="form-control" required disabled>
            </div>
            <div class="col-xs-3 col-md-4 form-group">
                <label for="txtcode" class="control-label">Año</label>
                <input type="text" id="txtanio" name="anio" maxlength="4" class="form-control" required disabled>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12 form-group">
                <label for="txtname" class="control-label">Nombre Sector</label>
                <input type="text" id="txtname" name="name" maxlength="300" class="form-control" required disabled>
            </div>
        </div>
        <div class="row hidden">
            <div class="col-xs-12 form-group">
                <label for="cbostatus" class="control-label">Estado Sector</label>
                <select id="cbostatus" name="status" class="form-control" required disabled>
                    <option selected disabled>Seleccione...</option>
                    <?php foreach (Utils::obtenerSelectEstados() as $row => $cell): ?>
                        <option data-selected="<?= $cell; ?>" value="<?= $row ?>"><?= $cell; ?></option>
                    <?php endforeach; ?>
                </select>
            </div>
        </div>
        <div class="row hidden" id="section_botones">
            <div class="col-md-offset-3 col-md-9">
                <button id="btn_zonificacion" type="button" data-accion="cargarZonificacion" data-id="" class="btn btn-md btn-success accionSector"><i class="fa fa-map"></i><br/>Zonificación</button>
                <button id="btn_haburbana" type="button" data-accion="cargarHabUrbanas" data-id="" class="btn btn-md btn-warning accionSector"><i class="fa fa-home"></i><br/>Hab. Urbanas</button>
            </div>
        </div>
        <div class="footer-body align-right">
            <button type="button" id="btn-close" class="btn default" >
                <i class="fa fa-times" aria-hidden="true"></i>&nbsp;Cerrar
            </button>
            <button type="submit" id="btn-done" disabled class="btn purple">
                <i class="fa fa-floppy-o" aria-hidden="true"></i>&nbsp;Aceptar
            </button>
        </div>
    </form>
</section>