<section id="md-delete" style="display:none;">
    <div class="row">
        <div class="col-xs-12">
            <p class="text">Se procederá a eliminar la fila con los siguientes valores:</p>
            <table class="table">
                <tbody>
                    <tr><td style="width:150px;"><b>Código</b></td><td class="code"></td></tr>
                    <tr><td style="width:150px;"><b>Año</b></td><td class="anio"></td></tr>
                    <tr><td style="width:150px;"><b>Nombre</b></td><td class="name"></td></tr>
                    <tr><td style="width:150px;"><b>Estado</b></td><td class="status"></td></tr>
                </tbody>
            </table>
        </div>
    </div>
    <div class="footer-body align-right">
        <button type="button" id="btn-close" class="btn default" >
            <i class="fa fa-times" aria-hidden="true"></i>&nbsp;Cerrar
        </button>
        <button type="button" id="btn-done" class="btn btn-success">
            <i class="fa fa-check" aria-hidden="true"></i>&nbsp;Aceptar
        </button>
    </div>
</section>