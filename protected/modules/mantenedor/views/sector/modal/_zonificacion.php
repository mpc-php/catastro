<section id="md-zonificacion" style="display:none;">
    <div class="row">
        <div class="col-xs-12">
            <h2 id="name" class="md-title"></h2>
            <h5 id="code" class="md-subtitle"></h5>
        </div>
    </div>
    <div class="row">
        <!--DIVISION PARA LOS ITEM DE NO PERTENECEN-->
        <div class="col-sm-12 col-md-6">
            <!--TABLA QUE ACTUARA DE CABECERA EN MODO POSITION:FIXED-->
            <table class="table-head">
                <caption>
                    <p class="align-left">
                        NO PERTENECEN
                        <button type="button" id="btn-add" disabled class="btn float-right bg-green">
                            <i class="fa fa-angle-right fa-2x" aria-hidden="true"></i>
                        </button>
                    </p>
                    <!--INPUT QUE PERMITIRÁ LA BUSQUEDA-->
                    <input type="search" name="noper-search" class="form-control buscador_modal" data-container="no-pertenece" placeholder="Buscar...">
                </caption>
                <thead class="bg-green">
                    <tr>
                        <th>
                            <!--CHECKBOX DE MARCAR|DESMARCAR A TODOS LOS ITEMS-->
                            <input type="checkbox" name="noper" id="checkAll-noper">
                        </th>
                        <th>Código</th>
                        <th>Zona</th>
                    </tr>
                </thead>
            </table>
            <!--DIVISOR QUE CONTIENE TODOS LOS ITEMS :importante que el tbody este definido-->
            <div class="container-table">
                <table id="no-pertenece" class="table-selected">
                    <tbody>
                    <td colspan="3"><label for="label-control">Cargando datos...</label></td>
                    </tbody>
                </table>
            </div>
        </div>
        <!--DIVISION PARA LOS ITEMS DE PERTENECEN-->
        <div class="col-sm-12 col-md-6">
            <!--TABLA QUE ACTUARA DE CABECERA EN MODO POSITION:FIXED-->
            <table class="table-head">
                <caption>
                    <p class="align-right">
                        PERTENECEN
                        <button type="button" id="btn-remove" disabled class="btn float-left">
                            <i class="fa fa-angle-left fa-2x" aria-hidden="true"></i>
                        </button>
                    </p>
                    <!--INPUT QUE PERMITIRÁ LA BUSQUEDA-->
                    <input type="search" name="noper-search" class="form-control buscador_modal" data-container="pertenece" placeholder="Buscar...">
                </caption>
                <thead class="green">
                    <tr>
                        <th>
                            <!--CHECKBOX DE MARCAR|DESMARCAR A TODOS LOS ITEMS-->
                            <input type="checkbox" name="perte" id="checkAll-perte">
                        </th>
                        <th>Código</th>
                        <th>Zona</th>
                    </tr>
                </thead>
            </table>
            <!--DIVISOR QUE CONTIENE TODOS LOS ITEMS :importante que el tbody este definido-->
            <div class="container-table">
                <table id="pertenece" class="table-selected">
                    <tbody>
                    <td colspan="3"><label for="label-control">Cargando datos...</label></td>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="footer-body align-right">
        <button type="button" id="btn-close" class="btn default" >
            <i class="fa fa-times" aria-hidden="true"></i>&nbsp;Cerrar
        </button>
    </div>
</section>