<section id="md-manage" style="display:none;">
    <form id="frm-manage" action="" method="post" autocomplete="off" spellcheck="off">
        <div class="row">
            <div class="col-xs-6 col-md-6 form-group">
                <label for="txtcode" class="control-label">Codigo</label>
                <input type="text" id="txtcode" name="code" maxlength="3" class="form-control" required disabled>
            </div>
            <div class="col-xs-6 col-md-6 form-group">
                <label for="txtcode" class="control-label">Descripcion</label>
                <input type="text" id="txtname" name="description" maxlength="100" class="form-control" required disabled>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-6 col-md-6 form-group">
                <label for="txtcode" class="control-label">U. de Medida</label>
                <input type="text" id="txtunit" name="unit" maxlength="12" class="form-control" required disabled>
            </div>
            <div class="col-xs-6 col-md-6 form-group">
                <label for="txtcode" class="control-label">Material de Medida</label>
                <input type="text" id="txtmaterial" name="material" maxlength="50" class="form-control" required disabled>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-3 col-md-3 form-group">
                <label for="txtCalculo" class="control-label">Calculo</label>
                <input type="checkbox" id="chkcalculo" name="calculo" class="form-control" disabled>
            </div>
            <div class="col-xs-3 col-md-3 form-group">
                <label for="txtLargo" class="control-label">Largo</label>
                <input type="checkbox" id="chklargo" name="largo" class="form-control" disabled>
            </div>
            <div class="col-xs-3 col-md-3 form-group">
                <label for="txtCalculo" class="control-label">Ancho</label>
                <input type="checkbox" id="chkancho" name="ancho" class="form-control" disabled>
            </div>
            <div class="col-xs-3 col-md-3 form-group">
                <label for="txtCalculo" class="control-label">Alto</label>
                <input type="checkbox" id="chkalto" name="alto" class="form-control" disabled>
            </div>
        </div>
        <div class="footer-body align-right">
            <button type="button" id="btn-close" class="btn default" >
                <i class="fa fa-times" aria-hidden="true"></i>&nbsp;Cerrar
            </button>
            <button type="submit" id="btn-done" disabled class="btn purple">
                <i class="fa fa-floppy-o" aria-hidden="true"></i>&nbsp;Aceptar
            </button>
        </div>
    </form>
</section>