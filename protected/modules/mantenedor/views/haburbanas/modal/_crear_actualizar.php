<section id="md-manage" style="display:none;">
    <form id="frm-manage" action="" method="post" autocomplete="off" spellcheck="off">
        <div class="row">
            <div class="col-xs-9 col-md-8 form-group">
                <label for="cbotype" class="control-label">Tipo habilitacion Urbana</label>
                <select id="cbotype" name="type" class="form-control" required disabled>
                    <option selected disabled>Seleccione...</option>
                    <?php foreach ($tipos as $row => $cell): ?>
                        <option data-selected="<?= $cell['nombre']; ?>" value="<?= $cell['CODIGO'] ?>"><?= $cell['nombre']; ?></option>
                    <?php endforeach; ?>
                </select>
            </div>
            <div class="col-xs-3 col-md-4 form-group">
                <label for="txtcode" class="control-label">Codigo</label>
                <input type="text" id="txtcode" name="code" maxlength="5" class="form-control" required disabled>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12 form-group">
                <label for="txtname" class="control-label">Nombre habilitación urbana</label>
                <input type="text" id="txtname" name="name" maxlength="100" class="form-control" required disabled>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12 form-group">
                <label for="txtzoni" class="control-label">Zona, sector, etapa</label>
                <input type="text" id="txtzoni" name="zoni" maxlength="50" class="form-control" disabled>
            </div>
        </div>
        <div class="footer-body align-right">
            <button type="button" id="btn-close" class="btn default" >
                <i class="fa fa-times" aria-hidden="true"></i>&nbsp;Cerrar
            </button>
            <button type="submit" id="btn-done" disabled class="btn purple">
                <i class="fa fa-floppy-o" aria-hidden="true"></i>&nbsp;Aceptar
            </button>
        </div>
    </form>
</section>