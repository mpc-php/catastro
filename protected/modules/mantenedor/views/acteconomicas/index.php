<div class="page-head">
    <div class="container">
        <div class="page-title"><h1>Actividades Economicas.</h1></div>
    </div>
</div>

<div class="page-content">
    <div class="container">

        <!-- BEGIN PAGE CONTENT INNER -->
        <div class="page-content-inner">
            <div class="search-page search-content-4">

                <div class="search-bar bordered">
                    <div class="row">
                        <div class="col-lg-8">
                            <div class="input-group">
                                <input id="buscador_generico" type="text" class="form-control" placeholder="Buscar código ó descripción" autofocus>
                                <span class="input-group-btn">
                                    <button id="buscador_generico_btn" class="btn" type="button">
                                        <i class="fa fa-search" aria-hidden="true"></i>
                                    </button>
                                </span>
                            </div>
                        </div>
                        <div class="col-lg-4 extra-buttons">
                            <button id="btn-manage" class="btn green-soft uppercase bold" type="button">Nuevo</button>
                        </div>
                    </div>
                </div>

                <div class="search-table table-responsive">
                    <table id="tdacteconomicas" class="table table-bordered table-striped table-condensed table-custom">
                        <thead class="bg-blue"></thead>
                    </table>
                </div>

            </div>
        </div>
        <!-- END PAGE CONTENT INNER -->
    </div>
</div>

<?php $this->renderPartial('modal/_crear_actualizar'); ?>
<?php $this->renderPartial('modal/_eliminar'); ?>