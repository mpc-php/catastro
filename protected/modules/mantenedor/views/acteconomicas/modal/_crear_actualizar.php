<section id="md-manage" style="display:none;">
    <form id="frm-manage" action="" method="post" autocomplete="off" spellcheck="off">
        <div class="row">
            <div class="col-xs-12 col-md-4 form-group">
                <label for="txtcode" maxlength="50" class="control-label">Codigo</label>
                <input type="text" id="txtcode" name="code" class="form-control" required disabled>
            </div>
            <div class="col-xs-12 col-md-8 form-group">
                <label for="txtname" class="control-label">Nombre</label>
                <input type="text" id="txtname" name="name" maxlength="100" class="form-control" disabled>
            </div>
        </div>
        <div class="footer-body align-right">
            <button type="button" id="btn-close" class="btn default" >
                <i class="fa fa-times" aria-hidden="true"></i>&nbsp;Cerrar
            </button>
            <button type="submit" id="btn-done" disabled class="btn purple">
                <i class="fa fa-floppy-o" aria-hidden="true"></i>&nbsp;Aceptar
            </button>
        </div>
    </form>
</section>