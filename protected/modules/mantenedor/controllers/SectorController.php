

<?php

/**
 * Clase que se encarga de controlar las acciones a ser utilizadas para  para el submodulo de Sector
 *
 * @author Francisco Isasi Chiesa <franco16fra@gmail.com>
 * @package Catastro\Modules\Mantenedor\Controllers
 */
class SectorController extends Auth {

    /**
     * Accion que inicia el Index del Modulo
     */
    public function actionIndex() {
        $this->render('index');
    }

    /**
     * Accion que muestra la data de los sectores
     * 
     * @throws Exception
     */
    public function actionShow() {
        try {
            if (!Yii::app()->request->isAjaxRequest)
                throw new Exception("El metodo no esta permitido", 403);

            $data['data'] = QSector::get_Sectores();

            JSON::response(FALSE, 200, "", $data);
        } catch (Exception $ex) {
            JSON::response(TRUE, $ex->getCode(), $ex->getMessage(), []);
        }
    }

    /**
     * Funcion que registra o actualiza un sector mediante la data enviada
     * 
     * @return boolean
     */
    public function actionManage() {
        $allowKeys = ['code', 'name', 'anio', 'status'];
        $postKeys = array_keys($_POST);

        try {

            if (!Yii::app()->request->isAjaxRequest)
                throw new Exception("El metodo no esta permitido", 403);

            $diff = array_diff($postKeys, $allowKeys);
            if (!empty($diff))
                throw new Exception("Faltan parametros para continuar con la acción", 400);

            $post = (object) $_POST;

            $confirm = (boolean) QSector::manage_Sector($post);

            $message = '';

            if ($confirm)
                $message = 'LA ACCIÓN SE REALIZO CON EXITO.';
            else
                $message = 'ERROR AL MOMENTO DE EJECUTAR LA ACCIÓN, VUELVA A INTENTARLO.';

            JSON::response((!$confirm), 200, $message, ['data' => $confirm]);
        } catch (Exception $ex) {
            JSON::response(TRUE, $ex->getCode(), $ex->getMessage(), []);
        }
    }

    /**
     * Accion para eliminar un sector.
     * 
     * @throws Exception
     */
    public function actionDelete() {
        try {

            if (!Yii::app()->request->isAjaxRequest)
                throw new Exception("El metodo no esta permitido", 403);

            if (!isset($_POST['code']))
                throw new Exception("Faltan parametros para continuar con la acción", 400);

            $confirm = (boolean) QSector::delete_Sector($_POST['code'],$_POST["anio"]);

            $message = '';

            if ($confirm)
                $message = 'LA ELIMINACIÓN SE REALIZO CON EXITO.';
            else
                $message = 'ERROR AL MOMENTO DE EJECUTAR LA ELIMINACIÓN, VUELVA A INTENTARLO.';

            JSON::response((!$confirm), 200, $message, ['data' => $confirm]);
        } catch (Exception $ex) {
            JSON::response(TRUE, $ex->getCode(), $ex->getMessage(), []);
        }
    }

    /**
     * Funcion para cargar las zonificaciones de un sector
     * 
     * @param String $q Codigo de Sector
     * @throws Exception
     */
    public function actionCargarZonificacion($q) {
        try {
            if (!Yii::app()->request->isAjaxRequest)
                throw new Exception("El metodo no esta permitido", 403);

            $data['pertenece'] = QSector::obtenerZonificacion($q, true);
            $data['no_pertenece'] = QSector::obtenerZonificacion($q, false);

            JSON::response(FALSE, 200, "", $data);
        } catch (Exception $ex) {
            JSON::response(TRUE, $ex->getCode(), $ex->getMessage(), []);
        }
    }

    /**
     * Funcion para cargar las habilitaciones urbanas de un sector
     * 
     * @param String $q Codigo de Sector
     * @throws Exception
     */
    public function actionCargarHabUrbana($q) {
        try {
            if (!Yii::app()->request->isAjaxRequest)
                throw new Exception("El metodo no esta permitido", 403);

            $data['pertenece'] = QSector::obtenerHabUrbana($q, true);
            $data['no_pertenece'] = QSector::obtenerHabUrbana($q, false);

            JSON::response(FALSE, 200, "", $data);
        } catch (Exception $ex) {
            JSON::response(TRUE, $ex->getCode(), $ex->getMessage(), []);
        }
    }

    /**
     * Funcion para cargar las habilitaciones urbanas de un sector
     * 
     * @param String $code Codigo de Sector
     * @throws Exception
     */
    public function actionHaburbanas($code) {
        try {

            if (!Yii::app()->request->isAjaxRequest)
                throw new Exception("El metodo no esta permitido", 403);

            $data['pertenece'] = QSector::obtenerHabUrbana($code, true);
            $data['no_pertenece'] = QSector::obtenerHabUrbana($code, false);

            JSON::response(FALSE, 200, "OK", $data);
        } catch (Exception $ex) {
            JSON::response(TRUE, $ex->getCode(), $ex->getMessage(), []);
        }
    }

    /**
     * Funcion para asignar las habilitaciones urbanas a un sector
     * 
     * @throws Exception
     */
    public function actionAsignarHabUrbanas() {
        try {

            if (!Yii::app()->request->isAjaxRequest)
                throw new Exception("El metodo no esta permitido", 403);

            $arr_actividades = $_POST['codeacti'];
            $codeUso = $_POST['codeuso'];

            $strUsosAEco = '';
            $strAEco = '';

            foreach ($arr_actividades as $index => $val) {
                if (strlen($strAEco) == 0) {
                    $strUsosAEco = '0';
                    $strAEco = $val;
                } else {
                    $strUsosAEco .= '@0';
                    $strAEco .= ('@' . $val);
                }
            }

            $acction = QSector::assign_Sector_HabUrbana($strUsosAEco, $codeUso, $strAEco);

            $data['pertenece'] = QSector::obtenerHabUrbana($codeUso, true);
            $data['no_pertenece'] = QSector::obtenerHabUrbana($codeUso, false);

            JSON::response(FALSE, 200, "OK", $data);
        } catch (Exception $ex) {
            JSON::response(TRUE, $ex->getCode(), $ex->getMessage(), []);
        }
    }

    /**
     * Funcion para desasignar las habilitaciones urbanas a un sector
     * 
     * @throws Exception
     */
    public function actionRemoveHabUrbanas() {
        try {

            if (!Yii::app()->request->isAjaxRequest)
                throw new Exception("El metodo no esta permitido", 403);

            $arr_actividades = $_POST['codeacti'];
            $codeUso = $_POST['codeuso'];

            $strUsosAEco = '';
            $strAEco = '';

            foreach ($arr_actividades as $index => $val) {
                if (strlen($strAEco) == 0) {
                    $strUsosAEco = $val['UsoAEcode'];
                    $strAEco = $val['AEcode'];
                } else {
                    $strUsosAEco .= '@' . $val['UsoAEcode'];
                    $strAEco .= ('@' . $val['AEcode']);
                }
            }

            $acction = QSector::assign_Sector_HabUrbana($strUsosAEco, $codeUso, $strAEco);

            $data['pertenece'] = QSector::obtenerHabUrbana($codeUso, true);
            $data['no_pertenece'] = QSector::obtenerHabUrbana($codeUso, false);

            JSON::response(FALSE, 200, "OK", $data);
        } catch (Exception $ex) {
            JSON::response(TRUE, $ex->getCode(), $ex->getMessage(), []);
        }
    }

    /**
     * Funcion para cargar las zonificaciones de un sector
     * 
     * @param String $code Codigo de Sector
     * @throws Exception
     */
    public function actionZonificacion($code) {
        try {

            if (!Yii::app()->request->isAjaxRequest)
                throw new Exception("El metodo no esta permitido", 403);

            $data['pertenece'] = QSector::obtenerZonificacion($code, true);
            $data['no_pertenece'] = QSector::obtenerZonificacion($code, false);

            JSON::response(FALSE, 200, "OK", $data);
        } catch (Exception $ex) {
            JSON::response(TRUE, $ex->getCode(), $ex->getMessage(), []);
        }
    }

    /**
     * Funcion para asignar las zonificaciones a un sector
     * 
     * @throws Exception
     */
    public function actionAsignarZonificaciones() {
        try {

            if (!Yii::app()->request->isAjaxRequest)
                throw new Exception("El metodo no esta permitido", 403);

            $arr_actividades = $_POST['codeacti'];
            $codeUso = $_POST['codeuso'];

            $strUsosAEco = '';
            $strAEco = '';

            foreach ($arr_actividades as $index => $val) {
                if (strlen($strAEco) == 0) {
                    $strUsosAEco = '0';
                    $strAEco = $val;
                } else {
                    $strUsosAEco .= '@0';
                    $strAEco .= ('@' . $val);
                }
            }

            $acction = QSector::assign_Sector_Zonificacion($strUsosAEco, $codeUso, $strAEco);

            $data['pertenece'] = QSector::obtenerZonificacion($codeUso, true);
            $data['no_pertenece'] = QSector::obtenerZonificacion($codeUso, false);

            JSON::response(FALSE, 200, "OK", $data);
        } catch (Exception $ex) {
            JSON::response(TRUE, $ex->getCode(), $ex->getMessage(), []);
        }
    }

    /**
     * Funcion para desasignar las zonificaciones a un sector
     * 
     * @throws Exception
     */
    public function actionRemoveZonificaciones() {
        try {

            if (!Yii::app()->request->isAjaxRequest)
                throw new Exception("El metodo no esta permitido", 403);

            $arr_actividades = $_POST['codeacti'];
            $codeUso = $_POST['codeuso'];

            $strUsosAEco = '';
            $strAEco = '';

            foreach ($arr_actividades as $index => $val) {
                if (strlen($strAEco) == 0) {
                    $strUsosAEco = $val['UsoAEcode'];
                    $strAEco = $val['AEcode'];
                } else {
                    $strUsosAEco .= '@' . $val['UsoAEcode'];
                    $strAEco .= ('@' . $val['AEcode']);
                }
            }

            $acction = QSector::assign_Sector_Zonificacion($strUsosAEco, $codeUso, $strAEco);

            $data['pertenece'] = QSector::obtenerZonificacion($codeUso, true);
            $data['no_pertenece'] = QSector::obtenerZonificacion($codeUso, false);

            JSON::response(FALSE, 200, "OK", $data);
        } catch (Exception $ex) {
            JSON::response(TRUE, $ex->getCode(), $ex->getMessage(), []);
        }
    }

}
