<?php

/**
 * Clase que se encarga de controlar las acciones a ser utilizadas para  para el submodulo de Usos
 *
 * @author Francisco Isasi Chiesa <franco16fra@gmail.com>
 * @package Catastro\Modules\Mantenedor\Controllers
 */
class UsosController extends Auth {

    /**
     * Accion que muestra el index
     */
    public function actionIndex() {
        $data['tipos'] = QUsos::get_TipoFichas();
        $this->render('index', $data);
    }

    /**
     * Accion que muestra la data de los usos en el listado
     * 
     * @throws Exception
     */
    public function actionShow() {
        try {

            if (!Yii::app()->request->isAjaxRequest)
                throw new Exception("El metodo no esta permitido", 403);

            $data['data'] = QUsos::get_Usos();

            JSON::response(FALSE, 200, "", $data);
        } catch (Exception $ex) {
            JSON::response(TRUE, $ex->getCode(), $ex->getMessage(), []);
        }
    }

    /**
     * Accion que registra y/o modifica los usos
     * 
     * @throws Exception
     */
    public function actionManage() {
        $allowKeys = ['type', 'code', 'name'];
        $postKeys  = array_keys($_POST);

        try {

            if (!Yii::app()->request->isAjaxRequest)
                throw new Exception("El metodo no esta permitido", 403);

            $diff = array_diff($postKeys, $allowKeys);
            if (!empty($diff))
                throw new Exception("Faltan parametros para continuar con la acción", 400);

            $post = (object) $_POST;

            $confirm = (boolean) QUsos::manage_Usos($post);

            $message = '';

            if ($confirm)
                $message = 'LA ACCIÓN SE REALIZO CON EXITO.';
            else
                $message = 'ERROR AL MOMENTO DE EJECUTAR LA ACCIÓN, VUELVA A INTENTARLO.';

            JSON::response((!$confirm), 200, $message, ['data' => $confirm]);
        } catch (Exception $ex) {
            JSON::response(TRUE, $ex->getCode(), $ex->getMessage(), []);
        }
    }

    /**
     * Accion que elimina el uso
     * 
     * @throws Exception
     */
    public function actionDelete() {
        try {

            if (!Yii::app()->request->isAjaxRequest)
                throw new Exception("El metodo no esta permitido", 403);

            if (!isset($_POST['code']))
                throw new Exception("Faltan parametros para continuar con la acción", 400);

            $confirm = (boolean) QUsos::delete_Usos($_POST['code']);

            $message = '';

            if ($confirm)
                $message = 'LA ELIMINACIÓN SE REALIZO CON EXITO.';
            else
                $message = 'ERROR AL MOMENTO DE EJECUTAR LA ELIMINACIÓN, VUELVA A INTENTARLO.';

            JSON::response((!$confirm), 200, $message, ['data' => $confirm]);
        } catch (Exception $ex) {
            JSON::response(TRUE, $ex->getCode(), $ex->getMessage(), []);
        }
    }

    /**
     * Accion que carga las actividades economicas por uso
     * 
     * @param String $code Codigo de Uso
     * @throws Exception
     */
    public function actionActividades($code) {
        try {

            if (!Yii::app()->request->isAjaxRequest)
                throw new Exception("El metodo no esta permitido", 403);

            $data['pertenece']    = QUsos::get_Usos_ActEconomicas($code, 1);
            $data['no_pertenece'] = QUsos::get_Usos_ActEconomicas($code, 0);

            JSON::response(FALSE, 200, "OK", $data);
        } catch (Exception $ex) {
            JSON::response(TRUE, $ex->getCode(), $ex->getMessage(), []);
        }
    }

    /**
     * Accion que asigna las actividades economicas por uso
     * 
     * @throws Exception
     */
    public function actionAsignaractividades() {
        try {

            if (!Yii::app()->request->isAjaxRequest)
                throw new Exception("El metodo no esta permitido", 403);

            $arr_actividades = $_POST['codeacti'];
            $codeUso         = $_POST['codeuso'];

            $strUsosAEco = '';
            $strAEco     = '';

            foreach ($arr_actividades as $index => $val) {
                if (strlen($strAEco) == 0) {
                    $strUsosAEco = '0';
                    $strAEco     = $val;
                } else {
                    $strUsosAEco .= '@0';
                    $strAEco .= ('@' . $val);
                }
            }

            $acction = QUsos::assign_Usos_ActEconomicas($strUsosAEco, $codeUso, $strAEco);

            $data['pertenece']    = QUsos::get_Usos_ActEconomicas($codeUso, 1);
            $data['no_pertenece'] = QUsos::get_Usos_ActEconomicas($codeUso, 0);

            JSON::response(FALSE, 200, "OK", $data);
        } catch (Exception $ex) {
            JSON::response(TRUE, $ex->getCode(), $ex->getMessage(), []);
        }
    }

    /**
     * Accion que desasigna las actividades economicas por uso
     * 
     * @throws Exception
     */
    public function actionRemoveactividades() {
        try {

            if (!Yii::app()->request->isAjaxRequest)
                throw new Exception("El metodo no esta permitido", 403);

            $arr_actividades = $_POST['codeacti'];
            $codeUso         = $_POST['codeuso'];

            $strUsosAEco = '';
            $strAEco     = '';

            foreach ($arr_actividades as $index => $val) {
                if (strlen($strAEco) == 0) {
                    $strUsosAEco = $val['UsoAEcode'];
                    $strAEco     = $val['AEcode'];
                } else {
                    $strUsosAEco .= '@' . $val['UsoAEcode'];
                    $strAEco .= ('@' . $val['AEcode']);
                }
            }

            $acction = QUsos::assign_Usos_ActEconomicas($strUsosAEco, $codeUso, $strAEco);

            $data['pertenece']    = QUsos::get_Usos_ActEconomicas($codeUso, 1);
            $data['no_pertenece'] = QUsos::get_Usos_ActEconomicas($codeUso, 0);

            JSON::response(FALSE, 200, "OK", $data);
        } catch (Exception $ex) {
            JSON::response(TRUE, $ex->getCode(), $ex->getMessage(), []);
        }
    }

}
