<?php

/**
 * Clase que se encarga de controlar las acciones a ser utilizadas para  para el submodulo de Anuncios
 *
 * @author Francisco Isasi Chiesa <franco16fra@gmail.com>
 * @package Catastro\Modules\Mantenedor\Controllers
 */
class AnunciosController extends Auth {

    /**
     * Accion que muestra el index
     */
    public function actionIndex() {
        $this->render('index');
    }

    /**
     * Accion que muestra la data de los anuncios en el listado
     * 
     * @throws Exception
     */
    public function actionShow() {
        try {
            if (!Yii::app()->request->isAjaxRequest)
                throw new Exception("El metodo no esta permitido", 403);

            $data['data'] = QAnuncios::get_Anuncios();

            JSON::response(FALSE, 200, "", $data);
        } catch (Exception $ex) {
            JSON::response(TRUE, $ex->getCode(), $ex->getMessage(), []);
        }
    }

    /**
     * Accion que registra y/o modifica los anuncios
     * 
     * @throws Exception
     */
    public function actionManage() {
        $allowKeys = ['type', 'code', 'name', 'zoni'];
        $postKeys  = array_keys($_POST);

        try {

            if (!Yii::app()->request->isAjaxRequest)
                throw new Exception("El metodo no esta permitido", 403);

            $diff = array_diff($postKeys, $allowKeys);
            if (!empty($diff))
                throw new Exception("Faltan parametros para continuar con la acción", 400);

            $post = (object) $_POST;

            $confirm = (boolean) QAnuncios::manage_Anuncios($post);

            $message = '';

            if ($confirm)
                $message = 'LA ACCIÓN SE REALIZO CON EXITO.';
            else
                $message = 'ERROR AL MOMENTO DE EJECUTAR LA ACCIÓN, VUELVA A INTENTARLO.';

            JSON::response((!$confirm), 200, $message, ['data' => $confirm]);
        } catch (Exception $ex) {
            JSON::response(TRUE, $ex->getCode(), $ex->getMessage(), []);
        }
    }

    /**
     * Accion que elimina los anuncios
     * 
     * @throws Exception
     */
    public function actionDelete() {
        try {

            if (!Yii::app()->request->isAjaxRequest)
                throw new Exception("El metodo no esta permitido", 403);

            if (!isset($_POST['code']))
                throw new Exception("Faltan parametros para continuar con la acción", 400);

            $confirm = (boolean) QAnuncios::delete_Anuncios($_POST['code']);

            $message = '';

            if ($confirm) {
                $message = 'LA ELIMINACIÓN SE REALIZO CON EXITO.';
            } else {
                $message = 'ERROR AL MOMENTO DE EJECUTAR LA ELIMINACIÓN, VUELVA A INTENTARLO.';
            }
            JSON::response((!$confirm), 200, $message, ['data' => $confirm]);
        } catch (Exception $ex) {
            JSON::response(TRUE, $ex->getCode(), $ex->getMessage(), []);
        }
    }

}
