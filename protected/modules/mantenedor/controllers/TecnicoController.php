

<?php

/**
 * Clase que se encarga de controlar las acciones a ser utilizadas para el submodulo de Tecnicos
 *
 * @author Franklin Ruiz Asto Leon <franklin.asto.leon@gmail.com>
 * @package Catastro\Modules\Mantenedor\Controllers
 */
class TecnicoController extends Auth {

    /**
     * Accion que muestra el index
     */
    public function actionIndex() {
        $this->render('index');
    }

    /**
     * Accion que muestra la data de tecnicos en el listado
     * 
     * @throws Exception
     */
    public function actionShow() {
        try {
            if (!Yii::app()->request->isAjaxRequest)
                throw new Exception("El metodo no esta permitido", 403);

            $data['data'] = QTecnico::get_Tecnico();

            JSON::response(FALSE, 200, "", $data);
        } catch (Exception $ex) {
            JSON::response(TRUE, $ex->getCode(), $ex->getMessage(), []);
        }
    }

    /**
     * Accion que registra y/o modifica a los tecnicos
     * 
     * @throws Exception
     */
    public function actionManage() {
        $allowKeys = ['id','idpeople', 'type', 'code', 'name', 'last_name1', 'last_name2','ides'];
        $postKeys = array_keys($_POST);

        try {

            if (!Yii::app()->request->isAjaxRequest)
                throw new Exception("El metodo no esta permitido", 403);

            $diff = array_diff($postKeys, $allowKeys);
            if (!empty($diff))
                throw new Exception("Faltan parametros para continuar con la acción", 400);

            $post = (object) $_POST;
            //Utils::show($post,TRUE);

            $confirm = (boolean) QTecnico::manage_Tecnico($post);

            $message = '';

            if ($confirm) {
                $message = 'LA ACCIÓN SE REALIZO CON EXITO.';
            } else {
                $message = 'ERROR AL MOMENTO DE EJECUTAR LA ACCIÓN, VUELVA A INTENTARLO.';
            }

            JSON::response((!$confirm), 200, $message, ['data' => $confirm]);
        } catch (Exception $ex) {
            JSON::response(TRUE, $ex->getCode(), $ex->getMessage(), []);
        }
    }

    /**
     * Accion que elimina a un tecnico de manera logica
     * 
     * @throws Exception
     */
    public function actionDelete() {

        try {

            if (!Yii::app()->request->isAjaxRequest)
                throw new Exception("El metodo no esta permitido", 403);

            if (!isset($_POST['id']))
                throw new Exception("Faltan parametros para continuar con la acción", 400);

            $confirm = (boolean) QTecnico::delete_Tecnico($_POST['id']);

            $message = '';

            if ($confirm) {
                $message = 'LA ELIMINACIÓN SE REALIZO CON EXITO.';
            } else {
                $message = 'ERROR AL MOMENTO DE EJECUTAR LA ELIMINACIÓN, VUELVA A INTENTARLO.';
            }

            JSON::response((!$confirm), 200, $message, ['data' => $confirm]);
        } catch (Exception $ex) {
            JSON::response(TRUE, $ex->getCode(), $ex->getMessage(), []);
        }
    }

    public function actionBuscar() {
        if ($_POST) {
            $dni = $_POST["dni"];
            $model = QTecnico::buscar_Tecnico($dni);
            $model2 = QTecnico::recuperar_Tecnico($dni);
            if ($model) {
                $estado = true;
                $existe = 'A';
            } else if ($model2) {
                $estado = true;
                $existe = 'I';
            } else {
                $estado = false;
                $existe = 'NO';
            }
            echo json_encode(["estado" => $estado, "existe" => $existe]);
        } else {
            throw new CHttpException(400, "Página no encontrada");
        }
    }

    public function actionRecover() {
        if ($_POST) {
            $dni = $_POST["dni"];
            $model = QTecnico::recover_Tecnico($dni);
            if ($model) {
                $estado = true;
            } else {
                $estado = false;
            }
            echo json_encode(["estado" => $estado]);
        } else {
            throw new CHttpException(400, "Página no encontrada");
        }
    }

    public function actionBuscarPersona() {
        if ($_POST) {
            $dni = $_POST["dni"];
            $model = QTecnico::buscar_persona($dni);
            if ($model) {
                $estado = true;
                $data = $model;
            } else {
                $estado = false;
                $data = "0";
            }
            echo json_encode(["estado" => $estado, "data" => $data]);
        } else {
            throw new CHttpException(400, "Página no encontrada");
        }
    }

}
