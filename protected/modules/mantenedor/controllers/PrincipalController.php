

<?php
/**
* Descripcción para Principal
*
* @author Nombre del Programador <correodelprogramador@email.com>
* @package Catastro\Modules\Mantenedores\Controllers
*/

class PrincipalController extends Auth
{
	public function actionIndex()
	{
		$this->render('index');
	}
}