<?php

/**
 * Clase que se encarga de controlar las acciones a ser utilizadas para  para el submodulo de Habilitaciones Urbanas
 *
 * @author Francisco Isasi Chiesa <franco16fra@gmail.com>
 * @package Catastro\Modules\Mantenedor\Controllers
 */
class HaburbanasController extends Auth {

    /**
     * Accion que muestra el index
     */
    public function actionIndex() {
        $data['tipos'] = QHaburbanas::get_TipoHabilitacionesUrbanas();
        $this->render('index', $data);
    }

    /**
     * Accion que muestra la data de las habilitaciones urbanas en el listado
     * 
     * @throws Exception
     */
    public function actionShow() {
        try {
            if (!Yii::app()->request->isAjaxRequest)
                throw new Exception("El metodo no esta permitido", 403);

            $data['data'] = QHaburbanas::get_HabilitacionesUrbanas();

            JSON::response(FALSE, 200, "OK", $data);
        } catch (Exception $ex) {
            JSON::response(TRUE, $ex->getCode(), $ex->getMessage(), []);
        }
    }

    /**
     * Accion que registra y/o modifica las habilitaciones urbanas
     * 
     * @throws Exception
     */
    public function actionManage() {
        $allowKeys = ['type', 'code', 'name', 'zoni'];
        $postKeys  = array_keys($_POST);

        try {

            if (!Yii::app()->request->isAjaxRequest)
                throw new Exception("El metodo no esta permitido", 403);

            $diff = array_diff($postKeys, $allowKeys);
            if (!empty($diff))
                throw new Exception("Faltan parametros para continuar con la acción", 400);

            $post = (object) $_POST;

            $confirm = (boolean) QHaburbanas::manage_HabilitacionesUrbanas($post);

            $message = '';

            if ($confirm)
                $message = 'LA ACCIÓN SE REALIZO CON EXITO.';
            else
                $message = 'ERROR AL MOMENTO DE EJECUTAR LA ACCIÓN, VUELVA A INTENTARLO.';

            JSON::response((!$confirm), 200, $message, ['data' => $confirm]);
        } catch (Exception $ex) {
            JSON::response(TRUE, $ex->getCode(), $ex->getMessage(), []);
        }
    }

    /**
     * Accion que elimina la habilitacion urbana
     * 
     * @throws Exception
     */
    public function actionDelete() {
        try {

            if (!Yii::app()->request->isAjaxRequest)
                throw new Exception("El metodo no esta permitido", 403);

            if (!isset($_POST['code']))
                throw new Exception("Faltan parametros para continuar con la acción", 400);

            $confirm = (boolean) QHaburbanas::delete_HabilitacionesUrbanas($_POST['code']);

            $message = '';

            if ( $confirm )
                $message = 'LA ELIMINACIÓN SE REALIZO CON EXITO.';
            else
                $message = 'ERROR AL MOMENTO DE EJECUTAR LA ELIMINACIÓN, VUELVA A INTENTARLO.';

            JSON::response((!$confirm), 200, $message, ['data' => $confirm]);
        } catch (Exception $ex) {
            JSON::response(TRUE, $ex->getCode(), $ex->getMessage(), []);
        }
    }

    public function actionVias($code) {
        try {

            if (!Yii::app()->request->isAjaxRequest)
                throw new Exception("El metodo no esta permitido", 403);

            $data['pertenece']    = QHaburbanas::get_HabilitacionesUrbanas_Vias($code, 1);
            $data['no_pertenece'] = QHaburbanas::get_HabilitacionesUrbanas_Vias($code, 0);

            JSON::response(FALSE, 200, "OK", $data);

        } catch (Exception $ex) {
            JSON::response(TRUE, $ex->getCode(), $ex->getMessage(), []);
        }
    }

    public function actionAsignarvias() {
        try {

            if (!Yii::app()->request->isAjaxRequest)
                throw new Exception("El metodo no esta permitido", 403);

            $arr_vias = $_POST['codevias'];
            $codeHU = $_POST['haburb'];

            $strHUvias = '';
            $strVias = '';

            foreach ( $arr_vias as $index => $val) {
                if ( strlen($strVias) == 0 ) {
                    $strHUvias = '0';
                    $strVias = $val;
                } else {
                    $strHUvias .= '@0';
                    $strVias .= ('@' . $val);
                }
            }

            $acction = QHaburbanas::assign_HabilitacionesUrbanas_Vias($strHUvias, $codeHU, $strVias);

            $data['pertenece']    = QHaburbanas::get_HabilitacionesUrbanas_Vias($codeHU, 1);
            $data['no_pertenece'] = QHaburbanas::get_HabilitacionesUrbanas_Vias($codeHU, 0);

            JSON::response(FALSE, 200, "OK", $data);

        } catch (Exception $ex) {
            JSON::response(TRUE, $ex->getCode(), $ex->getMessage(), []);
        }
    }

    public function actionRemovevias() {
        try {

            if (!Yii::app()->request->isAjaxRequest)
                throw new Exception("El metodo no esta permitido", 403);

            $arr_vias = $_POST['codevias'];
            $codeHU = $_POST['haburb'];

            $strHUvias = '';
            $strVias = '';

            foreach ( $arr_vias as $index => $val) {
                if ( strlen($strVias) == 0 ) {
                    $strHUvias = $val['huviacode'];
                    $strVias = $val['viacode'];
                } else {
                    $strHUvias .= '@' . $val['huviacode'];
                    $strVias .= ('@' . $val['viacode']);
                }
            }

            $acction = QHaburbanas::assign_HabilitacionesUrbanas_Vias($strHUvias, $codeHU, $strVias);

            $data['pertenece']    = QHaburbanas::get_HabilitacionesUrbanas_Vias($codeHU, 1);
            $data['no_pertenece'] = QHaburbanas::get_HabilitacionesUrbanas_Vias($codeHU, 0);

            JSON::response(FALSE, 200, "OK", $data);

        } catch (Exception $ex) {
            JSON::response(TRUE, $ex->getCode(), $ex->getMessage(), []);
        }
    }

}
