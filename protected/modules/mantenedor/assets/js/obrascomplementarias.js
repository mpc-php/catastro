// @koala-prepend "obrascomplementarias/_OBRASCOMPLEMENTARIAS.index.js"
// @koala-prepend "obrascomplementarias/_OBRASCOMPLEMENTARIAS.delete.js"
// @koala-prepend "obrascomplementarias/_OBRASCOMPLEMENTARIAS.manage.js"

$(function () {
    OBRASCOMPLEMENTARIAS.index.init();
    OBRASCOMPLEMENTARIAS.delete.init();
    OBRASCOMPLEMENTARIAS.manage.init();
});