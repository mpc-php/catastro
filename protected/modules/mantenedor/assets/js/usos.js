// @koala-prepend "usos/_USOS.index.js"
// @koala-prepend "usos/_USOS.delete.js"
// @koala-prepend "usos/_USOS.manage.js"
// @koala-prepend "usos/_USOS.actividades.js"


$(function() {
	USOS.index.init();
	USOS.delete.init();
	USOS.actividades.init();
	USOS.manage.init();
});