SECTORES.delete = (function (_win, $) {

    'use strict';

    var $md_delete = $('section#md-delete'),
            $bb_delete = {};

    var _bind = function () {
    },
            /**
             * EVENTO DE LA ACCIÓN ELIMINAR QUE ABRE UN MODAL DE CONFIRMACIÓN
             * @param  {object} 	e     	Instancia del evento
             * @param  {string} 	value 	Valor que contiene el boton
             * @param  {object} 	row   	objeto que contiene toda la información de la fila
             * @param  {int} 		index 	Posición de la fila
             * @return {void}
             */
            _open_modal = function (e, value, row, index) {
                $bb_delete = $md_delete.cBootbox({
                    title: "¿Realmente deseas eliminar?",
                    btnClose: "#btn-close",
                    beforeOpen: function () {
                        $.each(row, function (key, el) {
                            $md_delete.find('.' + key).text(el);
                        });
                        $md_delete.find('button[type=button]#btn-done')
                                .data({code: row.code, anio: row.anio})
                                .on('click.mantenedor.sector.module', _submit_delete);
                    }
                });
            },
            _submit_delete = function () {

                var $this = $(this);

                $this.prop({disabled: true});

                $.post(SECTORES.base_url + 'sector/delete', $this.data(), function (response) {

                    if (!response.error) {

                        $bb_delete.find('#btn-close').click();

                        SECTORES.index.table.refresh();

                        $.bootstrapGrowl(response.message, SECTORES.growl);
                    } else {
                        $this.prop({disabled: false});
                        $.bootstrapGrowl(response.message, SECTORES.growl);
                    }

                }).fail(function (xhr, status, error) {

                    $this.prop({disabled: false});
                    $.bootstrapGrowl(xhr.statusText, SECTORES.growl);

                }).always(function () {

                    $this.prop({disabled: false});

                });
            };

    return {
        init: _bind,
        show: _open_modal
    }

}(window, window.jQuery));