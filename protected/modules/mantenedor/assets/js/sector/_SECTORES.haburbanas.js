SECTORES.haburbanas = (function (_win, $) {

    'use strict';

    //ALMACENA TODOS LOS CHECKBOX
    var $chk_perte = $([]),
            $chk_noper = $([]),
            //ELMACENA EL VALOR O VALORES DE CADA CHECKEBOX EN ESTADO CHECKED
            selected_perte = {},
            selected_noper = {},
            data_json = {};

    var $md_actividades = $('section#md-haburbana'),
            $bb_actividades = {},
            //CHECKBOX EN CABECERA QUE MARCA TODOS LOS ITEMS
            $checkAll_perte = $md_actividades.find('input[type=checkbox]#checkAll-perte'),
            $checkAll_noper = $md_actividades.find('input[type=checkbox]#checkAll-noper'),
            //TABLAS PARA CADA COLUMNA
            $td_noper = $md_actividades.find('table#no-pertenece'),
            $td_perte = $md_actividades.find('table#pertenece'),
            //BOTON DE OPCIONES
            $btn_add = $md_actividades.find('button#btn-add'),
            $btn_remove = $md_actividades.find('button#btn-remove'),
            $txt_input = $md_actividades.find('.buscador_modal');

    var _bind = function () {
        $txt_input.on("keyup", function () {
            var data_json_final = [];
            var search_text = $(this).val().toUpperCase();
            var contenedor = $(this).attr("data-container");
            var data_seccion = data_json[contenedor];
            var filtro_codhaburb;
            for (var i = 0; i < data_seccion.length; i++) {
                if (contenedor == "pertenece") {
                    filtro_codhaburb = data_seccion[i].CodHabUrb.toUpperCase();
                } else {
                    filtro_codhaburb = data_seccion[i].Cod_Hab_Urb.toUpperCase();
                }
                if (data_seccion[i].HabilitacionUrbana.toUpperCase().indexOf(search_text) !== -1 || filtro_codhaburb.indexOf(search_text) !== -1) {
                    data_json_final.push(data_seccion[i]);
                }
            }
            if (contenedor == "no-pertenece") {
                _fill_table_noper(data_json_final);
            } else if (contenedor == "pertenece") {
                _fill_table_perte(data_json_final);
            }
        });
    },
            _open_modal = function () {

                var row = $(this).data('row');

                $bb_actividades = $md_actividades.cBootbox({
                    title: "Hab Urbanas por Sector",
                    btnClose: '#btn-close',
                    size: 'large',
                    beforeOpen: function () {

                        $.getJSON(SECTORES.base_url + 'sector/haburbanas/code/' + row.code, function (json) {

                            if (json.no_pertenece.length > 0) {
                                data_json['no-pertenece'] = json.no_pertenece;
                                _fill_table_noper(json.no_pertenece);
                            } else {
                                $td_noper.children('tbody').children('tr').remove();
                                $td_noper.children('tbody').append('<tr><td class="empty-list">La lista esta vacía</td></tr>');
                                $checkAll_noper.prop({disabled: true, checked: false});
                            }

                            if (json.pertenece.length > 0) {
                                data_json['pertenece'] = json.pertenece;
                                _fill_table_perte(json.pertenece);
                            } else {
                                $td_perte.children('tbody').children('tr').remove();
                                $td_perte.children('tbody').append('<tr><td class="empty-list">La lista esta vacía</td></tr>');
                                $checkAll_perte.prop({disabled: true, checked: false});
                            }

                        });

                        //EVENTOS PARA LOS CHECKBOX DE CABECERA QUE PERMITAN ELEGIR TODOS LOS ITEMS
                        $checkAll_noper.on('change.SECTORES.haburbanas.module', _checked_all_noper);
                        $checkAll_perte.on('change.SECTORES.haburbanas.module', _checked_all_perte);

                        //EVENTO QUE ENVIA DE NO PERTENECE A PERTENECE
                        $btn_add.data({codeuso: row.code}).on('click.SECTORES.haburbanas.module', _add_items);

                        //EVENTO QUE RETIRA DE PERTENECE Y REGRESA A NO PERTENECE
                        $btn_remove.data({codeuso: row.code}).on('click.SECTORES.haburbanas.module', _remove_items);

                    }

                });

                $bb_actividades.find('h2#name').text(row.name);
                $bb_actividades.find('h5#code').text(row.code);

            },
            //EVENTO QUE SE EJECUTA AL PRECIONAR EL BOTON AGREGAR
            _add_items = function () {
                var $this = $(this),
                        datasend = {};

                if (Object.keys(selected_noper).length == 0) {
                    $.bootstrapGrowl('Escoge minimo uno de los items de la columna NO PERTENECE', SECTORES.growl);
                    return false;
                } else if (Object.keys(selected_noper).length > 500) {
                    $.bootstrapGrowl('La cantidad de items seleccionados debe ser menor a 500', SECTORES.growl);
                    return false;
                }

                datasend = {
                    codeuso: $this.data('codeuso'),
                    codeacti: []
                };

                $.each(selected_noper, function (index, val) {
                    datasend.codeacti.push(parseInt(val));
                });


                $this.prop({disabled: true});
                $btn_remove.prop({disabled: true});

                $chk_perte.prop({disabled: true});
                $chk_noper.prop({disabled: true});

                $checkAll_perte.prop({disabled: true});
                $checkAll_noper.prop({disabled: true});

                $.post(SECTORES.base_url + 'sector/asignarhaburbanas', datasend, function (response, textStatus, xhr) {

                    if (!response.error) {
                        if (response.no_pertenece.length > 0)
                            _fill_table_noper(response.no_pertenece);
                        else {
                            $td_noper.children('tbody').children('tr').remove();
                            $td_noper.children('tbody').append('<tr><td>La lista esta vacía</td></tr>');
                        }

                        if (response.pertenece.length > 0)
                            _fill_table_perte(response.pertenece);
                        else {
                            $td_perte.children('tbody').children('tr').remove();
                            $td_perte.children('tbody').append('<tr><td>La lista esta vacía</td></tr>');
                        }
                    }

                    $this.prop({disabled: false});
                    $btn_remove.prop({disabled: false});

                    $chk_perte.prop({disabled: false});
                    $chk_noper.prop({disabled: false});

                    $checkAll_perte.prop({disabled: false, checked: false});
                    $checkAll_noper.prop({disabled: false, checked: false});

                    selected_noper = {};
                    selected_perte = {};

                });
            },
            //EVENTO QUE SE EJECUTA AL PRECIONAR EL BOTON DE REMOVER
            _remove_items = function () {
                var $this = $(this),
                        datasend = {};

                if (Object.keys(selected_perte).length == 0) {
                    $.bootstrapGrowl('Escoge minimo uno de los items de la columna PERTENECEN', SECTORES.growl);
                    return false;
                } else if (Object.keys(selected_perte).length > 500) {
                    $.bootstrapGrowl('La cantidad de items seleccionados debe ser menor a 500', SECTORES.growl);
                    return false;
                }

                datasend = {
                    codeuso: $this.data('codeuso'),
                    codeacti: []
                };

                $.each(selected_perte, function (index, val) {
                    datasend.codeacti.push(val);
                });

                //DESABILITANDO LOS CONTROLES
                $this.prop({disabled: true});
                $btn_remove.prop({disabled: true});

                $checkAll_noper.prop({disabled: true});
                $checkAll_perte.prop({disabled: true});

                $chk_perte.prop({disabled: true});
                $chk_noper.prop({disabled: true});

                $.post(SECTORES.base_url + 'sector/removehaburbanas', datasend, function (response, textStatus, xhr) {

                    if (!response.error) {
                        if (response.no_pertenece.length > 0)
                            _fill_table_noper(response.no_pertenece);
                        else {
                            $td_noper.children('tbody').children('tr').remove();
                            $td_noper.children('tbody').append('<tr><td>La lista esta vacía</td></tr>');
                        }

                        if (response.pertenece.length > 0)
                            _fill_table_perte(response.pertenece);
                        else {
                            $td_perte.children('tbody').children('tr').remove();
                            $td_perte.children('tbody').append('<tr><td>La lista esta vacía</td></tr>');
                        }
                    }

                    //HABILITANDO LOS CONTROLES
                    $this.prop({disabled: false});
                    $btn_remove.prop({disabled: false});

                    $checkAll_noper.prop({disabled: false, checked: false});
                    $checkAll_perte.prop({disabled: false, checked: false});

                    $chk_perte.prop({disabled: false});
                    $chk_noper.prop({disabled: false});

                    //RESETEANDO LOS VALORES CAPTURADOS
                    selected_noper = {};
                    selected_perte = {};

                });
            },
            //EVENTOS QUE SE DESENCADENA AL PRESIONAR EL CHECKBOX DE CABECERA
            _checked_all_noper = function () {
                var $this = $(this);

                //SI EL CHECKBOX ESTA EN ESTADO ACTIVO SELECCIONA A TODOS LOS DE 
                //SU COLUMNA Y EJECUTA SU EVENTO DE CAMBIO
                if ($this.is(':checked')) {
                    $chk_noper.prop({checked: true}).change();
                } else {
                    $chk_noper.prop({checked: false}).change();
                }
            },
            //EVENTOS QUE SE DESENCADENA AL PRESIONAR EL CHECKBOX DE CABECERA
            _checked_all_perte = function () {
                var $this = $(this);

                //SI EL CHECKBOX ESTA EN ESTADO ACTIVO SELECCIONA A TODOS LOS DE 
                //SU COLUMNA Y EJECUTA SU EVENTO DE CAMBIO
                if ($this.is(':checked')) {
                    $chk_perte.prop({checked: true}).change();
                } else {
                    $chk_perte.prop({checked: false}).change();
                }
            },
            //EVENTO PARA SELECCION INDIVIDUAL
            _selected_item = function () {
                var $this = $(this);

                if ($this.is(':checked')) {
                    $this.parents('tr').css({
                        backgroundColor: '#448AFF',
                        color: 'white'
                    });

                    if ($this.data('type') == 'perte') {
                        selected_perte[$this.attr('id')] = {
                            UsoAEcode: $this.data('vinc'),
                            AEcode: $this.val()
                        };
                    } else {
                        selected_noper[$this.attr('id')] = $this.val();
                    }

                } else {

                    $this.parents('tr').removeAttr('style');


                    if ($this.data('parent').is(':checked')) {
                        $this.data('parent').prop({indeterminate: true});
                    }

                    if ($this.data('type') == 'perte') {
                        delete selected_perte[$this.attr('id')];
                    } else {
                        delete selected_noper[$this.attr('id')];
                    }

                }
            },
            _dblClick_selected_item = function () {
                var $this = $(this),
                        checkbox = $this.find('td:eq(0) input[type=checkbox]');

                if (checkbox.is(':checked'))
                    checkbox.prop({checked: false}).change();
                else
                    checkbox.prop({checked: true}).change();
            },
            //LLENA TODA LA COLUMNA NOPERTENECE, Y AGREGA LOS EVENTOS
            _fill_table_noper = function (DATA) {

                var $tdbody = $td_noper.find('tbody');

                //LIMPIANDO TODA LA COLUMNA
                $tdbody.children('tr').remove();

                //SI NO HAY DATOS DESACTIVA EL BOTON DE AGREGAR
                if (DATA.length == 0) {
                    $btn_add.prop({disabled: true});
                } else {
                    $btn_add.prop({disabled: false});
                }

                DATA.forEach(function (obj, index) {

                    //CONSTRUYENDO LA FILA Y TODAS SUS COLUMNAS
                    var row = $([
                        '<tr>',
                        '<td><input type="checkbox" id="chknoper' + (index + 1) + '" value="' + obj.Cod_Hab_Urb + '" name="noper-' + (index + 1) + '" data-type="noper"></td>',
                        '<td>' + obj.Cod_Hab_Urb + '</td>',
                        '<td>' + obj.HabilitacionUrbana + '</td>',
                        '</tr>'
                    ].join(''));

                    row.on('dblclick.SECTORES.haburbanas.module', _dblClick_selected_item);

                    //BUSCA EL CHECKBOX GUARDA EN SU DATA EL BOTON PADRE(Boton que marca todos)
                    //AGREGA EL EVENTO DE SELECCION INDIVIDUAL
                    row.find('input[type=checkbox]').data({
                        parent: $checkAll_noper
                    }).on('change.SECTORES.haburbanas.module', _selected_item);

                    //AGREGANDO EL ITEM A LA COLUMNA
                    $tdbody.append(row);

                });

                //BUSCA TODOS LOS CHECKBOX AGREGADOS A LA LISTA
                $chk_noper = $tdbody.find('input[type=checkbox]');
            },
            //LLENA TODOS LOS ITEMS DE LA COLUMNA PERTENECEN Y AGREGA LOS EVENTOS
            _fill_table_perte = function (DATA) {

                var $tbody = $td_perte.find('tbody');

                //LIMPIANDO TODA LA COLUMNA
                $tbody.children('tr').remove();

                //SI NO HAY DATOS DESACTIVA EL BOTON DE REMOVER
                if (DATA.length == 0) {
                    $btn_remove.prop({disabled: true});
                } else {
                    $btn_remove.prop({disabled: false});
                }

                DATA.forEach(function (obj, index) {

                    //CONSTRUYENDO LA FILA Y TODAS SUS COLUMNAS
                    var row = $([
                        '<tr>',
                        '<td><input type="checkbox"  id="chkperte' + (index + 1) + '" name="perte-' + (index + 1) + '" value="' + obj.CodHabUrb + '" data-vinc="' + obj.idSectorHabUrba + '" data-type="perte"></td>',
                        '<td>' + obj.CodHabUrb + '</td>',
                        '<td>' + obj.HabilitacionUrbana + '</td>',
                        '</tr>'
                    ].join(''));

                    row.on('dblclick.SECTORES.haburbanas.module', _dblClick_selected_item);

                    //BUSCA EL CHECKBOX GUARDA EN SU DATA EL BOTON PADRE(Boton que marca todos)
                    //AGREGA EL EVENTO DE SELECCION INDIVIDUAL
                    row.find('input[type=checkbox]').data({
                        parent: $checkAll_perte
                    }).on('change.SECTORES.haburbanas.module', _selected_item);

                    //AGREGANDO EL ITEM A LA COLUMNA
                    $tbody.append(row);

                });

                //BUSCA TODOS LOS CHECKBOX AGREGADOS A LA LISTA
                $chk_perte = $tbody.find('input[type=checkbox]');
            };

    return {
        init: _bind,
        show: _open_modal
    };

}(window, window.jQuery));