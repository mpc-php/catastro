// @koala-prepend "haburbanas/_HU.index.js"
// @koala-prepend "haburbanas/_HU.vias.js"
// @koala-prepend "haburbanas/_HU.delete.js"
// @koala-prepend "haburbanas/_HU.manage.js"


$(function() {
	HU.index.init();
	HU.vias.init();
	HU.delete.init();
	HU.manage.init();
});