// @koala-prepend "sector/_SECTORES.index.js"
// @koala-prepend "sector/_SECTORES.delete.js"
// @koala-prepend "sector/_SECTORES.manage.js"
// @koala-prepend "sector/_SECTORES.haburbanas.js"
// @koala-prepend "sector/_SECTORES.zonificacion.js"

$(function () {
    SECTORES.index.init();
    SECTORES.delete.init();
    SECTORES.manage.init();
    SECTORES.haburbanas.init();
    SECTORES.zonificacion.init();
});
