HU.delete = (function (_win, $) {

	'use strict';

	var $md_delete = $('section#md-delete'),
			$bb_delete = {};

	//CONSTRUCTOR
	var _bind = function () {},

	_submit_delete = function() {
		var $this = $(this);

		$this.prop({disabled:true});

		$.post(HU.base_url + 'haburbanas/delete', $this.data(), function(response) {
			if ( ! response.error ) {
				$bb_delete.find('#btn-close').click();
				$.bootstrapGrowl(response.message, HU.growl);
				$table.bootstrapTable('refresh');
			} else {
				$this.prop({disabled:false});
				$.bootstrapGrowl(response.message, HU.growl);
			}
		}).fail(function(xhr, status, error){
			$this.prop({disabled:false});
			$.bootstrapGrowl(xhr.statusText, HU.growl);
		}).always(function() {
			$frm_manage.find('button[type=submit]').prop({disabled:false});
		});
	},

	/**
	* EVENTO DE LA ACCIÓN ELIMINAR QUE ABRE UN MODAL DE CONFIRMACIÓN
	* @param  {object} 	e     	Instancia del evento
	* @param  {string} 	value 	Valor que contiene el boton
	* @param  {object} 	row   	objeto que contiene toda la información de la fila
	* @param  {int} 		index 	Posición de la fila
	* @return {void}
	*/
	_delete_row = function(e, value, row, index) {
		$bb_delete = $md_delete.cBootbox({
			title: "¿Realmente deseas eliminar?",
			btnClose: '#btn-close',
			beforeOpen: function () {
				$.each(row, function(key, el) {
		  		$md_delete.find('.'+key).text(el);
		  	});
		  	$md_delete.find('button[type=button]#btn-done')
		  		.data({code:row.code})
		  		.on('click.mantenedor.haburbanas.module', _submit_delete);
			}
		});
	};

	return {
		init : _bind,
		show : _delete_row
	}

}(window, window.jQuery));