HU.manage = (function (_win, $) {

	'use strict';

	var validation = {},
			validator = {
				rules : {
					code: { required:true },
					name: { required:true }
				},
				messages : {
					type : 'Campo requerido.',
					code : 'Campo requerido.',
					name : 'Campo requerido.'
				}
			};

	var $btn_manage = $('button#btn-manage'),
			$md_manage = $('section#md-manage'),
			$frm_manage = $md_manage.find("form#frm-manage"),
			$bb_manage = {};

	//CONSTRUCTOR
	var _bind = function () {
		$btn_manage.on('click.HU.manage.module', _open_modal_new);
	},

	/**
	 * Quita espacios en blanco antes y despues del texto dentro del input
	 * @return {void} 
	 */
	_remove_empty_text = function() {
		var $this = $(this),
				value = $this.val();

		$this.val($.trim(value));
	},

	/**
		* Envia toda la información digitada para que sea o ingresada o actualizada
		* dependiendo del boton que abrió el modal
		* @param  {object} e Instancia del evento
		* @return {void}  
		*/
	_submit_manage = function(e) {

		var $form = $(this),
				resource = HU.base_url + 'haburbanas/manage';

		if ( ! validation.form() ) {
			e.preventDefault();
			return false;
		}

		$frm_manage.find('button[type=submit]').prop({disabled:true});

		$.post(resource, $frm_manage.serialize(), function(response) {

			if ( ! response.error ) {

				//Cerrando el modal (Forma correcta)
				$bb_manage.find('#btn-close').click();

				//Mostrando el mensaje
				$.bootstrapGrowl(response.message, HU.growl);

				//refrescando la tabla
				HU.index.table.refresh();

			} else {
				$frm_manage.find('button[type=submit]').prop({disabled:false});
				$.bootstrapGrowl(response.message, HU.growl);
			}

		}).always(function() {
			$frm_manage.find('button[type=submit]').prop({disabled:false});
		});

		e.preventDefault();
	},

	/**
	 * eVENTO QUE SE DESENCADENA AL PRECIONAR EL BOTON NUEVO
	 * PERMITIENDO ABRIR EL MODAL
	 * @return {[type]} [description]
	 */
	_open_modal_new = function() {
		$bb_manage = $md_manage.cBootbox({
			title: "Nuevo valor",
			btnClose: '#btn-close',
			beforeOpen: _init_frm_manage,
			afterClose: _reset_frm_manage
		});
	},

	/**
		* EVENTO DE LA ACCIÓN MODIFICAR QUE ABRE UN MODAL 
		* CON EL FORMULARIO PARA EL MANEJO DE LA DATA
		* @param  {object} 	e     	Instancia del evento
		* @param  {string} 	value 	Valor que contiene el boton
		* @param  {object} 	row   	objeto que contiene toda la información de la fila
		* @param  {int} 		index 	Posición de la fila
		* @return {void}
		*/
  _open_modal_edit = function(e, value, row, index) {

		//iTERAMOS LA FILA Y ASIGNAMOS LA DATA A CADA ELEMENTO
		$.each(row, function(key, el) {
			var $item = $frm_manage.find('[name='+key+']'),
					tag = $item[0].tagName;

			//cONFIRMAMOS EL TIPO DE ELEMENTO
			if ( tag == 'INPUT' ) 
				$item.val(el);
			else if ( tag == 'SELECT' ) 
				$item.children('option[data-selected="'+el+'"]').prop({selected:true});
		});

		//aBRIMOS EL MODAL PARA EL MANEJO DEL FORMULARIO
		$bb_manage = $md_manage.cBootbox({
			title: "Editar valor",
			btnClose:'#btn-close',
			beforeOpen: function(){
				_init_frm_manage();
				//eSTABLECEMOS EN SOLO-LECTURA EL INPUT DEL CODIGO
				$frm_manage.find('input#txtcode').prop({readonly:true});
			},
			afterClose: _reset_frm_manage
		});

  },

  /**
		* iNICIA LAS CONFIGURACIONES BASICAR DEL FORMULARIO (EVENTOS, VALIDACIONES, DESACTIVACION)
		* @return {void} 
		*/
	_init_frm_manage = function() {

		//iNICIANDO EL VALIDADOR
		validation = $frm_manage.validate(validator);

		//dESHABILITAR TODOS LOS CONTROLES DEL FORMULARIO
		$frm_manage.find('input[type=text], select, button[type=submit]').prop({disabled:false});

		//aGREGAR EVENTOS
		$frm_manage.on('submit.HU.manage.module', _submit_manage);

		//eVITAMOS QUE DEJEN ESPACIOS EN BLANCO ANTES Y DESPUES DE UN TEXTO O QUE SOLO
		//DIGITEN ESPACIOS EN BLANCO
		$frm_manage.find('input[type=text]').on('change.HU.manage.module', _remove_empty_text);

	},

  /**
   * REINICIA EL FORMULARIO, DESACTIVA LOS CONTROLES Y 
   * @return {[type]} [description]
   */
	_reset_frm_manage = function() {

		$frm_manage
			.find('input[type=text], select, button[type=submit]').prop({
				disabled: true,
				readonly:false
			}).removeClass('error');

		$frm_manage.find('input[type=text]').val('');
		$frm_manage.find('select option:eq(0)').prop({selected:true});
		$frm_manage.find('input[type=text]#txtcode').prop({disabled:false});

		validation.resetForm();
	};

	return {
		init: _bind,
		edit: _open_modal_edit
	};

}(window, window.jQuery));