//INSTANCIANDO EL MODULO HU = HABILITACIONES HURBANAS
var HU = {};

HU.base_url = Request.BaseUrl + '/' + Request.UrlHash.m + '/';

HU.bootTable = {
	escape: false,
  locale: 'es-SP',
  search: true,
  pagination: true,
  pageSize: 10,
  idField: "code"
};

HU.growl = {
	ele: 'body',
  type: 'success',
  offset: {from: 'top', amount: 80},
  align: 'center',
  width: 'auto',
  delay: 2500,
  allow_dismiss: true,
  stackup_spacing: 10
};

HU.index = (function (_win, $) {

	'use strict';

	var $table = $('table#tdhaburbanas'),
			$cnt_table = $(".search-table"),
			$input_search_gen = $('input#buscador_generico'),
			$btn_search_gen = $('button#buscador_generico_btn');

	//CONSTRUCTOR
	var _bind = function () {
		_build_table();
	},

	/**
	 * bOTONES DE ACCION PARA CADA FILA
	 * @param  {string} value 'valor que tendrá el boton'
	 * @param  {object} row   Objeto que contiene todos los valores de la fila
	 * @param  {int} index Posición de la fila
	 * @return {string}       Cadena de botones para cada fila
	 */
	_action_buttons = function(value, row, index) {
    return [
    	'<button id="btn-vias-'+row.code+'" class="btn-actions vias btn btn-sm btn-info" data-id="'+row.code+'" data-accion="vias">',
				'<i class="fa fa-map" aria-hidden="true"></i>',
			'</button>',
			'<button id="btn-edit-'+row.code+'" class="btn-actions edit btn btn-sm btn-info" data-id="'+row.code+'" data-accion="edit">',
				'<i class="fa fa-pencil"></i>',
			'</button>',
			'<button id="btn-delete-'+row.code+'" class="btn-actions delete btn btn-sm btn-danger" data-id="'+row.code+'">',
				'<i class="fa fa-times"></i>',
			'</button>'
		].join('');
  },

  /**
   * cONSTRULLE TABLA PRINCIPAL(CABECERA, CUERPO), ATRAVEZ DE UNA CONSULTA JSON
   * @return {VOID}
   */
	_build_table = function() {

  	var tableParams = $.extend(true, {}, HU.bootTable);

  	tableParams.url =  HU.base_url + 'haburbanas/show';

  	//TABLE HEADER
		tableParams.columns = [
			{ field: 'code', title: 'Código', align: 'center', sortable:true },
			{ field: 'type', title: 'Tipo HU', align: 'right', sortable:true },
			{ field: 'name', title: 'Nombre HU', align: 'center', sortable:true },
			{ field: 'zoni', title: 'Zona, Sector, Etapa', align: 'center', sortable:true },
			{ 
				field: 'action', 
				title: ' Acciones', 
				align: 'center', 
				formatter : _action_buttons, 
				events : {
						'click .vias': HU.vias.show,
		        'click .edit': HU.manage.edit,
		        'click .delete': HU.delete.show
	    		}
	    	}
		];

		$table.bootstrapTable(tableParams);
		
		//CUSTOM SEARCH
		$input_search_gen.on("keyup, change", function () {
			$cnt_table.find('.search input').val(this.value).trigger("keyup");
		});
		
		$btn_search_gen.on("click", function () {
			$cnt_table.find('.search input').val($input_search_gen.val()).trigger("keyup");
		});
  };

	//PUBLICAR
	return {
		init: _bind,
		table: {
			refresh : function(){
				$table.bootstrapTable('refresh');
			}
		}
	};

}(window, window.jQuery));