var mantenedor = mantenedor || {};

mantenedor.tecnico = (function (_win, $) {

    'use strict';

    var base_url = Request.BaseUrl + '/' + Request.UrlHash.m + '/',
            //GUARDA LA INSTANCIA DEL VALIDATOR PARA PODER RESETEAR EL FORMULARIO
            validation = {},
            params = {
                bootTable: {
                    escape: false,
                    search: true,
                    locale: 'es-SP',
                    pagination: true,
                    pageSize: 10,
                    idField: "id"
                },
                growl: {
                    ele: 'body',
                    type: 'success',
                    offset: {from: 'top', amount: 80},
                    align: 'center',
                    width: 'auto',
                    delay: 2500,
                    allow_dismiss: true,
                    stackup_spacing: 10
                }
            };

    var validator = {
        rules: {
            type: {required: true},
            code: {required: true},
            name: {required: true},
            last_name1: {required: true},
            last_name2: {required: true}
        },
        messages: {
            type: 'Campo requerido.',
            code: 'Campo requerido.',
            name: 'Campo requerido.',
            last_name1: 'Campo requerido.',
            last_name2: 'Campo requerido.'
        }
    };

    var $table = $('table#tdtecnicos'),
            $md_manage = $('section#md-manage'),
            $btn_manage_close = $md_manage.find('button#btn-close'),
            $frm_manage = $md_manage.find("form#frm-manage"),
            $btn_manage = $('button#btn-manage'),
            $bb_manage = {},
            $md_delete = $('section#md-delete'),
            $btn_delete_close = $md_delete.find('button#btn-close'),
            $bb_delete = {},
            $cnt_table = $(".search-table"),
            $input_search_gen = $('input#buscador_generico'),
            $btn_search_gen = $('button#buscador_generico_btn');

    var _bind = function () {
        _build_table();
        $btn_manage.on('click.mantenedor.tecnico.module', _new);
    },
            _remove_empty_text = function () {
                var $this = $(this),
                        value = $this.val();

                $this.val($.trim(value));
            },
            _submit_manage = function (e) {

                var $form = $(this),
                        resource = base_url + 'tecnico/manage';

                if (!validation.form()) {
                    e.preventDefault();
                    return false;
                }

                $frm_manage.find('button[type=submit]').prop({disabled: true});

                $.post(resource, $frm_manage.serialize(), function (response) {

                    if (!response.error) {
                        $bb_manage.modal('hide');
                        $.bootstrapGrowl(response.message, params.growl);
                        $table.bootstrapTable('refresh');
                    } else {
                        $frm_manage.find('button[type=submit]').prop({disabled: false});
                        $.bootstrapGrowl(response.message, params.growl);
                    }

                }).always(function () {
                    $frm_manage.find('button[type=submit]').prop({disabled: false});
                });

                e.preventDefault();
            },
            _submit_delete = function () {
                var $this = $(this);

                $this.prop({disabled: true});

                $.post(base_url + 'tecnico/delete', $this.data(), function (response) {
                    if (!response.error) {
                        $bb_delete.modal('hide');
                        $.bootstrapGrowl(response.message, params.growl);
                        $table.bootstrapTable('refresh');
                    } else {
                        $this.prop({disabled: false});
                        $.bootstrapGrowl(response.message, params.growl);
                    }
                }).fail(function (xhr, status, error) {
                    $this.prop({disabled: false});
                    $.bootstrapGrowl(xhr.statusText, params.growl);
                }).always(function () {
                    $frm_manage.find('button[type=submit]').prop({disabled: false});
                });
            },
            _new = function () {
                //aBRIMOS EL MODAL PARA EL MANEJO DEL FORMULARIO
                $bb_manage = $md_manage.cBootbox({
                    title: "Nuevo Técnico",
                    beforeOpen: _init_frm_manage,
                    afterClose: _reset_frm_manage
                });
                $("#txtid").val(0);
            },
            /**
             * EVENTO DE LA ACCIÓN MODIFICAR QUE ABRE UN MODAL 
             * CON EL FORMULARIO PARA EL MANEJO DE LA DATA
             * @param  {object} 	e     	Instancia del evento
             * @param  {string} 	value 	Valor que contiene el boton
             * @param  {object} 	row   	objeto que contiene toda la información de la fila
             * @param  {int} 		index 	Posición de la fila
             * @return {void}
             */
            _edit_row = function (e, value, row, index) {
                //iTERAMOS LA FILA Y ASIGNAMOS LA DATA A CADA ELEMENTO

                $.each(row, function (key, el) {
                    var $item = $frm_manage.find('[name=' + key + ']'), tag = $item[0].tagName;

                    //  cONFIRMAMOS EL TIPO DE ELEMENTO
                    if (tag == 'INPUT')
                        $item.val(el);
                    else if (tag == 'SELECT')
                        $item.children('option[data-selected="' + el + '"]').prop({selected: true});
                });

                //aBRIMOS EL MODAL PARA EL MANEJO DEL FORMULARIO
                $bb_manage = $md_manage.cBootbox({
                    title: "Editar Técnico",
                    beforeOpen: function () {
                        _init_frm_manage();
                        $frm_manage.find('input#txtcode').prop({readonly: true});
                    },
                    afterClose: _reset_frm_manage
                });
                $("#txtes").val("2");
            },
            /**
             * EVENTO DE LA ACCIÓN ELIMINAR QUE ABRE UN MODAL DE CONFIRMACIÓN
             * @param  {object} 	e     	Instancia del evento
             * @param  {string} 	value 	Valor que contiene el boton
             * @param  {object} 	row   	objeto que contiene toda la información de la fila
             * @param  {int} 		index 	Posición de la fila
             * @return {void}
             */
            _delete_row = function (e, value, row, index) {
                $bb_delete = $md_delete.cBootbox({
                    title: "¿Realmente deseas eliminar?",
                    btnClose: "#btn-close",
                    beforeOpen: function () {
                        $.each(row, function (key, el) {
                            $md_delete.find('.' + key).text(el);
                        });
                        $md_delete.find('button[type=button]#btn-done')
                                .data({id: row.id})
                                .on('click.mantenedor.tecnico.module', _submit_delete);
//                        $btn_delete_close.on('click.mantenedor.anuncios.module', _close_md_delete);
                    }
                });
            },
            /**
             * iNICIA LAS CONFIGURACIONES BASICAR DEL FORMULARIO (EVENTOS, VALIDACIONES, DESACTIVACION)
             * @return {void} 
             */
            _init_frm_manage = function () {
                //iNICIANDO EL VALIDADOR
                validation = $frm_manage.validate(validator);
                //dESHABILITAR TODOS LOS CONTROLES DEL FORMULARIO
                $frm_manage.find('input[type=text], select, button[type=submit]').prop({disabled: false});
                //aGREGAR EVENTOS
                $frm_manage.on('submit.mantenedor.tecnico.module', _submit_manage);

                $btn_manage_close.on('click.mantenedor.tecnico.module', _close_md_manage);
                //eVITAMOS QUE DEJEN ESPACIOS EN BLANCO ANTES Y DESPUES DE UN TEXTO O QUE SOLO
                //DIGITEN ESPACIOS EN BLANCO
                $frm_manage.find('input[type=text]').on('change.mantenedor.tecnico.module', _remove_empty_text);
            },
            _close_md_manage = function () {
                $bb_manage.find('.bootbox-close-button').click();
            },
            _close_md_delete = function () {
                $bb_delete.find('.bootbox-close-button').click();
            },
            /**
             * REINICIA EL FORMULARIO, DESACTIVA LOS CONTROLES Y 
             * @return {[type]} [description]
             */
            _reset_frm_manage = function () {
                $frm_manage.find('input[type=text], select, button[type=submit]')
                        .prop({
                            disabled: true,
                            readonly: false
                        })
                        .removeClass('error');

                $frm_manage.find('input[type=text]').val('');
                $frm_manage.find('select option:eq(0)').prop({selected: true});
                $frm_manage.find('input[type=text]#txtcode').prop({disabled: false});

                validation.resetForm();
            },
            /**
             * bOTONES DE ACCION PARA CADA FILA
             * @param  {string} value 'valor que tendrá el boton'
             * @param  {object} row   Objeto que contiene todos los valores de la fila
             * @param  {int} index Posición de la fila
             * @return {string}       Cadena de botones para cada fila
             */
            _action_buttons = function (value, row, index) {
                return [
                    '<button id="btn-edit-' + row.id + '" class="btn-actions edit btn btn-sm btn-info" data-id="' + row.id + '" data-accion="edit">',
                    '<i class="fa fa-pencil"></i>',
                    '</button>',
                    '<button id="btn-delete-' + row.id + '" class="btn-actions delete btn btn-sm btn-danger" data-id="' + row.id + '">',
                    '<i class="fa fa-times"></i>',
                    '</button>'
                ].join('');
            },
            /**
             * cONSTRULLE TABLA PRINCIPAL(CABECERA, CUERPO), ATRAVEZ DE UNA CONSULTA JSON
             * @return {VOID}
             */
            _build_table = function () {

                var tableParams = $.extend(true, {}, params.bootTable);

                tableParams.url = base_url + 'tecnico/show';

                //TABLE HEADER
                tableParams.columns = [
                    {field: 'id', title: 'ID', align: 'center', sortable: true},
                    {field: 'code', title: 'N° Documento', align: 'center', sortable: true},
                    {field: 'last_name1', title: 'A. Paterno', sortable: true},
                    {field: 'last_name2', title: 'A. Materno', sortable: true},
                    {field: 'name', title: 'Nombres', sortable: true},
                    {field: 'type', title: 'Cargo', sortable: true},
                    {
                        field: 'action',
                        title: ' Acciones',
                        align: 'center',
                        formatter: _action_buttons,
                        events: {
                            'click .edit': _edit_row,
                            'click .delete': _delete_row
                        }
                    }
                ];

                $table.bootstrapTable(tableParams);

                //CUSTOM SEARCH
                $input_search_gen.on("keyup", function () {
                    $cnt_table.find('.search input').val(this.value).trigger("keyup");
                });

                $btn_search_gen.on("click", function () {
                    $cnt_table.find('.search input').val($input_search_gen.val()).trigger("keyup");
                });
            };

    /**
     * PUBLIC MODULE
     */
    return {
        init: _bind
    };

}(window, window.jQuery));



$(function () {
    mantenedor.tecnico.init();
});


$(document).ready(function () {
    var base_url = Request.BaseUrl + '/' + Request.UrlHash.m + '/';
    $("#txtcode").on("blur", function () {
        var id = $("#txtcode").val();





        $.ajax({
            type: 'post',
            dataType: 'json',
            url: base_url + '/tecnico/Buscar',
            contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
            data: {dni: id},
            success: function (result) {
                if (result.estado === true && result.existe === "A") {
                    bootbox.alert({
                        message: "<label><br><strong>USUARIO YA REGISTRADO!</strong></label>",
                        size: 'small'
                    });
                    $("#txtcode").val("");
                } else if (result.estado === true && result.existe === "I") {
                    bootbox.confirm({
                        message: "<label><br><strong>USUARIO SE ENCUENTRA INACTIVO! ¿Desea recuperarlo?</strong></label>",
                        size: 'small',
                        buttons: {
                            confirm: {
                                label: 'Si',
                                className: 'btn-success'
                            },
                            cancel: {
                                label: 'No',
                                className: 'default'
                            }
                        },
                        callback: function (result) {
                            if (result === true) {
                                $.ajax({
                                    type: 'post',
                                    dataType: 'json',
                                    url: base_url + '/tecnico/recover',
                                    contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
                                    data: {dni: id},
                                    success: function (resul) {
                                        $("#tdtecnicos").bootstrapTable('refresh');
                                        $("#btn-close").click();
                                        $("#txtcode").val();
                                        $.bootstrapGrowl("USUARIO RECUPERADO", '');
                                    }
                                });
                            } else {
                                console.log('No existe usuario : ' + result);
                            }
                        }
                    });
                } else {
                    $.ajax({
                        type: 'post',
                        dataType: 'json',
                        url: base_url + '/tecnico/BuscarPersona',
                        contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
                        data: {dni: id},
                        success: function (result) {
                            if (result.estado === true) {
                                $("#txtes").val("1");
                                $("#txtpeople").val(result.data[0].ID_PERSONA);
                                $("#txtname").val(result.data[0].NOMBRES);
                                $("#last_name1").val(result.data[0].APE_PATERNO);
                                $("#last_name2").val(result.data[0].APE_MATERNO);
                            } else {
                                $("#txtes").val("0");
                                $("#txtpeople").val();
                                $("#txtname").val();
                                $("#last_name1").val();
                                $("#last_name2").val();
                            }
                        }
                    });
                }
            }
        });


    });
});




