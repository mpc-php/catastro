USOS.manage = (function (_win, $) {

	'use strict';

	var validation = {},
			validator = {
				rules : {
					code: { required:true },
					name: { required:true }
				},
				messages : {
					type : 'Campo requerido.',
					code : 'Campo requerido.',
					name : 'Campo requerido.'
				}
			};

	var $md_manage 	= $('section#md-manage'),
			$frm_manage = $md_manage.find("form#frm-manage"),
			$btn_manage = $('button#btn-manage'),
			$bb_manage 	= {};

	var _bind = function () {
		$btn_manage.on('click.USOS.manage.module', _open_modal_new);
	},

	_remove_empty_text = function() {
		var $this = $(this),
				value = $this.val();

		$this.val( $.trim(value) );
	},

	_submit_manage = function(e) {
		var $form = $(this),
				resource = USOS.base_url + 'usos/manage';
		
		if ( ! validation.form() ) {
			e.preventDefault();
			return false;
		}

		$frm_manage.find('button[type=submit]').prop({disabled:true});

		$.post(resource, $frm_manage.serialize(), function(response) {
			if ( ! response.error ) {

				$bb_manage.find('#btn-close').click();
				$.bootstrapGrowl(response.message, USOS.growl);
				USOS.index.table.refresh();

			} else {
				$frm_manage.find('button[type=submit]').prop({disabled:false});
				$.bootstrapGrowl(response.message, USOS.growl);
			}

		}).always(function() {
			$frm_manage.find('button[type=submit]').prop({disabled:false});
		});

		e.preventDefault();
	},

	_open_modal_new = function() {
  	//aBRIMOS EL MODAL PARA EL MANEJO DEL FORMULARIO
  	$bb_manage = $md_manage.cBootbox({
  		title: "Nuevo valor",
  		btnClose: '#btn-close',
  		beforeOpen: _init_frm_manage,
  		afterClose: _reset_frm_manage
  	});
  },

  /**
   * EVENTO DE LA ACCIÓN MODIFICAR QUE ABRE UN MODAL 
   * CON EL FORMULARIO PARA EL MANEJO DE LA DATA
   * @param  {object} 	e     	Instancia del evento
   * @param  {string} 	value 	Valor que contiene el boton
   * @param  {object} 	row   	objeto que contiene toda la información de la fila
   * @param  {int} 		index 	Posición de la fila
   * @return {void}
   */
  _open_modal_edit = function(e, value, row, index) {

  	//iTERAMOS LA FILA Y ASIGNAMOS LA DATA A CADA ELEMENTO
  	$.each(row, function(key, el) {
  		var $item = $frm_manage.find('[name='+key+']'),
  			tag = $item[0].tagName;

  		//cONFIRMAMOS EL TIPO DE ELEMENTO
  		if ( tag == 'INPUT' ) 
  			$item.val(el);
  		else if ( tag == 'SELECT' ) 
  			$item.children('option[data-selected="'+el+'"]').prop({selected:true});
  	});

  	//aBRIMOS EL MODAL PARA EL MANEJO DEL FORMULARIO
  	$bb_manage = $md_manage.cBootbox({
  		title: "Editar valor",
  		btnClose: "#btn-close",
  		beforeOpen: function(){
  			_init_frm_manage();
  			$frm_manage.find('input#txtcode').prop({readonly:true});
  		},
  		afterClose: _reset_frm_manage
  	});
  },

  /**
   * iNICIA LAS CONFIGURACIONES BASICAR DEL FORMULARIO (EVENTOS, VALIDACIONES, DESACTIVACION)
   * @return {void} 
   */
  _init_frm_manage = function() {
  	//iNICIANDO EL VALIDADOR
  	validation = $frm_manage.validate(validator);
		//dESHABILITAR TODOS LOS CONTROLES DEL FORMULARIO
		$frm_manage.find('input[type=text], select, button[type=submit]').prop({disabled:false});
		//aGREGAR EVENTOS
		$frm_manage.on('submit.USOS.manage.module', _submit_manage);

		//$btn_manage_close.on('click.USOS.manage.module', _close_md_manage);

		//eVITAMOS QUE DEJEN ESPACIOS EN BLANCO ANTES Y DESPUES DE UN TEXTO O QUE SOLO
		//DIGITEN ESPACIOS EN BLANCO
		$frm_manage.find('input[type=text]').on('change.USOS.manage.module', _remove_empty_text);
  },

  /**
   * REINICIA EL FORMULARIO, DESACTIVA LOS CONTROLES Y 
   * @return {[type]} [description]
   */
	_reset_frm_manage = function() {
		$frm_manage.find('input[type=text], select, button[type=submit]')
			.prop({
				disabled: true,
				readonly:false
			})
			.removeClass('error');

		$frm_manage.find('input[type=text]').val('');
		$frm_manage.find('select option:eq(0)').prop({selected:true});
		$frm_manage.find('input[type=text]#txtcode').prop({disabled:false});

		validation.resetForm();
	};

	return {
		init: _bind,
		edit: _open_modal_edit
	};

}(window, window.jQuery));