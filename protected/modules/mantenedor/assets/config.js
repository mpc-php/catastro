/**
 * Configuración personalizada para la carga de librerias del módulo
 * 
 * @type Object
 */
var Builder = {
    module: {
        controllers: {
            sector: {
                //Assets para un controllador especifico y todas sus acciones
                js: [{sector: ["_PATH_:bT_ES", "_PATH_:validate", "_PATH_:bootstrapSelect", "_PATH_:multi_select", "_PATH_:select2", "_PATH_:components_multi_select", "_PATH_:bootstrapGrowlUI", "_PATH_:PBootbox"]}],
                css: {
                    libs: [],
                    package: [],
                    custom: []
                }
            },
            via: {
                //Assets para un controllador especifico y todas sus acciones
                js: [{via: ["_PATH_:bT_ES", "_PATH_:validate", "_PATH_:bootstrapGrowlUI", "_PATH_:PBootbox"]}],
                css: {
                    libs: [],
                    package: [],
                    custom: []
                }
            },
            haburbanas: {
                //Assets para un controllador especifico y todas sus acciones
                js: [{haburbanas: ["_PATH_:bT_ES","_PATH_:validate", "_PATH_:bootstrapGrowlUI", "_PATH_:PBootbox"]}],
                css: {
                    libs: [],
                    package: [],
                    custom: []
                }
            },
            anuncios: {
                //Assets para un controllador especifico y todas sus acciones
                js: [{anuncios: ["_PATH_:bT_ES","_PATH_:validate", "_PATH_:bootstrapGrowlUI", "_PATH_:PBootbox"]}],
                css: {
                    libs: [],
                    package: [],
                    custom: []
                }
            },
            acteconomicas: {
                //Assets para un controllador especifico y todas sus acciones
                js: [{acteconomicas: ["_PATH_:bT_ES","_PATH_:validate", "_PATH_:bootstrapGrowlUI", "_PATH_:PBootbox"]}],
                css: {
                    libs: [],
                    package: [],
                    custom: []
                }
            },
            usos: {
                //Assets para un controllador especifico y todas sus acciones
                js: [{usos: ["_PATH_:bT_ES","_PATH_:validate", "_PATH_:bootstrapGrowlUI", "_PATH_:PBootbox"]}],
                css: {
                    libs: [],
                    package: [],
                    custom: []
                }
            },
            obrascomplementarias: {
                //Assets para un controllador especifico y todas sus acciones
                js: [{obrascomplementarias: ["_PATH_:bT_ES","_PATH_:validate", "_PATH_:bootstrapGrowlUI", "_PATH_:PBootbox"]}],
                css: {
                    libs: [],
                    package: [],
                    custom: []
                }
            },
             tecnico: {
                //Assets para un controllador especifico y todas sus acciones
                js: [{tecnico: ["_PATH_:bT_ES", "_PATH_:validate", "_PATH_:bootstrapGrowlUI", "_PATH_:PBootbox"]}],
                css: {
                    libs: [],
                    package: [],
                    custom: []
                }
            }
        },
        //Assets para todo el módulo, todos sus controlladores y todas sus acciones
        js: [],
        css: {
            libs: ['bootstrap-table/bootstrap-table.min', 'fancybox/source/jquery.fancybox', 'metronic/pages/css/search.min', 'bootstrap-select/css/bootstrap-select.min', 'jquery-multi-select/css/multi-select', 'select2/css/select2.min', 'select2/css/select2-bootstrap.min'],
            package: [],
            custom: ["mantenedor"]
        }
    }
};
