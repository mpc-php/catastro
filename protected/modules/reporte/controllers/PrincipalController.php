

<?php

/**
 * Clase que se encarga de controlar las acciones a ser utilizadas en el modulo Reportes
 *
 * @author Francisco Isasi Chiesa <franco16fra@gmail.com>
 * @package Catastro\Modules\Reporte\Controllers
 */
class PrincipalController extends Auth {

    /**
     * Accion que inicia el Index del Modulo
     */
    public function actionIndex() {
        $this->render('index');
    }

    /**
     * Accion que muestra la consulta de contribuyentes
     */
    public function actionConsultaContribuyente() {
        $data['titulo'] = "Consulta de Fichas por Contribuyente";
        $data['accion'] = "consultaContribuyente";
        $this->render('index', ["data" => $data]);
    }

    /**
     * Accion que muestra la Consulta por Ficha Catastral
     */
    public function actionConsultaFichaCatastral() {
        $data['titulo'] = "Consulta de Fichas Catastrales";
        $data['accion'] = "consultaFichaCatastral";
        $this->render('index', ["data" => $data]);
    }

    /**
     * Accion que muestra la Consulta por Direccion
     */
    public function actionConsultaDireccion() {
        $data['titulo'] = "Consulta de Fichas por Direccion";
        $data['accion'] = "consultaDireccion";
        $this->render('index', ["data" => $data]);
    }

    /**
     * Accion que muestra la data de una consulta especifica indicada en el tipo
     * 
     * @param String $tipo Tipo de consulta a ser ejecutada para obtener la data
     * @throws Exception
     */
    public function actionShow($tipo) {
        try {
            if (!Yii::app()->request->isAjaxRequest)
                throw new Exception("El metodo no esta permitido", 403);

            $data = [];
            if ($tipo == "consultacontribuyente") {
                $filtros['search'] = Yii::app()->request->getParam("search");
                $filtros['limit'] = Yii::app()->request->getParam("limit");
                $filtros['offset'] = Yii::app()->request->getParam("offset");
                $filtros['order'] = Yii::app()->request->getParam("order");
                $filtros['sort'] = Yii::app()->request->getParam("sort");
                $search_full = Yii::app()->request->getParam("search");
                $arreglo_search_full = explode("«", $search_full);
                $where = "";
                if (count($arreglo_search_full) == 5) {
                    for ($k = 0; $k < count($arreglo_search_full); $k++) {
                        if (trim($arreglo_search_full[$k]) != "") {
                            switch ($k) {
                                case 0:
                                    $columa_filter = "numDoc";
                                    break;
                                case 1:
                                    $columa_filter = "nombres";
                                    break;
                                case 2:
                                    $columa_filter = "ape_paterno";
                                    break;
                                case 3:
                                    $columa_filter = "ape_materno";
                                    break;
                                case 4:
                                    $columa_filter = "codContribRentas";
                                    break;
                            }
                            $where = $where . "{$columa_filter} LIKE '%{$arreglo_search_full[$k]}%' AND ";
                        }
                    }
                    $filtros['search'] = "";
                    $where = "AND " . substr($where, 0, count($where) - 5);
                }
                $columnas_key = [
                    'id_ficha' => 'id_ficha',
                    'code' => 'code',
                    'codLote' => 'codLote',
                    'tipDoc' => 'tipDoc',
                    'numDoc' => 'numDoc',
                    'name' => 'name',
                    'codContribRentas' => 'codContribRentas',
                    'codCatastral' => 'codCatastral'
                ];
                $columnas = ['id_ficha', 'code', 'codLote', 'tipDoc', 'numDoc', 'name', 'codContribRentas', 'codCatastral', 'nombres', 'ape_paterno', 'ape_materno'];
                $data = QUtils::generarPaginado("VW_REPORTE_FICHACONTRIBUYENTE", $columnas, "", $where, $filtros, "code", $columnas_key);
            } elseif ($tipo == "consultafichacatastral") {
                $filtros['search'] = "";
                $filtros['limit'] = Yii::app()->request->getParam("limit");
                $filtros['offset'] = Yii::app()->request->getParam("offset");
                $filtros['order'] = Yii::app()->request->getParam("order");
                $filtros['sort'] = Yii::app()->request->getParam("sort");
                $search_full = Yii::app()->request->getParam("search");
                if (trim($search_full) != "") {
                    $where = "AND ubicacion_concatenada = '{$search_full}'";
                } else {
                    $where = "";
                }
                $columnas_key = [
                    'id_ficha' => 'id_ficha',
                    'code' => 'code',
                    'tipFicha' => 'tipFicha',
                    'codCatastral' => 'codCatastral'
                ];
                $columnas = ['id_ficha', 'code', 'tipFicha', 'codCatastral', 'ubicacion_concatenada'];
                $data = QUtils::generarPaginado("VW_REPORTE_FICHACATASTRAL", $columnas, "", $where, $filtros, "code", $columnas_key);
            } elseif ($tipo == "consultadireccion") {
                $filtros['search'] = Yii::app()->request->getParam("search");
                $filtros['limit'] = Yii::app()->request->getParam("limit");
                $filtros['offset'] = Yii::app()->request->getParam("offset");
                $filtros['order'] = Yii::app()->request->getParam("order");
                $filtros['sort'] = Yii::app()->request->getParam("sort");
                $search_full = Yii::app()->request->getParam("search");
                $arreglo_search_full = explode("«", $search_full);
                $where = "";
                if (count($arreglo_search_full) == 8) {
                    for ($k = 0; $k < count($arreglo_search_full); $k++) {
                        if (trim($arreglo_search_full[$k]) != "") {
                            switch ($k) {
                                case 0:
                                    $columa_filter = "codVia";
                                    break;
                                case 1:
                                    $columa_filter = "nombreVia";
                                    break;
                                case 2:
                                    $columa_filter = "codHU";
                                    break;
                                case 3:
                                    $columa_filter = "nombreHabUrb";
                                    break;
                                case 4:
                                    $columa_filter = "mz";
                                    break;
                                case 5:
                                    $columa_filter = "lt";
                                    break;
                                case 6:
                                    $columa_filter = "nroMuni";
                                    break;
                                case 7:
                                    $columa_filter = "codPredRentas";
                                    break;
                            }
                            $where = $where . "{$columa_filter} LIKE '%{$arreglo_search_full[$k]}%' AND ";
                        }
                    }
                    $filtros['search'] = "";
                    $where = "AND " . substr($where, 0, count($where) - 5);
                }
                $columnas_key = [
                    'id_ficha' => 'id_ficha',
                    'code' => 'code',
                    'codLote' => 'codLote',
                    'codVia' => 'codVia',
                    'tipVia' => 'tipVia',
                    'nombreVia' => 'nombreVia',
                    'mz' => 'mz',
                    'lt' => 'lt',
                    'nroMuni' => 'nroMuni',
                    'tipPta' => 'tipPta',
                    'codHU' => 'codHU',
                    'nombreHabUrb' => 'nombreHabUrb',
                    'codPredRentas' => 'codPredRentas',
                    'codCatastral' => 'codCatastral',
                    'codContribRentas' => 'codContribRentas',
                    'uso' => 'uso',
                    'numDoc' => 'numDoc',
                    'name' => 'name'
                ];
                $columnas = ['id_ficha', 'code', 'codLote', 'codVia', 'tipVia', 'nombreVia', 'mz', 'lt', 'nroMuni', 'tipPta', 'codHU', 'nombreHabUrb', 'codPredRentas', 'codCatastral', 'codContribRentas', 'uso', 'numDoc', 'name'];
                $data =  QUtils::generarPaginado("VW_REPORTE_FICHADIRECCION", $columnas, "", $where, $filtros, "code", $columnas_key);
//                Utils::show(utf8_encode($data), true);
            } else {
                Utils::show("Error", true);
            }
            header('Content-type: application/json; charset=utf-8');
            http_response_code(200);

            JSON::response(FALSE, 200, "", $data);
        } catch (Exception $ex) {
            JSON::response(TRUE, $ex->getCode(), $ex->getMessage(), []);
        }
    }

    /**
     * Accion que exporta el reporte de la data de la consulta a un excel.
     */
    public function actionExportarReporte() {
        $tipo = Yii::app()->request->getParam("type");
        $search_full = Yii::app()->request->getParam("search_text");
        $arreglo_search_full = explode("«", $search_full);
        $cont = 0;
        $where = "";
        $data = [];
        $nombre_documento = "";
        $filtros['search'] = "";
        $filtros['where'] = $where;
        if ($tipo == "consultacontribuyente") {
            $nombre_documento = "REPORTE_CONSULTA_CONTRIBUYENTE_";
            $num_doc = $arreglo_search_full[0];
            if (trim($num_doc) != "") {
                $cont++;
                $search = $num_doc;
                $where = "code LIKE '%{$num_doc}%' AND ";
            }
            $nombre = $arreglo_search_full[1];
            if (trim($nombre) != "") {
                $cont++;
                $search = $nombre;
                $where = $where . "nombres LIKE '%{$nombre}%' AND ";
            }
            $apepaterno = $arreglo_search_full[2];
            if (trim($apepaterno) != "") {
                $cont++;
                $search = $apepaterno;
                $where = $where . "ape_paterno LIKE '%{$apepaterno}%' AND ";
            }
            $apematerno = $arreglo_search_full[3];
            if (trim($apematerno) != "") {
                $cont++;
                $search = $apematerno;
                $where = $where . "ape_materno LIKE '%{$apematerno}%' AND ";
            }
            $codcontribrenta = $arreglo_search_full[4];
            if (trim($codcontribrenta) != "") {
                $cont++;
                $search = $codcontribrenta;
                $where = $where . "codContribRentas LIKE '%{$codcontribrenta}%' AND ";
            }
            $filtros['search'] = "";
            $where = "AND " . substr($where, 0, count($where) - 5);
            $filtros['where'] = $where;
            $columnas = ['id_ficha', 'code', 'codLote', 'tipDoc', 'numDoc', 'name', 'codContribRentas', 'codCatastral', 'nombres', 'ape_paterno', 'ape_materno'];
            $data = QReporte::get_ConsultaContribuyente("VW_REPORTE_FICHACONTRIBUYENTE", $filtros, $columnas);
        } elseif ($tipo == "consultafichacatastral") {
            $nombre_documento = "REPORTE_CONSULTA_FICHA_CATASTRAL_";
            if (trim($search_full) != "") {
                $where = "AND ubicacion_concatenada = '{$search_full}'";
            } else {
                $where = "";
            }
            $filtros['search'] = "";
            $filtros['where'] = $where;
            $columnas = ['id_ficha', 'code', 'tipFicha', 'codCatastral', 'ubicacion_concatenada'];
            $data = QReporte::get_ConsultaContribuyente("VW_REPORTE_FICHACATASTRAL", $filtros, $columnas);
        } elseif ($tipo == "consultadireccion") {
            $nombre_documento = "REPORTE_CONSULTA_FICHA_DIRECCION_";
            $arreglo_search_full = explode("«", $search_full);
            $where = "";
            if (count($arreglo_search_full) == 8) {
                for ($k = 0; $k < count($arreglo_search_full); $k++) {
                    if (trim($arreglo_search_full[$k]) != "") {
                        switch ($k) {
                            case 0:
                                $columa_filter = "codVia";
                                break;
                            case 1:
                                $columa_filter = "nombreVia";
                                break;
                            case 2:
                                $columa_filter = "codHU";
                                break;
                            case 3:
                                $columa_filter = "nombreHabUrb";
                                break;
                            case 4:
                                $columa_filter = "mz";
                                break;
                            case 5:
                                $columa_filter = "lt";
                                break;
                            case 6:
                                $columa_filter = "nroMuni";
                                break;
                            case 7:
                                $columa_filter = "codPredRentas";
                                break;
                            case 8:
                                $columa_filter = "codCatastral";
                                break;
                            case 9:
                                $columa_filter = "codContribRentas";
                                break;
                            case 10:
                                $columa_filter = "uso";
                                break;
                            case 11:
                                $columa_filter = "numDoc";
                                break;
                            case 12:
                                $columa_filter = "name";
                                break;
                        }
                        $where = $where . "{$columa_filter} LIKE '%{$arreglo_search_full[$k]}%' AND ";
                    }
                }
                $filtros['search'] = "";
                $where = "AND " . substr($where, 0, count($where) - 5);
                $filtros['where'] = $where;
            }
            $columnas = ['id_ficha', 'code', 'codLote', 'codVia', 'tipVia', 'nombreVia', 'mz', 'lt', 'nroMuni', 'tipPta', 'codHU', 'nombreHabUrb', 'codPredRentas', 'codCatastral', 'codContribRentas', 'uso', 'numDoc', 'name'];
            $data = QReporte::get_ConsultaContribuyente("VW_REPORTE_FICHADIRECCION", $filtros, $columnas);
        }
        $ObjectExcel = libreriaExcel::initExcel();
        libreriaExcel::genera_reporte($ObjectExcel, $data, $tipo);
        libreriaExcel::guardar_archivo($ObjectExcel, $nombre_documento . date("Y-m-d"));
    }

    /**
     * Accion que muestra los datos de una ficha individual
     * 
     * @param String $id ID de la ficha a ser visualizada
     */
    public function actionVerFicha($id) {
        $modelFicha['general'] = QReporte::get_ConsultaFicha($id);
        if ($modelFicha['general']) {
            $modelFicha['cotitularidad'] = QReporte::get_ConsultaFichaCotitularidad($modelFicha['general']['cod_cat']);
            $modelFicha['economica'] = QReporte::get_ConsultaFichaEconomica($modelFicha['general']['cod_cat']);
            $modelFicha['biencomun'] = QReporte::get_ConsultaFichaBienComunCodCat($modelFicha['general']['cod_cat']);
        } else {
            $modelFicha['cotitularidad'] = "";
            $modelFicha['economica'] = "";
            $modelFicha['biencomun'] = "";
        }
        $modelFicha['ubicacion'] = QReporte::get_ConsultaFichaUbicacion($id);
        $modelFicha['titularidad'] = QReporte::get_ConsultaFichaTitularidad($id);
        $modelFicha['titular'] = QReporte::get_ConsultaFichaTitular($id);
        $modelFicha['descripcionPredio'] = QReporte::get_ConsultaFichaDescripcionPredio($id);
        $modelFicha['serviciosBasicos'] = QReporte::get_ConsultaFichaServiciosBasicos($id);
        $modelFicha['construccion'] = QReporte::get_ConsultaFichaConstruccion($id);
        $modelFicha['obra'] = QReporte::get_ConsultaFichaObra($id);
        $modelFicha['documentos'] = QReporte::get_ConsultaFichaDocumentos($id);
        $modelFicha['escritura'] = QReporte::get_ConsultaFichaEscritura($id);
        $modelFicha['sunarp'] = QReporte::get_ConsultaFichaSunarp($id);
        $modelFicha['complementaria'] = QReporte::get_ConsultaFichaComplementaria($id);
        $modelFicha['otros'] = QReporte::get_ConsultaFichaOtros($id);
        $modelFicha['digitador'] = QReporte::get_ConsultaFichaDigitador($id);
        $this->render('verFicha', ["data" => $modelFicha]);
    }

    /**
     * Accion que muestra los datos de una ficha de Actividades Economicas
     * 
     * @param String $id ID de la ficha a ser visualizada
     */
    public function actionVerFichaEconomica($id) {
        $modelFicha['general'] = QReporte::get_ConsultaFichaEconomicaID($id);
        $modelFicha['conductor'] = QReporte::get_ConsultaFichaEconomicaConductor($id);
        $modelFicha['otros'] = QReporte::get_ConsultaFichaOtros($id);
        $modelFicha['digitador'] = QReporte::get_ConsultaFichaDigitador($id);
        $domicilio_fiscal = QReporte::get_DomicilioFiscal($id);
        if ($domicilio_fiscal['id_ubi_geo'] == "070101") {
            $modelFicha['domicilio'] = QReporte::get_ConsultaFichaEconomicaCallao($id);
        } else {
            $modelFicha['domicilio'] = QReporte::get_ConsultaFichaEconomicaNoCallao($id);
        }
        $modelFicha['descripcionActividad'] = QReporte::get_ConsultaFichaEconomicaDescripcionActividad($id);
        $modelFicha['descripcionAnuncio'] = QReporte::get_ConsultaFichaEconomicaDescripcionAnuncio($id);
        $modelFicha['complementaria'] = QReporte::get_ConsultaFichaEconomicaComplementarias($id);
        $this->render('verFichaEconomica', ["data" => $modelFicha]);
    }

    /**
     * Accion que muestra los datos de una ficha de Cotitularidad
     * 
     * @param String $id ID de la ficha a ser visualizada
     */
    public function actionVerFichaCotitularidad($id) {
        $modelFicha['general'] = QReporte::get_ConsultaFichaCondominioID($id);
        $modelFicha['otros'] = QReporte::get_ConsultaFichaOtros($id);
        $modelFicha['digitador'] = QReporte::get_ConsultaFichaDigitador($id);
        $modelFicha['cotitular'] = QReporte::get_ConsultaFichaCondominioCotitular($id);
        $modelFicha['complementaria'] = QReporte::get_ConsultaFichaCondominioComplementarias($id);
        $this->render('verFichaCotitularidad', ["data" => $modelFicha]);
    }

    /**
     * Accion que muestra los datos de una ficha de Bien Comun
     * 
     * @param String $id ID de la ficha a ser visualizada
     */
    public function actionVerFichaBienComun($id) {
        $modelFicha['general'] = QReporte::get_ConsultaFichaBienComun($id);
        $modelFicha['ubicacion'] = QReporte::get_ConsultaFichaUbicacion($id);
        $modelFicha['descripcionPredio'] = QReporte::get_ConsultaFichaBienDescripcion($id);
        $modelFicha['serviciosBasicos'] = QReporte::get_ConsultaFichaServiciosBasicos($id);
        $modelFicha['construccion'] = QReporte::get_ConsultaFichaConstruccion($id);
        $modelFicha['obra'] = QReporte::get_ConsultaFichaObra($id);
        $modelFicha['recapitulacionEdificio'] = QReporte::get_ConsultaFichaBienRecapitulacionEdificio($id);
        $modelFicha['recapitulacionBien'] = QReporte::get_ConsultaFichaBienRecapitulacionBien($id);
        $modelFicha['areaInvadida'] = QReporte::get_ConsultaFichaBienAreaInvadida($id);
        $modelFicha['notarial'] = QReporte::get_ConsultaFichaBienNotarial($id);
        $modelFicha['sunarp'] = QReporte::get_ConsultaFichaBienSunarp($id);
        $modelFicha['complementaria'] = QReporte::get_ConsultaFichaBienComplementaria($id);

        $modelFicha['otros'] = QReporte::get_ConsultaFichaOtros($id);
        $modelFicha['digitador'] = QReporte::get_ConsultaFichaDigitador($id);
        $this->render('verFichaBienComun', ["data" => $modelFicha]);
    }

    /**
     * Accion que muestra los datos de un titular que es un fragmento de una ficha.
     * 
     */
    public function actionVerTitular() {

        $id_ficha = Yii::app()->request->getParam("id_ficha");
        $id_persona = Yii::app()->request->getParam("id_persona");

        $modelFicha['titular'] = QReporte::get_ConsultaFichaTitularPersona($id_ficha, $id_persona);
        $modelFicha['exoneracion'] = QReporte::get_ConsultaFichaTitularPersonaExoneraciones($id_ficha, $id_persona);
        $domicilio_fiscal = QReporte::get_DomicilioFiscal($id_ficha);
        if ($domicilio_fiscal['id_ubi_geo'] == "070101") {
            $modelFicha['domicilio'] = QReporte::get_ConsultaFichaEconomicaCallao($id_ficha);
        } else {
            $modelFicha['domicilio'] = QReporte::get_ConsultaFichaEconomicaNoCallao($id_ficha);
        }
        $data['html'] = $this->renderPartial("partials/_verTitular", ['data' => $modelFicha], TRUE);

        JSON::response(FALSE, 200, "", $data);
    }

}
