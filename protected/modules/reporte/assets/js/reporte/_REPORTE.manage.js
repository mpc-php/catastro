REPORTE.manage = (function (_win, $) {

    'use strict';


    var $md_manage = $('section#md-manage'),
            $frm_manage = $md_manage.find("form#frm-manage"),
            $btn_manage = $('button#btn-manage'),
            $bb_manage = {};

    var _bind = function () {
    },
            /**
             * EVENTO DE LA ACCIÓN MODIFICAR QUE ABRE UN MODAL 
             * CON EL FORMULARIO PARA EL MANEJO DE LA DATA
             * @param  {object} 	e     	Instancia del evento
             * @param  {string} 	value 	Valor que contiene el boton
             * @param  {object} 	row   	objeto que contiene toda la información de la fila
             * @param  {int} 		index 	Posición de la fila
             * @return {void}
             */
            _open_modal_ficha = function (e, value, row, index) {

            };
    /**
     * iNICIA LAS CONFIGURACIONES BASICAR DEL FORMULARIO (EVENTOS, VALIDACIONES, DESACTIVACION)
     * @return {void} 
     */

    return {
        init: _bind,
        viewFicha: _open_modal_ficha
    };

}(window, window.jQuery));