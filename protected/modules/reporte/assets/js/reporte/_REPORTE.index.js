var REPORTE = {};

REPORTE.base_url = Request.BaseUrl + '/' + Request.UrlHash.m + '/';

REPORTE.bootTable = {
    escape: false,
    locale: 'es-SP',
    search: true,
    pagination: true,
    pageSize: 25,
    idField: "code"
};

REPORTE.growl = {
    ele: 'body',
    type: 'success',
    offset: {from: 'top', amount: 80},
    align: 'center',
    width: 'auto',
    delay: 2500,
    allow_dismiss: true,
    stackup_spacing: 10
};

REPORTE.index = (function (_win, $) {

    'use strict';

    var $table = $('table#tdreporte'),
            $cnt_table = $(".search-table"),
            $input_search_gen = $('input#buscador_generico'),
            $btn_search_gen = $('button#buscador_generico_btn'),
            $btn_export = $('button#btn-export'),
            $cbo_export = $('select#cboExportarCantidad'),
            $btn_pop = $('.domfis'),
            $md_manage = $('section#md-manage'),
            $bb_manage = {};

    var _bind = function () {

        _build_table();
        _export_data();
        _open_modal();

    },
            /**
             * bOTONES DE ACCION PARA CADA FILA
             * @param  {string} value 'valor que tendrá el boton'
             * @param  {object} row   Objeto que contiene todos los valores de la fila
             * @param  {int} index Posición de la fila
             * @return {string}       Cadena de botones para cada fila
             */
            _action_buttons = function (value, row, index) {
                return [
                    '<a target="_blank" href="verFicha/id/' + row.id_ficha.trim() + '" id="btn-edit-' + row.id_ficha.trim() + '" class="" data-id="' + row.code + '" data-accion="viewFicha" title="Ver datos de ficha">',
                    row.code,
                    '</a>'
                ].join('');
            },
            /**
             * cONSTRULLE TABLA PRINCIPAL(CABECERA, CUERPO), ATRAVEZ DE UNA CONSULTA JSON
             * @return {VOID}
             */
            _build_table = function () {


                var tableParams = $.extend(true, {}, REPORTE.bootTable);

                tableParams.url = REPORTE.base_url + 'principal/show/tipo/' + Request.UrlHash.a;
                tableParams.sidePagination = "server";

                //TABLE HEADER
                if (Request.UrlHash.a == "consultacontribuyente") {
                    tableParams.columns = [
                        {
                            field: 'code',
                            title: 'Nro. Ficha',
                            align: 'center',
                            sortable: true,
                            formatter: _action_buttons,
                            events: {'click': REPORTE.manage.viewFicha}},
                        {field: 'codLote', title: 'Cod. Lote', align: 'left', sortable: true},
                        {field: 'tipDoc', title: 'Tip. Doc.', align: 'left', sortable: true},
                        {field: 'numDoc', title: 'N° Doc.', align: 'left', sortable: true},
                        {field: 'name', title: 'Apellidos y Nombres', align: 'left', sortable: true},
                        {field: 'codContribRentas', title: 'Cod. Contrib. Rentas', align: 'left', sortable: true},
                        {field: 'codCatastral', title: 'Cod. Catastral', align: 'left', sortable: true},
                    ];
                } else if (Request.UrlHash.a == "consultafichacatastral") {
                    tableParams.columns = [
                        {
                            field: 'code',
                            title: 'Nro. Ficha',
                            align: 'center',
                            sortable: true,
                            formatter: _action_buttons,
                            events: {'click': REPORTE.manage.viewFicha}},
                        {field: 'tipFicha', title: 'Tip. Ficha', align: 'left', sortable: true},
                        {field: 'codCatastral', title: 'Cod. Catastral', align: 'left', sortable: true},
                    ];
                } else if (Request.UrlHash.a == "consultadireccion") {
                    tableParams.columns = [
                        {
                            field: 'code',
                            title: 'Nro. Ficha',
                            align: 'center',
                            sortable: true,
                            formatter: _action_buttons,
                            events: {'click': REPORTE.manage.viewFicha}},
                        {field: 'codLote', title: 'Cod. Lote', align: 'left', sortable: true},
                        {field: 'codVia', title: 'Cod. Via', align: 'left', sortable: true},
                        {field: 'tipVia', title: 'Tip. Via', align: 'left', sortable: true},
                        {field: 'nombreVia', title: 'Nombre Via', align: 'left', sortable: true},
                        {field: 'mz', title: 'Mz.', align: 'left', sortable: true},
                        {field: 'lt', title: 'Lt.', align: 'left', sortable: true},
                        {field: 'nroMuni', title: 'N° Muni.', align: 'left', sortable: true},
                        {field: 'tipPta', title: 'Tip. Pta', align: 'left', sortable: true},
                        {field: 'codHU', title: 'Cod. H.U.', align: 'left', sortable: true},
                        {field: 'nombreHabUrb', title: 'Nombre Hab. Urb.', align: 'left', sortable: true},
                        {field: 'codPredRentas', title: 'Cod. Pred. Rentas', align: 'left', sortable: true},
                        {field: 'codCatastral', title: 'Cod. Catastral', align: 'left', sortable: true},
                        {field: 'codContribRentas', title: 'Cod. Contri.', align: 'left', sortable: true},
                        {field: 'uso', title: 'Uso', align: 'left', sortable: true},
                        {field: 'numDoc', title: 'D.N.I', align: 'left', sortable: true},
                        {field: 'name', title: 'Nom. Ape.', align: 'left', sortable: true}
                    ];
                }

                $table.bootstrapTable(tableParams);

                //CUSTOM SEARCH
                $input_search_gen.on("keyup", function () {
                    $cnt_table.find('.search input').val(this.value).trigger("keyup");
                });

                $btn_search_gen.on("click", function () {
                    var search_full = "";
                    if (Request.UrlHash.a == "consultacontribuyente") {
                        var filtro_documento = $("#flt_documento").val();
                        var filtro_nombre = $("#flt_nombres").val();
                        var filtro_apepaterno = $("#flt_apepaterno").val();
                        var filtro_apematerno = $("#flt_apematerno").val();
                        var filtro_codcontribrenta = $("#flt_codcontribrenta").val();
                        search_full = filtro_documento + "«" + filtro_nombre + "«" + filtro_apepaterno + "«" + filtro_apematerno + "«" + filtro_codcontribrenta;
                    } else if (Request.UrlHash.a == "consultafichacatastral") {
                        var filtro_sector = $("#flt_sector").val();
                        var filtro_manzana = $("#flt_manzana").val();
                        var filtro_lote = $("#flt_lote").val();
                        search_full = filtro_sector + filtro_manzana + filtro_lote;
                    } else if (Request.UrlHash.a == "consultadireccion") {
                        var flt_codvia = $("#flt_codvia").val();
                        var flt_nombrevia = $("#flt_nombrevia").val();
                        var flt_codhu = $("#flt_codhu").val();
                        var flt_nombrehaburb = $("#flt_nombrehaburb").val();
                        var flt_manzana = $("#flt_manzana").val();
                        var flt_lote = $("#flt_lote").val();
                        var flt_nromunic = $("#flt_nromunic").val();
                        var flt_codpredialrentas = $("#flt_codpredialrentas").val();
                        search_full = flt_codvia + "«" + flt_nombrevia + "«" + flt_codhu + "«" + flt_nombrehaburb + "«" + flt_manzana + "«" + flt_lote + "«" + flt_nromunic + "«" + flt_codpredialrentas;
                    }
                    $input_search_gen.val(search_full);
                    $cnt_table.find('.search input').val($input_search_gen.val()).trigger("keyup");
                });
            },
            _export_data = function () {
                $btn_export.on("click", function () {
                    var search_full = "";
                    if (Request.UrlHash.a == "consultacontribuyente") {
                        var filtro_documento = $("#flt_documento").val();
                        var filtro_nombre = $("#flt_nombres").val();
                        var filtro_apepaterno = $("#flt_apepaterno").val();
                        var filtro_apematerno = $("#flt_apematerno").val();
                        var filtro_codcontribrenta = $("#flt_codcontribrenta").val();
                        search_full = filtro_documento + "«" + filtro_nombre + "«" + filtro_apepaterno + "«" + filtro_apematerno + "«" + filtro_codcontribrenta;
                    } else if (Request.UrlHash.a == "consultafichacatastral") {
                        var filtro_sector = $("#flt_sector").val();
                        var filtro_manzana = $("#flt_manzana").val();
                        var filtro_lote = $("#flt_lote").val();
                        search_full = filtro_sector + filtro_manzana + filtro_lote;
                    } else if (Request.UrlHash.a == "consultadireccion") {
                        var flt_codvia = $("#flt_codvia").val();
                        var flt_nombrevia = $("#flt_nombrevia").val();
                        var flt_codhu = $("#flt_codhu").val();
                        var flt_nombrehaburb = $("#flt_nombrehaburb").val();
                        var flt_manzana = $("#flt_manzana").val();
                        var flt_lote = $("#flt_lote").val();
                        var flt_nromunic = $("#flt_nromunic").val();
                        var flt_codpredialrentas = $("#flt_codpredialrentas").val();
                        search_full = flt_codvia + "«" + flt_nombrevia + "«" + flt_codhu + "«" + flt_nombrehaburb + "«" + flt_manzana + "«" + flt_lote + "«" + flt_nromunic + "«" + flt_codpredialrentas;
                    }
                    $input_search_gen.val(search_full);
                    location.href = REPORTE.base_url + Request.UrlHash.c + '/exportarReporte?search_text=' + $input_search_gen.val() + "&type=" + Request.UrlHash.a;
//                    var data = {
//                        search_text: $input_search_gen.val(),
//                        export_count: $cbo_export.find(":selected").val(),
//                        type: Request.UrlHash.a
//                    };
//                    $.get(REPORTE.base_url + Request.UrlHash.c + '/exportarReporte', data, function (reponse) {
//                        if (reponse.error) {
//                        } else {
//                        }
//                    });
                });
            },
            _open_modal = function () {
                $btn_pop.on("click", function () {
                    var data = {
                        id_ficha: $(this).attr("data-id-ficha"),
                        id_persona: $(this).attr("data-id-persona")
                    };
                    $.post(REPORTE.base_url + Request.UrlHash.c + '/verTitular', data, function (response) {
                        if (!response.error) {
                            $bb_manage = $md_manage.cBootbox({
                                title: "Información del Titular",
                                size: "large",
                                btnClose: '#btn-close',
                                beforeOpen: function () {
                                    $md_manage.attr("display", "block");
                                    $md_manage.find("#contenedor_datos").html(response.html);
                                },
                                afterClose: _reset_frm_manage
                            });
                        } else {
                        }
                    });
                });
            },
            _reset_frm_manage = function () {
                $md_manage.find("#contenedor_datos").html("");
                $md_manage.attr("display", "none");
            };

    return {
        init: _bind,
        table: {
            refresh: function () {
                $table.bootstrapTable('refresh');
            }
        }
    };

}(window, window.jQuery));