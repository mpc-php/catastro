/**
 * Configuración personalizada para la carga de librerias del módulo
 * 
 * @type Object
 */
var Builder = {
    module: {
        controllers: {
            principal: {                
                //Assets para un controllador especifico y todas sus acciones
                js: [{reporte: ["_PATH_:jq_export", "_PATH_:bootstrapSelect", "_PATH_:bootstrapGrowlUI", "_PATH_:PBootbox", "_PATH_:lightbox"]}],
                css: {
                    libs: ['lightbox/src/css/lightbox'],
                    package: [],
                    custom: []
                }
            }
        },
        //Assets para todo el módulo, todos sus controlladores y todas sus acciones
        js: [],
        css: {
            libs: ['bootstrap-table/bootstrap-table.min', 'fancybox/source/jquery.fancybox', 'metronic/pages/css/search.min', 'bootstrap-select/css/bootstrap-select.min', 'jquery-multi-select/css/multi-select', 'select2/css/select2.min', 'select2/css/select2-bootstrap.min'],
            package: [],
            custom: []
        }
    }
};
