<?php
//Utils::show($data,true);
$cod_lote       = $data['general']['cod_lote'];
$id_ficha       = $data['general']['id_ficha'];
$nro_ficha      = $data['general']['nro_ficha'];
$tip_ficha      = $data['general']['tip_ficha'];
$cuc            = $data['general']['cuc'];
$cod_hoja_cat   = $data['general']['cod_hoja_cat'];
$nro_ficha_lote = $data['general']['nro_ficha_lote'];
$cod_cat        = $data['general']['cod_cat'];

$cod_hab_urb     = $data['general']['cod_hab_urb'];
$mza_urb         = $data['general']['mza_urb'];
$lote_dist       = $data['general']['lote_dist'];
$sub_lote        = $data['general']['sublote'];
$des_hab_urb     = $data['general']['des_hab_urb'];
$des_zn_sc_etapa = $data['general']['des_zn_sc_etapa'];

$clasificacion        = $data['descripcionPredio']['clasificacion'];
$predio_catastrado_en = $data['descripcionPredio']['predio_catastrado_en'];

$cod_uso  = $data['descripcionPredio']['cod_uso'];
$desc_uso = $data['descripcionPredio']['desc_uso'];

$area_titulo     = $data['descripcionPredio']['area_titulo'];
$area_verificada = $data['descripcionPredio']['area_verificada'];
$estructuracion  = $data['descripcionPredio']['estructuracion'];
$zonificacion    = $data['descripcionPredio']['zonificacion'];

$fre_med_campo = $data['descripcionPredio']['fre_med_campo'];
$der_med_campo = $data['descripcionPredio']['der_med_campo'];
$izq_med_campo = $data['descripcionPredio']['izq_med_campo'];
$fon_med_campo = $data['descripcionPredio']['fon_med_campo'];


$fre_med_titulo = $data['descripcionPredio']['fre_med_titulo'];
$der_med_titulo = $data['descripcionPredio']['der_med_titulo'];
$izq_med_titulo = $data['descripcionPredio']['izq_med_titulo'];
$fon_med_titulo = $data['descripcionPredio']['fon_med_titulo'];


$fre_col_campo = $data['descripcionPredio']['fre_col_campo'];
$der_col_campo = $data['descripcionPredio']['der_col_campo'];
$izq_col_campo = $data['descripcionPredio']['izq_col_campo'];
$fon_col_campo = $data['descripcionPredio']['fon_col_campo'];

$fre_col_titulo  = $data['descripcionPredio']['fre_col_titulo'];
$der_col_titulo  = $data['descripcionPredio']['der_col_titulo'];
$izq_col_titulo  = $data['descripcionPredio']['izq_col_titulo'];
$fon_col_titulo  = $data['descripcionPredio']['fon_col_titulo'];
$id_lote         = $data['descripcionPredio']['id_lote'];
$des_clasific    = $data['descripcionPredio']['des_clasific'];
$des_pred_cat_en = $data['descripcionPredio']['des_pred_cat_en'];
$des_zona        = $data['descripcionPredio']['des_zona'];

$fre_lim_fre_lot = $data['descripcionPredio']['fre_lim_fre_lot'];
$der_lim_fre_lot = $data['descripcionPredio']['der_lim_fre_lot'];
$izq_lim_fre_lot = $data['descripcionPredio']['izq_lim_fre_lot'];
$fon_lim_fre_lot = $data['descripcionPredio']['fon_lim_fre_lot'];
$tot_lim_fre_uc  = $data['descripcionPredio']['tot_lim_fre_uc'];

$luz      = $data['serviciosBasicos']['luz'];
$agua     = $data['serviciosBasicos']['agua'];
$telefono = $data['serviciosBasicos']['telefono'];


$desague           = $data['serviciosBasicos']['desague'];
$nro_sum_luz       = $data['serviciosBasicos']['nro_sum_luz'];
$nro_contrato_agua = $data['serviciosBasicos']['nro_contrato_agua'];
$nro_telefono      = $data['serviciosBasicos']['nro_telefono'];

$en_lote_colindante = $data['areaInvadida']['en_lote_colindante'];
$en_jardin_aislam   = $data['areaInvadida']['en_jardin_aislam'];
$en_area_publica    = $data['areaInvadida']['en_area_publica'];
$en_area_intang     = $data['areaInvadida']['en_area_intang'];

$kardex          = $data['notarial']['kardex'];
$fecha_escritura = $data['notarial']['fecha_escritura'];
$des_notaria     = $data['notarial']['des_notaria'];

$tipo_part_reg     = $data['sunarp']['tipo_part_reg'];
$numero            = $data['sunarp']['numero'];
$fojas             = $data['sunarp']['fojas'];
$asiento           = $data['sunarp']['asiento'];
$fecha_inscripcion = $data['sunarp']['fecha_inscripcion'];
$declar_fab        = $data['sunarp']['declar_fab'];
$asien_inscrip_fab = $data['sunarp']['asien_inscrip_fab'];
$fecha_inscrip_fab = $data['sunarp']['fecha_inscrip_fab'];
$des_tipo_part     = $data['sunarp']['des_tipo_part'];
$des_declar_fab    = $data['sunarp']['des_declar_fab'];

$condic_declara   = $data['complementaria']['condic_declara'];
$estado_llenado   = $data['complementaria']['estado_llenado'];
$observaciones    = $data['complementaria']['observaciones'];
$des_cond_declara = $data['complementaria']['des_cond_declara'];
$des_est_llenado  = $data['complementaria']['des_est_llenado'];

$firma_declarante      = $data['otros']['firma_declarante'];
$id_declarante         = $data['otros']['id_declarante'];
$fecha_declarante      = $data['otros']['fecha_declarante'];
$firma_supervisor      = $data['otros']['firma_supervisor'];
$id_supervisor         = $data['otros']['id_supervisor'];
$fecha_supervisor      = $data['otros']['fecha_supervisor'];
$firma_tec_catastral   = $data['otros']['firma_tec_catastral'];
$id_tec_catastral      = $data['otros']['id_tec_catastral'];
$fecha_tec_catastral   = $data['otros']['fecha_tec_catastral'];
$firma_verif_catastral = $data['otros']['firma_verif_catastral'];

$id_verif_catastral    = $data['otros']['id_verif_catastral'];
$fecha_verif_catastral = $data['otros']['fecha_verif_catastral'];

$nom_declarante      = $data['otros']['nom_declarante'];
$nom_supervisor      = $data['otros']['nom_supervisor'];
$nom_tec_catastral   = $data['otros']['nom_tec_catastral'];
$nom_verif_catastral = $data['otros']['nom_verif_catastral'];

$firma_tec_calidad = $data['otros']['firma_tec_calidad'];
$id_tec_calidad    = $data['otros']['id_tec_calidad'];
$fecha_tec_calidad = $data['otros']['fecha_tec_calidad'];
$nom_tec_calidad   = $data['otros']['nom_tec_calidad'];

$insert_digitador = $data['digitador']['insert_digitador'];
$insert_fecha     = $data['digitador']['fec_in'];
$update_digitador = $data['digitador']['update_digitador'];
$update_fecha     = $data['digitador']['fec_up'];
$digi_in          = $data['digitador']['digi_in'];
$digi_up          = $data['digitador']['digi_up'];
?>
<div class="page-head">
    <div class="container">
        <div class="page-title">
            <h4 class="h4">FICHA CATASTRAL BIENES COMUNES N° <?= $nro_ficha ?></h4>
        </div>
        <div class="page-toolbar">
            <div class="btn-group btn-theme-panel">
                <a href="<?= Yii::app()->request->urlReferrer ?>" class="btn btn-primary" >
                    Regresar
                </a>
            </div>
        </div>
    </div>
</div>
<div class="page-content">
    <div class="container">
        <!-- BEGIN PAGE CONTENT INNER -->
        <div class="page-content-inner">
            <div class="bordered">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="portlet light">
                            <div class="portlet-title">
                                <h4 class="h4">CODIGO DE REFERENCIA CATASTRAL</h4>
                            </div>
                            <div class="portlet-body">
                                <div class="table-responsive">
                                    <table class="table table-bordered table-condensed">
                                        <thead class="bg-gray">
                                            <tr>
                                                <th>UBIGEO</th>
                                                <th>SECTOR</th>
                                                <th>MANZANA</th>
                                                <th>LOTE</th>
                                                <th>EDIF</th>
                                                <th>ENTRADA</th>
                                                <th>PISO</th>
                                                <th>UNIDAD</th>
                                                <th>DC</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td class="bg-white"><?= substr($cod_cat, 0, 6); ?></td>
                                                <td class="bg-white"><?= substr($cod_cat, 10, 2); ?></td>
                                                <td class="bg-white"><?= substr($cod_cat, 12, 3); ?></td>
                                                <td class="bg-white"><?= substr($cod_cat, 15, 3); ?></td>
                                                <td class="bg-white"><?= substr($cod_cat, 18, 2); ?></td>
                                                <td class="bg-white"><?= substr($cod_cat, 20, 2); ?></td>
                                                <td class="bg-white"><?= substr($cod_cat, 22, 2); ?></td>
                                                <td class="bg-white"><?= substr($cod_cat, 24, 3); ?></td>
                                                <td class="bg-white"><?= UReporte::dig_check($cod_cat); ?></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="table-responsive">
                                    <table class="table table-bordered table-condensed">
                                        <thead class="bg-gray">
                                            <tr>
                                                <th colspan="2" class="bg-gray">CODIGO UNICO CATASTRAL - CUC</th>
                                                <th>CODIGO HOJA CATASTRAL</th>
                                                <th colspan="2" class="bg-gray">N° FICHA POR LOTE</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td class="bg-white"><?= substr($cuc, 0, 8); ?></td>
                                                <td class="bg-white"><?= substr($cuc, 8, 4); ?></td>
                                                <td class="bg-white"><?= $cod_hoja_cat; ?></td>
                                                <td class="bg-white"><?= substr($nro_ficha_lote, 0, 4); ?></td>
                                                <td class="bg-white"><?= substr($nro_ficha_lote, 5, 4); ?></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12">
                        <div class="portlet light">
                            <div class="portlet-title">
                                <h4 class="h4">UBICACION DEL PREDIO</h4>
                            </div>
                            <div class="portlet-body">
                                <div class="table-responsive">
                                    <table class="table table-bordered table-condensed">
                                        <thead class="bg-gray">
                                            <tr>
                                                <th>NRO</th>
                                                <th>CODIGO DE VIA</th>
                                                <th>TIPO DE VIA</th>
                                                <th>NOMBRE DE VIA</th>
                                                <th>TIPO DE PUERTA</th>
                                                <th>N° MUNICIPAL</th>
                                                <th>COND. NUMER</th>
                                                <th>N° CERTIFICADO</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            foreach ($data['ubicacion'] as $ubicacion):
                                                $id                 = $ubicacion['id'];
                                                $cod_via            = $ubicacion['cod_via'];
                                                $tipo_puerta        = $ubicacion['tipo_puerta'];
                                                $nro_municipal      = $ubicacion['nro_municipal'];
                                                $cond_numeracion    = $ubicacion['cond_numeracion'];
                                                $nro_certif_numerac = $ubicacion['nro_certif_numerac'];
                                                $tip_via            = $ubicacion['tip_via'];
                                                $nom_via            = $ubicacion['nom_via'];
                                                $desc_tip_via       = $ubicacion['desc_tabla_detalle'];
                                                $desc_tip_pta       = $ubicacion['des_puerta'];
                                                ?>
                                                <tr>
                                                    <td class="bg-white"><?= $id; ?></td>
                                                    <td class="bg-white"><?= $cod_via; ?></td>
                                                    <td class="bg-white"><?= $desc_tip_via; ?></td>
                                                    <td class="bg-white"><?= $nom_via; ?></td>
                                                    <td class="bg-white"><?= $desc_tip_pta; ?></td>
                                                    <td class="bg-white"><?= $nro_municipal; ?></td>
                                                    <td class="bg-white"><?= $cond_numeracion; ?></td>
                                                    <td class="bg-white"><?= $nro_certif_numerac; ?></td>
                                                </tr>
                                            <?php endforeach; ?>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="table-responsive">
                                    <table class="table table-bordered table-condensed">
                                        <thead class="bg-gray">
                                            <tr>
                                                <th>NOMBRE DE EDIFICACION</th>
                                                <th>TIPO DE EDIFICACION</th>
                                                <th>TIPO INTERIOR</th>
                                                <th>N° INTERIOR</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td class="bg-white">&nbsp;</td>
                                                <td class="bg-white">&nbsp;</td>
                                                <td class="bg-white">&nbsp;</td>
                                                <td class="bg-white">&nbsp;</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="table-responsive">
                                    <table class="table table-bordered table-condensed">
                                        <thead class="bg-gray">
                                            <tr>
                                                <th>CODIGO HU</th>
                                                <th>NOMBRE DE HAB. URBANA</th>
                                                <th>ZONA/SECTOR/ETAPA</th>
                                                <th>MANZANA</th>
                                                <th>LOTE</th>
                                                <th>SUB-LOTE</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td class="bg-white"><?= $cod_hab_urb; ?></td>
                                                <td class="bg-white"><?= $des_hab_urb; ?></td>
                                                <td class="bg-white"><?= $des_zn_sc_etapa; ?></td>
                                                <td class="bg-white"><?= $mza_urb; ?></td>
                                                <td class="bg-white"><?= $lote_dist; ?></td>
                                                <td class="bg-white"><?= $sub_lote; ?></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12">
                        <div class="portlet light">
                            <div class="portlet-title">
                                <h4 class="h4">DESCRIPCION DEL PREDIO</h4>
                            </div>
                            <div class="portlet-body">
                                <div class="table-responsive">
                                    <table class="table table-bordered table-condensed">
                                        <thead class="bg-gray">
                                            <tr>
                                                <th>CLASIFICACION DEL PREDIO</th>
                                                <th>PREDIO CATASTRADO EN</th>
                                                <th>COD. USO</th>
                                                <th>USO DEL PREDIO CATASTRADO</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td class="bg-white"><?= $clasificacion . ' ' . $des_clasific; ?></td>
                                                <td class="bg-white"><?= $predio_catastrado_en . ' ' . $des_pred_cat_en; ?></td>
                                                <td class="bg-white"><?= $cod_uso ?></td>
                                                <td class="bg-white"><?= $desc_uso; ?></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="table-responsive">
                                    <table class="table table-bordered table-condensed">
                                        <thead class="bg-gray">
                                            <tr>
                                                <th>ESTRUCTURACION</th>
                                                <th colspan="2" class="bg-gray">ZONIFICACION</th>
                                                <th>AT. TITULO (M2)</th>
                                                <th>AT. DECLAR (M2)</th>
                                                <th>AT. VERIF (M2)</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td class="bg-white"><?= $estructuracion; ?></td>
                                                <td class="bg-white"><?= $zonificacion; ?></td>
                                                <td class="bg-white"><?= $des_zona; ?></td>
                                                <td class="bg-white"><?= $area_titulo ?></td>
                                                <td class="bg-white">&nbsp;</td>
                                                <td class="bg-white"><?= $area_verificada ?></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="table-responsive">
                                    <table class="table table-bordered table-condensed">
                                        <thead class="bg-gray">
                                            <tr>
                                                <th>LINDEROS DEL LOTE</th>
                                                <th>MEDIDAS DE CAMPO</th>
                                                <th>MEDIDAS SEGUN TITULO</th>
                                                <th>COLINDANCIAS EN CAMPO</th>
                                                <th>COLINDANCIAS SEGUN TIT.</th>
                                                <th>LIMPIEZA FRENTE DE LOTE (ML)</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td class="bg-white">FRENTE</td>
                                                <td class="bg-white"><?= $fre_med_campo; ?></td>
                                                <td class="bg-white"><?= $fre_med_titulo; ?></td>
                                                <td class="bg-white"><?= $fre_col_campo; ?></td>
                                                <td class="bg-white"><?= $fre_col_titulo ?></td>
                                                <td class="bg-white"><?= $fre_lim_fre_lot ?></td>
                                            </tr>
                                            <tr>
                                                <td class="bg-white">DERECHA</td>
                                                <td class="bg-white"><?= $der_med_campo; ?></td>
                                                <td class="bg-white"><?= $der_med_titulo; ?></td>
                                                <td class="bg-white"><?= $der_col_campo; ?></td>
                                                <td class="bg-white"><?= $der_col_titulo ?></td>
                                                <td class="bg-white"><?= $der_lim_fre_lot ?></td>
                                            </tr>
                                            <tr>
                                                <td class="bg-white">IZQUIERDA</td>
                                                <td class="bg-white"><?= $izq_med_campo; ?></td>
                                                <td class="bg-white"><?= $izq_med_titulo; ?></td>
                                                <td class="bg-white"><?= $izq_col_campo; ?></td>
                                                <td class="bg-white"><?= $izq_col_titulo ?></td>
                                                <td class="bg-white"><?= $izq_lim_fre_lot ?></td>
                                            </tr>
                                            <tr>
                                                <td class="bg-white">FONDO</td>
                                                <td class="bg-white"><?= $fon_med_campo; ?></td>
                                                <td class="bg-white"><?= $fon_med_titulo; ?></td>
                                                <td class="bg-white"><?= $fon_col_campo; ?></td>
                                                <td class="bg-white"><?= $fon_col_titulo ?></td>
                                                <td class="bg-white"><?= $fon_lim_fre_lot ?></td>
                                            </tr>
                                            <tr>
                                                <td colspan="3" class="bg-gray">&nbsp;</td>
                                                <td colspan="2" class="bg-gray">LIMPIEZA DE FRENTE DE LOTE DE LA UC (ML)</td>
                                                <td class="bg-white"><?= $tot_lim_fre_uc ?></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="table-responsive">
                                    <table class="table table-bordered table-condensed">
                                        <thead class="bg-gray">
                                            <tr>
                                                <th colspan="10" class="bg-gray">SERVICIOS BASICOS</th>
                                            </tr>
                                            <tr>
                                                <th>LUZ</th>
                                                <th>N° SUM. LUZ</th>
                                                <th>AGUA</th>
                                                <th>N° SUM. AGUA</th>
                                                <th>TELEFONO</th>
                                                <th>N° SUM. TELEF.</th>
                                                <th>DESAGUE</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td class="bg-white"><input name="luz" type="checkbox" disabled="disabled" <?= ($luz == 1) ? "checked" : "" ?>/></td>
                                                <td class="bg-white"><?= $nro_sum_luz ?></td>
                                                <td class="bg-white"><input name="agua" type="checkbox" disabled="disabled" <?= ($agua == 1) ? "checked" : "" ?>/></td>
                                                <td class="bg-white"><?= $nro_contrato_agua ?></td>
                                                <td class="bg-white"><input name="telefono" type="checkbox" disabled="disabled" <?= ($telefono == 1) ? "checked" : "" ?>/></td>
                                                <td class="bg-white"><?= $nro_telefono ?></td>
                                                <td class="bg-white"><input name="desague" type="checkbox" disabled="disabled" <?= ($desague == 1) ? "checked" : "" ?>/></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12">
                        <div class="portlet light">
                            <div class="portlet-title">
                                <h4 class="h4">CONSTRUCCIONES</h4>
                            </div>
                            <div class="portlet-body">
                                <div class="table-responsive">
                                    <table class="table table-bordered table-condensed">
                                        <thead class="bg-gray">
                                            <tr>
                                                <th>Nº</th>
                                                <th>NIVEL</th>
                                                <th>MES</th>
                                                <th>AÑO</th>
                                                <th>MEP</th>
                                                <th>ECS</th>
                                                <th>MyC</th>
                                                <th>TECH</th>
                                                <th>PISO</th>
                                                <th>PyV</th>
                                                <th>REV</th>
                                                <th>BAÑO</th>
                                                <th>I. EL</th>
                                                <th>A. DECL</th>
                                                <th>A. VERIF.</th>
                                                <th>UCA</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            foreach ($data['construccion'] as $construccion):
                                                $porc_bc_terr_leg  = $construccion['porc_bc_terr_leg'];
                                                $porc_bc_terr_fis  = $construccion['porc_bc_terr_fis'];
                                                $porc_bc_const_leg = $construccion['porc_bc_const_leg'];
                                                $porc_bc_const_fis = $construccion['porc_bc_const_fis'];
                                                $id                = $construccion['id'];
                                                $nivel             = $construccion['nivel'];
                                                $mes               = $construccion['mes'];
                                                $anio              = $construccion['anio'];
                                                $mep               = $construccion['mep'];
                                                $ecs               = $construccion['ecs'];
                                                $ecc               = $construccion['ecc'];
                                                $estru_mur_col     = $construccion['estru_mur_col'];
                                                $estru_techo       = $construccion['estru_techo'];
                                                $acaba_piso        = $construccion['acaba_piso'];
                                                $acaba_pue_ven     = $construccion['acaba_pue_ven'];
                                                $acaba_revest      = $construccion['acaba_revest'];
                                                $acaba_banio       = $construccion['acaba_banio'];
                                                $insta_elec_sanit  = $construccion['insta_elec_sanit'];
                                                $area_declarada    = $construccion['area_declarada'];
                                                $area_verificada   = $construccion['area_verificada'];
                                                $uca               = $construccion['uca'];
                                                $des_mep           = $construccion['des_mep'];
                                                $des_ecs           = $construccion['des_ecs'];
                                                $des_ecc           = $construccion['des_ecc'];
                                                ?>
                                                <tr>
                                                    <td class="bg-white"><?= $id; ?></td>
                                                    <td class="bg-white"><?= $nivel; ?></td>
                                                    <td class="bg-white"><?= $mes ?></td>
                                                    <td class="bg-white"><?= $anio; ?></td>
                                                    <td class="bg-white"><?= $des_mep; ?></td>
                                                    <td class="bg-white"><?= $des_ecs; ?></td>
                                                    <td class="bg-white"><?= $des_ecc; ?></td>
                                                    <td class="bg-white"><?= $estru_mur_col; ?></td>
                                                    <td class="bg-white"><?= $estru_techo; ?></td>
                                                    <td class="bg-white"><?= $acaba_piso; ?></td>
                                                    <td class="bg-white"><?= $acaba_pue_ven; ?></td>
                                                    <td class="bg-white"><?= $acaba_revest; ?></td>
                                                    <td class="bg-white"><?= $acaba_banio; ?></td>
                                                    <td class="bg-white"><?= $insta_elec_sanit; ?></td>
                                                    <td class="bg-white"><?= $area_declarada; ?></td>
                                                    <td class="bg-white"><?= $area_verificada; ?></td>
                                                    <td class="bg-white"><?= $uca; ?></td>
                                                </tr>
                                            <?php endforeach; ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12">
                        <div class="portlet light">
                            <div class="portlet-title">
                                <h4 class="h4">OBRAS COMPLEMENTARIAS / OTRAS INSTALACIONES</h4>
                            </div>
                            <div class="portlet-body">
                                <div class="table-responsive">
                                    <table class="table table-bordered table-condensed">
                                        <thead class="bg-gray">
                                            <tr>
                                                <th>Nº</th>
                                                <th>CODIGO</th>
                                                <th>DESCRIPCION</th>
                                                <th>MES</th>
                                                <th>AÑO</th>
                                                <th>MEP</th>
                                                <th>ECS</th>
                                                <th>ECC</th>
                                                <th>LARGO</th>
                                                <th>ANCHO</th>
                                                <th>PROD. TOTAL</th>
                                                <th>U. MED</th>
                                                <th>UCA</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            foreach ($data['obra'] as $obra):
                                                $id              = $obra['id'];
                                                $cod_instalacion = $obra['cod_instalacion'];
                                                $mes             = $obra['mes'];
                                                $anio            = $obra['anio'];
                                                $mep             = $obra['mep'];
                                                $ecs             = $obra['ecs'];
                                                $ecc             = $obra['ecc'];
                                                $dimencion_largo = $obra['dimencion_largo'];
                                                $dimencion_ancho = $obra['dimencion_ancho'];
                                                $dimencion_alto  = $obra['dimencion_alto'];
                                                $dimencion_total = $obra['dimencion_total'];
                                                $descripcion     = $obra['descripcion'];
                                                $unidad_medida   = $obra['unidad_medida'];
                                                $uca             = $obra['uca'];
                                                $des_mep         = $obra['des_mep'];
                                                $des_ecs         = $obra['des_ecs'];
                                                $des_ecc         = $obra['des_ecc'];
                                                ?>
                                                <tr>
                                                    <td class="bg-white"><?= $id; ?></td>
                                                    <td class="bg-white"><?= $cod_instalacion; ?></td>
                                                    <td class="bg-white"><?= $descripcion; ?></td>
                                                    <td class="bg-white"><?= $mes ?></td>
                                                    <td class="bg-white"><?= $anio; ?></td>
                                                    <td class="bg-white"><?= $des_mep; ?></td>
                                                    <td class="bg-white"><?= $des_ecs; ?></td>
                                                    <td class="bg-white"><?= $des_ecc; ?></td>
                                                    <td class="bg-white"><?= $dimencion_largo; ?></td>
                                                    <td class="bg-white"><?= $dimencion_ancho; ?></td>
                                                    <td class="bg-white"><?= $dimencion_alto; ?></td>
                                                    <td class="bg-white"><?= $dimencion_total; ?></td>
                                                    <td class="bg-white"><?= $unidad_medida; ?></td>
                                                    <td class="bg-white"><?= $uca; ?></td>
                                                </tr>
                                            <?php endforeach; ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12">
                        <div class="portlet light">
                            <div class="portlet-title">
                                <h4 class="h4">RECAPITULACION DE EDIFICIOS, BIENES COMUNES</h4>
                            </div>
                            <div class="portlet-body">
                                <div class="table-responsive">
                                    <table class="table table-bordered table-condensed">
                                        <thead class="bg-gray">
                                            <tr>
                                                <th colspan="5" class="bg-gray">RECAPITULACION DE EDIFICIOS</th>
                                            </tr>
                                            <tr>
                                                <th>EDIFICIO</th>
                                                <th>PORCENTAJE</th>
                                                <th>ATC (M2)</th>
                                                <th>AOIC (M2)</th>
                                                <th>ACC (M2)</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            $totPorcentaje = 0;
                                            $totAtc        = 0;
                                            $totAoic       = 0;
                                            $totAcc        = 0;
                                            foreach ($data['recapitulacionEdificio'] as $recapitulacionEdificio):
                                                $edificio   = $recapitulacionEdificio['edificio'];
                                                $porcentaje = $recapitulacionEdificio['porcentaje'];
                                                $atc        = $recapitulacionEdificio['atc'];
                                                $aoic       = $recapitulacionEdificio['aoic'];
                                                $acc        = $recapitulacionEdificio['acc'];
                                                $totPorcentaje += $porcentaje;
                                                $totAtc += $atc;
                                                $totAoic += $aoic;
                                                $totAcc += $acc;
                                                ?>
                                                <tr>
                                                    <td class="bg-white"><?= $edificio; ?></td>
                                                    <td class="bg-white"><?= $porcentaje; ?></td>
                                                    <td class="bg-white"><?= $atc; ?></td>
                                                    <td class="bg-white"><?= $aoic ?></td>
                                                    <td class="bg-white"><?= $acc; ?></td>
                                                </tr>
                                            <?php endforeach; ?>
                                            <tr>
                                                <td class="bg-gray">TOTAL</td>
                                                <td class="bg-white"><?= $totPorcentaje; ?></td>
                                                <td class="bg-white"><?= $totAtc; ?></td>
                                                <td class="bg-white"><?= $totAoic ?></td>
                                                <td class="bg-white"><?= $totAcc; ?></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="table-responsive">
                                    <table class="table table-bordered table-condensed">
                                        <thead class="bg-gray">
                                            <tr>
                                                <th colspan="5" class="bg-gray">RECAPITULACION DE BIENES COMUNES</th>
                                            </tr>
                                            <tr>
                                                <th>Nº</th>
                                                <th>EDIFICACION</th>
                                                <th>ENTRADA</th>
                                                <th>PISO</th>
                                                <th>UNIDAD</th>
                                                <th>%</th>
                                                <th>ATC</th>
                                                <th>AOIC</th>
                                                <th>ACC</th>
                                                <th>F.IND</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            $totPorcentaje = 0;
                                            $totAtc        = 0;
                                            $totAoic       = 0;
                                            $totAcc        = 0;
                                            foreach ($data['recapitulacionBien'] as $recapitulacionEdificio):
                                                $nro        = $recapitulacionEdificio['nro'];
                                                $edifica    = $recapitulacionEdificio['edifica'];
                                                $entrada    = $recapitulacionEdificio['entrada'];
                                                $piso       = $recapitulacionEdificio['piso'];
                                                $unidad     = $recapitulacionEdificio['unidad'];
                                                $porcentaje = $recapitulacionEdificio['porcentaje'];
                                                $atc        = $recapitulacionEdificio['atc'];
                                                $aoic       = $recapitulacionEdificio['aoic'];
                                                $acc        = $recapitulacionEdificio['acc'];
                                                $ind        = $recapitulacionEdificio['ind'];
                                                $totPorcentaje += $porcentaje;
                                                $totAtc += $atc;
                                                $totAoic += $aoic;
                                                $totAcc += $acc;
                                                ?>
                                                <tr>
                                                    <td class="bg-white"><?= $nro; ?></td>
                                                    <td class="bg-white"><?= $edifica; ?></td>
                                                    <td class="bg-white"><?= $entrada; ?></td>
                                                    <td class="bg-white"><?= $piso; ?></td>
                                                    <td class="bg-white"><?= $unidad; ?></td>
                                                    <td class="bg-white"><?= $porcentaje; ?></td>
                                                    <td class="bg-white"><?= $atc; ?></td>
                                                    <td class="bg-white"><?= $aoic ?></td>
                                                    <td class="bg-white"><?= $acc; ?></td>
                                                    <td class="bg-white"><?= $ind; ?></td>
                                                </tr>
                                            <?php endforeach; ?>
                                            <tr>
                                                <td colspan="5" class="bg-gray">TOTAL</td>
                                                <td class="bg-white"><?= $totPorcentaje; ?></td>
                                                <td class="bg-white"><?= $totAtc; ?></td>
                                                <td class="bg-white"><?= $totAoic ?></td>
                                                <td class="bg-white"><?= $totAcc; ?></td>
                                                <td class="bg-gray">&nbsp;</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="table-responsive">
                                    <table class="table table-bordered table-condensed">
                                        <thead class="bg-gray">
                                            <tr>
                                                <th colspan="8" class="bg-gray">AREA DE TERRENO INVADIDA (M2)</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td class="bg-gray">EN LOTE COLINDANTE</td>
                                                <td class="bg-white"><?= $en_lote_colindante; ?></td>
                                                <td class="bg-gray">EN JARDIN DE AISLAMIENTO</td>
                                                <td class="bg-white"><?= $en_jardin_aislam; ?></td>
                                                <td class="bg-gray">EN AREA PUBLICA</td>
                                                <td class="bg-white"><?= $en_area_publica; ?></td>
                                                <td class="bg-gray">EN AREA INTANGIBLE</td>
                                                <td class="bg-white"><?= $en_area_intang; ?></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="table-responsive">
                                    <table class="table table-bordered table-condensed">
                                        <thead class="bg-gray">
                                            <tr>
                                                <th colspan="6" class="bg-gray">REGISTRO NOTARIAL DE ESCRITURA PUBLICA</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td class="bg-gray">NOMBRE DE LA NOTARIA</td>
                                                <td class="bg-white"><?= $des_notaria; ?></td>
                                                <td class="bg-gray">KARDEX</td>
                                                <td class="bg-white"><?= $kardex; ?></td>
                                                <td class="bg-gray">F. ESCR. PUBLICA</td>
                                                <td class="bg-white"><?= ($fecha_escritura != null) ? (substr($fecha_escritura, 0, 4) > '1900') ? date_format(new DateTime($fecha_escritura), 'd/m/Y') : "" : ""; ?></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="table-responsive">
                                    <table class="table table-bordered table-condensed">
                                        <thead class="bg-gray">
                                            <tr>
                                                <th colspan="8" class="bg-gray">INSCRIPCION DEL PREDIO CATASTRAL EN REGISTRO DE PREDIOS</th>
                                            </tr>
                                            <tr>
                                                <th>TIPO PARTIDA REGISTRAL</th>
                                                <th>NUMERO</th>
                                                <th>FOJAS</th>
                                                <th>ASIENTO</th>
                                                <th>FECHA</th>
                                                <th>DECLARATORIA DE FAB.</th>
                                                <th>ASIENTO DE INSC. DE FAB.</th>
                                                <th>F. INSC. FAB.</th>

                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td class="bg-white"><?= $tipo_part_reg . ' ' . $des_tipo_part; ?></td>
                                                <td class="bg-white"><?= $numero; ?></td>
                                                <td class="bg-white"><?= $fojas; ?></td>
                                                <td class="bg-white"><?= $asiento; ?></td>
                                                <td class="bg-white"><?= ($fecha_inscripcion != null) ? (substr($fecha_inscripcion, 0, 4) > '1900') ? date_format(new DateTime($fecha_inscripcion), 'd/m/Y') : "" : ""; ?></td>
                                                <td class="bg-white"><?= $declar_fab . ' ' . $des_declar_fab; ?></td>
                                                <td class="bg-white"><?= $asien_inscrip_fab; ?></td>
                                                <td class="bg-white"><?= ($fecha_inscrip_fab != null) ? (substr($fecha_inscrip_fab, 0, 4) > '1900') ? date_format(new DateTime($fecha_inscrip_fab), 'd/m/Y') : "" : ""; ?></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12">
                        <div class="portlet light">
                            <div class="portlet-title">
                                <h4 class="h4">INFORMACION COMPLEMENTARIA</h4>
                            </div>
                            <div class="portlet-body">
                                <div class="table-responsive">
                                    <table class="table table-bordered table-condensed">
                                        <tbody>
                                            <tr>
                                                <td class="bg-gray">COND. DECLARANTE</td>
                                                <td class="bg-white"><?= $condic_declara . ' ' . $des_cond_declara; ?></td>
                                                <td class="bg-gray">ESTAD. LLENADO</td>
                                                <td class="bg-white"><?= $estado_llenado . ' ' . $des_est_llenado; ?></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="table-responsive">
                                    <table class="table table-bordered table-condensed">
                                        <tbody>
                                            <tr>
                                                <td class="bg-gray">OBSERVACIONES</td>
                                            </tr>
                                            <tr>
                                                <td class="bg-white"><?= $observaciones; ?></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12">
                        <div class="portlet light">
                            <div class="portlet-title">
                                <h4 class="h4">OTROS</h4>
                            </div>
                            <div class="portlet-body">
                                <div class="table-responsive">
                                    <table class="table table-bordered table-condensed">
                                        <tbody>
                                            <tr>
                                                <td colspan="2" class="bg-gray">FIRMA DEL DECLARANTE <input name="firma_declarante" type="checkbox" disabled="disabled" <?= ($firma_declarante == 1) ? "checked" : "" ?>/></td>
                                                <td colspan="2" class="bg-gray">FIRMA DEL SUPERVISOR <input name="firma_supervisor" type="checkbox" disabled="disabled" <?= ($firma_supervisor == 1) ? "checked" : "" ?>/></td>
                                                <td colspan="2" class="bg-gray">FIRMA DEL TECNICO CATASTRAL <input name="firma_tec_catastral" type="checkbox" disabled="disabled" <?= ($firma_tec_catastral == 1) ? "checked" : "" ?>/></td>
                                                <td colspan="2" class="bg-gray">V° B° DEL TECNICO CONTROL DE CALIDAD <input name="firma_tec_calidad" type="checkbox" disabled="disabled" <?= ($firma_tec_calidad == 1) ? "checked" : "" ?>/></td>
                                                <td colspan="2" class="bg-gray">V° B° DEL VERIFICADOR CATASTRAL <input name="firma_verif_catastral" type="checkbox" disabled="disabled" <?= ($firma_verif_catastral == 1) ? "checked" : "" ?>/></td>
                                            </tr>
                                            <tr>
                                                <td class="bg-gray">DNI</td>
                                                <td class="bg-white"><?= $id_declarante; ?></td>
                                                <td class="bg-gray">CODIGO</td>
                                                <td class="bg-white"><?= $id_supervisor; ?></td>
                                                <td class="bg-gray">CODIGO</td>
                                                <td class="bg-white"><?= $id_tec_catastral; ?></td>
                                                <td class="bg-gray">CODIGO</td>
                                                <td class="bg-white"><?= $id_tec_calidad; ?></td>
                                                <td class="bg-gray">CODIGO</td>
                                                <td class="bg-white"><?= $id_verif_catastral; ?></td>
                                            </tr>
                                            <tr>
                                                <td class="bg-gray">FECHA</td>
                                                <td class="bg-white"><?= ($fecha_declarante != null) ? (substr($fecha_declarante, 0, 4) > '1900') ? date_format(new DateTime($fecha_declarante), 'd/m/Y') : "" : ""; ?></td>
                                                <td class="bg-gray">FECHA</td>
                                                <td class="bg-white"><?= ($fecha_supervisor != null) ? (substr($fecha_supervisor, 0, 4) > '1900') ? date_format(new DateTime($fecha_supervisor), 'd/m/Y') : "" : ""; ?></td>
                                                <td class="bg-gray">FECHA</td>
                                                <td class="bg-white"><?= ($fecha_tec_catastral != null) ? (substr($fecha_tec_catastral, 0, 4) > '1900') ? date_format(new DateTime($fecha_tec_catastral), 'd/m/Y') : "" : ""; ?></td>
                                                <td class="bg-gray">FECHA</td>
                                                <td class="bg-white"><?= ($fecha_tec_calidad != null) ? (substr($fecha_tec_calidad, 0, 4) > '1900') ? date_format(new DateTime($fecha_tec_calidad), 'd/m/Y') : "" : ""; ?></td>
                                                <td class="bg-gray">FECHA</td>
                                                <td class="bg-white"><?= ($fecha_verif_catastral != null) ? (substr($fecha_verif_catastral, 0, 4) > '1900') ? date_format(new DateTime($fecha_verif_catastral), 'd/m/Y') : "" : ""; ?></td>
                                            </tr>
                                            <tr>
                                                <td class="bg-white" colspan="2"><?= $nom_declarante; ?></td>
                                                <td class="bg-white" colspan="2"><?= $nom_supervisor; ?></td>
                                                <td class="bg-white" colspan="2"><?= $nom_tec_catastral; ?></td>
                                                <td class="bg-white" colspan="2"><?= $nom_tec_calidad; ?></td>
                                                <td class="bg-white" colspan="2"><?= $nom_verif_catastral; ?></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="table-responsive">
                                    <table class="table table-bordered table-condensed">
                                        <tbody>
                                            <tr>
                                                <td class="bg-gray">CREADO</td>
                                                <td class="bg-white"><?= $digi_in; ?></td>
                                                <td class="bg-white"><?= $insert_fecha; ?></td>
                                                <td class="bg-gray">MODIFICADO</td>
                                                <td class="bg-white"><?= $digi_up; ?></td>
                                                <td class="bg-white"><?= $update_fecha; ?></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>