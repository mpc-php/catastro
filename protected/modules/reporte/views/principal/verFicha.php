
<?php
$cod_lote              = $data['general']['cod_lote'];
$id_ficha              = $data['general']['id_ficha'];
$nro_ficha             = $data['general']['nro_ficha'];
$tip_ficha             = $data['general']['tip_ficha'];
$cuc                   = $data['general']['cuc'];
$cod_hoja_cat          = $data['general']['cod_hoja_cat'];
$nro_ficha_lote        = $data['general']['nro_ficha_lote'];
$cod_predial           = $data['general']['cod_predial'];
$unid_acum_cod_predial = $data['general']['unid_acum_cod_predial'];
$cod_cat               = $data['general']['cod_cat'];
$tipo_interior         = $data['general']['tipo_interior'];
$nro_interior          = $data['general']['nro_interior'];
$id_edificacion        = $data['general']['id_edificacion'];
$tipo_edificacion      = $data['general']['tipo_edificacion'];
$nombre_edificacion    = $data['general']['nombre_edificacion'];
$cod_hab_urb           = $data['general']['cod_hab_urb'];
$mza_urb               = $data['general']['mza_urb'];
$lote_dist             = $data['general']['lote_dist'];
$sublote               = $data['general']['sublote'];
$nom_hab_urb           = $data['general']['des_hab_urb'];
$zn_sc_etapa           = $data['general']['des_zn_sc_etapa'];
$des_tip_edif          = $data['general']['des_tip_edif'];
$des_tip_int           = $data['general']['des_tip_int'];

$cond_titular  = $data['titularidad']['condicion_titular'] . ' ' . $data['titularidad']['desc_tabla_detalle'];
$fuente_inform = $data['titularidad']['fuente_inform'] . ' ' . $data['titularidad']['des_fuente_inform'];

$clasificacion        = $data['descripcionPredio']['clasificacion'];
$predio_catastrado_en = $data['descripcionPredio']['predio_catastrado_en'];

$cod_uso  = $data['descripcionPredio']['cod_uso'];
$desc_uso = $data['descripcionPredio']['desc_uso'];

$area_titulo     = $data['descripcionPredio']['area_titulo'];
$area_declarada  = $data['descripcionPredio']['area_declarada'];
$area_verificada = $data['descripcionPredio']['area_verificada'];
$estructuracion  = $data['descripcionPredio']['estructuracion'];
$zonificacion    = $data['descripcionPredio']['zonificacion'];

$fre_med_campo = $data['descripcionPredio']['fre_med_campo'];
$der_med_campo = $data['descripcionPredio']['der_med_campo'];
$izq_med_campo = $data['descripcionPredio']['izq_med_campo'];
$fon_med_campo = $data['descripcionPredio']['fon_med_campo'];


$fre_med_titulo = $data['descripcionPredio']['fre_med_titulo'];
$der_med_titulo = $data['descripcionPredio']['der_med_titulo'];
$izq_med_titulo = $data['descripcionPredio']['izq_med_titulo'];
$fon_med_titulo = $data['descripcionPredio']['fon_med_titulo'];


$fre_col_campo = $data['descripcionPredio']['fre_col_campo'];
$der_col_campo = $data['descripcionPredio']['der_col_campo'];
$izq_col_campo = $data['descripcionPredio']['izq_col_campo'];
$fon_col_campo = $data['descripcionPredio']['fon_col_campo'];

$fre_col_titulo  = $data['descripcionPredio']['fre_col_titulo'];
$der_col_titulo  = $data['descripcionPredio']['der_col_titulo'];
$izq_col_titulo  = $data['descripcionPredio']['izq_col_titulo'];
$fon_col_titulo  = $data['descripcionPredio']['fon_col_titulo'];
$des_clasific    = $data['descripcionPredio']['des_clasific'];
$des_pred_cat_en = $data['descripcionPredio']['des_pred_cat_en'];
$des_zona        = $data['descripcionPredio']['des_zona'];

$area_ocupada    = $data['descripcionPredio']['area_ocupada'];
$fre_lim_fre_lot = $data['descripcionPredio']['fre_lim_fre_lot'];
$der_lim_fre_lot = $data['descripcionPredio']['der_lim_fre_lot'];
$izq_lim_fre_lot = $data['descripcionPredio']['izq_lim_fre_lot'];
$fon_lim_fre_lot = $data['descripcionPredio']['fon_lim_fre_lot'];
$tot_lim_fre_uc  = $data['descripcionPredio']['tot_lim_fre_uc'];

$luz      = $data['serviciosBasicos']['luz'];
$agua     = $data['serviciosBasicos']['agua'];
$telefono = $data['serviciosBasicos']['telefono'];


$desague           = $data['serviciosBasicos']['desague'];
$nro_sum_luz       = $data['serviciosBasicos']['nro_sum_luz'];
$nro_contrato_agua = $data['serviciosBasicos']['nro_contrato_agua'];
$nro_telefono      = $data['serviciosBasicos']['nro_telefono'];
$gas               = $data['serviciosBasicos']['gas'];
$cable             = $data['serviciosBasicos']['cable'];
$internet          = $data['serviciosBasicos']['internet'];

$kardex          = $data['escritura']['kardex'];
$fecha_escritura = $data['escritura']['fecha_escritura'];
$des_notaria     = $data['escritura']['des_notaria'];

$tipo_part_reg     = $data['sunarp']['tipo_part_reg'];
$numero            = $data['sunarp']['numero'];
$fojas             = $data['sunarp']['fojas'];
$asiento           = $data['sunarp']['asiento'];
$fecha_inscripcion = $data['sunarp']['fecha_inscripcion'];
$declar_fab        = $data['sunarp']['declar_fab'];
$asien_inscrip_fab = $data['sunarp']['asien_inscrip_fab'];
$fecha_inscrip_fab = $data['sunarp']['fecha_inscrip_fab'];
$des_tipo_part     = $data['sunarp']['des_tipo_part'];
$des_declar_fab    = $data['sunarp']['des_declar_fab'];

$evaluac_pred_cat   = $data['complementaria']['evaluac_pred_cat'];
$condic_declara     = $data['complementaria']['condic_declara'];
$estado_llenado     = $data['complementaria']['estado_llenado'];
$nro_habitantes     = $data['complementaria']['nro_habitantes'];
$nro_familias       = $data['complementaria']['nro_familias'];
$mantenimiento      = $data['complementaria']['mantenimiento'];
$en_lote_colindante = $data['complementaria']['en_lote_colindante'];
$en_jardin_aislam   = $data['complementaria']['en_jardin_aislam'];
$en_area_intang     = $data['complementaria']['en_area_intang'];
$en_area_publica    = $data['complementaria']['en_area_publica'];
$observaciones      = $data['complementaria']['observaciones'];
$des_cond_declar    = $data['complementaria']['des_cond_declar'];
$des_est_llenado    = $data['complementaria']['des_est_llenado'];
$des_mantenimiento  = $data['complementaria']['des_mantenimiento'];


$firma_declarante      = $data['otros']['firma_declarante'];
$id_declarante         = $data['otros']['id_declarante'];
$fecha_declarante      = $data['otros']['fecha_declarante'];
$firma_supervisor      = $data['otros']['firma_supervisor'];
$id_supervisor         = $data['otros']['id_supervisor'];
$fecha_supervisor      = $data['otros']['fecha_supervisor'];
$firma_tec_catastral   = $data['otros']['firma_tec_catastral'];
$id_tec_catastral      = $data['otros']['id_tec_catastral'];
$fecha_tec_catastral   = $data['otros']['fecha_tec_catastral'];
$firma_verif_catastral = $data['otros']['firma_verif_catastral'];

$id_verif_catastral    = $data['otros']['id_verif_catastral'];
$fecha_verif_catastral = $data['otros']['fecha_verif_catastral'];

$nom_declarante      = $data['otros']['nom_declarante'];
$nom_supervisor      = $data['otros']['nom_supervisor'];
$nom_tec_catastral   = $data['otros']['nom_tec_catastral'];
$nom_verif_catastral = $data['otros']['nom_verif_catastral'];

$firma_tec_calidad = $data['otros']['firma_tec_calidad'];
$id_tec_calidad    = $data['otros']['id_tec_calidad'];
$fecha_tec_calidad = $data['otros']['fecha_tec_calidad'];
$nom_tec_calidad   = $data['otros']['nom_tec_calidad'];

$insert_digitador = $data['digitador']['insert_digitador'];
$insert_fecha     = $data['digitador']['fec_in'];
$update_digitador = $data['digitador']['update_digitador'];
$update_fecha     = $data['digitador']['fec_up'];
$digi_in          = $data['digitador']['digi_in'];
$digi_up          = $data['digitador']['digi_up'];

//URL para la foto
$linkurl = "http://172.17.1.22/fotos/SECTOR " . substr($cod_lote, 0, 2) . "/" . substr($cod_lote, 0, 5) . "/" . $cod_lote . ".jpg";
?>
<div class="page-head">
    <div class="container">
        <div class="page-title">
            <h4 class="h4">FICHA CATASTRAL INDIVIDUAL N° <?= $nro_ficha ?></h4>
        </div>
        <div class="page-toolbar">
            <div class="btn-group btn-theme-panel">
                <a href="<?= $linkurl ?>" class="btn btn-primary" data-lightbox="foto-ficha" data-title="Foto de la Ficha">Fotos</a>
                <?php if (is_array($data['economica'])): ?>
                    <a href="<?= $this->createUrl("verfichaEconomica", ['id' => trim($data['economica']['id_ficha'])]) ?>" class="btn btn-warning" >
                        F. Activ. Econom.
                    </a>
                <?php endif; ?>
                <?php if (is_array($data['cotitularidad'])): ?>
                    <a href="<?= $this->createUrl("verfichaCotitularidad", ['id' => trim($data['cotitularidad']['id_ficha'])]) ?>" class="btn btn-info" >
                        F. Cotitularidad
                    </a>
                <?php endif; ?>
                <?php if (is_array($data['biencomun'])): ?>
                    <a href="<?= $this->createUrl("verfichaBienComun", ['id' => trim($data['biencomun']['id_ficha'])]) ?>" class="btn btn-danger" >
                        F. Bien Comun
                    </a>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>

<div class="page-content">
    <div class="container">
        <!-- BEGIN PAGE CONTENT INNER -->
        <div class="page-content-inner">
            <div class="bordered">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="portlet light">
                            <div class="portlet-title">
                                <h4 class="h4">CODIGO DE REFERENCIA CATASTRAL</h4>
                            </div>
                            <div class="portlet-body">
                                <div class="table-responsive">
                                    <table class="table table-bordered table-condensed">
                                        <thead class="bg-gray">
                                            <tr>
                                                <th>UBIGEO</th>
                                                <th>SECTOR</th>
                                                <th>MANZANA</th>
                                                <th>LOTE</th>
                                                <th>EDIF</th>
                                                <th>ENTRADA</th>
                                                <th>PISO</th>
                                                <th>UNIDAD</th>
                                                <th>DC</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td class="bg-white"><?= substr($cod_cat, 0, 6); ?></td>
                                                <td class="bg-white"><?= substr($cod_cat, 10, 2); ?></td>
                                                <td class="bg-white"><?= substr($cod_cat, 12, 3); ?></td>
                                                <td class="bg-white"><?= substr($cod_cat, 15, 3); ?></td>
                                                <td class="bg-white"><?= substr($cod_cat, 18, 2); ?></td>
                                                <td class="bg-white"><?= substr($cod_cat, 20, 2); ?></td>
                                                <td class="bg-white"><?= substr($cod_cat, 22, 2); ?></td>
                                                <td class="bg-white"><?= substr($cod_cat, 24, 3); ?></td>
                                                <td class="bg-white"><?= UReporte::dig_check($cod_cat); ?></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="table-responsive">
                                    <table class="table table-bordered table-condensed">
                                        <thead class="bg-gray">
                                            <tr>
                                                <th colspan="2">CODIGO UNICO CATASTRAL - CUC</th>
                                                <th>CODIGO HOJA CATASTRAL</th>
                                                <th colspan="2">N° FICHA POR LOTE</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td class="bg-white"><?= substr($cuc, 0, 8); ?></td>
                                                <td class="bg-white"><?= substr($cuc, 8, 4); ?></td>
                                                <td class="bg-white"><?= $cod_hoja_cat; ?></td>
                                                <td class="bg-white"><?= substr($nro_ficha_lote, 0, 4); ?></td>
                                                <td class="bg-white"><?= substr($nro_ficha_lote, 5, 4); ?></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="table-responsive">
                                    <table class="table table-bordered table-condensed">
                                        <thead class="bg-gray">
                                            <tr>
                                                <th colspan="2">CODIGO PREDIAL RENTAS</th>
                                                <th>&nbsp;</th>
                                                <th colspan="2">UNIDAD ACUMULADA DE RENTAS</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td colspan="2" class="bg-white"><?= $cod_predial; ?></td>
                                                <td class="bg-white">&nbsp;</td>
                                                <td class="bg-white"><?= $unid_acum_cod_predial; ?></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12">
                        <div class="portlet light">
                            <div class="portlet-title">
                                <h4 class="h4">UBICACION DEL PREDIO</h4>
                            </div>
                            <div class="portlet-body">
                                <div class="table-responsive">
                                    <table class="table table-bordered table-condensed">
                                        <thead class="bg-gray">
                                            <tr>
                                                <th>NRO</th>
                                                <th>CODIGO DE VIA</th>
                                                <th>TIPO DE VIA</th>
                                                <th>NOMBRE DE VIA</th>
                                                <th>TIPO DE PUERTA</th>
                                                <th>N° MUNICIPAL</th>
                                                <th>COND. NUMER</th>
                                                <th>N° CERTIFICADO</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            foreach ($data['ubicacion'] as $ubicacion):
                                                $id                 = $ubicacion['id'];
                                                $cod_via            = $ubicacion['cod_via'];
                                                $tipo_puerta        = $ubicacion['tipo_puerta'];
                                                $nro_municipal      = $ubicacion['nro_municipal'];
                                                $cond_numeracion    = $ubicacion['cond_numeracion'];
                                                $nro_certif_numerac = $ubicacion['nro_certif_numerac'];
                                                $tip_via            = $ubicacion['tip_via'];
                                                $nom_via            = $ubicacion['nom_via'];
                                                $desc_tip_via       = $ubicacion['desc_tabla_detalle'];
                                                $desc_tip_pta       = $ubicacion['des_puerta'];
                                                ?>
                                                <tr>
                                                    <td class="bg-white"><?= $id; ?></td>
                                                    <td class="bg-white"><?= $cod_via; ?></td>
                                                    <td class="bg-white"><?= $desc_tip_via; ?></td>
                                                    <td class="bg-white"><?= $nom_via; ?></td>
                                                    <td class="bg-white"><?= $desc_tip_pta; ?></td>
                                                    <td class="bg-white"><?= $nro_municipal; ?></td>
                                                    <td class="bg-white"><?= $cond_numeracion; ?></td>
                                                    <td class="bg-white"><?= $nro_certif_numerac; ?></td>
                                                </tr>
                                            <?php endforeach; ?>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="table-responsive">
                                    <table class="table table-bordered table-condensed">
                                        <thead class="bg-gray">
                                            <tr>
                                                <th>NOMBRE DE EDIFICACION</th>
                                                <th>TIPO DE EDIFICACION</th>
                                                <th>TIPO INTERIOR</th>
                                                <th>N° INTERIOR</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td class="bg-white"><?= $nombre_edificacion; ?></td>
                                                <td class="bg-white"><?= $tipo_edificacion . ' ' . $des_tip_edif; ?></td>
                                                <td class="bg-white"><?= $tipo_interior . ' ' . $des_tip_int; ?></td>
                                                <td class="bg-white"><?= $nro_interior; ?></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="table-responsive">
                                    <table class="table table-bordered table-condensed">
                                        <thead class="bg-gray">
                                            <tr>
                                                <th>CODIGO HU</th>
                                                <th>NOMBRE DE HAB. URBANA</th>
                                                <th>ZONA/SECTOR/ETAPA</th>
                                                <th>MANZANA</th>
                                                <th>LOTE</th>
                                                <th>SUB-LOTE</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td class="bg-white"><?= $cod_hab_urb; ?></td>
                                                <td class="bg-white"><?= $nom_hab_urb; ?></td>
                                                <td class="bg-white"><?= $zn_sc_etapa; ?></td>
                                                <td class="bg-white"><?= $mza_urb; ?></td>
                                                <td class="bg-white"><?= $lote_dist; ?></td>
                                                <td class="bg-white"><?= $sublote; ?></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12">
                        <div class="portlet light">
                            <div class="portlet-title">
                                <h4 class="h4">TITULARIDAD</h4>
                            </div>
                            <div class="portlet-body">
                                <div class="table-responsive">
                                    <table class="table table-bordered table-condensed">
                                        <tbody>
                                            <tr>
                                                <td class="bg-gray">CONDICION DEL TITULAR</td>
                                                <td class="bg-white"><?= $cond_titular; ?></td>
                                                <td class="bg-gray">FUENTE DE INFORMACION</td>
                                                <td class="bg-white"><?= $fuente_inform; ?></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="table-responsive">
                                    <table class="table table-bordered table-condensed">
                                        <thead class="bg-gray">
                                            <tr>
                                                <th>CONDICION ESPECIAL DEL PREDIO EXONERADO</th>
                                                <th>N° RESOLUCION</th>
                                                <th>PORCENTAJE</th>
                                                <th>FECHA DE INICIO</th>
                                                <th>FECHA DE VENCIMIENTO</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td class="bg-white">&nbsp;</td>
                                                <td class="bg-white">&nbsp;</td>
                                                <td class="bg-white">&nbsp;</td>
                                                <td class="bg-white">&nbsp;</td>
                                                <td class="bg-white">&nbsp;</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="table-responsive">
                                    <table class="table table-bordered table-condensed">
                                        <thead class="bg-gray">
                                            <tr>
                                                <th>ID</th>
                                                <th>CODIGO</th>
                                                <th>TIPO DOC.</th>
                                                <th>N° DOC.</th>
                                                <th>TITULAR</th>
                                                <th>COD. CONTRIBUY.</th>
                                                <th>TELEFONO</th>
                                                <th>E_MAIL</th>
                                                <th>POR. PERT.</th>
                                                <th>IT</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            $idt = 1;
                                            foreach ($data['titular'] as $titular):
                                                $condicion_titular      = $titular['condicion_titular'];
                                                $id_persona             = $titular['id_persona'];
                                                $tip_doc                = $titular['tip_doc'];
                                                $nro_doc                = $titular['nro_doc'];
                                                $nombre                 = $titular['ape_paterno'] . ' ' . $titular['ape_materno'] . ', ' . $titular['nombres'];
                                                $telefono               = $titular['telefono'];
                                                $correo_elect           = $titular['correo_elect'];
                                                $porcentaje_pertenencia = $titular['porcentaje_pertenencia'];
                                                $abrev_tabla_detalle    = $titular['abrev_tabla_detalle'];
                                                $cod_contribuy          = $titular['cod_contribuy'];
                                                ?>
                                                <tr>
                                                    <td class="bg-white"><?= $idt ?></td>
                                                    <td class="bg-white"><?= $id_persona ?></td>
                                                    <td class="bg-white"><?= $abrev_tabla_detalle ?></td>
                                                    <td class="bg-white"><?= $nro_doc ?></td>
                                                    <td class="bg-white"><?= $nombre ?></td>
                                                    <td class="bg-white"><?= $cod_contribuy ?></td>
                                                    <td class="bg-white"><?= $telefono ?></td>
                                                    <td class="bg-white"><?= $correo_elect ?></td>
                                                    <td class="bg-white"><?= $porcentaje_pertenencia ?></td>
                                                    <td class="bg-white"><input type="button" class="btn btn-sm btn-primary domfis" value="..." data-id-ficha="<?= $id_ficha; ?>" data-id-persona="<?= $id_persona; ?>"/></td>
                                                </tr>
                                                <?php
                                                $idt++;
                                            endforeach;
                                            ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12">
                        <div class="portlet light">
                            <div class="portlet-title">
                                <h4 class="h4">DESCRIPCION DEL PREDIO</h4>
                            </div>
                            <div class="portlet-body">
                                <div class="table-responsive">
                                    <table class="table table-bordered table-condensed">
                                        <thead class="bg-gray">
                                            <tr>
                                                <th>CLASIFICACION DEL PREDIO</th>
                                                <th>PREDIO CATASTRADO EN</th>
                                                <th>COD. USO</th>
                                                <th>USO DEL PREDIO CATASTRADO</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td class="bg-white"><?= $clasificacion . ' ' . $des_clasific; ?></td>
                                                <td class="bg-white"><?= $predio_catastrado_en . ' ' . $des_pred_cat_en; ?></td>
                                                <td class="bg-white"><?= $cod_uso ?></td>
                                                <td class="bg-white"><?= $desc_uso; ?></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="table-responsive">
                                    <table class="table table-bordered table-condensed">
                                        <thead class="bg-gray">
                                            <tr>
                                                <th>ESTRUCTURACION</th>
                                                <th colspan="2">ZONIFICACION</th>
                                                <th>AT. TITULO (M2)</th>
                                                <th>AT. DECLAR (M2)</th>
                                                <th>AT. VERIF (M2)</th>
                                                <th>AT. COMUN (M2)</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td class="bg-white"><?= $estructuracion; ?></td>
                                                <td class="bg-white"><?= $zonificacion; ?></td>
                                                <td class="bg-white"><?= $des_zona; ?></td>
                                                <td class="bg-white"><?= $area_titulo ?></td>
                                                <td class="bg-white"><?= $area_declarada ?></td>
                                                <td class="bg-white"><?= $area_verificada ?></td>
                                                <td class="bg-white">&nbsp;</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="table-responsive">
                                    <table class="table table-bordered table-condensed">
                                        <thead class="bg-gray">
                                            <tr>
                                                <th>LINDEROS DEL LOTE</th>
                                                <th>MEDIDAS DE CAMPO</th>
                                                <th>MEDIDAS SEGUN TITULO</th>
                                                <th>COLINDANCIAS EN CAMPO</th>
                                                <th>COLINDANCIAS SEGUN TIT.</th>
                                                <th>LIMPIEZA FRENTE DE LOTE (ML)</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td class="bg-white">FRENTE</td>
                                                <td class="bg-white"><?= $fre_med_campo; ?></td>
                                                <td class="bg-white"><?= $fre_med_titulo; ?></td>
                                                <td class="bg-white"><?= $fre_col_campo; ?></td>
                                                <td class="bg-white"><?= $fre_col_titulo ?></td>
                                                <td class="bg-white"><?= $fre_lim_fre_lot ?></td>
                                            </tr>
                                            <tr>
                                                <td class="bg-white">DERECHA</td>
                                                <td class="bg-white"><?= $der_med_campo; ?></td>
                                                <td class="bg-white"><?= $der_med_titulo; ?></td>
                                                <td class="bg-white"><?= $der_col_campo; ?></td>
                                                <td class="bg-white"><?= $der_col_titulo ?></td>
                                                <td class="bg-white"><?= $der_lim_fre_lot ?></td>
                                            </tr>
                                            <tr>
                                                <td class="bg-white">IZQUIERDA</td>
                                                <td class="bg-white"><?= $izq_med_campo; ?></td>
                                                <td class="bg-white"><?= $izq_med_titulo; ?></td>
                                                <td class="bg-white"><?= $izq_col_campo; ?></td>
                                                <td class="bg-white"><?= $izq_col_titulo ?></td>
                                                <td class="bg-white"><?= $izq_lim_fre_lot ?></td>
                                            </tr>
                                            <tr>
                                                <td class="bg-white">FONDO</td>
                                                <td class="bg-white"><?= $fon_med_campo; ?></td>
                                                <td class="bg-white"><?= $fon_med_titulo; ?></td>
                                                <td class="bg-white"><?= $fon_col_campo; ?></td>
                                                <td class="bg-white"><?= $fon_col_titulo ?></td>
                                                <td class="bg-white"><?= $fon_lim_fre_lot ?></td>
                                            </tr>
                                            <tr>
                                                <td class="bg-gray" colspan="3">&nbsp;</td>
                                                <td class="bg-gray" colspan="2">LIMPIEZA DE FRENTE DE LOTE DE LA UC (ML)</td>
                                                <td class="bg-white"><?= $tot_lim_fre_uc ?></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="table-responsive">
                                    <table class="table table-bordered table-condensed">
                                        <thead class="bg-gray">
                                            <tr>
                                                <th colspan="10">SERVICIOS BASICOS</th>
                                            </tr>
                                            <tr>
                                                <th>LUZ</th>
                                                <th>N° SUM. LUZ</th>
                                                <th>AGUA</th>
                                                <th>N° SUM. AGUA</th>
                                                <th>TELEFONO</th>
                                                <th>N° SUM. TELEF.</th>
                                                <th>DESAGUE</th>
                                                <th>GAS</th>
                                                <th>CABLE</th>
                                                <th>INTERNET</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td class="bg-white"><input name="luz" type="checkbox" disabled="disabled" <?= ($luz == 1) ? "checked" : "" ?>/></td>
                                                <td class="bg-white"><?= $nro_sum_luz ?></td>
                                                <td class="bg-white"><input name="agua" type="checkbox" disabled="disabled" <?= ($agua == 1) ? "checked" : "" ?>/></td>
                                                <td class="bg-white"><?= $nro_contrato_agua ?></td>
                                                <td class="bg-white"><input name="telefono" type="checkbox" disabled="disabled" <?= ($telefono == 1) ? "checked" : "" ?>/></td>
                                                <td class="bg-white"><?= $nro_telefono ?></td>
                                                <td class="bg-white"><input name="desague" type="checkbox" disabled="disabled" <?= ($desague == 1) ? "checked" : "" ?>/></td>
                                                <td class="bg-white"><input name="gas" type="checkbox" disabled="disabled" <?= ($gas == 1) ? "checked" : "" ?>/></td>
                                                <td class="bg-white"><input name="cable" type="checkbox" disabled="disabled" <?= ($cable == 1) ? "checked" : "" ?>/></td>
                                                <td class="bg-white"><input name="internet" type="checkbox" disabled="disabled" <?= ($internet == 1) ? "checked" : "" ?>/></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12">
                        <div class="portlet light">
                            <div class="portlet-title">
                                <h4 class="h4">CONSTRUCCIONES</h4>
                            </div>
                            <div class="portlet-body">
                                <div class="table-responsive">
                                    <table class="table table-bordered table-condensed">
                                        <thead class="bg-gray">
                                            <tr>
                                                <th>Nº</th>
                                                <th>NIVEL</th>
                                                <th>MES</th>
                                                <th>AÑO</th>
                                                <th>MEP</th>
                                                <th>ECS</th>
                                                <th>MyC</th>
                                                <th>TECH</th>
                                                <th>PISO</th>
                                                <th>PyV</th>
                                                <th>REV</th>
                                                <th>BAÑO</th>
                                                <th>I. EL</th>
                                                <th>A. DECL</th>
                                                <th>A. VERIF.</th>
                                                <th>UCA</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            foreach ($data['construccion'] as $construccion):
                                                $porc_bc_terr_leg  = $construccion['porc_bc_terr_leg'];
                                                $porc_bc_terr_fis  = $construccion['porc_bc_terr_fis'];
                                                $porc_bc_const_leg = $construccion['porc_bc_const_leg'];
                                                $porc_bc_const_fis = $construccion['porc_bc_const_fis'];
                                                $id                = $construccion['id'];
                                                $nivel             = $construccion['nivel'];
                                                $mes               = $construccion['mes'];
                                                $anio              = $construccion['anio'];
                                                $mep               = $construccion['mep'];
                                                $ecs               = $construccion['ecs'];
                                                $ecc               = $construccion['ecc'];
                                                $estru_mur_col     = $construccion['estru_mur_col'];
                                                $estru_techo       = $construccion['estru_techo'];
                                                $acaba_piso        = $construccion['acaba_piso'];
                                                $acaba_pue_ven     = $construccion['acaba_pue_ven'];
                                                $acaba_revest      = $construccion['acaba_revest'];
                                                $acaba_banio       = $construccion['acaba_banio'];
                                                $insta_elec_sanit  = $construccion['insta_elec_sanit'];
                                                $area_declarada    = $construccion['area_declarada'];
                                                $area_verificada   = $construccion['area_verificada'];
                                                $uca               = $construccion['uca'];
                                                $des_mep           = $construccion['des_mep'];
                                                $des_ecs           = $construccion['des_ecs'];
                                                $des_ecc           = $construccion['des_ecc'];
                                                ?>
                                                <tr>
                                                    <td class="bg-white"><?= $id; ?></td>
                                                    <td class="bg-white"><?= $nivel; ?></td>
                                                    <td class="bg-white"><?= $mes ?></td>
                                                    <td class="bg-white"><?= $anio; ?></td>
                                                    <td class="bg-white"><?= $des_mep; ?></td>
                                                    <td class="bg-white"><?= $des_ecs; ?></td>
                                                    <td class="bg-white"><?= $des_ecc; ?></td>
                                                    <td class="bg-white"><?= $estru_mur_col; ?></td>
                                                    <td class="bg-white"><?= $estru_techo; ?></td>
                                                    <td class="bg-white"><?= $acaba_piso; ?></td>
                                                    <td class="bg-white"><?= $acaba_pue_ven; ?></td>
                                                    <td class="bg-white"><?= $acaba_revest; ?></td>
                                                    <td class="bg-white"><?= $acaba_banio; ?></td>
                                                    <td class="bg-white"><?= $insta_elec_sanit; ?></td>
                                                    <td class="bg-white"><?= $area_declarada; ?></td>
                                                    <td class="bg-white"><?= $area_verificada; ?></td>
                                                    <td class="bg-white"><?= $uca; ?></td>
                                                </tr>
                                            <?php endforeach; ?>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="table-responsive">
                                    <table class="table table-bordered table-condensed">
                                        <tbody>
                                            <tr>
                                                <td class="bg-gray">% B.C. TERRENO- LEGAL</td>
                                                <td class="bg-white"><?= (isset($porc_bc_terr_leg)) ? $porc_bc_terr_leg : ""; ?></td>
                                                <td class="bg-gray">% B.C. TERRENO- FISICO</td>
                                                <td class="bg-white"><?= (isset($porc_bc_terr_fis)) ? $porc_bc_terr_fis : ""; ?></td>
                                                <td class="bg-gray">% B.C. CONSTRUC-LEGAL</td>
                                                <td class="bg-white"><?= (isset($porc_bc_const_leg)) ? $porc_bc_const_leg : ""; ?></td>
                                                <td class="bg-gray">% B.C. CONSTRUC-LEGAL</td>
                                                <td class="bg-white"><?= (isset($porc_bc_const_fis)) ? $porc_bc_const_fis : ""; ?></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12">
                        <div class="portlet light">
                            <div class="portlet-title">
                                <h4 class="h4">OBRAS COMPLEMENTARIAS / OTRAS INSTALACIONES</h4>
                            </div>
                            <div class="portlet-body">
                                <div class="table-responsive">
                                    <table class="table table-bordered table-condensed">
                                        <thead class="bg-gray">
                                            <tr>
                                                <th>Nº</th>
                                                <th>CODIGO</th>
                                                <th>DESCRIPCION</th>
                                                <th>MES</th>
                                                <th>AÑO</th>
                                                <th>MEP</th>
                                                <th>ECS</th>
                                                <th>ECC</th>
                                                <th>LARGO</th>
                                                <th>ANCHO</th>
                                                <th>PROD. TOTAL</th>
                                                <th>U. MED</th>
                                                <th>UCA</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            foreach ($data['obra'] as $obra):
                                                $id              = $obra['id'];
                                                $cod_instalacion = $obra['cod_instalacion'];
                                                $mes             = $obra['mes'];
                                                $anio            = $obra['anio'];
                                                $mep             = $obra['mep'];
                                                $ecs             = $obra['ecs'];
                                                $ecc             = $obra['ecc'];
                                                $dimencion_largo = $obra['dimencion_largo'];
                                                $dimencion_ancho = $obra['dimencion_ancho'];
                                                $dimencion_alto  = $obra['dimencion_alto'];
                                                $dimencion_total = $obra['dimencion_total'];
                                                $descripcion     = $obra['descripcion'];
                                                $unidad_medida   = $obra['unidad_medida'];
                                                $uca             = $obra['uca'];
                                                $des_mep         = $obra['des_mep'];
                                                $des_ecs         = $obra['des_ecs'];
                                                $des_ecc         = $obra['des_ecc'];
                                                ?>
                                                <tr>
                                                    <td class="bg-white"><?= $id; ?></td>
                                                    <td class="bg-white"><?= $cod_instalacion; ?></td>
                                                    <td class="bg-white"><?= $descripcion; ?></td>
                                                    <td class="bg-white"><?= $mes ?></td>
                                                    <td class="bg-white"><?= $anio; ?></td>
                                                    <td class="bg-white"><?= $des_mep; ?></td>
                                                    <td class="bg-white"><?= $des_ecs; ?></td>
                                                    <td class="bg-white"><?= $des_ecc; ?></td>
                                                    <td class="bg-white"><?= $dimencion_largo; ?></td>
                                                    <td class="bg-white"><?= $dimencion_ancho; ?></td>
                                                    <td class="bg-white"><?= $dimencion_alto; ?></td>
                                                    <td class="bg-white"><?= $dimencion_total; ?></td>
                                                    <td class="bg-white"><?= $unidad_medida; ?></td>
                                                    <td class="bg-white"><?= $uca; ?></td>
                                                </tr>
                                            <?php endforeach; ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12">
                        <div class="portlet light">
                            <div class="portlet-title">
                                <h4 class="h4">DOCUMENTOS Y REGISTROS</h4>
                            </div>
                            <div class="portlet-body">
                                <div class="table-responsive">
                                    <table class="table table-bordered table-condensed">
                                        <thead class="bg-gray">
                                            <tr>
                                                <th>DOC</th>
                                                <th>TIPO DOCUMENTO</th>
                                                <th>N° DOCUMENTO</th>
                                                <th>FECHA</th>
                                                <th>AREA AUTORIZADA</th>
                                                <th>1° PISO</th>
                                                <th>2° PISO</th>
                                                <th>3° PISO</th>
                                                <th>4° PISO</th>
                                                <th>5° PISO</th>
                                                <th>6° PISO</th>
                                                <th>OTROS</th>
                                                <th>SOT</th>
                                                <th>MEZ</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            foreach ($data['documentos'] as $documento):
                                                $tip_doc         = $documento['tip_doc'];
                                                $nro_doc         = $documento['nro_doc'];
                                                $fecha_doc       = $documento['fecha_doc'];
                                                $area_autorizada = $documento['area_autorizada'];
                                                $des_tip_doc     = $documento['des_tip_doc'];
                                                $area_const_p01  = $documento['area_const_p01'];
                                                $area_const_p02  = $documento['area_const_p02'];
                                                $area_const_p03  = $documento['area_const_p03'];
                                                $area_const_p04  = $documento['area_const_p04'];
                                                $area_const_p05  = $documento['area_const_p05'];
                                                $area_const_p06  = $documento['area_const_p06'];
                                                $area_const_otro = $documento['area_const_otro'];
                                                $area_const_sot  = $documento['area_const_sot'];
                                                $area_const_mez  = $documento['area_const_mez'];
                                                ?>
                                                <tr>
                                                    <td class="bg-white"><?= $tip_doc; ?></td>
                                                    <td class="bg-white"><?= $des_tip_doc; ?></td>
                                                    <td class="bg-white"><?= $nro_doc; ?></td>
                                                    <td class="bg-white"><?= date_format(new DateTime($fecha_doc), 'd/m/Y'); ?></td>
                                                    <td class="bg-white"><?= $area_autorizada; ?></td>
                                                    <td class="bg-white"><?= $area_const_p01; ?></td>
                                                    <td class="bg-white"><?= $area_const_p02; ?></td>
                                                    <td class="bg-white"><?= $area_const_p03; ?></td>
                                                    <td class="bg-white"><?= $area_const_p04; ?></td>
                                                    <td class="bg-white"><?= $area_const_p05; ?></td>
                                                    <td class="bg-white"><?= $area_const_p06; ?></td>
                                                    <td class="bg-white"><?= $area_const_otro; ?></td>
                                                    <td class="bg-white"><?= $area_const_sot; ?></td>
                                                    <td class="bg-white"><?= $area_const_mez; ?></td>
                                                </tr>
                                            <?php endforeach; ?>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="table-responsive">
                                    <table class="table table-bordered table-condensed">
                                        <thead class="bg-gray">
                                            <tr>
                                                <th colspan="3">REGISTRO NOTARIAL DE ESCRITURA PUBLICA</th>
                                            </tr>
                                            <tr>
                                                <th>NOMBRE DE LA NOTARIA</th>
                                                <th>KARDEX</th>
                                                <th>FEC. ESCRIT. PUBLICA</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td class="bg-white"><?= $des_notaria; ?></td>
                                                <td class="bg-white"><?= $kardex; ?></td>
                                                <td class="bg-white"><?= ($fecha_escritura != null) ? (substr($fecha_escritura, 0, 4) > '1900') ? date_format(new DateTime($fecha_escritura), 'd/m/Y') : "" : ""; ?></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="table-responsive">
                                    <table class="table table-bordered table-condensed">
                                        <thead class="bg-gray">
                                            <tr>
                                                <th colspan="8">INSCRIPCION DEL PREDIO CATASTRAL EN REGISTRO DE PREDIOS</th>
                                            </tr>
                                            <tr>
                                                <th>TIPO PARTIDA REGISTRAL</th>
                                                <th>NUMERO</th>
                                                <th>FOJAS</th>
                                                <th>ASIENTO</th>
                                                <th>FECHA</th>
                                                <th>DECLARATORIA DE FAB.</th>
                                                <th>ASIENTO DE INSC. DE FAB.</th>
                                                <th>F. INSC. FAB</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td class="bg-white"><?= $tipo_part_reg . ' ' . $des_tipo_part; ?></td>
                                                <td class="bg-white"><?= $numero; ?></td>
                                                <td class="bg-white"><?= $fojas; ?></td>
                                                <td class="bg-white"><?= $asiento; ?></td>
                                                <td class="bg-white"><?= ($fecha_inscripcion != null) ? (substr($fecha_inscripcion, 0, 4) > '1900') ? date_format(new DateTime($fecha_inscripcion), 'd/m/Y') : "" : ""; ?></td>
                                                <td class="bg-white"><?= $declar_fab . ' ' . $des_declar_fab; ?></td>
                                                <td class="bg-white"><?= $asien_inscrip_fab; ?></td>
                                                <td class="bg-white"><?= ($fecha_inscrip_fab != null) ? (substr($fecha_inscrip_fab, 0, 4) > '1900') ? date_format(new DateTime($fecha_inscrip_fab), 'd/m/Y') : "" : ""; ?></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12">
                        <div class="portlet light">
                            <div class="portlet-title">
                                <h4 class="h4">INFORMACION COMPLEMENTARIA</h4>
                            </div>
                            <div class="portlet-body">
                                <div class="table-responsive">
                                    <table class="table table-bordered table-condensed">
                                        <thead class="bg-gray">
                                            <tr>
                                                <th colspan="4">EVALUACION DEL PREDIO CATASTRAL</th>
                                                <th colspan="4">AREA DEL TERRENO INVADIDA (m2)</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td class="bg-gray">PREDIO CATASTRAL OMISO</td>
                                                <td class="bg-white"><input name="evaluac_pred_cat" type="checkbox" disabled="disabled" <?= ($evaluac_pred_cat == 1) ? "checked" : "" ?>/></td>
                                                <td class="bg-gray">PREDIO CATASTRAL SOBREVALUADO</td>
                                                <td class="bg-white"><input name="evaluac_pred_cat" type="checkbox" disabled="disabled" <?= ($evaluac_pred_cat == 3) ? "checked" : "" ?>/></td>
                                                <td class="bg-gray">EN LOTE COLINDANTE</td>
                                                <td class="bg-gray">EN JARDIN DE AISLAM</td>
                                                <td class="bg-gray">EN AREA PUBLICA</td>
                                                <td class="bg-gray">EN AREA INTANGIB</td>
                                            </tr>
                                            <tr>
                                                <td class="bg-gray">PREDIO CATASTRAL SUBVALUADO</td>
                                                <td class="bg-white"><input name="evaluac_pred_cat" type="checkbox" disabled="disabled" <?= ($evaluac_pred_cat == 2) ? "checked" : "" ?>/></td>
                                                <td class="bg-gray">PREDIO CATASTRAL CONFORME</td>
                                                <td class="bg-white"><input name="evaluac_pred_cat" type="checkbox" disabled="disabled" <?= ($evaluac_pred_cat == 4) ? "checked" : "" ?>/></td>
                                                <td class="bg-white"><?= $en_lote_colindante; ?></td>
                                                <td class="bg-white"><?= $en_jardin_aislam; ?></td>
                                                <td class="bg-white"><?= $en_area_publica; ?></td>
                                                <td class="bg-white"><?= $en_area_intang; ?></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="table-responsive">
                                    <table class="table table-bordered table-condensed">
                                        <thead class="bg-gray">
                                            <tr>
                                                <th colspan="4">LITIGANTES</th>
                                            </tr>
                                            <tr>
                                                <th>TIPO DE DOCUMENTO</th>
                                                <th>N° DOCUMENTO</th>
                                                <th>APELLIDOS Y NOMBRES</th>
                                                <th>COD. CONTRIBUYENTE</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td class="bg-white">&nbsp;</td>
                                                <td class="bg-white">&nbsp;</td>
                                                <td class="bg-white">&nbsp;</td>
                                                <td class="bg-white">&nbsp;</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="table-responsive">
                                    <table class="table table-bordered table-condensed">
                                        <thead class="bg-gray">
                                            <tr>
                                                <th colspan="5">INFORMACION COMPLEMENTARIA</th>
                                            </tr>
                                            <tr>
                                                <th>COND. DECLARANTE</th>
                                                <th>EST. LLENADO</th>
                                                <th>N° HABITANTES</th>
                                                <th>N° FAMILIAS</th>
                                                <th>MANTENIMIENTO</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td class="bg-white"><?= $condic_declara . ' ' . $des_cond_declar; ?></td>
                                                <td class="bg-white"><?= $estado_llenado . ' ' . $des_est_llenado; ?></td>
                                                <td class="bg-white"><?= $nro_habitantes; ?></td>
                                                <td class="bg-white"><?= $nro_familias; ?></td>
                                                <td class="bg-white"><?= $mantenimiento . ' ' . $des_mantenimiento; ?></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="table-responsive">
                                    <table class="table table-bordered table-condensed">
                                        <thead class="bg-gray">
                                            <tr>
                                                <th>OBSERVACIONES</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td class="bg-white"><?= $observaciones; ?></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12">
                        <div class="portlet light">
                            <div class="portlet-title">
                                <h4 class="h4">OTROS</h4>
                            </div>
                            <div class="portlet-body">
                                <div class="table-responsive">
                                    <table class="table table-bordered table-condensed">
                                        <tbody>
                                            <tr>
                                                <td class="bg-gray" colspan="2">FIRMA DEL DECLARANTE <input name="firma_declarante" type="checkbox" disabled="disabled" <?= ($firma_declarante == 1) ? "checked" : "" ?>/></td>
                                                <td class="bg-gray" colspan="2">FIRMA DEL SUPERVISOR <input name="firma_supervisor" type="checkbox" disabled="disabled" <?= ($firma_supervisor == 1) ? "checked" : "" ?>/></td>
                                                <td class="bg-gray" colspan="2">FIRMA DEL TECNICO CATASTRAL <input name="firma_tec_catastral" type="checkbox" disabled="disabled" <?= ($firma_tec_catastral == 1) ? "checked" : "" ?>/></td>
                                                <td class="bg-gray" colspan="2">V° B° DEL TECNICO CONTROL DE CALIDAD <input name="firma_tec_calidad" type="checkbox" disabled="disabled" <?= ($firma_tec_calidad == 1) ? "checked" : "" ?>/></td>
                                                <td class="bg-gray" colspan="2">V° B° DEL VERIFICADOR CATASTRAL <input name="firma_verif_catastral" type="checkbox" disabled="disabled" <?= ($firma_verif_catastral == 1) ? "checked" : "" ?>/></td>
                                            </tr>
                                            <tr>
                                                <td class="bg-gray">DNI</td>
                                                <td class="bg-white"><?= $id_declarante; ?></td>
                                                <td class="bg-gray">CODIGO</td>
                                                <td class="bg-white"><?= $id_supervisor; ?></td>
                                                <td class="bg-gray">CODIGO</td>
                                                <td class="bg-white"><?= $id_tec_catastral; ?></td>
                                                <td class="bg-gray">CODIGO</td>
                                                <td class="bg-white"><?= $id_tec_calidad; ?></td>
                                                <td class="bg-gray">CODIGO</td>
                                                <td class="bg-white"><?= $id_verif_catastral; ?></td>
                                            </tr>
                                            <tr>
                                                <td class="bg-gray">FECHA</td>
                                                <td class="bg-white"><?= ($fecha_declarante != null) ? (substr($fecha_declarante, 0, 4) > '1900') ? date_format(new DateTime($fecha_declarante), 'd/m/Y') : "" : ""; ?></td>
                                                <td class="bg-gray">FECHA</td>
                                                <td class="bg-white"><?= ($fecha_supervisor != null) ? (substr($fecha_supervisor, 0, 4) > '1900') ? date_format(new DateTime($fecha_supervisor), 'd/m/Y') : "" : ""; ?></td>
                                                <td class="bg-gray">FECHA</td>
                                                <td class="bg-white"><?= ($fecha_tec_catastral != null) ? (substr($fecha_tec_catastral, 0, 4) > '1900') ? date_format(new DateTime($fecha_tec_catastral), 'd/m/Y') : "" : ""; ?></td>
                                                <td class="bg-gray">FECHA</td>
                                                <td class="bg-white"><?= ($fecha_tec_calidad != null) ? (substr($fecha_tec_calidad, 0, 4) > '1900') ? date_format(new DateTime($fecha_tec_calidad), 'd/m/Y') : "" : ""; ?></td>
                                                <td class="bg-gray">FECHA</td>
                                                <td class="bg-white"><?= ($fecha_verif_catastral != null) ? (substr($fecha_verif_catastral, 0, 4) > '1900') ? date_format(new DateTime($fecha_verif_catastral), 'd/m/Y') : "" : ""; ?></td>
                                            </tr>
                                            <tr>
                                                <td class="bg-white" colspan="2"><?= $nom_declarante; ?></td>
                                                <td class="bg-white" colspan="2"><?= $nom_supervisor; ?></td>
                                                <td class="bg-white" colspan="2"><?= $nom_tec_catastral; ?></td>
                                                <td class="bg-white" colspan="2"><?= $nom_tec_calidad; ?></td>
                                                <td class="bg-white" colspan="2"><?= $nom_verif_catastral; ?></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="table-responsive">
                                    <table class="table table-bordered table-condensed">
                                        <tbody>
                                            <tr>
                                                <td class="bg-gray">CREADO</td>
                                                <td class="bg-white"><?= $digi_in; ?></td>
                                                <td class="bg-white"><?= $insert_fecha; ?></td>
                                                <td class="bg-gray">MODIFICADO</td>
                                                <td class="bg-white"><?= $digi_up; ?></td>
                                                <td class="bg-white"><?= $update_fecha; ?></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <br/>
        </div>
        <!-- END PAGE CONTENT INNER -->
    </div>
</div>

<?php $this->renderPartial('modal/_verTitular', []); ?>