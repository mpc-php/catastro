<?php
$id_ficha               = $data['titular']['id_ficha'];
$id_persona             = $data['titular']['id_persona'];
$tip_persona            = $data['titular']['tip_persona'];
$cod_contribuy          = $data['titular']['cod_contribuy'];
$forma_adq              = $data['titular']['forma_adq'];
$fecha_adq              = $data['titular']['fecha_adq'];
$porcentaje_pertenencia = $data['titular']['porcentaje_pertenencia'];
$estado_civil           = $data['titular']['estado_civil'];
$tip_doc                = $data['titular']['tip_doc'];
$nro_doc                = $data['titular']['nro_doc'];
$nombres                = $data['titular']['nombres'] . ' ' . $data['titular']['ape_paterno'] . ' ' . $data['titular']['ape_materno'];
$tip_persona_juridica   = $data['titular']['tip_persona_juridica'];
$des_tip_persona        = $data['titular']['des_tip_persona'];
$des_tip_forma_adq      = $data['titular']['des_tip_forma_adq'];
$des_estado_civil       = $data['titular']['des_estado_civil'];
$des_tip_doc            = $data['titular']['des_tip_doc'];
$des_persona_juridica   = $data['titular']['des_persona_juridica'];

$condicion          = $data['exoneracion']['condicion'];
$nro_resolucion     = $data['exoneracion']['nro_resolucion'];
$nro_boleta_pension = $data['exoneracion']['nro_boleta_pension'];
$fecha_inicio       = $data['exoneracion']['fecha_inicio'];
$fecha_vencimiento  = $data['exoneracion']['fecha_vencimiento'];
$des_condicion      = $data['exoneracion']['des_condicion'];

$id_ubi_geo     = $data['domicilio']['id_ubi_geo'];
$telefono       = $data['domicilio']['telefono'];
$anexo          = $data['domicilio']['anexo'];
$fax            = $data['domicilio']['fax'];
$correo_elect   = $data['domicilio']['correo_elect'];
$cod_via        = $data['domicilio']['cod_via'];
$cod_hab_urb    = $data['domicilio']['cod_hab_urb'];
$nro_municipal  = $data['domicilio']['nro_municipal'];
$nombre_edifica = $data['domicilio']['nombre_edifica'];
$nro_interior   = $data['domicilio']['nro_interior'];
$mza_urbana     = $data['domicilio']['mza_urbana'];
$lote_urbano    = $data['domicilio']['lote_urbano'];
$sub_lote       = $data['domicilio']['sub_lote'];
$departamento   = $data['domicilio']['departamento'];
$provincia      = $data['domicilio']['provincia'];
$distrito       = $data['domicilio']['distrito'];
$des_via        = $data['domicilio']['descrip_via'];
$des_hab_urb    = $data['domicilio']['descrip_hab_urb'];
?>
<div class="row">
    <div class="col-xs-12">
        <div class="portlet light">
            <div class="portlet-title">
                <h4 class="h4">INFORMACION DEL TITULAR</h4>
            </div>
            <div class="portlet-body">
                <div class="table-responsive">
                    <table class="table mg-b-20 table-bordered table-condensed">
                        <thead class="bg-gray">
                            <tr>
                                <th colspan="5">IDENTIFICACION DEL TITULAR REGISTRAL</th>
                            </tr>
                            <tr>
                                <th>TIPO DE TITULAR</th>
                                <th>COD. CONTRIBUYENTE</th>
                                <th>FORMA DE ADQUISICION</th>
                                <th>F.ADQUISICION</th>
                                <th>PORCENTAJE</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td class="bg-white"><?= $des_tip_persona; ?></td>
                                <td class="bg-white"><?= $cod_contribuy; ?></td>
                                <td class="bg-white"><?= $des_tip_forma_adq; ?></td>
                                <td class="bg-white"><?= ($fecha_adq != null) ? (substr($fecha_adq, 0, 4) > '1900') ? date_format(new DateTime($fecha_adq), 'd/m/Y') : "" : ""; ?></td>
                                <td class="bg-white"><?= $porcentaje_pertenencia; ?></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="table-responsive">
                    <table class="table mg-b-20 table-bordered table-condensed">
                        <thead class="bg-gray">
                            <tr>
                                <th>ESTADO CIVIL</th>
                                <th>TIP. DOC. IDENTID.</th>
                                <th>N° DOC</th>
                                <th>NOMBRE</th>
                                <th>PERSONA JURIDICA</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td class="bg-white"><?= $des_estado_civil; ?></td>
                                <td class="bg-white"><?= $des_tip_doc; ?></td>
                                <td class="bg-white"><?= $nro_doc; ?></td>
                                <td class="bg-white"><?= $nombres; ?></td>
                                <td class="bg-white"><?= $des_persona_juridica; ?></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="table-responsive">
                    <table class="table mg-b-20 table-bordered table-condensed">
                        <tbody>
                            <tr>
                                <td class="bg-green font-white text-center">DOC. CONTRIBUYENTE</td>
                                <td class="bg-white"><?= 'col1'; ?></td>
                                <td class="bg-green font-white text-center">NOMBRE CONTRIBUYENTE</td>
                                <td class="bg-white"><?= 'col2'; ?></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="table-responsive">
                    <table class="table mg-b-20 table-bordered table-condensed">
                        <thead class="bg-gray">
                            <tr>
                                <th colspan="5">EXONERACION DEL TITULAR REGISTRAL</th>
                            </tr>
                            <tr>
                                <th>COND. ESP. DEL TITULAR</th>
                                <th>N° RESOLUCION</th>
                                <th>N° DE BOLETA DE PENSIONISTA</th>
                                <th>FECHA INICIO</th>
                                <th>FECHA VENCIMIENTO</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td class="bg-white"><?= $des_condicion; ?></td>
                                <td class="bg-white"><?= $nro_resolucion; ?></td>
                                <td class="bg-white"><?= $nro_boleta_pension; ?></td>
                                <td class="bg-white"><?= ($fecha_inicio != null) ? (substr($fecha_inicio, 0, 4) > '1900') ? date_format(new DateTime($fecha_inicio), 'd/m/Y') : "" : ""; ?></td>
                                <td class="bg-white"><?= ($fecha_vencimiento != null) ? (substr($fecha_vencimiento, 0, 4) > '1900') ? date_format(new DateTime($fecha_vencimiento), 'd/m/Y') : "" : ""; ?></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="table-responsive">
                    <table class="table mg-b-20 table-bordered table-condensed">
                        <thead class="bg-gray">
                            <tr>
                                <th colspan="7">DOMICILIO FISCAL DEL TITULAR CATASTRAL</th>
                            </tr>
                            <tr>
                                <th>DEPARTAMENTO</th>
                                <th>PROVINCIA</th>
                                <th>DISTRITO</th>
                                <th>TELEFONO</th>
                                <th>ANEXO</th>
                                <th>FAX</th>
                                <th>CORREO ELECTRONICO</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td class="bg-white"><?= $departamento; ?></td>
                                <td class="bg-white"><?= $provincia; ?></td>
                                <td class="bg-white"><?= $distrito; ?></td>
                                <td class="bg-white"><?= $telefono; ?></td>
                                <td class="bg-white"><?= $anexo; ?></td>
                                <td class="bg-white"><?= $fax; ?></td>
                                <td class="bg-white"><?= $correo_elect; ?></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="table-responsive">
                    <table class="table mg-b-20 table-bordered table-condensed">
                        <thead class="bg-gray">
                            <tr>
                                <th>COD. VIA</th>
                                <th>NOMBRE DE VIA</th>
                                <th>N° MUNICIP</th>
                                <th>NOMBRE DE LA EDIFICACION</th>
                                <th>N° INTERIOR</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td class="bg-white"><?= $cod_via; ?></td>
                                <td class="bg-white"><?= $des_via; ?></td>
                                <td class="bg-white"><?= $nro_municipal; ?></td>
                                <td class="bg-white"><?= $nombre_edifica; ?></td>
                                <td class="bg-white"><?= $nro_interior; ?></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="table-responsive">
                    <table class="table mg-b-20 table-bordered table-condensed">
                        <thead class="bg-gray">
                            <tr>
                                <th>CODIGO HU.</th>
                                <th>NOMBRE HAB. URBANA</th>
                                <th>MANZANA</th>
                                <th>LOTE</th>
                                <th>SUBLOTE</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td class="bg-white"><?= $cod_hab_urb; ?></td>
                                <td class="bg-white"><?= $des_hab_urb; ?></td>
                                <td class="bg-white"><?= $mza_urbana; ?></td>
                                <td class="bg-white"><?= $lote_urbano; ?></td>
                                <td class="bg-white"><?= $sub_lote; ?></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>