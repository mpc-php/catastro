<?php
//Utils::show($data,true);

$cod_lote          = $data['general']['cod_lote'];
$id_ficha          = $data['general']['id_ficha'];
$nro_ficha         = $data['general']['nro_ficha'];
$tip_ficha         = $data['general']['tip_ficha'];
$cuc               = $data['general']['cuc'];
$cod_hoja_cat      = $data['general']['cod_hoja_cat'];
$nro_ficha_lote    = $data['general']['nro_ficha_lote'];
$cod_cat           = $data['general']['cod_cat'];
$nro_expediente    = $data['general']['nro_expediente'];
$nro_licencia      = $data['general']['nro_licencia'];
$fecha_expedicion  = $data['general']['fecha_expedicion'];
$fecha_vencimiento = $data['general']['fecha_vencimiento'];
$inicio_actividad  = $data['general']['inicio_actividad'];
$area_aut_predcat  = $data['general']['area_aut_predcat'];
$area_aut_viapub   = $data['general']['area_aut_viapub'];
$area_aut_bc       = $data['general']['area_aut_bc'];
$area_aut_total    = $data['general']['area_aut_total'];
$area_ver_predcat  = $data['general']['area_ver_predcat'];
$area_ver_viapub   = $data['general']['area_ver_viapub'];
$area_ver_bc       = $data['general']['area_ver_bc'];
$area_ver_total    = $data['general']['area_ver_total'];

$condicion        = $data['conductor']['condicion'];
$nombre_comercial = $data['conductor']['nombre_comercial'];
$id_persona       = $data['conductor']['id_persona'];
$tip_persona      = $data['conductor']['tip_persona'];
$tip_doc          = $data['conductor']['tip_doc'];
$nro_doc          = $data['conductor']['nro_doc'];
$nombres          = $data['conductor']['nombres'] . ' ' . $data['conductor']['ape_paterno'] . ' ' . $data['conductor']['ape_materno'];
$nro_ruc_2        = $data['conductor']['nro_ruc_2'];
$des_tip_persona  = $data['conductor']['des_tip_persona'];
$des_tip_doc      = $data['conductor']['des_tip_doc'];
$des_condicion    = $data['conductor']['des_condicion'];

$id_ubi_geo     = $data['domicilio']['id_ubi_geo'];
$telefono       = $data['domicilio']['telefono'];
$anexo          = $data['domicilio']['anexo'];
$fax            = $data['domicilio']['fax'];
$correo_elect   = $data['domicilio']['correo_elect'];
$cod_via        = $data['domicilio']['cod_via'];
$cod_hab_urb    = $data['domicilio']['cod_hab_urb'];
$nro_municipal  = $data['domicilio']['nro_municipal'];
$nombre_edifica = $data['domicilio']['nombre_edifica'];
$nro_interior   = $data['domicilio']['nro_interior'];
$mza_urbana     = $data['domicilio']['mza_urbana'];
$lote_urbano    = $data['domicilio']['lote_urbano'];
$sub_lote       = $data['domicilio']['sub_lote'];
$departamento   = $data['domicilio']['departamento'];
$provincia      = $data['domicilio']['provincia'];
$distrito       = $data['domicilio']['distrito'];
$des_via        = $data['domicilio']['descrip_via'];
$des_hab_urb    = $data['domicilio']['descrip_hab_urb'];

$dctos_presentados = $data['complementaria']['dctos_presentados'];
$condic_declara    = $data['complementaria']['condic_declara'];
$estado_llenado    = $data['complementaria']['estado_llenado'];
$mantenimiento     = $data['complementaria']['mantenimiento'];
$observaciones     = $data['complementaria']['observaciones'];
$des_doc_pres      = $data['complementaria']['des_doc_pres'];
$des_cond_declara  = $data['complementaria']['des_cond_declara'];
$des_est_llenado   = $data['complementaria']['des_est_llenado'];
$des_mantenimiento = $data['complementaria']['des_mantenimiento'];

$firma_declarante      = $data['otros']['firma_declarante'];
$id_declarante         = $data['otros']['id_declarante'];
$fecha_declarante      = $data['otros']['fecha_declarante'];
$firma_supervisor      = $data['otros']['firma_supervisor'];
$id_supervisor         = $data['otros']['id_supervisor'];
$fecha_supervisor      = $data['otros']['fecha_supervisor'];
$firma_tec_catastral   = $data['otros']['firma_tec_catastral'];
$id_tec_catastral      = $data['otros']['id_tec_catastral'];
$fecha_tec_catastral   = $data['otros']['fecha_tec_catastral'];
$firma_verif_catastral = $data['otros']['firma_verif_catastral'];

$id_verif_catastral    = $data['otros']['id_verif_catastral'];
$fecha_verif_catastral = $data['otros']['fecha_verif_catastral'];

$nom_declarante      = $data['otros']['nom_declarante'];
$nom_supervisor      = $data['otros']['nom_supervisor'];
$nom_tec_catastral   = $data['otros']['nom_tec_catastral'];
$nom_verif_catastral = $data['otros']['nom_verif_catastral'];

$firma_tec_calidad = $data['otros']['firma_tec_calidad'];
$id_tec_calidad    = $data['otros']['id_tec_calidad'];
$fecha_tec_calidad = $data['otros']['fecha_tec_calidad'];
$nom_tec_calidad   = $data['otros']['nom_tec_calidad'];

$insert_digitador = $data['digitador']['insert_digitador'];
$insert_fecha     = $data['digitador']['fec_in'];
$update_digitador = $data['digitador']['update_digitador'];
$update_fecha     = $data['digitador']['fec_up'];
$digi_in          = $data['digitador']['digi_in'];
$digi_up          = $data['digitador']['digi_up'];
?>
<div class="page-head">
    <div class="container">
        <div class="page-title">
            <h4 class="h4">FICHA CATASTRAL ECONOMICA N° <?= $nro_ficha ?></h4>
        </div>
        <div class="page-toolbar">
            <div class="btn-group btn-theme-panel">
                <a href="<?= Yii::app()->request->urlReferrer ?>" class="btn btn-primary" >
                    Regresar
                </a>
            </div>
        </div>
    </div>
</div>
<div class="page-content">
    <div class="container">
        <!-- BEGIN PAGE CONTENT INNER -->
        <div class="page-content-inner">
            <div class="bordered">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="portlet light">
                            <div class="portlet-title">
                                <h4 class="h4">CODIGO DE REFERENCIA CATASTRAL</h4>
                            </div>
                            <div class="portlet-body">
                                <div class="table-responsive">
                                    <table class="table table-bordered table-condensed">
                                        <thead class="bg-gray">
                                            <tr>
                                                <th>UBIGEO</th>
                                                <th>SECTOR</th>
                                                <th>MANZANA</th>
                                                <th>LOTE</th>
                                                <th>EDIF</th>
                                                <th>ENTRADA</th>
                                                <th>PISO</th>
                                                <th>UNIDAD</th>
                                                <th>DC</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td class="bg-white"><?= substr($cod_cat, 0, 6); ?></td>
                                                <td class="bg-white"><?= substr($cod_cat, 10, 2); ?></td>
                                                <td class="bg-white"><?= substr($cod_cat, 12, 3); ?></td>
                                                <td class="bg-white"><?= substr($cod_cat, 15, 3); ?></td>
                                                <td class="bg-white"><?= substr($cod_cat, 18, 2); ?></td>
                                                <td class="bg-white"><?= substr($cod_cat, 20, 2); ?></td>
                                                <td class="bg-white"><?= substr($cod_cat, 22, 2); ?></td>
                                                <td class="bg-white"><?= substr($cod_cat, 24, 3); ?></td>
                                                <td class="bg-white"><?= UReporte::dig_check($cod_cat); ?></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="table-responsive">
                                    <table class="table table-bordered table-condensed">
                                        <thead class="bg-gray">
                                            <tr>
                                                <th colspan="2" class="bg-gray">CODIGO UNICO CATASTRAL - CUC</th>
                                                <th>CODIGO HOJA CATASTRAL</th>
                                                <th colspan="2" class="bg-gray">N° FICHA POR LOTE</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td class="bg-white"><?= substr($cuc, 0, 8); ?></td>
                                                <td class="bg-white"><?= substr($cuc, 8, 4); ?></td>
                                                <td class="bg-white"><?= $cod_hoja_cat; ?></td>
                                                <td class="bg-white"><?= substr($nro_ficha_lote, 0, 4); ?></td>
                                                <td class="bg-white"><?= substr($nro_ficha_lote, 5, 4); ?></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12">
                        <div class="portlet light">
                            <div class="portlet-title">
                                <h4 class="h4">IDENTIFICADOR DEL CONDUCTOR</h4>
                            </div>
                            <div class="portlet-body">
                                <div class="table-responsive">
                                    <table class="table table-bordered table-condensed">
                                        <tbody>
                                            <tr>
                                                <td class="bg-gray">TIPO TITULAR</td>
                                                <td class="bg-white"><?= $tip_persona . ' ' . $des_tip_persona; ?></td>
                                                <td class="bg-gray">NOMBRE COMERCIAL</td>
                                                <td class="bg-white"><?= $nombre_comercial; ?></td>
                                                <td class="bg-gray">COND. CONDUCTOR</td>
                                                <td class="bg-white"><?= $condicion . ' ' . $des_condicion; ?></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="table-responsive">
                                    <table class="table table-bordered table-condensed">
                                        <thead class="bg-gray">
                                            <tr>
                                                <th>TIPO DOC. IDENTIDAD</th>
                                                <th>N° DOCUMENTO</th>
                                                <th>NOMBRE Y APELLIDOS O RAZON SOCIAL</th>
                                                <th>RUC</th>

                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td class="bg-white"><?= $tip_doc . ' ' . $des_tip_doc; ?></td>
                                                <td class="bg-white"><?= $nro_doc; ?></td>
                                                <td class="bg-white"><?= $nombres; ?></td>
                                                <td class="bg-white"><?= $nro_ruc_2; ?></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="table-responsive">
                                    <table class="table table-bordered table-condensed">
                                        <thead class="bg-gray">
                                            <tr>
                                                <th colspan="7" class="bg-gray">DOMICILIO FISCAL DEL CONDUCTOR DE LA ACTIVIDAD</th>
                                            </tr>
                                            <tr>
                                                <th>DEPARTAMENTO</th>
                                                <th>PROVINCIA</th>
                                                <th>DISTRITO</th>
                                                <th>TELEFONO</th>
                                                <th>ANEXO</th>
                                                <th>FAX</th>
                                                <th>CORREO ELECTRONICO</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td class="bg-white"><?= $departamento; ?></td>
                                                <td class="bg-white"><?= $provincia; ?></td>
                                                <td class="bg-white"><?= $distrito; ?></td>
                                                <td class="bg-white"><?= $telefono; ?></td>
                                                <td class="bg-white"><?= $anexo; ?></td>
                                                <td class="bg-white"><?= $fax; ?></td>
                                                <td class="bg-white"><?= $correo_elect; ?></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="table-responsive">
                                    <table class="table table-bordered table-condensed">
                                        <thead class="bg-gray">
                                            <tr>
                                                <th>COD. VIA</th>
                                                <th>NOMBRE DE VIA</th>
                                                <th>N° MUNICIP.</th>
                                                <th>NOMBRE DE LA EDIFICACION</th>
                                                <th>N° INTERIOR</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td class="bg-white"><?= $cod_via; ?></td>
                                                <td class="bg-white"><?= $des_via; ?></td>
                                                <td class="bg-white"><?= $nro_municipal; ?></td>
                                                <td class="bg-white"><?= $nombre_edifica; ?></td>
                                                <td class="bg-white"><?= $nro_interior; ?></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="table-responsive">
                                    <table class="table table-bordered table-condensed">
                                        <thead class="bg-gray">
                                            <tr>
                                                <th>CODIGO HU.</th>
                                                <th>NOMBRE HAB. URBANA</th>
                                                <th>MANZANA</th>
                                                <th>LOTE</th>
                                                <th>SUB.LOTE</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td class="bg-white"><?= $cod_hab_urb; ?></td>
                                                <td class="bg-white"><?= $des_hab_urb; ?></td>
                                                <td class="bg-white"><?= $mza_urbana; ?></td>
                                                <td class="bg-white"><?= $lote_urbano; ?></td>
                                                <td class="bg-white"><?= $sub_lote; ?></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12">
                        <div class="portlet light">
                            <div class="portlet-title">
                                <h4 class="h4">AUTORIZACION MUNICIPAL DE FUNCIONAMIENTO</h4>
                            </div>
                            <div class="portlet-body">
                                <div class="table-responsive">
                                    <table class="table table-bordered table-condensed">
                                        <tbody>
                                            <tr>
                                                <td class="bg-gray">N° EXPEDIENTE</td>
                                                <td class="bg-gray">N° LICENCIA</td>
                                                <td class="bg-gray">UBICACION</td>
                                                <td class="bg-gray">AREA AUTORIZADA</td>
                                                <td class="bg-gray">AREA VERIFICADA</td>
                                            </tr>
                                            <tr>
                                                <td class="bg-white"><?= $nro_expediente; ?></td>
                                                <td class="bg-white"><?= $nro_licencia; ?></td>
                                                <td class="bg-gray">PREDIO CATASTRAL</td>
                                                <td class="bg-white"><?= $area_aut_predcat; ?></td>
                                                <td class="bg-white"><?= $area_ver_predcat; ?></td>
                                            </tr>
                                            <tr>
                                                <td class="bg-gray">F. EXPEDICION</td>
                                                <td class="bg-gray">F. VENCIMIENTO</td>
                                                <td class="bg-gray">VIA PUBLICA</td>
                                                <td class="bg-white"><?= $area_aut_viapub; ?></td>
                                                <td class="bg-white"><?= $area_ver_viapub; ?></td>                                        
                                            </tr>
                                            <tr>
                                                <td class="bg-white"><?= ($fecha_expedicion != null) ? (substr($fecha_expedicion, 0, 4) > '1900') ? date_format(new DateTime($fecha_expedicion), 'd/m/Y') : "" : ""; ?></td>
                                                <td class="bg-white"><?= ($fecha_vencimiento != null) ? (substr($fecha_vencimiento, 0, 4) > '1900') ? date_format(new DateTime($fecha_vencimiento), 'd/m/Y') : "" : ""; ?></td>
                                                <td class="bg-gray">BIEN COMUN</td>
                                                <td class="bg-white"><?= $area_aut_bc; ?></td>
                                                <td class="bg-white"><?= $area_ver_bc; ?></td>
                                            </tr>
                                            <tr>
                                                <td class="bg-gray">INICIO DE ACTIV.</td>
                                                <td class="bg-white"><?= ($inicio_actividad != null) ? (substr($inicio_actividad, 0, 4) > '1900') ? date_format(new DateTime($inicio_actividad), 'd/m/Y') : "" : ""; ?></td>
                                                <td class="bg-gray">TOTAL</td>
                                                <td class="bg-white"><?= $area_aut_total; ?></td>
                                                <td class="bg-white"><?= $area_ver_total; ?></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="table-responsive">
                                    <table class="table table-bordered table-condensed">
                                        <thead class="bg-gray">
                                            <tr>
                                                <th>CODIGO</th>
                                                <th>DESCRIPCION DE LA ACTIVIDAD</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            foreach ($data['descripcionActividad'] as $descripcionActividad):
                                                $cod_actividad  = $descripcionActividad['cod_actividad'];
                                                $desc_actividad = $descripcionActividad['desc_actividad'];
                                                ?>
                                                <tr>
                                                    <td class="bg-white"><?= $cod_actividad; ?></td>
                                                    <td class="bg-white"><?= $desc_actividad; ?></td>
                                                </tr>
                                            <?php endforeach; ?>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="table-responsive">
                                    <table class="table table-bordered table-condensed">
                                        <thead class="bg-gray">
                                            <tr>
                                                <th>N°</th>
                                                <th>CODIGO</th>
                                                <th>DESCRIPCION DEL TIPO DE ANUNCIO</th>
                                                <th>N° DE LADOS</th>
                                                <th>AREA AUTORIZADA DEL ANUNCIO (M2)</th>
                                                <th>AREA VERIFICADA DEL ANUNCIO (M2)</th>
                                                <th>N° EXPEDIENTE</th>
                                                <th>N° LICENCIA</th>
                                                <th>FECHA DE EXPEDICION</th>
                                                <th>FECHA DE VENCIMIENTO</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            foreach ($data['descripcionAnuncio'] as $descripcionAnuncio):
                                                $nro               = $descripcionAnuncio['nro'];
                                                $cod_anuncio       = $descripcionAnuncio['cod_anuncio'];
                                                $nro_lados         = $descripcionAnuncio['nro_lados'];
                                                $area_aut_anuncio  = $descripcionAnuncio['area_aut_anuncio'];
                                                $area_ver_anuncio  = $descripcionAnuncio['area_ver_anuncio'];
                                                $nro_expediente    = $descripcionAnuncio['nro_expediente'];
                                                $nro_licencia      = $descripcionAnuncio['nro_licencia'];
                                                $fecha_expedicion  = $descripcionAnuncio['fecha_expedicion'];
                                                $fecha_vencimiento = $descripcionAnuncio['fecha_vencimiento'];
                                                $desc_anuncio      = $descripcionAnuncio['desc_anuncio'];
                                                ?>
                                                <tr>
                                                    <td class="bg-white"><?= $nro; ?></td>
                                                    <td class="bg-white"><?= $cod_anuncio; ?></td>
                                                    <td class="bg-white"><?= $nro_lados; ?></td>
                                                    <td class="bg-white"><?= $area_aut_anuncio; ?></td>
                                                    <td class="bg-white"><?= $area_ver_anuncio; ?></td>
                                                    <td class="bg-white"><?= $nro_expediente; ?></td>
                                                    <td class="bg-white"><?= $nro_licencia; ?></td>
                                                    <td class="bg-white"><?= $fecha_expedicion; ?></td>
                                                    <td class="bg-white"><?= $fecha_vencimiento; ?></td>
                                                    <td class="bg-white"><?= $desc_anuncio; ?></td>
                                                </tr>
                                            <?php endforeach; ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12">
                        <div class="portlet light">
                            <div class="portlet-title">
                                <h4 class="h4">INFORMACION COMPLEMENTARIA</h4>
                            </div>
                            <div class="portlet-body">
                                <div class="table-responsive">
                                    <table class="table table-bordered table-condensed">
                                        <tbody>
                                            <tr>
                                                <td class="bg-gray">DOC. PRESENTADOS</td>
                                                <td class="bg-gray">COND. DECLARANTE</td>
                                                <td class="bg-gray">ESTAD. LLENADO</td>
                                                <td class="bg-gray">MANTENIMIENTO</td>
                                            </tr>
                                            <tr>
                                                <td class="bg-white"><?= $dctos_presentados . ' ' . $des_doc_pres; ?></td>
                                                <td class="bg-white"><?= $condic_declara . ' ' . $des_cond_declara; ?></td>
                                                <td class="bg-white"><?= $estado_llenado . ' ' . $des_est_llenado; ?></td>
                                                <td class="bg-white"><?= $mantenimiento . ' ' . $des_mantenimiento; ?></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="table-responsive">
                                    <table class="table table-bordered table-condensed">
                                        <tbody>
                                            <tr>
                                                <td class="bg-gray">OBSERVACIONES</td>
                                            </tr>
                                            <tr>
                                                <td class="bg-white"><?= $observaciones; ?></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12">
                        <div class="portlet light">
                            <div class="portlet-title">
                                <h4 class="h4">OTROS</h4>
                            </div>
                            <div class="portlet-body">
                                <div class="table-responsive">
                                    <table class="table table-bordered table-condensed">
                                        <tbody>
                                            <tr>
                                                <td colspan="2" class="bg-gray">FIRMA DEL DECLARANTE <input name="firma_declarante" type="checkbox" disabled="disabled" <?= ($firma_declarante == 1) ? "checked" : "" ?>/></td>
                                                <td colspan="2" class="bg-gray">FIRMA DEL SUPERVISOR <input name="firma_supervisor" type="checkbox" disabled="disabled" <?= ($firma_supervisor == 1) ? "checked" : "" ?>/></td>
                                                <td colspan="2" class="bg-gray">FIRMA DEL TECNICO CATASTRAL <input name="firma_tec_catastral" type="checkbox" disabled="disabled" <?= ($firma_tec_catastral == 1) ? "checked" : "" ?>/></td>
                                                <td colspan="2" class="bg-gray">V° B° DEL TECNICO CONTROL DE CALIDAD <input name="firma_tec_calidad" type="checkbox" disabled="disabled" <?= ($firma_tec_calidad == 1) ? "checked" : "" ?>/></td>
                                                <td colspan="2" class="bg-gray">V° B° DEL VERIFICADOR CATASTRAL <input name="firma_verif_catastral" type="checkbox" disabled="disabled" <?= ($firma_verif_catastral == 1) ? "checked" : "" ?>/></td>
                                            </tr>
                                            <tr>
                                                <td class="bg-gray">DNI</td>
                                                <td class="bg-white"><?= $id_declarante; ?></td>
                                                <td class="bg-gray">CODIGO</td>
                                                <td class="bg-white"><?= $id_supervisor; ?></td>
                                                <td class="bg-gray">CODIGO</td>
                                                <td class="bg-white"><?= $id_tec_catastral; ?></td>
                                                <td class="bg-gray">CODIGO</td>
                                                <td class="bg-white"><?= $id_tec_calidad; ?></td>
                                                <td class="bg-gray">CODIGO</td>
                                                <td class="bg-white"><?= $id_verif_catastral; ?></td>
                                            </tr>
                                            <tr>
                                                <td class="bg-gray">FECHA</td>
                                                <td class="bg-white"><?= ($fecha_declarante != null) ? (substr($fecha_declarante, 0, 4) > '1900') ? date_format(new DateTime($fecha_declarante), 'd/m/Y') : "" : ""; ?></td>
                                                <td class="bg-gray">FECHA</td>
                                                <td class="bg-white"><?= ($fecha_supervisor != null) ? (substr($fecha_supervisor, 0, 4) > '1900') ? date_format(new DateTime($fecha_supervisor), 'd/m/Y') : "" : ""; ?></td>
                                                <td class="bg-gray">FECHA</td>
                                                <td class="bg-white"><?= ($fecha_tec_catastral != null) ? (substr($fecha_tec_catastral, 0, 4) > '1900') ? date_format(new DateTime($fecha_tec_catastral), 'd/m/Y') : "" : ""; ?></td>
                                                <td class="bg-gray">FECHA</td>
                                                <td class="bg-white"><?= ($fecha_tec_calidad != null) ? (substr($fecha_tec_calidad, 0, 4) > '1900') ? date_format(new DateTime($fecha_tec_calidad), 'd/m/Y') : "" : ""; ?></td>
                                                <td class="bg-gray">FECHA</td>
                                                <td class="bg-white"><?= ($fecha_verif_catastral != null) ? (substr($fecha_verif_catastral, 0, 4) > '1900') ? date_format(new DateTime($fecha_verif_catastral), 'd/m/Y') : "" : ""; ?></td>
                                            </tr>
                                            <tr>
                                                <td class="bg-white" colspan="2"><?= $nom_declarante; ?></td>
                                                <td class="bg-white" colspan="2"><?= $nom_supervisor; ?></td>
                                                <td class="bg-white" colspan="2"><?= $nom_tec_catastral; ?></td>
                                                <td class="bg-white" colspan="2"><?= $nom_tec_calidad; ?></td>
                                                <td class="bg-white" colspan="2"><?= $nom_verif_catastral; ?></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="table-responsive">
                                    <table class="table table-bordered table-condensed">
                                        <tbody>
                                            <tr>
                                                <td class="bg-gray">CREADO</td>
                                                <td class="bg-white"><?= $digi_in; ?></td>
                                                <td class="bg-white"><?= $insert_fecha; ?></td>
                                                <td class="bg-gray">MODIFICADO</td>
                                                <td class="bg-white"><?= $digi_up; ?></td>
                                                <td class="bg-white"><?= $update_fecha; ?></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>