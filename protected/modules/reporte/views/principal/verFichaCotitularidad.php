<?php
//Utils::show($data,true);
$cod_lote       = $data['general']['cod_lote'];
$id_ficha       = $data['general']['id_ficha'];
$nro_ficha      = $data['general']['nro_ficha'];
$tip_ficha      = $data['general']['tip_ficha'];
$cuc            = $data['general']['cuc'];
$cod_hoja_cat   = $data['general']['cod_hoja_cat'];
$nro_ficha_lote = $data['general']['nro_ficha_lote'];
$cod_cat        = $data['general']['cod_cat'];

$condic_declara   = $data['complementaria']['condic_declara'];
$estado_llenado   = $data['complementaria']['estado_llenado'];
$observaciones    = $data['complementaria']['observaciones'];
$des_cond_declara = $data['complementaria']['des_cond_declara'];
$des_est_llenado  = $data['complementaria']['des_est_llenado'];

$firma_declarante      = $data['otros']['firma_declarante'];
$id_declarante         = $data['otros']['id_declarante'];
$fecha_declarante      = $data['otros']['fecha_declarante'];
$firma_supervisor      = $data['otros']['firma_supervisor'];
$id_supervisor         = $data['otros']['id_supervisor'];
$fecha_supervisor      = $data['otros']['fecha_supervisor'];
$firma_tec_catastral   = $data['otros']['firma_tec_catastral'];
$id_tec_catastral      = $data['otros']['id_tec_catastral'];
$fecha_tec_catastral   = $data['otros']['fecha_tec_catastral'];
$firma_verif_catastral = $data['otros']['firma_verif_catastral'];

$id_verif_catastral    = $data['otros']['id_verif_catastral'];
$fecha_verif_catastral = $data['otros']['fecha_verif_catastral'];

$nom_declarante      = $data['otros']['nom_declarante'];
$nom_supervisor      = $data['otros']['nom_supervisor'];
$nom_tec_catastral   = $data['otros']['nom_tec_catastral'];
$nom_verif_catastral = $data['otros']['nom_verif_catastral'];

$firma_tec_calidad = $data['otros']['firma_tec_calidad'];
$id_tec_calidad    = $data['otros']['id_tec_calidad'];
$fecha_tec_calidad = $data['otros']['fecha_tec_calidad'];
$nom_tec_calidad   = $data['otros']['nom_tec_calidad'];

$insert_digitador = $data['digitador']['insert_digitador'];
$insert_fecha     = $data['digitador']['fec_in'];
$update_digitador = $data['digitador']['update_digitador'];
$update_fecha     = $data['digitador']['fec_up'];
$digi_in          = $data['digitador']['digi_in'];
$digi_up          = $data['digitador']['digi_up'];
?>
<div class="page-head">
    <div class="container">
        <div class="page-title">
            <h4 class="h4">FICHA CATASTRAL COTITULARIDAD N° <?= $nro_ficha ?></h4>
        </div>
        <div class="page-toolbar">
            <div class="btn-group btn-theme-panel">
                <a href="<?= Yii::app()->request->urlReferrer ?>" class="btn btn-primary" >
                    Regresar
                </a>
            </div>
        </div>
    </div>
</div>
<div class="page-content">
    <div class="container">
        <!-- BEGIN PAGE CONTENT INNER -->
        <div class="page-content-inner">
            <div class="bordered">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="portlet light">
                            <div class="portlet-title">
                                <h4 class="h4">CODIGO DE REFERENCIA CATASTRAL</h4>
                            </div>
                            <div class="portlet-body">
                                <div class="table-responsive">
                                    <table class="table table-bordered table-condensed">
                                        <thead class="bg-gray">
                                            <tr>
                                                <th>UBIGEO</th>
                                                <th>SECTOR</th>
                                                <th>MANZANA</th>
                                                <th>LOTE</th>
                                                <th>EDIF</th>
                                                <th>ENTRADA</th>
                                                <th>PISO</th>
                                                <th>UNIDAD</th>
                                                <th>DC</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td class="bg-white"><?= substr($cod_cat, 0, 6); ?></td>
                                                <td class="bg-white"><?= substr($cod_cat, 10, 2); ?></td>
                                                <td class="bg-white"><?= substr($cod_cat, 12, 3); ?></td>
                                                <td class="bg-white"><?= substr($cod_cat, 15, 3); ?></td>
                                                <td class="bg-white"><?= substr($cod_cat, 18, 2); ?></td>
                                                <td class="bg-white"><?= substr($cod_cat, 20, 2); ?></td>
                                                <td class="bg-white"><?= substr($cod_cat, 22, 2); ?></td>
                                                <td class="bg-white"><?= substr($cod_cat, 24, 3); ?></td>
                                                <td class="bg-white"><?= UReporte::dig_check($cod_cat); ?></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="table-responsive">
                                    <table class="table table-bordered table-condensed">
                                        <thead class="bg-gray">
                                            <tr>
                                                <th colspan="2" class="bg-gray">CODIGO UNICO CATASTRAL - CUC</th>
                                                <th>CODIGO HOJA CATASTRAL</th>
                                                <th colspan="2" class="bg-gray">N° FICHA POR LOTE</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td class="bg-white"><?= substr($cuc, 0, 8); ?></td>
                                                <td class="bg-white"><?= substr($cuc, 8, 4); ?></td>
                                                <td class="bg-white"><?= $cod_hoja_cat; ?></td>
                                                <td class="bg-white"><?= substr($nro_ficha_lote, 0, 4); ?></td>
                                                <td class="bg-white"><?= substr($nro_ficha_lote, 5, 4); ?></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12">
                        <div class="portlet light">
                            <div class="portlet-title">
                                <h4 class="h4">IDENTIFICADOR DEL COTITULAR CATASTRAL</h4>
                            </div>
                            <div class="portlet-body">
                                <div class="table-responsive">
                                    <table class="table table-bordered table-condensed">
                                        <thead class="bg-gray">
                                            <tr>
                                                <th>ID</th>
                                                <th>CODIGO</th>
                                                <th>TIPO_DOC</th>
                                                <th>N° DOC</th>
                                                <th>TITULAR</th>
                                                <th>UBIGEO</th>
                                                <th>TELEFONO</th>
                                                <th>MAIL</th>
                                                <th>POR. PERT</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            $cont             = 1;
                                            foreach ($data['cotitular'] as $cotitular):
                                                $id_persona             = $cotitular['id_persona'];
                                                $tip_doc                = $cotitular['tip_doc'];
                                                $nro_doc                = $cotitular['nro_doc'];
                                                $nombres                = $cotitular['ape_patErno'] . ' ' . $cotitular['ape_matErno'] . ', ' . $cotitular['nombres'];
                                                $telefono               = $cotitular['telefono'];
                                                $correo_elect           = $cotitular['correo_elect'];
                                                $porcentaje_pertenencia = $cotitular['porcentaje_pertenencia'];
                                                $ubigeo                 = $cotitular['departamento'] . '/' . $cotitular['provincia'] . '/' . $cotitular['distrito'];
                                                ?>
                                                <tr>
                                                    <td class="bg-white"><?= $cont; ?></td>
                                                    <td class="bg-white"><?= $id_persona; ?></td>
                                                    <td class="bg-white"><?= $tip_doc; ?></td>
                                                    <td class="bg-white"><?= $nro_doc; ?></td>
                                                    <td class="bg-white"><?= $nombres; ?></td>
                                                    <td class="bg-white"><?= $telefono; ?></td>
                                                    <td class="bg-white"><?= $correo_elect; ?></td>
                                                    <td class="bg-white"><?= $porcentaje_pertenencia; ?></td>
                                                    <td class="bg-white"><?= $ubigeo; ?></td>
                                                </tr>
                                                <?php
                                                $cont++;
                                            endforeach;
                                            ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12">
                        <div class="portlet light">
                            <div class="portlet-title">
                                <h4 class="h4">INFORMACION COMPLEMENTARIA</h4>
                            </div>
                            <div class="portlet-body">
                                <div class="table-responsive">
                                    <table class="table table-bordered table-condensed">
                                        <tbody>
                                            <tr>
                                                <td class="bg-gray">COND. DECLARANTE</td>
                                                <td class="bg-white"><?= $condic_declara . ' ' . $des_cond_declara; ?></td>
                                                <td class="bg-gray">ESTAD. LLENADO</td>
                                                <td class="bg-white"><?= $estado_llenado . ' ' . $des_est_llenado; ?></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="table-responsive">
                                    <table class="table table-bordered table-condensed">
                                        <tbody>
                                            <tr>
                                                <td class="bg-gray">OBSERVACIONES</td>
                                            </tr>
                                            <tr>
                                                <td class="bg-white"><?= $observaciones; ?></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12">
                        <div class="portlet light">
                            <div class="portlet-title">
                                <h4 class="h4">OTROS</h4>
                            </div>
                            <div class="portlet-body">
                                <div class="table-responsive">
                                    <table class="table table-bordered table-condensed">
                                        <tbody>
                                            <tr>
                                                <td colspan="2" class="bg-gray">FIRMA DEL DECLARANTE <input name="firma_declarante" type="checkbox" disabled="disabled" <?= ($firma_declarante == 1) ? "checked" : "" ?>/></td>
                                                <td colspan="2" class="bg-gray">FIRMA DEL SUPERVISOR <input name="firma_supervisor" type="checkbox" disabled="disabled" <?= ($firma_supervisor == 1) ? "checked" : "" ?>/></td>
                                                <td colspan="2" class="bg-gray">FIRMA DEL TECNICO CATASTRAL <input name="firma_tec_catastral" type="checkbox" disabled="disabled" <?= ($firma_tec_catastral == 1) ? "checked" : "" ?>/></td>
                                                <td colspan="2" class="bg-gray">V° B° DEL TECNICO CONTROL DE CALIDAD <input name="firma_tec_calidad" type="checkbox" disabled="disabled" <?= ($firma_tec_calidad == 1) ? "checked" : "" ?>/></td>
                                                <td colspan="2" class="bg-gray">V° B° DEL VERIFICADOR CATASTRAL <input name="firma_verif_catastral" type="checkbox" disabled="disabled" <?= ($firma_verif_catastral == 1) ? "checked" : "" ?>/></td>
                                            </tr>
                                            <tr>
                                                <td class="bg-gray">DNI</td>
                                                <td class="bg-white"><?= $id_declarante; ?></td>
                                                <td class="bg-gray">CODIGO</td>
                                                <td class="bg-white"><?= $id_supervisor; ?></td>
                                                <td class="bg-gray">CODIGO</td>
                                                <td class="bg-white"><?= $id_tec_catastral; ?></td>
                                                <td class="bg-gray">CODIGO</td>
                                                <td class="bg-white"><?= $id_tec_calidad; ?></td>
                                                <td class="bg-gray">CODIGO</td>
                                                <td class="bg-white"><?= $id_verif_catastral; ?></td>
                                            </tr>
                                            <tr>
                                                <td class="bg-gray">FECHA</td>
                                                <td class="bg-white"><?= ($fecha_declarante != null) ? (substr($fecha_declarante, 0, 4) > '1900') ? date_format(new DateTime($fecha_declarante), 'd/m/Y') : "" : ""; ?></td>
                                                <td class="bg-gray">FECHA</td>
                                                <td class="bg-white"><?= ($fecha_supervisor != null) ? (substr($fecha_supervisor, 0, 4) > '1900') ? date_format(new DateTime($fecha_supervisor), 'd/m/Y') : "" : ""; ?></td>
                                                <td class="bg-gray">FECHA</td>
                                                <td class="bg-white"><?= ($fecha_tec_catastral != null) ? (substr($fecha_tec_catastral, 0, 4) > '1900') ? date_format(new DateTime($fecha_tec_catastral), 'd/m/Y') : "" : ""; ?></td>
                                                <td class="bg-gray">FECHA</td>
                                                <td class="bg-white"><?= ($fecha_tec_calidad != null) ? (substr($fecha_tec_calidad, 0, 4) > '1900') ? date_format(new DateTime($fecha_tec_calidad), 'd/m/Y') : "" : ""; ?></td>
                                                <td class="bg-gray">FECHA</td>
                                                <td class="bg-white"><?= ($fecha_verif_catastral != null) ? (substr($fecha_verif_catastral, 0, 4) > '1900') ? date_format(new DateTime($fecha_verif_catastral), 'd/m/Y') : "" : ""; ?></td>
                                            </tr>
                                            <tr>
                                                <td class="bg-white" colspan="2"><?= $nom_declarante; ?></td>
                                                <td class="bg-white" colspan="2"><?= $nom_supervisor; ?></td>
                                                <td class="bg-white" colspan="2"><?= $nom_tec_catastral; ?></td>
                                                <td class="bg-white" colspan="2"><?= $nom_tec_calidad; ?></td>
                                                <td class="bg-white" colspan="2"><?= $nom_verif_catastral; ?></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="table-responsive">
                                    <table class="table table-bordered table-condensed">
                                        <tbody>
                                            <tr>
                                                <td class="bg-gray">CREADO</td>
                                                <td class="bg-white"><?= $digi_in; ?></td>
                                                <td class="bg-white"><?= $insert_fecha; ?></td>
                                                <td class="bg-gray">MODIFICADO</td>
                                                <td class="bg-white"><?= $digi_up; ?></td>
                                                <td class="bg-white"><?= $update_fecha; ?></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>