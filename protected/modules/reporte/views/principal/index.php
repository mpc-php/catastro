<div class="page-head">
    <div class="container">
        <h2 class="text-center reporte-titulo"><?= $data['titulo'] ?></h2>
    </div>
</div>

<div class="page-content">
    <div class="container">
        <!-- BEGIN PAGE CONTENT INNER -->
        <div class="page-content-inner">
            <div class="search-page search-content-4">
                <div class="portlet light">
                    <div class="portlet-title">
                        <h3>
                            <i class="fa fa-filter"></i>
                            Filtro
                        </h3>
                    </div>
                    <div class="portlet-body">
                        <div class="row">
                            <div class="col-lg-12">
                                <table class="table table-bordered table-condensed">
                                    <tbody>
                                        <?php $this->renderPartial('partials/_' . $data['accion'], []); ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="footer-body text-center">
                            <div class="input-group hidden">
                                <input id="buscador_generico" type="text" class="form-control" placeholder="Buscar...">
                            </div>
                            <button id="buscador_generico_btn" class="btn btn-sm grey-salsa uppercase bold" type="button">
                                <i class="fa fa-search"></i> Buscar
                            </button>

                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- END PAGE CONTENT INNER -->
    </div>
</div>
<div class="page-content">
    <div class="container-fluid">
        <div class="portlet light">

            <div class="portlet-body">
                <h3 class="text-center bold">
                    <i class="fa fa-table"></i>
                    Resultados  Obtenidos
                </h3>
                <hr/>
                <div class="text-right">
                    <label>Acciones: </label>
                    <button id="btn-export" class="btn btn-sm green uppercase bold" type="button">
                        Exportar XLS
                    </button>
                </div>
                <br>
                <div class="search-table table-responsive">
                    <table id="tdreporte" class="table table-bordered table-striped table-condensed table-custom">
                        <thead class="bg-gray"></thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>