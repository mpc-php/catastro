<?php

/**
 * Clase que se encargará de funciones a ser reutilizadas en el modulo Reportes
 *
 * @author Francisco Isasi Chiesa <franco16fra@gmail.com>
 * @package Catastro\Modules\Reporte\Components
 */
class UReporte {

    public static function dig_check($cod_cat) {
        $sec = (int) substr($cod_cat, 10, 2);
        $man = (int) substr($cod_cat, 12, 3);
        $lot = (int) substr($cod_cat, 15, 3);
        $edi = (int) substr($cod_cat, 18, 2);
        $ent = (int) substr($cod_cat, 20, 2);
        $pis = (int) substr($cod_cat, 22, 2);
        $uni = (int) substr($cod_cat, 24, 3);
        $dc  = ($sec + $man + $lot + $edi + $ent + $pis + $uni) % 9;
        return $dc;
    }

}
