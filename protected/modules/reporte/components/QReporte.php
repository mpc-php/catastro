<?php

/**
 * Clase que se encarga de las funciones que se conectarán a la Base de Datos para el modulo Reportes
 *
 * @author Francisco Isasi Chiesa <franco16fra@gmail.com>
 * @package Catastro\Modules\Reporte\Components
 */
class QReporte {

    /**
     * Funcion que obtiene la consulta para los reportes a ser mostrados
     * 
     * @param String $tabla Tabla o Vista a ser consultada de la base de datos
     * @param Array $params Parametros a ser utilizados en el WHERE de la consulta
     * @param Array $columnas Columnas a ser obtenidas en el SELECT
     * @return Array
     */
    public static function get_ConsultaContribuyente($tabla, $params, $columnas) {
        $filtro            = (isset($params['search'])) ? strtolower($params['search']) : "";
        $sql_where         = "%{$filtro}%";
        $columnas_anexadas = implode(",", $columnas);
        $columnas_arreglo  = [];
        foreach ($columnas as $k => $v) {
            $columnas_arreglo[$k] = "CAST(" . $v . " AS VARCHAR)";
        }
        $columnas_concatenadas = implode("+", $columnas_arreglo);

        $sql_total = "SELECT *
                        FROM (SELECT {$columnas_concatenadas} CONCATENADO,{$columnas_anexadas} FROM {$tabla}) xyz
                        WHERE LOWER(xyz.CONCATENADO) LIKE '{$sql_where}' {$params['where']}";

        $command = Yii::app()->db->createCommand($sql_total);
        $models  = $command->queryAll();

        return $models;
    }

    /**
     * Funcion que obtiene la Data de una ficha específica
     * 
     * @param String $id_ficha Ficha a ser utilizada en la consulta.
     * @return Array
     */
    public static function get_ConsultaFicha($id_ficha) {
        $sql     = "EXECUTE SP_REPORTE_FICHA '{$id_ficha}'";
        $command = Yii::app()->db->createCommand($sql);
        return $command->queryRow();
    }

    /**
     * Funcion que obtiene la Data de una ficha de cotitularidad específica
     * 
     * @param String $cod_cat Codigo de Catastro a ser utilizada en la consulta.
     * @return Array
     */
    public static function get_ConsultaFichaCotitularidad($cod_cat) {
        $sql     = "select f.id_ficha, f.cod_cat from fichas f, fichas_condominio fc 
                    where f.id_ficha=fc.id_ficha 
                    and f.cod_cat= '{$cod_cat}'";
        $command = Yii::app()->db->createCommand($sql);
        return $command->queryRow();
    }

    /**
     * Funcion que obtiene la Data de una ficha de activiades economica específica
     * 
     * @param String $cod_cat Codigo de Catastro a ser utilizada en la consulta.
     * @return Array
     */
    public static function get_ConsultaFichaEconomica($cod_cat) {
        $sql     = "select f.id_ficha, f.cod_cat from fichas f, fichas_economicas fe
                    where f.id_ficha=fe.id_ficha 
                    and f.cod_cat= '{$cod_cat}'";
        $command = Yii::app()->db->createCommand($sql);
        return $command->queryRow();
    }

    /**
     * Funcion que obtiene la Data de una ficha de bien comun específica
     * 
     * @param String $cod_cat Codigo de Catastro a ser utilizada en la consulta.
     * @return Array
     */
    public static function get_ConsultaFichaBienComunCodCat($cod_cat) {
        $sql     = "select f.id_ficha, f.cod_cat from fichas f, fichas_bienes_comunes fbc
                    where f.id_ficha=fbc.id_ficha 
                    and f.cod_cat='{$cod_cat}'";
        $command = Yii::app()->db->createCommand($sql);
        return $command->queryRow();
    }

    /**
     * Funcion que obtiene la Data de una ficha-uicacion específica
     * 
     * @param String $id_ficha Ficha a ser utilizada en la consulta.
     * @return Array
     */
    public static function get_ConsultaFichaUbicacion($id_ficha) {
        $sql     = "EXECUTE SP_REPORTE_FICHA_UBICACION_PREDIO '{$id_ficha}'";
        $command = Yii::app()->db->createCommand($sql);
        return $command->queryAll();
    }

    /**
     * Funcion que obtiene la Data de una ficha-titularidad específica
     * 
     * @param String $id_ficha Ficha a ser utilizada en la consulta.
     * @return Array
     */
    public static function get_ConsultaFichaTitularidad($id_ficha) {
        $sql     = "EXECUTE SP_REPORTE_FICHA_TITULARIDAD '{$id_ficha}'";
        $command = Yii::app()->db->createCommand($sql);
        return $command->queryRow();
    }

    /**
     * Funcion que obtiene la Data de una ficha-titular específica
     * 
     * @param String $id_ficha Ficha a ser utilizada en la consulta.
     * @return Array
     */
    public static function get_ConsultaFichaTitular($id_ficha) {
        $sql     = "EXECUTE SP_REPORTE_FICHA_TITULAR '{$id_ficha}'";
        $command = Yii::app()->db->createCommand($sql);
        return $command->queryAll();
    }

    /**
     * Funcion que obtiene la Data de una ficha-descripcion del predio específica
     * 
     * @param String $id_ficha Ficha a ser utilizada en la consulta.
     * @return Array
     */
    public static function get_ConsultaFichaDescripcionPredio($id_ficha) {
        $sql     = "EXECUTE SP_REPORTE_FICHA_DESCRIPCION_PREDIO '{$id_ficha}'";
        $command = Yii::app()->db->createCommand($sql);
        return $command->queryRow();
    }

    /**
     * Funcion que obtiene la Data de una ficha-servicios basicos específica
     * 
     * @param String $id_ficha Ficha a ser utilizada en la consulta.
     * @return Array
     */
    public static function get_ConsultaFichaServiciosBasicos($id_ficha) {
        $sql     = "EXECUTE SP_REPORTE_FICHA_SERVICIOS_BASICOS '{$id_ficha}'";
        $command = Yii::app()->db->createCommand($sql);
        return $command->queryRow();
    }

    /**
     * Funcion que obtiene la Data de una ficha-construccion específica
     * 
     * @param String $id_ficha Ficha a ser utilizada en la consulta.
     * @return Array
     */
    public static function get_ConsultaFichaConstruccion($id_ficha) {
        $sql     = "EXECUTE SP_REPORTE_FICHA_CONSTRUCCION '{$id_ficha}'";
        $command = Yii::app()->db->createCommand($sql);
        return $command->queryAll();
    }

    /**
     * Funcion que obtiene la Data de una ficha-obra específica
     * 
     * @param String $id_ficha Ficha a ser utilizada en la consulta.
     * @return Array
     */
    public static function get_ConsultaFichaObra($id_ficha) {
        $sql     = "EXECUTE SP_REPORTE_FICHA_OBRAS '{$id_ficha}'";
        $command = Yii::app()->db->createCommand($sql);
        return $command->queryAll();
    }

    /**
     * Funcion que obtiene la Data de una ficha-documentos específica
     * 
     * @param String $id_ficha Ficha a ser utilizada en la consulta.
     * @return Array
     */
    public static function get_ConsultaFichaDocumentos($id_ficha) {
        $sql     = "EXECUTE SP_REPORTE_FICHA_DOCUMENTOS '{$id_ficha}'";
        $command = Yii::app()->db->createCommand($sql);
        return $command->queryAll();
    }

    /**
     * Funcion que obtiene la Data de una ficha-escritura específica
     * 
     * @param String $id_ficha Ficha a ser utilizada en la consulta.
     * @return Array
     */
    public static function get_ConsultaFichaEscritura($id_ficha) {
        $sql     = "EXECUTE SP_REPORTE_FICHA_ESCRITURA '{$id_ficha}'";
        $command = Yii::app()->db->createCommand($sql);
        return $command->queryRow();
    }

    /**
     * Funcion que obtiene la Data de una ficha-sunarp específica
     * 
     * @param String $id_ficha Ficha a ser utilizada en la consulta.
     * @return Array
     */
    public static function get_ConsultaFichaSunarp($id_ficha) {
        $sql     = "EXECUTE SP_REPORTE_FICHA_SUNARP '{$id_ficha}'";
        $command = Yii::app()->db->createCommand($sql);
        return $command->queryRow();
    }

    /**
     * Funcion que obtiene la Data de una ficha-complementaria específica
     * 
     * @param String $id_ficha Ficha a ser utilizada en la consulta.
     * @return Array
     */
    public static function get_ConsultaFichaComplementaria($id_ficha) {
        $sql     = "EXECUTE SP_REPORTE_FICHA_COMPLEMENTARIA '{$id_ficha}'";
        $command = Yii::app()->db->createCommand($sql);
        return $command->queryRow();
    }

    /**
     * Funcion que obtiene la Data de una ficha-otros específica
     * 
     * @param String $id_ficha Ficha a ser utilizada en la consulta.
     * @return Array
     */
    public static function get_ConsultaFichaOtros($id_ficha) {
        $sql     = "EXECUTE SP_REPORTE_FICHA_OTROS '{$id_ficha}'";
        $command = Yii::app()->db->createCommand($sql);
        return $command->queryRow();
    }

    /**
     * Funcion que obtiene la Data de una ficha-digitador específica
     * 
     * @param String $id_ficha Ficha a ser utilizada en la consulta.
     * @return Array
     */
    public static function get_ConsultaFichaDigitador($id_ficha) {
        $sql     = "EXECUTE SP_REPORTE_FICHA_DIGITADOR '{$id_ficha}'";
        $command = Yii::app()->db->createCommand($sql);
        return $command->queryRow();
    }

    /**
     * Funcion que obtiene la Data de una ficha-economica específica
     * 
     * @param String $id_ficha Ficha a ser utilizada en la consulta.
     * @return Array
     */
    public static function get_ConsultaFichaEconomicaID($id_ficha) {
        $sql     = "EXECUTE SP_REPORTE_FICHA_ECONOMICA '{$id_ficha}'";
        $command = Yii::app()->db->createCommand($sql);
        return $command->queryRow();
    }

    /**
     * Funcion que obtiene la Data de una ficha-economica conductor específica
     * 
     * @param String $id_ficha Ficha a ser utilizada en la consulta.
     * @return Array
     */
    public static function get_ConsultaFichaEconomicaConductor($id_ficha) {
        $sql     = "EXECUTE SP_REPORTE_FICHA_ECONOMICA_CONDUCTOR '{$id_ficha}'";
        $command = Yii::app()->db->createCommand($sql);
        return $command->queryRow();
    }

    /**
     * Funcion que obtiene la Data de una ficha-domicilio fiscal específica
     * 
     * @param String $id_ficha Ficha a ser utilizada en la consulta.
     * @return Array
     */
    public static function get_DomicilioFiscal($id_ficha) {
        $sql     = "select id_ficha,id_ubi_geo 
		from domicilio_fiscal  
		where id_ficha= '{$id_ficha}'";
        $command = Yii::app()->db->createCommand($sql);
        return $command->queryRow();
    }

    /**
     * Funcion que obtiene la Data de una ficha-economica domicilio callao específica
     * 
     * @param String $id_ficha Ficha a ser utilizada en la consulta.
     * @return Array
     */
    public static function get_ConsultaFichaEconomicaCallao($id_ficha) {
        $sql     = "EXECUTE SP_REPORTE_FICHA_ECONOMICA_DOMICILIO_CALLAO '{$id_ficha}'";
        $command = Yii::app()->db->createCommand($sql);
        return $command->queryRow();
    }

    /**
     * Funcion que obtiene la Data de una ficha-economica domicilio no callao específica
     * 
     * @param String $id_ficha Ficha a ser utilizada en la consulta.
     * @return Array
     */
    public static function get_ConsultaFichaEconomicaNoCallao($id_ficha) {
        $sql     = "EXECUTE SP_REPORTE_FICHA_ECONOMICA_DOMICILIO_NO_CALLAO '{$id_ficha}'";
        $command = Yii::app()->db->createCommand($sql);
        return $command->queryRow();
    }

    /**
     * Funcion que obtiene la Data de una ficha-economica descripcion de actividades específica
     * 
     * @param String $id_ficha Ficha a ser utilizada en la consulta.
     * @return Array
     */
    public static function get_ConsultaFichaEconomicaDescripcionActividad($id_ficha) {
        $sql     = "select lf.id_ficha, lf.cod_actividad, a.desc_actividad from licencias_funcionamiento lf, actividades a 
	  where lf.cod_actividad=a.cod_actividad  and lf.id_ficha= '{$id_ficha}'";
        $command = Yii::app()->db->createCommand($sql);
        return $command->queryAll();
    }

    /**
     * Funcion que obtiene la Data de una ficha-economica descripcion de anuncios específica
     * 
     * @param String $id_ficha Ficha a ser utilizada en la consulta.
     * @return Array
     */
    public static function get_ConsultaFichaEconomicaDescripcionAnuncio($id_ficha) {
        $sql     = "select  ac.id_ficha, ac.nro, ac.cod_anuncio, ac.nro_lados, ac.area_aut_anuncio, ac.area_ver_anuncio, 
ac.nro_expediente, ac.nro_licencia, ac.fecha_expedicion, ac.fecha_vencimiento, an.desc_anuncio  
from anuncios_act ac, anuncios an 
where ac.cod_anuncio=an.cod_anuncio 
and ac.id_ficha= '{$id_ficha}'";
        $command = Yii::app()->db->createCommand($sql);
        return $command->queryAll();
    }

    /**
     * Funcion que obtiene la Data de una ficha-economica Complementaria específica
     * 
     * @param String $id_ficha Ficha a ser utilizada en la consulta.
     * @return Array
     */
    public static function get_ConsultaFichaEconomicaComplementarias($id_ficha) {
        $sql     = "select  fe.id_ficha, fe.dctos_presentados, f.condic_declara, f.estado_llenado, f.mantenimiento, fe.observaciones, 
(select td.desc_tabla_detalle from tablas_detalle  td where td.id_tabla='32' and td.codigo=fe.dctos_presentados) as des_doc_pres,   
(select td.desc_tabla_detalle from tablas_detalle  td where td.id_tabla='25' and td.codigo=f.condic_declara) as des_cond_declara,           
(select td.desc_tabla_detalle from tablas_detalle  td where td.id_tabla='26' and td.codigo=f.estado_llenado) as des_est_llenado, 
(select td.desc_tabla_detalle from tablas_detalle  td where td.id_tabla='28' and td.codigo=f.mantenimiento) as des_mantenimiento      
from fichas_economicas fe, fichas f 
where fe.id_ficha=f.id_ficha 
and fe.id_ficha= '{$id_ficha}'";
        $command = Yii::app()->db->createCommand($sql);
        return $command->queryRow();
    }

    /**
     * Funcion que obtiene la Data de una ficha-condominio específica
     * 
     * @param String $id_ficha Ficha a ser utilizada en la consulta.
     * @return Array
     */
    public static function get_ConsultaFichaCondominioID($id_ficha) {
        $sql     = "EXECUTE SP_REPORTE_FICHA_CONDOMINIO '{$id_ficha}'";
        $command = Yii::app()->db->createCommand($sql);
        return $command->queryRow();
    }

    /**
     * Funcion que obtiene la Data de una ficha-condominio cotitular específica
     * 
     * @param String $id_ficha Ficha a ser utilizada en la consulta.
     * @return Array
     */
    public static function get_ConsultaFichaCondominioCotitular($id_ficha) {
        $sql     = "select fc.id_ficha, 
t.id_persona, p.tip_doc, p.nro_doc, p.nombres, p.ape_patErno, p.ape_matErno, 
t.telefono,t.correo_elect, t.porcentaje_pertenencia, id_ubi_geo, 
(select u.nom_ubi_geo from ubigeo u      
  where substring(df.id_ubi_geo,1,2)=u.id_ubi_geo) as departamento,  
(select u.nom_ubi_geo from ubigeo u      
  where substring(df.id_ubi_geo,1,4)=u.id_ubi_geo) as provincia,  
(select u.nom_ubi_geo from ubigeo u      
  where substring(df.id_ubi_geo,1,6)=u.id_ubi_geo) as distrito    
from fichas_condominio fc, titular t, personas p, domicilio_fiscal df  where 
  fc.id_ficha=t.id_ficha  
  and p.id_persona=t.id_persona 
  and (t.id_ficha=df.id_ficha  and t.id_persona=df.id_persona) 
  and fc.id_ficha = '{$id_ficha}'";
        $command = Yii::app()->db->createCommand($sql);
        return $command->queryAll();
    }

    /**
     * Funcion que obtiene la Data de una ficha-condominio complementaria específica
     * 
     * @param String $id_ficha Ficha a ser utilizada en la consulta.
     * @return Array
     */
    public static function get_ConsultaFichaCondominioComplementarias($id_ficha) {
        $sql     = "select  fc.id_ficha,  f.condic_declara, f.estado_llenado, fc.observaciones, 
(select td.desc_tabla_detalle from tablas_detalle  td where td.id_tabla='25' and td.codigo=f.condic_declara) as des_cond_declara,           
(select td.desc_tabla_detalle from tablas_detalle  td where td.id_tabla='26' and td.codigo=f.estado_llenado) as des_est_llenado  
from fichas_condominio fc, fichas f 
where fc.id_ficha=f.id_ficha 
and fc.id_ficha= '{$id_ficha}'";
        $command = Yii::app()->db->createCommand($sql);
        return $command->queryRow();
    }

    /**
     * Funcion que obtiene la Data de una ficha-bien comun específica
     * 
     * @param String $id_ficha Ficha a ser utilizada en la consulta.
     * @return Array
     */
    public static function get_ConsultaFichaBienComun($id_ficha) {
        $sql     = "EXECUTE SP_REPORTE_FICHA_BIENCOMUN '{$id_ficha}'";
        $command = Yii::app()->db->createCommand($sql);
        return $command->queryRow();
    }

    /**
     * Funcion que obtiene la Data de una ficha-bien comun descripcion específica
     * 
     * @param String $id_ficha Ficha a ser utilizada en la consulta.
     * @return Array
     */
    public static function get_ConsultaFichaBienDescripcion($id_ficha) {
        $sql     = "select 
  fb.id_ficha, fb.clasificacion, fb.predio_catastrado_en, fb.cod_uso, u.desc_uso,
  fb.area_titulo, fb.area_verificada, 
  li.fre_med_campo, li.der_med_campo, li.izq_med_campo,li.fon_med_campo,  
  li.fre_med_titulo, li.der_med_titulo, li.izq_med_titulo,li.fon_med_titulo,  
  li.fre_col_campo, li.der_col_campo, li.izq_col_campo, li.fon_col_campo,  
  li.fre_col_titulo, li.der_col_titulo, li.izq_col_titulo,li.fon_col_titulo, f.id_lote,l.estructuracion, l.zonificacion,   
 (select desc_tabla_detalle from tablas_detalle where id_tabla='15' and codigo=fb.clasificacion) as des_clasific, 
(select desc_tabla_detalle from tablas_detalle where id_tabla='16' and codigo=fb.predio_catastrado_en) as des_pred_cat_en, 
 (select z.descrip_zona  from zona z where ltrim(z.zonificacion)=l.zonificacion) as des_zona, 
li.fre_lim_fre_lot,li.der_lim_fre_lot,li.izq_lim_fre_lot,li.fon_lim_fre_lot,tot_lim_fre_uc         
   from fichas_bienes_comunes fb,uso u, linderos li, fichas f, lotes l  where 
  fb.cod_uso=u.cod_uso  
   and fb.id_ficha=li.id_ficha 
   and f.id_ficha=fb.id_ficha 
   and f.id_lote=l.id_lote 
  and fb.id_ficha =  '{$id_ficha}'";
        $command = Yii::app()->db->createCommand($sql);
        return $command->queryRow();
    }

    /**
     * Funcion que obtiene la Data de una ficha-bien comun recapitulacion de edificios específica
     * 
     * @param String $id_ficha Ficha a ser utilizada en la consulta.
     * @return Array
     */
    public static function get_ConsultaFichaBienRecapitulacionEdificio($id_ficha) {
        $sql     = "select 
	fb.id_ficha, re.edificio, re.porcentaje, re.atc, re.aoic, re.acc 
  	from fichas_bienes_comunes fb, recap_edificio re 
	where 
	fb.id_ficha=re.id_ficha  
	and fb.id_ficha ='{$id_ficha}'";
        $command = Yii::app()->db->createCommand($sql);
        return $command->queryAll();
    }

    /**
     * Funcion que obtiene la Data de una ficha-bien comun recapitulacion de bienes específica
     * 
     * @param String $id_ficha Ficha a ser utilizada en la consulta.
     * @return Array
     */
    public static function get_ConsultaFichaBienRecapitulacionBien($id_ficha) {
        $sql     = "select 
	rb.nro, rb.edifica, rb.entrada, rb.piso, rb.unidad, rb.porcentaje, rb.atc,  rb.aoic, rb.acc, substring(rb.id_ficha_ind,13,7) as ind 
  	from recap_bbcc rb 
	where 
	rb.id_ficha ='{$id_ficha}'";
        $command = Yii::app()->db->createCommand($sql);
        return $command->queryAll();
    }

    /**
     * Funcion que obtiene la Data de una ficha-bien comun area invadida específica
     * 
     * @param String $id_ficha Ficha a ser utilizada en la consulta.
     * @return Array
     */
    public static function get_ConsultaFichaBienAreaInvadida($id_ficha) {
        $sql     = "select en_lote_colindante, en_jardin_aislam, en_area_publica, en_area_intang  
	from area_terr_invadida
	where 
	id_ficha ='{$id_ficha}'";
        $command = Yii::app()->db->createCommand($sql);
        return $command->queryRow();
    }

    /**
     * Funcion que obtiene la Data de una ficha-bien comun notarial específica
     * 
     * @param String $id_ficha Ficha a ser utilizada en la consulta.
     * @return Array
     */
    public static function get_ConsultaFichaBienNotarial($id_ficha) {
        $sql     = "select 
	fb.id_ficha, rl.id_notaria, rl.kardex, rl.fecha_escritura,
	(select n.nom_notaria from notarias n where  n.id_notaria=rl.id_notaria) as des_notaria   	  
  	from fichas_bienes_comunes fb, registro_legal rl 
	where 
	fb.id_ficha=rl.id_ficha  
	and fb.id_ficha ='{$id_ficha}'";
        $command = Yii::app()->db->createCommand($sql);
        return $command->queryRow();
    }

    /**
     * Funcion que obtiene la Data de una ficha-bien comun sunarp específica
     * 
     * @param String $id_ficha Ficha a ser utilizada en la consulta.
     * @return Array
     */
    public static function get_ConsultaFichaBienSunarp($id_ficha) {
        $sql     = "select 
	fb.id_ficha,s.tipo_part_reg, s.numero, s.fojas, s.asiento, s.fecha_inscripcion, s.declar_fab, s.asien_inscrip_fab, s.fecha_inscrip_fab, 
	(select td.desc_tabla_detalle from tablas_detalle td where  td.id_tabla='22' and td.codigo=s.tipo_part_reg) as des_tipo_part, 
	(select td.desc_tabla_detalle from tablas_detalle td where  td.id_tabla='23' and td.codigo=s.declar_fab) as des_declar_fab   	    	 
  	from fichas_bienes_comunes fb, sunarp s
	where 
	fb.id_ficha=s.id_ficha  
	and fb.id_ficha = '{$id_ficha}'";
        $command = Yii::app()->db->createCommand($sql);
        return $command->queryRow();
    }

    /**
     * Funcion que obtiene la Data de una ficha-bien comun complementaria específica
     * 
     * @param String $id_ficha Ficha a ser utilizada en la consulta.
     * @return Array
     */
    public static function get_ConsultaFichaBienComplementaria($id_ficha) {
        $sql     = "select  fb.id_ficha,  f.condic_declara, f.estado_llenado, fb.observaciones, 
(select td.desc_tabla_detalle from tablas_detalle  td where td.id_tabla='25' and td.codigo=f.condic_declara) as des_cond_declara,           
(select td.desc_tabla_detalle from tablas_detalle  td where td.id_tabla='26' and td.codigo=f.estado_llenado) as des_est_llenado  
from fichas_bienes_comunes fb, fichas f 
where fb.id_ficha=f.id_ficha 
and fb.id_ficha= '{$id_ficha}'";
        $command = Yii::app()->db->createCommand($sql);
        return $command->queryRow();
    }

    /**
     * Funcion que obtiene la Data de una ficha titular persona específica
     * 
     * @param String $id_ficha Ficha a ser utilizada en la consulta.
     * @return Array
     */
    public static function get_ConsultaFichaTitularPersona($id_ficha, $id_persona) {
        $sql     = "select t.id_ficha, t.id_persona, p.tip_persona, t.cod_contribuy, 
t.forma_adq, t.fecha_adq,t.porcentaje_pertenencia,p.estado_civil,p.tip_doc,p.nro_doc,
p.nombres,p.ape_paterno, p.ape_materno,  p.tip_persona_juridica,  
(select tdp.desc_tabla_detalle from tablas_detalle tdp    
  where tdp.id_tabla='07' and tdp.codigo=p.tip_persona) as des_tip_persona, 
(select tdp.desc_tabla_detalle from tablas_detalle tdp    
  where tdp.id_tabla='13' and tdp.codigo=t.forma_adq) as des_tip_forma_adq, 
(select tdp.desc_tabla_detalle from tablas_detalle tdp    
  where tdp.id_tabla='08' and tdp.codigo=p.estado_civil) as des_estado_civil, 
(select tdp.desc_tabla_detalle from tablas_detalle tdp    
  where tdp.id_tabla='09' and tdp.codigo=p.tip_doc) as des_tip_doc, 
(select tdp.desc_tabla_detalle from tablas_detalle tdp    
  where tdp.id_tabla='10' and tdp.codigo=p.tip_persona_juridica) as des_persona_juridica
from titular t, personas p 
where t.id_persona=p.id_persona 
and t.id_ficha='{$id_ficha}' 
and t.id_persona='{$id_persona}'";
        $command = Yii::app()->db->createCommand($sql);
        return $command->queryRow();
    }

    /**
     * Funcion que obtiene la Data de una ficha- titular persona exoneracion específica
     * 
     * @param String $id_ficha Ficha a ser utilizada en la consulta.
     * @return Array
     */
    public static function get_ConsultaFichaTitularPersonaExoneraciones($id_ficha, $id_persona) {
        $sql     = "select id_ficha,id_persona,condicion,nro_resolucion,nro_boleta_pension, fecha_inicio, fecha_vencimiento,
(select tdp.desc_tabla_detalle from tablas_detalle tdp    
  where tdp.id_tabla='11' and tdp.codigo=condicion) as des_condicion  
from exoneraciones_titular 
where id_ficha='{$id_ficha}'  
and id_persona='{$id_persona}'";
        $command = Yii::app()->db->createCommand($sql);
        return $command->queryRow();
    }

}
