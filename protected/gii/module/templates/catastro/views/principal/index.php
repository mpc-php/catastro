<!-- BEGIN CONTENT BODY -->
<!-- BEGIN PAGE HEAD-->
<div class="page-head">
    <div class="container">
        <!-- BEGIN PAGE TITLE -->
        <div class="page-title">
            <h1><?php echo "<?php"; ?> echo $this->module->id; ?> </h1>
        </div>
        <!-- END PAGE TITLE -->
    </div>
</div>
<!-- END PAGE HEAD-->
<!-- BEGIN PAGE CONTENT BODY -->
<div class="page-content">
    <div class="container-fluid">
        <!-- BEGIN PAGE BREADCRUMBS -->
        <ul class="page-breadcrumb breadcrumb">
            <li>
                <a href="index.html"><?php echo "<?php"; ?> echo ucwords($this->module->id); ?></a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <span><?php echo "<?php"; ?> echo get_class($this); ?></span>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <span><?php echo "<?php"; ?> echo $this->action->id; ?></span>
                <i class="fa fa-circle"></i>
            </li>
        </ul>
        <!-- END PAGE BREADCRUMBS -->
        <!-- BEGIN PAGE CONTENT INNER -->
        <div class="page-content-inner">
            <div class="note note-info">
                <p> Este es el contenido de la vista para la acción <strong>"<?php echo "<?php"; ?> echo $this->action->id; ?>"</strong>. La acción pertenece al controlador <strong>"<?php echo "<?php"; ?> echo get_class($this); ?>"</strong> en el módulo <strong>"<?php echo "<?php"; ?> echo ucwords($this->module->id); ?>"</strong> </p>
            </div>
        </div>
        <!-- END PAGE CONTENT INNER -->
    </div>
</div>
<!-- END PAGE CONTENT BODY -->
<!-- END CONTENT BODY -->