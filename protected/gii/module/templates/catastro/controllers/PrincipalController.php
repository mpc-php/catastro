

<?php echo "<?php\n"; ?>
/**
* Descripcción para Principal
*
* @author Nombre del Programador <correodelprogramador@email.com>
* @package Catastro\Modules\<?php echo ucwords($this->moduleID); ?>\Controllers
*/

class PrincipalController extends Auth
{
	public function actionIndex()
	{
		$this->render('index');
	}
}