<?php

/**
 * Utils es la clase creada para colocar funciones reutilizables
 * 
 * Vease a esta clase como un Helper o Utilitarios que permite concentrar
 * todas las funciones que son de uso cotidiano y utilizado por todos.
 *
 * @author Nolberto Vilchez Moreno <jnolbertovm@gmail.com>
 * @package Catastro\Components
 */
class Utils {

    public static $busqueda = [
        'select'    => '*',
        'condition' => '',
        'params'    => []
    ];

    /**
     * Funcion que encodea la Url para TripleDes
     * 
     * @param String $string
     * @return String
     */
    public static function encodeUrlTripleDes($string) {
        return str_replace(" ", "+", $string);
    }

    /**
     * Función que muestra la data a ser utilizada
     * 
     * @param Array $data Data a ser mostrada
     * @param boolean $detenerProcesos Indicador para saber si el proceso va a matar el PHP
     * @param String $titulo Titulo para mostrar antes de la data.
     */
    public static function show($data, $detenerProcesos = false, $titulo = 'Datos') {
        echo "<code class='code'><b>{$titulo} :</b></code>";
        echo "<pre>";
        print_r($data);
        echo '</pre>';
        if ($detenerProcesos) {
            die();
        }
    }

    /**
     * Función que obtiene la IP de la máquina
     * 
     * @return String
     */
    public static function obtenerIP() {
        if (isset($_SERVER["HTTP_CLIENT_IP"])) {
            return $_SERVER["HTTP_CLIENT_IP"];
        } elseif (isset($_SERVER["HTTP_X_FORWARDED_FOR"])) {
            return $_SERVER["HTTP_X_FORWARDED_FOR"];
        } elseif (isset($_SERVER["HTTP_X_FORWARDED"])) {
            return $_SERVER["HTTP_X_FORWARDED"];
        } elseif (isset($_SERVER["HTTP_FORWARDED_FOR"])) {
            return $_SERVER["HTTP_FORWARDED_FOR"];
        } elseif (isset($_SERVER["HTTP_FORWARDED"])) {
            return $_SERVER["HTTP_FORWARDED"];
        } elseif (isset($_SERVER["REMOTE_ADDR"])) {
            return $_SERVER["REMOTE_ADDR"];
        } else {
            return "000.000.000.000";
        }
    }

    /**
     * Función que obtiene la ubicación de una IP
     * 
     * @param String $ip IP a ser ubicada en la función.
     * @return boolean
     */
    public static function geoIP($ip) {
        $file_get_contents = file_get_contents('http://www.geoplugin.net/php.gp?ip=' . $ip);
        $unserialize       = unserialize($file_get_contents);

//        return $unserialize;
        if ((int) $unserialize["geoplugin_status"] === 200) {
            return [
                "ciudad"   => $unserialize["geoplugin_countryName"],
                "region"   => $unserialize["geoplugin_regionName"],
                "latitud"  => $unserialize["geoplugin_latitude"],
                "longitud" => $unserialize["geoplugin_longitude"]
            ];
        }
        return false;
    }

    /**
     * Genera etiquetas HTML con la clase CHtml
     * @param string $nombre name e id del campo
     * @param string $valor en el caso del label, es el for
     * @param string $tipocampo desde input a labels
     * @param array $opciones opciones para el select, radio, checkbox
     * @param array $class_opciones opciones html como class, style, etc.
     * @return object
     */
    public static function generarCHTML($nombre, $valor, $tipocampo, $opciones, $class_opciones) {
        $campo = "";
        switch ($tipocampo) {
            case "TEXT":
                $campo = CHtml::textField(self::reset_string($nombre), $valor, $class_opciones);
                break;
            case "SELECT":
                if (!isset($class_opciones['empty'])) {
                    $class_opciones['empty'] = "Seleccione ... ";
                }
                $campo = CHtml::dropDownList(self::reset_string($nombre), $valor, $opciones, $class_opciones);
                break;
            case "RADIO":
                if ($valor == '' || empty($valor) || $valor == null) {
                    $valor = false;
                }
                $campo = CHtml::radioButton(self::reset_string($nombre), $valor, $class_opciones);
                break;
            case "RADIOLIST":
                if ($valor == '' || empty($valor) || $valor == null) {
                    $valor = false;
                }
                $campo = CHtml::radioButtonList(self::reset_string($nombre), $valor, $opciones, $class_opciones);
                break;
            case "CHECK":
                if ($valor == '' || empty($valor) || $valor == null) {
                    $valor = false;
                }
                $campo = CHtml::checkBox(self::reset_string($nombre), $valor, $class_opciones);
                break;
            case "CHECKLIST":
                if ($valor == '' || empty($valor) || $valor == null) {
                    $valor = false;
                }
                $campo = CHtml::checkBoxList(self::reset_string($nombre), $valor, $opciones, $class_opciones);
                break;
            case "TEXTAREA":
                $campo = CHtml::textArea(self::reset_string($nombre), $valor, $class_opciones);
                break;
            case "HIDDEN":
                $campo = CHtml::hiddenField(self::reset_string($nombre), $valor, $class_opciones);
                break;
            case "PASSWORD":
                $campo = CHtml::passwordField(self::reset_string($nombre), $valor, $class_opciones);
                break;
            case "DATE":
                if (isset($class_opciones['class'])) {
                    $class      = $class_opciones['class'];
                    $class_date = "{$class} datepicker";
                } else {
                    $class_date = "datepicker";
                }
                $class_opciones['class']    = $class_date;
                $class_opciones['readonly'] = true;
                $campo                      = CHtml::textField(self::reset_string($nombre), $valor, $class_opciones);
                break;
            case "SPINNER":
                if (isset($class_opciones['class'])) {
                    $class         = $class_opciones['class'];
                    $class_spinner = "{$class} input-mini spinner-input form-control esNumero";
                } else {
                    $class_spinner = "spinner-input esNumero";
                }
                $class_opciones['class'] = $class_spinner;
                $campo                   = CHtml::textField(self::reset_string($nombre), $valor, $class_opciones);
                break;
            case "FILE":
                $campo                   = CHtml::fileField(self::reset_string($nombre), $valor, $class_opciones);
                break;
            case "LABEL":
                $valor                   = ($valor != "") ? $valor : false;
                $campo                   = CHtml::label($nombre, $valor, $class_opciones);
                break;
            default:
                break;
        }
        return $campo;
    }

    /**
     * Reinicia la cadena de caracteres raros.
     * @param string $string
     * @return string
     */
    public static function reset_string($string, $restriccionlogin = false) {

        $string = trim($string);

        $string = str_replace(
                array('á', 'à', 'ä', 'â', 'ª', 'Á', 'À', 'Â', 'Ä'), array('a', 'a', 'a', 'a', 'a', 'A', 'A', 'A', 'A'), $string
        );

        $string = str_replace(
                array('é', 'è', 'ë', 'ê', 'É', 'È', 'Ê', 'Ë'), array('e', 'e', 'e', 'e', 'E', 'E', 'E', 'E'), $string
        );

        $string = str_replace(
                array('í', 'ì', 'ï', 'î', 'Í', 'Ì', 'Ï', 'Î'), array('i', 'i', 'i', 'i', 'I', 'I', 'I', 'I'), $string
        );

        $string = str_replace(
                array('ó', 'ò', 'ö', 'ô', 'Ó', 'Ò', 'Ö', 'Ô'), array('o', 'o', 'o', 'o', 'O', 'O', 'O', 'O'), $string
        );

        $string = str_replace(
                array('ú', 'ù', 'ü', 'û', 'Ú', 'Ù', 'Û', 'Ü'), array('u', 'u', 'u', 'u', 'U', 'U', 'U', 'U'), $string
        );

        $string = str_replace(
                array('ç', 'Ç'), array('c', 'C',), $string
        );

        if (!$restriccionlogin) {
            $string = str_replace(
                    array('ñ', 'Ñ'), array('n', 'N'), $string
            );
        }


//Esta parte se encarga de eliminar cualquier caracter extraño
        $string = str_replace(
                array("\\", "¨", "º", "-", "~",
            "#", "@", "|", "!", "\"",
            "·", "$", "%", "&", "/",
            "(", ")", "?", "'", "¡",
            "¿", "^", "`",
            "+", "}", "{", "¨", "´",
            ">", "< ", ";", ",", ":",
            ".", " "), '', $string
        );


        return $string;
    }

    public static function getConstante($constante, $tipo = Constante::NUMERO) {
        if ($tipo == Constante::NUMERO) {
            return (int) $constante;
        } else {
            return (string) $constante;
        }
    }

    /**
     * Funcion que encripta un valor para ser usado como token.
     * @param string $valor valor a ser encriptado
     * @return string valor encriptado
     */
    public static function token($valor) {
        return sha1(TripleDes::Encrypt($valor));
    }

    /**
     * Función que valida si el Token y el Valor son iguales
     * 
     * @param String $token Cadena encriptada a ser comparada
     * @param String $valor Valor que será encriptada para la comparación.
     * @return boolean
     */
    public static function validarToken($token, $valor) {
        if ($token === self::token($valor)) {
            return true;
        }
        return false;
    }

    /**
     * Funcion que setea los campos para la búsqueda del Criteria de Yii
     * 
     * @param Array $buscar Arreglo con los keys a ser utilizados en la busqueda
     * @param String $select Campos a ser buscados en el SELECT
     * @param String $order Order que será utilizado en la consulta
     */
    public static function setBusqueda($buscar, $select = false, $order = false) {
        if (is_array($buscar)) {
            $condition = '';
            $and       = 'and ';
            $params    = [];
            foreach ($buscar as $key => $val) {
                $condition .= "{$key} = :{$key} {$and}";
                $params[":{$key}"] = $val;
            }

            $condition = substr($condition, 0, -(strlen($and)));

            if ($order) {
                $condition .= "ORDER BY {$order}";
            }

            if ($select) {
                self::$busqueda['select'] = $select;
            }

            self::$busqueda['condition'] = $condition;
            self::$busqueda['params']    = $params;
        }
    }

    /**
     * Returna los campos seteados en la funcion setBusqueda
     * 
     * @return Array
     */
    public static function getBusqueda() {
        return self::$busqueda;
    }

    /**
     * Funcion que valida si el dato existe, para printearlo.
     * @param string/int/array $valor El dato a ser validado, o un arreglo de datos
     * @param string/int $key Si se da este parámetro, quiere decir que $valor es un arreglo/objeto, y este sería el key a ser validado.
     * @return string Retorna el valor o el contenido del arreglo/objeto solicitado, y de no existir, retorna vacío.
     */
    public static function validaDato($valor, $key = NULL, $arreglo = true) {
        if ($key != NULL) {
            if ($arreglo) {
                return (isset($valor[$key])) ? $valor[$key] : "";
            } else {
                return (isset($valor->$key)) ? $valor->$key : "";
            }
        } else {
            return (isset($valor)) ? $valor : "";
        }
    }

    /**
     * Funcion que obtiene los campos en la URL
     * 
     * @param String $nombreGet El nombre del campo a ser obtenido de la URL
     * @return String
     */
    public static function _get($nombreGet) {
        if (!isset($_GET[$nombreGet])) {
            return null;
        }
        return $_GET[$nombreGet];
    }

    /**
     * Devuelve un arreglo con las opciones SI y NO
     * @return array Arreglo de datos de las opciones SI y NO
     */
    public static function obtenerSelectSiNo() {
        return [
            Constante::SI => "SI",
            Constante::NO => "NO"
        ];
    }
    /**
     * Devuelve un arreglo con las opciones SI y NO
     * @return array Arreglo de datos de las opciones SI y NO
     */
    public static function obtenerSelectEstados() {
        return [
            Constante::ACTIVO => "ACTIVO",
            Constante::INACTIVO => "INACTIVO"
        ];
    }

    /**
     * Cambia formato de moneda a decimal
     * @param float $monto
     * @return float
     */
    public static function moneyToDecimal($monto) {
        $monto = (trim($monto) == "") ? 0.00 : $monto;
        return Utils::aDecimal((float) str_replace(",", "", $monto));
    }

    /**
     * Cambia formato de decimal a moneda
     * @param float $monto
     * @return float
     */
    public static function decimalToMoney($monto) {
        if (trim($monto) == "") {
            return number_format(0, 2, ".", ",");
        } else {
            return number_format($monto, 2, ".", ",");
        }
    }

    /**
     * Convierte el valor dado a decimal
     * @param int/float $valor
     * @param string $simbolo
     * @return float
     */
    public static function aDecimal($valor = 0, $simbolo = ".") {
        if (trim($valor) == "") {
            return (float) self::moneyToDecimal(0);
        } else {
            return (float) number_format($valor, 2, $simbolo, '');
        }
    }

}
