<?php

/**
 * Description of Folder
 *
 * @author usuario
 */
class Folder {

    /**
     * Funcion:     existeCarpeta
     * Param:       urlServer
     * Descripcion: Se entrega la url absoluta de la carpeta que se quiere verificar su existencia, si no existe se procede a crear la carpeta.
     */
    public static function crear($urlDIr, $recursivo = true) {
        if (!is_dir($urlDIr)) {
            return mkdir($urlDIr, 0777, $recursivo);
        }
        return true;
    }

}
