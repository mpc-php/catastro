<?php

class Files {

    public static function getExtension($file) {
        $array = explode(".", $file);
        $total = count($array);
        if ($total > 0) {
            return $array[$total - 1];
        } else {
            return "";
        }
    }

    public static function getNombre($file) {
        $array  = explode(".", $file);
        $total  = count($array);
        $nombre = substr($file, 0, -(strlen($array[$total - 1]) + 1));
        return $nombre;
    }

    /**
     * Se recibe el nombre del file para poder extraer su nombre y su extension mediante un arreglo.['nombre] y ['extension']
     * @param string $nombrefile
     * @return array
     */
    public static function getNombreExtensionFile($nombrefile) {
        $array             = explode(".", $nombrefile);
        $total             = count($array);
        $data['nombre']    = substr($nombrefile, 0, -(strlen($array[$total - 1]) + 1));
        $data['extension'] = $array[$total - 1];
        if ($total > 0) {
            return $data;
        } else {
            return array();
        }
    }

    /*
     * Funcion:     deleteDir
     * Param:       dirPath
     * Descripcion: Se recibe la url de la carpeta a eliminar con todos sus hijos correspondientes
     */

    public static function deleteDir($dirPath) {
        if (!is_dir($dirPath)) {
            throw new InvalidArgumentException("$dirPath must be a directory");
        }
        if (substr($dirPath, strlen($dirPath) - 1, 1) != '/') {
            $dirPath .= '/';
        }
        $files = glob($dirPath . '*', GLOB_MARK);
        foreach ($files as $file) {
            if (is_dir($file)) {
                self::deleteDir($file);
            } else {
                unlink($file);
            }
        }
        return rmdir($dirPath);
    }

    public static function obtenerMimeTypeporExtension($extension) {
        $mimetype = "";
        switch ($extension) {
            case 'pdf':
                $mimetype = "application/pdf";
                break;
            case 'doc':
                $mimetype = "application/msword";
                break;
            case 'docx':
                $mimetype = "application/msword";
                break;
            case 'zip':
                $mimetype = "application/zip";
                break;
        }
        return $mimetype;
    }

}
