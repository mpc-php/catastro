<?php

class Criteria {

    public static $busqueda = [
        'select'    => '*',
        'condition' => '',
        'params'    => []
    ];

    public static function set($buscar, $select = false, $order = false) {
        if (is_array($buscar)) {
            $condition = '';
            $and       = 'and ';
            $params    = [];
            foreach ($buscar as $key => $val) {
                $condition .= "{$key} = :{$key} {$and}";
                $params[":{$key}"] = $val;
            }

            $condition = substr($condition, 0, -(strlen($and)));

            if ($order) {
                $condition .= "ORDER BY {$order}";
            }

            if ($select) {
                self::$busqueda['select'] = $select;
            }

            self::$busqueda['condition'] = $condition;
            self::$busqueda['params']    = $params;
        }
    }

    public static function get() {
        return self::$busqueda;
    }

}
