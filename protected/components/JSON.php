<?php

class JSON {

    /**
     * 
     * @param Boolean $error Error de la consulta
     * @param Int $code Código de respuesta http
     * @param String $message Mensaje que detalla la consulta
     * @param Array $data Datos obtenidos de la consulta
     * @return String 
     */
    public static function response($error = FALSE, $code = 200, $message = '', $data = [], $die = true) {
        $response          = new stdClass;
        $response->message = $message;
        $response->error   = $error;
        $response->code    = (!is_numeric($code)) ? 500 : $code;

        foreach ($data as $key => $value) {
            $response->{$key} = $value;
        }

        header('Content-type: application/json; charset=UTF-8');
        http_response_code($response->code);

        echo CJSON::encode($response);

        if ($die)
            Yii::app()->end();
    }

    public static function utf8($string) {

        $retornar = [];

        if (is_object($string)) 
            $string = (array)$string;

        if (is_array($string)) {

            foreach ($string as $key => $val) {

                if (is_object($val)) 
                    $val = (array)$val;
                
                if (is_array($val)) {
                    $retornar[$key] = self::utf8($val);
                } else {
                    $retornar[$key] = utf8_encode($val);
                }
            }
        } else {
            $retornar =  utf8_encode($string);
        }

        return $retornar;
    }

}
