<?php

/*
 * @package Catastro\Components
 */

class Params {

    /**
     * URL de la ubicacion de la carpeta usuarios
     * @var string
     */
    public static $url_user = 'FILES/usuario/';

    /**
     * ID del usuario.
     * @var integer
     */
    public static $id_user = 0;

    /**
     * retorna el id_user
     * @return integer
     */
    public static function getId_user() {
        return self::$id_user;
    }

    /**
     * setea el id_user de la clase, para su manejo.
     * @param integer $id_user
     */
    public static function setId_user($id_user) {
        self::$id_user = $id_user;
    }

    /**
     * Obtiene la url del archivo /config/params.php dependiendo del modulo que se halla enviado. 
     * Si se envia usuario como true, se obtiene el param usuario, y se obtiene el modulo dentro del arreglo usuarios
     * retorna la url (fisica o web) de la carpeta del modulo que se esta ejecutando.
     * @param string $url
     * @param string $module
     * @param boolean $user
     * @return string
     */
    private static function setParams($url, $module, $user) {
        $params = ($user) ? Yii::app()->params['usuario'] : Yii::app()->params[$module];

        if ($module != '' && $user) {
            $url_user = self::getUserUrl();
            if (isset($params[$module])) {
                $params = $url_user . $params[$module];
            }
        }

        $link = $url . $params;

        return $link;
    }

    /*
     * Function web
     * Params:  $module = recibe el nombre del modulo que se esta ejecutando
     *          $user = true o false, dependiendo si es una carpeta dentro de FILES/usuarios
     * Descripcion: Utilizando la funcion setParams, genera la url web del modulo que se esta ejecutando.
     * Retorna la url web del modulo en ejecucion.
     */

    public static function web($module = false, $user = false) {
        $url = yii::app()->request->hostInfo . yii::app()->homeUrl;
        return self::setParams($url, $module, $user);
    }

    /**
     * Descripcion: Utilizando la funcion recursiva setParams, genera la url fisica del modulo que se esta ejecutando.
     * Retorna la url fisica del modulo en ejecucion.
     * @param string $module recibe el nombre del modulo que se esta ejecutando
     * @param boolean $user true o false, dependiendo si es una carpeta dentro de FILES/usuarios
     * @return type
     */
    public static function server($module = '', $user = false) {
        $url = yii::getPathOfAlias("webroot") . '/';
//        $url = Constante::RUTA_SERVER_APU;
        return self::setParams($url, $module, $user);
    }

    /**
     * 
     * @param string $module Nombre del modulo del param.
     * @param array $parametros llaves a ser reemplazadas en el param (:id)
     * @param boolean/int $user para indicar si el param pertenece a usuario (se da el id_user para setearlo) o si es externo (FALSE).
     * @param string $tipo recibe web o server dependiendo de la url que se requiera.
     * @return string
     */
    public static function urlFile($module, $parametros = [], $user = false, $tipo = "web") {
        if ($user != false) {
            self::setId_user($user);
        }
        $path = self::web($module, $user);

        if ($tipo === "server") {
            $path = self::server($module, $user);
        }

        foreach ($parametros as $idParametro => $parametro) {
            $path = str_replace($idParametro, $parametro, $path);
        }

        return $path;
    }

    /**
     * Retorna la url de FILES de usuarios incluido el id del usuario, validando la existencia de la carpeta.
     * @return string
     */
    private static function getUserUrl() {
        Folder::crear(self::$url_user);
        $url = self::$url_user . self::getId_user() . "/";
        Folder::crear($url);
        return $url;
    }

}
