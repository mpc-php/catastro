<?php

/**
 * QUtils es la clase que se encarga de hacer funciones genéricas que apuntan a la base de datos para retornar arreglos.
 *
 * Vease a esta clase como un Helper o Utilitarios que permite concentrar
 * todas las funciones que son de uso cotidiano y utilizado por todos conectando a una base de datos.
 * 
 * @author Francisco Isasi Chiesa <franco16fra@gmail.com>
 * @package Catastro\Components
 */
class QUtils {

    /**
     * Funcion que genera el paginado para el bootrstap table
     * @param string $tabla Nombre de la tabla o vista a ser consultada
     * @param array $columnas Arreglo de columnas a ser consultadas de la tabla o vista
     * @param string $joins cadena para realizar el join respectivo de la consulta
     * @param string $where Cadena para realizar el where respectivo de la consulta
     * @param array $filtros Arreglo de filtros que contiene las keys (limit, offset y search)
     * @param string $pk Nombre del campo que cumple como llave primaria para ser usado en las acciones
     * @param array $columnas_key Arreglo de columnas de la tabla a ser mostrada, incluida el nombre de los campos de la BD, para mas detalle visualizar la funcion administrado/response/FiltrarAdministrado()
     * @return array Arreglo generado con la estructura para el bootstrap table. en el controller es necesario hacer un json_encode.
     */
    public static function generarPaginado($tabla, $columnas, $joins, $where, $filtros, $pk, $columnas_key) {
        $datos             = [];
        $columnas_anexadas = implode(",", $columnas);
        $columnas_arreglo  = [];
        foreach ($columnas as $k => $v) {
            $columnas_arreglo[$k] = "CAST(" . $v . " AS VARCHAR)";
        }
        $columnas_concatenadas = implode("+", $columnas_arreglo);
        $inicioRow             = (isset($filtros['offset'])) ? $filtros['offset'] : 1;
        $finRow                = $inicioRow + (int) ((isset($filtros['limit'])) ? $filtros['limit'] : Constante::DEFAULT_PAGESIZE);
        $filtro                = (isset($filtros['search'])) ? strtolower($filtros['search']) : "";
        $sql_where             = "%{$filtro}%";
        $order                 = (isset($filtros['order'])) ? $filtros['order'] : 'asc';
        $sort                  = (isset($filtros['sort'])) ? $filtros['sort'] : FALSE;
        $sql_sort              = "";
        $columnas_key          = array_map(function($val) {
            $find = strpos($val, ".");
            if ($find) {
                $ar = explode(".", $val);
                if (isset($ar[1])) {
                    return $ar[1];
                }
            } else {
                return $val;
            }
        }, $columnas_key);

        if ($sort) {
            if (isset($columnas_key[$sort])) {
                $campo    = $columnas_key[$sort];
                $sql_sort = "ORDER BY {$campo} {$order}";
            }
        }

        $sql_total = "SELECT count(*)
                        FROM (SELECT {$columnas_concatenadas} CONCATENADO,{$columnas_anexadas} FROM {$tabla} {$joins}) xyz
                        WHERE LOWER(xyz.CONCATENADO) LIKE '{$sql_where}' {$where}";

        $commandT       = Yii::app()->db->createCommand($sql_total);
        $datos["total"] = (int) $commandT->queryScalar();

        $sql_paginado = "SELECT *
                                FROM (SELECT ROW_NUMBER() OVER ( ORDER BY {$pk} ) AS rnum,xyz.*
                                    FROM (SELECT {$columnas_concatenadas} CONCATENADO,{$columnas_anexadas} FROM {$tabla} {$joins} {$sql_sort}) xyz
                                        WHERE LOWER(xyz.CONCATENADO) LIKE '{$sql_where}' {$where}) abc
                                WHERE abc.rnum BETWEEN {$inicioRow} AND $finRow";

        $command = Yii::app()->db->createCommand($sql_paginado);
        $models  = $command->queryAll();

        $datos['rows'] = [];
        foreach ($models as $model) {
            $arreglo = [];

            foreach ($columnas_key as $key => $value) {
                $arreglo[$key] = $model[$value];
            }

            $datos['rows'][] = $arreglo;
        }

        return $datos;
    }

}
