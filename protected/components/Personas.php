<?php

class Personas {

    public static function findByPk($idPersona) {
        return ModelPersonas::model()->findByPk($idPersona);
    }

    public static function findByTipoDocumento($idDocumento, $numeroDocumento) {
        Criteria::set([
            "id_tipo_documento"       => $idDocumento,
            "numeroDocumento_persona" => $numeroDocumento,
            "stado_persona"           => Constante::ACTIVO,
            "estado"                  => Constante::ACTIVO
        ]);
        return ModelPersonas::model()->find(Criteria::get());
    }

    public static function findByDocumento($numeroDocumento) {
        Criteria::set([
            "numeroDocumento_persona" => $numeroDocumento,
            "estado_persona"          => Constante::ACTIVO,
            "estado"                  => Constante::ACTIVO
        ]);
        return ModelPersonas::model()->find(Criteria::get());
    }

}
