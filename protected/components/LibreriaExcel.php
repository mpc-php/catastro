<?php

/**
 * libreriaExcel es la clase que se encarga de generar Excels
 * 
 * La clase se encarga de manejar la libreria PHPExcel para la generación de documentos en formato xls
 *
 * @author Francisco Isasi Chiesa <franco16fra@gmail.com>
 * @package Catastro\Components
 */
class libreriaExcel {

    /**
     * Funcion que inicializa el objeto PHPExcel para la posterior generación del documento
     * @return \PHPExcel
     */
    static public function initExcel() {
        $phpWordPath = Yii::getPathOfAlias('ext.*');

        //Utils::show($phpWordPath . DIRECTORY_SEPARATOR . 'PHPExcel.php' , true) ;
        // Turn off our amazing library autoload 
        //
        // making use of our reference, include the main class
        // when we do this, phpExcel has its own autoload registration
        // procedure (PHPExcel_Autoloader::Register();)
        include($phpWordPath . DIRECTORY_SEPARATOR . 'PHPExcel.php');

        // Create new PHPExcel object
        $PHPExcel = new PHPExcel();

        return $PHPExcel;
    }

    /**
     * Funcion que crea una hoja en el libro Excel.
     * @param PHPExcel $PHPExcel Objeto creado de la funcion initExcel
     * @param String $nombre_hoja Nombre de la hoja a ser creada en el libro actual.
     */
    static public function crea_hoja($PHPExcel, $nombre_hoja) {
        $myWorkSheet = new PHPExcel_Worksheet($PHPExcel, $nombre_hoja);
        $PHPExcel->addSheet($myWorkSheet, 0);
    }

    /**
     * Función que genera reportes específicos dependiendo del tipo que se indique (llenado de la hoja).
     * @param PHPExcel $PHPExcel Objeto creado de la función initExcel
     * @param Array $models Data a ser utilizada en el llenado de la hoja
     * @param String $tipo Tipo de reporte a ser generado
     */
    static public function genera_reporte($PHPExcel, $models, $tipo) {
        $headerstyleArray = array(
            'font'    => array(
                'bold'  => true,
                'color' => array('rgb' => 'FFFFFF'),
            ),
            'fill'    => array(
                'type'       => PHPExcel_Style_Fill::FILL_SOLID,
                'startcolor' => array('rgb' => '3598DC')
            ),
            'borders' => array(
                'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN,
                ),
            ),
        );
        $styleArray       = array(
            'borders' => array(
                'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN,
                ),
            ),
        );
        if ($tipo == "consultacontribuyente") {
            $PHPExcel->getSheet(0);
            //generamos la cabezera
            $PHPExcel->getSheet(0)->getStyle('A1:G1')->applyFromArray($headerstyleArray);
            $PHPExcel->getSheet(0)->getStyle('A1:G1')->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
            $PHPExcel->getSheet(0)->setCellValue('A1', 'NRO. FICHA');
            $PHPExcel->getSheet(0)->setCellValue('B1', 'COD. LOTE');
            $PHPExcel->getSheet(0)->setCellValue('C1', 'TIP. DOC.');
            $PHPExcel->getSheet(0)->setCellValue('D1', 'NRO. DOC.');
            $PHPExcel->getSheet(0)->setCellValue('E1', 'APELLIDOS Y NOMBRES');
            $PHPExcel->getSheet(0)->setCellValue('F1', 'COD. CONTRIB. RENTAS');
            $PHPExcel->getSheet(0)->setCellValue('G1', 'COD. CATASTRAL');
            for ($j = 0; $j < 7; $j++) {
                self::autosize($PHPExcel, $j);
            }
            $i = 2;
            foreach ($models as $model) {
                $PHPExcel->getSheet(0)->setCellValue('A' . $i, '="' . $model['code'] . '"', PHPExcel_Cell_DataType::TYPE_STRING);
                $PHPExcel->getSheet(0)->setCellValue('B' . $i, '="' . $model['codLote'] . '"', PHPExcel_Cell_DataType::TYPE_STRING);
                $PHPExcel->getSheet(0)->setCellValue('C' . $i, '="' . $model['tipDoc'] . '"', PHPExcel_Cell_DataType::TYPE_STRING);
                $PHPExcel->getSheet(0)->setCellValue('D' . $i, '="' . $model['numDoc'] . '"', PHPExcel_Cell_DataType::TYPE_STRING);
                $PHPExcel->getSheet(0)->setCellValue('E' . $i, '="' . $model['name'] . '"', PHPExcel_Cell_DataType::TYPE_STRING);
                $PHPExcel->getSheet(0)->setCellValue('F' . $i, '="' . $model['codContribRentas'] . '"', PHPExcel_Cell_DataType::TYPE_STRING);
                $PHPExcel->getSheet(0)->setCellValue('G' . $i, '="' . $model['codCatastral'] . '"', PHPExcel_Cell_DataType::TYPE_STRING);
                $PHPExcel->getSheet(0)->getStyle('A' . $i . ':G' . $i)->applyFromArray($styleArray);
                self::autosize($PHPExcel, $i);
                $i++;
            }
            $PHPExcel->getSheet(0)->getStyle('A2:G' . $i)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
        } elseif ($tipo == "consultafichacatastral") {
            $PHPExcel->getSheet(0);
            //generamos la cabezera
            $PHPExcel->getSheet(0)->getStyle('A1:C1')->applyFromArray($headerstyleArray);
            $PHPExcel->getSheet(0)->getStyle('A1:C1')->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
            $PHPExcel->getSheet(0)->setCellValue('A1', 'NRO. FICHA');
            $PHPExcel->getSheet(0)->setCellValue('B1', 'TIP. FICHA');
            $PHPExcel->getSheet(0)->setCellValue('C1', 'COD. CATASTRAL');
            for ($j = 0; $j < 3; $j++) {
                self::autosize($PHPExcel, $j);
            }
            $i = 2;
            foreach ($models as $model) {
                $PHPExcel->getSheet(0)->setCellValue('A' . $i, '="' . $model['code'] . '"', PHPExcel_Cell_DataType::TYPE_STRING);
                $PHPExcel->getSheet(0)->setCellValue('B' . $i, '="' . $model['tipFicha'] . '"', PHPExcel_Cell_DataType::TYPE_STRING);
                $PHPExcel->getSheet(0)->setCellValue('C' . $i, '="' . $model['codCatastral'] . '"', PHPExcel_Cell_DataType::TYPE_STRING);
                $PHPExcel->getSheet(0)->getStyle('A' . $i . ':C' . $i)->applyFromArray($styleArray);
                self::autosize($PHPExcel, $i);
                $i++;
            }
            $PHPExcel->getSheet(0)->getStyle('A2:C' . $i)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
        } elseif ($tipo == "consultadireccion") {
            $PHPExcel->getSheet(0);
            //generamos la cabezera
            $PHPExcel->getSheet(0)->getStyle('A1:Q1')->applyFromArray($headerstyleArray);
            $PHPExcel->getSheet(0)->getStyle('A1:Q1')->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
            $PHPExcel->getSheet(0)->setCellValue('A1', 'NRO. FICHA');
            $PHPExcel->getSheet(0)->setCellValue('B1', 'COD. LOTE');
            $PHPExcel->getSheet(0)->setCellValue('C1', 'COD. VIA');
            $PHPExcel->getSheet(0)->setCellValue('D1', 'TIP. VIA');
            $PHPExcel->getSheet(0)->setCellValue('E1', 'NOMBRE VIA');
            $PHPExcel->getSheet(0)->setCellValue('F1', 'MZ.');
            $PHPExcel->getSheet(0)->setCellValue('G1', 'LT.');
            $PHPExcel->getSheet(0)->setCellValue('H1', 'NRO. MUNI.');
            $PHPExcel->getSheet(0)->setCellValue('I1', 'TIP. PTA');
            $PHPExcel->getSheet(0)->setCellValue('J1', 'COD. H.U.');
            $PHPExcel->getSheet(0)->setCellValue('K1', 'NOMBRE HAB. URB.');
            $PHPExcel->getSheet(0)->setCellValue('L1', 'COD. PRED. RENTAS');
            $PHPExcel->getSheet(0)->setCellValue('M1', 'COD. CATASTRAL');
            $PHPExcel->getSheet(0)->setCellValue('N1', 'COD. CONTRIBUYENTE');
            $PHPExcel->getSheet(0)->setCellValue('O1', 'USO');
            $PHPExcel->getSheet(0)->setCellValue('P1', 'D.N.I.');
            $PHPExcel->getSheet(0)->setCellValue('Q1', 'DATOS PERSONA');
            for ($j = 0; $j < 7; $j++) {
                self::autosize($PHPExcel, $j);
            }
            $i = 2;
            foreach ($models as $model) {
                $PHPExcel->getSheet(0)->setCellValue('A' . $i, '="' . $model['code'] . '"', PHPExcel_Cell_DataType::TYPE_STRING);
                $PHPExcel->getSheet(0)->setCellValue('B' . $i, '="' . $model['codLote'] . '"', PHPExcel_Cell_DataType::TYPE_STRING);
                $PHPExcel->getSheet(0)->setCellValue('C' . $i, '="' . $model['codVia'] . '"', PHPExcel_Cell_DataType::TYPE_STRING);
                $PHPExcel->getSheet(0)->setCellValue('D' . $i, '="' . $model['tipVia'] . '"', PHPExcel_Cell_DataType::TYPE_STRING);
                $PHPExcel->getSheet(0)->setCellValue('E' . $i, '="' . $model['nombreVia'] . '"', PHPExcel_Cell_DataType::TYPE_STRING);
                $PHPExcel->getSheet(0)->setCellValue('F' . $i, '="' . $model['mz'] . '"', PHPExcel_Cell_DataType::TYPE_STRING);
                $PHPExcel->getSheet(0)->setCellValue('G' . $i, '="' . $model['lt'] . '"', PHPExcel_Cell_DataType::TYPE_STRING);
                $PHPExcel->getSheet(0)->setCellValue('H' . $i, '="' . $model['nroMuni'] . '"', PHPExcel_Cell_DataType::TYPE_STRING);
                $PHPExcel->getSheet(0)->setCellValue('I' . $i, '="' . $model['tipPta'] . '"', PHPExcel_Cell_DataType::TYPE_STRING);
                $PHPExcel->getSheet(0)->setCellValue('J' . $i, '="' . $model['codHU'] . '"', PHPExcel_Cell_DataType::TYPE_STRING);
                $PHPExcel->getSheet(0)->setCellValue('K' . $i, '="' . $model['nombreHabUrb'] . '"', PHPExcel_Cell_DataType::TYPE_STRING);
                $PHPExcel->getSheet(0)->setCellValue('L' . $i, '="' . $model['codPredRentas'] . '"', PHPExcel_Cell_DataType::TYPE_STRING);
                $PHPExcel->getSheet(0)->setCellValue('M' . $i, '="' . $model['codCatastral'] . '"', PHPExcel_Cell_DataType::TYPE_STRING);
                $PHPExcel->getSheet(0)->setCellValue('N' . $i, '="' . $model['codContribRentas'] . '"', PHPExcel_Cell_DataType::TYPE_STRING);
                $PHPExcel->getSheet(0)->setCellValue('O' . $i, '="' . $model['uso'] . '"', PHPExcel_Cell_DataType::TYPE_STRING);
                $PHPExcel->getSheet(0)->setCellValue('P' . $i, '="' . $model['numDoc'] . '"', PHPExcel_Cell_DataType::TYPE_STRING);
                $PHPExcel->getSheet(0)->setCellValue('Q' . $i, '="' . $model['name'] . '"', PHPExcel_Cell_DataType::TYPE_STRING);
                $PHPExcel->getSheet(0)->getStyle('A' . $i . ':M' . $i)->applyFromArray($styleArray);
                self::autosize($PHPExcel, $i);
                $i++;
            }
            $PHPExcel->getSheet(0)->getStyle('A2:M' . $i)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
        }
    }

    /**
     * Función que manda la descarga del archivo excel generado en memoria.
     * @param PHPExcel $ObjectExcel Objeto creado en la funcion initExcel
     * @param String $nombre_archivo Nombre del archivo con el que será descargado el archivo guardado.
     */
    static public function guardar_archivo($ObjectExcel, $nombre_archivo) {
        ob_end_clean();
        ob_start();

        header('Content-Type: application/vnd.ms-excel');
        header("Content-Disposition: attachment;filename=\"{$nombre_archivo}.xls\" ");
        header('Cache-Control: max-age=0');
        $objWriter = PHPExcel_IOFactory::createWriter($ObjectExcel, 'Excel5');
        $objWriter->save('php://output');
//        // Save File
//        $objWriter = PHPExcel_IOFactory::createWriter($ObjectExcel, 'Excel2007');
//        $objWriter->save($ruta . $nombre_archivo . '.xlsx');
    }

    /**
     * Función que permite cambiar el tamaño de las celdas de manera automática dependiendo del texto ingresado en la misma
     * @param PHPExcel $ObjExcel Objeto creado en la funcion initExcel
     * @param int $nroColumna Número que indica la columna en la cual se va a redimensionar la celda.
     */
    static public function autosize($ObjExcel, $nroColumna) {
        $ObjExcel->getActiveSheet()->getColumnDimensionByColumn($nroColumna)
                ->setAutoSize(true);
    }

}
