<?php

/**
 * Constante es la clase creada para almacenar constantes globales.
 * 
 * Constantes que se utilizarán por todos los programadores
 * y en todos los módulos desarrollados.
 *
 * @author Nolberto Vilchez Moreno <jnolbertovm@gmail.com>
 * @package AAHH\Components
 */
class Constante {

    /**
     * @const Nombre de la empresa
     */
    const EMPRESA = "Municipalidad Provincial del Callao";

    /**
     * @const Website de la empresa
     */
    const EMPRESA_WEBSITE = "http://www.municallao.gob.pe/";

    /**
     * @const Nombre del proyecto completo
     */
    const PROYECTO = "Gerencia de Planeamiento Urbano y Catastro";

    /**
     * @const Nombre del proyecto en siglas
     */
    const PROYECTO_SIGLAS = "GPUC";

    /**
     * @const Estado activo
     */
    const ACTIVO = 1;

    /**
     * @const Estado inactivo
     */
    const INACTIVO = 0;

    /**
     * 
     */
    const ESTADO_USUARIO_ACTIVO   = 1;
    const ESTADO_USUARIO_INACTIVO = 2;

}
