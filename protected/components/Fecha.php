<?php

class Fecha
{
	public static function format($fecha, $formato_actual, $formato_nuevo) 
	{
		if ( trim($fecha) == '' ) {
			$fecha = '01/01/1900';
			$formato_actual = 'd/m/Y';
		}

		$date = DateTime::createFromFormat($formato_actual, $fecha);
		return $date->format($formato_nuevo);
		/*return date($formato_nuevo, strtotime($fecha));*/
	}
}