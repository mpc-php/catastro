<?php

Yii::import('application.extensions.PadLock.components.QPadlock');
Yii::import('application.extensions.PadLock.components.CPadlock');

class PadLock {

    public $connectionID = "db_padlock";
    public $llave        = "xxxxxxxxxxxxxxxxxxxxxxx";
    public $secreto      = "xxxxxxxxxxxxxxxxxxxxxxx";
    public $debug        = false;
    public $db;
    private $_id;

    public function init() {
        $this->getDbConnection();
    }

    public function validarAccesoAplicacion() {
        $acceso = QPadlock::getAccesoAplicacion($this);

        if ((int) $acceso["CODIGO_ERROR"] !== CPadlock::ERROR_NONE) {
            throw new CHttpException($acceso["CODIGO_ERROR"], $acceso["NOMBRE_ERROR"]);
        }
        $this->_id = $acceso["ID"];
        return true;
    }

    public function validarSession($usuario, $contrasenia) {
        $usuario = QPadlock::getSessionAplicacion($this, $usuario, $contrasenia);
        if ((int)$usuario["CODIGO_ERROR"] !== CPadlock::ERROR_NONE) {
            return false;
        }
        return $usuario;
    }

    protected function getDbConnection() {
        if ($this->db !== null)
            return $this->db;
        elseif (($this->db = Yii::app()->getComponent($this->connectionID)) instanceof CDbConnection)
            return $this->db;
        else
            throw new PadLockException("No hay base de datos para el manejo de usuarios");
    }

    public function getId() {
        return $this->_id;
    }

}

class PadLockException extends Exception {
    
}
