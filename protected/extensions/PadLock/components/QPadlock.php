<?php

class QPadlock {

    public static function getAccesoAplicacion($padlock) {
        $llave   = $padlock->llave;
        $secreto = $padlock->secreto;

        $sql     = "EXEC SP_VALIDAR_ACCESO_APLICACION :llave, :secreto";
        $command = $padlock->db->createCommand($sql);
        $command->bindParam(":llave", $llave, PDO::PARAM_STR);
        $command->bindParam(":secreto", $secreto, PDO::PARAM_STR);
        return $command->queryRow();
    }

    public static function getSessionAplicacion($padlock, $usuario, $password) {
        $id = $padlock->getId();

        $sql     = "EXEC SP_VALIDAR_SESSION_APLICACION :id_aplicacion, :usuario, :password";
        $command = $padlock->db->createCommand($sql);
        $command->bindParam(":id_aplicacion", $id, PDO::PARAM_INT);
        $command->bindParam(":usuario", $usuario, PDO::PARAM_STR);
        $command->bindParam(":password", $password, PDO::PARAM_STR);
        return $command->queryRow();
    }

}
