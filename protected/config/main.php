<?php

return array(
    'basePath'          => dirname(__FILE__) . DIRECTORY_SEPARATOR . '..',
    'name'              => 'Catastro',
    'theme'             => 'metronic',
    'defaultController' => 'inicio',
    'language'          => 'es',
    'sourceLanguage'    => 'en',
    'timeZone'          => 'America/Lima',
    'preload'           => array('log'),
    'import'            => array(
        'application.models.*',
        'application.components.*',
    ),
    'modules'           => require(dirname(__FILE__) . '/modules.php'),
    'components'        => array(
        'user'         => [
            'allowAutoLogin' => true,
            'loginUrl'       => ['login'],
        ],
        'urlManager'   => array(
            'urlFormat'      => 'path',
            'showScriptName' => false,
            'rules'          => array(
                //Url limpia para el login
                'login'                       => 'cuenta/login',
                'logout'                      => 'cuenta/login/logout',
                'login/<action:\w+>'          => 'cuenta/login/<action>',
                'login/<action:\w+>/<id:\d+>' => 'cuenta/login/<action>',
            ),
        ),
        'padlock'      => [
            'class'        => 'ext.PadLock.PadLock',
            'connectionID' => 'access',
            'llave'        => 'gK7UEjFHpwTgCRkaTcLz',
            'secreto'      => 'AYAPPQnKUYUGxg6YnsRQ6XyrGYY7z9dyBQRTpzKZ',
            'debug'        => true
        ],
        'db'           => [],
        'access'       => [],
        'errorHandler' => array(
            'errorAction' => 'sistema/error',
        )
    ),
    'params'            => require(dirname(__FILE__) . '/params.php'),
);

