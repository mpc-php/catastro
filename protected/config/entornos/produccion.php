<?php

return [
    'components' => [
        'db'     => require(dirname(__FILE__) . '/../db.catastro.php'),
        'access' => require(dirname(__FILE__) . '/../db.padlock.php')
    ]
];
