<?php

return [
    'gii'    => [
        'class'     => 'system.gii.GiiModule',
        'password'  => '123456',
        'ipFilters' => array('127.0.0.1', '::1'),
    ],
    'inicio' => [
        'defaultController' => 'principal'
    ],
    'cuenta' => [
        'defaultController' => 'principal'
    ],
    'sistema' => [
        'defaultController' => 'principal'
    ],
    'empresa' => [
        'defaultController' => 'principal'
    ],
    'mantenedor' => [
        'defaultController' => 'principal'
    ],
    'ficha' => [
        'defaultController' => 'principal'
    ],
    'reporte' => [
        'defaultController' => 'principal'
    ],
];
