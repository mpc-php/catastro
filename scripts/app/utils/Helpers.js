function extend(a, b) {
    for (var key in b) {
        if (typeof b[key] == "object") {
            a[key] = extend(a[key], b[key]);
        } else {
            if (b.hasOwnProperty(key)) {
                a[key] = b[key];
            }
        }
    }

    return a;
}