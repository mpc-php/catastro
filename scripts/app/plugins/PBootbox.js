define(["bootbox"], function(bootbox){
    window.bootbox = bootbox;
});

(function($) {
	// What does the cBootbox plugin do?
	$.fn.cBootbox = function(options) {

		if (!this.length) { return this; }

		var ziBootbox = 1050,
				ziModal = 1051,
				ziBackdrop = 1049,
				ziIncrement = 1;

		var ELEMENT = $({}),
				BTNCLOSE = $({}),
				modal = $('<div />'),
				_win = window,
				opts = $.extend(true, {}, $.fn.cBootbox.defaults, options);

		var _beforeOpen = function() {

			//verificando la existencia modales ya abiertos
			var bootboxs = $('.bootbox.modal:last');
			if ( bootboxs.length > 0 ) {
				// en caso de encontrar modales le extrae el posicionamiento del ultimo modal
				// e incrementa los parametros en base a la variable ziIncrement
				ziBootbox 	= (parseInt(bootboxs.css('z-index')) + ziIncrement),
				ziModal 		= (parseInt(bootboxs.find('.modal-dialog').css('z-index')) + ziIncrement),
				ziBackdrop 	= (parseInt(bootboxs.siblings('.modal-backdrop').css('z-index')) + ziIncrement);
			}

			_win.setTimeout( function () {

				if ( opts.beforeOpen != null && typeof opts.beforeOpen === 'function' )
					opts.beforeOpen();

				ELEMENT.slideDown(300);

			}, 150);

		};

		var _afterClose = function() {

			_win.setTimeout(function() {
				
				if ( opts.afterClose != null && typeof opts.afterClose === 'function' )
					opts.afterClose();

				$('html').removeAttr('style');
				
				ELEMENT.slideUp(16);

			}, 150);

		};

		var _focused_element = function() {

			var element_focused = modal.find('[autofocus]');
			
			element_focused.focus();

			if ( element_focused.length > 0 ) {
				_win.setTimeout(function(){
					element_focused.focus();
				}, 200);
			}

		};

		var _close_modal = function() {
			modal.find('button[type=button].bootbox-close-button.close').click();
		};

		var _assign_btnClose = function() {
			if ( opts.btnClose !== null ) {

				if ( typeof(opts.btnClose) === 'string' ) {
					BTNCLOSE = modal.find(opts.btnClose);
				} else if ( opts.btnClose instanceof jQuery ) {
					BTNCLOSE = opts.btnClose;
				}

				if ( BTNCLOSE.length > 0 ) BTNCLOSE.on('click', _close_modal);
			}
		};

		this.each(function() {

			ELEMENT = opts.message || $(this);

			_beforeOpen();

			var nativeOptions = { 
				title: opts.title,
				message: ELEMENT,
				animation: false,
				onEscape: _afterClose,
			};

			if ( opts.size != null) {
				nativeOptions.size = opts.size;
			}

			

			//INICIANDO EL MODAL
			modal = bootbox.dialog(nativeOptions);
			
			//agregando posicionamiento del nuevo modal
			modal.css({zIndex:ziBootbox});
			modal.find('.modal-dialog').css({zIndex:ziModal});
			modal.siblings('.modal-backdrop:last').css({zIndex:ziBackdrop, overflowY:'scroll'});

			$('html').css({
				marginRight: '17px',
				overflow: 'hidden'
			});

			//BUSCAR Y ASIGNAR EL BOTON QUE CERRARA EL MODAL
			_assign_btnClose();

			//_focused_element();

		});

		return modal;
	};

	// default options
	$.fn.cBootbox.defaults = {
		title: 'Bootbox Custom',
		message: null,
		size: null,
		focus: null,
		//boton extra que cerrará el modal class|object jQuery
		btnClose: null,
		beforeOpen: null,
		afterClose: true
	};
})(jQuery);