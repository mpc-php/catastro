/* 
 * Contiene la variable global que manejara el proyecto
 * donde se colocarán valores por defecto para ser utilizado
 * por todos los programadores en todos los módulos.
 * @author Nolberto Vilchez Moreno <jnolbertovm@gmail.com>
 */

/**
 * @type object 
 */
var app = {
    requireConfig: {
        baseUrl: Request.BaseUrl + '/scripts',
//        urlArgs: "b=" + new Date().getTime(),
//        waitSeconds: 2000,
        deps: [
            "jquery",
            "bootstrap",
            "cookie",
            "slimscroll",
            "blockui",
            "switchs",
            "system"
        ],
        paths: {
            underscore: 'underscore-min',
            jquery: 'libs/jquery.min',
            bootstrap: 'libs/bootstrap/js/bootstrap.min',
            cookie: 'libs/js.cookie.min',
            slimscroll: 'libs/jquery-slimscroll/jquery.slimscroll.min',
            blockui: 'libs/jquery.blockui.min',
            switchs: 'libs/bootstrap-switch/js/bootstrap-switch.min',
            system: 'app/system.min',
            bootbox: 'libs/bootbox/bootbox.min',
            validate: 'libs/jquery-validation/js/jquery.validate.min',
            bootstrapTable: 'libs/bootstrap-table/bootstrap-table.min',
            bT_ES: "libs/bootstrap-table/locale/bootstrap-table-es-SP.min",
            bT_export: "libs/bootstrap-table/extensions/export/bootstrap-table-export.min",
            jq_export: "libs/tableExport.jquery.plugin/tableExport.min",
            bootstrapSelect: "libs/bootstrap-select/js/bootstrap-select.min",
            multi_select: "libs/jquery-multi-select/js/jquery.multi-select",
            select2: "libs/select2/js/select2.full.min",
            components_multi_select: "libs/metronic/pages/scripts/components-multi-select.min",
            bootstrapGrowl: "libs/bootstrap-growl/jquery.bootstrap-growl.min",
            bootstrapGrowlUI: "libs/metronic/pages/scripts/ui-bootstrap-growl.min",
            PBootbox: 'app/plugins/PBootbox',
            inputmask: 'libs/jquery-inputmask/jquery.inputmask.bundle.min',
            lightbox: 'libs/lightbox/src/js/lightbox',
        },
        shim: {
            underscore: {
                exports: ["_"]
            },
            jquery: {
                deps: ["underscore"],
                exports: ["$"]
            },
            bootstrap: {
                deps: ["jquery"]
            },
            multi_select: {
                deps: ["bootstrapSelect"]
            },
            select2: {
                deps: ["bootstrapSelect"]
            },
            components_multi_select: {
                deps: ["select2"]
            },
            validate: {
                deps: ["jquery"]
            },
            cookie: {
                deps: ["jquery"]
            },
            slimscroll: {
                deps: ["jquery"]
            },
            blockui: {
                deps: ["jquery"]
            },
            switchs: {
                deps: ["bootstrap"]
            },
            bootstrapSelect: {
                deps: ["bootstrap"]
            },
            bootstrapGrowl: {
                deps: ["bootstrap"]
            },
            bootstrapGrowlUI: {
                deps: ["bootstrapGrowl"]
            },
            bootbox: {
                deps: ["jquery"]
            },
            bootstrapTable: {
                deps: ["bootstrap"]
            },
            bT_ES: {
                deps: ["bootstrapTable"]
            },
            bT_export: {
                deps: ["bT_ES"]
            },
            jq_export: {
                deps: ["bT_export"]
            },
            PBootbox: {
                deps: ['bootbox']
            },
            inputmask: {
                deps: ['jquery']
            },
            lightbox: {
                deps: ['jquery']
            },
            system: {
                deps: ["cookie", "slimscroll", "blockui", "switchs"]
            }
        }

    }
};