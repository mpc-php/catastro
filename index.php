<?php

require_once(dirname(__FILE__) . '/protected/config/entorno.php');
$environment = new Entorno(Entorno::DESARROLLO);
// change the following paths if necessary
$yii = dirname(__FILE__) . '/system/framework/yii.php';
//$config = dirname(__FILE__) . '/protected/config/main.php';
// remove the following lines when in production mode
defined('YII_DEBUG') or define('YII_DEBUG', $environment->getDebug());
// specify how many levels of call stack should be shown in each log message
defined('YII_TRACE_LEVEL') or define('YII_TRACE_LEVEL', $environment->getTraceLevel());

require_once($yii);
Yii::createWebApplication($environment->getConfig())->run();
