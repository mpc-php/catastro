<!-- BEGIN GLOBAL MANDATORY STYLES -->
<link href="<?= Yii::app()->theme->baseUrl ?>/css/open_sans.css" rel="stylesheet" type="text/css" />
<link href="<?= Yii::app()->request->baseUrl ?>/scripts/libs/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
<link href="<?= Yii::app()->request->baseUrl ?>/scripts/libs/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
<link href="<?= Yii::app()->request->baseUrl ?>/scripts/libs/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="<?= Yii::app()->request->baseUrl ?>/scripts/libs/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />
<!-- END GLOBAL MANDATORY STYLES -->
<!-- BEGIN THEME GLOBAL STYLES -->
<link href="<?= Yii::app()->request->baseUrl ?>/scripts/libs/metronic/global/css/components.min.css" rel="stylesheet" type="text/css" />
<link href="<?= Yii::app()->request->baseUrl ?>/scripts/libs/metronic/global/css/plugins.min.css" rel="stylesheet" type="text/css" />
<!-- END THEME GLOBAL STYLES -->
<!-- BEGIN THEME LAYOUT STYLES -->
<link href="<?= Yii::app()->theme->baseUrl ?>/assets/layouts/layout3/css/layout.min.css" rel="stylesheet" type="text/css" />
<link href="<?= Yii::app()->theme->baseUrl ?>/assets/layouts/layout3/css/themes/default.css" rel="stylesheet" type="text/css" />
<link href="<?= Yii::app()->theme->baseUrl ?>/assets/layouts/layout3/css/custom.min.css" rel="stylesheet" type="text/css" />
<link href="<?= Yii::app()->theme->baseUrl ?>/css/app.min.css" rel="stylesheet" type="text/css" />
<!-- END THEME LAYOUT STYLES -->
<link rel="shortcut icon" href="favicon.ico" /> 
<link rel="icon" type="image/png" href="<?=Yii::app()->theme->baseUrl?>/images/isotipo-transparent-small.png" />