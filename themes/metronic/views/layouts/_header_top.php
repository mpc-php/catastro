<div class="page-header-top">
    <div class="container-fluid">
        <!-- BEGIN LOGO -->
        <div class="page-logo">
            <a href="index.html">
                <img src="<?= Yii::app()->theme->baseUrl ?>/images/logo-red-xsmall.png" alt="logo" class="logo-default">
            </a>
        </div>
        <!-- END LOGO -->
        <!-- BEGIN RESPONSIVE MENU TOGGLER -->
        <a href="javascript:;" class="menu-toggler"></a>
        <!-- END RESPONSIVE MENU TOGGLER -->
        <!-- BEGIN TOP NAVIGATION MENU -->
        <div class="top-menu">
            <ul class="nav navbar-nav pull-right">
                <!-- BEGIN TODO DROPDOWN -->
                <li class="dropdown" id="header_task_bar">
                    <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true" title="Año de proceso">
                        <strong>Año de Proceso:&nbsp;</strong><small><?= Yii::app()->user->proceso['selected'] ?></small>
                    </a>
                    <ul id="processYear" class="dropdown-menu" role="menu" style="min-width:100px; text-align:center;">
                    <?php foreach (Yii::app()->user->proceso['items'] as $key => $val): ?>
                        <li role="presentation" class="<?= (Yii::app()->user->proceso['selected'] == $val) ? 'active' : '' ?>">
                            <a role="menuitem" 
                                class="itemYear <?= (Yii::app()->user->proceso['selected'] == $val) ? 'selected' : '' ?>" 
                                data-key="<?= $key ?>" 
                                tabindex="-1" 
                                href="javascript:;">
                                <strong><?= $val ?></strong>
                            </a>
                        </li>
                    <?php endforeach; ?>
                    </ul>
                </li>
                <!-- END TODO DROPDOWN -->
                <li class="droddown dropdown-separator">
                    <span class="separator"></span>
                </li>
                <!-- BEGIN USER LOGIN DROPDOWN -->
                <li class="dropdown dropdown-user dropdown-dark">
                    <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                        <img alt="" class="img-circle" src="<?= Yii::app()->theme->baseUrl ?>/assets/layouts/layout3/img/avatar9.jpg">
                        <span class="username username-hide-mobile"><?= Yii::app()->user->nombres ?> <?= Yii::app()->user->apellido_paterno ?></span>
                    </a>
                    <ul class="dropdown-menu dropdown-menu-default">
                        <li>
                            <a href="#">
                                <i class="icon-user"></i> Editar Perfil </a>
                        </li>
                        <li class="divider"> </li>
                        <li>
                            <a href="<?= Yii::app()->createUrl("logout") ?>">
                                <i class="icon-key"></i> Cerrar Sesión </a>
                        </li>
                    </ul>
                </li>
                <!-- END USER LOGIN DROPDOWN -->
              
            </ul>
        </div>
        <!-- END TOP NAVIGATION MENU -->
    </div>
</div>