<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
    <!--<![endif]-->
    <!-- BEGIN HEAD -->
    <head>
        <meta charset="utf-8" />
        <title><?= CHtml::encode($this->pageTitle); ?></title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport" />
        <meta content="#1 selling multi-purpose bootstrap admin theme sold in themeforest marketplace packed with angularjs, material design, rtl support with over thausands of templates and ui elements and plugins to power any type of web applications including saas and admin dashboards. Preview page of Theme #3 for "
              name="description" />
        <meta content="" name="author" />
        <?php $this->renderPartial("//layouts/_links"); ?>
        <!--[if lt IE 9]>
        <script src="<?= Yii::app()->request->baseUrl ?>/scripts/libs/respond.min.js"></script>
        <script src="<?= Yii::app()->request->baseUrl ?>/scripts/libs/excanvas.min.js"></script> 
        <script src="<?= Yii::app()->request->baseUrl ?>/scripts/libs/ie8.fix.min.js"></script> 
        <![endif]-->
    </head>
    <!-- END HEAD -->
    <body class="page-container-bg-solid">
        <div class="page-wrapper">
            <div class="page-wrapper-row">
                <div class="page-wrapper-top">
                    <!-- BEGIN HEADER -->
                    <div class="page-header">
                        <!-- BEGIN HEADER TOP -->
                        <?php $this->renderPartial("//layouts/_header_top"); ?>
                        <!-- END HEADER TOP -->
                        <!-- BEGIN HEADER MENU -->
                        <?php $this->renderPartial("//layouts/_header_menu"); ?>
                        <!-- END HEADER MENU -->
                    </div>
                    <!-- END HEADER -->
                </div>
            </div>
            <div class="page-wrapper-row full-height">
                <div class="page-wrapper-middle">
                    <!-- BEGIN CONTAINER -->
                    <div class="page-container">
                        <!-- BEGIN CONTENT -->
                        <div class="page-content-wrapper">
                            <?= $content ?>
                        </div>
                        <!-- END CONTENT -->
                    </div>
                    <!-- END CONTAINER -->
                </div>
            </div>
            <div class="page-wrapper-row">
                <div class="page-wrapper-bottom">
                    <!-- BEGIN FOOTER -->
                    <?php $this->renderPartial("//layouts/_footer"); ?>
                    <!-- END FOOTER -->
                </div>
            </div>
        </div>
        <!-- BEGIN QUICK NAV -->
        <?php $this->renderPartial("//layouts/_quick_nav"); ?>
        <!-- END QUICK NAV -->
        <!-- BEGIN THEME GLOBAL SCRIPTS -->
        <!--<script src="../assets/global/scripts/app.min.js" type="text/javascript"></script>-->
        <!-- END THEME GLOBAL SCRIPTS -->
        <!-- BEGIN THEME LAYOUT SCRIPTS -->
        <!--<script src="../assets/layouts/layout3/scripts/layout.min.js" type="text/javascript"></script>-->
        <!--<script src="../assets/layouts/layout3/scripts/demo.min.js" type="text/javascript"></script>-->
        <!--<script src="../assets/layouts/global/scripts/quick-sidebar.min.js" type="text/javascript"></script>-->
        <!--<script src="../assets/layouts/global/scripts/quick-nav.min.js" type="text/javascript"></script>-->
        <!-- END THEME LAYOUT SCRIPTS -->
        <?php $this->renderPartial("//layouts/_scripts"); ?>
    </body>

</html>