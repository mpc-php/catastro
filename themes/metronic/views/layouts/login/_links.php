<!-- BEGIN GLOBAL MANDATORY STYLES -->
<link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
<link href="<?= Yii::app()->request->baseUrl ?>/scripts/libs/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
<link href="<?= Yii::app()->request->baseUrl ?>/scripts/libs/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
<link href="<?= Yii::app()->request->baseUrl ?>/scripts/libs/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="<?= Yii::app()->request->baseUrl ?>/scripts/libs/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />
<!-- END GLOBAL MANDATORY STYLES -->
<!-- BEGIN THEME GLOBAL STYLES -->
<link href="<?= Yii::app()->request->baseUrl ?>/scripts/libs/metronic/global/css/components.min.css" rel="stylesheet" type="text/css" />
<link href="<?= Yii::app()->request->baseUrl ?>/scripts/libs/metronic/global/css/plugins.min.css" rel="stylesheet" type="text/css" />
<!-- END THEME GLOBAL STYLES -->
<!-- BEGIN THEME LAYOUT STYLES -->
<link href="<?= Yii::app()->request->baseUrl ?>/scripts/libs/metronic/pages/css/login.min.css" rel="stylesheet" type="text/css" />
<link href="<?= Yii::app()->theme->baseUrl ?>/css/login.min.css" rel="stylesheet" type="text/css" />
<!-- END THEME LAYOUT STYLES -->
<link rel="shortcut icon" href="favicon.ico" />
<link rel="icon" type="image/png" href="<?=Yii::app()->theme->baseUrl?>/images/isotipo-transparent-small.png" />