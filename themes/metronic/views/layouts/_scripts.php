<script type="text/javascript">
    var Request = {
        Host: '<?= Yii::app()->request->hostInfo ?>',
        BaseUrl: '<?= Yii::app()->baseUrl ?>',
        _GET: <?= json_encode($_GET) ?>,
        UrlHash: {
            m: '<?= strtolower($this->module->id) ?>',
            c: '<?= strtolower($this->id) ?>',
            a: '<?= strtolower($this->action->id) ?>'
        },
        guest: <?= (Yii::app()->user->isGuest) ? "true" : "false" ?>};
</script>
<script src="<?= Yii::app()->request->baseUrl ?>/scripts/app/settings.js"></script>
<!--config.js De los modulos-->
<?php
Assets::setModule($this->module->id);
Assets::configModule();
?>
<!--fin-->
<script src="<?= Yii::app()->request->baseUrl ?>/scripts/require.js"></script>
<script src="<?= Yii::app()->request->baseUrl ?>/scripts/app/common.js"></script>