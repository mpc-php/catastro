<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
    <!--<![endif]-->
    <!-- BEGIN HEAD -->
    <head>
        <meta charset="utf-8" />
        <title><?= CHtml::encode($this->pageTitle); ?></title>
        <?php $this->renderPartial("//layouts/_metas"); ?>
        <?php $this->renderPartial("//layouts/login/_links"); ?>
        <!--[if lt IE 9]>
        <script src="<?= Yii::app()->request->baseUrl ?>/scripts/libs/respond.min.js"></script>
        <script src="<?= Yii::app()->request->baseUrl ?>/scripts/libs/excanvas.min.js"></script> 
        <script src="<?= Yii::app()->request->baseUrl ?>/scripts/libs/ie8.fix.min.js"></script> 
        <![endif]-->
    </head>
    <!-- END HEAD -->
    <body class="login">
        
        <!-- BEGIN LOGO -->
        <div class="logo">
            <a href="#">
                <img src="<?= Yii::app()->theme->baseUrl ?>/images/logo-red-small.png" alt="" /> 
            </a>
        </div>
        <!-- END LOGO -->
        <!-- BEGIN LOGIN -->
        <div class="content">
            <!-- BEGIN LOGIN FORM -->
            <?= $content ?>
            <!-- END LOGIN FORM -->
        </div>
        <!-- BEGIN FOOTER -->
        <?php $this->renderPartial("//layouts/login/_footer"); ?>
        <!-- END FOOTER -->

        <?php $this->renderPartial("//layouts/_scripts"); ?>
    </body>

</html>