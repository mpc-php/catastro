<div class="page-header-menu">
    <div class="container-fluid">
        <!-- BEGIN MEGA MENU -->
        <div class="hor-menu  ">
            <ul class="nav navbar-nav">
                <li class="">
                    <a href="<?= Yii::app()->createUrl("") ?>"> 
                        <i class="fa fa-home"></i> Inicio
                        <span class="arrow"></span>
                    </a>

                </li>
                <li class="menu-dropdown classic-menu-dropdown <?= (isset($this->module->id) && $this->module->id == "mantenedor") ? "active" : "" ?>">
                    <a href="javascript:;">
                        <i class="fa fa-cogs"></i> Mantenedores
                        <span class="arrow"></span>
                    </a>
                    <ul class="dropdown-menu pull-left">
                        <li class="<?= (isset($this->module->id) && ($this->module->id == "mantenedor" && $this->id == "sector")) ? "active" : "" ?>">
                            <a href="<?= Yii::app()->createUrl("mantenedor/sector") ?>" class="nav-link  "> 
                                <i class="fa fa-map"></i> Sectores 
                                <span class="arrow"></span>
                            </a>
                        </li>
                        <li class="<?= (isset($this->module->id) && ($this->module->id == "mantenedor" && $this->id == "haburbanas")) ? "active" : "" ?>">
                            <a href="<?= Yii::app()->createUrl("mantenedor/haburbanas") ?>" class="nav-link  "> 
                                <i class="fa fa-home"></i>Hab. Urbanas 
                                <span class="arrow"></span>
                            </a>
                        </li>
                        <li class="<?= (isset($this->module->id) && ($this->module->id == "mantenedor" && $this->id == "via")) ? "active" : "" ?>">
                            <a href="<?= Yii::app()->createUrl("mantenedor/via") ?>" class="nav-link  "> 
                                <i class="fa fa-road"></i> Vías 
                                <span class="arrow"></span>                            
                            </a>
                        </li>
                        <li class="<?= (isset($this->module->id) && ($this->module->id == "mantenedor" && $this->id == "acteconomicas")) ? "active" : "" ?>">
                            <a href="<?= Yii::app()->createUrl("mantenedor/acteconomicas") ?>" class="nav-link  "> 
                                <i class="fa fa-dollar"></i> Act. Económicas 
                                <span class="arrow"></span>                            
                            </a>
                        </li>
                        <li class="<?= (isset($this->module->id) && ($this->module->id == "mantenedor" && $this->id == "usos")) ? "active" : "" ?>">
                            <a href="<?= Yii::app()->createUrl("mantenedor/usos") ?>" class="nav-link  "> 
                                <i class="fa fa-archive"></i> Usos 
                                <span class="arrow"></span>                            
                            </a>
                        </li>
                        <li class="<?= (isset($this->module->id) && ($this->module->id == "mantenedor" && $this->id == "anuncios")) ? "active" : "" ?>">
                            <a href="<?= Yii::app()->createUrl("mantenedor/anuncios") ?>" class="nav-link "> 
                                <i class="fa fa-dashboard"></i> Anuncios 
                                <span class="arrow"></span>                            
                            </a>
                        </li>
                        <li class="<?= (isset($this->module->id) && ($this->module->id == "mantenedor" && $this->id == "obrascomplementarias")) ? "active" : "" ?>">
                            <a href="<?= Yii::app()->createUrl("mantenedor/obrascomplementarias") ?>" class="nav-link  active"> 
                                <i class="fa fa-cubes"></i> Obras Comp.
                                <span class="arrow"></span>                            
                            </a>
                        </li>
                        <li class="<?= (isset($this->module->id) && ($this->module->id == "mantenedor" && $this->id == "tecnico")) ? "active" : "" ?>">
                            <a href="<?= Yii::app()->createUrl("mantenedor/tecnico") ?>" class="nav-link  active"> 
                                <i class="fa fa-users"></i> Tecnicos
                                <span class="arrow"></span>                            
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="menu-dropdown classic-menu-dropdown <?= (isset($this->module->id) && $this->module->id == "ficha") ? "active" : "" ?>">
                    <a href="javascript:;">
                        <i class="fa fa-keyboard-o"></i> Digitación
                        <span class="arrow"></span>
                    </a>
                    <ul class="dropdown-menu pull-left">
                        <li class="dropdown-submenu ">
                            <a href="javascript:;" class="nav-link nav-toggle ">
                                <i class="fa fa-file-word-o"></i> Fichas Catastrales
                                <span class="arrow"></span>
                            </a>
                            <ul class="dropdown-menu">
                                <li class=" ">
                                    <a href="<?= Yii::app()->createUrl("ficha/individual/crear") ?>" class="nav-link ">
                                        <i class="fa fa-user"></i> Ficha Individual </a>
                                </li>
                                <li class=" ">
                                    <a href="<?= Yii::app()->createUrl("ficha/cotitularidad/crear") ?>" class="nav-link ">
                                        <i class="fa fa-users"></i> Ficha de Cotitularidad </a>
                                </li>
                                <li class=" ">
                                    <a href="<?= Yii::app()->createUrl("ficha/acteconomica/crear") ?>" class="nav-link ">
                                        <i class="fa fa-money"></i> Ficha Act. Económica </a>
                                </li>
                                <li class=" ">
                                    <a href="<?= Yii::app()->createUrl("ficha/biencomun/crear") ?>" class="nav-link ">
                                        <i class="fa fa-home"></i> Ficha de Bien Común </a>
                                </li>
                            </ul>
                        </li>
                        <li class="dropdown-submenu ">
                            <a href="javascript:;" class="nav-link nav-toggle ">
                                <i class="fa fa-edit"></i> Modificación
                                <span class="arrow"></span>
                            </a>
                            <ul class="dropdown-menu">
                                <li class=" ">
                                    <a href="<?= Yii::app()->createUrl("ficha/individual/") ?>" class="nav-link ">
                                        <i class="fa fa-user"></i> Ficha Individual </a>
                                </li>
                                <li class=" ">
                                    <a href="<?= Yii::app()->createUrl("ficha/cotitularidad/") ?>" class="nav-link ">
                                        <i class="fa fa-users"></i> Ficha de Cotitularidad </a>
                                </li>
                                <li class=" ">
                                    <a href="<?= Yii::app()->createUrl("ficha/acteconomica/") ?>" class="nav-link ">
                                        <i class="fa fa-money"></i> Ficha Act. Económica </a>
                                </li>
                                <li class=" ">
                                    <a href="<?= Yii::app()->createUrl("ficha/biencomun/") ?>" class="nav-link ">
                                        <i class="fa fa-home"></i> Ficha de Bien Común </a>
                                </li>
                            </ul>
                        </li>

                    </ul>
                </li>
                <li class="menu-dropdown classic-menu-dropdown <?= (isset($this->module->id) && $this->module->id == "reporte") ? "active" : "" ?>">
                    <a href="javascript:;"> 
                        <i class="fa fa-search-plus"></i> Consulta
                        <span class="arrow"></span>
                    </a>
                    <ul class="dropdown-menu pull-left">
                        <li class="<?= (isset($this->module->id) && ($this->module->id == "reporte" && $this->action->id == "consultaContribuyente")) ? "active" : "" ?>">
                            <a href="<?= Yii::app()->createUrl("reporte/principal/consultaContribuyente") ?>" class="nav-link  "> 
                                <i class="fa fa-user"></i> Por Contribuyente 
                            </a>
                        </li>
                        <li class="<?= (isset($this->module->id) && ($this->module->id == "reporte" && $this->action->id == "consultaFichaCatastral")) ? "active" : "" ?>">
                            <a href="<?= Yii::app()->createUrl("reporte/principal/consultaFichaCatastral") ?>" class="nav-link  "> 
                                <i class="fa fa-file-word-o"></i> Por Ficha Catastral 
                            </a>
                        </li>
                        <li class="<?= (isset($this->module->id) && ($this->module->id == "reporte" && $this->action->id == "consultaDireccion")) ? "active" : "" ?>">
                            <a href="<?= Yii::app()->createUrl("reporte/principal/consultaDireccion") ?>" class="nav-link  "> 
                                <i class="fa fa-road"></i> Por Direccion 
                            </a>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
        <!-- END MEGA MENU -->
    </div>
</div>