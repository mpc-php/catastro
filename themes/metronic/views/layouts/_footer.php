<!-- BEGIN INNER FOOTER -->
<div class="page-footer">
    <div class="container"> <?= date("Y") ?> &copy; <a href="<?= Constante::EMPRESA_WEBSITE ?>" title="<?= Constante::EMPRESA ?>" target="_blank"><?= Constante::EMPRESA ?></a>
        &nbsp;|&nbsp;
        <?= Constante::PROYECTO ?>
    </div>
</div>
<div class="scroll-to-top">
    <i class="icon-arrow-up"></i>
</div>
<!-- END INNER FOOTER -->